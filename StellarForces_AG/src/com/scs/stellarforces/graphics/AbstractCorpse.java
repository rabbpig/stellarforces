package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

import dsr.data.UnitData;
import dsrwebserver.tables.UnitsTable;

public class AbstractCorpse extends GameObject {

	private Bitmap bmp;


	/**
	 * This is called when a unit has just been killed in the game.
	 */
	public static AbstractCorpse CorpseFactory(GameModule m, UnitData data) {
		switch (data.model_type) {
		case UnitsTable.MT_MALE_SIDE_1:
			return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.corpse1);
		case UnitsTable.MT_MALE_SIDE_2:
			return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.corpse2);
		case UnitsTable.MT_MALE_SIDE_3:
			return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.corpse3);
		case UnitsTable.MT_MALE_SIDE_4:
			return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.corpse4);
		case UnitsTable.MT_ALIEN_TYRANT:
			return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.alien_corpse);
		case UnitsTable.MT_QUEEN_ALIEN:
			return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.alien_corpse);
		case UnitsTable.MT_SCIENTIST:
			return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.sterner_corpse);
		case UnitsTable.MT_BLOB:
			return null;
		case UnitsTable.MT_CRAB:
			return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.alien_corpse);
		case UnitsTable.MT_GHOUL:
			return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.alien_corpse);
		case UnitsTable.MT_GHOUL_QUEEN:
			return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.alien_corpse);
		case UnitsTable.MT_ZOMBIE:
			return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.zombie_corpse);
		case UnitsTable.MT_ANGEL:
			return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.angel_corpse);
		case UnitsTable.MT_CLONE:
			switch (data.getSide()) {
			case 1:
				return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.clone1_corpse);
			case 2:
				return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.clone2_corpse);
			case 3:
				return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.clone3_corpse);
			case 4:
				return new AbstractCorpse(m, data.name, data.getMapX(), data.getMapY(), R.drawable.clone4_corpse);
			default:
				throw new RuntimeException("Unknown clone side: " + data.getSide());
			}
			// If you add any here, also add them to EquipmentModel.Factory
		default:
			throw new RuntimeException("Unknown model type: " + data.model_type);
		}
	}


	public AbstractCorpse(GameModule _game, String name, int map_x, int map_y, int img) {
		super(_game, "Corpse_" + name, true, map_x * Statics.SQ_SIZE, map_y * Statics.SQ_SIZE, Statics.SQ_SIZE, Statics.SQ_SIZE, false, false);

		bmp = _game.img_cache.getImage(img, Statics.SQ_SIZE, Statics.SQ_SIZE);
		//game.attachToRootNode(this, true);
	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		//if (this.visible) {
		g.drawBitmap(bmp, this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);
		//}
	}


}
