package com.scs.stellarforces.graphics.gui;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.lib2d.Node;
import ssmith.android.lib2d.gui.Label;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

import dsr.data.EquipmentData;
import dsr.data.UnitData;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public class StatusBarNode extends Node {

	private GameModule game;
	private Label str_name, str_equip;

	private static Paint paint_bigger_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.045f));

		paint_bigger_text.setARGB(255, 255, 255, 255);
		paint_bigger_text.setAntiAlias(true);
		paint_bigger_text.setTextSize(Statics.GetHeightScaled(0.055f));
	}


	public StatusBarNode(GameModule _game, Typeface tf) {
		super("StatNode");

		paint_normal_text.setTypeface(tf);
		paint_bigger_text.setTypeface(tf);
		
		game = _game;

		str_name = new Label("Unit_Name", "", Statics.ICONS_WIDTH*2, 0, LogWindow.back_col, paint_bigger_text, false);
		this.attachChild(str_name);
		str_equip = new Label("Unit_Equipment", "", Statics.ICONS_WIDTH*2, paint_bigger_text.getTextSize() * Statics.LABEL_SPACING, LogWindow.back_col, paint_normal_text, false);
		this.attachChild(str_equip);


	}


	public void updateStatus() {
		AbstractActivity act = Statics.act;
		
		UnitData current_unit = game.getCurrentUnit();

		str_name.setText("");
		str_equip.setText("");
		if (game.game_data != null) {
			if (game.game_data.game_status == GamesTable.GS_DEPLOYMENT) {
				if (game.next_to_deploy >= 0) {
					str_name.append(act.getString(R.string.deploying_unit) + " " + game.units[game.next_to_deploy].order_by + ": " + game.units[game.next_to_deploy].name);
				} else {
					str_name.append(act.getString(R.string.deployment_finished));
				}
			} else if (game.game_data.game_status == GamesTable.GS_STARTED) {
				if (current_unit != null) {
					String name = current_unit.name;
					if (current_unit.unit_type == UnitsTable.UT_ENGINEER) {
						name = name + " (Eng)";
					}
					str_name.append(current_unit.order_by + " " + name + "  - APs: " + current_unit.getAPs() + " (" + current_unit.opp_fire_aps_req + ") - HP: " + current_unit.getHealth() + "/" + current_unit.max_health);
					if (game.game_data.is_advanced == 1 && current_unit.skillid > 0) {
						str_equip.append(UnitsTable.Skill2String(current_unit.skillid) + " ");
					}
					str_equip.append("Prot:" + current_unit.protection);
					/*if (Statics.IsAE()) {
						if (game.game_data.getOurSpecialPower() == GamesTable.SP_TELEPORT || game.game_data.getOurSpecialPower() == GamesTable.SP_CONVERT_ENERGY) {
							str_equip.append("  Power Points:" + current_unit.power_points);
						}
					} else {*/
						if (current_unit.current_item != null) {
							EquipmentData eq = current_unit.current_item;
							str_equip.append(" - " + eq.getName(true) + " ");
						} else if (current_unit.can_use_equipment) {
							str_equip.append(" - * " + act.getString(R.string.nothing) + " *");
						}
					//}
				}
			}
		} else {
			str_name.append("SELECT GAME");

		}

		this.str_name.updateWidth();
		this.str_equip.updateWidth();

		this.str_name.visible = this.str_name.getText().length() > 0;
		this.str_equip.visible = this.str_equip.getText().length() > 0;

		if (str_name.getText().length() > 0 || str_equip.getText().length() > 0) {
			// Change colour if unit injured
			if (game.game_data != null) {
				if (game.game_data.game_status == GamesTable.GS_STARTED) {
					if (current_unit != null) {
						if (current_unit.getHealth() < current_unit.max_health/2) {
							//g.setColor(Color.red);
							str_name.setInkRGB(255, 0, 0);
						} else if (current_unit.getHealth() < current_unit.max_health) {
							str_name.setInkRGB(255, 255, 0);
						} else {
							str_name.setInkRGB(255, 255, 255);
						}
					}
				} else {
					str_name.setInkRGB(255, 255, 255);
				}
			}
		}
	}

}
