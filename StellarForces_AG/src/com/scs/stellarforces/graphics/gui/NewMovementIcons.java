package com.scs.stellarforces.graphics.gui;

import ssmith.android.lib2d.layouts.GridLayout;
import android.graphics.Bitmap;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.graphics.icons.MovementIcon;
import com.scs.stellarforces.main.lite.R;

import dsr.data.UnitData;

public class NewMovementIcons extends GridLayout {

	private GameModule game;

	public static Bitmap bmp_arrow_up, bmp_arrow_up_right, bmp_arrow_right, bmp_arrow_down_right, bmp_arrow_down, bmp_arrow_down_left, bmp_arrow_left, bmp_arrow_up_left;
	public static Bitmap bmp_turn_left, bmp_turn_right;
	//public static Bitmap[] bmps;

	static {
		bmp_arrow_up = Statics.img_cache.getImage(R.drawable.arrow_up,  Statics.ICONS_WIDTH,  Statics.ICONS_HEIGHT);
		bmp_arrow_up_right = Statics.img_cache.getImage(R.drawable.arrow_up_right,  Statics.ICONS_WIDTH,  Statics.ICONS_HEIGHT);
		bmp_arrow_right = Statics.img_cache.getImage(R.drawable.arrow_right,  Statics.ICONS_WIDTH,  Statics.ICONS_HEIGHT);
		bmp_arrow_down_right = Statics.img_cache.getImage(R.drawable.arrow_down_right,  Statics.ICONS_WIDTH,  Statics.ICONS_HEIGHT);
		bmp_arrow_down = Statics.img_cache.getImage(R.drawable.arrow_down,  Statics.ICONS_WIDTH,  Statics.ICONS_HEIGHT);
		bmp_arrow_down_left = Statics.img_cache.getImage(R.drawable.arrow_down_left,  Statics.ICONS_WIDTH,  Statics.ICONS_HEIGHT);
		bmp_arrow_left = Statics.img_cache.getImage(R.drawable.arrow_left,  Statics.ICONS_WIDTH,  Statics.ICONS_HEIGHT);
		bmp_arrow_up_left = Statics.img_cache.getImage(R.drawable.arrow_up_left,  Statics.ICONS_WIDTH,  Statics.ICONS_HEIGHT);
		bmp_turn_left = Statics.img_cache.getImage(R.drawable.curved_arrow_left,  Statics.ICONS_WIDTH,  Statics.ICONS_HEIGHT);
		bmp_turn_right = Statics.img_cache.getImage(R.drawable.curved_arrow_right,  Statics.ICONS_WIDTH,  Statics.ICONS_HEIGHT);

	}


	public NewMovementIcons(GameModule _game) {
		super("NewMovementIcons", Statics.ICONS_WIDTH, Statics.ICONS_HEIGHT, 5);

		game = _game;
	}


	public void updateMovementIcons(UnitData unit) {
		this.detachAllChildren();

		int dir = unit.angle/45;
		switch (dir) {
		case 0: // East
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_up), 0, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_LEFT, R.drawable.arrow_up), 1, 0); // Done
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.curved_arrow_left_ur), 2, 0); // Done
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_FWD, R.drawable.arrow_right), 2, 1); // Done
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_RIGHT, R.drawable.curved_arrow_right_dr), 2, 2); // Done
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_RIGHT, R.drawable.arrow_down), 1, 2);
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_up), 0, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_BACK, R.drawable.arrow_left), 0, 1); // Done
			break;
		case 1: // South East
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_BACK, R.drawable.arrow_up_left), 0, 0);
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_up), 1, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_LEFT, R.drawable.arrow_up_right), 2, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.curved_arrow_left_ur), 2, 1);
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_FWD, R.drawable.arrow_down_right), 2, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_RIGHT, R.drawable.curved_arrow_right_dl), 1, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_RIGHT, R.drawable.arrow_down_left), 0, 2);
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_down), 0, 1);
			break;
		case 2: // South
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_up), 0, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_BACK, R.drawable.arrow_up), 1, 0);
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_left), 2, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_LEFT, R.drawable.arrow_right), 2, 1);
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.curved_arrow_left_dr), 2, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_FWD, R.drawable.arrow_down), 1, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_RIGHT, R.drawable.curved_arrow_right_dl), 0, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_RIGHT, R.drawable.arrow_left), 0, 1);
			break;
		case 3: // South West
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_RIGHT, R.drawable.arrow_up_left), 0, 0);
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_up), 1, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_BACK, R.drawable.arrow_up_right), 2, 0);
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_right), 2, 1);
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_LEFT, R.drawable.arrow_down_right), 2, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.curved_arrow_left_dr), 1, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_FWD, R.drawable.arrow_down_left), 0, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_RIGHT, R.drawable.curved_arrow_right_ul), 0, 1);
			break;
		case 4: // West
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_RIGHT, R.drawable.curved_arrow_right_ul), 0, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_RIGHT, R.drawable.arrow_up), 1, 0);
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_left), 2, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_BACK, R.drawable.arrow_right), 2, 1);
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_down), 2, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_LEFT, R.drawable.arrow_down), 1, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.curved_arrow_left_dl), 0, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_FWD, R.drawable.arrow_left), 0, 1);
			break;
		case 5: // North West
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_FWD, R.drawable.arrow_up_left), 0, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_RIGHT, R.drawable.curved_arrow_right_ur), 1, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_RIGHT, R.drawable.arrow_up_right), 2, 0);
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_right), 2, 1);
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_BACK, R.drawable.arrow_down_right), 2, 2);
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_down), 1, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_LEFT, R.drawable.arrow_down_left), 0, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.curved_arrow_left_dl), 0, 1);
			break;
		case 6: // North
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.curved_arrow_left_ul), 0, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_FWD, R.drawable.arrow_up), 1, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_RIGHT, R.drawable.curved_arrow_right_ur), 2, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_RIGHT, R.drawable.arrow_right), 2, 1);
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_down), 2, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_BACK, R.drawable.arrow_down), 1, 2);
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_up), 0, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_LEFT, R.drawable.arrow_left), 0, 1);
			break;
		case 7: // North East
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_LEFT, R.drawable.arrow_up_left), 0, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.curved_arrow_left_ul), 1, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_FWD, R.drawable.arrow_up_right), 2, 0);
			this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_RIGHT, R.drawable.curved_arrow_right_dr), 2, 1);
			this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_RIGHT, R.drawable.arrow_down_right), 2, 2);
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_down), 1, 2);
			this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_BACK, R.drawable.arrow_down_left), 0, 2);
			//this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.arrow_down), 0, 1);
			break;
		}

		this.updateGeometricState();
	}


	/*private void drawMovementIcons(Canvas g, Camera cam) {
		int cell = 0;
		for (int y=-1 ; y<=1 ; y++) {
			for (int x=-1 ; x<=1 ; x++) {
				if (movement_icons != null) {
					if (movement_icons[cell] != null) {
						g.drawBitmap(movement_icons[cell], this.world_bounds.left - cam.left + (Statics.ICONS_WIDTH*x), this.world_bounds.top - cam.top + (Statics.ICONS_HEIGHT*y), paint);
					}
				}
				cell++;
			}
		}

	}*/


	/*static int[] Angles = {MovementIcon.MOVE_FWD, MovementIcon.TURN_RIGHT, MovementIcon.SLIDE_RIGHT, -1, MovementIcon.MOVE_BACK, -1, MovementIcon.SLIDE_LEFT, MovementIcon.TURN_LEFT};
	public static int GetIconFromPosition(UnitData unit, int x, int y) {
		return Angles[unit.angle/45];
	}*/


}
