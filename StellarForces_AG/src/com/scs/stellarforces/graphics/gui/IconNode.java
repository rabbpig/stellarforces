package com.scs.stellarforces.graphics.gui;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.lib2d.layouts.FlowGridLayout;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.graphics.icons.AbstractIcon;
import com.scs.stellarforces.graphics.icons.CommandIcon;
import com.scs.stellarforces.graphics.icons.EquipmentIcon;
import com.scs.stellarforces.graphics.icons.LessIcon;
import com.scs.stellarforces.graphics.icons.MoreIcon;
import com.scs.stellarforces.graphics.icons.PrimeGrenadeIcon;
import com.scs.stellarforces.graphics.icons.ShotTypeIcon;
import com.scs.stellarforces.graphics.icons.ThrowIcon;
import com.scs.stellarforces.main.lite.R;

import dsr.data.AppletMapSquare;
import dsr.data.EquipmentData;
import dsr.data.UnitData;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public class IconNode extends FlowGridLayout {

	public byte menu_mode;
	public ArrayList<AbstractIcon> our_unit_icons = new ArrayList<AbstractIcon>();
	public ArrayList<AbstractIcon> visible_enemy_icons = new ArrayList<AbstractIcon>();

	// Icons
	private CommandIcon cancel_icon;
	private CommandIcon scanner_icon;
	private CommandIcon next_tutorial_icon;
	private CommandIcon pickup_item_icon;
	private CommandIcon change_item_icon;
	private CommandIcon equip_unit_icon;
	private CommandIcon drop_item_icon;
	private CommandIcon remove_item_icon;
	private CommandIcon select_shot_type_icon;
	private CommandIcon show_prime_menu_icon;
	private CommandIcon activate_menu_icon;
	public PrimeGrenadeIcon do_prime_icon;
	//private CommandIcon end_turn_icon;
	//private CommandIcon confirm_end_turn_icon;
	private ShotTypeIcon aimed_shot_icon;
	private ShotTypeIcon snap_shot_icon;
	private ShotTypeIcon auto_shot_icon;
	private CommandIcon throw_menu_icon;
	private CommandIcon escape_icon;
	public ThrowIcon throw_icon;
	private CommandIcon reload_icon;
	public LessIcon less_icon;
	public MoreIcon more_icon;
	private CommandIcon use_medikit_self_icon;
	private CommandIcon use_medikit_other_icon;
	private CommandIcon heal_icon;
	private CommandIcon open_door_icon, close_door_icon;
	private CommandIcon next_unit_icon, prev_unit_icon;
	private CommandIcon warning_icon;
	private CommandIcon split_icon, absorb_icon, explode_blob_icon;
	private CommandIcon test_icon;
	private CommandIcon equipment_menu_icon;
	private CommandIcon use_scanner_icon;
	//private CommandIcon teleport_icon;//, create_absorb_icon_;
	private CommandIcon undo_deploy_icon;
	private CommandIcon build_structure, dismantle_structure, lay_egg;

	private GameModule game;

	public IconNode(GameModule _game) {
		super("IconNode", Statics.ICONS_WIDTH, Statics.ICONS_HEIGHT, 5, 2);

		AbstractActivity act = Statics.act;
		
		game = _game;

		String unit = "Unit";

		// Create the icons
		cancel_icon = new CommandIcon(game, R.drawable.menu_frame_yellow, act.getString(R.string.back), CommandIcon.CANCEL);
		scanner_icon = new CommandIcon(game, R.drawable.menu_frame_yellow, act.getString(R.string.scanner), CommandIcon.SCANNER);
		//better_scanner_icon = new CommandIcon(game, act.getString(R.string.better_scanner), CommandIcon.BETTER_SCANNER);
		//show_log_icon = new CommandIcon(game, act.getString(R.string.game_log), CommandIcon.SHOW_LOG);
		//grid_icon = new CommandIcon(game, act.getString(R.string.grid), CommandIcon.SHOW_GRID);
		//start_tutorial_icon = new CommandIcon(game, R.drawable.menu_frame_green, act.getString(R.string.start_tutorial), CommandIcon.START_TUTORIAL);
		next_tutorial_icon = new CommandIcon(game, R.drawable.menu_frame_green, act.getString(R.string.next_tutorial), CommandIcon.NEXT_TUTORIAL);
		//help_mode_icon = new CommandIcon(game, R.drawable.menu_frame_green, act.getString(R.string.help_mode), CommandIcon.HELP_MODE);
		pickup_item_icon = new CommandIcon(game, act.getString(R.string.pickup) + " (" + GameModule.APS_PICKUP_ITEM + ")", CommandIcon.PICKUP_ITEM);
		change_item_icon = new CommandIcon(game, act.getString(R.string.change_item) + " (" + GameModule.APS_CHANGE_ITEM + ")", CommandIcon.CHANGE_ITEM);
		equip_unit_icon = new CommandIcon(game, act.getString(R.string.equip_item) + " (" + GameModule.APS_CHANGE_ITEM + ")", CommandIcon.CHANGE_ITEM);
		drop_item_icon = new CommandIcon(game, act.getString(R.string.drop_item) + " (" + GameModule.APS_DROP_ITEM + ")", CommandIcon.DROP_ITEM);
		remove_item_icon = new CommandIcon(game, act.getString(R.string.remove_item) + " (" + GameModule.APS_REMOVE_ITEM + ")", CommandIcon.REMOVE_ITEM);
		select_shot_type_icon = new CommandIcon(game, act.getString(R.string.shoot), CommandIcon.SELECT_SHOT_TYPE);
		show_prime_menu_icon = new CommandIcon(game, act.getString(R.string.prime) + " (" + GameModule.APS_PRIME + ")", CommandIcon.SHOW_PRIME_MENU);
		activate_menu_icon = new CommandIcon(game, act.getString(R.string.activate) + " (" + GameModule.APS_ACTIVATE + ")", CommandIcon.ACTIVATE);
		do_prime_icon = new PrimeGrenadeIcon(game);
		//end_turn_icon = new CommandIcon(game, R.drawable.menu_frame_red, act.getString(R.string.end_turn), CommandIcon.END_TURN);
		//confirm_end_turn_icon = new CommandIcon(game, R.drawable.menu_frame_red, act.getString(R.string.end_turn_now), CommandIcon.CONFIRM_END_TURN);
		aimed_shot_icon = new ShotTypeIcon(game, act.getString(R.string.aimed), CommandIcon.AIMED_SHOT);
		snap_shot_icon = new ShotTypeIcon(game, act.getString(R.string.snap), CommandIcon.SNAP_SHOT);
		auto_shot_icon = new ShotTypeIcon(game, act.getString(R.string.auto), CommandIcon.AUTO_SHOT);
		throw_menu_icon = new CommandIcon(game, R.drawable.menu_frame_blue, "Aim\nThrow" + " (" + GameModule.APS_THROW + ")", CommandIcon.SHOW_THROW_MENU);
		escape_icon = new CommandIcon(game, R.drawable.menu_frame_red, act.getString(R.string.escape), CommandIcon.ESCAPE);
		throw_icon = new ThrowIcon(game);
		less_icon = new LessIcon(game);
		more_icon = new MoreIcon(game);
		use_medikit_self_icon = new CommandIcon(game, "Use Medikit\nOn Self" + " (" + GameModule.APS_USE_MEDIKIT + ")", CommandIcon.USE_MEDI_KIT_SELF);
		use_medikit_other_icon = new CommandIcon(game, "Use Medikit\nOn Other" + " (" + GameModule.APS_USE_MEDIKIT + ")", CommandIcon.USE_MEDI_KIT_OTHER);
		heal_icon = new CommandIcon(game, "Heal" + " (" + GameModule.APS_USE_MEDIKIT + ")", CommandIcon.HEAL);
		//use_porta_porter = new CommandIcon(game, StartupNew.strings.getTranslation("Use") + " PP Trigger (" + GameModule.APS_USE_EQUIPMENT + ")", CommandIcon.USE_PORTA_PORTER);
		//use_adrenalin_shot = new CommandIcon(game, StartupNew.strings.getTranslation("Use Adrenalin Shot") + " (" + GameModule.APS_USE_EQUIPMENT + ")", CommandIcon.USE_ADRENALIN_SHOT);
		open_door_icon = new CommandIcon(game, act.getString(R.string.open_door) + " (" + GameModule.APS_OPEN_DOOR + ")", CommandIcon.OPEN_DOOR);
		close_door_icon = new CommandIcon(game, act.getString(R.string.close_door) + " (" + GameModule.APS_OPEN_DOOR + ")", CommandIcon.CLOSE_DOOR);
		next_unit_icon = new CommandIcon(game, R.drawable.menu_frame_green, "Next\n" + unit, CommandIcon.NEXT_UNIT);
		prev_unit_icon = new CommandIcon(game, R.drawable.menu_frame_green, "Prev\n" + unit, CommandIcon.PREV_UNIT);
		warning_icon = new CommandIcon(game, R.drawable.menu_frame_red, act.getString(R.string.warning_see_console), CommandIcon.WARNING);
		split_icon = new CommandIcon(game, act.getString(R.string.split) + " (" + GameModule.APS_SPLIT + ")", CommandIcon.SPLIT);
		absorb_icon = new CommandIcon(game, act.getString(R.string.absorb) + " (" + GameModule.APS_ABSORB + ")", CommandIcon.ABSORB);
		explode_blob_icon = new CommandIcon(game, R.drawable.menu_frame_yellow, act.getString(R.string.explode) + " (" + GameModule.APS_EXPLODE + ")", CommandIcon.EXPLODE);
		test_icon = new CommandIcon(game, R.drawable.menu_frame_red, "TEST", CommandIcon.TEST);
		equipment_menu_icon = new CommandIcon(game, act.getString(R.string.equipment_menu), CommandIcon.EQUIPMENT_MENU);
		//misc_menu_icon = new CommandIcon(game, R.drawable.menu_frame_green, act.getString(R.string.misc), CommandIcon.MISC_MENU);
		use_scanner_icon = new CommandIcon(game, act.getString(R.string.use_scanner) + " (" + GameModule.APS_USE_SCANNER + ")", CommandIcon.USE_SCANNER);
		//teleport_menu_icon = new CommandIcon(game, R.drawable.menu_frame_blue, "Teleport\nMenu", CommandIcon.SHOW_TELEPORT_MENU);
		//teleport_icon = new CommandIcon(game, R.drawable.menu_frame_yellow, "Teleport\nNow", CommandIcon.TELEPORT);
		//create_absorb_icon = new CommandIcon(game, R.drawable.menu_frame_yellow, "Create or\nAbsorb", CommandIcon.CREATE_OR_ABSORB);
		undo_deploy_icon = new CommandIcon(game, R.drawable.menu_frame_red, act.getString(R.string.undo_deploy), CommandIcon.UNDO_DEPLOY);
		build_structure = new CommandIcon(game, R.drawable.menu_frame_yellow, "Build\nStructure", CommandIcon.BUILD_STRUCTURE);
		dismantle_structure = new CommandIcon(game, R.drawable.menu_frame_yellow, "Dismantle\nStructure (" + GameModule.APS_DISMANTLE + ")", CommandIcon.DISMANTLE_STRUCTURE);
		lay_egg = new CommandIcon(game, R.drawable.menu_frame_yellow, "Lay\nEgg", CommandIcon.LAY_EGG);

	}


	public void updateMenu(byte menu) {
		AbstractActivity act = Statics.act;
		
		this.menu_mode = menu;
		removeAllChildren();

		//if (this.game_data.ai_for_side[this.game_data.our_side] <= 0) {
		if (this.menu_mode != GameModule.MM_TURN_FINISHED_NO_MENU) {
			//if (this.menu_mode == GameModule.MM_DEPLOYING_UNITS) {
			if (game.game_data.game_status == GamesTable.GS_DEPLOYMENT) {
				// Do nothing
			} else {
				//if (this.our_unit_icons.size() > 1) { Always show as otherwise we have no way of selecting them
				this.add(this.next_unit_icon);
				this.add(this.prev_unit_icon);
				//}
			}
			if (this.menu_mode == GameModule.MM_NO_UNIT_SELECTED) {
				/*if (game.game_data.game_status != GamesTable.GS_DEPLOYMENT) {
					if (game.game_data.turn_side == game.game_data.our_side) { // Just in case it's not!
						//this.addAll(this.our_unit_icons); NO AS THERE'S SOMETIMES TOO MANY!
						this.add(this.end_turn_icon);
					}
				}*/
			} else if (this.menu_mode == GameModule.MM_UNIT_SELECTED) {
				if (game.current_unit != null) {
					if (game.current_unit.can_use_equipment) { 
						this.add(equipment_menu_icon);
						AppletMapSquare sq = game.current_unit.model.getSquareInFrontOfUnit_MaybeNULL();
						if (sq != null) { // Off the edge of the map!
							if (sq.door_type > 0) {
								if (sq.door_open) {
									this.add(close_door_icon);
								} else {
									this.add(open_door_icon);
								}
							}
						}

						ArrayList<EquipmentData> al = game.mapdata.getSq_MaybeNULL(game.current_unit.getMapX(), game.current_unit.getMapY()).getEquipment_MaybeNULL();
						if (al != null) {
							if (al.size() > 0) {
								if (game.current_unit.current_item == null) {
									pickup_item_icon.setText(act.getString(R.string.pickup) + " (" + GameModule.APS_PICKUP_ITEM + ")");
								} else {
									pickup_item_icon.setText(act.getString(R.string.pickup) + " (" + (GameModule.APS_PICKUP_ITEM*2) + ")");
								}
								this.add(pickup_item_icon);
							}
						}
						EquipmentData eq = game.current_unit.current_item;
						if (eq != null) {
							if (eq.major_type == EquipmentTypesTable.ET_GUN) {
								if (eq.getAmmo() > 0) {
									this.add(select_shot_type_icon);
								}
								if (eq.getAmmo() < eq.ammo_capacity) {
									reload_icon  = new CommandIcon(game, act.getString(R.string.reload)+ " (" + eq.reload_cost + ")", CommandIcon.RELOAD);
									this.add(reload_icon);
								}
							} else if (eq.major_type == EquipmentTypesTable.ET_GRENADE || eq.major_type == EquipmentTypesTable.ET_SMOKE_GRENADE || eq.major_type == EquipmentTypesTable.ET_NERVE_GAS || eq.major_type == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
								if (eq.primed == false) {
									this.add(this.show_prime_menu_icon);
								}
							} else if (eq.major_type == EquipmentTypesTable.ET_DEATH_GRENADE) {
								if (eq.primed == false) {
									this.add(this.activate_menu_icon);
								}
							} else if (eq.major_type == EquipmentTypesTable.ET_MEDIKIT) {
								this.add(this.use_medikit_self_icon);
								UnitData unit = game.current_unit.model.getUnitInFrontOfUnit_MaybeNULL();
								if (unit != null) {
									//if (unit.getSide() == game.current_unit.getSide()) {
										this.add(this.use_medikit_other_icon);
									//}
								}
							} else if (eq.major_type == EquipmentTypesTable.ET_SCANNER) {
								this.add(this.use_scanner_icon);
							}
						}
						if (game.current_unit.skillid == UnitsTable.SK_MEDIC) {
							UnitData unit = game.current_unit.model.getUnitInFrontOfUnit_MaybeNULL();
							if (unit != null) {
								//if (unit.getSide() == game.current_unit.getSide()) {
									this.add(this.heal_icon);
								//}
							}
						}
					}
					if (game.current_unit.model_type == UnitsTable.MT_BLOB) {
						this.add(this.absorb_icon);
						this.add(this.split_icon);
						this.add(this.explode_blob_icon);
					}
					AppletMapSquare sq = game.current_unit.model.getSquare_MaybeNULL();
					if (sq.escape_hatch_side == game.current_unit.getSide() || sq.escape_hatch_side > 4) {
						this.add(escape_icon);
					}
						//if (game.game_data.mission_type == 89 || game.game_data.mission_type == 93) {
						if (game.game_data.can_build_and_dismantle == 1) {
							if (game.current_unit.unit_type == UnitsTable.UT_ENGINEER) {
								this.add(this.build_structure);
							} else if (game.current_unit.model_type == UnitsTable.MT_QUEEN_ALIEN) {
								this.add(this.lay_egg);
								this.add(this.absorb_icon);
							} else if (game.current_unit.model_type == UnitsTable.MT_ALIEN_TYRANT) {
								this.add(this.absorb_icon);
							}
							this.add(this.dismantle_structure);
						}
				} else {
					this.updateMenu(GameModule.MM_NO_UNIT_SELECTED);
					return;
				}
			} else if (this.menu_mode == GameModule.MM_EQUIPMENT_MENU) {
				if (game.current_unit != null) {
					if (game.current_unit.can_use_equipment) { 
						EquipmentData eq = game.current_unit.current_item;
						if (game.current_unit.items.size() > 0) {
							if (eq == null) {
								this.add(equip_unit_icon);
							} else {
								if (game.current_unit.items.size() > 1) {
									this.add(change_item_icon);
								}
							}
						}
						if (eq != null) {
							this.add(this.throw_menu_icon); // Move so this is "after" equipping a unit
							this.add(remove_item_icon);
							this.add(drop_item_icon);
						}
					}
				}
			} else if (this.menu_mode == GameModule.MM_CHANGE_ITEM) {
				EquipmentData equip;
				for (int i=0 ; i<game.equipment.length ; i++) {
					equip = game.equipment[i];
					if (equip.getUnitID() == game.current_unit.unitid && equip.destroyed == false) {
						this.add(new EquipmentIcon(game, equip, EquipmentIcon.CHANGE));
					}
				}
			} else if (this.menu_mode == GameModule.MM_PICKUP_ITEM) {
				EquipmentData equip;
				ArrayList<EquipmentData> al = game.mapdata.getSq_MaybeNULL(game.current_unit.getMapX(), game.current_unit.getMapY()).getEquipment_MaybeNULL();
				if (al != null) {
					for (int i=0 ; i<al.size() ; i++) {
						equip = al.get(i);
						if (equip.destroyed == false) {
							this.add(new EquipmentIcon(game, equip, EquipmentIcon.PICKUP));
						}
					}
				}
			} else if (this.menu_mode == GameModule.MM_SHOOTING) {
				aimed_shot_icon.setPcentAndAPCost(GameModule.getAimedShotAccuracy(game.current_unit), game.current_unit.current_item.aimed_shot_aps);
				snap_shot_icon.setPcentAndAPCost(GameModule.getSnapShotAccuracy(game.current_unit), game.current_unit.current_item.snap_shot_aps);
				auto_shot_icon.setPcentAndAPCost(GameModule.getAutoShotAccuracy(game.current_unit), game.current_unit.current_item.auto_shot_aps);
				this.add(aimed_shot_icon);
				if (game.current_unit.current_item.snap_shot_acc > 0) {
					this.add(snap_shot_icon);
				}
				if (game.current_unit.current_item.auto_shot_acc > 0) {
					this.add(auto_shot_icon);
				}
			} else if (this.menu_mode == GameModule.MM_THROW) {
				this.add(this.throw_icon);
			/*} else if (this.menu_mode == GameModule.MM_TELEPORT) {
				this.add(this.teleport_icon);*/
			} else if (this.menu_mode == GameModule.MM_PRIME_GRENADE) {
				this.add(this.less_icon);
				this.add(this.more_icon);
				if (game.current_unit.getAPs() < (GameModule.APS_PRIME + GameModule.APS_THROW)) {
					this.add(this.warning_icon);
					game.addToHUD(act.getString(R.string.warn_no_aps_to_throw), true);
				}
				this.add(this.do_prime_icon);
				/*} else if (this.menu_mode == GameModule.MM_CONFIRM_END_TURN) {
				if (game.isAnyFriendlyUnitHoldingPrimedGrenade()) {
					this.add(this.warning_icon);
					game.addToHUD(act.getString(R.string.warn_holding_primed_grenade), true);
				}
				this.add(this.confirm_end_turn_icon);*/
			} else {
				throw new RuntimeException("Unknown menu mode: " + this.menu_mode);
			}

			if (this.menu_mode != GameModule.MM_NO_UNIT_SELECTED) {// && this.menu_mode != GameModule.MM_DEPLOYING_UNITS) {
				this.add(cancel_icon);
			}
			this.add(this.scanner_icon);
			/*if (this.menu_mode != GameModule.MM_MISC_MENU) {
				this.add(misc_menu_icon);
			}*/
			if (game.show_tutorial) {
				this.add(next_tutorial_icon);
			}
			if (game.next_to_deploy > 0) {
				this.add(this.undo_deploy_icon);
			}

			/*if (Statics.DEBUG) {
				this.add(test_icon);
			}*/

			// Add any visible enemies
			this.addAll(this.visible_enemy_icons);
		}
		//}

		this.updateGeometricState();

	}


	private void add(AbstractIcon icon) {
		if (icon != null) {
			this.attachChild(icon);
		} else {
			throw new RuntimeException("Null icon!");
		}

	}


	private void addAll(ArrayList<AbstractIcon> icons) {
		for (AbstractIcon icon : icons) {
			this.add(icon);
		}


	}


}
