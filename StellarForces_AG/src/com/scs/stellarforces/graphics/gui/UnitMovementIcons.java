package com.scs.stellarforces.graphics.gui;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.lib2d.layouts.GridLayout;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.graphics.icons.MovementIcon;
import com.scs.stellarforces.main.lite.R;

public class UnitMovementIcons extends GridLayout {

	public UnitMovementIcons(GameModule game) {
		super("Movement_Icons", Statics.ICONS_WIDTH, Statics.ICONS_HEIGHT, 5);

		AbstractActivity act = Statics.act;
		
		this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_LEFT, R.drawable.curved_arrow_left), 0, 0);
		this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_FWD, R.drawable.arrow_up), 1, 0);
		this.attachChild(new MovementIcon(game, "", MovementIcon.TURN_RIGHT, R.drawable.curved_arrow_right), 2, 0);
		this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_LEFT, R.drawable.arrow_left), 0, 1);
		this.attachChild(new MovementIcon(game, act.getString(R.string.centre), MovementIcon.CENTRE_ON_UNIT, R.drawable.menu_frame_green), 1, 1);
		this.attachChild(new MovementIcon(game, "", MovementIcon.SLIDE_RIGHT, R.drawable.arrow_right), 2, 1);
		this.attachChild(new MovementIcon(game, "AP Lock", MovementIcon.AP_LOCK), 0, 2);
		this.attachChild(new MovementIcon(game, "", MovementIcon.MOVE_BACK, R.drawable.arrow_down), 1, 2);
		//this.attachChild(new MovementIcon(game, "End\nTurn", MovementIcon.END_TURN, R.drawable.menu_frame_red), 2, 2);
	}

}
