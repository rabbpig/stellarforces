package com.scs.stellarforces.graphics.gui;

import ssmith.android.lib2d.gui.MultiLineLabel;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Typeface;

import com.scs.stellarforces.Statics;

public class LogWindow { //extends MultiLineLabel {

	//private static final int MAX_LINES = 5;

	private static Paint paint_normal_text = new Paint();
	public static Paint back_col = new Paint();

	private float max_height;
	private StringBuffer text_to_add = new StringBuffer();
	public MultiLineLabel mll;

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.04f));

		back_col.setARGB(120, 0, 80, 0);
		back_col.setStyle(Style.FILL);
	}


	public LogWindow(float w, Typeface tf) { //float x, float y, float w, float h) {
		super();

		paint_normal_text.setTypeface(tf);

		//float w = Statics.SCREEN_WIDTH/2 - movement.getWidth() - Statics.SCREEN_WIDTH * 0.01f;
		mll = new MultiLineLabel("LogWindow", "", back_col, paint_normal_text, true, w);

		max_height = paint_normal_text.getTextSize() * 8;// Statics.SCREEN_HEIGHT * 0.4f;
		//max_height = h;

		mll.setLocation(Statics.SCREEN_WIDTH * 0.002f, Statics.SCREEN_HEIGHT - max_height);
	}


	public void appendText(String s) {
		synchronized (text_to_add) {
			text_to_add.append(s + "\n");
		}
		this.process();
		/*
		int lines = text.toString().split("\n").length;
		while (lines > MAX_LINES) {
			int pos = text.indexOf("\n");
			text.delete(0, pos+1);
			mll.str.delete(0, pos+1); // Delete the same from actual text
			lines = text.toString().split("\n").length;
		}
		/*super.appendText(s + "\n");
		while (this.local_bounds.height() > max_height) {
			int pos = str.indexOf("\n");
			if (pos >= 0) {
				super.str.delete(0,  pos+1);
				super.showText(); // to recalc size
			} else {
				break;
			}
		}

		super.showText();*/

	}


	public void process() {
		// Is there any text to add?
		synchronized (text_to_add) {
			if (this.text_to_add.length() > 0) {
				int to_add = 1;
				if (this.text_to_add.length() > 2) {
					if (this.text_to_add.toString().toLowerCase().indexOf("please wait") >= 0) {
						// Show "please wait" immed!
						to_add = this.text_to_add.length();
					} else {
						to_add = 3;
					}
				}
				mll.appendText(this.text_to_add.substring(0, to_add));
				this.text_to_add.delete(0, to_add);

				// Trim if too long
				//int lines = mll.getText().split("\n").length;
				//while (lines > MAX_LINES) {
				while (mll.getHeight() > max_height) {
					int pos = mll.getText().indexOf("\n");
					mll.delete(0, pos+1); // Delete the same from actual text
					//lines = mll.getText().toString().split("\n").length;
				}
				/*while (mll.local_bounds.height() > max_height) {
				int pos = str.indexOf("\n");
				if (pos >= 0) {
					mll.str.delete(0,  pos+1);
					mll.showText(); // to recalc size
				} else {
					break;
				}
			}*/
				//mll.showText();
			}
		}
	}


	public void clearText() {
		synchronized (text_to_add) {
			this.text_to_add.setLength(0);// .delete(0, this.text_to_add.length());
		}
		mll.setText("");
	}


}
