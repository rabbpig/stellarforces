package com.scs.stellarforces.graphics.gui;

import ssmith.android.lib2d.Node;
import android.graphics.Typeface;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.game.IProcessable;

public class HUD extends Node implements IProcessable {

	private StatusBarNode stat_node;
	private LogWindow log;
	private IconNode icons;
	private UnitMovementIcons movement;
	public NewMovementIcons new_movement;

	public HUD(GameModule game, IconNode _icons, Typeface tf) {
		super("HUD");

		icons = _icons;
		this.attachChild(icons);

		stat_node = new StatusBarNode(game, tf); 
		this.attachChild(stat_node);

		movement = new UnitMovementIcons(game);
		movement.updateGeometricState();
		movement.setLocation(Statics.SCREEN_WIDTH - movement.getWidth(), Statics.SCREEN_HEIGHT - movement.getHeight());
		this.attachChild(movement);

		if (Statics.USE_NEW_MOVEMENT_ICONS == 1) {
			movement.visible = false;
			movement.collides = false;
			
			new_movement = new NewMovementIcons(game);
			new_movement.setLocation((Statics.SCREEN_WIDTH - Statics.ICONS_WIDTH*3)/2, (Statics.SCREEN_HEIGHT - Statics.ICONS_WIDTH*3)/2);
			this.attachChild(new_movement);
		}

		float w = Statics.SCREEN_WIDTH - 0 - (Statics.SCREEN_WIDTH * 0.01f);
		if (Statics.USE_NEW_MOVEMENT_ICONS == 0) {
			w = w - movement.getWidth();
		}
		log = new LogWindow(w, tf); // was "- movement.getWidth()"
		this.attachChild(log.mll);
	}


	public void clearText() {
		log.clearText();
	}


	public void appendText(String s) {
		log.appendText(s);
	}


	public void updateStatusBar() {
		this.stat_node.updateStatus();
	}


	public void setMovementIconsVisible(boolean b) {
		if (Statics.USE_NEW_MOVEMENT_ICONS == 0) {
			movement.visible = b;
			movement.collides = b;
		} else {
			new_movement.visible = b;
			new_movement.collides = b;
		}
	}


	public byte getMenuMode() {
		return icons.menu_mode;
	}


	@Override
	public void process(long interpol) {
		log.process();
	}


}
