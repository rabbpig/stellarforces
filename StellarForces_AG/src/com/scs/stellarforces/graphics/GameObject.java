package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.MyPickResults;
import ssmith.android.lib2d.MyPointF;
import ssmith.android.lib2d.Ray;
import ssmith.android.lib2d.shapes.AbstractRectangle;
import ssmith.lang.GeometryFuncs;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.graphics.units.AbstractUnit;
import com.scs.stellarforces.graphics.units.Alien;
import com.scs.stellarforces.graphics.units.AlienQueen;
import com.scs.stellarforces.graphics.units.Blob;

public abstract class GameObject extends AbstractRectangle {

	protected GameModule game;
	public boolean can_be_shot, blocks_view;

	public GameObject(GameModule _game, String name, boolean _collides, float pxl_x, float pxl_y, float w, float h, boolean _can_be_shot, boolean _blocks_view) {
		super(name, null, pxl_x, pxl_y, w, h);

		game = _game;
		collides = _collides;
		can_be_shot = _can_be_shot;
		blocks_view = _blocks_view;

	}


	public float getMapDistTo(GameObject go) {
		return getMapDistTo((int)(go.getWorldX()/Statics.SQ_SIZE), (int)(go.getWorldY()/Statics.SQ_SIZE)); // map_x, map_y
	}


	public float getMapDistTo(int map_x, int map_y) {
		//return (float)GeometryFuncs.distance((int)(this.getWorldX()/Statics.SQ_SIZE), (int)(this.getWorldY()/Statics.SQ_SIZE), map_x, map_y);
		return (float)(GeometryFuncs.distance(this.getWorldX(), this.getWorldY(), map_x*Statics.SQ_SIZE, map_y*Statics.SQ_SIZE)/Statics.SQ_SIZE);
	}


	public boolean canSee(GameObject other, int ang_or_minus_one, boolean do_units_block) {
		return canSee(other, ang_or_minus_one, do_units_block, 0, false, false);
	}


	public boolean canSee(GameObject other, int ang_or_minus_one, boolean do_units_block, int max_block_count) {
		return canSee(other, ang_or_minus_one, do_units_block, max_block_count, false, false);
	}


	public boolean canSee(GameObject other, int ang_or_minus_one, boolean do_units_block, int max_block_count, boolean ignore_smoke, boolean ignore_cpu) {
		return getCanSeeCount(other, ang_or_minus_one, do_units_block, 0, ignore_smoke, ignore_cpu) == 0;
	}


	/**
	 * This returns the number of objects between the viewer and viewee.
	 * Returns -1 if the viewer CANNOT see the viewee (i.e. because not facing it) - NO IT DOESNT
	 * Returns 999 if there is nothing blocking it.
	 * 
	 * @param other
	 * @param ang_or_minus_one
	 * @param do_units_block
	 * @param ignore_classes
	 * @param block_count
	 * @return
	 */
	public int getCanSeeCount(GameObject other, int ang_or_minus_one, boolean do_units_block, int max_block_count, boolean ignore_smoke, boolean ignore_cpu) {
		int CANNOT_SEE = 999;
		int CAN_SEE = 0;

		float dist = this.getDistanceTo(other);
		
		// If they on the same square (i.e. just dropped an item) then they can see it.
		// Notice we DONT use mapsquares as it's too inaccurate.
		if (dist <= Statics.SQ_SIZE * 0.6f) { 
			return CAN_SEE;
		}

		if (ang_or_minus_one >= 0 && this instanceof Blob == false) { // Blobs can see 360, aliens 180
			int ang_to_target = this.getAbsoluteAngleTo(other.getWorldCentreX(), other.getWorldCentreY());
			float diff = GeometryFuncs.GetDiffBetweenAngles(ang_or_minus_one, ang_to_target);
			int field_of_vision = Statics.VIEW_ANGLE; 
			if (this instanceof Alien || this instanceof AlienQueen) {
				field_of_vision = field_of_vision * 2;
			}
			if (diff > field_of_vision) {
				return CANNOT_SEE;
			}
		}

		// If they are within one square, just show them (there are slight problems with corners of walls)
		// Note that we do this AFTER checking the angle, otherwise unit's won't opp-fire if stood next to a friendly unit (that they can't see)
		// Notice we DONT use mapsquares as it's too innaccurate
		if (dist <= Statics.SQ_SIZE * 1.6f) { // SCS 12/6/13 was 1.5f 
			return CAN_SEE;
		}

		int block_count = 0;

		MyPointF dir = other.getWorldCentre_CreatesNew().subtract(this.getWorldCentre_CreatesNew());
		dir.normalizeLocal();
		Ray r = new Ray(this.getWorldCentre_CreatesNew().multiply(1), dir);
		MyPickResults can_see_results = new MyPickResults(this, other);
		game.root_node.findPick(r, can_see_results);

		//Line line = new Line("LOS", this, other, null);
		//ArrayList<Geometry> colls = line.getColliders(game.root_node);
		//addCollidersToPickResult(can_see_results, this, colls);
		//can_see_results.add(colls);

		// Check for walls
		game.mapdata.canSee(r, can_see_results, ignore_smoke, ignore_cpu);

		can_see_results.sort();

		if (can_see_results.getNumber() > 0) {
			for (int i=0 ; i<can_see_results.getNumber() ; i++) {
				GameObject obj = can_see_results.getGameObject(i);

				if (obj == other) {
					return block_count;
				} else if (obj.blocks_view) {
					if (obj instanceof AbstractUnit && do_units_block == false) {
						continue;
					}
					max_block_count--;
					block_count++;
					if (max_block_count < 0) {
						return block_count;
					}
				}
			}
		}
		return block_count;//CANNOT_SEE;
	}


	/**
	 * This checks if the object has been hit by an explosion.
	 * @param mapx
	 * @param mapy
	 * @return
	 */
	public int getExplosionWallProtectionCount(int s_mapx, int s_mapy) {
		float dist = getMapDistTo(s_mapx, s_mapy);
		if (dist <= 1.5f) {
			return 0;
		}
		int ex = (int)(this.getWorldCentre_CreatesNew().x/Statics.SQ_SIZE);
		int ey = (int)(this.getWorldCentre_CreatesNew().y/Statics.SQ_SIZE);
		return game.mapdata.getBlockCountInLOS_Pixels_Centre(s_mapx, s_mapy, ex, ey, true, true);
	}
	

	public float getDistanceTo(GameObject other) {
		return GeometryFuncs.distance(this.getWorldCentreX(), this.getWorldCentreY(), other.getWorldCentreX(), other.getWorldCentreY());
	}


	public int getAbsoluteAngleTo(float x, float y) {
		int x2 = (int)(x - this.getWorldCentreX());
		int y2 = (int)(y - this.getWorldCentreY());
		return (int) Math.toDegrees(Math.atan2(y2, x2)); // Math.toDegrees(Math.atan2(10, 10))
	}


	public boolean isChanceofNotHitting() {
		return this.getChanceofNotHitting() > 0;
	}


	/**
	 * Override if req
	 * @return
	 */
	public int getChanceofNotHitting() {
		return 0;
	}


}
