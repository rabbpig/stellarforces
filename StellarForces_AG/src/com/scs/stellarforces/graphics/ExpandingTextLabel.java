package com.scs.stellarforces.graphics;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.graphics.units.AbstractUnit;

import ssmith.android.lib2d.Camera;
import ssmith.android.lib2d.gui.Label;
import android.graphics.Canvas;
import android.graphics.Paint;

public class ExpandingTextLabel extends Label {
	
	private static final float MAX_SIZE = Statics.SQ_SIZE*2;
	
	public ExpandingTextLabel(AbstractUnit unit, String _text) {
		super("ExpandingTextLabel", _text, null, new Paint());
		
		this.ink.setARGB(255, 255, 255, 0);
		this.ink.setTextSize(1);
		this.ink.setAntiAlias(true);

		this.setLocation(unit.getLocalCentreX(), unit.getLocalCentreY());
	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		this.ink.setTextSize(this.ink.getTextSize()+2);
		super.doDraw(g, cam, interpol);
		if (this.ink.getTextSize() >= MAX_SIZE) {
			this.removeFromParent();
		}
	}
	

}
