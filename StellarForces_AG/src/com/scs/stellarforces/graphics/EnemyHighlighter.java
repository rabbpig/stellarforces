package com.scs.stellarforces.graphics;

import android.graphics.Color;

public class EnemyHighlighter extends AbstractUnitHighlighter {
	
	public EnemyHighlighter() { //float cx, float cy) {
		super(Color.argb(248, 255, 0, 0), Color.argb(235, 255, 138, 155));
	}

}
