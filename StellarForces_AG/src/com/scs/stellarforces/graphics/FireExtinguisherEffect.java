package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.MyPointF;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

import dsr.data.AppletMapSquare;
import dsrwebserver.tables.EquipmentTypesTable;

public class FireExtinguisherEffect extends AbstractAreaEffectGun {
	
	public FireExtinguisherEffect(GameModule _game, MyPointF pxl_start, MyPointF _pixel_end) {
		super(_game, "FireExtinguisherEffect", R.drawable.fire_ext_smoke, pxl_start, _pixel_end);
	}

	
	@Override
	protected void checkSquares() {
		int x = (int)(this.getWorldCentreX()/Statics.SQ_SIZE_INT);
		int y = (int)(this.getWorldCentreY()/Statics.SQ_SIZE_INT);
		AppletMapSquare sq = game.mapdata.getSq_MaybeNULL(x, y);
		if (sq != null) {
			if (sq.smoke_type == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
				sq.smoke_type = 0;
				game.updateMapOnServer(sq, -1);
			}
		}		
	}

}
