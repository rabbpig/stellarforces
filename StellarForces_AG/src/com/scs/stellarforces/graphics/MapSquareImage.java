package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import ssmith.android.lib2d.gui.BitmapComponent;
import ssmith.android.media.ImageFunctions;
import ssmith.android.util.ImageCache;
import ssmith.lang.NumberFunctions;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

import dsr.data.AppletMapSquare;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.MapDataTable;

public class MapSquareImage extends BitmapComponent {

	private static final float CPU_INSET = Statics.SQ_SIZE * 0.1f;

	private static Bitmap bmp_door_lr_closed, bmp_door_ud_closed, bmp_door_lr_open, bmp_door_ud_open;
	private static Bitmap bmp_cpu1, bmp_cpu2, bmp_cpu3, bmp_cpu4, bmp_cpu_destroyed;
	private static Bitmap bmp_smoke1, bmp_smoke2, bmp_smoke3, bmp_smoke4;
	private static Bitmap bmp_fire1, bmp_fire2, bmp_fire3, bmp_fire4;
	private static Bitmap bmp_nerve_gas1, bmp_nerve_gas2, bmp_nerve_gas3, bmp_nerve_gas4; 
	private static Bitmap bmp_grid;

	public byte x, y;
	private AppletMapSquare sq;
	private Bitmap bmp_smoke, bmp_nerve_gas, bmp_fire;
	public Bitmap bmp_shad;
	public float shad_offset_x, shad_offset_y;
	
	private static void CreateBitmaps(ImageCache img_cache) {
		if (bmp_door_lr_closed == null || bmp_door_lr_closed.isRecycled()) {
			bmp_door_lr_closed = img_cache.getImage(R.drawable.door_lr, Statics.SQ_SIZE_INT, Statics.SQ_SIZE_INT);
			bmp_door_ud_closed = img_cache.getImage(R.drawable.door_ud, Statics.SQ_SIZE_INT, Statics.SQ_SIZE_INT);
			bmp_door_lr_open = img_cache.getImage(R.drawable.door_lr_open, Statics.SQ_SIZE_INT, Statics.SQ_SIZE_INT);
			bmp_door_ud_open = img_cache.getImage(R.drawable.door_ud_open, Statics.SQ_SIZE_INT, Statics.SQ_SIZE_INT);
			bmp_nerve_gas1 = img_cache.getImage(R.drawable.nerve_gas, Statics.SQ_SIZE+(CPU_INSET*2), Statics.SQ_SIZE+(CPU_INSET*2));
			bmp_nerve_gas2 = ImageFunctions.Rotate(bmp_nerve_gas1, 90, false);
			bmp_nerve_gas3 = ImageFunctions.Rotate(bmp_nerve_gas2, 90, false);
			bmp_nerve_gas4 = ImageFunctions.Rotate(bmp_nerve_gas3, 90, false);
			bmp_smoke1 = img_cache.getImage(R.drawable.smoke, Statics.SQ_SIZE+(CPU_INSET*2), Statics.SQ_SIZE+(CPU_INSET*2));
			bmp_smoke2 = ImageFunctions.Rotate(bmp_smoke1, 90, false);
			bmp_smoke3 = ImageFunctions.Rotate(bmp_smoke2, 90, false);
			bmp_smoke4 = ImageFunctions.Rotate(bmp_smoke3, 90, false);
			bmp_fire1 = img_cache.getImage(R.drawable.fire, Statics.SQ_SIZE+(CPU_INSET*2), Statics.SQ_SIZE+(CPU_INSET*2));
			bmp_fire2 = ImageFunctions.Rotate(bmp_fire1, 90, false);
			bmp_fire3 = ImageFunctions.Rotate(bmp_fire2, 90, false);
			bmp_fire4 = ImageFunctions.Rotate(bmp_fire3, 90, false);
			bmp_cpu1 = img_cache.getImage(R.drawable.computerconsole1, Statics.SQ_SIZE-(CPU_INSET*2), Statics.SQ_SIZE+(CPU_INSET*2));
			bmp_cpu2 = img_cache.getImage(R.drawable.computerconsole2, Statics.SQ_SIZE-(CPU_INSET*2), Statics.SQ_SIZE+(CPU_INSET*2));
			bmp_cpu3 = img_cache.getImage(R.drawable.computerconsole3, Statics.SQ_SIZE-(CPU_INSET*2), Statics.SQ_SIZE+(CPU_INSET*2));
			bmp_cpu4 = img_cache.getImage(R.drawable.computerconsole4, Statics.SQ_SIZE-(CPU_INSET*2), Statics.SQ_SIZE+(CPU_INSET*2));
			bmp_cpu_destroyed = img_cache.getImage(R.drawable.damaged_computer, Statics.SQ_SIZE-(CPU_INSET*2), Statics.SQ_SIZE-(CPU_INSET*2));
			bmp_grid = img_cache.getImage(R.drawable.mapsquare_grid, Statics.SQ_SIZE_INT, Statics.SQ_SIZE_INT);
		}
	}


	public MapSquareImage(GameModule game, String name, AppletMapSquare _sq, Bitmap _bmp, byte map_x, byte map_y) {
		super(name, name, _bmp);

		sq = _sq;
		x = map_x;
		y = map_y;

		CreateBitmaps(game.img_cache);

		Bitmap bmp = null;
		switch (NumberFunctions.rnd(1, 4)) {
		case 1:
			bmp = bmp_nerve_gas1;
			break;
		case 2:
			bmp = bmp_nerve_gas2;
			break;
		case 3:
			bmp = bmp_nerve_gas3;
			break;
		default:
			bmp = bmp_nerve_gas4;
			break;
		}
		this.bmp_nerve_gas = bmp;

		bmp = null;
		switch (NumberFunctions.rnd(1, 4)) {
		case 1:
			bmp = bmp_smoke1;
			break;
		case 2:
			bmp = bmp_smoke2;
			break;
		case 3:
			bmp = bmp_smoke3;
			break;
		default:
			bmp = bmp_smoke4;
			break;
		}
		this.bmp_smoke = bmp;

		bmp = null;
		switch (NumberFunctions.rnd(1, 4)) {
		case 1:
			bmp = bmp_fire1;
			break;
		case 2:
			bmp = bmp_fire2;
			break;
		case 3:
			bmp = bmp_fire3;
			break;
		default:
			bmp = bmp_fire4;
			break;
		}
		this.bmp_fire = bmp;

	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		super.doDraw(g, cam, interpol);
		if (sq.door_type > 0) {
			if (sq.door_type == MapDataTable.DOOR_EW) {
				if (sq.door_open == false) {
					g.drawBitmap(bmp_door_lr_closed, this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);
				} else {
					g.drawBitmap(bmp_door_lr_open, this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);
				}
			} else if (sq.door_type == MapDataTable.DOOR_NS) {
				if (sq.door_open == false) {
					g.drawBitmap(bmp_door_ud_closed, this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);
				} else {
					g.drawBitmap(bmp_door_ud_open, this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);
				}

			}
		}
		if (sq.smoke_type == EquipmentTypesTable.ET_NERVE_GAS) {
			g.drawBitmap(bmp_nerve_gas, this.world_bounds.left - cam.left - CPU_INSET, this.world_bounds.top - cam.top - CPU_INSET, paint);
		} else if (sq.smoke_type == EquipmentTypesTable.ET_SMOKE_GRENADE) {
			g.drawBitmap(bmp_smoke, this.world_bounds.left - cam.left - CPU_INSET, this.world_bounds.top - cam.top - CPU_INSET, paint);
		} else if (sq.smoke_type == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
			g.drawBitmap(bmp_fire, this.world_bounds.left - cam.left - CPU_INSET, this.world_bounds.top - cam.top - CPU_INSET, paint);
		}
		if (this.sq.major_type == MapDataTable.MT_COMPUTER) {
			if (sq.destroyed == 0) {
				switch (sq.owner_side) {
				case 0:
				case 1:
					g.drawBitmap(bmp_cpu1, this.world_bounds.left - cam.left + CPU_INSET, this.world_bounds.top - cam.top - (CPU_INSET*2), paint);
					break;
				case 2:
					g.drawBitmap(bmp_cpu2, this.world_bounds.left - cam.left + CPU_INSET, this.world_bounds.top - cam.top - (CPU_INSET*2), paint);
					break;
				case 3:
					g.drawBitmap(bmp_cpu3, this.world_bounds.left - cam.left + CPU_INSET, this.world_bounds.top - cam.top - (CPU_INSET*2), paint);
					break;
				case 4:
					g.drawBitmap(bmp_cpu4, this.world_bounds.left - cam.left + CPU_INSET, this.world_bounds.top - cam.top - (CPU_INSET*2), paint);
					break;
				default:
					throw new RuntimeException("Unknown owner: " + sq.owner_side);
				}
			} else {
				g.drawBitmap(bmp_cpu_destroyed, this.world_bounds.left - cam.left + CPU_INSET, this.world_bounds.top - cam.top + CPU_INSET, paint);
			}
		}
		if (this.bmp_shad != null) {
			g.drawBitmap(bmp_shad, this.world_bounds.left - cam.left + this.shad_offset_x, this.world_bounds.top - cam.top + this.shad_offset_y, paint);
		}
		if (GameModule.show_grid) {
			g.drawBitmap(bmp_grid, this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);
		}
	}


}
