package com.scs.stellarforces.graphics.icons;

import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

public class PrimeGrenadeIcon extends AbstractIcon {
	
	private byte count = 2;

	public PrimeGrenadeIcon(GameModule _game) {
		super(_game, R.drawable.menu_frame_red, "Prime");
		
		this.adjCount((byte)0);
	}

	
	@Override
	public boolean mouseClicked() {
		if (game.question) {
			game.addToHUD("Prime the grenade for " + count + " player-turns");
		} else {
			game.primeGrenade(count);
		}
		return true;
	}

	
	public void adjCount(byte amt) {
		count += amt;
		if (count < 0) {
			count = 0;
		}
		this.setText("Prime for\n" + count + " turns");
		//main.refreshHUD();
	}

}
