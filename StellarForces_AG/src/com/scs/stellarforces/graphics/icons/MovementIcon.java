package com.scs.stellarforces.graphics.icons;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

public class MovementIcon extends AbstractIcon {

	public static final int MOVE_FWD = 1;
	public static final int TURN_RIGHT = 2;
	public static final int SLIDE_RIGHT = 3;
	public static final int MOVE_BACK = 4;
	public static final int SLIDE_LEFT = 5;
	public static final int TURN_LEFT = 6;
	
	public static final int CENTRE_ON_UNIT = 7;
	public static final int AP_LOCK = 9;
	//public static final int END_TURN = 10;
	public static final int QUESTION = 11;

	private int cmd;

	public MovementIcon(GameModule game, String text, int _cmd) {
		this(game, text, _cmd, R.drawable.menu_frame_yellow);
	}


	public MovementIcon(GameModule game, String text, int _cmd, int r) {
		super(game, r, text);

		cmd = _cmd;
	}


	@Override
	public boolean mouseClicked() {
		if (game.question && this.cmd != QUESTION) { // So they can turn it off!
			switch (cmd) {
			case MOVE_FWD:
				game.addToHUD("Moves a unit forward one square");
				break;
			case MOVE_BACK:
				game.addToHUD("Moves a unit backwards one square");
				break;
			case SLIDE_LEFT:
				game.addToHUD("Moves a unit left one square");
				break;
			case SLIDE_RIGHT:
				game.addToHUD("Moves a unit right one square");
				break;
			case TURN_LEFT:
				game.addToHUD("Turns a unit left by 45 degrees");
				break;
			case TURN_RIGHT:
				game.addToHUD("Turns a unit right by 45 degrees");
				break;
			case CENTRE_ON_UNIT:
				game.addToHUD("Centres the camera on the current unit");
				break;
			case AP_LOCK:
				game.addToHUD("Ensures a unit has enough APs for Opp Fire");
				break;
			case QUESTION:
				game.addToHUD("Gives help on each icon");
				break;
			default:
				game.addToHUD("There is no help on that icon");
				break;
			}
		} else {
			if (game.areExplosionsScheduled() == false) {
				if (game.current_unit != null) {
					if (game.current_unit.model != null) {
						switch (cmd) {
						case MOVE_FWD:
							game.current_unit.model.moveFwd();
							break;
						case MOVE_BACK:
							game.current_unit.model.moveBack();
							break;
						case SLIDE_LEFT:
							game.current_unit.model.moveStrafeLeft();
							break;
						case SLIDE_RIGHT:
							game.current_unit.model.moveStrafeRight();
							break;
						case TURN_LEFT:
							game.current_unit.model.turnBy(-45);
							break;
						case TURN_RIGHT:
							game.current_unit.model.turnBy(45);
							break;
						case CENTRE_ON_UNIT:
							game.selectOurUnit(game.current_unit, true);
							break;
						case AP_LOCK:
							game.ap_lock = !game.ap_lock;
							if (game.ap_lock) {
								this.bmp_background = game.img_cache.getImage(R.drawable.menu_frame_red, Statics.ICONS_WIDTH, Statics.ICONS_HEIGHT);
								game.addToHUD("AP Lock ON");
							} else {
								this.bmp_background = game.img_cache.getImage(R.drawable.menu_frame_yellow, Statics.ICONS_WIDTH, Statics.ICONS_HEIGHT);
								game.addToHUD("AP Lock off");
							}
							break;
						/*case END_TURN:
							game.askIfEndTurn();
							break;*/
						case QUESTION:
							game.question = !game.question;
							if (game.question) {
								this.bmp_background = game.img_cache.getImage(R.drawable.menu_frame_red, Statics.ICONS_WIDTH, Statics.ICONS_HEIGHT);
								game.addToHUD("Icon Help Mode: Please click on another icon for help.  Press this icon again to return to normal mode.");
							} else {
								this.bmp_background = game.img_cache.getImage(R.drawable.menu_frame_yellow, Statics.ICONS_WIDTH, Statics.ICONS_HEIGHT);
								game.addToHUD("Normal mode restored");
							}
							break;
						/*case SCANNER:
							game.toggleScanner();
							break;*/
						default:
							throw new RuntimeException("Unknown movement command: " + cmd);
						}
					}
				}
			}
			/*if (Statics.RELEASE_MODE == false) {
				throw new RuntimeException("Null unit!");
			}*/
		}
		return true;

	}
	
	
	/*public static int GetRForIcon(int icon) {
		switch (cmd) {
		case MOVE_FWD:
			return R.drawable
		case MOVE_BACK:
			game.addToHUD("Moves a unit backwards one square");
			break;
		case SLIDE_LEFT:
			game.addToHUD("Moves a unit left one square");
			break;
		case SLIDE_RIGHT:
			game.addToHUD("Moves a unit right one square");
			break;
		case TURN_LEFT:
			game.addToHUD("Turns a unit left by 45 degrees");
			break;
		case TURN_RIGHT:
			game.addToHUD("Turns a unit right by 45 degrees");
			break;
		case CENTRE_ON_UNIT:
			game.addToHUD("Centres the camera on the current unit");
			break;
		case AP_LOCK:
			game.addToHUD("Ensures a unit has enough APs for Opp Fire");
			break;
		case QUESTION:
			game.addToHUD("Gives help on each icon");
			break;
		default:
			game.addToHUD("There is no help on that icon");
			break;
		}
		
	}
*/
}
