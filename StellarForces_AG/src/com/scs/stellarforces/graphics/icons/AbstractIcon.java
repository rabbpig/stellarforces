package com.scs.stellarforces.graphics.icons;

import ssmith.android.lib2d.gui.FlashingButton;
import ssmith.android.lib2d.gui.GUIFunctions;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;


public abstract class AbstractIcon extends FlashingButton {
	
	protected GameModule game;
	
	public static Paint paint_white_text = new Paint();
	public static Paint paint_transp = new Paint();
	
	static {
		paint_white_text.setARGB(255, 255, 255, 255);
		paint_white_text.setAntiAlias(true);
		//paint_white_text.setStyle(Style.STROKE);
		paint_white_text.setTextSize(GUIFunctions.GetTextSizeToFit("WARNING! ", Statics.ICONS_WIDTH, -1));

		paint_transp.setARGB(100, 255, 255, 255);

		if (Statics.tf != null) {
			paint_white_text.setTypeface(Statics.tf);
		}
		
	}
	
	
	public AbstractIcon(GameModule _game, int r, String text) {
		super(text, paint_transp, paint_white_text, _game.img_cache.getImage(r, Statics.ICONS_WIDTH, Statics.ICONS_HEIGHT), null, 0);
		
		game = _game;
	}
	
	
	public abstract boolean mouseClicked();


	public void adjCount(byte amt) {
		// Override if req
	}
	
	
}
