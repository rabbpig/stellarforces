package com.scs.stellarforces.graphics.icons;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

public class MoreIcon extends AbstractIcon {
	
	public AbstractIcon action;
	
	public MoreIcon(GameModule game) {
		super(game, R.drawable.menu_frame_blue, Statics.act.getString(R.string.more));
	}


	@Override
	public boolean mouseClicked() {
		if (game.question) {
			game.addToHUD("Choose 1 more");
		} else {
			action.adjCount((byte)+1);
		}
		return true;
	}

}
