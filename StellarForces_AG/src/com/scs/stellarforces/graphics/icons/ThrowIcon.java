package com.scs.stellarforces.graphics.icons;

import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

public class ThrowIcon extends AbstractIcon {
	
	public ThrowIcon(GameModule game) {
		super(game, R.drawable.menu_frame_yellow, "Throw");
	}

	
	@Override
	public boolean mouseClicked() {
		if (game.question) {
			game.addToHUD("Throws the current item");
		} else {
			game.throwItem();
		}
		return true;
	}

	

}
