package com.scs.stellarforces.graphics.icons;

import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

import dsr.data.EquipmentData;

public class EquipmentIcon extends AbstractIcon {

	public static final int PICKUP = 1;
	public static final int CHANGE = 2;

	private EquipmentData eq;
	private int mode;

	public EquipmentIcon(GameModule _game, EquipmentData _eq, int _mode) {
		super(_game, R.drawable.menu_frame_green, _eq.getName(true).replaceAll(" ", "\n"));

		eq = _eq;
		mode = _mode;
	}


	public boolean mouseClicked() {
		if (game.question) {
			switch (mode) {
			case PICKUP:
				game.addToHUD("Pick up an item");
				break;
			case CHANGE:
				game.addToHUD("Change the unit's current item");
				break;
			default:
				throw new RuntimeException("Unknown equipment command: " + mode);
			}
		} else {
			switch (mode) {
			case PICKUP:
				game.pickupEquipment(eq);
				break;
			case CHANGE:
				game.changeCurrentEquipment(eq);
				break;
			default:
				throw new RuntimeException("Unknown equipment command: " + mode);
			}
		}
		return true;

	}

}
