package com.scs.stellarforces.graphics.icons;

import com.scs.stellarforces.game.GameModule;

public class ShotTypeIcon extends CommandIcon {
	
	private String orig_text;
	
	public ShotTypeIcon(GameModule _game, String text, byte cmd) {
		super(_game, text, cmd);
		
		orig_text = text;
	}

	
	public void setPcentAndAPCost(int acc, int aps) {
		this.setText(this.orig_text + "\n" + acc + "% (" + aps + ")");
	}


}
