package com.scs.stellarforces.graphics.icons;

import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.graphics.ExpandingTextLabel;
import com.scs.stellarforces.main.lite.R;

public class CommandIcon extends AbstractIcon {

	public static final byte SCANNER = 2;
	public static final byte CANCEL = 3;
	public static final byte PICKUP_ITEM = 4;
	public static final byte CHANGE_ITEM = 5;
	public static final byte DROP_ITEM = 6;
	public static final byte SELECT_SHOT_TYPE = 7;
	public static final byte END_TURN = 10;
	public static final byte AIMED_SHOT = 11;
	public static final byte SNAP_SHOT = 12;
	public static final byte AUTO_SHOT = 13;
	public static final byte RELOAD = 14;
	public static final byte SHOW_PRIME_MENU = 15;
	public static final byte SHOW_THROW_MENU = 16;
	//public static final byte CONFIRM_END_TURN = 17;
	public static final byte USE_MEDI_KIT_SELF = 18;
	public static final byte NEXT_UNIT = 19;
	public static final byte PREV_UNIT = 20;
	public static final byte WARNING = 21;
	//public static final byte USE_PORTA_PORTER = 22;
	public static final byte ACTIVATE = 23;
	//public static final byte USE_ADRENALIN_SHOT = 24;
	public static final byte OPEN_DOOR = 25;
	//public static final byte START_TUTORIAL = 26;
	public static final byte REMOVE_ITEM = 27;
	//public static final byte AUTO_AIM = 28;
	public static final byte ESCAPE = 29;
	public static final byte CLOSE_DOOR = 30;
	/*public static final byte OPP_FIRE_AIMED = 31;
	public static final byte OPP_FIRE_SNAP = 32;
	public static final byte OPP_FIRE_AUTO = 33;*/
	public static final byte ABSORB = 34;
	public static final byte EXPLODE = 35;
	public static final byte SPLIT = 36;
	public static final byte TEST = 37;
	//public static final byte SHOW_GRID = 38;
	public static final byte EQUIPMENT_MENU = 39;
	//public static final byte MISC_MENU = 40;
	public static final byte NEXT_TUTORIAL = 41;
	//public static final byte HELP_MODE = 42;
	//public static final byte SHOW_LOG = 43;
	//public static final byte BETTER_SCANNER = 44;
	public static final byte USE_SCANNER = 45;
	//public static final byte SHOW_TELEPORT_MENU = 46;
	//public static final byte TELEPORT = 47;
	//public static final byte CREATE_OR_ABSORB_ = 48;
	public static final byte UNDO_DEPLOY = 49;
	public static final byte USE_MEDI_KIT_OTHER = 50;
	public static final byte BUILD_STRUCTURE = 51;
	public static final byte DISMANTLE_STRUCTURE = 52;
	public static final byte LAY_EGG = 53;
	public static final byte HEAL = 54;

	public byte cmd;

	public CommandIcon(GameModule m, int col, String text, byte _cmd) {
		super(m, col, text);

		cmd = _cmd;

	}


	public CommandIcon(GameModule m, String text, byte _cmd) {
		this(m, R.drawable.menu_frame_blue, text, _cmd);

	}


	public String toString() {
		return "CommandIcon: " + cmd;
	}


	public boolean mouseClicked() {
		/*if (Statics.TEST_SOUND) {
			game.act.playSound(R.raw.toggle2);
		}*/
		if (game.question) {
			switch (cmd) {
			case SCANNER:
				game.addToHUD("Show the strategic scanner");
				break;
			case CANCEL:
				game.addToHUD("Return to Previous menu");
				break;
			case PICKUP_ITEM:
				game.addToHUD("Lists items that the unit can pick up");
				break;
			case CHANGE_ITEM:
				game.addToHUD("Change the unit's current item");
				break;
			case DROP_ITEM:
				game.addToHUD("The unit will drop the item they are holding");
				break;
			case SELECT_SHOT_TYPE:
				game.addToHUD("Selects the type of shot the unit should make");
				break;
			case AIMED_SHOT:
				game.addToHUD("Make an aimed shot (most accurate)");
				break;
			case SNAP_SHOT:
				game.addToHUD("Make an snap shot (medium accuracy)");
				break;
			case AUTO_SHOT:
				game.addToHUD("Make an autoshot (least accurate)");
				break;
			case RELOAD:
				game.addToHUD("Reload the current weapon.  An ammo pack is required");
				break;
			case SHOW_PRIME_MENU:
				game.addToHUD("Brings up the Prime Grenade menu");
				break;
			case SHOW_THROW_MENU:
				game.addToHUD("Aims a throw");
				break;
			case END_TURN:
				game.addToHUD("Ends your current turn");
				break;
			/*case CONFIRM_END_TURN:
				game.addToHUD("Confirms you wish to end your turn");
				break;*/
			case USE_MEDI_KIT_SELF:
				game.addToHUD("Use the unit's medikit to heal themself");
				break;
			case USE_MEDI_KIT_OTHER:
				game.addToHUD("Use the unit's medikit to heal facing unit");
				break;
			case HEAL:
				game.addToHUD("Heal another unit");
				break;
			case NEXT_UNIT:
				game.addToHUD("Select the next deployed unit in your squad");
				break;
			case PREV_UNIT:
				game.addToHUD("Select the previous deployed unit in your squad");
				break;
			case ACTIVATE:
				game.addToHUD("Activate the unit's current item");
				break;
			case OPEN_DOOR:
				game.addToHUD("Opens the door that the unit is facing");
				break;
			case REMOVE_ITEM:
				game.addToHUD("The unit puts their current item back in their pocket");
				break;
			case ESCAPE:
				game.addToHUD("Escape through the escape hatch");
				break;
			case CLOSE_DOOR:
				game.addToHUD("Closes the door that the unit is facing");
				break;
			case ABSORB:
				game.addToHUD("Absorb a corpse for more strength");
				break;
			case SPLIT:
				game.addToHUD("Split the blob in half");
				break;
			case EXPLODE:
				game.addToHUD("Explode cuasing damage but killing the blob");
				break;
			case NEXT_TUTORIAL:
				game.addToHUD("Select the next stage of the tutorial");
				break;
			case TEST:
				break;
			case EQUIPMENT_MENU:
				game.addToHUD("Shows the equipment menu");
				break;
			case USE_SCANNER:
				game.addToHUD("Use the distance scanner");
				break;
			/*case SHOW_TELEPORT_MENU:
				game.addToHUD("Shows the teleport menu");
				break;
			case TELEPORT:
				game.addToHUD("Teleports the unit");
				break;*/
			/*case CREATE_OR_ABSORB:
				game.addToHUD("Create or absorb the square facing the unit");
				break;*/
			case UNDO_DEPLOY:
				game.addToHUD("Undo the deployment of the last deployed unit");
				break;
			case BUILD_STRUCTURE:
				game.addToHUD("Build a new Structure");
				break;
			case DISMANTLE_STRUCTURE:
				game.addToHUD("Dismantle a Structure");
				break;
			case LAY_EGG:
				game.addToHUD("Lays an egg");
				break;
			default:
				// NO! as we might not have added it throw new RuntimeException("Unknown command: " + cmd);
			}
			return true;
		} else {
			switch (cmd) {
			case SCANNER:
				game.toggleScanner();
				break;
			case CANCEL:
				game.cancelMenu();
				break;
			case PICKUP_ITEM:
				game.showPickupItemMenu();
				break;
			case CHANGE_ITEM:
				game.showChangeUnitsItemMenu();
				break;
			case DROP_ITEM:
				game.dropCurrentItem();
				break;
			case SELECT_SHOT_TYPE:
				game.showSelectShotTypeMenu();
				break;
			case AIMED_SHOT:
				game.makeAimedShot();
				break;
			case SNAP_SHOT:
				game.makeSnapShot();
				break;
			case AUTO_SHOT:
				game.makeAutoShot();
				break;
			case RELOAD:
				game.reload();
				break;
			case SHOW_PRIME_MENU:
				game.showPrimeMenu();
				break;
			case SHOW_THROW_MENU:
				game.showThrowMenu();
				break;
			/*case END_TURN:
				game.showConfirmEndTurnMenu();
				break;
			/*case CONFIRM_END_TURN:
				game.showExplosionsAndEndTurn();
				break;*/
			case USE_MEDI_KIT_SELF:
				game.useMedikit(true);
				break;
			case USE_MEDI_KIT_OTHER:
				game.useMedikit(false);
				break;
			case HEAL:
				game.heal();
				break;
			case NEXT_UNIT:
				game.selectNextUnit();
				break;
			case PREV_UNIT:
				game.selectPrevUnit();
				break;
			case WARNING:
				// Do nothing
				break;
				/*case USE_PORTA_PORTER:
			//main.usePortaPorter();
			break;*/
			case ACTIVATE:
				game.activateEquipment();
				break;
				/*case USE_ADRENALIN_SHOT:
			//main.useAdrenalinShot();
			break;*/
			case OPEN_DOOR:
				game.openDoor();
				break;
				/*case WAIT:
			main.makeUnitWait();
			break;*/
			case REMOVE_ITEM:
				game.askIfRemoveItem();
				break;
			case ESCAPE:
				game.escape();
				break;
			case CLOSE_DOOR:
				game.closeDoor();
				break;
				/*case OPP_FIRE_AIMED:
			main.selectOppFire(OppFireTypes.AIMED_SHOT);
			break;
		case OPP_FIRE_SNAP:
			main.selectOppFire(OppFireTypes.SNAP_SHOT);
			break;
		case OPP_FIRE_AUTO:
			main.selectOppFire(OppFireTypes.AUTO_SHOT);
			break;*/
			case ABSORB:
				game.absorbeCorpse();
				break;
			case SPLIT:
				game.splitBlob();
				break;
			case EXPLODE:
				game.blobAttemptExplode();
				break;
				/*case START_TUTORIAL:
			game.startTutorial();
			break;*/
			case NEXT_TUTORIAL:
				game.nextTutorial();
				break;
			case TEST:
				//game.hud.appendText("wgfbsfgb sfgb sfb sfbsfb sfgb sfgbsfgb sfg s bbg sf bsfgb  sfb sfb sfbsfb");
				game.root_node.attachChild(new ExpandingTextLabel(this.game.getCurrentUnit().model, "20"));
				game.root_node.updateGeometricState();
				break;
				/*case SHOW_GRID:
			game.toggleGrid();
			break;*/
			case EQUIPMENT_MENU:
				game.showEquipmentMenu();
				break;
				/*case MISC_MENU:
			game.showMiscMenu();
			break;
		case HELP_MODE:
			game.toggleHelpMode();
			break;
		case SHOW_LOG:
			game.showGameLog();
			break;
		case BETTER_SCANNER:
			game.showBetterScanner();
			break;*/
			case USE_SCANNER:
				game.useScanner();
				break;
			/*case SHOW_TELEPORT_MENU:
				game.showTeleportMenu();
				break;
			case TELEPORT:
				game.teleport();
				break;*/
			/*case CREATE_OR_ABSORB:
				game.createOrAbsorb();
				break;*/
			case UNDO_DEPLOY:
				game.undoDeployment();
				break;
			case BUILD_STRUCTURE:
				game.showBuildStructureMenu();
				break;
			case DISMANTLE_STRUCTURE:
				game.dismantleStructure();
				break;
			case LAY_EGG:
				game.layEgg();
				break;
			default:
				throw new RuntimeException("Unknown command: " + cmd);
			}
			return true;
		}

	}

}
