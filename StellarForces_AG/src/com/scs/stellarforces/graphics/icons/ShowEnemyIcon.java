package com.scs.stellarforces.graphics.icons;

import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

import dsr.data.UnitData;

public class ShowEnemyIcon extends AbstractIcon {
	
	public static Paint sei_paint_transp = new Paint();
	
	static {
		sei_paint_transp.setARGB(255, 255, 255, 255);
	}

	private UnitData unit;
	
	public ShowEnemyIcon(GameModule game, UnitData _unit) {
		super(game, R.drawable.menu_frame_red, _unit.name.replaceAll(" ", "\n"));
		
		//this.paint_transp.setAlpha(255); // Not transp!
		this.paint = sei_paint_transp; // So it's not transparent

		unit =_unit;
		_unit.icon = this;

	}
	
	
	public void setFlashing(boolean b) {
		if (b) {
			unit.icon.setupFlashing(game.img_cache.getImage(R.drawable.menu_frame_yellow, Statics.ICONS_WIDTH, Statics.ICONS_HEIGHT), 300);
		} else {
			unit.icon.stopFlashing();
		}
	}

	@Override
	public boolean mouseClicked() {
		if (game.question) {
			game.addToHUD("Move the camera to show this enemy unit");
		} else {
			game.lookAtEnemyUnit(unit);
		}
		return true;
	}
	
	
	public String toString() {
		return "ShowEnemyIcon: " + unit.name;
	}
	
	
	public UnitData getUnit() {
		return this.unit;
	}
	

}
