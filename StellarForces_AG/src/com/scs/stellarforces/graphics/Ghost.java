package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.game.IProcessable;
import com.scs.stellarforces.main.lite.R;

public class Ghost extends GameObject implements IProcessable {

	private static final int SPEED = 5;
	
	private Bitmap frame;
	private float dist_moved = 0;

	public Ghost(GameModule game, float pxl_centre_x, float pxl_centre_y, float pxl_size) {
		super(game, "Ghost", false, pxl_centre_x-(pxl_size/2), pxl_centre_y-(pxl_size/2), pxl_size, pxl_size, false, false);

		frame = game.img_cache.getImage(R.drawable.ghost, pxl_size, pxl_size);

		game.attachToRootNode_Top(this, true);
		this.updateGeometricState();

		game.addToProcess(this);
	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		g.drawBitmap(frame, this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);

		/*if (this.world_bounds.top - cam.top + this.getHeight() < 0) {
			game.removeFromProcess(this);
			this.removeFromParent();
		}*/
	}
	
	
	@Override
	public void process(long interpol) {
		this.setLocation(this.local_rect.left, this.local_rect.top - SPEED);
		this.updateGeometricState();
		
		dist_moved += SPEED;
		
		if (this.dist_moved > Statics.SCREEN_HEIGHT) {
			game.removeFromProcess(this);
			this.removeFromParent();
		}
		
	}


}
