package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import ssmith.android.lib2d.MyPointF;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.game.IProcessable;

public abstract class AbstractAreaEffectGun extends GameObject implements IProcessable {
	
	private MyPointF move_dir, pixel_end;
	private float max_dist;
	private float dist_so_far = 0;
	private Bitmap bmp;

	public AbstractAreaEffectGun(GameModule _game, String name, int r, MyPointF pxl_start, MyPointF _pixel_end) {
		super(_game, name, false, pxl_start.x, pxl_start.y, Statics.SQ_SIZE, Statics.SQ_SIZE, false, false);

		pixel_end = _pixel_end;
		bmp = Statics.img_cache.getImage(r, Statics.SQ_SIZE, Statics.SQ_SIZE);
		
		move_dir = pixel_end.subtract(pxl_start).normalize().multiply(Statics.BULLET_MOVE_DIST);
		max_dist = pixel_end.subtract(pxl_start).length();

		game.attachToRootNode_Top(this, true);
		game.addToProcess(this);
	}
	

	@Override
	public void process(long interpol) {
		if (move_dir.length() == 0) {
			remove();
		} else { 
			MyPointF act_dir = move_dir.multiply(interpol);
			this.adjustLocation(act_dir);
			dist_so_far += act_dir.length();
			this.updateGeometricState();
			if (dist_so_far > max_dist) {
				remove();
			} else {
				checkSquares();
			}
		}
	}

	
	protected abstract void checkSquares();

	private void remove() {
		game.removeFromProcess(this);
		this.removeFromParent();

	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		if (this.visible) {
			g.drawBitmap(bmp, this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);
		}
	}


}
