package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.game.IProcessable;
import com.scs.stellarforces.main.lite.R;

public class CCExplosion extends GameObject implements IProcessable {

	private static final int DURATION = 200;

	private int current_frame = 0;
	private long timer = DURATION;
	private Bitmap frames[] = new Bitmap[3];

	public CCExplosion(GameModule game, float pxl_centre_x, float pxl_centre_y) {
		super(game, "CCExplosion", false, pxl_centre_x-(Statics.SQ_SIZE/2), pxl_centre_y-(Statics.SQ_SIZE/2), Statics.SQ_SIZE, Statics.SQ_SIZE, false, false);

		/*if (pxl_size <= 0) {
			pxl_size = Statics.SQ_SIZE/2; // Avoid error
		}*/
		frames[0] = game.img_cache.getImage(R.drawable.explosion5, Statics.SQ_SIZE, Statics.SQ_SIZE);
		frames[1] = game.img_cache.getImage(R.drawable.explosion6, Statics.SQ_SIZE, Statics.SQ_SIZE);
		frames[2] = game.img_cache.getImage(R.drawable.explosion7, Statics.SQ_SIZE, Statics.SQ_SIZE);

		game.attachToRootNode_Top(this, true);
		this.updateGeometricState();

		game.addToProcess(this);
	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		if (frames[current_frame] != null) {
			if (frames[current_frame].isRecycled() == false) {
				//g.drawBitmap(frames[current_frame], this.world_bounds.left - cam.left - (Statics.SQ_SIZE/4), this.world_bounds.top - cam.top - (Statics.SQ_SIZE/4), paint);
				g.drawBitmap(frames[current_frame], this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);
			}
		}
	}


	@Override
	public void process(long interpol) {
		timer -= interpol;
		if (timer < 0) {
			timer = DURATION; // game.root_node
			this.current_frame++;
			if (this.current_frame >= frames.length) {
				this.removeFromParent();
				game.removeFromProcess(this);
			}
		}

	}


}
