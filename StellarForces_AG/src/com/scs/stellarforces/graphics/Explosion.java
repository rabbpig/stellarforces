package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.game.IProcessable;
import com.scs.stellarforces.main.lite.R;

public class Explosion extends GameObject implements IProcessable {

	private static final int DURATION = 200;

	private int current_frame = 0;
	private long timer = DURATION;
	private Bitmap frames[] = new Bitmap[7];

	public Explosion(GameModule game, float pxl_centre_x, float pxl_centre_y, float pxl_size) {
		//super(game, "Explosion", false, (map_x*Statics.SQ_SIZE)-(pxl_size/2), (map_y*Statics.SQ_SIZE)-(pxl_size/2), pxl_size, pxl_size, false, false);
		super(game, "Explosion", false, pxl_centre_x-(pxl_size/2), pxl_centre_y-(pxl_size/2), pxl_size, pxl_size, false, false);

		if (pxl_size <= 0) {
			pxl_size = Statics.SQ_SIZE/2; // Avoid error
		} else if (pxl_size > Statics.SCREEN_WIDTH/3) {
			pxl_size = Statics.SCREEN_WIDTH/3;
		}

		try {
			frames[0] = game.img_cache.getImage(R.drawable.explosion1, pxl_size, pxl_size);
			frames[1] = game.img_cache.getImage(R.drawable.explosion2, pxl_size, pxl_size);
			frames[2] = game.img_cache.getImage(R.drawable.explosion3, pxl_size, pxl_size);
			frames[3] = game.img_cache.getImage(R.drawable.explosion4, pxl_size, pxl_size);
			frames[4] = game.img_cache.getImage(R.drawable.explosion5, pxl_size, pxl_size);
			frames[5] = game.img_cache.getImage(R.drawable.explosion6, pxl_size, pxl_size);
			frames[6] = game.img_cache.getImage(R.drawable.explosion7, pxl_size, pxl_size);
		} catch (java.lang.OutOfMemoryError ex) {
			// Do nothing
			ex.printStackTrace();
		}

		game.attachToRootNode_Top(this, true);
		this.updateGeometricState();

		game.addToProcess(this);
	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		//g.drawBitmap(frames[current_frame], this.world_bounds.left - cam.left - (Statics.SQ_SIZE/4), this.world_bounds.top - cam.top - (Statics.SQ_SIZE/4), paint);
		if (frames[current_frame] != null) {
			if (frames[current_frame].isRecycled() == false) {
				g.drawBitmap(frames[current_frame], this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);
			}
		}
	}


	@Override
	public void process(long interpol) {
		timer -= interpol;
		if (timer < 0) {
			timer = DURATION; // game.root_node
			this.current_frame++;
			if (this.current_frame >= frames.length) {
				this.removeFromParent();
				game.removeFromProcess(this);
				
				// Recycle bitmaps
				for (int i=0 ; i<frames.length ; i++) {
					game.img_cache.remove(frames[i]);
					frames[i] = null;
				}
			}
		}

	}


}
