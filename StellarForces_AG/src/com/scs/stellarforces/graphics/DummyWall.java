package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import android.graphics.Canvas;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;

import dsr.data.AppletMapSquare;


/**
 * This is used to check for blocked views
 *
 */
public class DummyWall extends GameObject {
	
	public AppletMapSquare sq;
	
	public DummyWall(GameModule game, AppletMapSquare _sq, int x, int y) {
		super(game, "DummyWall", true, x, y, Statics.SQ_SIZE, Statics.SQ_SIZE, true, true);
		
		sq = _sq;
	}

	
	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		// Do nothing
		
	}

}
