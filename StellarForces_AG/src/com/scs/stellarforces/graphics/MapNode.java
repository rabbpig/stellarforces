package com.scs.stellarforces.graphics;

import java.util.Hashtable;

import ssmith.android.framework.ErrorReporter;

import ssmith.android.lib2d.layouts.EfficientGridLayout;
import ssmith.android.media.ImageFunctions;
import ssmith.android.media.InvalidImageConfigException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Color;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.game.TextureStateCache;
import com.scs.stellarforces.main.lite.R;

import dsr.data.AppletMapSquare;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;

public class MapNode extends EfficientGridLayout {

	private static final String RAISED_TEX = "RAISED_TEX_";
	private static final String WALL_3D = "WALL3D_";
	private static final String SCENERY = "SCENERY_";

	private GameModule game;
	private static Hashtable<String, Bitmap> bmp_cache = new Hashtable<String, Bitmap>(); 

	public MapNode(GameModule _game) {
		super(_game.mapdata.getMapWidth(), _game.mapdata.getMapHeight(), Statics.SQ_SIZE);

		game = _game;

		this.createMapModel();
	}


	private void createMapModel() {
		boolean sent_error = false; // Only send the error once!
		for (int y=0 ; y<game.mapdata.getMapHeight() ; y++) {
			for (int x=0 ; x<game.mapdata.getMapWidth() ; x++) {
				AppletMapSquare sq = game.mapdata.getSq_MaybeNULL(x, y);
				String key = ""+sq.texture_code; 
				Bitmap bmp = null;
				if ((sq.escape_hatch_side == game.game_data.our_side || sq.escape_hatch_side > 4) && game.game_data.our_side > 0) {
					bmp = game.img_cache.getImage(R.drawable.escape_hatch, Statics.SQ_SIZE_INT, Statics.SQ_SIZE_INT);
					key = key + "_escapehatch";
				} else if (sq.deploy_sq_side > 0 && sq.major_type == MapDataTable.MT_FLOOR && game.game_data.areSidesFriends(sq.deploy_sq_side, game.game_data.our_side) && game.game_data.game_status == GamesTable.GS_DEPLOYMENT) {
					bmp = game.img_cache.getImage(R.drawable.deploy_sq, Statics.SQ_SIZE_INT, Statics.SQ_SIZE_INT);
					key = key + "_deploy" + sq.deploy_sq_side;
				} else if (sq.deploy_sq_side > 0 && sq.major_type == MapDataTable.MT_FLOOR && game.game_data.areSidesFriends(sq.deploy_sq_side, game.game_data.our_side) == false && game.game_data.game_status == GamesTable.GS_DEPLOYMENT) {
					bmp = game.img_cache.getImage(R.drawable.deploy_sq_opponent, Statics.SQ_SIZE_INT, Statics.SQ_SIZE_INT);
					key = key + "_deploy" + sq.deploy_sq_side;
					/*} else if (sq.destroyed > 0 && sq.texture_code != TextureStateCache.TEX_WATER) {
					bmp = game.img_cache.getImage(TextureStateCache.GetTexResourceID(TextureStateCache.GetRandomRubbleTex()), Statics.SQ_SIZE_INT, Statics.SQ_SIZE_INT);
					key = key + "_rubble";*/
				} else {
					bmp = game.img_cache.getImage(TextureStateCache.GetTexResourceID(sq.texture_code), Statics.SQ_SIZE_INT, Statics.SQ_SIZE_INT);
				}

				// NO!  As we "destroy" dismantled walls but we don't want to show rubble
				/*if (sq.destroyed > 0 && sq.texture_code != TextureStateCache.TEX_WATER && sq.escape_hatch_side <= 0) {
					sq.raised_texture_code = TextureStateCache.GetRandomRubbleTexTransp();
				}*/

				// See if there is a raised tex that needs combining
				if (sq.raised_texture_code > 0) {
					// Have we already combined them and cached it?  If so, use it
					key = key + "_" + RAISED_TEX + sq.raised_texture_code;
					if (bmp_cache.containsKey(key)) {
						bmp = bmp_cache.get(key);
					} else {
						try {
							// This is a new tex that needs creating
							if (bmp.getConfig().name().equalsIgnoreCase(Bitmap.Config.RGB_565.name()) == false || bmp.isMutable() == false) {
								bmp = bmp.copy(Config.RGB_565, true);
							}
							Bitmap raised_bmp = game.img_cache.getImage(TextureStateCache.GetTexResourceID(sq.raised_texture_code), Statics.SQ_SIZE_INT, Statics.SQ_SIZE_INT);
							if (raised_bmp.getConfig().name().equalsIgnoreCase(Bitmap.Config.ARGB_8888.name()) == false) {
								raised_bmp = raised_bmp.copy(Config.ARGB_8888, true);
							}
							bmp = ImageFunctions.CombineBitmaps(bmp, raised_bmp);
							bmp_cache.put(key, bmp);
						} catch (InvalidImageConfigException ex) {
							if (sent_error == false) {
								ErrorReporter.getInstance().handleSilentException(ex);
								sent_error = true;
							}
						}
					}
				}
				// See if there is a scenery tex that needs combining
				if (sq.scenery_code > 0) {
					if (TextureStateCache.GetSceneryResourceID(sq.scenery_code) > 0) { // Check we can handle it
						// Have we already combined them and cached it?  If so, use it
						key = key + "_" + SCENERY + sq.scenery_code; 
						if (bmp_cache.containsKey(key)) {
							bmp = bmp_cache.get(key);
						} else {
							try {
								// This is a new tex that needs creating
								if (bmp.getConfig().name().equalsIgnoreCase(Bitmap.Config.RGB_565.name()) == false || bmp.isMutable() == false) {
									bmp = bmp.copy(Config.RGB_565, true);
								}
								Bitmap scenery_bmp = game.img_cache.getImage(TextureStateCache.GetSceneryResourceID(sq.scenery_code), Statics.SQ_SIZE_INT, Statics.SQ_SIZE_INT);
								if (scenery_bmp.getConfig().name().equalsIgnoreCase(Bitmap.Config.ARGB_8888.name()) == false) {
									scenery_bmp = scenery_bmp.copy(Config.ARGB_8888, true);
								}
								bmp = ImageFunctions.CombineBitmaps(bmp, scenery_bmp);
								bmp_cache.put(key, bmp);
							} catch (InvalidImageConfigException ex) {
								if (sent_error == false) {
									ErrorReporter.getInstance().handleSilentException(ex);
									sent_error = true;
								}
							}
						}
					}
				}
				// Show wall shadow?
				if (sq.major_type == MapDataTable.MT_WALL) {
					boolean change = false;
					if (y < game.mapdata.getMapHeight()-1) {
						AppletMapSquare sq_down = game.mapdata.getSq_MaybeNULL(x, y+1);
						if (sq_down.major_type != MapDataTable.MT_WALL && sq_down.door_type <= 0) {
							change = true;
						}
					} else {
						change = true;
					}
					if (change) {
						key = key + "_" + WALL_3D + sq.texture_code; 
						if (bmp_cache.containsKey(key)) {
							bmp = bmp_cache.get(key);
						} else {
							Bitmap bmp2 = bmp.copy(Bitmap.Config.RGB_565, true); // Create copy so we don't edit the original and thus change all squares.
							if (bmp2 != null) { // Can happen
								bmp = bmp2;
								// Make bottom-half darker
								for(int by = (int)(bmp.getHeight()*0.4f) ; by < bmp.getHeight() ; by++) {
									for(int bx = 0 ; bx < bmp.getWidth() ; bx++) {
										int pixel = bmp.getPixel(bx, by);
										bmp.setPixel(bx, by, Color.argb(Color.alpha(pixel), Color.red(pixel)/2, Color.green(pixel)/2, Color.blue(pixel)/2));
									}
								}
							}
							bmp_cache.put(key, bmp);
						}
					}

				}

				MapSquareImage bc = new MapSquareImage(game, "Map_Sq", sq, bmp, (byte)x, (byte)y);
				super.setRectAtMap(bc, x, y);

				// Add shadows
				if (sq.major_type == MapDataTable.MT_FLOOR) {
					boolean wall_left = false;
					boolean wall_left_bottom = false;
					boolean wall_bottom = false;
					if (x > 0) {
						wall_left = game.mapdata.getSq_MaybeNULL(x-1, y).castsShadow();
					}
					if (x > 0 && y < game.mapdata.getMapHeight()-1) {
						wall_left_bottom = game.mapdata.getSq_MaybeNULL(x-1, y+1).castsShadow();
					}
					if (y < game.mapdata.getMapHeight()-1) {
						wall_bottom = game.mapdata.getSq_MaybeNULL(x, y+1).castsShadow();
					}
					int checksum = 0;
					checksum += (wall_left ? 1 : 0);
					checksum += (wall_left_bottom ? 2 : 0);
					checksum += (wall_bottom ? 4 : 0);

					if (checksum == 1) { // Left only
						bc.bmp_shad = game.img_cache.getImage(TextureStateCache.GetTexResourceID(TextureStateCache.TEX_SHADOW_RIGHT), Statics.SQ_SIZE/2, Statics.SQ_SIZE);
						//OtherSprite shad1 = new OtherSprite(window, -1, "Shadow", Type.SCENERY, x*Statics.SQ_SIZE, y*Statics.SQ_SIZE, Statics.SQ_SIZE/2, Statics.SQ_SIZE, AbstractClientSprite.DRAW_LEVEL_SHADOW, false, false, ImageCodes.SHADOW_RIGHT_TRI, 0, (byte)0, false);
					} else if (checksum == 2) { // left-bottom only
						bc.bmp_shad = game.img_cache.getImage(TextureStateCache.GetTexResourceID(TextureStateCache.TEX_SHADOW_SQUARE), Statics.SQ_SIZE/2, Statics.SQ_SIZE/2);
						bc.shad_offset_y = Statics.SQ_SIZE/2;
						//OtherSprite shad2 = new OtherSprite(window, -1, "Shadow", Type.SCENERY, x*Statics.SQ_SIZE, y*Statics.SQ_SIZE, Statics.SQ_SIZE/2, Statics.SQ_SIZE/2, AbstractClientSprite.DRAW_LEVEL_SHADOW, false, false, ImageCodes.SHADOW_SQUARE, 0, (byte)0, false);
						//shad2.draw_off_y = Statics.SQ_SIZE/2;
					} else if (checksum == 4) { // bottom only
						bc.bmp_shad = game.img_cache.getImage(TextureStateCache.GetTexResourceID(TextureStateCache.TEX_SHADOW_TOP), Statics.SQ_SIZE, Statics.SQ_SIZE/2);
						bc.shad_offset_y = Statics.SQ_SIZE/2;
						//OtherSprite shad2 = new OtherSprite(window, -1, "Shadow", Type.SCENERY, x*Statics.SQ_SIZE, y*Statics.SQ_SIZE, Statics.SQ_SIZE, Statics.SQ_SIZE/2, AbstractClientSprite.DRAW_LEVEL_SHADOW, false, false, ImageCodes.SHADOW_TOP_TRI, 0, (byte)0, false);
						//shad2.draw_off_y = Statics.SQ_SIZE/2;
					} else if (checksum == 3) { // left and left-bottom
						bc.bmp_shad = game.img_cache.getImage(TextureStateCache.GetTexResourceID(TextureStateCache.TEX_SHADOW_SQUARE), Statics.SQ_SIZE/2, Statics.SQ_SIZE);
						//OtherSprite shad2 = new OtherSprite(window, -1, "Shadow", Type.SCENERY, x*Statics.SQ_SIZE, y*Statics.SQ_SIZE, Statics.SQ_SIZE/2, Statics.SQ_SIZE, AbstractClientSprite.DRAW_LEVEL_SHADOW, false, false, ImageCodes.SHADOW_SQUARE, 0, (byte)0, false);
					} else if (checksum == 5 || checksum == 7) { // left and bottom
						bc.bmp_shad = game.img_cache.getImage(TextureStateCache.GetTexResourceID(TextureStateCache.TEX_SHADOW_L_SHAPE), Statics.SQ_SIZE, Statics.SQ_SIZE);
						//OtherSprite shad2 = new OtherSprite(window, -1, "Shadow", Type.SCENERY, x*Statics.SQ_SIZE, y*Statics.SQ_SIZE, Statics.SQ_SIZE, Statics.SQ_SIZE, AbstractClientSprite.DRAW_LEVEL_SHADOW, false, false, ImageCodes.SHADOW_L_SHAPE, 0, (byte)0, false);
					} else if (checksum == 6) { // left-bottom and bottom
						bc.bmp_shad = game.img_cache.getImage(TextureStateCache.GetTexResourceID(TextureStateCache.TEX_SHADOW_SQUARE), Statics.SQ_SIZE, Statics.SQ_SIZE/2);
						bc.shad_offset_y = Statics.SQ_SIZE/2;
						//OtherSprite shad2 = new OtherSprite(window, -1, "Shadow", Type.SCENERY, x*Statics.SQ_SIZE, y*Statics.SQ_SIZE, Statics.SQ_SIZE, Statics.SQ_SIZE/2, AbstractClientSprite.DRAW_LEVEL_SHADOW, false, false, ImageCodes.SHADOW_SQUARE, 0, (byte)0, false);
						//shad2.draw_off_y = Statics.SQ_SIZE/2;
					}
				}

			}
		}
		game.root_node.updateGeometricState();
	}


}
