package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.game.IProcessable;
import com.scs.stellarforces.main.lite.R;

public class Sparks extends GameObject implements IProcessable {

	private static final float SIZE = Statics.SCREEN_WIDTH * .015f;
	private static final float MAX_SIZE = Statics.SQ_SIZE;
	private static final float SPEED = Statics.SQ_SIZE/10;

	private Bitmap bmp;
	private float dist = 0;

	public Sparks(GameModule game, float pxl_centre_x, float pxl_centre_y) {
		super(game, "Sparks", false, pxl_centre_x, pxl_centre_y, 0, 0, false, false);

		bmp = game.img_cache.getImage(R.drawable.explosion1, SIZE, SIZE);
		game.attachToRootNode_Top(this, true);
		this.updateGeometricState();

		game.addToProcess(this);
	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		g.drawBitmap(bmp, this.world_bounds.left - cam.left-dist, this.world_bounds.top - cam.top-dist, paint);
		g.drawBitmap(bmp, this.world_bounds.left - cam.left+dist, this.world_bounds.top - cam.top-dist, paint);
		g.drawBitmap(bmp, this.world_bounds.left - cam.left-dist, this.world_bounds.top - cam.top+dist, paint);
		g.drawBitmap(bmp, this.world_bounds.left - cam.left+dist, this.world_bounds.top - cam.top+dist, paint);
	}


	@Override
	public void process(long interpol) {
		dist += SPEED;
		if (dist > MAX_SIZE) {
			this.removeFromParent();
			game.removeFromProcess(this);
		}

	}


}
