package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import ssmith.lang.NumberFunctions;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.game.IProcessable;
import com.scs.stellarforces.main.lite.R;

public class JokeSpider extends GameObject implements IProcessable {

	private static final float SIZE = Statics.SCREEN_WIDTH/10f;
	//private static final int SPEED = 15;
	
	private Bitmap frame;
	private float dist_moved = 0;

	
	public JokeSpider(GameModule game) {
		super(game, "JokeSpider", false, game.root_cam.left + NumberFunctions.rndFloat(Statics.SCREEN_WIDTH/4, Statics.SCREEN_WIDTH*.75f), game.root_cam.top-SIZE, SIZE, SIZE, false, false);

		frame = game.img_cache.getImage(R.drawable.fire_ant_s, SIZE, SIZE);

		game.attachToRootNode_Top(this, true);
		this.updateGeometricState();

		game.addToProcess(this);
	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		g.drawBitmap(frame, this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);

	}
	
	
	@Override
	public void process(long interpol) {
		float dist = interpol;
		this.setLocation(this.local_rect.left, this.local_rect.top + dist);
		this.updateGeometricState();
		
		dist_moved += dist;
		
		if (this.dist_moved > Statics.SCREEN_HEIGHT+SIZE) {
			game.removeFromProcess(this);
			this.removeFromParent();
		}
		
	}


}
