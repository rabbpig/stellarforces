package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import ssmith.android.lib2d.MyPointF;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.game.IProcessable;

public class BulletLaser extends GameObject implements IProcessable {

	private static final float SIZE = Statics.SQ_SIZE/4;

	private MyPointF move_dir, pixel_end;
	private float max_dist;
	private float dist_so_far = 0;
	private boolean explosion_at_end;
	private boolean track;
	private MyPointF orig_cam_loc; // For after tracking a bullet

	private static Paint _paintSimple, _paintBlur;

	static {
		_paintSimple = new Paint();
	    _paintSimple.setAntiAlias(true);
	    _paintSimple.setDither(true);
	    _paintSimple.setColor(Color.argb(248, 255, 255, 255));
	    _paintSimple.setStrokeWidth(Statics.SCREEN_HEIGHT * 0.005f);
	    _paintSimple.setStyle(Paint.Style.STROKE);
	    _paintSimple.setStrokeJoin(Paint.Join.ROUND);
	    _paintSimple.setStrokeCap(Paint.Cap.ROUND);

	    _paintBlur = new Paint();
	    _paintBlur.set(_paintSimple);
	    _paintBlur.setColor(Color.argb(235, 255, 255, 0));
	    _paintBlur.setStrokeWidth(Statics.SCREEN_HEIGHT * 0.02f);
	    _paintBlur.setMaskFilter(new BlurMaskFilter(Statics.SCREEN_HEIGHT * 0.01f, BlurMaskFilter.Blur.NORMAL)); 
	}


	public BulletLaser(GameModule _game, MyPointF pxl_start, MyPointF _pixel_end, boolean explode, boolean _track) {
		super(_game, "BulletLaser", true, pxl_start.x, pxl_start.y, Statics.BULLET_SIZE, Statics.BULLET_SIZE, false, false);

		pixel_end = _pixel_end;
		
		move_dir = pixel_end.subtract(pxl_start).normalize().multiply(Statics.BULLET_MOVE_DIST);
		max_dist = pixel_end.subtract(pxl_start).length();
		track = _track;
		explosion_at_end = explode;

		game.attachToRootNode_Top(this, true);
		game.addToProcess(this);

		if (track) {
			this.orig_cam_loc = this.game.root_cam.getActualCentre().copy();
		}

	}


	public void process(long interpol) {
		if (move_dir.length() == 0) {
			remove();
		} else { 
			MyPointF act_dir = move_dir.multiply(interpol);
			this.adjustLocation(act_dir);
			dist_so_far += act_dir.length();
			this.updateGeometricState();
			if (dist_so_far > max_dist) {
				remove();
			} else {
				if (track) {
					game.root_cam.lookAt(this, true);
				}
			}
		}
	}


	private void remove() {
		game.removeFromProcess(this);
		if (explosion_at_end) {
			//new Explosion(game, this.getWorldCentreX(), this.getWorldCentreY(), Statics.SQ_SIZE);
			new Explosion(game, this.pixel_end.x, this.pixel_end.y, Statics.SQ_SIZE);
		} else {
			new Sparks(game, this.pixel_end.x, this.pixel_end.y);
		}
		// Point the targetter back to the original location
		if (this.track && this.orig_cam_loc != null) {
			this.game.root_cam.lookAt(this.orig_cam_loc.x, this.orig_cam_loc.y, true);
		}
		this.removeFromParent();

	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		if (this.visible) {
			float x = this.world_bounds.left - cam.left - (SIZE/2);
			float y = this.world_bounds.top - cam.top - (SIZE/2);
			float x2 = x+(move_dir.x * (Statics.SQ_SIZE));
			float y2 = y+(move_dir.y * (Statics.SQ_SIZE));
			//g.drawLine(x, y, x2, y2, paint_laser);
			 g.drawLine(x, y, x2, y2, _paintBlur);
			 g.drawLine(x, y, x2, y2, _paintSimple);

		}
	}

}
