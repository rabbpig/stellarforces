package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import ssmith.android.lib2d.MyPointF;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.game.IProcessable;

/**
 * This class is also used for thrown grenades.
 *
 */
public class Bullet extends GameObject implements IProcessable {
	
	private static final float SIZE = Statics.SQ_SIZE/4;
	private static final float LENGTH = Statics.SCREEN_WIDTH * 0.01f; // Was 0.007f

	private MyPointF move_dir, pixel_end;
	private float max_dist;
	private float dist_so_far = 0;
	private boolean explosion_at_end;
	private Bitmap bmp;
	private boolean track;
	private MyPointF orig_cam_loc; // For after tracking a bullet
	private int num_of_sprites;

	public Bullet(GameModule _game, MyPointF pxl_start, MyPointF _pixel_end, boolean explode, boolean _track, int r, int num, float size) {
		super(_game, "Bullet", true, pxl_start.x, pxl_start.y, size, size, false, false);

		pixel_end =_pixel_end;
		
		move_dir = pixel_end.subtract(pxl_start).normalize().multiply(Statics.BULLET_MOVE_DIST);
		max_dist = pixel_end.subtract(pxl_start).length();
		bmp = _game.img_cache.getImage(r, SIZE, SIZE);
		track = _track;
		explosion_at_end = explode;
		num_of_sprites = num;
		
		game.attachToRootNode_Top(this, true);
		game.addToProcess(this);

		if (track) {
			this.orig_cam_loc = this.game.root_cam.getActualCentre().copy();
		}

	}


	public void process(long interpol) {
		if (move_dir.length() == 0) {
			remove();
		} else { 
			MyPointF act_dir = move_dir.multiply(interpol);
			this.adjustLocation(act_dir);
			dist_so_far += act_dir.length();
			this.updateGeometricState();
			if (dist_so_far > max_dist) {
				remove();
			} else {
				if (track) {
					game.root_cam.lookAt(this, true);
				}
			}
		}
	}


	private void remove() {
		game.removeFromProcess(this);
		if (explosion_at_end) {
			new Explosion(game, this.pixel_end.x, this.pixel_end.y, Statics.SQ_SIZE);
		} else {
			new Sparks(game, this.pixel_end.x, this.pixel_end.y);
		}
		// Point the targetter back to the original location
		if (this.track && this.orig_cam_loc != null) {
			this.game.root_cam.lookAt(this.orig_cam_loc.x, this.orig_cam_loc.y, true);
		}
		this.removeFromParent();

	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		if (this.visible) {
			float x = this.world_bounds.left - cam.left - (SIZE/2);
			float y = this.world_bounds.top - cam.top - (SIZE/2);
			for (int i=0 ; i<num_of_sprites ; i++) {
				g.drawBitmap(bmp, x, y, paint);
				x += (move_dir.x * LENGTH);
				y += (move_dir.y * LENGTH);
			}
		}
	}

}
