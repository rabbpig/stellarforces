package com.scs.stellarforces.graphics;

import ssmith.util.Interval;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public abstract class AbstractUnitHighlighter {

	private Paint _paintSimple;
	private static int de_offset = 0;
	private static float de_arr[] = {30, 30, 30, 30};
	private Interval rotate_int = new Interval(100);

	public AbstractUnitHighlighter(int c1, int c2) {
		super();

		_paintSimple = new Paint();
		_paintSimple.setAntiAlias(true);
		_paintSimple.setDither(true);
		_paintSimple.setColor(c1); // Color.argb(248, 255, 255, 255));
		_paintSimple.setStrokeWidth(Statics.SCREEN_HEIGHT * 0.01f);
		_paintSimple.setStyle(Paint.Style.STROKE);
		_paintSimple.setStrokeJoin(Paint.Join.ROUND);
		_paintSimple.setStrokeCap(Paint.Cap.ROUND);

		/*_paintBlur = new Paint();
		_paintBlur.set(_paintSimple);
		_paintBlur.setColor(c2);//Color.argb(235, 74, 138, 255));
		_paintBlur.setStrokeWidth(Statics.SCREEN_HEIGHT * 0.05f);
		_paintBlur.setMaskFilter(new BlurMaskFilter(Statics.SCREEN_HEIGHT * 0.02f, BlurMaskFilter.Blur.NORMAL)); 
*/
	}


	public void doDraw(Canvas g, float cx, float cy) {
		//g.drawCircle(cx, cy, Statics.SQ_SIZE, _paintBlur);
		g.drawCircle(cx, cy, Statics.SQ_SIZE, _paintSimple);

		if (rotate_int.hitInterval()) {
			/*de_offset++;
			if (de_offset >= de_arr.length) {
				de_offset = 0;
			}*/
			de_arr[0]++;
			if (de_arr[0] >= 30) {
				de_arr[0] = 0;
			}
			_paintSimple.setPathEffect(new DashPathEffect(de_arr, de_offset));
			//_paintBlur.setPathEffect(new DashPathEffect(de_arr, de_offset));
		}
	}



}
