package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

import dsr.data.EquipmentData;

public class GasCannister extends GameObject {
	
	public EquipmentData eq;
	private Bitmap bmp;
	
	public GasCannister(GameModule game, EquipmentData _eq, int map_x, int map_y) {
		super(game, _eq.getName(false), true, map_x*Statics.SQ_SIZE, map_y*Statics.SQ_SIZE, Statics.SQ_SIZE, Statics.SQ_SIZE, true, false);
		
		eq = _eq;
		
		bmp = game.img_cache.getImage(R.drawable.barrel, Statics.SQ_SIZE, Statics.SQ_SIZE);

	}

	
	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		//g.drawBitmap(bmp, this.world_bounds.left - cam.left + (Statics.SQ_SIZE/4), this.world_bounds.top - cam.top + (Statics.SQ_SIZE/4), paint);
		g.drawBitmap(bmp, this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);
	}

	
}
