package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

import dsr.data.EquipmentData;
import dsrwebserver.tables.EquipmentTypesTable;

public class EquipmentModel extends GameObject {
	
	protected Bitmap bmp;

	/**
	 * This is called when loading the data from the server.
	 */
	public static GameObject Factory(GameModule game, EquipmentData eq, int map_x, int map_y) {
		//String name = eq.getName(false).toLowerCase();
		String equipcode = eq.code;//.toLowerCase(); 
			switch (eq.major_type) {
			case EquipmentTypesTable.ET_HUMAN_CORPSE_1:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.corpse1);
			case EquipmentTypesTable.ET_HUMAN_CORPSE_2:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.corpse2);
			case EquipmentTypesTable.ET_HUMAN_CORPSE_3:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.corpse3);
			case EquipmentTypesTable.ET_HUMAN_CORPSE_4:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.corpse4);
			case EquipmentTypesTable.ET_SCIENTIST_CORPSE:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.sterner_corpse);
			case EquipmentTypesTable.ET_ALIEN_CORPSE:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.alien_corpse);
			case EquipmentTypesTable.ET_ALIEN_QUEEN_CORPSE:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.alien_corpse);
			case EquipmentTypesTable.ET_CRAB_CORPSE:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.alien_corpse);
			case EquipmentTypesTable.ET_GHOUL_CORPSE:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.alien_corpse);
			case EquipmentTypesTable.ET_GHOUL_QUEEN_CORPSE:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.alien_corpse);
			case EquipmentTypesTable.ET_ZOMBIE_CORPSE:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.zombie_corpse);
			case EquipmentTypesTable.ET_CLONE_CORPSE_1:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.clone1_corpse);
			case EquipmentTypesTable.ET_CLONE_CORPSE_2:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.clone2_corpse);
			case EquipmentTypesTable.ET_CLONE_CORPSE_3:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.clone3_corpse);
			case EquipmentTypesTable.ET_CLONE_CORPSE_4:
				return new AbstractCorpse(game, eq.getName(false), map_x, map_y, R.drawable.clone4_corpse);
				// ##  If you add any corpses here, also add them to AbstractCorpse.Factory ##
			case EquipmentTypesTable.ET_EGG:
				return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.alien_egg);
			case EquipmentTypesTable.ET_GRENADE:
				return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.grenade);
			case EquipmentTypesTable.ET_SMOKE_GRENADE:
				return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.smoke_grenade);
			case EquipmentTypesTable.ET_NERVE_GAS:
				return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.nerve_gas_grenade);
			case EquipmentTypesTable.ET_DEATH_GRENADE:
				return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.death_grenade);
			case EquipmentTypesTable.ET_GAS_CANNISTER:
				return new GasCannister(game, eq, map_x, map_y);
			case EquipmentTypesTable.ET_GUN:
				if (equipcode.equalsIgnoreCase(EquipmentTypesTable.CD_SP30)) {
					return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.sp30);
				} else if (equipcode.equalsIgnoreCase("rocket_launcher")) {
					return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.rocket_launcher);
				} else if (equipcode.equalsIgnoreCase("marsecautogun")) {
					return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.ms_autogun);
				} else if (equipcode.equalsIgnoreCase("heavylaser")) {
					return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.heavy_laser);
				} else if (equipcode.indexOf("lasrifle") >= 0) {
					return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.las_rifle);
				} else if (equipcode.equalsIgnoreCase("m4000")) {
					return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.m4000);
				} else if (equipcode.equalsIgnoreCase("marsecpistol")) {
					return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.marsec_pistol);
				} else if (equipcode.equalsIgnoreCase("l50")) {
					return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.l50);
				} else if (equipcode.equalsIgnoreCase("mk1")) {
					return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.mk1);
				} else if (equipcode.equalsIgnoreCase("sniperrifle")) {
					return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.sniper_rifle);
				} else if (equipcode.equalsIgnoreCase("autocannon")) {
					return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.autocannon);
				} else {
					// Default
					return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.rifle);
				}
			case EquipmentTypesTable.ET_MEDIKIT:
				return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.medikit);
			case EquipmentTypesTable.ET_CC_WEAPON:
				return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.knife);
			case EquipmentTypesTable.ET_SCANNER:
				return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.scanner);
			case EquipmentTypesTable.ET_FLAG:
				return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.flag);
			case EquipmentTypesTable.ET_AMMO_CLIP:
				if (equipcode.equalsIgnoreCase("rocket")) {
					return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.rocket);
				} else {
					return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.ammo_clip);
				}
			case EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE:
				return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.incendiary_grenade);
			default:
				// DON'T THROW AN ERROR HERE AS WE MAY ADD NEW TYPES THAT AN OLD CLIENT WON'T KNOW ABOUT
				return new EquipmentModel(game, eq.getName(false), map_x, map_y, R.drawable.crate);
			}
	}
	
	
	public EquipmentModel(GameModule _game, String name, float map_x, float map_y, int img) {
		super(_game, name, true, map_x*Statics.SQ_SIZE, map_y*Statics.SQ_SIZE, Statics.SQ_SIZE, Statics.SQ_SIZE, false, false);
		
		bmp = Statics.img_cache.getImage(img, Statics.SQ_SIZE*.7f, Statics.SQ_SIZE*.7f);
	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		if (this.visible) {
			g.drawBitmap(bmp, this.world_bounds.left - cam.left + (Statics.SQ_SIZE/4), this.world_bounds.top - cam.top + (Statics.SQ_SIZE/4), paint);
		}
	}

	
	
}
