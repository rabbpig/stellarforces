package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.MyPointF;

import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

public class FlameThrowerEffect extends AbstractAreaEffectGun {
	
	public FlameThrowerEffect(GameModule _game, MyPointF pxl_start, MyPointF _pixel_end) {
		super(_game, "FlameThrowerEffect", R.drawable.flamethrower_blast, pxl_start, _pixel_end);

	}
	
	
	@Override
	protected void checkSquares() {
		// Do nothing
	}

}
