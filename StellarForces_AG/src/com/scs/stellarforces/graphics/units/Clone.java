package com.scs.stellarforces.graphics.units;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

import dsr.data.UnitData;

public class Clone extends AbstractUnit {

	public Clone(GameModule GameModule, UnitData unit) {
		super(GameModule, unit, unit.map_x * Statics.SQ_SIZE, unit.map_x * Statics.SQ_SIZE);

		if (unit.getSide() == 1) {
			bmps[0] = GameModule.img_cache.getImage(R.drawable.clone1_e, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			bmps[1] = GameModule.img_cache.getImage(R.drawable.clone1_se, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			bmps[2] = GameModule.img_cache.getImage(R.drawable.clone1_s, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			bmps[3] = GameModule.img_cache.getImage(R.drawable.clone1_sw, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			bmps[4] = GameModule.img_cache.getImage(R.drawable.clone1_w, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			bmps[5] = GameModule.img_cache.getImage(R.drawable.clone1_nw, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			bmps[6] = GameModule.img_cache.getImage(R.drawable.clone1_n, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			bmps[7] = GameModule.img_cache.getImage(R.drawable.clone1_ne, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
		} else if (unit.getSide() == 2) {
			bmps[0] = GameModule.img_cache.getImage(R.drawable.clone2_e, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[1] = GameModule.img_cache.getImage(R.drawable.clone2_se, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[2] = GameModule.img_cache.getImage(R.drawable.clone2_s, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[3] = GameModule.img_cache.getImage(R.drawable.clone2_sw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[4] = GameModule.img_cache.getImage(R.drawable.clone2_w, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[5] = GameModule.img_cache.getImage(R.drawable.clone2_nw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[6] = GameModule.img_cache.getImage(R.drawable.clone2_n, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[7] = GameModule.img_cache.getImage(R.drawable.clone2_ne, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
		} else if (unit.getSide() == 3) {
			bmps[0] = GameModule.img_cache.getImage(R.drawable.clone3_e, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[1] = GameModule.img_cache.getImage(R.drawable.clone3_se, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[2] = GameModule.img_cache.getImage(R.drawable.clone3_s, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[3] = GameModule.img_cache.getImage(R.drawable.clone3_sw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[4] = GameModule.img_cache.getImage(R.drawable.clone3_w, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[5] = GameModule.img_cache.getImage(R.drawable.clone3_nw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[6] = GameModule.img_cache.getImage(R.drawable.clone3_n, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[7] = GameModule.img_cache.getImage(R.drawable.clone3_ne, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
		} else if (unit.getSide() == 4) {
			bmps[0] = GameModule.img_cache.getImage(R.drawable.clone4_e, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[1] = GameModule.img_cache.getImage(R.drawable.clone4_se, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[2] = GameModule.img_cache.getImage(R.drawable.clone4_s, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[3] = GameModule.img_cache.getImage(R.drawable.clone4_sw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[4] = GameModule.img_cache.getImage(R.drawable.clone4_w, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[5] = GameModule.img_cache.getImage(R.drawable.clone4_nw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[6] = GameModule.img_cache.getImage(R.drawable.clone4_n, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmps[7] = GameModule.img_cache.getImage(R.drawable.clone4_ne, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
		} else {
			throw new RuntimeException("Unknown side: " + unit.getSide());
		}
	}
	
}

