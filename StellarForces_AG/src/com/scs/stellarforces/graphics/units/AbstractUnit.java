package com.scs.stellarforces.graphics.units;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.lib2d.Camera;
import ssmith.android.lib2d.MyPointF;
import ssmith.lang.GeometryFuncs;
import ssmith.lang.NumberFunctions;
import ssmith.util.PointByte;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.game.IProcessable;
import com.scs.stellarforces.game.TextureStateCache;
import com.scs.stellarforces.graphics.EnemyHighlighter;
import com.scs.stellarforces.graphics.GameObject;
import com.scs.stellarforces.main.lite.R;

import dsr.comms.UnitDataComms;
import dsr.data.AppletMapSquare;
import dsr.data.EquipmentData;
import dsr.data.UnitData;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitsTable;

public abstract class AbstractUnit extends GameObject implements IProcessable {

	private static Paint paint_unit_name = new Paint();

	public Bitmap bmps[] = new Bitmap[8];
	public Bitmap current_bmp = null;
	public UnitData unit_data;
	public static Bitmap[] unit_highlighters = new Bitmap[8];
	protected static final float UNIT_IMAGE_SIZE = Statics.SQ_SIZE * 1.2f;
	protected static final float SIZE_DIFF = UNIT_IMAGE_SIZE - Statics.SQ_SIZE;
	protected float centre_offset = 0f;
	private EnemyHighlighter enemy_highlighter;
	private static Bitmap bmp_on_fire;
	private Bitmap[] movement_icons;

	static {
		paint_unit_name.setARGB(255, 255, 255, 255);
		paint_unit_name.setAntiAlias(true);
		paint_unit_name.setTextSize(Statics.SCREEN_HEIGHT * 0.04f);
		if (Statics.tf != null) {
			paint_unit_name.setTypeface(Statics.tf);
		}

	}


	public static AbstractUnit Factory(GameModule game, UnitData unit) {
		if (unit_highlighters[0] == null || unit_highlighters[0].isRecycled()) {
			unit_highlighters[0] = game.img_cache.getImage(R.drawable.unit_highlighter_e, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			unit_highlighters[1] = game.img_cache.getImage(R.drawable.unit_highlighter_se, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			unit_highlighters[2] = game.img_cache.getImage(R.drawable.unit_highlighter_s, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			unit_highlighters[3] = game.img_cache.getImage(R.drawable.unit_highlighter_sw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			unit_highlighters[4] = game.img_cache.getImage(R.drawable.unit_highlighter_w, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			unit_highlighters[5] = game.img_cache.getImage(R.drawable.unit_highlighter_nw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			unit_highlighters[6] = game.img_cache.getImage(R.drawable.unit_highlighter_n, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			unit_highlighters[7] = game.img_cache.getImage(R.drawable.unit_highlighter_ne, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			bmp_on_fire = game.img_cache.getImage(R.drawable.fire, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
		}

		switch (unit.model_type) {
		case UnitsTable.MT_MALE_SIDE_1:
		case UnitsTable.MT_MALE_SIDE_2:
		case UnitsTable.MT_SCIENTIST:
		case UnitsTable.MT_MALE_SIDE_3:
		case UnitsTable.MT_MALE_SIDE_4:
			return new HumanUnit(game, unit);
		case UnitsTable.MT_QUEEN_ALIEN:
			return new AlienQueen(game, unit);
		case UnitsTable.MT_ALIEN_TYRANT:
			return new Alien(game, unit);
		case UnitsTable.MT_BLOB:
			return new Blob(game, unit);
		case UnitsTable.MT_CRAB:
			return new Crab(game, unit);
		case UnitsTable.MT_GHOUL:
			return new Ghoul(game, unit);
		case UnitsTable.MT_GHOUL_QUEEN:
			return new GhoulQueen(game, unit);
		case UnitsTable.MT_ZOMBIE:
			return new Zombie(game, unit);
		case UnitsTable.MT_CLONE:
			return new Clone(game, unit);
		case UnitsTable.MT_ANGEL:
			return new Angel(game, unit);
		default:
			throw new RuntimeException("Unknown model type: " + unit.model_type + ".  You need to update your client!");
		} 
	}


	protected AbstractUnit(GameModule _game, UnitData _unit, float x, float y) {
		super(_game, _unit.name, true, x, y, Statics.SQ_SIZE * 0.9f, Statics.SQ_SIZE * 0.9f, true, true);

		unit_data =_unit;

		//CreateBitmaps();

	}


	public void moveFwd() {
		Point p = GetDirFromAngle(this.unit_data.getAngle());
		this.move(p, 1);
	}


	public void moveBack() {
		Point p = GetDirFromAngle(this.unit_data.getAngle() + 180);
		this.move(p, 2f);
	}


	public void moveStrafeLeft() {
		Point p = GetDirFromAngle(this.unit_data.getAngle() - 90);
		this.move(p, 1.5f);
	}


	public void moveStrafeRight() {
		Point p = GetDirFromAngle(this.unit_data.getAngle() + 90);
		this.move(p, 1.5f);
	}


	public void turnBy(int ang) {
		game.root_cam.lookAt(this, false); // Look at us
		boolean no_aps = unit_data.model_type == UnitsTable.MT_BLOB;
		if (no_aps || this.unit_data.checkAndReduceAPs(game, 1)) {
			this.unit_data.turnBy(ang);

			game.updateUnitOnServer(this.unit_data);
			boolean who_can_see[] = game.whichSidesCanSeeUnit(this.unit_data);
			game.sendEventToServer_UnitMoved(this.unit_data, who_can_see);

			game.recalcVisibleEnemiesAndOppFire(true, this.unit_data);
			game.updateMenu();

			if (Statics.USE_NEW_MOVEMENT_ICONS == 1) {
				game.hud.new_movement.updateMovementIcons(this.unit_data);
			}
		}
	}


	public void anim_walk() {
	}


	public void anim_standStill() {
	}


	public AppletMapSquare getSquareInFrontOfUnit_MaybeNULL() {
		Point offset = GetDirFromAngle(this.unit_data.getAngle());
		Point new_pos = new Point(this.unit_data.getMapX() + offset.x, this.unit_data.getMapY() + offset.y);
		AppletMapSquare sq = this.game.mapdata.getSq_MaybeNULL(new_pos.x, new_pos.y);
		return sq;
	}


	public UnitData getUnitInFrontOfUnit_MaybeNULL() {
		Point offset = GetDirFromAngle(this.unit_data.getAngle());
		Point new_pos = new Point(this.unit_data.getMapX() + offset.x, this.unit_data.getMapY() + offset.y);
		UnitData unit = this.game.getUnitAt(new_pos.x, new_pos.y);
		return unit;
	}


	public AppletMapSquare getSquare_MaybeNULL() {
		AppletMapSquare sq = this.game.mapdata.getSq_MaybeNULL(this.unit_data.getMapX(), this.unit_data.getMapY());
		return sq;
	}


	private void move(Point offset, float ap_mult) {
		move(offset, ap_mult, false);
	}


	private void move(Point offset, float ap_mult, boolean stairs) {
		AbstractActivity act = Statics.act;
		
		game.root_cam.lookAt(this, false); // Look at us

		// Check we're not currently moving
		if (this.game.isObjectUpdating(this) == false) {
			// See if the square is empty
			PointByte new_pos = new PointByte(this.unit_data.getMapX() + offset.x, this.unit_data.getMapY() + offset.y);
			AppletMapSquare sq = this.game.mapdata.getSq_MaybeNULL(new_pos.x, new_pos.y);
			if (sq != null) { // In case walking off edge of map
				if (sq.texture_code == TextureStateCache.TEX_WATER) {
					if (this.unit_data.on_fire == 1) {
						this.unit_data.on_fire = 0;
						game.updateUnitOnServer(this.unit_data);
					}
				}
				boolean can_walk = (sq.major_type == MapDataTable.MT_FLOOR) && (sq.texture_code != TextureStateCache.TEX_WATER);
				if (can_walk == false) { // Double APs if walking through walls
					can_walk = can_walk || (sq.major_type == MapDataTable.MT_WALL && sq.texture_code == TextureStateCache.TEX_ALIEN_SKIN && game.game_data.mission_type == AbstractMission.ALIENS);
					if (can_walk) {
						ap_mult = ap_mult * 2;
					}
				}
				if (can_walk) {
					byte ap_cost = this.getAPCostToWalk(ap_mult, sq);
					UnitData opponent = game.getUnitAt(new_pos.x, new_pos.y);
					if (opponent != null) { // Inc cost for combat
						ap_cost = (byte)(ap_cost * 2);
					}
					if (stairs || this.unit_data.checkAndReduceAPs(game, ap_cost)) {
						if (opponent == null) {
							// Are they moving onto stairs?
							if (stairs == false && (sq.texture_code == TextureStateCache.TEX_STAIRS_UP || sq.texture_code == TextureStateCache.TEX_STAIRS_DOWN)) {
								sq = game.findEndStairsSquare(sq);
								if (sq != null) {
									this.move(new Point(sq.x-this.unit_data.map_x, sq.y-this.unit_data.map_y), ap_mult, true);
								}
							} else {
								this.unit_data.setTargetMapLocation(game, new_pos.x, new_pos.y, sq);
							}
							if (this.unit_data.model_type == UnitsTable.MT_BLOB) {
								act.playSound(R.raw.blobmove);
							} else {
								act.playSound(R.raw.footstep);
							}
							boolean who_can_see[] = game.whichSidesCanSeeUnit(this.unit_data);
							game.sendEventToServer_UnitMoved(this.unit_data, who_can_see);
						} else {
							boolean snafu_blocked = (game.game_data.is_snafu == 1 && game.game_data.snafu_will_opp_fire_on_side[opponent.getSide()] == 0);
							if (game.game_data.areSidesFriends(opponent.getSide(), this.unit_data.getSide()) || snafu_blocked) {
								if (game.game_data.areSidesFriends(opponent.getSide(), this.unit_data.getSide())) {
									// Is it a blob?
									if (this.unit_data.model_type == UnitsTable.MT_BLOB && opponent.model_type == UnitsTable.MT_BLOB) {
										// Merge!
										this.unit_data.incHealth(opponent.getHealth(), true);
										this.unit_data.combat_skill += opponent.combat_skill;
										this.unit_data.strength += opponent.strength;
										game.updateUnitOnServer(this.unit_data);

										// Remove the blob we moved into
										opponent.damage(game, opponent.getHealth(), this.unit_data, UnitDataComms.FOA_MERGED);
										return;
									} else {
										game.addToHUD(opponent.name + " is in the way.");
									}
								} else if (snafu_blocked) {
									game.addToHUD(opponent.name + " is not set as an enemy.");
								}
								// Re-add APs
								this.unit_data.incAPs(ap_cost);
								//game.updateUnitOnServer(this.unit_data);
							} else {
								game.closeCombat(this.unit_data, opponent);
							}
						}
					}
				} else if (sq.major_type == MapDataTable.MT_COMPUTER) {
					if (sq.destroyed == 0) {
						if (game.game_data.areSidesFriends(sq.owner_side, this.unit_data.getSide()) == false) {
							int ap_cost = this.getAPCostToWalk(ap_mult, sq) * 3;
							if (this.unit_data.checkAndReduceAPs(game, ap_cost)) {
								//game.updateUnitOnServer(this.unit_data);
								int str = this.unit_data.strength;
								if (this.unit_data.current_item != null) {
									str += this.unit_data.current_item.cc_damage;
								}
								//AppletMain.p("Str: " + str);
								int rnd = NumberFunctions.rnd(1, str);
								//AppletMain.p("Rnd: " + rnd);
								//AppletMain.p("Required: " + AppletMain.STRENGTH_TO_DESTROY_COMP);
								if (rnd >= GameModule.STRENGTH_TO_DESTROY_COMP) {
									game.computerDestroyed(sq, this.unit_data);
								} else {
									game.addToHUD("Computer not destroyed.");
								}
							}
						} else {
							// We own the computer!
							game.addToHUD("That is your computer!");
						}
					} else {
						// Computer already destroyed!
						game.addToHUD("The computer is already destroyed!");
					}
				} else {
					act.playSound(R.raw.bump1);
				}
				game.updateUnitOnServer(this.unit_data);
			}
		}
	}


	private byte getAPCostToWalk(float mult, AppletMapSquare sq) {
		// Calc AP cost
		byte ap_cost = 6;
		if (this.unit_data.getAngle() % 90 == 0) { // Move at 90 degrees
			ap_cost = 4;
		}
		if (sq.door_type > 0) {
			if (sq.door_open == false) {
				ap_cost = (byte)(ap_cost * 2);
			}
		}
		/* No, since they are not drawn!
		if (sq.scenery_code == MapDataTable.HEDGEROW || sq.scenery_code == MapDataTable.BRICK_WALL) {
			ap_cost = (byte)(ap_cost * 2);
		}*/
		ap_cost = (byte)((float)ap_cost * mult);
		return ap_cost;
	}


	private static Point tmp = new Point(0, 0);

	public static Point GetDirFromAngle(int ang_deg) {
		int ang = (int)GeometryFuncs.NormalizeAngle(ang_deg);
		switch (ang) {
		case 0:
			tmp.x = 1;
			tmp.y = 0;
			break;
		case 45:
			tmp.x = 1;
			tmp.y = 1;
			break;
		case 90:
			tmp.x = 0;
			tmp.y = 1;
			break;
		case 135:
			tmp.x = -1;
			tmp.y = 1;
			break;
		case 180:
			tmp.x = -1;
			tmp.y = 0;
			break;
		case 225:
			tmp.x = -1;
			tmp.y = -1;
			break;
		case 270:
			tmp.x = 0;
			tmp.y = -1;
			break;
		case 315:
			tmp.x = 1;
			tmp.y = -1;
			break;
		default:
			throw new RuntimeException("Unknown angle: " + ang_deg);
		}
		return tmp;
	}


	public void changeCurrentItem(EquipmentData equipment) {
		this.unit_data.current_item = equipment;
	}


	public void process(long interpolation) {
		MyPointF target_pos = new MyPointF(this.unit_data.getMapX() * Statics.SQ_SIZE, this.unit_data.getMapY() * Statics.SQ_SIZE);
		float dist_to_target = this.getWorldTopLeft_CreatesNew().distance(target_pos);
		float move_dist = Statics.UNIT_SPEED * interpolation;
		if (dist_to_target <= move_dist) {
			this.setLocation(unit_data.getMapX()*Statics.SQ_SIZE, unit_data.getMapY() * Statics.SQ_SIZE);
			this.updateGeometricState(); // to update world coords
			this.game.removeFromProcess(this);
			this.game.unitFinishedMoving(this.unit_data);
			this.anim_standStill();
		} else { // Keep moving!
			MyPointF move_dir = target_pos.subtract(this.getWorldTopLeft_CreatesNew()).normalize();
			float x_off = move_dir.x * move_dist;//Statics.UNIT_SPEED * interpolation);
			if (x_off > dist_to_target) {
				x_off = dist_to_target;
			}
			float y_off = move_dir.y * move_dist;//Statics.UNIT_SPEED * interpolation;
			if (y_off > dist_to_target) {
				y_off = dist_to_target;
			}
			this.adjustLocation(x_off, y_off);
			this.updateGeometricState();
			if (game.current_unit == this.unit_data) {
				game.root_cam.lookAt(this, false); // Look at us in case we're going through stairs
			}
		}
	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		if (this.visible) {
			// Draw the unit
			if (current_bmp != null) {
				g.drawBitmap(current_bmp, this.world_bounds.left - cam.left - SIZE_DIFF + centre_offset, this.world_bounds.top - cam.top - SIZE_DIFF + centre_offset, paint);
				if (this.game.game_data.num_players > 2) {
					if (this.unit_data.getSide() != this.game.game_data.our_side) {
						g.drawText(this.game.game_data.players_name_by_side[this.unit_data.getSide()], this.world_bounds.left - cam.left - SIZE_DIFF + centre_offset, this.world_bounds.top - cam.top - SIZE_DIFF + centre_offset, paint_unit_name);
					}
				}
			}
			if (this.unit_data.on_fire > 0) {
				g.drawBitmap(bmp_on_fire, this.world_bounds.left - cam.left - SIZE_DIFF + centre_offset, this.world_bounds.top - cam.top - SIZE_DIFF + centre_offset, paint);
			}
			// Draw the highlighter
			if (game.current_unit != null) {
				if (this == game.current_unit.model) {
					int img_num = game.current_unit.angle/45; // unit_highlighters[img_num].getWidth()
					g.drawBitmap(unit_highlighters[img_num], this.world_bounds.left - cam.left - SIZE_DIFF, this.world_bounds.top - cam.top - SIZE_DIFF, paint);
				}
			}
			if (game.game_data.areSidesFriends(game.game_data.our_side, this.unit_data.getSide()) == false) {
				if (enemy_highlighter == null) {
					enemy_highlighter = new EnemyHighlighter();
				}
				enemy_highlighter.doDraw(g, this.getWorldCentreX() - cam.left, this.getWorldCentreY() - cam.top);
			}
			/*if (Statics.DEBUG_GFX) {
				debug_rect.set(this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, this.world_bounds.left - cam.left + this.getWidth(), this.world_bounds.top - cam.top + this.getHeight());
				g.drawRect(debug_rect, Lib2DStatics.paint_red_line);
			}*/

			// Movement icons
			/*if (Statics.USE_NEW_MOVEMENT_ICONS) {
				if (this.game.current_unit == this.unit_data) { // Only draw if selected unit
					if (movement_icons == null) {
						this.updateMovementIconData();
					}
					drawMovementIcons(g, cam);
				}
			}*/
		}

	}


	/*private void updateMovementIconData() {
		movement_icons = new Bitmap[9];
		int dir = this.unit_data.angle/45;
		movement_icons[0] = null;
		movement_icons[1] = NewMovementIcons.bmp_arrow_up;
		movement_icons[2] = NewMovementIcons.bmp_arrow_up;
		movement_icons[3] = NewMovementIcons.bmp_arrow_up;
		//movement_icons[4] = NewMovementIcons.bmp_arrow_up;
		movement_icons[5] = NewMovementIcons.bmp_arrow_up;
		movement_icons[6] = NewMovementIcons.bmp_arrow_up;
		movement_icons[7] = NewMovementIcons.bmp_arrow_up;
		movement_icons[8] = NewMovementIcons.bmp_arrow_up;
		//movement_icons[9] = NewMovementIcons.bmp_arrow_up;
	}*/


}
