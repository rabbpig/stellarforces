package com.scs.stellarforces.graphics.units;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

import dsr.data.UnitData;

public class Zombie extends AbstractUnit {

	public Zombie(GameModule _game, UnitData unit) {
		super(_game, unit, unit.map_x * Statics.SQ_SIZE, unit.map_x * Statics.SQ_SIZE);

		bmps[0] = _game.img_cache.getImage(R.drawable.zombie_e, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
		bmps[1] = _game.img_cache.getImage(R.drawable.zombie_se, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
		bmps[2] = _game.img_cache.getImage(R.drawable.zombie_s, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
		bmps[3] = _game.img_cache.getImage(R.drawable.zombie_sw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
		bmps[4] = _game.img_cache.getImage(R.drawable.zombie_w, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
		bmps[5] = _game.img_cache.getImage(R.drawable.zombie_nw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
		bmps[6] = _game.img_cache.getImage(R.drawable.zombie_n, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
		bmps[7] = _game.img_cache.getImage(R.drawable.zombie_ne, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);

	}


}
