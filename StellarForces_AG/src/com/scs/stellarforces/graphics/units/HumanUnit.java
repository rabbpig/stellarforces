package com.scs.stellarforces.graphics.units;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

import dsr.data.UnitData;
import dsrwebserver.tables.UnitsTable;

public class HumanUnit extends AbstractUnit {

	public HumanUnit(GameModule _game, UnitData unit) {
		super(_game, unit, unit.map_x * Statics.SQ_SIZE, unit.map_x * Statics.SQ_SIZE);

		if (unit.model_type == UnitsTable.MT_SCIENTIST) {
			bmps[0] = _game.img_cache.getImage(R.drawable.sterner_e, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			bmps[1] = _game.img_cache.getImage(R.drawable.sterner_se, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			bmps[2] = _game.img_cache.getImage(R.drawable.sterner_s, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			bmps[3] = _game.img_cache.getImage(R.drawable.sterner_sw, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			bmps[4] = _game.img_cache.getImage(R.drawable.sterner_w, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			bmps[5] = _game.img_cache.getImage(R.drawable.sterner_nw, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			bmps[6] = _game.img_cache.getImage(R.drawable.sterner_n, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			bmps[7] = _game.img_cache.getImage(R.drawable.sterner_ne, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
		} else {
			if (unit.getSide() == 1) {
				bmps[0] = _game.img_cache.getImage(R.drawable.human_s1_e, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
				bmps[1] = _game.img_cache.getImage(R.drawable.human_s1_se, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
				bmps[2] = _game.img_cache.getImage(R.drawable.human_s1_s, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
				bmps[3] = _game.img_cache.getImage(R.drawable.human_s1_sw, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
				bmps[4] = _game.img_cache.getImage(R.drawable.human_s1_w, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
				bmps[5] = _game.img_cache.getImage(R.drawable.human_s1_nw, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
				bmps[6] = _game.img_cache.getImage(R.drawable.human_s1_n, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
				bmps[7] = _game.img_cache.getImage(R.drawable.human_s1_ne, UNIT_IMAGE_SIZE, UNIT_IMAGE_SIZE);
			} else if (unit.getSide() == 2) {
				bmps[0] = _game.img_cache.getImage(R.drawable.human_s2_e, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[1] = _game.img_cache.getImage(R.drawable.human_s2_se, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[2] = _game.img_cache.getImage(R.drawable.human_s2_s, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[3] = _game.img_cache.getImage(R.drawable.human_s2_sw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[4] = _game.img_cache.getImage(R.drawable.human_s2_w, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[5] = _game.img_cache.getImage(R.drawable.human_s2_nw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[6] = _game.img_cache.getImage(R.drawable.human_s2_n, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[7] = _game.img_cache.getImage(R.drawable.human_s2_ne, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			} else if (unit.getSide() == 3) {
				bmps[0] = _game.img_cache.getImage(R.drawable.human_s3_e, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[1] = _game.img_cache.getImage(R.drawable.human_s3_se, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[2] = _game.img_cache.getImage(R.drawable.human_s3_s, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[3] = _game.img_cache.getImage(R.drawable.human_s3_sw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[4] = _game.img_cache.getImage(R.drawable.human_s3_w, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[5] = _game.img_cache.getImage(R.drawable.human_s3_nw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[6] = _game.img_cache.getImage(R.drawable.human_s3_n, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[7] = _game.img_cache.getImage(R.drawable.human_s3_ne, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			} else if (unit.getSide() == 4) {
				bmps[0] = _game.img_cache.getImage(R.drawable.human_s4_e, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[1] = _game.img_cache.getImage(R.drawable.human_s4_se, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[2] = _game.img_cache.getImage(R.drawable.human_s4_s, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[3] = _game.img_cache.getImage(R.drawable.human_s4_sw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[4] = _game.img_cache.getImage(R.drawable.human_s4_w, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[5] = _game.img_cache.getImage(R.drawable.human_s4_nw, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[6] = _game.img_cache.getImage(R.drawable.human_s4_n, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
				bmps[7] = _game.img_cache.getImage(R.drawable.human_s4_ne, UNIT_IMAGE_SIZE,UNIT_IMAGE_SIZE);
			} else {
				throw new RuntimeException("Unknown side: " + unit.getSide());
			}
		}
	}


}
