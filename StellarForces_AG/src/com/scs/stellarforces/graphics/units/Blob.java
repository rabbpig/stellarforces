package com.scs.stellarforces.graphics.units;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

import dsr.data.UnitData;

public class Blob extends AbstractUnit {
	
	public Blob(GameModule _game, UnitData unit) {
		super(_game, unit, unit.map_x * Statics.SQ_SIZE, unit.map_x * Statics.SQ_SIZE);

		this.recreateBitmaps(_game, unit);
	}

	
	// BLobs bitmaps are dependent on their max health, starts at around 50-60
	public void recreateBitmaps(GameModule _game, UnitData unit) {
		float size2 = UNIT_IMAGE_SIZE;
		size2 = size2 * (unit.getMaxHealth() / 55f);
		if (size2 < UNIT_IMAGE_SIZE/4f) {
			size2 = UNIT_IMAGE_SIZE/4f;
		}
		if (size2 > UNIT_IMAGE_SIZE * 1.2f) {
			size2 = UNIT_IMAGE_SIZE * 1.2f;
		}
		int size = (int)size2;
		bmps[0] = _game.img_cache.getImage(R.drawable.blob, size, size);
		bmps[1] = _game.img_cache.getImage(R.drawable.blob, size, size);
		bmps[2] = _game.img_cache.getImage(R.drawable.blob, size, size);
		bmps[3] = _game.img_cache.getImage(R.drawable.blob, size, size);
		bmps[4] = _game.img_cache.getImage(R.drawable.blob, size, size);
		bmps[5] = _game.img_cache.getImage(R.drawable.blob, size, size);
		bmps[6] = _game.img_cache.getImage(R.drawable.blob, size, size);
		bmps[7] = _game.img_cache.getImage(R.drawable.blob, size, size);

		centre_offset = (UNIT_IMAGE_SIZE - size2)/2;
		
		if (this.current_bmp != null) { // Only call if we're in the middle of the game
			unit.updateAngleFromUnitData(); // To set current_bmp
		}
	}

}
