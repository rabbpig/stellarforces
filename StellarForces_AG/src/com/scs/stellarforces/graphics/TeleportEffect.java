package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import ssmith.android.lib2d.shapes.AbstractRectangle;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.graphics.units.AbstractUnit;

public class TeleportEffect extends AbstractRectangle {
	
	private static Paint _paintSimple, _paintBlur;
	//private float y_pos = 0;
	private AbstractUnit model;
	private long timer;
	
	static {
		_paintSimple = new Paint();
	    _paintSimple.setAntiAlias(true);
	    _paintSimple.setDither(true);
	    _paintSimple.setColor(Color.argb(248, 255, 255, 255));
	    _paintSimple.setStrokeWidth(Statics.SCREEN_HEIGHT * 0.04f);
	    _paintSimple.setStyle(Paint.Style.STROKE);
	    _paintSimple.setStrokeJoin(Paint.Join.ROUND);
	    _paintSimple.setStrokeCap(Paint.Cap.ROUND);

	    _paintBlur = new Paint();
	    _paintBlur.set(_paintSimple);
	    _paintBlur.setColor(Color.argb(235, 74, 138, 255));
	    _paintBlur.setStrokeWidth(Statics.SCREEN_HEIGHT * 0.07f);
	    _paintBlur.setMaskFilter(new BlurMaskFilter(Statics.SCREEN_HEIGHT * 0.02f, BlurMaskFilter.Blur.NORMAL)); 
	}
	
	
	public TeleportEffect(AbstractUnit _model) {
		super("TeleportEffect", _paintSimple, _model.getWorldX(), _model.getWorldY(), _model.getWorldX(), _model.getWorldY());
		
		this.model = _model;
		
		this.collides = false;
		
	}
	
	
	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		 g.drawLine(model.getWorldCentreX() - cam.left, 0, model.getWorldCentreX() - cam.left, model.getWorldY() - cam.top, _paintBlur);
		 g.drawLine(model.getWorldCentreX() - cam.left, 0, model.getWorldCentreX() - cam.left, model.getWorldY() - cam.top, _paintSimple);

		 timer += interpol;
		 
		 if (timer > 500) {
			 this.removeFromParent();
		 }
	}


}
