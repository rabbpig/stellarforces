package com.scs.stellarforces.graphics;

import android.graphics.Color;

public class SelectedUnitHighlighter extends AbstractUnitHighlighter {
	
	public SelectedUnitHighlighter() { //float cx, float cy) {
		super(Color.argb(248, 0, 255, 0), Color.argb(235, 74, 138, 255));
	}

}
