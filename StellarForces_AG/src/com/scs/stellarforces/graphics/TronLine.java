package com.scs.stellarforces.graphics;

import ssmith.android.lib2d.Camera;
import ssmith.android.lib2d.shapes.AbstractRectangle;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public class TronLine extends AbstractRectangle {
	
	private static Paint _paintSimple, _paintBlur;
	private float y_pos = 0;
	private boolean moving_down = true;
	
	static {
		_paintSimple = new Paint();
	    _paintSimple.setAntiAlias(true);
	    _paintSimple.setDither(true);
	    _paintSimple.setColor(Color.argb(248, 255, 255, 255));
	    _paintSimple.setStrokeWidth(Statics.SCREEN_HEIGHT * 0.01f);
	    _paintSimple.setStyle(Paint.Style.STROKE);
	    _paintSimple.setStrokeJoin(Paint.Join.ROUND);
	    _paintSimple.setStrokeCap(Paint.Cap.ROUND);

	    _paintBlur = new Paint();
	    _paintBlur.set(_paintSimple);
	    _paintBlur.setColor(Color.argb(235, 74, 138, 255));
	    _paintBlur.setStrokeWidth(Statics.SCREEN_HEIGHT * 0.05f);
	    _paintBlur.setMaskFilter(new BlurMaskFilter(Statics.SCREEN_HEIGHT * 0.02f, BlurMaskFilter.Blur.NORMAL)); 
	}
	

	public TronLine() {//String name, float sx, float sy, float ex, float ey, Paint paint) {
		super("TronLine", _paintSimple, 0, 0, Statics.SCREEN_WIDTH, Statics.SCREEN_HEIGHT);// {//name, sx, sy, ex, ey, paint);
		
		this.collides = false;
	}
	
	
	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		//super.doDraw(g, cam, interpol);
		
		 g.drawLine(0, y_pos, Statics.SCREEN_WIDTH, y_pos, _paintBlur);
		 g.drawLine(0, y_pos, Statics.SCREEN_WIDTH, y_pos, _paintSimple);

		 if (moving_down) {
			 y_pos += (interpol/10);
			 if (y_pos >= Statics.SCREEN_HEIGHT) {
				 moving_down = false;
			 }
		 } else {
			 y_pos -= (interpol/10);
			 if (y_pos <= 0) {
				 moving_down = true;
			 }
		 }
	}



}
