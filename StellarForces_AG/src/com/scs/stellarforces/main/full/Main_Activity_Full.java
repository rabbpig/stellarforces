/*
 * Copyright (c) 2009-2016 Stephen Carlyle-Smith (stephen.carlylesmith@googlemail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * * Neither the name of 'jMonkeyEngine' nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.scs.stellarforces.main.full;

import ssmith.android.framework.AbstractActivity;
import android.os.Bundle;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

/**
 * This is for initially loading the game.
 *
 */
public final class Main_Activity_Full extends AbstractActivity {

	public void onCreate(Bundle savedInstanceState) {
		Statics.FULL_VERSION = true;
		Statics.MARKET_URL_FREE_VERSION = "market://details?id=com.scs.stellarforces.main.lite";
		Statics.MARKET_URL_FULL_VERSION = "market://details?id=com.scs.stellarforces.main.full";
		Statics.URL_FOR_CLIENT = "http://www.stellarforces.com";
		Statics.EMAIL = "support@stellarforces.com";
		//Statics.PREFS_NAME = "sf_prefs.txt";
		Statics.BACKGROUND_R = R.drawable.space1;
		Statics.BUTTON_R = R.drawable.button_blue;
		Statics.BUTTON_R_OVAL = R.drawable.button_transp_oval;
		Statics.FREE_TEXT = "This is the free version - restricted to The Assassins and Moonbase Assault missions";

		super.onCreate(savedInstanceState);
	}

}
