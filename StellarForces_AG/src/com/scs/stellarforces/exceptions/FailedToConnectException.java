package com.scs.stellarforces.exceptions;

import java.io.IOException;

public class FailedToConnectException extends IOException {
	
	private static final long serialVersionUID = 1L;

	public FailedToConnectException() {
		super("Failed to connect to server");
	}

}
