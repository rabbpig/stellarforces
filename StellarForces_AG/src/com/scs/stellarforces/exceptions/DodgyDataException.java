package com.scs.stellarforces.exceptions;

public class DodgyDataException extends RuntimeException {
	
	public DodgyDataException(String s) {
		super(s);
	}

}
