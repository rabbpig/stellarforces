package com.scs.stellarforces;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import android.app.Application;

//@ReportsCrashes(formKey = "dE41R245ZmoxUDM0YWtwVERNd0FMQ1E6MQ") 
@ReportsCrashes(formUri = "http://www.rafakrotiri.info/services/acraservice.cls")
public class MyApplication extends Application {

	@Override
    public void onCreate() {
        // The following line triggers the initialization of ACRA
        ACRA.init(this);
        super.onCreate();
    }
	
}
