package com.scs.stellarforces.alert;

import java.util.ArrayList;

import ssmith.android.framework.ErrorReporter;
import ssmith.lang.NumberFunctions;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.exceptions.FailedToConnectException;
import com.scs.stellarforces.main.full.Main_Activity_Full;
import com.scs.stellarforces.main.lite.Main_Activity_Lite;
import com.scs.stellarforces.main.lite.R;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.PlayersGamesComms;
import dsr.comms.WGet_SF;
import dsr.data.GameData;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class NotificationsThread extends Thread {

	private UpdateService service;
	private static int err_count = 0;

	public NotificationsThread(UpdateService _service) {
		super("NotificationsThread");

		service = _service;

		start();
	}


	public void run() {
		// Connect to server and see if we have any turns
		try {
			if (Statics.CHECK_FOR_TURNS) {
				Statics.LoadPrefs(service.getBaseContext()); // To fill in LAST_LOGIN and LAST_PWD
				if (Statics.LAST_LOGIN.length() > 0 && Statics.LAST_PWD.length() > 0) {
					String s = PlayersGamesComms.GetPlayersGamesRequest(Statics.LAST_LOGIN, Statics.LAST_PWD);
					WGet_SF wget = new WGet_SF(Statics.act, null, s);
					if (wget.getResponseCode() == 200) {
						String response = wget.getResponse();
						ArrayList<GameData> games = PlayersGamesComms.DecodeGameDataResponse(response);
						int count = 0;
						for (GameData game : games) {
							if (game.mission_type != 45 && game.mission_type != 84) { // Ignore practise mission
								count++;
							}
						}
						if (count > 0) {
							String msg = "You have " + games.size() + " turn(s) to take.";
							createNotification(msg, games.size());
						}
					}

					// Check for messages
					WGet_SF wc = new WGet_SF(Statics.act, null, "cmd=" + MiscCommsPage.GET_NUM_UNREAD_MESSAGES + "&version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
					if (wc.getResponseCode() == 200) {
						String r = wc.getResponse();
						if (NumberFunctions.IsNumeric(r)) {
							int i = NumberFunctions.ParseInt(r);
							if (i > 0) {
								String msg = "You have " + i + " unread message(s)!";
								createNotification(msg, i);
							}
						}
					}
				}
			}
		} catch (FailedToConnectException ex) {
			// do nothing
		} catch (Exception ex) {
			err_count++;
			if (err_count > 3) {
				Statics.CHECK_FOR_TURNS = false;
				ErrorReporter.getInstance().handleSilentException(ex);
			}
		}
	}


	private void createNotification(String msg, int num) {
		Context context = service.getBaseContext();
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Service.NOTIFICATION_SERVICE);
		Intent contentIntent = null;
		if (Statics.FULL_VERSION) {
			contentIntent = new Intent(context, Main_Activity_Full.class);
		} else {
			contentIntent = new Intent(context, Main_Activity_Lite.class);
		}
		contentIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		Notification notification = new Notification(R.drawable.notification_icon, msg, System.currentTimeMillis());
		notification.setLatestEventInfo(context, "Stellar Forces", msg, PendingIntent.getActivity(context, 0, contentIntent, PendingIntent.FLAG_CANCEL_CURRENT));
		notification.flags = Notification.FLAG_AUTO_CANCEL;// | Notification.FLAG_FOREGROUND_SERVICE;// Notification.FLAG_FOREGROUND_SERVICE stops it dismissing!
		notification.number = num;
		notificationManager.notify(1, notification); // 1 = def

	}


}
