package com.scs.stellarforces.alert;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class UpdateOnReboot extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            Intent serviceIntent = new Intent("com.scs.stellarforces.alert.UpdateService");
            context.startService(serviceIntent);
        }
    }
}
