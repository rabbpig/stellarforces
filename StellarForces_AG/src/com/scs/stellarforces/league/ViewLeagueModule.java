package com.scs.stellarforces.league;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

import dsr.comms.DataTable;
import dsr.comms.OtherComms;
import dsr.comms.WGet_SF;

public class ViewLeagueModule extends SimpleScrollingAbstractModule { // This is currently unused
	
	private static DataTable league_data;

	private static Paint paint_normal_text = new Paint();
	private static Paint paint_team_name = new Paint();

	private VerticalFlowLayout vfl;

	static {
		paint_normal_text.setARGB(255, 0, 0, 0);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.SCREEN_WIDTH * 0.05f);

		paint_team_name.setARGB(255, 255, 255, 255);
		paint_team_name.setAntiAlias(true);
		paint_team_name.setTextSize(Statics.SCREEN_WIDTH * 0.07f);

	}


	public ViewLeagueModule(AbstractActivity act, AbstractModule _return_to) {
		super(-1);//, 1, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R, Statics.SCREEN_WIDTH*0.9f, Statics.SCREEN_HEIGHT/6), -1, false, "Select Game to Play", true);
		
		this.mod_return_to = _return_to;

		this.setBackground(Statics.BACKGROUND_R);

		//this.loadControls();
	}
	
	
	@Override
	public void started() {
		AbstractActivity act = Statics.act;
		
		if (league_data == null) {
			this.showPleaseWait("Getting league table..."); // showPleaseWait() sometimes gets stuck
			try {
				WGet_SF wc = new WGet_SF(act, null, OtherComms.GetLeagueTableRequest());
				String response = wc.getResponse();
				league_data = new DataTable(response);
				this.loadControls();
			} catch (Exception ex) {
				this.dismissPleaseWait();
				AbstractActivity.HandleError(ex);
			} finally {
				this.dismissPleaseWait();
			}
		}
	}


	private void loadControls() {
		this.root_node.removeAllChildren();

		final float GAP = Statics.SCREEN_WIDTH * 0.01f;

		vfl = new VerticalFlowLayout("vfl", GAP);

		Label lbl_team_name = new Label("lbl_team_name", "League Table", null, paint_team_name);
		vfl.attachChild(lbl_team_name);

		LeagueTableItemControl unit_ctrl = new LeagueTableItemControl("Name", "Pld", "Won", "Lost", "Pts", GAP);
		vfl.attachChild(unit_ctrl);
		
		league_data.moveBeforeFirst();
		int i=1;
		while (league_data.moveNext()) {
			String name = league_data.getString("Name");
			if (name.equalsIgnoreCase(Statics.DEF_LOGIN)) {
				name = "*" + name + "*"; 
			}
			unit_ctrl = new LeagueTableItemControl(i + " " + name, ""+league_data.getInt("Played"), ""+league_data.getInt("Won"), ""+league_data.getInt("Lost"), ""+league_data.getInt("Points"), GAP);
			vfl.attachChild(unit_ctrl);
			i++;
		}
		this.root_node.attachChild(vfl);

		this.root_node.updateGeometricState();
		this.root_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);

	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		// Do nothing
	}


}
