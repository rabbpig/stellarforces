package com.scs.stellarforces.league;

import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.layouts.HorizontalFlowGridLayout;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public class LeagueTableItemControl extends HorizontalFlowGridLayout {

	private static Paint paint_player_name = new Paint();
	private static Paint paint_pos_name = new Paint();

	static {
		paint_player_name.setARGB(255, 255, 255, 255);
		paint_player_name.setAntiAlias(true);
		paint_player_name.setTextSize(Statics.GetHeightScaled(0.07f));

		paint_pos_name.setARGB(255, 255, 255, 255);
		paint_pos_name.setAntiAlias(true);
		paint_pos_name.setTextSize(Statics.GetHeightScaled(0.06f));
	}


	public LeagueTableItemControl(String name, String played, String won, String lost, String pts, float GAP) {
		super("PlayerPositionControl", GAP);

		float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.12f;
		float ICON_HEIGHT = ICON_WIDTH * 0.6f;

		Label l = new Label("Unit_name", name, null, paint_player_name, false);
		l.setSize(Statics.SCREEN_WIDTH * 0.5f, ICON_HEIGHT);
		l.collides = false; // TO select unit stats
		this.attachChild(l);

		l = new Label("Unit_name", ""+played, null, paint_player_name, false);
		l.setSize(Statics.SCREEN_WIDTH * 0.1f, ICON_HEIGHT);
		l.collides = false; // TO select unit stats
		this.attachChild(l);

		l = new Label("Unit_name", ""+won, null, paint_player_name, false);
		l.setSize(Statics.SCREEN_WIDTH * 0.1f, ICON_HEIGHT);
		l.collides = false; // TO select unit stats
		this.attachChild(l);

		l = new Label("Unit_name", ""+lost, null, paint_player_name, false);
		l.setSize(Statics.SCREEN_WIDTH * 0.1f, ICON_HEIGHT);
		l.collides = false; // TO select unit stats
		this.attachChild(l);

		l = new Label("Unit_name", ""+pts, null, paint_player_name, false);
		l.setSize(Statics.SCREEN_WIDTH * 0.1f, ICON_HEIGHT);
		l.collides = false; // TO select unit stats
		this.attachChild(l);


		this.updateGeometricState(); // to get size

	}


}

