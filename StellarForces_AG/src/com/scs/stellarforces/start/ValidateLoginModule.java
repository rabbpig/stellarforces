package com.scs.stellarforces.start;

import java.io.IOException;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.ErrorReporter;
import ssmith.android.framework.modules.AbstractPleaseWaitModule;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.exceptions.FailedToConnectException;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class ValidateLoginModule extends AbstractPleaseWaitModule {

	public ValidateLoginModule(AbstractActivity _act) {
		super(_act, Statics.MOD_LOGIN);

		this.setBackground(Statics.BACKGROUND_R);

	}


	@Override
	public void started() {
		start();
	}


	public void run() {
		AbstractActivity act = Statics.act;
		
		try {
			//this.displayMessage("Connecting to server...");
			// Notice we allow repeats since we want the result
			WGet_SF wc = new WGet_SF(act, this, "cmd=" + MiscCommsPage.VALIDATE_LOGIN + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD), true);
			String response = wc.getResponse();
			if (AbstractCommFuncs.IsResponseGood(response)) {
				Statics.SavePrefs(act.getBaseContext());
				Statics.CHECK_FOR_TURNS = true;

				try {
					int tries = 3;
					while (tries > 0) {
						tries--;
						wc = new WGet_SF(act, this, "cmd=" + MiscCommsPage.GET_PLAYERS_DATA + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD), true);
						if (wc.getResponseCode() == 200) {
							response = wc.getResponse();
							Statics.players_data_tbl = new DataTable(response);
							Statics.players_data_tbl.moveFirst();
							break;
						}
					}
				} catch (FailedToConnectException ex) {
					// Do nothing
				} catch (Exception ex) {
					ErrorReporter.getInstance().handleSilentException(ex);
				}

				super.getThread().setNextModule(Statics.GetModule(Statics.MOD_MENU));
			} else {
				super.getThread().setNextModule(new ErrorModule(act, Statics.MOD_LOGIN, "Invalid Login/Password", "Sorry, that was an invalid or unknown login/password.\n\nIf you have a valid login/password but are unable to log in, please email support at " + Statics.EMAIL + "."));
			}

		} catch (IOException ex) {
			this.getThread().setNextModule(new ErrorModule(act, Statics.MOD_LOGIN, ex));
		}

	}


}
