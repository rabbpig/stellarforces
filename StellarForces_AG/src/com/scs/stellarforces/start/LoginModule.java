package com.scs.stellarforces.start;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.SimpleAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.GUIFunctions;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.PasswordField;
import ssmith.android.lib2d.gui.TextBox;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.text.InputType;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.graphics.TronLine;
import com.scs.stellarforces.main.lite.R;

public class LoginModule extends SimpleAbstractModule {
	
	private static final String LOGIN = "login";
	private static final String PWD = "pwd";

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * .8f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT/7;

	private Label lbl_login, lbl_pwd;
	private TextBox txt_login;
	private PasswordField txt_pwd;
	private Button cmd_ok;

	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();
	
	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		paint_large_text.setTextSize(GUIFunctions.GetTextSizeToFit("stephen.carlyles@googlemail.com", ICON_WIDTH, ICON_HEIGHT));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(paint_large_text.getTextSize() * 0.4f);
	}


	public LoginModule(AbstractActivity act) {
		super(Statics.MOD_START);

		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		stat_node.attachChild(new TronLine());

		Bitmap bmp = Statics.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT);
		lbl_login = new Label("Login", "Login or Email Address", ICON_WIDTH, ICON_HEIGHT, null, paint_large_text, false);
		txt_login = new TextBox("Login", "", null, paint_large_text, -1, bmp);
		lbl_pwd = new Label("Pwd", act.getString(R.string.password), ICON_WIDTH, ICON_HEIGHT, null, paint_large_text, false);
		txt_pwd = new PasswordField("Pwd", "", null, paint_large_text, -1, bmp);
		cmd_ok = new Button("OK", act.getString(R.string.ok), null, paint_large_text, bmp); 
		//Bitmap bmp2 = Statics.img_cache.getImage(R.drawable.button_red, ICON_WIDTH/2, ICON_HEIGHT);
		//cmd_forgotten = new Button("Forgotten", "Forgotten login/pwd?", null, paint_large_text, bmp2); 

		VerticalFlowLayout layout = new VerticalFlowLayout("Menu", 5);
		layout.attachChild(lbl_login);
		layout.attachChild(txt_login);
		layout.attachChild(lbl_pwd);
		layout.attachChild(txt_pwd);
		layout.attachChild(cmd_ok);
		//layout.attachChild(cmd_forgotten);

		// Check for wifi
		/*if (act.isWifiValid() == false) {
			Label lbl_help = new Label("Help", "Warning - No wifi found!", ICON_WIDTH, ICON_HEIGHT, null, paint_large_text, false);
			layout.attachChild(lbl_help);
		} else {
			Label lbl_help = new Label("Help", "WiFi connectivity found.", ICON_WIDTH, ICON_HEIGHT, null, paint_normal_text, false);
			layout.attachChild(lbl_help);
		}*/
		Label lbl_help = new Label("Help", "Forgotton login/password?  Just send an email to '" + Statics.EMAIL + "' with details.", ICON_WIDTH, ICON_HEIGHT, null, paint_normal_text, false);
		layout.attachChild(lbl_help);

		this.stat_node.attachChild(layout);
		this.stat_node.updateGeometricState();

		this.stat_cam.lookAt(layout, true);

		this.setBackground(Statics.BACKGROUND_R);

		if (Statics.USE_DEF_LOGIN) {
			txt_login.setText(Statics.DEF_LOGIN);
			txt_pwd.setText(Statics.DEF_PWD);
		} else {
			txt_login.setText(Statics.LAST_LOGIN);
			txt_pwd.setText(Statics.LAST_PWD);
		}

	}


	/**
	 * This is called when the module is activated.
	 */
	@Override
	public void started() {
		// Have we been passed any data?
		if (Statics.data.containsKey("login")) {
			this.txt_login.setText(Statics.data.get("login"));
		} else if (Statics.data.containsKey("pwd")) {
			this.txt_pwd.setText(Statics.data.get("pwd"));
		}
		Statics.data.clear();
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		if (c == txt_login) {
			showKeyboard_New(LOGIN, this.txt_login.getText(), false, InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		} else if (c == txt_pwd) {
			showKeyboard_New(PWD, this.txt_pwd.getText(), true, InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
		} else if (c == cmd_ok) {
			if (this.txt_login.getText().trim().length() == 0) {
				this.showToast("Please enter a login");
				return;
			}
			if (this.txt_pwd.getText().trim().length() == 0) {
				this.showToast("Please enter a password");
				return;
			}
			Statics.LAST_LOGIN = this.txt_login.getText().trim();
			Statics.LAST_PWD = this.txt_pwd.getText().trim();
			super.getThread().setNextModule(Statics.GetModule(Statics.MOD_VALIDATE_LOGIN));
		}
	}


	private void showKeyboard_New(String code, String curr_val, boolean mask, int hint) {
		AbstractActivity act = Statics.act;
		
		Statics.data.put("code", code);
		Statics.data.put("text", curr_val);
		act.showKeyboard(hint);
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			started(); // populate data
		}
	}
	
}
