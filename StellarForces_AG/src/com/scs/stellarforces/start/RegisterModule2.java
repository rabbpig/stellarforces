package com.scs.stellarforces.start;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.GUIFunctions;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.TextBox;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.text.InputType;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

public class RegisterModule2 extends SimpleScrollingAbstractModule {

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * .8f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT/7;

	private Label lbl_email, lbl_display_name, lbl_pwd;
	private TextBox txt_email, txt_pwd, txt_display_name;
	private Button cmd_ok;

	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(GUIFunctions.GetTextSizeToFit("stephen.carlylesmith@googlemail.com", ICON_WIDTH, ICON_HEIGHT));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		//paint_normal_text.setStyle(Style.STROKE);
		paint_normal_text.setTextSize(paint_large_text.getTextSize()/2);
	}


	public RegisterModule2(AbstractActivity act) {
		super(Statics.MOD_START);

		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		Bitmap bmp = Statics.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT);
		Bitmap bmp_green = Statics.img_cache.getImage(R.drawable.button_green, ICON_WIDTH, ICON_HEIGHT);

		VerticalFlowLayout layout = new VerticalFlowLayout("Menu", 5);

		lbl_display_name = new Label("DisplayName", act.getString(R.string.display_name), ICON_WIDTH, ICON_HEIGHT, null, paint_large_text, false);
		layout.attachChild(lbl_display_name);
		Label lbl_display_name2 = new Label("Email Address", "This is your name that will be used for logging in and also displayed", ICON_WIDTH, ICON_HEIGHT, null, paint_normal_text, false);
		layout.attachChild(lbl_display_name2);
		Label lbl_display_name3 = new Label("Email Address", "in the game and on the forums.", ICON_WIDTH, ICON_HEIGHT, null, paint_normal_text, false);
		layout.attachChild(lbl_display_name3);
		txt_display_name = new TextBox("DisplayName", Statics.data.get("display_name"), null, paint_large_text, -1, bmp);
		layout.attachChild(txt_display_name);

		lbl_email = new Label("Email Address", "Email Address (never shared)", ICON_WIDTH, ICON_HEIGHT, null, paint_large_text, false);
		layout.attachChild(lbl_email);
		Label lbl_email2 = new Label("Email Address", "Optional.  This is used for turn notifications and other game-related messages.", ICON_WIDTH, ICON_HEIGHT, null, paint_normal_text, false);
		layout.attachChild(lbl_email2);
		Label lbl_email3 = new Label("Email Address", "Email addresses are never shared with anyone.", ICON_WIDTH, ICON_HEIGHT, null, paint_normal_text, false);
		layout.attachChild(lbl_email3);
		txt_email = new TextBox("Email_Address", Statics.data.get("email_address"), null, paint_large_text, -1, bmp);
		
		lbl_pwd = new Label("Pwd", act.getString(R.string.password), ICON_WIDTH, ICON_HEIGHT, null, paint_large_text, false);
		layout.attachChild(lbl_pwd);
		txt_pwd = new TextBox("Pwd", Statics.data.get("pwd"), null, paint_large_text, -1, bmp);
		layout.attachChild(txt_pwd);

		cmd_ok = new Button("OK", "Register!", null, paint_large_text, bmp_green);
		layout.attachChild(cmd_ok);

		Label lbl_help = new Label("Help", "Once you have registered you will receive a welcome message", ICON_WIDTH, ICON_HEIGHT, null, paint_normal_text, false);
		layout.attachChild(lbl_help);
		Label lbl_help2 = new Label("Help", "and you can start playing immediately.", ICON_WIDTH, ICON_HEIGHT, null, paint_normal_text, false);
		layout.attachChild(lbl_help2);
		Label lbl_help3 = new Label("Help", "Your login and password can also be used in the forums.", ICON_WIDTH, ICON_HEIGHT, null, paint_normal_text, false);
		layout.attachChild(lbl_help3);

		this.root_node.attachChild(layout);
		this.root_node.updateGeometricState();

		this.root_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);

		this.setBackground(Statics.BACKGROUND_R);

	}


	/**
	 * This is called when the module is activated.
	 */
	@Override
	public void started() {
		if (Statics.data.containsKey("login")) {
			this.txt_email.setText(Statics.data.get("login"));
		} else if (Statics.data.containsKey("pwd")) {
			this.txt_pwd.setText(Statics.data.get("pwd"));
		} else if (Statics.data.containsKey("display_name")) {
			this.txt_display_name.setText(Statics.data.get("display_name"));
		}

		Statics.data.clear();
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		if (c == txt_email) {
			showKeyboard_New("login", this.txt_email.getText(), false, InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		} else if (c == txt_display_name) {
			showKeyboard_New("display_name", this.txt_display_name.getText(), false, InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
		} else if (c == txt_pwd) {
			showKeyboard_New("pwd", this.txt_pwd.getText(), false, InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
		} else if (c == cmd_ok) {
			// Validate data
			if (txt_email.getText().length() > 0 && txt_email.getText().indexOf("@") < 0) {
				this.showToast("Please enter a valid email address.");
				return;
			}
			if (txt_display_name.getText().length() == 0) {
				this.showToast("Please enter a display name.");
				return;
			}
			if (txt_pwd.getText().length() == 0) {
				this.showToast("Please enter a password.");
				return;
			}
			Statics.data.put("email_address", this.txt_email.getText());
			Statics.data.put("display_name", this.txt_display_name.getText());
			Statics.data.put("pwd", this.txt_pwd.getText());
			super.getThread().setNextModule(Statics.GetModule(Statics.MOD_REGISTER_NEW_LOGIN_WITH_SERVER));
		}
	}


	private void showKeyboard_New(String code, String curr_val, boolean mask, int hint) {
		AbstractActivity act = Statics.act;

		Statics.data.put("code", code);
		Statics.data.put("text", curr_val);
		act.showKeyboard(hint);
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			started(); // populate data
		}
	}

}

