package com.scs.stellarforces.start.missionintros;

import ssmith.android.framework.MyEvent;
import ssmith.android.framework.modules.AbstractModule;

public abstract class AbstractIntroModule extends AbstractModule {
	
	public AbstractIntroModule(AbstractModule mod) {
		super(-1);
		
		this.mod_return_to = mod;
	}


	@Override
	public boolean processEvent(MyEvent evt) throws Exception {
		super.returnTo();
		return true;
	}


}
