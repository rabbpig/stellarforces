package com.scs.stellarforces.start;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;

import com.scs.stellarforces.Statics;

public class FreeWarningModule extends SimpleAbstractModule {
	
	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.4f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT * 0.15f;

	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();
	
	private Button btn_show_full;

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.06f));

	}
	
	
	public FreeWarningModule(AbstractActivity act, AbstractModule _return_to) {
		this(act, -1);
		
		this.mod_return_to = _return_to;
	}
	
	
	public FreeWarningModule(AbstractActivity act, int mod) {
		super(mod);
		
		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.setBackground(Statics.BACKGROUND_R);

		String text = "";
		text = "Apologies, but this is the free version of Stellar Forces and is designed to give you a demonstration of the full game.  ";
		text = text + "To enable this option you will need the full version from Google Play.\n\nPress the screen to return.";

		showMsg("Mission Unavailable", text);
		
		btn_show_full = new Button("See Full Version", "See Full Version", 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT));
		btn_show_full.setLocation(Statics.SCREEN_WIDTH - ICON_WIDTH, Statics.SCREEN_HEIGHT -ICON_HEIGHT);
		this.stat_node.attachChild(btn_show_full);
		
		this.stat_node.updateGeometricState();
		

	}
	
	
	private void showMsg(String title, String text) {
		Label l = new Label("Title", title, 0, 0, null, paint_large_text, true);
		l.setCentre(Statics.SCREEN_WIDTH/2, paint_large_text.getTextSize());
		this.stat_node.attachChild(l);

		MultiLineLabel label2 = new MultiLineLabel("credits", text, null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
		label2.updateGeometricState();
		label2.setLocation(10, (Statics.SCREEN_HEIGHT/2)-(label2.getHeight()/2));
		stat_node.attachChild(label2);
		
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		AbstractActivity act = Statics.act;
		
		if (c == this.btn_show_full) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.MARKET_URL_FULL_VERSION));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);

		}
		
	}


	protected boolean noComponentPressed() {
		this.returnTo();
		return true;
	}

}

