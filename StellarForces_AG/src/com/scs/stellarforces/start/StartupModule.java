package com.scs.stellarforces.start;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.ErrorReporter;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.GUIFunctions;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.layouts.GridLayout;
import ssmith.android.lib2d.shapes.BitmapRectangle;
import ssmith.lang.Functions;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.graphics.TronLine;
import com.scs.stellarforces.main.lite.R;

import dsr.comms.AbstractCommFuncs;

public final class StartupModule extends SimpleAbstractModule {

	// Menu options
	private static String QUICKSTART, LOG_IN, ABOUT, REGISTER, FEEDBACK, VIEW_FULL_VERSION, OTHER_GAMES;
	private static final String MUTE = "Mute";

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH/3;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT/7;
	private static final float MUTE_SIZE = Statics.SCREEN_WIDTH/10;

	private Label website;
	private Button cmd_mute;
	private boolean running = true;
	private Label lbl_help = null;

	public static Paint paint_menu_text = new Paint();
	private static Paint paint_small_text = new Paint();
	private static Paint paint_warning_text = new Paint();

	static {
		paint_menu_text.setARGB(255, 255, 255, 255);
		paint_menu_text.setAntiAlias(true);
		paint_menu_text.setTextSize(GUIFunctions.GetTextSizeToFit("Practise Game", ICON_WIDTH, ICON_HEIGHT));
		/*if (Statics.tf != null) {
			paint_menu_text.setTypeface(Statics.tf);
		} else {
			Typeface tf = Typeface.createFromAsset(this.getApplicationContext().getAssets(), "fonts/JLSDataGothicC_NC.ttf");
			paint_menu_text.setTypeface(Statics.tf);
		}*/

		paint_small_text.setARGB(255, 255, 255, 255);
		paint_small_text.setAntiAlias(true);
		paint_small_text.setTextSize(GUIFunctions.GetTextSizeToFit(Statics.FREE_TEXT, Statics.SCREEN_WIDTH, -1));

		paint_warning_text.setARGB(255, 255, 0, 0);
		paint_warning_text.setAntiAlias(true);
		paint_warning_text.setTextSize(paint_small_text.getTextSize()*2);
	}


	public StartupModule(AbstractActivity act) {
		super(-1);

		if (Statics.GetTypeface(act) != null) {
			paint_menu_text.setTypeface(Statics.GetTypeface(act));
			paint_small_text.setTypeface(Statics.GetTypeface(act));
			paint_warning_text.setTypeface(Statics.GetTypeface(act));
		}

		LOG_IN = act.getString(R.string.log_in);
		ABOUT = act.getString(R.string.about);
		QUICKSTART = "QUICKSTART";
		REGISTER = act.getString(R.string.register);
		FEEDBACK = act.getString(R.string.feedback);
		VIEW_FULL_VERSION = "Full Version";
		OTHER_GAMES = "Other Games";

		this.setBackground(Statics.BACKGROUND_R);

		act.resumeMusic(act.getBaseContext());
	}


	@Override
	public void started() {
		AbstractActivity act = Statics.act;
		
		super.started();

		// Create initial menu
		this.stat_node.removeAllChildren();

		stat_node.attachChild(new TronLine());

		int r = R.drawable.sf_logo2_small;
		try {
			BitmapRectangle l_title = new BitmapRectangle("Title", Statics.img_cache.getImageByKey_HeightOnly(r, Statics.SCREEN_HEIGHT * 0.2f), 0, 0);
			l_title.setCentre(Statics.SCREEN_WIDTH/2, l_title.getHeight()*.6f);
			this.stat_node.attachChild(l_title);
		} catch (java.lang.OutOfMemoryError ex) {
			AbstractActivity.HandleError(ex);
		}

		if (Statics.FULL_VERSION == false) {
			Label free_msg = new Label("Free_version", Statics.FREE_TEXT, null, paint_small_text);
			//free_msg.setCentre(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT - (paint_small_text.getTextSize()*6.5f));
			free_msg.setCentre(Statics.SCREEN_WIDTH/2, (paint_small_text.getTextSize()*1f));
			stat_node.attachChild(free_msg);
		}

		GridLayout menu_node = new GridLayout("Menu", ICON_WIDTH, ICON_HEIGHT, Statics.GetButtonSpacing());
		menu_node.attachChild(new Button(LOG_IN, LOG_IN, null, paint_menu_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT)), 0, 0);
		menu_node.attachChild(new Button(REGISTER, REGISTER, null, paint_menu_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT)), 1, 0);
		if (Statics.FULL_VERSION == false || Statics.DEBUG) {
			menu_node.attachChild(new Button(QUICKSTART, QUICKSTART, null, paint_menu_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT)), 0, 1);
		} else {
			menu_node.attachChild(new Button(OTHER_GAMES, OTHER_GAMES, null, paint_menu_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT)), 0, 1);
		}
		menu_node.attachChild(new Button(ABOUT, ABOUT, null, paint_menu_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT)), 1, 1);
		menu_node.attachChild(new Button(FEEDBACK, FEEDBACK, null, paint_menu_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT)), 0, 2);
		if (Statics.FULL_VERSION == false) {
			menu_node.attachChild(new Button(VIEW_FULL_VERSION, VIEW_FULL_VERSION, null, paint_menu_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT)), 1, 2);
		}
		menu_node.updateGeometricState();
		menu_node.setCentre(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2);
		this.stat_node.attachChild(menu_node);

		Label label2 = new Label("Version", act.getString(R.string.version) + " " + Statics.VERSION_NUM, null, paint_small_text);
		label2.setLocation(5, Statics.SCREEN_HEIGHT - paint_small_text.getTextSize());
		stat_node.attachChild(label2);

		// Check for wifi
		lbl_help = new Label("Help", "Checking wifi...", ICON_WIDTH, ICON_HEIGHT, null, paint_small_text, false);
		lbl_help.setCentre(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT - (paint_small_text.getTextSize()*5f));
		stat_node.attachChild(lbl_help);

		website = new Label("Website", Statics.URL_FOR_CLIENT, null, paint_menu_text);
		website.setCentre(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT - (paint_small_text.getTextSize()*3));
		website.collides = true;
		stat_node.attachChild(website);

		Label prog_by = new Label("Programmed by Steve Smith", "Programmed by Steve Smith", null, paint_small_text);
		prog_by.setCentre(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT - paint_small_text.getTextSize());
		stat_node.attachChild(prog_by);

		showMuteIcon();

		stat_node.updateGeometricState();

		this.stat_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);

		running = true;
		start();


	}


	@Override
	public void stopped() {
		this.running = false;
	}


	public void handleClick(AbstractComponent b) throws Exception {
		AbstractActivity act = Statics.act;
		
		if (b == website) {
			Intent intent = null;
			if (Statics.LAST_LOGIN.length() > 0) {
				// Try and auto-login to website
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.URL_FOR_CLIENT + "/dsr/LoginPage.cls?login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD)));
			} else {
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.URL_FOR_CLIENT));
			}
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		} else {
			String cmd = b.getActionCommand().toString();
			if (cmd.length() > 0) { // In case it's a textbox or something
				//IOFunctions.Vibrate(this.act.getBaseContext(), Statics.VIBRATE_LEN);
				if (cmd.equalsIgnoreCase(LOG_IN)) {
					/*if (Statics.login_module == null) {
						Statics.login_module = new LoginModule(act, this.view, this);
					}*/
					super.getThread().setNextModule(new LoginModule(act));
				} else if (cmd.equalsIgnoreCase(ABOUT)) {
					super.getThread().setNextModule(new AboutModule(act, Statics.MOD_START));
				} else if (cmd.equalsIgnoreCase(REGISTER)) {
					super.getThread().setNextModule(Statics.GetModule(Statics.MOD_ENTER_NEW_LOGIN_FOR_REGISTER));
				} else if (cmd.equalsIgnoreCase(QUICKSTART)) {
					super.getThread().setNextModule(new QuickstartModule(act));
				} else if (cmd.equalsIgnoreCase(FEEDBACK)) {
					SendFeedback(act, this);
				} else if (cmd.equalsIgnoreCase(OTHER_GAMES)) {
					try {
						Context mContext = act.getBaseContext();
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:Penultimate Apps"));
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						mContext.startActivity(intent);
					} catch (Exception ex) {
						if (ex instanceof ActivityNotFoundException == false) {
							ErrorReporter.getInstance().handleSilentException(ex);
						}
						this.getThread().setNextModule(new ErrorModule(act, Statics.MOD_START, "Unable to view Play", "Sorry, your phone does not seem to be able to view Google Play this way.  Please use the Google Play app."));
					}
				} else if (cmd.equalsIgnoreCase(VIEW_FULL_VERSION)) {
					try {
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.MARKET_URL_FULL_VERSION));
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						act.getBaseContext().startActivity(intent);
					} catch (Exception ex) {
						if (ex instanceof ActivityNotFoundException == false) {
							ErrorReporter.getInstance().handleSilentException(ex);
						}
						this.getThread().setNextModule(new ErrorModule(act, Statics.MOD_START, "Unable to view Play", "Sorry, your phone does not seem to be able to view Google Play this way.  Please use the Google Play app."));
					}
				} else if (cmd.equalsIgnoreCase(MUTE)) {
					if (Statics.MUTE_MUSIC == 0) {
						Statics.MUTE_MUSIC = 1;
						Statics.MUTE_SFX = 1;
						act.pauseMusic();
					} else {
						Statics.MUTE_MUSIC = 0;
						Statics.MUTE_SFX = 0;
						act.resumeMusic(act.getBaseContext());
					}
					Statics.SavePrefs(act);
					this.showMuteIcon();
				} else {
					throw new RuntimeException("Unknown command: '" + cmd + "'");
				}
			}
		}

	}


	public static void SendFeedback(AbstractActivity act, AbstractModule mod) {
		try {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			String subject = Statics.NAME + " Feedback".replaceAll(" ", "%20");
			Uri data = Uri.parse("mailto:" + Statics.EMAIL + "?subject=" + subject);
			intent.setData(data);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		} catch (ActivityNotFoundException ex) {
			//ErrorReporter.getInstance().handleSilentException(ex);
			mod.getThread().setNextModule(new ErrorModule(act, Statics.MOD_START, "Unable to Send Email", "Sorry, your phone does not seem to allow sending of an email via this app.  Please send feedback manually to " + Statics.EMAIL + "."));
		}
	}


	public void run() {
		AbstractActivity act = Statics.act;
		
		while (running) {
			if (act.isWifiValid() == false) {
				lbl_help.setText("WARNING - NO WIFI FOUND!!");
			} else {
				lbl_help.setText("WiFi connectivity found.");
			}
			Functions.delay(10 * 1000);
		}

	}


	@Override
	public boolean onBackPressed() {
		AbstractActivity act = Statics.act;
		
		act.finish();
		return true;
	}


	private void showMuteIcon() {
		if (cmd_mute != null) {
			this.cmd_mute.removeFromParent();
		}
		if (Statics.MUTE_MUSIC == 0) {
			cmd_mute = new Button(MUTE, "", null, null, Statics.img_cache.getImage(R.drawable.sound_on_icon, MUTE_SIZE, MUTE_SIZE));
		} else {
			cmd_mute = new Button(MUTE, "", null, null, Statics.img_cache.getImage(R.drawable.sound_off_icon, MUTE_SIZE, MUTE_SIZE));
		}
		cmd_mute.setLocation(Statics.SCREEN_WIDTH - MUTE_SIZE, Statics.SCREEN_HEIGHT - MUTE_SIZE);
		this.stat_node.attachChild(cmd_mute);
		this.stat_node.updateGeometricState();

	}
}

