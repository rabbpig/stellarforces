package com.scs.stellarforces.start;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

public final class AboutModule extends SimpleScrollingAbstractModule {
	
	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));
	}
	
	
	public AboutModule(AbstractActivity act, int _return_to) {
		super(_return_to);
		
		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.setBackground(Statics.BACKGROUND_R);

		Label l = new Label("Credits_title", act.getString(R.string.about) + " " + Statics.NAME, null, paint_large_text);
		l.setCentre(Statics.SCREEN_WIDTH/2, paint_large_text.getTextSize());
		this.root_node.attachChild(l);
		
		StringBuffer str = new StringBuffer();
			str.append(Statics.NAME + " " + act.getString(R.string.about_1) + "\n\n");
			str.append(act.getString(R.string.about_2, Statics.EMAIL) + "\n\n");
			str.append(act.getString(R.string.about_3) + "\n\n");
			str.append("Press Back to return.\n\n");

		MultiLineLabel label2 = new MultiLineLabel("credits", str.toString(), null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
		label2.setLocation(10, paint_large_text.getTextSize()*2);//this.view.canvas_width/2,this.view.canvas_height/2);
		root_node.attachChild(label2);
		
		this.root_node.updateGeometricState();

	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		// Do nothing
		
	}


	public void noComponentClicked() {
		this.returnTo();
	}

	

}
