package com.scs.stellarforces.start;

import java.io.IOException;
import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractPleaseWaitModule;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;

import dsr.comms.PlayersGamesComms;
import dsr.comms.WGet_SF;
import dsr.data.GameData;

public final class GetGamesModule extends AbstractPleaseWaitModule {
	
	private boolean auto_select;
	
	public GetGamesModule(AbstractActivity _act, boolean _auto_select) { //, AbstractModule return_to) {
		super(_act, Statics.MOD_MENU);
		
		auto_select = _auto_select;

		this.setBackground(Statics.BACKGROUND_R);
		
		//this.mod_return_to = return_to;

	}


	@Override
	public void started() {
		AbstractActivity act = Statics.act;
		
		start();

		act.resumeMusic(act.getBaseContext()); // In case we've come back from a game

		if (GameModule.img_cache != null) {
			GameModule.img_cache.clear(); // We can clear it now as it's not being used.
		}
	}


	public void run() {
		AbstractActivity act = Statics.act;
		
		try {
			String s = PlayersGamesComms.GetPlayersGamesRequest(Statics.LAST_LOGIN, Statics.LAST_PWD);
			WGet_SF wget = new WGet_SF(act, this, s);
			if (wget.getResponseCode() == 200 || wget.getResponseCode() == -1) {
				String response = wget.getResponse();
				ArrayList<GameData> games = PlayersGamesComms.DecodeGameDataResponse(response);

				super.getThread().setNextModule(new ListGamesModule(act, games, auto_select));//, true));//Statics.GetModule(Statics.MOD_LIST_GAMES));
			} else {
				super.getThread().setNextModule(new ErrorModule(act, Statics.MOD_MENU, "Error Getting Games", "Apologies, there was an error getting your current games."));//, true));//Statics.GetModule(Statics.MOD_LIST_GAMES));
			}
		} catch (IOException ex) {
			AbstractActivity.HandleError(ex);
		}

	}


}
