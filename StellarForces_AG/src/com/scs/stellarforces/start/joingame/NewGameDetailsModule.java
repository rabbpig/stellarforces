package com.scs.stellarforces.start.joingame;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import ssmith.android.lib2d.layouts.HorizontalFlowGridLayout;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import ssmith.lang.NumberFunctions;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.start.GetGamesModule;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class NewGameDetailsModule extends SimpleScrollingAbstractModule {

	private int reqid; 
	private DataTable new_games;
	private Button[] cmd_accept;
	private Button cmd_show_mission;

	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		///paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.1f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));

	}


	public NewGameDetailsModule(AbstractActivity act, AbstractModule ret, int _reqid, DataTable _new_games) {
		super(-1);

		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.mod_return_to = ret;

		this.reqid = _reqid;
		this.new_games = _new_games;

		new_games.find("GameRequestID", reqid);

		this.setBackground(Statics.BACKGROUND_R);
	}


	@Override
	public void started() {
		this.showControls();
	}


	private void showControls() {
		Label l = new Label("Title", "Game Details", 0, 0, null, paint_large_text, true);
		l.setCentre(Statics.SCREEN_WIDTH/2, paint_large_text.getTextSize());
		this.root_node.attachChild(l);

		VerticalFlowLayout vfl = new VerticalFlowLayout("vfl", Statics.SCREEN_HEIGHT * 0.05f);
		vfl.setLocation(0,  Statics.SCREEN_HEIGHT * 0.2f);
		StringBuffer str = new StringBuffer();
		if (new_games.containsColumn("ForUs") && new_games.getString("ForUs").equalsIgnoreCase("1")) {
			str.append("This game has been specifically created with you as an opponent.  Only you can join it!\n\n");
		}
		str.append("Mission: " + new_games.getString("MissionName") + "\n");
		str.append("Creator: " + new_games.getString("Player1Name") + "\n");
		str.append("Sides: " + new_games.getString("NumSides") + "\n");
		if (new_games.getString("Comment").length() > 0) {
			str.append("Comment: " + new_games.getString("Comment") + "\n");
		}
		if (new_games.getInt("GameType") == 1) {
			str.append("Is a Practise game\n");
		} else {
		str.append("Points for Win: " + new_games.getInt("PointsForWin") + "\n");
		str.append("Points for Lose: " + new_games.getInt("PointsForLose") + "\n");
		}
		if (new_games.getInt("Advanced") == 0) {
			//str.append("Is NOT advanced mode\n");
		} else {
			str.append("Is ADVANCED mode\n");
		}
		String side_names[] = new_games.getString("SideNames").split(",", -1);
		for (int s=1 ; s<= new_games.getInt("NumSides") ; s++) {
			str.append("Side " + s + ": " + side_names[s-1] + "\n");
		}
		MultiLineLabel label2 = new MultiLineLabel("credits", str.toString(), null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
		label2.setLocation(10, Statics.SCREEN_HEIGHT * 0.15f);
		vfl.attachChild(label2);
		label2.updateGeometricState();

		cmd_show_mission = new Button("View\nMission\nDetails", null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R, Statics.SCREEN_WIDTH * 0.33f, Statics.SCREEN_HEIGHT*.4f));
		cmd_show_mission.setLocation(Statics.SCREEN_WIDTH*0.6f, Statics.SCREEN_HEIGHT * 0.25f);
		root_node.attachChild(cmd_show_mission);

		HorizontalFlowGridLayout hfl = new HorizontalFlowGridLayout("hfl", Statics.SCREEN_WIDTH * 0.005f);
		String sides[] = new_games.getString("SidesAvailable").split(",", -1);
		cmd_accept = new Button[sides.length];
		float width = (Statics.SCREEN_WIDTH * 0.9f) / (sides.length);
		float height = Statics.SCREEN_HEIGHT / 7f;
		for (int b=0 ; b<sides.length ; b++) {
			cmd_accept[b] = new Button(sides[b], "Select Side " + sides[b], null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R, width, height));
			hfl.attachChild(cmd_accept[b]);
		}
		vfl.attachChild(hfl);

		this.root_node.attachChild(vfl);
		this.root_node.updateGeometricState();

		this.root_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		AbstractActivity act = Statics.act;
		
		try {
			if (c instanceof Button) {
				//IOFunctions.Vibrate(this.act.getBaseContext(), Statics.VIBRATE_LEN);
				if (c == cmd_show_mission) {
					String url = Statics.URL_FOR_CLIENT + "/dsr/missiondescriptions.cls?type=" + new_games.getInt("MissionType") + "&android_login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&android_pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD); 
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					act.getBaseContext().startActivity(intent);
				} else {
					this.showPleaseWait("Please Wait");
					Button b = (Button) c;
					String tmp = b.getActionCommand();
					int side = NumberFunctions.ParseInt(tmp);
					String cmd = "cmd=" + MiscCommsPage.JOIN_NEW_GAME + "&version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD) + "&reqid=" + new_games.getInt("GameRequestID") + "&side=" + side;
					WGet_SF wget = new WGet_SF(act, null, cmd);
					String response = wget.getResponse();
					this.dismissPleaseWait();
					if (response.length() == 0 || response.equalsIgnoreCase("ok")) { // Success
						this.getThread().setNextModule(new GetGamesModule(act, true));
					} else {
						throw new RuntimeException(response);
					}
				}
			}
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
		}
	}

}
