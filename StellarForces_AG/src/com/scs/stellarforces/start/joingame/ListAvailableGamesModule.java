package com.scs.stellarforces.start.joingame;

import java.io.IOException;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.AbstractOptionsModule2;
import ssmith.android.lib2d.gui.GUIFunctions;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.start.GenericMessageModule;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class ListAvailableGamesModule extends AbstractOptionsModule2 {

	private static Paint paint_text = new Paint();
	private static Paint paint_free_text = new Paint();
	private static Paint paint_title = new Paint();

	private DataTable new_games;

	static {
		paint_text.setARGB(255, 255, 255, 255);
		paint_text.setAntiAlias(true);

		paint_free_text.setARGB(255, 200, 200, 200);
		paint_free_text.setAntiAlias(true);
		paint_free_text.setTextSize(GUIFunctions.GetTextSizeToFit(Statics.FREE_TEXT, Statics.SCREEN_WIDTH, -1));

		paint_title.setARGB(255, 200, 200, 200);
		paint_title.setAntiAlias(true);
		paint_title.setTextSize(GUIFunctions.GetTextSizeToFit(Statics.FREE_TEXT, Statics.SCREEN_WIDTH, -1));
	}


	public ListAvailableGamesModule(AbstractActivity act) {
		super(act, Statics.MOD_START_OR_JOIN_GAME, 1, paint_text, Statics.img_cache.getImage(Statics.BUTTON_R, Statics.SCREEN_WIDTH*0.9f, Statics.SCREEN_HEIGHT/6), -1, false, (Statics.FULL_VERSION==false ? "Please join any game" : "Available Games"), true);

		if (Statics.GetTypeface(act) != null) {
			paint_text.setTypeface(Statics.GetTypeface(act));
			paint_free_text.setTypeface(Statics.GetTypeface(act));
			paint_title.setTypeface(Statics.GetTypeface(act));
		}

		this.setBackground(Statics.BACKGROUND_R);

	}


	@Override
	public void getOptions() {
		AbstractActivity act = Statics.act;
		
		this.showPleaseWait("Getting available games...");
		try{
			String cmd = "cmd=" + MiscCommsPage.GET_NEW_GAMES_LIST + "&comms_version=" + Statics.COMMS_VERSION + "&android_version=" + Statics.VERSION_NUM + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD);
			WGet_SF wget = new WGet_SF(act, null, cmd);
			String response = wget.getResponse();
			new_games = new DataTable(response);

			while (new_games.moveNext()) {
				if (new_games.containsColumn("ForUs") && new_games.getString("ForUs").equalsIgnoreCase("1")) {
					addOption(new_games.getString("Player1Name") + " is waiting for YOU!", new_games.getInt("GameRequestID"));
				} else {
					addOption(new_games.getString("MissionName") + " (vs. " + new_games.getString("Player1Name") + ")", new_games.getInt("GameRequestID"));
				}
			}
			if (areThereAnyOptions() == false) {
				this.getThread().setNextModule(new GenericMessageModule(act, Statics.MOD_START_NEW_GAME, "No Available Games", "Sorry, there are currently no games waiting for players.\n\nPress the screen to start a new game."));
			}
		} catch (IOException ex) {
			this.showToast("Error getting games.");
			this.returnTo();
		} finally {
			this.dismissPleaseWait();
		}
	}


	@Override
	public void optionSelected(int idx) {
		AbstractActivity act = Statics.act;
		
		String cmd = this.getActionCommand(idx);
		if (this.new_games.find("GameRequestID", cmd)) {
			AbstractModule mod = null;
			if (Statics.FULL_VERSION || AbstractMission.allowed_mission.contains(new_games.getInt("MissionType"))) {
				mod = new NewGameDetailsModule(act, this, this.new_games.getInt("GameRequestID"), new_games);
			} else {
				mod = Statics.GetFreeWarningModule(this);
			}
			this.getThread().setNextModule(mod);
		}
	}


	@Override
	public void doDraw(Canvas c, long interpol) {
		super.doDraw(c, interpol);

		if (Statics.FULL_VERSION == false) {
			c.drawText(Statics.FREE_TEXT, Statics.SCREEN_WIDTH * 0.05f, Statics.SCREEN_HEIGHT - (paint_title.getTextSize()*2), paint_title);
		}
	}


}
