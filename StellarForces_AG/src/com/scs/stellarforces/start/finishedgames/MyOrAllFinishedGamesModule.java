package com.scs.stellarforces.start.finishedgames;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractOptionsModule2;
import ssmith.android.lib2d.gui.GUIFunctions;
import android.graphics.Paint;
import android.graphics.Paint.Style;

import com.scs.stellarforces.Statics;

public class MyOrAllFinishedGamesModule extends AbstractOptionsModule2 {
	
	private static final String MY_GAMES = "My Finished Games";
	private static final String ALL_GAMES = "All Finished Games";

	private static Paint paint_text = new Paint();
	private static Paint paint_title = new Paint();
	private static Paint paint_background = new Paint();

	static {
		paint_text.setARGB(255, 255, 255, 255);
		paint_text.setAntiAlias(true);
		//paint_text.setStyle(Style.STROKE);
		paint_text.setTextSize(Statics.SCREEN_WIDTH * 0.04f);

		paint_background.setARGB(255, 100, 100, 100);
		paint_background.setAntiAlias(true);
		paint_background.setStyle(Style.FILL);

		paint_title.setARGB(255, 200, 200, 200);
		paint_title.setAntiAlias(true);
		paint_title.setTextSize(GUIFunctions.GetTextSizeToFit(Statics.FREE_TEXT, Statics.SCREEN_WIDTH, -1));

	}


	public MyOrAllFinishedGamesModule(AbstractActivity act) {
		super(act, Statics.MOD_MORE, 2, paint_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, Statics.SCREEN_WIDTH/3, Statics.SCREEN_WIDTH/7), -1, false, "Select Which Games", false);

		if (Statics.GetTypeface(act) != null) {
			paint_text.setTypeface(Statics.GetTypeface(act));
			paint_title.setTypeface(Statics.GetTypeface(act));
		}

		this.setBackground(Statics.BACKGROUND_R);

	}


	/*@Override
	public void started() {
		// Get the list of games
		if (games == null) {
			this.showPleaseWait("Getting games...");
			try {
				WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.GET_FINISHED_GAME_LIST + "&login=" + CommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + CommFuncs.URLEncodeString(Statics.LAST_PWD));
				String response = wc.getResponse();
				if (response != null && response.length() > 0) {
					games = new DataTable(response);
				} else {
					throw new IOException("Got response " + wc.getResponseCode());
				}
			} catch (Exception ex) {
				AbstractActivity.HandleError(ex);
				return;
			} finally {
				this.dismissPleaseWait();
			}
		}

	}*/


	@Override
	public void getOptions() {
		this.addOption(MY_GAMES, MY_GAMES);
		this.addOption(ALL_GAMES, ALL_GAMES);
	}


	@Override
	public void optionSelected(int idx) {
		AbstractActivity act = Statics.act;
		
		this.getThread().setNextModule(new FinishedGamesModule(act, idx == 0));
	}


}
