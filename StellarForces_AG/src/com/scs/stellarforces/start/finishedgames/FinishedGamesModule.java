package com.scs.stellarforces.start.finishedgames;

import java.io.IOException;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractMultilineOptionsModule2;
import ssmith.android.lib2d.gui.GUIFunctions;
import android.graphics.Paint;
import android.graphics.Paint.Style;

import com.scs.stellarforces.Statics;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class FinishedGamesModule extends AbstractMultilineOptionsModule2 {

	private static Paint paint_text = new Paint();
	private static Paint paint_title = new Paint();
	private static Paint paint_background = new Paint();

	static {
		paint_text.setARGB(255, 255, 255, 255);
		paint_text.setAntiAlias(true);
		//paint_text.setStyle(Style.STROKE);
		paint_text.setTextSize(Statics.SCREEN_WIDTH * 0.04f);

		paint_background.setARGB(255, 100, 100, 100);
		paint_background.setAntiAlias(true);
		paint_background.setStyle(Style.FILL);

		paint_title.setARGB(255, 200, 200, 200);
		paint_title.setAntiAlias(true);
		//paint_title.setStyle(Style.STROKE);
		paint_title.setTextSize(GUIFunctions.GetTextSizeToFit(Statics.FREE_TEXT, Statics.SCREEN_WIDTH, -1));

	}
	
	
	private boolean only_mine;
	private DataTable games;

	public FinishedGamesModule(AbstractActivity act, boolean _only_mine) {
		super(act, Statics.MOD_MORE, paint_background, paint_text, -1, false, "Select Game to View", Statics.SCREEN_WIDTH * 0.9f, true);

		only_mine = _only_mine;
		
		if (Statics.GetTypeface(act) != null) {
			paint_text.setTypeface(Statics.GetTypeface(act));
			paint_title.setTypeface(Statics.GetTypeface(act));
		}

		this.setBackground(Statics.BACKGROUND_R);

	}


	@Override
	public void started() {
		AbstractActivity act = Statics.act;
		
		// Get the list of games
		if (games == null) {
			this.showPleaseWait("Getting games...");
			try {
				String cmd = "";
				if (only_mine) {
					cmd = MiscCommsPage.GET_FINISHED_GAME_LIST;
				} else {
					cmd = MiscCommsPage.GET_ALL_FINISHED_GAME_LIST;
				}
				WGet_SF wc = new WGet_SF(act, null, "cmd=" + cmd + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
				String response = wc.getResponse();
				if (response != null && response.length() > 0) {
					games = new DataTable(response);
				} else {
					throw new IOException("Got response " + wc.getResponseCode());
				}
			} catch (Exception ex) {
				AbstractActivity.HandleError(ex);
				return;
			} finally {
				this.dismissPleaseWait();
			}
		}

	}


	@Override
	public void getOptions() {
		this.addOption("Enter Game Number", "-1");
		if (games != null) {
			if (games.size() > 0) {
				games.moveBeforeFirst();
				while (games.moveNext()) {
					String name = games.getString("Name");
					this.addOption(name, games.getInt("GameID"));
				}
			} else {
				this.setTitle("You have not finished any games yet.");
			}
		}
	}


	@Override
	public void optionSelected(int idx) {
		AbstractActivity act = Statics.act;
		
		if (idx > 0) {
			int gameid = Integer.parseInt(super.getActionCommand(idx));
			games.find("GameID", gameid);
			this.getThread().setNextModule(new FinishedGameDetailsModule(act, this, games));
		} else {
			this.getThread().setNextModule(new ManualGameEntryModule(this));
		}
	}


}
