package com.scs.stellarforces.start.finishedgames;

import java.io.IOException;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import ssmith.android.lib2d.layouts.GridLayout;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.playback.PlaybackModule;
import com.scs.stellarforces.start.GenericMessageModule;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class FinishedGameDetailsModule extends SimpleScrollingAbstractModule { 

	private static final String CMD_WATCH_PLAYBACK = "Watch Playback";
	private static final String CMD_GAME_CHAT = "Game Chat";
	private static final String CMD_REMATCH_SAME_SIDES = "Rematch - Same Sides";
	private static final String CMD_OPPOSITE_SIDES = "Rematch - Opposite Sides";

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.4f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT * 0.15f;

	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	private DataTable dt;

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));

	}


	public FinishedGameDetailsModule(AbstractActivity act, AbstractModule _return_to, DataTable _dt) {
		super(-1);

		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		dt = _dt;
		this.mod_return_to = _return_to;

		this.setBackground(Statics.BACKGROUND_R);

		VerticalFlowLayout vfl = new VerticalFlowLayout("vfl", Statics.SCREEN_HEIGHT * 0.001f);

		Label l = new Label("Title", "Game " + dt.getInt("GameID") + " Summary", 0, 0, null, paint_large_text, true);
		l.setCentre(Statics.SCREEN_WIDTH/2, paint_large_text.getTextSize());
		vfl.attachChild(l);

		StringBuffer str = new StringBuffer();
		str.append("Mission: " + dt.getString("MissionName"));// + "\n");
		if (dt.getInt("IsAdvanced") == 1) {
			str.append("\nAdvanced Mode");
		}
		str.append("\n");
		//str.append("\nVPs at end of last turn:\n");
		for (int s=1 ; s<=dt.getInt("Sides") ; s++) {
			str.append(dt.getString("Player" + s + "Name") + " (" + dt.getString("Side" + s + "Name") + "): " + dt.getString("Side" + s + "VPs") + " VPs");
			str.append("\n");
		}
		MultiLineLabel mll = new MultiLineLabel("credits", str.toString(), null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
		mll.setLocation(10, Statics.SCREEN_HEIGHT * 0.15f);

		vfl.attachChild(mll);
		vfl.updateGeometricState();

		GridLayout menu_node = new GridLayout("Menu", ICON_WIDTH, ICON_HEIGHT, Statics.GetButtonSpacing());

		Button b = new Button(CMD_WATCH_PLAYBACK, CMD_WATCH_PLAYBACK, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(b, 0, 0);

		b = new Button(CMD_GAME_CHAT, CMD_GAME_CHAT, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(b, 1, 0);

		if (dt.getInt("Sides") == 2) {
			b = new Button(CMD_REMATCH_SAME_SIDES, CMD_REMATCH_SAME_SIDES, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
			menu_node.attachChild(b, 0, 1);

			b = new Button(CMD_OPPOSITE_SIDES, CMD_OPPOSITE_SIDES, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
			menu_node.attachChild(b, 1, 1);
		}

		vfl.attachChild(menu_node);

		this.root_node.attachChild(vfl);

		this.root_node.updateGeometricState();
		
		this.root_cam.lookAt(this.root_node, true);
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		AbstractActivity act = Statics.act;
		
		String cmd = c.getActionCommand();
		if (cmd.equalsIgnoreCase(CMD_WATCH_PLAYBACK)) {
			this.getThread().setNextModule(new PlaybackModule(act, this, dt.getInt("GameID"), -1));
		} else if (cmd.equalsIgnoreCase(CMD_GAME_CHAT)) {
			this.showGameChat();
		} else if (cmd.equalsIgnoreCase(CMD_REMATCH_SAME_SIDES)) {
			createNewGame(true);
		} else if (cmd.equalsIgnoreCase(CMD_OPPOSITE_SIDES)) {
			createNewGame(false);
		}
	}


	private void createNewGame(boolean same_sides) {
		AbstractActivity act = Statics.act;
		
		try {
			String cmd = MiscCommsPage.START_NEW_SAME_SIDES;
			if (same_sides == false) {
				cmd = MiscCommsPage.START_NEW_OPP_SIDES;
			}
			WGet_SF wc = new WGet_SF(act, null, "cmd=" + cmd + "&gameid=" + dt.getInt("GameID") + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
			String response = wc.getResponse();
			if (AbstractCommFuncs.IsResponseGood(response)) {
				//this.showToast("Game Created!");
				//this.getThread().setNextModule(new GetGamesModule(act, true));
				this.getThread().setNextModule(new GenericMessageModule(act, Statics.MOD_GET_PLAYERS_GAMES, "Game Being Created", "The new game is now in the process of being created.\n\nPress the screen to continue."));
			} else {
				this.showToast("Error creating game.");
			}
		} catch (IOException ex) {
			this.showToast("Error creating game: " + ex.getMessage());
		}
	}


	private void showGameChat() {
		AbstractActivity act = Statics.act;
		
		try {
			String url = Statics.URL_FOR_CLIENT + "/dsr/forums/ForumPostingsPage.cls?topic=" + dt.getInt("ForumID") + "&android_login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&android_pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD);
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
		}

	}


}

