package com.scs.stellarforces.start.finishedgames;

import android.app.Activity;
import android.content.Intent;
import android.text.InputType;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.playback.PlaybackModule;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.MyEvent;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.lang.NumberFunctions;

public class ManualGameEntryModule extends AbstractModule {

	public ManualGameEntryModule(AbstractModule _return_to) {
		super(-1);

		this.mod_return_to = _return_to;


	}


	@Override
	public void started() {
		showKeyboard_New();

	}


	@Override
	public boolean processEvent(MyEvent evt) throws Exception {
		return false;
	}


	@Override
	public void updateGame(long interpol) {

	}


	private void showKeyboard_New() {
		AbstractActivity act = Statics.act;
		
		Statics.data.put("code", "gameid");
		Statics.data.put("text", "");
		act.showKeyboard(InputType.TYPE_NUMBER_VARIATION_NORMAL);
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		AbstractActivity act = Statics.act;
		
		if (resultCode == Activity.RESULT_OK) {
			if (Statics.data.containsKey("gameid")) {
				try {
					int gameid = NumberFunctions.ParseInt(Statics.data.get("gameid"));
					this.getThread().setNextModule(new PlaybackModule(act, this, gameid, -1));
					return;
				} catch (NumberFormatException ex) {
					this.showToast("Invalid Number");
					return;
				}
			}
		}
		this.returnTo();
	}

}
