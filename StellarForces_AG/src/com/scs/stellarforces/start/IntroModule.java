package com.scs.stellarforces.start;

import ssmith.android.framework.MyEvent;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.lib2d.shapes.BitmapRectangle;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

public final class IntroModule extends AbstractModule {

	private BitmapRectangle logo;
	private float speed;

	public IntroModule() {
		super(Statics.MOD_START);
		
		try {
			int r = R.drawable.sf_logo2_small;
			logo = new BitmapRectangle("Logo", Statics.img_cache.getImageByKey_HeightOnly(r, Statics.SCREEN_HEIGHT), Statics.SCREEN_WIDTH, 0);//Statics.SCREEN_HEIGHT*.2f);
			this.root_node.attachChild(logo);
			this.root_node.updateGeometricState();

		} catch (Exception ex) {
			// Probably out of memory.
			this.returnTo();
			return;
		}
		this.root_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);

		speed = Statics.SCREEN_WIDTH / 80f;

		this.setBackground(Statics.BACKGROUND_R);

	}


	@Override
	public void stopped() {
		Statics.img_cache.remove(logo.bmp);

	}


	@Override
	public boolean processEvent(MyEvent evt) throws Exception {
		this.returnTo();
		return true;
	}


	@Override
	public void updateGame(long interpol) {
		logo.adjustLocation(-speed, 0);
		logo.updateGeometricState();

		if (logo.getWorldBounds().right < 0) {
			returnTo();
		}
	}


}
