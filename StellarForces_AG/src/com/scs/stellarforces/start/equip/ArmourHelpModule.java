package com.scs.stellarforces.start.equip;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public class ArmourHelpModule extends SimpleScrollingAbstractModule {
	
	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));
	}
	
	
	public ArmourHelpModule(AbstractActivity act, AbstractModule _return_to) {
		super(-1);
		
		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.mod_return_to = _return_to;
		
		this.setBackground(Statics.BACKGROUND_R);

		Label l = new Label("Credits_title", "Buying Armour", null, paint_large_text);
		l.setCentre(Statics.SCREEN_WIDTH/2, paint_large_text.getTextSize());
		this.root_node.attachChild(l);

		StringBuffer str = new StringBuffer();
		str.append("Buying armour is the first step to equipping your units.  Armour reduces the damage that your units incur from being shot.\n\n");
		str.append("To choose which armour to buy for a unit, simply press the name of the armour so it is highlighted.  Once you have chosen armour for all your units, press the Finished button at the bottom to go to the next stage.\n\n");
		str.append("Each type of armour varies in cost, and heavier the armour will slow a unit down.  The price is shown below the name of the armour.  You have a certain number of credits to spend, and you will also need to buy weapons and equipment which is the next stage.\n\n");
		str.append("Press Back to return.\n\n");

		MultiLineLabel label2 = new MultiLineLabel("credits", str.toString(), null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
		label2.setLocation(10, paint_large_text.getTextSize()*2);//this.view.canvas_width/2,this.view.canvas_height/2);
		root_node.attachChild(label2);
		
		this.root_node.updateGeometricState();

	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		// Do nothing
		
	}


	public void noComponentClicked() {
		this.returnTo();
	}
	

}
