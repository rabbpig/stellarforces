package com.scs.stellarforces.start.equip;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public class EquipHelpModule extends SimpleScrollingAbstractModule {
	
	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));
	}
	
	
	public EquipHelpModule(AbstractActivity act, AbstractModule _return_to) {
		super(-1);
		
		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.mod_return_to = _return_to;
		
		this.setBackground(Statics.BACKGROUND_R);

		Label l = new Label("Credits_title", "Equipping Units", null, paint_large_text);
		l.setCentre(Statics.SCREEN_WIDTH/2, paint_large_text.getTextSize());
		this.root_node.attachChild(l);

		StringBuffer str = new StringBuffer();
		str.append("Equipping your units is the last stage before deploying onto the battlefield.  Here you choose what weapons and equipment to give each unit.\n\n");
		str.append("You have a limited number of creds to spend.  To view the available weapons and equipment, press the left and right arrows.  This will go through all the items that you can afford.\n\n");
		str.append("To buy an item, press the Buy button next to the unit you wish to give it to.\n\n");
		str.append("To remove an item, press Remove Item next to the unit, and then select the item.  You will be reimbursed the full amount for the item.\n\n");
		str.append("Once you have finished, press Finish.\n\n");
		str.append("Press Back to return.\n\n");

		MultiLineLabel label2 = new MultiLineLabel("credits", str.toString(), null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
		label2.setLocation(10, paint_large_text.getTextSize()*2);//this.view.canvas_width/2,this.view.canvas_height/2);
		root_node.attachChild(label2);
		
		this.root_node.updateGeometricState();

	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		// Do nothing
		
	}


	public void noComponentClicked() {
		this.returnTo();
	}
	

}
