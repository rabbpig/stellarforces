package com.scs.stellarforces.start.equip;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.AbstractOptionsModule2;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

import dsr.data.EquipmentData;
import dsr.data.UnitData;

public class SellItemModule extends AbstractOptionsModule2 {

	private UnitData unit;
	private EquipUnitControl ctrl;
	
	private static Paint paint_normal_text = new Paint();

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));

	}

	public SellItemModule(AbstractActivity act, AbstractModule equip_mod, UnitData _unit, EquipUnitControl _ctrl) {
		super(act, -1, 2, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, Statics.SCREEN_WIDTH * .4f, Statics.SCREEN_HEIGHT/7), -1, true, "Select Item to Remove", true);

		if (Statics.GetTypeface(act) != null) {
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.mod_return_to = equip_mod;

		unit = _unit;
		ctrl = _ctrl;

		this.setBackground(Statics.BACKGROUND_R);
	}


	@Override
	public void getOptions() {
		//ArrayList<String> al = new ArrayList<String>();
		for (EquipmentData eq : SelectArmourModule.bought_equipment) {
			if (eq.destroyed == false) {
				if (eq.getUnitID() == unit.unitid) {
					addOption(eq.getName(false));
				}
			}
		}
		if (areThereAnyOptions() == false) {
			this.returnTo();
		}
	}


	@Override
	public void optionSelected(int idx) {
		String cmd = super.getButtonText(idx);
		for (EquipmentData eq : SelectArmourModule.bought_equipment) {
			if (eq.destroyed == false) { // Ignore destroyed items
				if (eq.getUnitID() == unit.unitid) {
					if (eq.getName(false).equalsIgnoreCase(cmd)) {
						eq.destroyed = true;
						eq.setUnitID(-1); // so it "disappears" when we send it to the server.
						ctrl.updateItemList();
						break; // Stop us selling the same item
					}
				}
			}
		}

		this.returnTo();
	}

}
