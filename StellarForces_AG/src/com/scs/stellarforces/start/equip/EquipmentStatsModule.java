package com.scs.stellarforces.start.equip;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

import dsr.comms.DataTable;
import dsrwebserver.tables.EquipmentTypesTable;

public class EquipmentStatsModule extends SimpleScrollingAbstractModule {
	
	private DataTable eq;
	
	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();
	private static Paint paint_small_text = new Paint();

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.06f));

		paint_small_text.setARGB(255, 255, 255, 255);
		paint_small_text.setAntiAlias(true);
		paint_small_text.setTextSize(Statics.GetHeightScaled(0.04f));
	}


	public EquipmentStatsModule(AbstractActivity act, AbstractModule ret, DataTable _eq) {
		super(-1);
		
		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
			paint_small_text.setTypeface(Statics.GetTypeface(act));
		}

		this.mod_return_to = ret;
		eq = _eq;
		
		this.setBackground(Statics.BACKGROUND_R);
	}
	
	
	@Override
	public void started() {
		this.root_node.detachAllChildren();
		
		VerticalFlowLayout layout = new VerticalFlowLayout("vfl", Statics.SCREEN_HEIGHT * 0.005f);
		layout.setLocation(0, 0);
		Label name = new Label("name", eq.getString("Name"), null, paint_large_text, false);
		layout.attachChild(name);
		name = new Label("name", "Cost: " + eq.getInt("Cost"), null, paint_large_text, false);
		layout.attachChild(name);
		if (eq.getInt("MajorTypeID") == EquipmentTypesTable.ET_GUN) {
			name = new Label("name", "Aimed Shot: " + eq.getInt("AimedShotAccuracy") + "%, " + eq.getInt("AimedShotAPCost") + "APs", null, paint_normal_text, false);
			layout.attachChild(name);
			name = new Label("name", "Snap Shot: " + eq.getInt("SnapShotAccuracy") + "%, " + eq.getInt("SnapShotAPCost") + "APs", null, paint_normal_text, false);
			layout.attachChild(name);
			name = new Label("name", "Auto Shot: " + eq.getInt("AutoShotAccuracy") + "%, " + eq.getInt("AutoShotAPCost") + "APs", null, paint_normal_text, false);
			layout.attachChild(name);
			name = new Label("name", "Shot Damage: " + eq.getInt("ShotDamage"), null, paint_normal_text, false);
			layout.attachChild(name);
			name = new Label("name", "Ammo Capacity: " + eq.getInt("AmmoCapacity"), null, paint_normal_text, false);
			layout.attachChild(name);
			name = new Label("name", "APs to Reload: " + eq.getInt("ReloadAPCost"), null, paint_normal_text, false);
			layout.attachChild(name);
			eq.pushRow();
			if (eq.find("MajorTypeID", ""+EquipmentTypesTable.ET_AMMO_CLIP, "AmmoTypeID", ""+eq.getInt("AmmoTypeID"))) {
				name = new Label("name", "Ammo Type: " + eq.getString("Name"), null, paint_normal_text, false);
				layout.attachChild(name);
			}
			eq.popRow();
		}
		if (eq.getInt("Explodes") > 0) {
			name = new Label("name", "Explosion Range: " + eq.getInt("ExplosionRad"), null, paint_normal_text, false);
			layout.attachChild(name);
			name = new Label("name", "Explosion Damage: " + eq.getInt("ExplosionDamage"), null, paint_normal_text, false);
			layout.attachChild(name);
		}
		name = new Label("name", "CC Accuracy: " + eq.getInt("CCAccuracy"), null, paint_normal_text, false);
		layout.attachChild(name);
		name = new Label("name", "CC Damage: " + eq.getInt("CCDamage"), null, paint_normal_text, false);
		layout.attachChild(name);
		name = new Label("name", "Noise Range: " + eq.getInt("NoiseRange"), null, paint_normal_text, false);
		layout.attachChild(name);
		name = new Label("name", "Weight: " + eq.getInt("Weight"), null, paint_normal_text, false);
		layout.attachChild(name);
		name = new Label("name", "Description:", null, paint_large_text, false);
		layout.attachChild(name);
		MultiLineLabel mll = new MultiLineLabel("name", eq.getString("Description"), null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.8f);
		layout.attachChild(mll);

		this.root_node.attachChild(layout);
		
		this.root_node.updateGeometricState();
		this.root_cam.lookAt(Statics.SCREEN_HEIGHT/2, Statics.SCREEN_HEIGHT/2, true);
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		this.returnTo();
	}

	
}

