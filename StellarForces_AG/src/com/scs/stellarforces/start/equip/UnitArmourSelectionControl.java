package com.scs.stellarforces.start.equip;

import ssmith.android.lib2d.Spatial;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.ToggleButton;
import ssmith.android.lib2d.layouts.HorizontalFlowGridLayout;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

import dsr.comms.DataTable;
import dsr.data.GameData;
import dsr.data.UnitData;

public class UnitArmourSelectionControl extends HorizontalFlowGridLayout {

	private static final float OVERALL_WIDTH = Statics.SCREEN_WIDTH * 0.90f;
	private static float ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT;


	private UnitData unit;

	private static Paint paint_unit_name = new Paint();
	private static Paint paint_armour_name = new Paint();

	static {
		paint_unit_name.setARGB(255, 255, 255, 255);
		paint_unit_name.setAntiAlias(true);
		paint_unit_name.setTextSize(Statics.GetHeightScaled(0.06f));

		paint_armour_name.setARGB(255, 255, 255, 255);
		paint_armour_name.setAntiAlias(true);
		paint_armour_name.setTextSize(Statics.GetHeightScaled(0.03f));
	}


	public UnitArmourSelectionControl(UnitData _unit, DataTable armour_dt, float GAP, GameData game_data) {
		super("UnitArmourSelectionControl", GAP);

		if (Statics.tf != null) {
			paint_unit_name.setTypeface(Statics.tf);
			paint_armour_name.setTypeface(Statics.tf);
		}

		ARMOUR_ICON_WIDTH = OVERALL_WIDTH * 0.6f / (armour_dt.size()+1);
		ARMOUR_ICON_HEIGHT = ARMOUR_ICON_WIDTH * 0.9f;

		unit = _unit;

		Label l = new Label("Unit_name", unit.name, null, paint_unit_name, false);
		l.setSize(OVERALL_WIDTH * 0.4f, ARMOUR_ICON_HEIGHT);
		l.collides = true; // TO select unit stats
		this.attachChild(l);

		// Add "no armour" option
		ToggleButton b = new ToggleButton("0", "None", null, null, paint_armour_name, Statics.img_cache.getImage(R.drawable.button_blue, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
		this.attachChild(b);
		b.setSelected(unit.protection == 0);

		armour_dt.moveBeforeFirst();
		while (armour_dt.moveNext()) {
				if (game_data.getMaxProtectionForOurSide() >= 0 && armour_dt.getInt("Protection") > game_data.getMaxProtectionForOurSide()) {
					continue;
				}
			String name = armour_dt.getString("Name") + "\n" + armour_dt.getInt("Cost");
			b = new ToggleButton(armour_dt.getString("ArmourTypeID"), name, null, null, paint_armour_name, Statics.img_cache.getImage(R.drawable.button_blue, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
			//b.setSelected(unit.protection == armour_dt.getInt("Protection"));
			b.setSelected(unit.armour_type_id == armour_dt.getInt("ArmourTypeID"));
			this.attachChild(b);
		}
		this.updateGeometricState(); // to get size

	}


	public void selectArmour(AbstractComponent c)  {
		// Loop through controls
		for (Spatial s: this.getChildren()) {
			if (s instanceof ToggleButton) {
				ToggleButton t = (ToggleButton)s;
				t.setSelected(c == t);
				if (c == t) {
					unit.armour_type_id = Short.parseShort(t.getActionCommand());
				}
			}
		}
	}


}

