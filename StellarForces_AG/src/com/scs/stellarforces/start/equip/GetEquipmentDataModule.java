package com.scs.stellarforces.start.equip;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractPleaseWaitModule;
import ssmith.android.framework.modules.ConfirmModule;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.start.ErrorModule;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.EquipmentDataComms;
import dsr.comms.UnitDataComms;
import dsr.comms.WGet_SF;
import dsr.data.EquipmentData;
import dsr.data.GameData;
import dsr.data.UnitData;
import dsrwebserver.pages.appletcomm.MiscCommsPage;
import dsrwebserver.tables.EquipmentTypesTable;

public class GetEquipmentDataModule extends AbstractPleaseWaitModule {

	private static final String CONFIRM_USE_DEF_EQUIPMENT = "use_def_equipment";

	private GameData gamedata;
	private DataTable available_armour_dt, available_equip_dt;
	private ArrayList<UnitData> units;

	public GetEquipmentDataModule(AbstractActivity _act, GameData _gamedata) {
		super(_act, Statics.MOD_GET_PLAYERS_GAMES);

		gamedata = _gamedata;

		this.setBackground(Statics.BACKGROUND_R);

		start();
	}


	public void run() {
		AbstractActivity act = Statics.act;

		try {
			// Get unit data
			/*super.displayMessage("Getting unit data...");
			WGet_SF wc = new WGet_SF(act, this, UnitDataComms.GetUnitDataRequest(gamedata.game_id, gamedata.gamecode, Statics.LAST_LOGIN, Statics.LAST_PWD));
			String response = wc.getResponse();
			if (response.length() > 0) {
				units = UnitDataComms.DecodeUnitDataResponse(response);*/
			getUnitData();
			// Get equipment data
			super.displayMessage("Getting unit's equipment data...");
			WGet_SF wc = new WGet_SF(act, this, EquipmentDataComms.GetEquipmentDataRequest(gamedata.game_id, gamedata.gamecode));
			String response = wc.getResponse();
			if (response.length() > 0) {
				SelectArmourModule.bought_equipment = EquipmentDataComms.DecodeEquipmentDataResponse(null, units, response);
				// Get equipment list
				super.displayMessage("Getting equipment list...");
				wc = new WGet_SF(act, this, "cmd=" + MiscCommsPage.GET_EQUIPMENT_LIST + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD) + "&gid=" + gamedata.game_id);
				response = wc.getResponse();
				if (response.length() > 0) {
					available_equip_dt = new DataTable(response);
					super.displayMessage("Getting armour list...");
					wc = new WGet_SF(act, this, "cmd=" + MiscCommsPage.GET_ARMOUR_LIST + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD) + "&gid=" + gamedata.game_id);
					response = wc.getResponse();
					if (response.length() > 0) {
						available_armour_dt = new DataTable(response);

						// Load default equipment?
						if (gamedata.creds > 0) {
							// Check no equipment has been bought yet!
							boolean already_equipped = false;
							for (EquipmentData eq : SelectArmourModule.bought_equipment) {
								if (eq.getUnitID() > 0) {
									UnitData unit = UnitData.GetUnitDataFromID(units, eq.getUnitID());
									if (unit.getSide() == gamedata.our_side) {
										already_equipped = true;
										break;
									}
								}
							}
							if (already_equipped == false && gamedata.is_camp_game == 0) {
								this.getThread().setNextModule(new ConfirmModule(act, this, "Use Default Equipment?", "Would you like to automatically equip with the standard equipment for this mission?", Statics.BACKGROUND_R, CONFIRM_USE_DEF_EQUIPMENT));
								return;
							}
						}

						SelectArmourModule sel_armour = new SelectArmourModule(act, gamedata, units, available_armour_dt, available_equip_dt);
						super.getThread().setNextModule(sel_armour);
						return;
					}
				}
			}
			//}
			super.getThread().setNextModule(new ErrorModule(act, Statics.MOD_MENU, "Failed!", "Sorry, I failed to get the equipment data."));
		} catch (Exception ex) {
			this.getThread().setNextModule(new ErrorModule(act, Statics.MOD_MENU, ex));
		}

	}


	private void getUnitData() throws Exception {
		super.displayMessage("Getting unit data...");
		int count = 0;
		Exception last_error = null;
		while (true) {
			count++;
			if (count >= 3) {
				throw last_error;
			}
			try {
				WGet_SF wc = new WGet_SF(Statics.act, this, UnitDataComms.GetUnitDataRequest(gamedata.game_id, gamedata.gamecode, Statics.LAST_LOGIN, Statics.LAST_PWD));
				String response = wc.getResponse();
				if (response.length() > 0) {
					units = UnitDataComms.DecodeUnitDataResponse(response);
					return;
				}
			} catch (Exception ex) {
				last_error = ex;
			}
		}
	}

	@Override
	public void started() {
		AbstractActivity act = Statics.act;

		// Have we come back from "confirmed"?
		if (Statics.data.containsKey(CONFIRM_USE_DEF_EQUIPMENT)) {
			String s = Statics.data.get(CONFIRM_USE_DEF_EQUIPMENT);
			Statics.data.clear();
			if (s.equalsIgnoreCase("yes")) {
				loadStdEquipment(units, available_armour_dt, available_equip_dt);
			}
			SelectArmourModule sel_armour = new SelectArmourModule(act, gamedata, units, available_armour_dt, available_equip_dt);
			super.getThread().setNextModule(sel_armour);
		}
	}


	private void loadStdEquipment(ArrayList<UnitData> units, DataTable armour_dt, DataTable equipment_dt) {
		// Give armour
		for (UnitData unit : units) {
			if (unit.getSide() == gamedata.our_side) {
				if (unit.can_use_equipment) {
					armour_dt.find("Name", "Light");
					unit.armour_type_id = armour_dt.getShort("ArmourTypeID");
					unit.protection = armour_dt.getShort("Protection");
				}
			}
		}

		// Give equipment until money runs out
		String EQUIPMENT[] = {EquipmentTypesTable.CD_SP30, "AP50", "Medikit", "Smokegrenade", "nervegas"};
		for (int i=0 ; i<EQUIPMENT.length ; i++) {
			for (UnitData unit : units) {
				if (unit.getSide() == gamedata.our_side) {
					if (unit.can_use_equipment) {
						if (this.addEquipment(unit.unitid, EQUIPMENT[i], armour_dt, equipment_dt) == false) {
							break;
						}
					}
				}
			}
		}

	}


	private boolean addEquipment(int unitid, String code, DataTable armour_dt, DataTable equipment_dt) {
		if (equipment_dt.find("Code", code)) {
			// Check we've enough money
			int creds_remaining = GetEquipmentDataModule.GetCredsRemaining(gamedata, units, armour_dt, equipment_dt);
			int cost = equipment_dt.getInt("Cost");
			if (creds_remaining < cost) {
				return false;
			}

			EquipmentData new_eq = new EquipmentData();
			//new_eq.new_item = 1;  We add this when sending to the server now, to ensure ALL items are covered
			new_eq.equip_id = -1;
			new_eq.setName(equipment_dt.getString("Name"));
			new_eq.equipment_type_id = equipment_dt.getInt("EquipmentTypeID");
			new_eq.setUnitID(unitid);
			new_eq.setAmmo(equipment_dt.getByte("AmmoCapacity"));
			AddEquipmentRec(new_eq);
			return true;
		} else {
			/*try {
				throw new RuntimeException("Code '" + code + "' not found");
			} catch (Exception ex) {
				ErrorReporter.getInstance().handleSilentException(ex);
			}*/
			return true; // Return true as we want them to continue
		}
	}


	public static void AddEquipmentRec(EquipmentData new_rec) {
		EquipmentData new_data[] = new EquipmentData[SelectArmourModule.bought_equipment.length+1];
		for (int i=0 ; i<SelectArmourModule.bought_equipment.length ; i++) {
			new_data[i] = SelectArmourModule.bought_equipment[i];
		}
		new_data[SelectArmourModule.bought_equipment.length] = new_rec;
		SelectArmourModule.bought_equipment = new_data;
	}


	public static int GetCredsRemaining(GameData gamedata, ArrayList<UnitData> units, DataTable armour_dt, DataTable equip_dt) { // equip_dt.getString("Name")
		equip_dt.saveCurrentPos();
		int total = 0;
		for (UnitData unit : units) {
			if (unit.getSide() == gamedata.our_side) {
				if (unit.armour_type_id > 0) {
					if (armour_dt.find("ArmourTypeID", unit.armour_type_id)) {
						total += armour_dt.getInt("cost");
					} else {
						throw new RuntimeException("Armour " + unit.protection + " not found!");
					}
				}
			}
		}
		for (EquipmentData eq : SelectArmourModule.bought_equipment) {
			if (eq.destroyed == false) {
				if (eq.getUnitID() > 0) {
					UnitData unit = UnitData.GetUnitDataFromID(units, eq.getUnitID());
					if (unit.getSide() == gamedata.our_side) {
						if (equip_dt.find("EquipmentTypeID", eq.equipment_type_id)) {
							total += equip_dt.getInt("Cost"); // equip_dt.getString("Name")
						} else {
							eq.destroyed = true; // Just in case
							if (Statics.RELEASE_MODE == false) {
								throw new RuntimeException("Item " + eq.equipment_type_id + " not found!");
							}
						}
					}
				}
			}
		}
		equip_dt.restoreCurrentPos();
		return gamedata.creds - total;
	}


	public int getTotalCreds() {
		return this.gamedata.creds;
	}



}
