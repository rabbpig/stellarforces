package com.scs.stellarforces.start.equip;

import ssmith.android.lib2d.gui.Button;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

import dsr.data.EquipmentData;

public class EquipmentIcon extends Button {
	
	private static float ICON_WIDTH = Statics.SCREEN_HEIGHT/10f;
	private static float ICON_HEIGHT = ICON_WIDTH;
	
	private static Paint paint_normal_text = new Paint();

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));

	}

	
	public EquipmentIcon(EquipmentData eq, Typeface tf) {
		super(eq.getName(false), null, paint_normal_text, Statics.img_cache.getImage(R.drawable.menu_frame_blue, ICON_WIDTH, ICON_HEIGHT));
		
		paint_normal_text.setTypeface(tf);
	}
	

}
