package com.scs.stellarforces.start.equip;

import java.io.IOException;

import java.net.UnknownHostException;
import java.util.ArrayList;
import ssmith.android.framework.ErrorReporter;
import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.ConfirmModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineButton;
import ssmith.android.lib2d.layouts.GridLayout;
import ssmith.android.lib2d.layouts.HorizontalFlowGridLayout;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import ssmith.android.lib2d.shapes.Rectangle;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.AllUnitStatsModule;
import com.scs.stellarforces.main.lite.R;
import com.scs.stellarforces.start.CurrentGameDetailsModule;
import com.scs.stellarforces.start.GetGameDataModule;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.EquipmentDataComms;
import dsr.comms.GameDataComms;
import dsr.comms.WGet_SF;
import dsr.data.EquipmentData;
import dsr.data.GameData;
import dsr.data.UnitData;
import dsrwebserver.pages.appletcomm.MiscCommsPage;
import dsrwebserver.tables.EquipmentTypesTable;

public class EquipUnitsModule3 extends SimpleScrollingAbstractModule {

	private static final float WIDTH = Statics.SCREEN_WIDTH * 0.19f;
	private static final float HEIGHT = Statics.SCREEN_HEIGHT * 0.13f;

	private static final float GAP = Statics.SCREEN_WIDTH * 0.01f;
	private static final String CONFIRM_END = "finish_equipping";
	private static final String CMD_ITEM_INFO = "item_info";

	private GameData game_data;
	private DataTable equipment_dt, armour_dt;
	private MultiLineButton item_name;
	private Button help, next_item, prev_item, stats;
	private int creds_remaining; 
	private ArrayList<UnitData> units;
	private Button grenades, guns, melee, other_equip;
	private Button finished, armour, game_details, mission_file;
	private int num_units;
	private AllUnitStatsModule stats_mod;
	private boolean any_units = false;
	private Label creds_left;

	private static Paint paint_unit_name = new Paint();
	private static Paint paint_normal_text = new Paint();
	private static Paint paint_equip_text = new Paint();
	private static Paint paint_black_fill = new Paint();

	static {
		paint_unit_name.setARGB(255, 255, 255, 255);
		paint_unit_name.setAntiAlias(true);
		paint_unit_name.setTextSize(Statics.GetHeightScaled(0.07f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));

		paint_equip_text.setARGB(255, 255, 255, 255);
		paint_equip_text.setAntiAlias(true);
		paint_equip_text.setTextSize(Statics.GetHeightScaled(0.04f));

		paint_black_fill.setARGB(155, 0, 0, 0);
		paint_black_fill.setStyle(Paint.Style.FILL);
	}



	public EquipUnitsModule3(AbstractActivity act, SelectArmourModule sa_mod, GameData _gamedata, ArrayList<UnitData> _units, DataTable _armour, DataTable _eq, int _num_units) {
		super(-1);

		if (Statics.GetTypeface(act) != null) {
			paint_unit_name.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
			paint_equip_text.setTypeface(Statics.GetTypeface(act));
		}

		super.limit_dragging = false;

		this.mod_return_to = sa_mod;
		game_data = _gamedata;
		equipment_dt = _eq;
		armour_dt = _armour;
		units = _units;
		num_units = _num_units;

		this.setBackground(Statics.BACKGROUND_R);

		this.root_node.removeAllChildren();
		this.stat_node.removeAllChildren();

		Rectangle back = new Rectangle("back", 0, 0, Statics.SCREEN_WIDTH, HEIGHT*2, paint_black_fill, null);
		this.stat_node.attachChild(back);

		VerticalFlowLayout vfl_header = new VerticalFlowLayout("vfl", GAP);

		// Row 0
		HorizontalFlowGridLayout hfgl0 = new HorizontalFlowGridLayout("hfgl0", GAP);
		help = new Button("Help", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, WIDTH*.75f, HEIGHT));
		hfgl0.attachChild(help);
		creds_left = new Label("creds_left", "Filled in later..", paint_black_fill, paint_unit_name, false);
		hfgl0.attachChild(creds_left);
		vfl_header.attachChild(hfgl0);

		// Row 3
		/*add sometime? HorizontalFlowGridLayout hfgl3 = new HorizontalFlowGridLayout("hfgl1", GAP);
		guns = new Button("Guns", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, WIDTH, HEIGHT));
		hfgl3.attachChild(guns);
		grenades = new Button("Grenades", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, WIDTH, HEIGHT));
		hfgl3.attachChild(grenades);
		melee = new Button("Melee", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, WIDTH, HEIGHT));
		hfgl3.attachChild(melee);
		other_equip = new Button("Other", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, WIDTH, HEIGHT));
		hfgl3.attachChild(other_equip);
		vfl_header.attachChild(hfgl3);
		 */

		// Row 4
		GridLayout hfgl4 = new GridLayout("hfgl4", WIDTH, HEIGHT, GAP);
		prev_item = new Button("", paint_black_fill, paint_normal_text, Statics.img_cache.getImage(R.drawable.big_arrow_left, WIDTH, HEIGHT)); // "Prev\nItem"
		hfgl4.attachChild(prev_item, 0, 0);
		item_name = new MultiLineButton("item_name", CMD_ITEM_INFO, "", paint_black_fill, paint_equip_text, true, (Statics.SCREEN_WIDTH-(WIDTH*2)), false);
		item_name.setCollides(true);
		//item_name.updateGeometricState();
		hfgl4.attachChild(item_name, 1, 0);
		/*info = new Button("Item\nInfo", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, WIDTH, HEIGHT));
		hfgl4.attachChild(info);*/
		next_item = new Button("", paint_black_fill, paint_normal_text, Statics.img_cache.getImage(R.drawable.big_arrow_right, WIDTH, HEIGHT)); // "Next\nItem"
		hfgl4.attachChild(next_item, 4, 0);
		vfl_header.attachChild(hfgl4);

		stat_node.attachChild(vfl_header);

		// Footer
		GridLayout hfl_footer = new GridLayout("footer", WIDTH, HEIGHT, GAP);
		hfl_footer.setLocation(0, Statics.SCREEN_HEIGHT - HEIGHT);
		stats = new Button("Unit\nStats", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, WIDTH, HEIGHT));
		hfl_footer.attachChild(stats, 0, 0);
		armour = new Button("Return to\nArmour", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, WIDTH, HEIGHT));
		hfl_footer.attachChild(armour, 1, 0);
		game_details = new Button("Game\nDetails", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, WIDTH, HEIGHT));
		hfl_footer.attachChild(game_details, 2, 0);
		mission_file = new Button("Mission\nFile", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, WIDTH, HEIGHT));
		hfl_footer.attachChild(mission_file, 3, 0);
		finished = new Button("Finished!", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_red, WIDTH, HEIGHT));
		hfl_footer.attachChild(finished, 4, 0);
		stat_node.attachChild(hfl_footer);

		// Load units
		VerticalFlowLayout vfl_units = new VerticalFlowLayout("vfl", GAP);

		for (UnitData unit : units) {
			if (unit.getSide() == game_data.our_side) {
				if (unit.can_use_equipment) {
					if (unit.can_deploy > 0) {
						if (unit.can_equip > 0) {
							any_units = true;
							EquipUnitControl ctrl = new EquipUnitControl(this, unit, GAP, this.game_data, paint_unit_name.getTypeface()); 
							vfl_units.attachChild(ctrl);
						}
					}
				}
			}
		}
		root_node.attachChild(vfl_units);

		this.root_node.updateGeometricState();
		this.root_cam.lookAt(Statics.SCREEN_WIDTH/2, (Statics.SCREEN_HEIGHT/2) - (HEIGHT*2), true);

		if (any_units == false)   {
			this.finish();
		}

		this.updateCredsLeft();

		equipment_dt.moveFirst();
		showItem();

		this.showToast("Equip Your Units!");
	}


	@Override
	public void started() {
		if (any_units == false || this.game_data.creds <= 0) {
			finish(); 
			return;

		}

		// Have we come back from "confirmed"?
		if (Statics.data.containsKey(CONFIRM_END)) {
			String s = Statics.data.get(CONFIRM_END);
			Statics.data.clear();
			if (s.equalsIgnoreCase("yes")) {
				this.finish();
			}
		}

		// New these here in case coming back from 'Sell Item'
		this.updateCredsLeft();
		showItem();

	}


	private void updateCredsLeft() {
		creds_remaining = GetEquipmentDataModule.GetCredsRemaining(game_data, units, armour_dt, equipment_dt);
		int avg_each = 0;
		if (num_units > 0) {
			avg_each = creds_remaining / this.num_units;
		}
		creds_left.setText("Equip Units - Creds Left:" + creds_remaining + " (" + avg_each + " each)");

	}


	private void showItem() {
		StringBuffer str = new StringBuffer();
		str.append("Currently Viewing " + this.equipment_dt.getString("MajorType") + ":-\n" + this.equipment_dt.getString("name") + " (" + this.equipment_dt.getString("cost") + " creds)\n");
		try {
			switch (this.equipment_dt.getInt("MajorTypeID")) {
			case EquipmentTypesTable.ET_GUN:
				str.append("Acc: " + this.equipment_dt.getInt("AimedShotAccuracy") + "%, Damage: " + this.equipment_dt.getInt("ShotDamage"));
				// Find ammo
				equipment_dt.pushRow();
				if (equipment_dt.find("MajorTypeID", ""+EquipmentTypesTable.ET_AMMO_CLIP, "AmmoTypeID", ""+equipment_dt.getInt("AmmoTypeID"))) {
					str.append(", Uses " + equipment_dt.getString("Name") + " ammo");
				}
				equipment_dt.popRow();
				break;
			case EquipmentTypesTable.ET_GRENADE:
				str.append("Explosion Range: " + this.equipment_dt.getInt("ExplosionRad") + ", Damage: " + this.equipment_dt.getInt("ExplosionDamage"));
				break;
			case EquipmentTypesTable.ET_CC_WEAPON:
				str.append("CC Damage: " + this.equipment_dt.getInt("CCDamage"));
				break;
			case EquipmentTypesTable.ET_AMMO_CLIP:
				str.append(this.equipment_dt.getString("Description"));
				break;
			default:
				//str.append(this.equipment_dt.getString("Description"));
				break;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		this.item_name.setText(str.toString());
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		AbstractActivity act = Statics.act;
		
		if (c == help) {
			EquipHelpModule mod = new EquipHelpModule(act, this);
			this.getThread().setNextModule(mod);
		} else if (c == guns) {
			int count = this.equipment_dt.size(); // Check we don't on forever!
			do {
				if (this.equipment_dt.moveNext() == false) {
					this.equipment_dt.moveFirst();
					count--;
				}
			} while (this.equipment_dt.getInt("MajorTypeID") != EquipmentTypesTable.ET_GUN && count > 0);
			this.showItem();
		} else if (c == grenades) {
			int count = this.equipment_dt.size(); // Check we don't on forever!
			do {
				if (this.equipment_dt.moveNext() == false) {
					this.equipment_dt.moveFirst();
					count--;
				}
			} while ((this.equipment_dt.getInt("MajorTypeID") != EquipmentTypesTable.ET_GRENADE || this.equipment_dt.getInt("MajorTypeID") != EquipmentTypesTable.ET_SMOKE_GRENADE || this.equipment_dt.getInt("MajorTypeID") != EquipmentTypesTable.ET_NERVE_GAS || this.equipment_dt.getInt("MajorTypeID") != EquipmentTypesTable.ET_DEATH_GRENADE) && count > 0);
			this.showItem();
		} else if (c == melee) {
			int count = this.equipment_dt.size(); // Check we don't on forever!
			do {
				if (this.equipment_dt.moveNext() == false) {
					this.equipment_dt.moveFirst();
					count--;
				}
			} while ((this.equipment_dt.getInt("MajorTypeID") != EquipmentTypesTable.ET_CC_WEAPON) && count > 0);
			this.showItem();
		} else if (c == other_equip) {
			int count = this.equipment_dt.size(); // Check we don't on forever!
			do {
				if (this.equipment_dt.moveNext() == false) {
					this.equipment_dt.moveFirst();
					count--;
				}
			} while ((this.equipment_dt.getInt("MajorTypeID") != EquipmentTypesTable.ET_MEDIKIT) && count > 0);
			this.showItem();
		} else if (c == next_item) {
			int count = this.equipment_dt.size(); // Check we don't on forever!
			do {
				if (this.equipment_dt.moveNext() == false) {
					this.equipment_dt.moveFirst();
					count--;
				}
			} while (this.equipment_dt.getInt("Cost") > creds_remaining && count > 0 && creds_remaining > 0);
			this.showItem();
		} else if (c == prev_item) {
			// skip items that are too expensive
			int count = this.equipment_dt.size(); // Check we don't on forever!
			do {
				if (this.equipment_dt.movePrev() == false) {
					this.equipment_dt.moveLast();
					count--;
				}
			} while (this.equipment_dt.getInt("Cost") > creds_remaining && count > 0 && creds_remaining > 0); // equipment_dt.getString("Name")
			this.showItem();
			/*} else if (c == buy) {
			// check we can afford it
			if (this.equipment_dt.getInt("Cost") <= creds_remaining) {
				buyItem();
			} else {
				this.showToast("Insufficient Credits!");
			}
		/*} else if (c == remove_item) {
			this.getThread().setNextModule(new SellItemModule(act, this, units[this.current_unit]));*/
		} else if (c == this.game_details) {
			CurrentGameDetailsModule mod = new CurrentGameDetailsModule(act, this, game_data, true);
			this.getThread().setNextModule(mod);
		} else if (c == this.mission_file) {
			String url = Statics.URL_FOR_CLIENT + "/dsr/missiondescriptions.cls?type=" + game_data.mission_type + "&android_login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&android_pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD); 
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		} else if (c == armour) {
			if (this.saveEquipment()) {
				this.returnTo(); // equipment
			}
		} else if (c == stats) {
			//this.getThread().setNextModule(new SingleUnitStatsModule(act, this, units[this.current_unit], null));
			if (stats_mod == null) {
				stats_mod = new AllUnitStatsModule(act, this, game_data, units, null);
			}
			this.getThread().setNextModule(stats_mod);
		} else if (c.getActionCommand().equalsIgnoreCase(CMD_ITEM_INFO)) {
			this.getThread().setNextModule(new EquipmentStatsModule(act, this, this.equipment_dt));
		} else if (c == finished) {
			// check the player has enough credits
			if (GetEquipmentDataModule.GetCredsRemaining(game_data, units, armour_dt, equipment_dt) >= 0) {
				if (this.saveEquipment()) {
					confirmIfFinished();
				}
			} else {
				// Show message saying not enough credits
				// this.getThread().setNextModule(new GenericMessageModule(act, this, "Not Enough Creds", "Sorry, you don't have enough creds to buy that armour."));
				this.showToast("You've spent too many creds!");
			}
		} else if (c.parent.parent instanceof EquipUnitControl) {
			EquipUnitControl ctrl = (EquipUnitControl) c.parent.parent;
			ctrl.handleClick(c);
		}

	}


	private void confirmIfFinished() {
		AbstractActivity act = Statics.act;
		
		this.getThread().setNextModule(new ConfirmModule(act, this, "Are You Sure?", "Are you sure you have definitely finished equipping your units?  You can't go back afterwards!", Statics.BACKGROUND_R, CONFIRM_END));
	}


	private void finish() {
		AbstractActivity act = Statics.act;
		
		try {
			// Mark player as equipped
			this.showPleaseWait("Saving data...");
			WGet_SF wc = new WGet_SF(act, null, GameDataComms.GetGameUpdateRequest(game_data.game_id, game_data.gamecode, game_data.our_side, GameDataComms.EQUIPPED));
			String response = wc.getResponse();
			this.dismissPleaseWait();
			if (AbstractCommFuncs.IsResponseGood(response)) {
				super.getThread().setNextModule(new GetGameDataModule(act, this.game_data));
			} else {
				throw new IOException("Bad response");
			}
		} catch (IOException ex) {
			this.dismissPleaseWait();
			this.showToast("Unable to save data.  Please try again.");
		}

	}


	public void buyItem(UnitData unit) {
		// check we can afford it
		if (this.equipment_dt.getInt("Cost") <= creds_remaining) {
			EquipmentData new_eq = new EquipmentData();
			new_eq.equip_id = -1;//this.equipment_dt.getAsInt("EquipmentID");
			new_eq.setName(this.equipment_dt.getString("Name"));
			new_eq.equipment_type_id = this.equipment_dt.getInt("EquipmentTypeID");
			new_eq.setUnitID(unit.unitid);
			new_eq.setAmmo(this.equipment_dt.getByte("AmmoCapacity"));
			GetEquipmentDataModule.AddEquipmentRec(new_eq);
			this.updateCredsLeft();
		} else {
			this.showToast("Insufficient Credits!");
		}
	}


	public void removeItem(UnitData unit, EquipUnitControl ctrl) {
		AbstractActivity act = Statics.act;
		
		this.getThread().setNextModule(new SellItemModule(act, this, unit, ctrl));
	}


	private boolean saveEquipment() throws UnknownHostException, IOException {
		AbstractActivity act = Statics.act;
		
		// Save the data
		if (SelectArmourModule.bought_equipment.length > 0) { // Only show please wait if there's something to save, otherwise it's too quick
			try {
				this.showPleaseWait("Saving equipment...");
				// Send command to remove all previous purchases - in case of dupe request!
				String req = "cmd=" + MiscCommsPage.REMOVE_EQUIPMENT_DATA + "&version=" + Statics.COMMS_VERSION + "&gid=" + game_data.game_id + "&gc=" + game_data.gamecode + "&side=" + game_data.our_side;
				new WGet_SF(act, null, req);
				//request.append(req + MiscCommsPage.SEP);
				StringBuffer request = new StringBuffer();
				for (EquipmentData eq : SelectArmourModule.bought_equipment) {
					if (eq.getUnitID() > 0) { // Ignore equipment that isn't held by a unit as it's a pre-generated item, e.g. flag, so we don't want to mess with it.
						eq.new_item = 1; // Ensure it is created since we've just told the server to delete all existing items
						req = EquipmentDataComms.GetEquipmentUpdateRequest(eq, -1, -1, game_data.game_id);
						request.append(req + MiscCommsPage.SEP);
					} else {
						// Probably a gas cannister or other existing item
					}
				}
				new WGet_SF(act, null, request.toString());
			} catch (Exception ex) {
				if (ex instanceof IOException == false) {
					ErrorReporter.getInstance().handleSilentException(ex);
				}
				this.dismissPleaseWait();
				if (ex != null) {
					this.showToast(ex.toString());
				} else {
					this.showToast("'null' error.  Sorry.");
				}
				return false;
			} finally {
				this.dismissPleaseWait();
			}
		}
		return true;
	}


}

