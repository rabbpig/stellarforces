package com.scs.stellarforces.start;

import ssmith.android.framework.ErrorReporter;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.AbstractPleaseWaitModule;
import ssmith.android.framework.modules.ConfirmModule;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.exceptions.FailedToConnectException;

import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class QuickstartModule extends AbstractPleaseWaitModule {

	private static final String CONFIRM_QUICKSTART = "quickstart";

	public QuickstartModule(AbstractActivity act) {
		super(act, Statics.MOD_START);

		this.setBackground(Statics.BACKGROUND_R);

	}


	@Override
	public void started() {
		if (Statics.data.containsKey(CONFIRM_QUICKSTART)) {
			if (Statics.data.get(CONFIRM_QUICKSTART).equalsIgnoreCase(ConfirmModule.YES)) {
				super.displayMessage("Connecting to server to register user and start new mission...");
				start();
			} else {
				this.getThread().setNextModule(Statics.GetModule(Statics.MOD_START));
			}
			Statics.data.clear();
			return;
		}
		askIfQuickstart();
	}


	public void askIfQuickstart() {
		AbstractActivity act = Statics.act;
		AbstractModule m = new ConfirmModule(act, this, "Quickstart?", "This will automatically log you in with a random username, replacing any username you may have created already.  (If you want to access any games linked to your old login, you will have to log in manually with the your old login details).", Statics.BACKGROUND_R, CONFIRM_QUICKSTART);
		this.getThread().setNextModule(m);

	}


	public void run() {
		AbstractActivity act = Statics.act;

		int tries = 0;
		Exception last_ex = null;
		while (tries < 3) {
			tries++;
			try {
				WGet_SF wc = new WGet_SF(act, this, "cmd=" + MiscCommsPage.AUTOREGISTER);
				if (wc.getResponseCode() == 200) {
					String response = wc.getResponse();
					DataTable data = new DataTable(response);
					data.moveFirst();
					Statics.LAST_LOGIN = data.getString("Login");
					Statics.LAST_PWD = data.getString("Pwd");
					super.getThread().setNextModule(Statics.GetModule(Statics.MOD_VALIDATE_LOGIN));
					return;
				}
			} catch (FailedToConnectException ex) {
				// Do nothing
				last_ex = ex;
			} catch (Exception ex) {
				ErrorReporter.getInstance().handleSilentException(ex);
				last_ex = ex;
			}
		}
		String msg = "";
		if (last_ex != null) {
			msg = last_ex.getMessage();
		}
		//throw new RuntimeException("Error connecting to server.  Please re-try.  (Last error: " + msg + ")");
		super.getThread().setNextModule(new ErrorModule(act, Statics.MOD_MENU, "Failed!", "Error connecting to server.  Please re-try.  (Last error: " + msg + ")"));
	}

}
