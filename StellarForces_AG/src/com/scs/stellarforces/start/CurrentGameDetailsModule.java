package com.scs.stellarforces.start;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

import dsr.data.GameData;

public class CurrentGameDetailsModule extends SimpleScrollingAbstractModule { 

	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	private GameData gamedata;

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));

	}


	public CurrentGameDetailsModule(AbstractActivity act, AbstractModule _return_to, GameData _gamedata, boolean take_turn_msg) {
		super(-1);

		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.mod_return_to = _return_to;
		gamedata = _gamedata;

		this.setBackground(Statics.BACKGROUND_R);

		Label l = new Label("Title", "Game " + gamedata.game_id + " Summary", 0, 0, null, paint_large_text, true);
		l.setCentre(Statics.SCREEN_WIDTH/2, paint_large_text.getTextSize());
		this.root_node.attachChild(l);

		StringBuffer str = new StringBuffer();
		if (gamedata.max_turns > 0) {
			str.append("Turn: " + gamedata.turn_no + "/" + gamedata.max_turns + "\n");
			if (gamedata.turn_no >= gamedata.max_turns && gamedata.max_turns > 0) {
				str.append("THIS IS THE LAST TURN!\n");
			}
		} else {
			str.append("Turn: " + gamedata.turn_no + "\n");
		}
		str.append("Mission: " + gamedata.mission_name);// + "\n");
		if (gamedata.isItOurTurn() == false) {
			if (gamedata.days_waiting > 0) {
				str.append("\nYou have been waiting " + gamedata.days_waiting + " days for your opponent.");
			}
		}
		if (gamedata.is_advanced == 1) {
			str.append("\nAdvanced Mode");
		}
		if (gamedata.is_practise == 1) {
			str.append("\nPractise Mode");
		}
		str.append("\n");
		str.append("\nVPs at end of last turn:\n");
		for (int s=1 ; s<=gamedata.num_players ; s++) {
			if (gamedata.is_snafu == 0) {// || s == gamedata.our_side) {
				str.append(gamedata.GetPlayersNameFromSide(s) + " (" + gamedata.side_names[s] + "): " + gamedata.vps[s] + " VPs");
				if (gamedata.is_advanced == 0 || gamedata.areSidesFriends(s, gamedata.our_side)) { // || gamedata.can_build_and_dismantle == 1) { // Show num units for Colony 
					str.append(", " + gamedata.units_remaining[s] + " units remaining");
				}
				str.append("\n");
			} else {
				str.append(gamedata.GetPlayersNameFromSide(s) + " (" + gamedata.side_names[s] + ")\n");
			}
		}
		if (gamedata.is_snafu != 0) {
			String sides = "";
			for (int s=1 ; s<=gamedata.num_players ; s++) {
				if (gamedata.snafu_will_opp_fire_on_side[s] == 1) {
					sides = sides + s + ",";
				}
			}
			if (sides.length() > 0) {
				sides = "You will ONLY opp-fire at side(s) " + sides.substring(0, sides.length()-1) + ".";
			} else {
				sides = "You will not opp-fire at any sides.  Use website to change this.";
			}
			str.append("This is a SNAFU mission so VPs are hidden.  " + sides + "\n");
		}
		if (this.gamedata.can_build_and_dismantle == 1) {
			str.append("\nYou have " + this.gamedata.getResPointsForOurSide() + " Resource Points\n");
		}
		str.append("\nObjective:\n" + gamedata.mission1liner + "\n");


		/*if (take_turn_msg) {
			l = new Label("Title", "Press screen to continue!", 0, 0, null, paint_large_text, true);
		} else {
			l = new Label("Title", "Waiting for opponent!", 0, 0, null, paint_large_text, true);
		}
		l.setCentre(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT-paint_large_text.getTextSize());
		this.root_node.attachChild(l);*/

		if (take_turn_msg) {
			str.append("\nPress screen to continue!");
		} else {
			str.append("\nWaiting for opponent!");
		}

		MultiLineLabel label2 = new MultiLineLabel("credits", str.toString(), null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
		label2.setLocation(10, Statics.SCREEN_HEIGHT * 0.15f);
		root_node.attachChild(label2);

		this.root_node.updateGeometricState();
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		// Do nothing

	}


	public void noComponentClicked() {
		this.returnTo();
	}

}

