package com.scs.stellarforces.start;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.AbstractSingleScreenModule;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public final class ErrorModule extends AbstractSingleScreenModule {

	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));

	}


	public ErrorModule(AbstractActivity act, AbstractModule _return_to, String title, String text) {
		this(act, -1, title, text);

		this.mod_return_to = _return_to;
	}


	public ErrorModule(AbstractActivity act, int mod, Throwable t) {
		this(act, mod, "Error", t.toString());
	}


	public ErrorModule(AbstractActivity act, int mod, String title, String text) {
		super(mod);

		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		showError(title, text);

		this.setBackground(Statics.BACKGROUND_R);
	}


	/*@Override
	public void started() {
		show_count--;
		if (show_count < 0) {
			this.i_return_to = Statics.MOD_START;
			this.returnTo();
		}
	}*/


	private void showError(String title, String text) {
		Label l = new Label("Title", title, 0, 0, null, paint_large_text, true);
		l.setCentre(Statics.SCREEN_WIDTH/2, paint_large_text.getTextSize());
		this.stat_node.attachChild(l);

		MultiLineLabel label2 = new MultiLineLabel("credits", text + "\n\nPress screen to return.", null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
		label2.setLocation(10, Statics.SCREEN_HEIGHT/2);
		stat_node.attachChild(label2);

		this.stat_node.updateGeometricState();
	}

}
