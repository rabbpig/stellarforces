package com.scs.stellarforces.start.newgame;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.start.GetGamesModule;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class StartPractiseGameModule extends SimpleScrollingAbstractModule {

	private Button cmd_accept;

	private static Paint paint_large_text = new Paint();
	private static Paint paint_med_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_med_text.setARGB(255, 255, 255, 255);
		paint_med_text.setAntiAlias(true);
		//paint_med_text.setStyle(Style.STROKE);
		paint_med_text.setTextSize(Statics.GetHeightScaled(0.07f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		//paint_normal_text.setStyle(Style.STROKE);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));

	}


	public StartPractiseGameModule(AbstractActivity act) {
		super(Statics.MOD_START_OR_JOIN_GAME);

		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_med_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.setBackground(Statics.BACKGROUND_R);
	}


	@Override
	public void started() {
		// Any text returned?
		this.root_node.detachAllChildren();

		Label l = new Label("Title", "New Practise Game", 0, 0, null, paint_large_text, true);
		l.setCentre(Statics.SCREEN_WIDTH/2, paint_large_text.getTextSize());
		this.root_node.attachChild(l);

		VerticalFlowLayout vfl = new VerticalFlowLayout("vfl", Statics.SCREEN_HEIGHT * 0.05f);
		vfl.setLocation(0,  Statics.SCREEN_HEIGHT * 0.2f);
		
		MultiLineLabel mll = new MultiLineLabel("mll", "", null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
		mll.setText("Here you can start a practise game.  It is the same as a normal game except you are playing against a very simple AI.  When you end your turn, the AI will move, and then it will be your turn again.\n\nPractise games are a good way to get familiar with the basic game, but we would recommend playing against another player to get the most enjoyment!");
		vfl.attachChild(mll);

		int sides = 1;
		float width = (Statics.SCREEN_WIDTH * 0.9f) / (sides+1);
		float height = Statics.SCREEN_HEIGHT/7;
		cmd_accept  = new Button("Start", "Start Practise Game", null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R, width, height));
		cmd_accept.setLocation(10, Statics.SCREEN_HEIGHT-height);
		vfl.attachChild(cmd_accept);

		vfl.updateGeometricState();
		vfl.setCentre(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2);

		this.root_node.attachChild(vfl);
		this.root_node.updateGeometricState();
		this.root_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		if (c == cmd_accept) {
			createNewGame();
		}
	}


	private void createNewGame() {
		AbstractActivity act = Statics.act;
		
		this.showPleaseWait("Creating game...");
		try {
			WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.START_PRACTISE_GAME_WITH_AI + "&version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
			String response = wc.getResponse();
			if (response.length() == 0 || response.equalsIgnoreCase("ok")) {
				//this.getThread().setNextModule(new GenericMessageModule(act, Statics.MOD_GET_PLAYERS_GAMES, "Game Being Created", "Your chosen game is now in the process of being created.  Please note that it may take a few minutes, so it may not immediately appear on your list of games.\n\nPress the screen to continue."));
				this.getThread().setNextModule(new GetGamesModule(act, true));
			} else {
				this.dismissPleaseWait();
				this.showToast(response);
			}
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
		} finally {
			this.dismissPleaseWait();
		}

	}


}
