package com.scs.stellarforces.start.newgame;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.AbstractOptionsModule2;
import ssmith.android.lib2d.gui.GUIFunctions;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.text.InputType;

import com.scs.stellarforces.Statics;

import dsr.comms.DataTable;

public class ChooseMissionModule extends AbstractOptionsModule2 {

	private static final String CMD_FILTER = "FILTER";

	private DataTable missions_dt;
	private String current_filter = "";

	private static Paint paint_free_text = new Paint();

	static {
		paint_free_text.setARGB(255, 200, 200, 200);
		paint_free_text.setAntiAlias(true);
		//paint_free_text.setStyle(Style.STROKE);
		paint_free_text.setTextSize(GUIFunctions.GetTextSizeToFit(Statics.FREE_TEXT, Statics.SCREEN_WIDTH, -1));

	}


	public ChooseMissionModule(AbstractActivity act, AbstractModule _return_to, DataTable _missions_dt) {
		super(act, -1, 1, paint_free_text, Statics.img_cache.getImage(Statics.BUTTON_R, Statics.SCREEN_WIDTH*0.9f, Statics.SCREEN_HEIGHT/6), -1, false, "Select Mission", true);

		if (Statics.GetTypeface(act) != null) {
			paint_free_text.setTypeface(Statics.GetTypeface(act));
		}

		this.mod_return_to = _return_to;
		missions_dt = _missions_dt;

		this.setBackground(Statics.BACKGROUND_R);
	}


	/*@Override
	public void started() {
		if (Statics.data.containsKey(CMD_FILTER)) {
			this.current_filter = Statics.data.get(CMD_FILTER);
		}
		super.started();
	}*/


	@Override
	public void getOptions() {
		if (Statics.FULL_VERSION) {
			super.addOption("Filter List", CMD_FILTER);
		}
		missions_dt.moveBeforeFirst();
		int num = 1;
		while (missions_dt.moveNext()) {
			if (missions_dt.getString("Name").toLowerCase().indexOf(this.current_filter.toLowerCase()) >= 0) {
				super.addOption(num + ": " + missions_dt.getString("Name"), missions_dt.getString("Name"));
				num++;
			}
		}
	}


	@Override
	public void optionSelected(int idx) {
		String cmd = super.getActionCommand(idx);
		if (cmd.equalsIgnoreCase(CMD_FILTER) == false) {
			Statics.data.clear();
			Statics.data.put(StartNewGameModule.DATA_MISSION_NAME, cmd);
			this.returnTo();
		} else {
			this.showKeyboard_New(CMD_FILTER, current_filter, false);
		}

	}


	private void showKeyboard_New(String code, String curr_val, boolean mask) {
		AbstractActivity act = Statics.act;
		
		Statics.data.put("code", code);
		Statics.data.put("text", curr_val);
		act.showKeyboard(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (Statics.data.containsKey(CMD_FILTER)) {
				this.current_filter = Statics.data.get(CMD_FILTER);
				Statics.data.clear();
			}
			this.setOptions();
		}
	}

}
