package com.scs.stellarforces.start.newgame;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.CheckBox;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import ssmith.android.lib2d.gui.ToggleButton;
import ssmith.android.lib2d.gui.ToggleButtonGroup;
import ssmith.android.lib2d.layouts.HorizontalFlowGridLayout;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.text.InputType;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;
import com.scs.stellarforces.start.GenericMessageModule;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class StartNewGameModule extends SimpleScrollingAbstractModule {

	// Toggle Button Commands
	private static final String YOU = "You";
	private static final String ANOTHER_PLAYER = "Another Player";
	private static final String AI = "AI";

	public static final String DATA_MISSION_NAME = "mission_name";
	public static final String DATA_OPPONENT_NAME = "opponent_name";

	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT/8;

	private static DataTable missions_dt;
	private String selected_mission_name = "[Press Here]";
	private Button cmd_mission, cmd_opponent, start_game, cmd_show_mission;
	private CheckBox cmd_advanced, cmd_practise;
	private ToggleButtonGroup groups[];

	private static Paint paint_large_text = new Paint();
	private static Paint paint_med_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_med_text.setARGB(255, 255, 255, 255);
		paint_med_text.setAntiAlias(true);
		paint_med_text.setTextSize(Statics.GetHeightScaled(0.07f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));

	}


	public StartNewGameModule(AbstractActivity act) {
		super(Statics.MOD_START_OR_JOIN_GAME);

		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_med_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.setBackground(Statics.BACKGROUND_R);
	}


	@Override
	public void started() {
		AbstractActivity act = Statics.act;
		
		// Get the list of missions
		if (missions_dt == null || missions_dt.size() == 0) {
			this.showPleaseWait("Getting missions...");
			try {
				WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.GET_MISSION_LIST + "&free=" + (Statics.FULL_VERSION?"":"free") + "&comms_version=" + Statics.COMMS_VERSION + "&android_version=" + Statics.VERSION_NUM + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
				String response = wc.getResponse();
				if (response != null && response.length() > 0) {
					missions_dt = new DataTable(response);
				} else {
					throw new IOException("Got response " + wc.getResponseCode() + " from server.");
				}
			} catch (Exception ex) {
				AbstractActivity.HandleError(ex);
				return;
			} finally {
				this.dismissPleaseWait();
			}
		}

		// Was a mission selected?
		if (Statics.data.containsKey(StartNewGameModule.DATA_MISSION_NAME)) {
			selected_mission_name = Statics.data.get(StartNewGameModule.DATA_MISSION_NAME);
			/*Statics.data.clear();
			return; // So we don't clear all the current options*/
		}
		// Any text returned?
		String opponent_names = "";
		if (Statics.data.containsKey(DATA_OPPONENT_NAME)) {
			String text = Statics.data.get(DATA_OPPONENT_NAME).toString();
			opponent_names = text;
			if (cmd_opponent != null) {
				cmd_opponent.setText(text);
			}
			Statics.data.clear();
			return; // So we don't clear all the current options
		}
		Statics.data.clear();

		this.root_node.detachAllChildren();

		Label l = new Label("Title", "Start New Game", 0, 0, null, paint_large_text, true);
		l.setCentre(Statics.SCREEN_WIDTH/2, paint_large_text.getTextSize());
		this.root_node.attachChild(l);

		VerticalFlowLayout vfl = new VerticalFlowLayout("vfl", Statics.SCREEN_HEIGHT * 0.05f);
		vfl.setLocation(0,  Statics.SCREEN_HEIGHT * 0.2f);

		Label mission = new Label("mission", "Choose Mission:", null, paint_med_text);
		vfl.attachChild(mission);
		cmd_mission = new Button(selected_mission_name, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R, Statics.SCREEN_WIDTH * 0.8f, ICON_HEIGHT));
		vfl.attachChild(cmd_mission);

		if (selected_mission_name.length() > 0) {
			if (missions_dt.find("Name", selected_mission_name)) {
				MultiLineLabel mll;
				String text = "";
				try {
					text = AbstractCommFuncs.URLDecodeString(missions_dt.getString("Description"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				mll = new MultiLineLabel("Mission text", text, null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
				vfl.attachChild(mll);

				cmd_show_mission = new Button("View Mission Details", null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R, Statics.SCREEN_WIDTH * 0.8f, ICON_HEIGHT));
				vfl.attachChild(cmd_show_mission);

				Label lbl_opponent = new Label("opponent", "Optional Opponent(s):", null, paint_med_text);
				vfl.attachChild(lbl_opponent);
				Label opponent2 = new Label("opponent", "(leave blank for anyone to accept)", null, paint_normal_text);
				vfl.attachChild(opponent2);
				cmd_opponent = new Button(opponent_names, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R, Statics.SCREEN_WIDTH * 0.8f, ICON_HEIGHT));
				vfl.attachChild(cmd_opponent);

				if (selected_mission_name.toLowerCase().indexOf("beta") < 0) {
					cmd_advanced = new CheckBox("Advanced Mode?", null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R, Statics.SCREEN_WIDTH * 0.8f, ICON_HEIGHT), false);
					vfl.attachChild(cmd_advanced);
					cmd_practise = new CheckBox("Practise Game?", null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R, Statics.SCREEN_WIDTH * 0.8f, ICON_HEIGHT), false);
					vfl.attachChild(cmd_practise);
				}

				Label lbl_sides = new Label("sides", "Who should control each side?", null, paint_med_text);
				vfl.attachChild(lbl_sides);

				int sides = missions_dt.getInt("Sides");
				groups = new ToggleButtonGroup[sides+1];
				for (int b=0 ; b<sides ; b++) {
					l = new Label("side1", "Side " + (b+1) + " - " + missions_dt.getString("Side" + (b+1) + "Name") + ":", null, paint_normal_text);
					vfl.attachChild(l);

					HorizontalFlowGridLayout hfl = new HorizontalFlowGridLayout("hfl", Statics.SCREEN_WIDTH * 0.05f);
					ToggleButtonGroup grp = new ToggleButtonGroup();
					groups[b+1] = grp;

					ToggleButton you = new ToggleButton(YOU, YOU, null, null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, Statics.SCREEN_WIDTH * 0.25f, ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, Statics.SCREEN_WIDTH * 0.25f, ICON_HEIGHT));
					you.setGroup(grp);
					hfl.attachChild(you);

					ToggleButton opponent = new ToggleButton(ANOTHER_PLAYER, ANOTHER_PLAYER, null, null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, Statics.SCREEN_WIDTH * 0.25f, ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, Statics.SCREEN_WIDTH * 0.25f, ICON_HEIGHT));
					opponent.setGroup(grp);
					hfl.attachChild(opponent);

					if (missions_dt.getInt("AIForSide" + (b+1)) == 1) {
						ToggleButton ai = new ToggleButton(AI, AI, null, null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, Statics.SCREEN_WIDTH * 0.25f, ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, Statics.SCREEN_WIDTH * 0.25f, ICON_HEIGHT));
						ai.setGroup(grp);
						hfl.attachChild(ai);
					}

					vfl.attachChild(hfl);
				}

				start_game = new Button("start_game", "Start Game!", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_green, Statics.SCREEN_WIDTH * 0.9f, ICON_HEIGHT));
				vfl.attachChild(start_game);


				/*HorizontalFlowGridLayout hfl = new HorizontalFlowGridLayout("hfl", Statics.SCREEN_WIDTH * 0.05f);

				cmd_accept = new Button[sides];
				float width = (Statics.SCREEN_WIDTH * 0.9f) / (sides+1);
				float height = Statics.SCREEN_HEIGHT/7;
				for (int b=0 ; b<sides ; b++) {
					cmd_accept[b] = new Button(""+(b+1), missions_dt.getString("Side" + (b+1) + "Name"), null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R, width, height));
					cmd_accept[b].setLocation(width * b, Statics.SCREEN_HEIGHT-height);
					hfl.attachChild(cmd_accept[b]);
				}
				hfl.setLocation(0, Statics.SCREEN_HEIGHT-height);
				vfl.attachChild(hfl);*/
			}
		}

		this.root_node.attachChild(vfl);
		this.root_node.updateGeometricState();
		this.root_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		AbstractActivity act = Statics.act;
		
		//IOFunctions.Vibrate(this.act.getBaseContext(), Statics.VIBRATE_LEN);
		if (c == cmd_mission) {
			this.getThread().setNextModule(new ChooseMissionModule(act, this, missions_dt));
		} else if (c == cmd_show_mission) {
			String url = Statics.URL_FOR_CLIENT + "/dsr/missiondescriptions.cls?type=" + missions_dt.getInt("MissionID") + "&android_login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&android_pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD); 
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		} else if (c == cmd_opponent) {
			showKeyboard_New(DATA_OPPONENT_NAME, this.cmd_opponent.getText(), InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
		} else if (c == this.cmd_advanced) {
			this.cmd_advanced.toggle();
		} else if (c == this.cmd_practise) {
			this.cmd_practise.toggle();
		} else if (c instanceof ToggleButton) {
			ToggleButton tb = (ToggleButton)c;
			tb.toggeSelected();
		} else if (c == this.start_game) {
			// Check the options are valid
			int sides = missions_dt.getInt("Sides");
			int our_side=-1, ai_side=-1;
			for (int b=1 ; b<=sides ; b++) {
				ToggleButtonGroup grp = this.groups[b];
				ToggleButton tb = grp.getSelected();
				if (tb != null) {
					if (tb.getActionCommand().equals(YOU)) {
						if (our_side < 0) {
							our_side = b;
						} else {
							this.showToast("Only choose one side!");
							return;
						}
					} else if (tb.getActionCommand().equals(ANOTHER_PLAYER)) {
						// Do nothing
					} else if (tb.getActionCommand().equals(AI)) {
						ai_side = b;
					}
				} else {
					this.showToast("No option selected for side " + b);
					return;
				}
			}
			if (our_side > 0) {
				createNewGame(our_side, ai_side);
			} else {
				this.showToast("No side selected for yourself!");
			}
		} else {
			// Side selected?
			/*int sides = missions_dt.getInt("Sides");
			for (int b=0 ; b<sides ; b++) {
				if (c == cmd_accept[b]) {
					createNewGame(b+1);
				}
			}*/	
		}
	}


	private void createNewGame(int our_side, int ai_side) {
		AbstractActivity act = Statics.act;
		
		this.showPleaseWait("Creating game...");
		try {
			int advanced = 0;
			if (this.cmd_advanced != null) {
				if (this.cmd_advanced.isChecked()) {
					advanced = 1;
				}
			}
			int practise = 0;
			if (this.cmd_practise != null) {
				if (this.cmd_practise.isChecked()) {
					practise = 1;
				}
			}
			if (selected_mission_name.toLowerCase().indexOf("beta") > 0) {
				advanced = 0;
				practise = 1;
			}
			WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.CREATE_NEW_GAME + "&version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD) + "&missionid=" + missions_dt.getInt("MissionID") + "&opponent=" + AbstractCommFuncs.URLEncodeString(this.cmd_opponent.getText()) + "&side=" + our_side + "&advanced=" + advanced + "&practise=" + practise + "&aiside=" + ai_side);
			String response = wc.getResponse();
			if (response.length() == 0 || response.equalsIgnoreCase("ok")) {
				this.getThread().setNextModule(new GenericMessageModule(act, Statics.MOD_GET_PLAYERS_GAMES, "Game Being Created", "Your chosen game is now in the process of being created.  If you are not playing against the AI, you will receive an email when another player joins your game.\n\nPress the screen to continue."));
				//this.getThread().setNextModule(new GetGamesModule(act));
			} else {
				this.dismissPleaseWait();
				this.showToast(response);
			}
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
			this.dismissPleaseWait();
		} finally {
			//this.dismissPleaseWait();
		}

	}


	private void showKeyboard_New(String code, String curr_val, int hint) {
		AbstractActivity act = Statics.act;
		
		Statics.data.put("code", code);
		Statics.data.put("text", curr_val);
		act.showKeyboard(hint);
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			started(); // populate data
		}
	}

}
