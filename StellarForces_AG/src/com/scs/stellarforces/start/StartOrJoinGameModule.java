package com.scs.stellarforces.start;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractOptionsModule;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;

import com.scs.stellarforces.Statics;

import dsr.comms.AbstractCommFuncs;

public class StartOrJoinGameModule extends AbstractOptionsModule {
	
	private static final String JOIN_GAME = "Join Waiting Game";
	private static final String START_NEW_GAME = "Create New Game";
	private static final String START_PRACTISE_GAME = "Start Practise Game";
	private static final String LIST_CURRENT_GAMES = "List Current Games";
	
	private static Paint paint_text = new Paint();
	private static Paint paint_free_text = new Paint();

	static {
		paint_text.setARGB(255, 255, 255, 255);
		paint_text.setAntiAlias(true);
		//paint_text.setStyle(Style.STROKE);

		paint_free_text.setARGB(255, 200, 200, 200);
		paint_free_text.setAntiAlias(true);
		//paint_free_text.setStyle(Style.STROKE);
		//paint_free_text.setTextSize(GUIFunctions.GetTextSizeToFit(StartupModule.FREE_TEXT, Statics.SCREEN_WIDTH, -1));

	}


	public StartOrJoinGameModule(AbstractActivity act) {
		super(Statics.MOD_MENU, 1, paint_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, Statics.SCREEN_WIDTH*0.8f, Statics.SCREEN_HEIGHT/6), 0, false);

		if (Statics.GetTypeface(act) != null) {
			paint_text.setTypeface(Statics.GetTypeface(act));
			paint_free_text.setTypeface(Statics.GetTypeface(act));
		}

		this.setBackground(Statics.BACKGROUND_R);

	}

	
	@Override
	public ArrayList<String> getOptions() {
		ArrayList<String> al = new ArrayList<String>();
		al.add(JOIN_GAME);
		al.add(START_NEW_GAME);
		al.add(START_PRACTISE_GAME);
		al.add(LIST_CURRENT_GAMES);
		return al;
	}

	
	@Override
	public void optionSelected(String cmd) {
		AbstractActivity act = Statics.act;
		
		if (cmd.equalsIgnoreCase(JOIN_GAME)) {
			this.getThread().setNextModule(Statics.GetModule(Statics.MOD_LIST_AVAILABLE_GAMES));
		} else if (cmd.equalsIgnoreCase(START_NEW_GAME)) {
			this.getThread().setNextModule(Statics.GetModule(Statics.MOD_START_NEW_GAME));
		} else if (cmd.equalsIgnoreCase(START_PRACTISE_GAME)) {
			this.getThread().setNextModule(Statics.GetModule(Statics.MOD_START_PRACTISE_GAME));
		} else if (cmd.equalsIgnoreCase(LIST_CURRENT_GAMES)) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.URL_FOR_CLIENT + "/dsr/MyGames.cls?login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD)));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		}
		
	}

}
