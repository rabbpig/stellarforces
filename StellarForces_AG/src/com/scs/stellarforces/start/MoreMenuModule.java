package com.scs.stellarforces.start;

import ssmith.android.framework.ErrorReporter;
import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.layouts.GridLayout;
import ssmith.android.lib2d.shapes.BitmapRectangle;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;
import com.scs.stellarforces.start.finishedgames.MyOrAllFinishedGamesModule;
import com.scs.stellarforces.universe.UniverseMenuModule;

import dsr.comms.AbstractCommFuncs;

public class MoreMenuModule extends SimpleAbstractModule {

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.4f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT * 0.15f;

	private static final String CMD_FINISHED_GAMES = "Finished Games";
	private static final String CMD_FEEDBACK = "Send Feedback";
	private static final String CMD_RATE = "Rate";
	private static final String CMD_CAMPAIGN_MENU = "Campaign Menu"; // only show if not free version!
	private static final String CMD_UNIVERSE_MENU = "Universe Menu"; // only show if not free version!
	private static final String CMD_LEAGUE = "View League";
	private static final String CMD_RECENT_POSTS = "Recent Forum Posts";
	private static final String CMD_OPTIONS = "Options";

	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	private Button btn_feedback, btn_current_games, btn_recent_posts; 
	private Button btn_rate, btn_camp_menu, btn_uni_menu, btn_finished_games, btn_view_league, btn_options;
	private Label website;

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.055f));

	}

	
	public MoreMenuModule(AbstractActivity act) {
		super(Statics.MOD_MENU);

		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.setBackground(Statics.BACKGROUND_R);

		start();
	}


	@Override
	public void started() {
		this.stat_node.removeAllChildren();

		/*Label l = new Label("Title", "Main Menu", 0, 0, null, paint_large_text, true);
		l.setCentre(Statics.SCREEN_WIDTH/2, paint_large_text.getTextSize());
		this.stat_node.attachChild(l);
		 */
		int r = R.drawable.sf_logo2_small;
		BitmapRectangle l_title = new BitmapRectangle("Title", Statics.img_cache.getImageByKey_HeightOnly(r, Statics.SCREEN_HEIGHT * 0.2f), 0, 0);
		l_title.setCentre(Statics.SCREEN_WIDTH/2, l_title.getHeight()*.6f);
		this.stat_node.attachChild(l_title);

		GridLayout menu_node = new GridLayout("Menu", ICON_WIDTH, ICON_HEIGHT, Statics.GetButtonSpacing());

		btn_finished_games = new Button(CMD_FINISHED_GAMES, CMD_FINISHED_GAMES, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(btn_finished_games, 0, 0);

		btn_view_league = new Button(CMD_LEAGUE, CMD_LEAGUE, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(btn_view_league, 0, 1);

		btn_recent_posts = new Button(CMD_RECENT_POSTS, CMD_RECENT_POSTS, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(btn_recent_posts, 1, 0);

		btn_camp_menu = new Button(CMD_CAMPAIGN_MENU, CMD_CAMPAIGN_MENU, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(btn_camp_menu, 1, 1);

		btn_feedback = new Button(CMD_FEEDBACK, CMD_FEEDBACK, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(btn_feedback, 0, 2);

		btn_rate = new Button(CMD_RATE, CMD_RATE, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(btn_rate, 1, 2);

		btn_options = new Button(CMD_OPTIONS, CMD_OPTIONS, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(btn_options, 0, 3);

		if (Statics.UNIVERSE_MODULE) {
			btn_uni_menu = new Button(CMD_UNIVERSE_MENU, CMD_UNIVERSE_MENU, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
			menu_node.attachChild(btn_uni_menu, 1, 3);
		}

		menu_node.updateGeometricState();
		menu_node.setCentre(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT * 0.5f);
		this.stat_node.attachChild(menu_node);

		website = new Label("Website", Statics.URL_FOR_CLIENT, null, paint_normal_text);
		website.setCentre(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT - (paint_normal_text.getTextSize()));
		website.collides = true;
		stat_node.attachChild(website);

		this.stat_node.updateGeometricState();

		stat_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		AbstractActivity act = Statics.act;
		
		if (c == website) {
			Intent intent = null;
			if (Statics.LAST_LOGIN.length() > 0) {
				// Try and auto-login to website
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.URL_FOR_CLIENT + "/dsr/LoginPage.cls?login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD)));
			} else {
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.URL_FOR_CLIENT));
			}
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		} else if (c == this.btn_current_games) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.URL_FOR_CLIENT + "/dsr/MyGames.cls?android_login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&android_pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD)));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		} else if (c == this.btn_finished_games) {
			super.getThread().setNextModule(new MyOrAllFinishedGamesModule(act));
		} else if (c == this.btn_feedback) {
			StartupModule.SendFeedback(act, this);
		} else if (c == this.btn_view_league) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.URL_FOR_CLIENT + "/dsr/leaguetable.cls?android_login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&android_pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD)));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		} else if (c == this.btn_recent_posts) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.stellarforces.com/dsr/recentposts.cls")); //http://forums.stellarforces.com/viewforum.php?f=1"));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		} else if (c == this.btn_rate) {
			try {
				if (Statics.FULL_VERSION) {
					Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.MARKET_URL_FULL_VERSION));
					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					act.getBaseContext().startActivity(i);
				} else {
					Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.MARKET_URL_FREE_VERSION));
					i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					act.getBaseContext().startActivity(i);
				}
			} catch (Exception ex) {
				ErrorReporter.getInstance().handleSilentException(ex);
				this.getThread().setNextModule(new ErrorModule(act, Statics.MOD_START, "Unable to view Play", "Sorry, your phone does not seem to be able to view Google Play this way.  Please use the Google Play app."));
			}
		} else if (c == btn_camp_menu) {
			if (Statics.FULL_VERSION) {
				super.getThread().setNextModule(new CampaignMenuModule(this));
			} else {
				AbstractModule mod = Statics.GetFreeWarningModule(this);
				this.getThread().setNextModule(mod);
			}
		} else if (c == btn_uni_menu) {
			if (Statics.FULL_VERSION) {
				super.getThread().setNextModule(new UniverseMenuModule(act));
			} else {
				AbstractModule mod = Statics.GetFreeWarningModule(this);
				this.getThread().setNextModule(mod);
			}
		} else if (c == btn_options) {
			super.getThread().setNextModule(new OptionsModule(act));
		} else {
			this.returnTo();
		}

	}



}
