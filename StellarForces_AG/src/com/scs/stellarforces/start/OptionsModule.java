package com.scs.stellarforces.start;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.CheckBox;
import ssmith.android.lib2d.gui.GUIFunctions;
import ssmith.android.lib2d.layouts.HorizontalFlowGridLayout;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.graphics.Bitmap;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

public class OptionsModule extends SimpleScrollingAbstractModule {

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.6f;
	private static final float ICON_HEIGHT = Statics.SCREEN_WIDTH/10;

	private CheckBox cb_vibrate;
	private CheckBox cb_mute_sfx, cb_mute_music, cb_new_movement_icons;
	private Button save, cancel;

	private static Paint paint_normal_text = new Paint();

	static {
		paint_normal_text.setARGB(255, 0, 0, 0);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(GUIFunctions.GetTextSizeToFit("Use buttons for movemement", ICON_WIDTH, ICON_HEIGHT));//Statics.SCREEN_WIDTH * 0.75f));//Statics.GetHeightScaled(0.07f));

	}


	public OptionsModule(AbstractActivity act) { //, AbstractModule _return_to) {
		super(Statics.MOD_MORE);

		if (Statics.GetTypeface(act) != null) {
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.setBackground(Statics.BACKGROUND_R);

		Bitmap bmp_mf_yellow = Statics.img_cache.getImage(R.drawable.button_green, ICON_WIDTH, ICON_HEIGHT);
		Bitmap bmp_mf_blue = Statics.img_cache.getImage(R.drawable.button_green, ICON_WIDTH/2, ICON_HEIGHT);

		VerticalFlowLayout vfl = new VerticalFlowLayout("vfl", 5);

		boolean vibrate = Statics.VIBRATE_ON_PRESS == 1;
		cb_vibrate = new CheckBox("Vibrate on Press", null, paint_normal_text, bmp_mf_yellow, vibrate);
		vfl.attachChild(cb_vibrate);

		boolean mute_sfx = Statics.MUTE_SFX == 1;
		cb_mute_sfx = new CheckBox("Mute SFX", null, paint_normal_text, bmp_mf_yellow, mute_sfx);
		vfl.attachChild(cb_mute_sfx);

		boolean mute_music = Statics.MUTE_MUSIC == 1;
		cb_mute_music = new CheckBox("Mute Music", null, paint_normal_text, bmp_mf_yellow, mute_music);
		vfl.attachChild(cb_mute_music);

		boolean new_movement_icons = Statics.USE_NEW_MOVEMENT_ICONS == 1;
		cb_new_movement_icons = new CheckBox("New Movement Icons", null, paint_normal_text, bmp_mf_yellow, new_movement_icons);
		vfl.attachChild(cb_new_movement_icons);

		HorizontalFlowGridLayout hfl = new HorizontalFlowGridLayout("hfl", 5);

		save = new Button("Save Settings", null, paint_normal_text, bmp_mf_blue);
		hfl.attachChild(save);

		cancel = new Button("Cancel", null, paint_normal_text, bmp_mf_blue);
		hfl.attachChild(cancel);

		vfl.attachChild(hfl);

		this.root_node.attachChild(vfl);

		this.root_node.updateGeometricState();

		this.root_cam.lookAt(this.root_node, true);
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		//IOFunctions.Vibrate(this.act.getBaseContext(), Statics.VIBRATE_LEN);
		if (c instanceof CheckBox) {
			CheckBox cb = (CheckBox)c;
			cb.toggle();
		} else if (c == cancel) {
			this.returnTo();
		} else if (c == save) {
			saveSettings();
			this.returnTo();
		}

	}


	private void saveSettings() {
		AbstractActivity act = Statics.act;
		
		if (this.cb_vibrate.isChecked()) {
			Statics.VIBRATE_ON_PRESS = 1;
		} else {
			Statics.VIBRATE_ON_PRESS = 0;
		}
		if (this.cb_mute_music.isChecked()) {
			Statics.MUTE_MUSIC = 1;
			act.pauseMusic();
		} else {
			Statics.MUTE_MUSIC = 0;
			act.resumeMusic(act.getBaseContext());
		}
		if (this.cb_mute_sfx.isChecked()) {
			Statics.MUTE_SFX = 1;
		} else {
			Statics.MUTE_SFX = 0;
		}
		if (this.cb_new_movement_icons.isChecked()) {
			Statics.USE_NEW_MOVEMENT_ICONS = 1;
		} else {
			Statics.USE_NEW_MOVEMENT_ICONS = 0;
		}
		Statics.SavePrefs(act.getBaseContext());
	}



}
