package com.scs.stellarforces.start;

import java.io.IOException;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractPleaseWaitModule;

import com.scs.stellarforces.Statics;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class RegisterLoginWithServerModule extends AbstractPleaseWaitModule {
	
	public RegisterLoginWithServerModule(AbstractActivity _act) {
		super(_act, Statics.MOD_ENTER_NEW_LOGIN_FOR_REGISTER);
		
		this.setBackground(Statics.BACKGROUND_R);
	}
	
	
	@Override
	public void started() {
		start();
	}
	
	
	public void run() {
		try {
			AbstractActivity act = Statics.act;
			
			WGet_SF wc = new WGet_SF(act, this, "cmd=" + MiscCommsPage.REGISTER_LOGIN + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.data.get("email_address")) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.data.get("pwd")) + "&display_name=" + AbstractCommFuncs.URLEncodeString(Statics.data.get("display_name")));
			String response = wc.getResponse();
			if (AbstractCommFuncs.IsResponseGood(response)) {
				Statics.LAST_LOGIN = Statics.data.get("display_name").trim();
				Statics.LAST_PWD = Statics.data.get("pwd").trim();
				Statics.SavePrefs(act.getBaseContext());
				super.getThread().setNextModule(new GenericMessageModule(act, Statics.MOD_MENU, "Registered!", "Congratulations, you have successfully registered with the email address " + Statics.data.get("email_address") + ".  Press the screen to start playing games\n\nYou can also use your login and password to log onto the website at " + Statics.URL_FOR_CLIENT + " and also the forums at http://forums.stellarforces.com to ask any questions or introduce yourself."));
			} else {
				super.getThread().setNextModule(new ErrorModule(act, Statics.MOD_ENTER_NEW_LOGIN_FOR_REGISTER, "Invalid Login/Password", "Sorry, that email or display name have already been taken.  Please choose different ones."));
			}

		} catch (IOException ex) {
			//BugSenseHandler.log(Statics.NAME, ex);
			AbstractActivity.HandleError(ex);
			//this.getThread().setNextModule(new ErrorModule(this.act, Statics.MOD_LOGIN, ex));
		}

	}

	
	/*@Override
	public void handleClick(AbstractComponent c) throws Exception {
		// Do nothing
		
	}*/


}
