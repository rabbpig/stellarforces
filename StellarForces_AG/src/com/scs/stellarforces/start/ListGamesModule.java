package com.scs.stellarforces.start;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractOptionsModule2;
import ssmith.android.lib2d.gui.GUIFunctions;
import ssmith.lang.NumberFunctions;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.campaign.SelectCampaignSquadModule;
import com.scs.stellarforces.start.equip.GetEquipmentDataModule;

import dsr.data.GameData;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.GamesTable;

public final class ListGamesModule extends AbstractOptionsModule2 {

	private static final String REFRESH_CMD = "Refresh List";
	private static final String START_NEW_CMD = "Start New Game";

	private ArrayList<GameData> available_games;

	private static Paint paint_text = new Paint();
	private static Paint paint_title = new Paint();

	static {
		paint_text.setARGB(255, 255, 255, 255);
		paint_text.setAntiAlias(true);

		paint_title.setARGB(255, 200, 200, 200);
		paint_title.setAntiAlias(true);
		paint_title.setTextSize(GUIFunctions.GetTextSizeToFit(Statics.FREE_TEXT, Statics.SCREEN_WIDTH, -1));

	}


	public ListGamesModule(AbstractActivity act, ArrayList<GameData> games, boolean auto_select) { //, boolean _only_our_turn) {
		super(act, Statics.MOD_MENU, 1, paint_text, Statics.img_cache.getImage(Statics.BUTTON_R, Statics.SCREEN_WIDTH*0.9f, Statics.SCREEN_HEIGHT/6), -1, auto_select, "Select Game to Play", true);

		if (Statics.GetTypeface(act) != null) {
			paint_text.setTypeface(Statics.GetTypeface(act));
			paint_title.setTypeface(Statics.GetTypeface(act));
		}

		Statics.img_cache.clear(Statics.SQ_SIZE); // Try and save some memory

		this.setBackground(Statics.BACKGROUND_R);

		this.available_games = games;
	}


	@Override
	public void getOptions() {
		if (available_games != null) {
			for (GameData game : available_games) {
				String s = "";
				//if (Statics.list_only_our_turn) {
				if (game.haveWeEquipped() && game.haveWeDeployed() && (game.turn_side != game.our_side || game.game_status == GamesTable.GS_DEPLOYMENT)) { // Is it NOT our turn?
					continue;
				}
				/*} else { // List ALL games
					if (game.haveWeEquipped() && game.haveWeDeployed() && game.turn_side != game.our_side) { // Is it NOT our turn?
					} else {
						s = "TURN: ";

					}
				}*/
				s = s + game.getFullTitle();
				addOption(s, ""+game.game_id);
			}
		}
		if (super.getNumOfOptions() == 0) {
			this.setTitle("You currently have no turns to take!");
		} 
		if (super.getNumOfOptions() != 1 || super.auto_select == false) { // Only show this if there are no games, so that autoselect works.
			addOption(START_NEW_CMD);
			addOption(REFRESH_CMD);
		}

	}


	@Override
	public void optionSelected(int idx) {
		AbstractActivity act = Statics.act;
		
		try {
			String cmd = super.getButtonText(idx);
			if (cmd.equalsIgnoreCase(REFRESH_CMD)) {
				super.getThread().setNextModule(new GetGamesModule(act, false));
			} else if (cmd.equalsIgnoreCase(START_NEW_CMD)) {
				super.getThread().setNextModule(new StartOrJoinGameModule(act));
			} else {
				int gameid = NumberFunctions.ParseInt(super.getActionCommand(idx));
				GameData game = this.findGame(gameid);//available_games.get(idx);
				if (game.haveWeEquipped() && game.haveWeDeployed() && (game.turn_side != game.our_side || game.game_status == GamesTable.GS_DEPLOYMENT)) { // Is it NOT our turn?
					// Show game details
					super.getThread().setNextModule(new CurrentGameDetailsModule(act, this, game, false));
				} else {
					if (Statics.FULL_VERSION || AbstractMission.allowed_mission.contains(game.mission_type)) {
						if (game.has_side_equipped[game.our_side] == 0) {
							if (game.is_camp_game == 1 && game.haveWeChosenCampUnits() == false) {
								// Camp game but units not chosen yet!
								//ErrorModule mod = new ErrorModule(act, -1, "Campaign Game", "This is a campaign game, but you have not selected your squad yet.  Please visit the website and do this to continue.");
								//mod.mod_return_to = this;
								super.getThread().setNextModule(new SelectCampaignSquadModule(act, Statics.MOD_GET_PLAYERS_GAMES, game));
							} else {
								super.getThread().setNextModule(new GetEquipmentDataModule(act, game));
							}
						} else if (game.has_side_deployed[game.our_side] == 0 || game.turn_side == game.our_side) {
							super.getThread().setNextModule(new GetGameDataModule(act, game));
						} else {
							// Nothing to do
							super.getThread().setNextModule(new CurrentGameDetailsModule(act, this, game, false));
						}
					} else {
						super.getThread().setNextModule(Statics.GetFreeWarningModule(this));
					}
				}
			}
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
		}
	}


	private GameData findGame(int gameid) {
		for (GameData game : available_games) {
			if (game.game_id == gameid) {
				return game;
			}
		}
		return null;
	}


	@Override
	public void doDraw(Canvas c, long interpol) {
		super.doDraw(c, interpol);

		if (Statics.FULL_VERSION == false) {
			c.drawText(Statics.FREE_TEXT, Statics.SCREEN_WIDTH * 0.05f, Statics.SCREEN_HEIGHT - (paint_title.getTextSize()*2), paint_title);
		}
	}


}
