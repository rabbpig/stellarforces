package com.scs.stellarforces.start;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.layouts.GridLayout;
import ssmith.android.lib2d.shapes.BitmapRectangle;
import ssmith.lang.NumberFunctions;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class CampaignMenuModule extends SimpleAbstractModule {

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.4f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT * 0.15f;

	private static final String CMD_JOIN_FACTION = "Join Faction";
	//private static final String CMD_STARMAP = "Galaxy Map";

	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	private Button btn_join_faction;//, btn_starmap;

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.055f));

	}

	public CampaignMenuModule(AbstractModule _ret) {
		super(-1);

		this.mod_return_to = _ret;

		this.setBackground(Statics.BACKGROUND_R);

		start();
	}


	@Override
	public void started() {
		AbstractActivity act = Statics.act;
		
		this.stat_node.removeAllChildren();

		int r = R.drawable.sf_logo2_small;
		BitmapRectangle l_title = new BitmapRectangle("Title", Statics.img_cache.getImageByKey_HeightOnly(r, Statics.SCREEN_HEIGHT * 0.2f), 0, 0);
		l_title.setCentre(Statics.SCREEN_WIDTH/2, l_title.getHeight()*.6f);
		this.stat_node.attachChild(l_title);

		GridLayout menu_node = new GridLayout("Menu", ICON_WIDTH, ICON_HEIGHT, Statics.GetButtonSpacing());

		if (Statics.players_data_tbl != null) {
			//if (Statics.players_data_tbl.getInt("CanJoinFaction") == 1 && Statics.players_data_tbl.getInt("FactionID") <= 0) {
			if (Statics.players_data_tbl.getInt("FactionID") <= 0) {
				if (Statics.players_data_tbl.getInt("CanJoinFaction") == 1) {
					btn_join_faction = new Button(CMD_JOIN_FACTION, CMD_JOIN_FACTION, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT));
					menu_node.attachChild(btn_join_faction, 0, 0);
				} else {
					this.getThread().setNextModule(new GenericMessageModule(act, Statics.MOD_MENU, "Not Enough Experience", "I'm afraid you need to play at least 4 normal games before you can join a faction and take part in the campaign."));
					return;
				}
			} else {
				//this.getThread().setNextModule(new GenericMessageModule(act, Statics.MOD_MENU, "Please Visit the Website", "As you are already a member of a faction, please visit the website to view the Galaxy Map and select a sector to invade."));
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.URL_FOR_CLIENT + "/dsr/starmap.cls?android_login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&android_pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD)));
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				act.getBaseContext().startActivity(intent);

				return;
			}
			//btn_starmap = new Button(CMD_STARMAP, CMD_STARMAP, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT));
			//menu_node.attachChild(btn_starmap, 0, 1);

			menu_node.updateGeometricState();
			menu_node.setCentre(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT * 0.5f);
			this.stat_node.attachChild(menu_node);

			this.stat_node.updateGeometricState();

			stat_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);
		} else {
			this.returnTo(); // Since we've got no player data
		}

	}



	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		AbstractActivity act = Statics.act;
		
		if (c == this.btn_join_faction) {
			this.showPleaseWait("Please Wait...");
			WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.JOIN_CAMPAIGN + "&comms_version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
			String response = wc.getResponse();
			if (NumberFunctions.IsNumeric(response)) {
				int fid = NumberFunctions.ParseInt(response);
				Statics.players_data_tbl.setInt("FactionID", fid);
				//this.showPleaseWait("You have joined a faction!");
				this.getThread().setNextModule(new GenericMessageModule(act, Statics.MOD_MENU, "Faction Joined!", "You have successfully joined a faction.  Please visit the website to view the Galaxy Map and help your faction dominate the universe!\n\nPress the screen to return."));
			} else {
				this.showPleaseWait(response);
			}
			//super.getThread().setNextModule(Statics.GetModule(Statics.MOD_CAMP_MENU));
		} else {
			this.returnTo();
		}

	}



}
