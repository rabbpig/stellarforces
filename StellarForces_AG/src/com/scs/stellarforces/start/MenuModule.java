package com.scs.stellarforces.start;

import ssmith.android.framework.ErrorReporter;
import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.SimpleAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.layouts.GridLayout;
import ssmith.android.lib2d.shapes.BitmapRectangle;
import ssmith.dbs.SQLFuncs;
import ssmith.lang.DateFunctions;
import ssmith.lang.NumberFunctions;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.graphics.TronLine;
import com.scs.stellarforces.instructions.InstructionsModule;
import com.scs.stellarforces.main.lite.R;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class MenuModule extends SimpleAbstractModule {

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.4f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT * 0.15f;

	private static final String CMD_TAKE_TURN = "Take Turn";
	//private static final String CMD_LIST_CURRENT_GAMES = "List Current Games";
	private static final String CMD_START_NEW_GAME = "Start or Join Game";
	private static final String CMD_MESSAGES = "Messages";
	private static final String CMD_FORUM = "Forum";
	private static final String CMD_MORE = "More Options";
	private static final String CMD_INSTRUCTIONS = "Manual";

	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();
	private static Paint paint_small_text = new Paint();

	private Button btn_play_games, btn_start_new, btn_messages, btn_more, btn_instructions, btn_forum; //, btn_current_games;
	private Label website;
	private long last_time;
	private static String msg_from_server = "";

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.055f));

		paint_small_text.setARGB(255, 255, 255, 255);
		paint_small_text.setAntiAlias(true);
		paint_small_text.setTextSize(paint_normal_text.getTextSize()/2);

	}

	public MenuModule(AbstractActivity act) {
		super(Statics.MOD_START);

		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
			paint_small_text.setTypeface(Statics.GetTypeface(act));
		}

		this.setBackground(Statics.BACKGROUND_R);

		start();
	}


	@Override
	public void started() {
		this.stat_node.removeAllChildren();

		stat_node.attachChild(new TronLine());

		int r = R.drawable.sf_logo2_small;
		BitmapRectangle l_title = new BitmapRectangle("Title", Statics.img_cache.getImageByKey_HeightOnly(r, Statics.SCREEN_HEIGHT * 0.2f), 0, 0);
		l_title.setCentre(Statics.SCREEN_WIDTH/2, l_title.getHeight()*.6f);
		this.stat_node.attachChild(l_title);

		try {
			if (Statics.players_data_tbl != null) {
				Label l = new Label("Details", "Logged in as " + Statics.players_data_tbl.getString("DisplayName") + "/" + Statics.players_data_tbl.getString("Email"), 10, paint_small_text.getTextSize(), null, paint_small_text, true);
				//l.setCentre(Statics.SCREEN_WIDTH/2, paint_large_text.getTextSize());
				this.stat_node.attachChild(l);
			}
		} catch (Exception ex) {
			ErrorReporter.getInstance().handleSilentException(ex);
		}

		GridLayout menu_node = new GridLayout("Menu", ICON_WIDTH, ICON_HEIGHT, Statics.GetButtonSpacing());

		btn_play_games = new Button(CMD_TAKE_TURN, CMD_TAKE_TURN, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(btn_play_games, 0, 0);

		/*btn_current_games = new Button(CMD_LIST_CURRENT_GAMES, CMD_LIST_CURRENT_GAMES, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(btn_current_games, 1, 0);*/

		btn_start_new = new Button(CMD_START_NEW_GAME, CMD_START_NEW_GAME, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(btn_start_new, 1, 0);

		btn_messages = new Button(CMD_MESSAGES, CMD_MESSAGES, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(btn_messages, 0, 1);

		btn_forum = new Button(CMD_FORUM, CMD_FORUM, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(btn_forum, 1, 1);

		btn_instructions = new Button(CMD_INSTRUCTIONS, CMD_INSTRUCTIONS, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(btn_instructions, 0, 2);

		btn_more = new Button(CMD_MORE, CMD_MORE, 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		menu_node.attachChild(btn_more, 1, 2);

		menu_node.updateGeometricState();
		menu_node.setCentre(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT * 0.45f);
		this.stat_node.attachChild(menu_node);

		Label server_msg = new Label("server_msg", msg_from_server, null, paint_normal_text);
		server_msg.setCentre(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT - (paint_normal_text.getTextSize()*3));
		stat_node.attachChild(server_msg);

		website = new Label("Website", Statics.URL_FOR_CLIENT, null, paint_normal_text);
		website.setCentre(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT - (paint_normal_text.getTextSize()));
		website.collides = true;
		stat_node.attachChild(website);

		this.stat_node.updateGeometricState();

		stat_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);
	}



	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		AbstractActivity act = Statics.act;

		if (c == website) {
			//IOFunctions.Vibrate(this.act.getBaseContext(), Statics.VIBRATE_LEN);
			Intent intent = null;
			if (Statics.LAST_LOGIN.length() > 0) {
				// Try and auto-login to website
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.URL_FOR_CLIENT + "/dsr/LoginPage.cls?login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD)));
			} else {
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.URL_FOR_CLIENT));
			}
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		} else if (c == this.btn_play_games) {
			super.getThread().setNextModule(new GetGamesModule(act, true));
			/*} else if (c == this.btn_current_games) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Statics.URL_FOR_CLIENT + "/dsr/MyGames.cls?login=" + CommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + CommFuncs.URLEncodeString(Statics.LAST_PWD)));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			this.act.getBaseContext().startActivity(intent);*/
		} else if (c == this.btn_start_new) {
			super.getThread().setNextModule(Statics.GetModule(Statics.MOD_START_OR_JOIN_GAME));
		} else if (c == this.btn_more) {
			super.getThread().setNextModule(Statics.GetModule(Statics.MOD_MORE));
		} else if (c == this.btn_forum) {
			Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.stellarforces.com/dsr/forums/ForumMainPage.cls"));
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(i);
		} else if (c == btn_messages) {
			super.getThread().setNextModule(Statics.GetModule(Statics.MOD_LIST_MESSAGES));
		} else if (c == btn_instructions) {
			super.getThread().setNextModule(new InstructionsModule(act, this));
		} else {
			this.returnTo();
		}

	}


	public void run() {
		AbstractActivity act = Statics.act;
		
		try {
			if (System.currentTimeMillis() - last_time > DateFunctions.MINUTE*5) {
				last_time = System.currentTimeMillis();
				// Check for messages
				WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.GET_NUM_UNREAD_MESSAGES + "&version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
				String r = wc.getResponse();
				if (NumberFunctions.IsNumeric(r)) {
					int i = NumberFunctions.ParseInt(r);
					if (i > 0) {
						showToast("You have " + i + " unread message(s)!");
					}
				}
				// Get text from server
				wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.GET_MSG_FROM_SERVER + "&comms_version=" + Statics.COMMS_VERSION + "&app_version=" + Statics.VERSION_NUM + "&full=" + SQLFuncs.b201(Statics.FULL_VERSION) + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
				if (wc.getResponseCode() == 200) {
					String response = wc.getResponse();
					if (response.equalsIgnoreCase("error") == false) {
						MenuModule.msg_from_server = response;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}
