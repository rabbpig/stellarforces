package com.scs.stellarforces.start;

import java.io.IOException;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.AbstractPleaseWaitModule;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;

import dsr.comms.GetAllGameData;
import dsr.data.GameData;

/**
 * After this has run, it creates a new GameModule and goes to it!
 *
 */
public final class GetGameDataModule extends AbstractPleaseWaitModule {

	private GameData game;
	private AbstractModule game_details;
	private boolean cancelled = false;

	public GetGameDataModule(AbstractActivity _act, GameData _game) {
		super(_act, Statics.MOD_MENU);

		game = _game;

		this.setBackground(Statics.BACKGROUND_R);

		start();
	}


	@Override
	public void started() {
		AbstractActivity act = Statics.act;
		
		// Show game details while we're waiting for the data to load, but only once
		if (game_details == null) {
			game_details = new CurrentGameDetailsModule(act, this, game, true);
			super.getThread().setNextModule(game_details);
		}
	}


	@Override
	public boolean onBackPressed() {
		this.cancelled = true;
		return super.onBackPressed();
	}


	public void run() {
		AbstractActivity act = Statics.act;
		
		try {
			GetAllGameData setup = new GetAllGameData(act, game, false, this);
			if (this.cancelled == false) {
				if (setup.wasSuccessful()) {
					act.pauseMusic();
					super.getThread().setNextModule(new GameModule(act, setup));
				} else {
					super.getThread().setNextModule(new ErrorModule(act, Statics.MOD_LOGIN, "Error", "Bad data from server."));
				}
			}
		} catch (IOException ex) {
			AbstractActivity.HandleError(ex);
		}

	}


}
