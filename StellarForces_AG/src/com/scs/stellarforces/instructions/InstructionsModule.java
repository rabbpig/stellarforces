package com.scs.stellarforces.instructions;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

/**
 * Todo - Trophies, Scanner, colonization, enemy unit icons
 * todo - add \n\n* Enemy units are only shown on the scanner when they can be seen by one of your units.
 *
 */
public class InstructionsModule extends AbstractInstructionsList {

	private static final String OVERVIEW = "Overview";
	private static final String STARTING = "Starting a Game";
	private static final String MOVEMENT = "Movement";
	private static final String SHOOTING = "Shooting";
	private static final String COMBAT = "Close Combat";
	private static final String OPP_FIRE = "Opportunity Fire";
	private static final String IN_GAME_MENU = "In-Game Menu";
	private static final String HEARING = "Hearing Enemies";
	private static final String DEPLOYMENT = "Deployment";
	private static final String HINTS_TIPS = "Hints & Tips";
	private static final String UNIT_TYPES = "Unit Types";
	private static final String GRENADES = "Grenades";
	private static final String THROWING_CATCHING = "Throwing & Catching";
	private static final String ENDING_GAME = "Ending a Game";
	private static final String SNAFU = "SNAFU Missions";
	private static final String UNIT_STATS = "Unit Stats";
	private static final String REMINDERS = "Reminders";
	private static final String AP_LOCK = "AP Lock";
	//private static final String ALIEN_BIOLOGY = "Alien Biology";
	//private static final String SPECIAL_POWERS = "Special Powers";
	private static final String CAMP_GAMES = "Campaign Games";
	private static final String ADVANCED_MODE = "Advanced Mode";
	private static final String SKILLS = "Skills";

	public InstructionsModule(AbstractActivity act, AbstractModule return_to) {
		super(act, return_to, new Paint());

		this.mod_return_to = return_to;
	}


	@Override
	public void started() {
		super.setOptions(); // Need this to recalc font sizes after coming back from 
	}


	@Override
	public void getOptions() {
		this.addOption(OVERVIEW);
		this.addOption(STARTING);
		this.addOption(DEPLOYMENT);
		this.addOption(MOVEMENT);
		this.addOption(SHOOTING);
		this.addOption(OPP_FIRE);
		this.addOption(COMBAT);
			this.addOption(GRENADES);
			this.addOption(THROWING_CATCHING);
		this.addOption(IN_GAME_MENU);
		this.addOption(HEARING);
		this.addOption(AP_LOCK);
		this.addOption(ENDING_GAME);
			this.addOption(UNIT_TYPES);
		this.addOption(UNIT_STATS);
		this.addOption(SKILLS);
		this.addOption(ADVANCED_MODE);
		this.addOption(SNAFU);
		this.addOption(CAMP_GAMES);
		this.addOption(HINTS_TIPS);
	}


	@Override
	public void optionSelected(int idx) {
		AbstractActivity act = Statics.act;

		String cmd = super.getActionCommand(idx);
		if (cmd.equalsIgnoreCase(HEARING)) {
			String text = "* 'Hearing' is only active in Advanced Mode games.\n\n* If this is selected, then opponents will be informed if an enemy units is within 3 squares (10 for the Alien Queen) of a friendly unit.  You will need to view the Scanner to see where the sound came from\n\n* In addition, each weapon has a noise radius. A unit's location will be revealed if they shoot their weapon and a friendly unit is within that range.\n\n* Note: Aliens never make a sound and their location is never revealed in this way.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Hearing Enemies", text));
		} else if (cmd.equalsIgnoreCase(OVERVIEW)) {
			String text = "* This game is turn-based, so you take turns with your human or AI opponent.\n\n* You will be sent an email to inform you when it is your turn.\n\n* Depending on the mission, you will have an objective. The winner is the first person to complete this objective and get 100 victory points.\n\n* A game can also be won by killing all of your opponents units.\n\nYou can play as many concurrent games as you like.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Overview", text));
		} else if (cmd.equalsIgnoreCase(MOVEMENT)) {
			String text = "* Each of your units has a certain number of Action Points (APs) which they can spend to perform actions such as move, shoot etc…\n\n* Units always face in one of 8 directions (i.e. 45 degree angles). The direction a unit is facing determines what they can see and where they can shoot.\n\n* If a unit has more than half their APs remaining at the end of your turn (this is the number in brackets shown to the right of a unit's current APs), they will automatically shoot at an enemy that becomes visible during your opponents turn. This is called ”Opportunity Fire”.\n\n* Normal movement costs 4 APs for horizontal and vertical, and 6 APs for diagonal.\n\n* Walking through doors doubles the AP cost, as does walking backwards.\n\n* Turning 45 degress costs 1 AP.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Movement", text));
		} else if (cmd.equalsIgnoreCase(STARTING)) {
			String text = "* You start a game by either creating a game for another player to join, or joining an existing game that it waiting for a player.\n\n* If you create a new game, you will need to wait for another player to join before you can take a turn.  You will receive an email when another player has joined your game.\n\n";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Starting a Game", text));
		} else if (cmd.equalsIgnoreCase(COMBAT)) {
			String text = "To attack using Close Combat, simply try to “walk into” the enemy unit you wish to attack. Close combat takes 3 times as many APs as normal movement, so it is 12 APs when straight on, and 18 APs diagonally. If you have enough action points left, they will be deducted from your action point total.\n\nMelee is calculated as follows:-\n\nThe attackers combat skill is increased by any melee weapon they might be carrying, and that total is doubled. The defenders combat skill is reduced depending on the angle of attack (halved when attacked from the side, quartered when attacked from the rear). The defenders combat skill is then subtracted from the attackers giving a total. A random number is chosen from 1-100. If that is less than the total, the attack has been succesful.\n\nThe amount of damage inflicted is the attackers strength, plus any extra damage from a melee weapon. This is then randomized from 50% - 150%, and finally the defenders armour rating is subtracted.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Combat", text));
		} else if (cmd.equalsIgnoreCase(OPP_FIRE)) {
			String text = "* This is sometimes called overwatch.  If your unit has at least half their AP's remaining at the end of a turn (shown as the number in brackets), they will get opportunity fire.\n\n* If a unit has opportunity fire, then during your opponents turn, they will automatically shoot at any enemy units that appear in their view during your opponents turn, until all their remaining APs are used up. The accuracy and AP cost is the same as for a snap-shot.\n\n* They will NOT shoot at any opponents that they could already see at the end of your turn.  Nor will a unit use their opportunity fire if there is a friendly unit visible in an adjacent square.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Opportunity Fire", text));
		} else if (cmd.equalsIgnoreCase(HINTS_TIPS)) {
			String text = "* At the start of your turn, always consult the Game Log and the Scanner to see what happened during your opponent's turn.\n\n* When using a grenade, if you set the timer to zero, ensure you have enough APs to throw it!\n\n* If you stand in a door, your opponent will know you are there as the door will stay open.\n\n* Don't wait around a corner on opportunity fire: your shots will probably hit the corner of the wall.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Hints & Tips", text));
		} else if (cmd.equalsIgnoreCase(SHOOTING)) {
			String text = "* Select the Shoot icon to enter shooting mode. This icon is only visible if the unit is currently using a gun.\n\n* This will make a line appear showing the direction you wish to shoot in. You can move this line by moving the camera.\n\n* A unit can only shoot in the general direction that they are facing.\n\n* There are 3 different kinds of shot (aimed, snap and auto). Each one costs a different amount of APs but each have different chances of success (shown on the icons). The chance of success is the chance that the shot will be perfectly accurate (although even “perfectly accurate” shots are adjusted by a random amount from 0 - 0.4 degrees).\n\n* Click on the shot type to make the actual shot.\n\n* To determine the actual angle of a shot, the computer chooses a random number from 0-100. If this is below the % chance of success, the shot is perfectly accurate. If the number is above the % chance of success, the angle is adjusted by a random amount from 2 - how much higher the random number was. So for example, if the random number was 95, and the chance of success was 80%, then the angle will be adjusted by a random amount from 2 - 15. Whether this is to the left or right is also chosen randomly.\n\nShot Accuracy Calculation\n\nThe accuracy of the 3 different types of shot are worked out as follows:-\n\nAimed Shot: Unit Shot Skill + Weapon Aimed Accuracy\nSnap Shot: (Unit Shot Skill / 2) + Weapon Aimed Accuracy\nAuto Shot: (Unit Shot Skill / 3) + Weapon Aimed Accuracy\n\nThere is also a set minimum accuracy of 15 and a set maximum accuracy of 85.\n\nDamage\n\n* The damage that a bullet will inflict is affected by many factors:- the damage rating of the weapon, the protection the target is wearing, the angle that the bullet hits the target, and also the distance the shot travels.\n\n* First of all, the damage of the weapon is adjusted by a random amount from 50%-150%.\n\n* Then the armour protection of the target is subtracted, taking into account the angle that the shot hit them.\n\n* For shot at point blank range (i.e. the units are in adjacent squares) there is a 10-point damage bonus. For shots over 5 squares, non-laser weapon shots lose 1 point of damage for each square up to a maximum of half the damage.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Shooting", text));
		} else if (cmd.equalsIgnoreCase(UNIT_TYPES)) {
			this.getThread().setNextModule(new UnitTypesListModule(act, this));
		} else if (cmd.equalsIgnoreCase(THROWING_CATCHING)) {
			String text = "* A unit can thrown an item a maximum distance of the unit's strength minus the item weight, squares.\n\n* The distance and direction is adjusted by a random amount based on the unit's combat skill.\n\n* If a thrown item reaches a unit that can see the thrower, and they are not holding an item, they will catch the item and it will become their current item. This costs them 3 APs.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Throwing & Catching", text));
		} else if (cmd.equalsIgnoreCase(ENDING_GAME)) {
			String text = "* A game will end when either side has 100 VP's, or only one side has any units remaining.\n\n* If a mission is 2-player, you can propose to mutually conceed a game, which, if accepted, will make the game a draw. This option is on the website under Game Details.\n\n* You can conceed a game when at least half of your units have been killed. This option is on the website under Game Details.\n\n* Each mission has it's own victory conditions that is used to decide the winner.  See the mission descriptions on the website for specific details.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Ending a Game", text));
		} else if (cmd.equalsIgnoreCase(GRENADES)) {
			String text = "* Grenades must be primed before they will explode.\n\n* The number of units they are primed for equates to the number of player-turns before it explodes. For example, if you are in a 2-player game and want the grenade to explode at the end of your next turn, you would set it to 2. That is because you will end your turn (1) and then your opponent will end their turn (1), and at the end of your next turn the grenade timer will be on 0.\n\n* There are three main types of grenade: incendiary, nerve gas and smoke grenade.\n\n* There is a 50% chance that an incendiary grenade will cause another grenade to explode if it is caught in the blast area, even if being carried by a unit.\n\nStandard Grenade Damage\n\n* The distance from a grenade makes no difference (unless the unit is outside the range of the blast of course). Neither do walls provide any extra protection, assuming walls can be destroyed in the mission.\n* The grenade damage is randomly adjusted by 50% - 150%\n* The protection afforded by armour is adjusted by the angle of the blast and then subtracted from the damage.\n* The remaining amount is subtracted from the units health.\n\nNerve Gas Grenade Damage\n\n* Any unit that uses any APs inside a nerve gas cloud (unless it's during their opponents turn) will have the equivalent amount reduced from their health. For example, if a unit walks into a nerve gas cloud and then turns twice, they will lose 6 HP.\n* Any unit inside a nerve gas cloud at the end of a turn will lose HP equivalent to their remaining APs. For example, if a unit has 10 APs left at the end of a turn and they are inside a nerve gas cloud, they will lose 10 HP.\n\nSmoke Grenades\nThese will not cause any harm, but units cannot see through any square of smoke.\n\nDeath Grenades\nThese are unlike other grenades; they are designed to ONLY explode when the holder dies, enabling a last minute piece of vengeance.  Beware of nearby friendly units though!";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Grenades", text));
		} else if (cmd.equalsIgnoreCase(SNAFU)) {
			String text = "* SNAFU missions are only recommended for experienced players.\n\n* A SNAFU mission is one where the actual enemy is unknown by everyone except themselves, and must be discovered by examining their actions.  The game details on the website will tell you if you are the enemy and what your objective is.\n\n* SNAFU missions are always played in Advanced Mode.\n\n* To prevent the early identification of the enemy, no VP's are shown until the end, and the number of enemy units remaining is not revealed.\n\n* Opportunity fire must be selected (by side) for your units to shoot at the enemy or attack them. This is currently changed on the website in the game details page. Please note that if you change it because it is the middle of your turn and you wish to attack a unit in close combat, you currently have to restart your client for it to take effect.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "SNAFU Missions", text));
		} else if (cmd.equalsIgnoreCase(UNIT_STATS)) {
			String text = "* Strength: This determines how far an item can be thrown (the unit's strength minus the item weight) and how much damage is caused in close combat.\n* Health: How much harm they can take before they die. Units on < 3rd health have their APs reduced by half.\n* Combat Skill: How good they are at close-combat.\n* Max APs: This is how many APs the unit starts with each turn before it is modified by Burden.\n* Strength: This affects how much damage an unarmed unit can cause in close combat, how far a unit can throw an item, and how heavy an item can be for a unit to pick it up.\n* Burden: This is the total weight of all the unit's equipment, and is subtracted from their APs at the start of each turn.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Unit Stats", text));
		} else if (cmd.equalsIgnoreCase(DEPLOYMENT)) {
			String text = "* You can deploy your units on any of the yellow/orange squares.  Simply press that square and your unit will appear.\n\n* The blue squares are where your opponent(s) can deploy.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Deployment", text));
		} else if (cmd.equalsIgnoreCase(REMINDERS)) {
			String text = "* If you do not take your turn within a certain number of days, you will be emailed a reminder.  After 14 days your opponent can force a concede of the game.\n\n* If no turn is taken for 21 days, it may be automatically cancelled.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, cmd, text));
		} else if (cmd.equalsIgnoreCase(AP_LOCK)) {
			String text = "* AP Lock is designed to prevent you from spending too many APs and not having opportunity fire.\n\n* If AP Lock is activated, your unit will not be able to spend more than half their APs, in order to ensure that they have enough for opportunity fire.\n\n* AP Lock can be toggled by pressing the AP Lock button.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, cmd, text));
		} else if (cmd.equalsIgnoreCase(IN_GAME_MENU)) {
			String text = "The in-game menu has the following options:\n\nGame Log\nThis will describe what has happened in the game so far.  This is very useful for knowing what happened during your opponent's turn.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, cmd, text));
		} else if (cmd.equalsIgnoreCase(CAMP_GAMES)) {
			String text = "* In order to play a Campaign game, you need to join a faction via the website.\n\n* Once you are part of a faction, you can go to the Galaxy Map and choose a sector to attack or defend (if it's under attack).\n\n* Campaign games are automatically conceded after only 7 days.\n\n* Before you elect to attack or defend, ensure you have enough units in your squad.\n\n* Once a campaign game is started, you must first select the units from your squad that you wish to deploy.\n\nUnits that survive campaign games get an increase in their stats, more so if they actually win.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, cmd, text));
		} else if (cmd.equalsIgnoreCase(ADVANCED_MODE)) {
			String text = "* Playing a mission in advanced mode changes a few features:-\n\n* Units can be heard, by when they move or shoot.  If you hear an enemy units, you will be told in the log, and you can view where they were heard in the Scanner.\n\n* Equipment will only be shown on the map if one of your units has seen it.\n\n* Some units have skills which will affect the game.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, cmd, text));
		} else if (cmd.equalsIgnoreCase(SKILLS)) {
			String text = "If a game is in advanced mode, some randomly selected units will have special skills.  Both sides will have an equal number of skilled units.\n\n* Medic - These can heal people without the need for a medikit.  However, they can only heal units back to half-health\n\n* Stealth - This unit will not be heard by enemy units.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, cmd, text));
		} else {
			throw new RuntimeException("Unknown topic:" + cmd);
		}
	}


}
