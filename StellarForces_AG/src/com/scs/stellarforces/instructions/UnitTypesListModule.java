package com.scs.stellarforces.instructions;

import com.scs.stellarforces.Statics;

import android.graphics.Paint;
import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;

public class UnitTypesListModule extends AbstractInstructionsList {

	public static final String ALIENS = "Aliens";
	public static final String BLOBS = "Blobs";
	public static final String HUMANS = "Humans";
	public static final String ZOMBIES = "Zombies";
	public static final String ANGELS = "Angels";

	public UnitTypesListModule(AbstractActivity act, AbstractModule return_to) {
		super(act, return_to, new Paint());
	}

	
	@Override
	public void getOptions() {
		this.addOption(HUMANS);
		this.addOption(ALIENS);
		this.addOption(BLOBS);
		this.addOption(ZOMBIES);
		this.addOption(ANGELS);
	}


	@Override
	public void optionSelected(int idx) {
		AbstractActivity act = Statics.act;
		
		String cmd = super.getActionCommand(idx);
		if (cmd.equalsIgnoreCase(HUMANS)) {
			String text = "* Humans can use equipment.\n\n* They have 90 degree vision.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Puny Humans", text));
		} else if (cmd.equalsIgnoreCase(ALIENS)) {
			String text = "* Aliens cannot use equipment.\n\n* Aliens can only attack using melee/close combat.\n\n* Aliens are NOT harmed by nerve gas.\n\n* Aliens have 180 degree vision.\n\n* In some missions, aliens can impregnate. This means that if they kill a human, there will be a new alien on the next turn.\n\nAliens explode in a ball of acid when killed, of between 1-3 squares (2-4 for the Queen).\n\n* The Queen has particularly sensitive hearing and can hear twice the distance of other units.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Aliens", text));
		} else if (cmd.equalsIgnoreCase(BLOBS)) {
			String text = "* Blobs cannot use equipment.\n\n* They can walk into each to merge. This creates a single blob twice as powerful and twice the size.\n\n* Blobs can split into two smaller blobs. However, the new blob, which is placed in a random adjacent square, cannot do anything until the next turn.\n\n* Blobs can absorb corpses to give them strength.\n\n* Blobs can explode, which will kill them and harm units around them. The damage of the explosion is equivalent to the blob's strength x 2, and the radius is the strength / 10. For example, a blob of strength 100 will explode with a radius of 10 and a damage of 200.\n\n* Blobs have 360 degree vision, and it doesn't cost a blob any APs to turn.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Blobs", text));
		} else if (cmd.equalsIgnoreCase(ZOMBIES)) {
			String text = "* Zombies are just like normal human units.  The only difference is that at the end of every turn, there is a 1 in 3 chance that a dead zombie will reanimate.";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Zombies", text));
		} else if (cmd.equalsIgnoreCase(ANGELS)) {
			String text = "* Angels are indestructible.  However. they cannot move when an enemy unit can see them.\n\n* Angels are not affected by nerve gas or fire. ";
			this.getThread().setNextModule(new AbstractInstructionsPage(act, this, "Angels", text));
		}

	}

}
