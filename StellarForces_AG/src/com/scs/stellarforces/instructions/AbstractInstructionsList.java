package com.scs.stellarforces.instructions;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.AbstractOptionsModule2;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public abstract class AbstractInstructionsList extends AbstractOptionsModule2 {
	
	public AbstractInstructionsList(AbstractActivity act, AbstractModule return_to, Paint paint_menu_text) {
		super(act, -1, 1, paint_menu_text, Statics.img_cache.getImage(Statics.BUTTON_R, Statics.SCREEN_WIDTH*0.8f, Statics.SCREEN_HEIGHT/7), -1, false, "Instructions", false);
		
		this.mod_return_to = return_to;

		paint_menu_text.setARGB(255, 255, 255, 255);
		paint_menu_text.setAntiAlias(true);
		//paint_menu_text.setStyle(Style.STROKE);
		paint_menu_text.setTextSize(Statics.SCREEN_HEIGHT * 0.08f);

		if (Statics.GetTypeface(act) != null) {
			paint_menu_text.setTypeface(Statics.GetTypeface(act));
		}

		this.setBackground(Statics.BACKGROUND_R);

	}

}
