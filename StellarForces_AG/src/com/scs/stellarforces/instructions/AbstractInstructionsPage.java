package com.scs.stellarforces.instructions;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractComplexModule;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import ssmith.android.lib2d.shapes.Geometry;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public class AbstractInstructionsPage extends AbstractComplexModule {
	
	public static Paint paint_menu_text = new Paint();
	private static Paint paint_small_text = new Paint();

	static {
		paint_menu_text.setARGB(255, 255, 255, 255);
		paint_menu_text.setAntiAlias(true);
		//paint_menu_text.setStyle(Style.STROKE);
		paint_menu_text.setTextSize(Statics.SCREEN_HEIGHT * 0.09f);

		paint_small_text.setARGB(255, 255, 255, 255);
		paint_small_text.setAntiAlias(true);
		//paint_small_text.setStyle(Style.STROKE);
		paint_small_text.setTextSize(Statics.SCREEN_HEIGHT * 0.06f);
	}


	public AbstractInstructionsPage(AbstractActivity act, AbstractModule _return_to, String title, String text) {
		super(-1);
		
		if (Statics.GetTypeface(act) != null) {
			paint_menu_text.setTypeface(Statics.GetTypeface(act));
			paint_small_text.setTypeface(Statics.GetTypeface(act));
		}

		this.mod_return_to = _return_to;
		this.dont_scroll_lr = true;
		
		String forum = forum = "http://forums.stellarforces.com";
		text = text + "\n\n--\nIf you cannot find the answer to your questions about the game here, please visit the forums at " + forum;
		
		this.setBackground(Statics.BACKGROUND_R);

		VerticalFlowLayout vfl = new VerticalFlowLayout("vfl", Statics.SCREEN_WIDTH * 0.01f);
		Label l = new Label(title, title, null, paint_menu_text);
		vfl.attachChild(l);
		MultiLineLabel mll = new MultiLineLabel(text, text, null, paint_small_text, true, Statics.SCREEN_WIDTH * 0.8f);
		mll.setLocation(Statics.SCREEN_WIDTH * 0.1f, 0f);
		vfl.attachChild(mll);
		
		this.root_node.attachChild(vfl);
		this.root_node.updateGeometricState();
		this.root_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);
		
	}
	

	@Override
	public boolean componentClicked(Geometry c) {
		return false;
	}
	

	@Override
	public void updateGame(long interpol) {
		// Do nothing
		
	}

}
