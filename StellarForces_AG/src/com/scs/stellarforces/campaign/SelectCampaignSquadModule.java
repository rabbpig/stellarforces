package com.scs.stellarforces.campaign;

import java.io.IOException;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.ConfirmModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.Spatial;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.CheckBox;
import ssmith.android.lib2d.gui.ToggleButton;
import ssmith.android.lib2d.layouts.HorizontalFlowGridLayout;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;
import com.scs.stellarforces.start.CurrentGameDetailsModule;
import com.scs.stellarforces.start.equip.GetEquipmentDataModule;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsr.data.GameData;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class SelectCampaignSquadModule extends SimpleScrollingAbstractModule {

	private static final String CONFIRM_SQUAD = "conf_squad";

	private static final float GAP = Statics.SCREEN_WIDTH * 0.005f;

	private GameData game_data;
	private static Paint paint_normal_text = new Paint();
	private DataTable camp_squad_dt;
	private Button finished, finished2, game_details, mission_file;
	private VerticalFlowLayout vfl;

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.07f));

	}


	public SelectCampaignSquadModule(AbstractActivity act, int _ret, GameData _game_data) {
		super(_ret);

		game_data = _game_data;

		this.setBackground(Statics.BACKGROUND_R);

		this.showPleaseWait("Getting squad roster...");
		try {
			WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.GET_CAMP_SQUAD_LIST + "&comms_version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
			String response = wc.getResponse();
			if (response != null && response.length() > 0) {
				if (response.equalsIgnoreCase("error") == false) {
					camp_squad_dt = new DataTable(response);
				} else {
					throw new IOException("Invalid data for campaign squad");
				}
			} else {
				throw new IOException("Invalid data for campaign squad");
			}
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
			return;
		} finally {
			this.dismissPleaseWait();
		}

		this.root_node.removeAllChildren();

		vfl = new VerticalFlowLayout("vfl", GAP);

		finished = new Button("Deploy Selected Units", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_green, Statics.SCREEN_WIDTH, Statics.SCREEN_HEIGHT/7f));
		vfl.attachChild(finished);


		//CampUnitSelectionControl unit_ctrl = new CampUnitSelectionControl(GAP, 0, "Name", "Hlth", "APs", "Shot", "CC", "Str", "Nrg", "Mrl");
		HorizontalFlowGridLayout heading = new HorizontalFlowGridLayout("heading", GAP);
		Button tb_unit_name = new Button("Name", null, CampUnitSelectionControl.paint_unit_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.OVERALL_WIDTH * 0.6f, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		//tb_unit_name.setSize(OVERALL_WIDTH * 0.6f, ARMOUR_ICON_HEIGHT);
		heading.attachChild(tb_unit_name);

		// why are these togglebuttons?
		ToggleButton b = new ToggleButton("Hlth", null, null, CampUnitSelectionControl.paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		heading.attachChild(b);
		b = new ToggleButton("APs", null, null, CampUnitSelectionControl.paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		heading.attachChild(b);
		b = new ToggleButton("Shot", null, null, CampUnitSelectionControl.paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		heading.attachChild(b);
		b = new ToggleButton("CC", null, null, CampUnitSelectionControl.paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		heading.attachChild(b);
		b = new ToggleButton("Str", null, null, CampUnitSelectionControl.paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		heading.attachChild(b);
		b = new ToggleButton("Nrg", null, null, CampUnitSelectionControl.paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		heading.attachChild(b);
		b = new ToggleButton("Mrl", null, null, CampUnitSelectionControl.paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		heading.attachChild(b);
		heading.updateGeometricState(); // to get size
		vfl.attachChild(heading);

		camp_squad_dt.moveBeforeFirst();
		//int num = 1;
		while (camp_squad_dt.moveNext()) {
			CampUnitSelectionControl unit_ctrl = new CampUnitSelectionControl(GAP, camp_squad_dt.getInt("CampUnitID"), camp_squad_dt.getString("Name"), ""+camp_squad_dt.getInt("MaxHealth"), ""+camp_squad_dt.getInt("MaxAPs"), ""+camp_squad_dt.getInt("ShotSkill"), ""+camp_squad_dt.getInt("CombatSkill"), ""+camp_squad_dt.getInt("Strength"), ""+camp_squad_dt.getInt("MaxEnergy"), ""+camp_squad_dt.getInt("MaxMorale"));
			vfl.attachChild(unit_ctrl);
			//num++;
		}
		vfl.updateGeometricState();

		HorizontalFlowGridLayout hfgl = new HorizontalFlowGridLayout("hfgl", Statics.SCREEN_WIDTH * 0.02f);

		game_details = new Button("Game Details", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, Statics.SCREEN_WIDTH*0.47f, Statics.SCREEN_HEIGHT/7f));
		hfgl.attachChild(game_details);
		//hfgl.updateGeometricState();

		mission_file = new Button("Mission File", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, Statics.SCREEN_WIDTH*0.47f, Statics.SCREEN_HEIGHT/7f));
		hfgl.attachChild(mission_file);
		hfgl.updateGeometricState();
		
		vfl.attachChild(hfgl);

		finished2 = new Button("Deploy Selected Units", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_green, Statics.SCREEN_WIDTH, Statics.SCREEN_HEIGHT/7f));
		vfl.attachChild(finished2);

		this.root_node.attachChild(vfl);
		this.root_node.updateGeometricState();
		this.root_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);

	}


	@Override
	public void started() {
		// Have we come back from "confirmed"?
		if (Statics.data.containsKey(CONFIRM_SQUAD)) {
			String s = Statics.data.get(CONFIRM_SQUAD);
			Statics.data.clear();
			if (s.equalsIgnoreCase("yes")) {
				this.finish();
			}
		}
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		AbstractActivity act = Statics.act;
		
		if (c instanceof CheckBox) {
			CheckBox cb = (CheckBox)c;
			cb.toggle();
		} else if (c == game_details) {
			CurrentGameDetailsModule mod = new CurrentGameDetailsModule(act, this, game_data, false);
			this.getThread().setNextModule(mod);
		} else if (c == mission_file) {
			String url = Statics.URL_FOR_CLIENT + "/dsr/missiondescriptions.cls?type=" + game_data.mission_type + "&android_login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&android_pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD); 
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		} else if (c == finished || c == finished2) {
			//finish();
			confirmIfFinished();
		}

	}


	private void confirmIfFinished() {
		AbstractActivity act = Statics.act;
		
		this.getThread().setNextModule(new ConfirmModule(act, this, "Are You Sure?", "Are you sure you have definitely finished selecting your units?  You can't go back afterwards!", Statics.BACKGROUND_R, CONFIRM_SQUAD));
	}


	private void finish() {
		AbstractActivity act = Statics.act;
		
		// Tell the server who we've selected
		StringBuffer str = new StringBuffer();
		int count = 0;
		for (Spatial s : vfl.getChildren()) {
			if (s instanceof CampUnitSelectionControl) {
				CampUnitSelectionControl cb = (CampUnitSelectionControl)s;
				if (cb.isChecked()) {
					if (cb.campunitid <= 0) {
						throw new RuntimeException("No cb.campunitid!");
					}
					str.append(cb.campunitid + ",");
					count++;
				}
			}
		}
		if (count >= this.game_data.min_camp_units[this.game_data.our_side]) {
			if (count <= this.game_data.max_camp_units[this.game_data.our_side] && this.game_data.max_camp_units[this.game_data.our_side] > 0) {
			try {
				WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.SEND_CAMP_SQUAD_SELECTION + "&comms_version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD) + "&gameid=" + this.game_data.game_id + "&side=" + game_data.our_side + "&data=" + str.toString());
				String response = wc.getResponse();
				if (AbstractCommFuncs.IsResponseGood(response)) {
					super.getThread().setNextModule(new GetEquipmentDataModule(act, game_data));
				} else {
					throw new IOException("Error from server: " + response);
				}
			} catch (Exception ex) {
				AbstractActivity.HandleError(ex);
			}
			} else {
				this.showToast("You can only select a maximum of " + this.game_data.max_camp_units[this.game_data.our_side] + " unit(s)");
			}
		} else {
			this.showToast("You must select at least " + this.game_data.min_camp_units[this.game_data.our_side] + " unit(s)");
		}

	}


}

