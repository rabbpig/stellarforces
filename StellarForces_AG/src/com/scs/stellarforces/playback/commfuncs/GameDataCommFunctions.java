package com.scs.stellarforces.playback.commfuncs;

import ssmith.android.framework.ErrorReporter;
import ssmith.lang.NumberFunctions;

import com.scs.stellarforces.playback.PlayBackGameData;
import com.scs.stellarforces.playback.ServerMapSquare;

import dsr.data.UnitData;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitsTable;

public class GameDataCommFunctions {

	public static final String GET_PLAYBACK_GAME_DATA2 = "getgamedata2";

	// Called by the applet to request the data
	public static String GetGameDataRequest(int game_id) {
		return "cmd=" + GET_PLAYBACK_GAME_DATA2 + "&gid=" + game_id;
	}


	// --------------------------------------------------------------------

	// Called by the applet to decode the data
	public static PlayBackGameData DecodeResponse(String response) {
		if (response.length() > 0) {
			PlayBackGameData game_data = new PlayBackGameData();
			String data[] = response.split("\\|", -1);
			game_data.max_turns = NumberFunctions.ParseInt(data[0]);
			game_data.max_events = NumberFunctions.ParseInt(data[1]);
			game_data.total_sides = NumberFunctions.ParseInt(data[2]);
			game_data.map_width = NumberFunctions.ParseInt(data[3]);
			game_data.units = new UnitData[NumberFunctions.ParseInt(data[4])];
			int cell = 5;

			for (int i=0 ; i<game_data.units.length ; i++) {
				game_data.units[i] = new UnitData(NumberFunctions.ParseInt(data[cell+2]));
				game_data.units[i].setStatus(UnitsTable.ST_AWAITING_DEPLOYMENT);
				game_data.units[i].unitid = NumberFunctions.ParseInt(data[cell]);
				game_data.units[i].setSide(NumberFunctions.ParseByte(data[cell+1]));
				game_data.units[i].order_by = NumberFunctions.ParseInt(data[cell+2]);
				game_data.units[i].map_x = NumberFunctions.ParseByte(data[cell+3]);
				game_data.units[i].map_y = NumberFunctions.ParseByte(data[cell+4]);
				game_data.units[i].angle = NumberFunctions.ParseInt(data[cell+5]);
				game_data.units[i].model_type = NumberFunctions.ParseByte(data[cell+6]);
				cell += 7;
			}

			game_data.map = new ServerMapSquare[game_data.map_width][game_data.map_width];
			for (byte y=0 ; y<game_data.map_width ; y++) {
				for (byte x=0 ; x<game_data.map_width ; x++) {
					game_data.map[x][y] = new ServerMapSquare(MapDataTable.MT_FLOOR, x, y, (byte)0, false); // Default to floor
				}			
			}

			// Walls
			int squares = NumberFunctions.ParseInt(data[cell+1]);
			cell+=2;
			for (int i=0 ; i<squares ; i++) {
				try {
					int x = NumberFunctions.ParseInt(data[cell]);
					int y = NumberFunctions.ParseInt(data[cell+1]);
					game_data.map[x][y].major_type = NumberFunctions.ParseByte(data[cell+2]);
					game_data.map[x][y].door_type = NumberFunctions.ParseByte(data[cell+3]);
					cell += 4;
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

			// Destroyed walls
			squares = NumberFunctions.ParseInt(data[cell+1]);
			cell+=2;
			for (int i=0 ; i<squares ; i++) {
				game_data.map[NumberFunctions.ParseInt(data[cell])][NumberFunctions.ParseInt(data[cell+1])].major_type = MapDataTable.MT_WALL;
				cell += 2;
			}

			try {
				game_data.player_names[1] = data[cell++];
				game_data.player_names[2] = data[cell++];
				game_data.player_names[3] = data[cell++];
				game_data.player_names[4] = data[cell++];
			} catch (Exception ex) {
				ErrorReporter.getInstance().handleSilentException(ex);
			}

			return game_data;
		} else {
			return null;
		}
	}

}
