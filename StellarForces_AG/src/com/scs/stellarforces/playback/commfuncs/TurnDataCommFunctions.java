package com.scs.stellarforces.playback.commfuncs;

import java.util.ArrayList;

import com.scs.stellarforces.playback.PlaybackTurnData;

public class TurnDataCommFunctions {

	public static final String GET_TURN_DATA = "getturndata";

	// Called by the applet to request the data
	public static String GetTurnDataRequest(int game_id, int login_id, int turn) {
		return "cmd=" + GET_TURN_DATA + "&gid=" + game_id + "&lid=" + login_id + "&turn="+turn;
	}

	// --------------------------------------------------------------------

	// Called by the applet to decode the data
	public static ArrayList<PlaybackTurnData> DecodeResponse(String response) {
		//System.out.println(response);
		String data[] = response.split("\\|");
		//System.out.println(data);
		ArrayList<PlaybackTurnData> al_data = new ArrayList<PlaybackTurnData>();

		if (data.length > 3) {
			int cell = 2;
			while (true) {
				PlaybackTurnData turn_data = new PlaybackTurnData();
				turn_data.unit_id = Integer.parseInt(data[cell]);
				turn_data.status = Integer.parseInt(data[cell+1]);
				turn_data.mapx = Byte.parseByte(data[cell+2]);
				if (turn_data.mapx < 0) { // Handle small bug
					turn_data.mapx = 0;
				}
				turn_data.mapy = Byte.parseByte(data[cell+3]);
				if (turn_data.mapy < 0) { // Handle small bug
					turn_data.mapy = 0;
				}
				turn_data.angle = Integer.parseInt(data[cell+4]);
				turn_data.event_type = Integer.parseInt(data[cell+5]);
				turn_data.rad = Integer.parseInt(data[cell+6]);

				cell += 7;

				al_data.add(turn_data);

				if (cell >= data.length-5) {
					break;
				}
			}
		}

		return al_data;

	}

}
