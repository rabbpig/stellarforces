package com.scs.stellarforces.playback;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.lang.Functions;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.playback.commfuncs.TurnDataCommFunctions;

import dsr.comms.WGet_SF;

public class GetEventDataThread extends Thread {

	public ArrayList<PlaybackTurnData> al_event_data = new ArrayList<PlaybackTurnData>();
	private int max_turns, game_id, login_id, turn_no = 0;
	private AbstractActivity app;

	public GetEventDataThread(AbstractActivity _app, int gid, int lid, int _max_turns) {
		super("GetEventDataThread");

		app = _app;
		game_id = gid;
		login_id = lid;
		max_turns = _max_turns;

		this.setDaemon(true);
		start();
	}


	public void run() {
		try {
			while (turn_no <= max_turns) {
				WGet_SF wg = new WGet_SF(app, null, WGet_SF.T_OTHER, Statics.URL_FOR_CLIENT + "/appletcomm/PlaybackComms.cls", TurnDataCommFunctions.GetTurnDataRequest(game_id, login_id, turn_no), true);
				String response = wg.getResponse();
				ArrayList<PlaybackTurnData> turn_data = TurnDataCommFunctions.DecodeResponse(response);
				al_event_data.addAll(turn_data);

				Functions.delay(2000);
				turn_no++; // Get turn 0 first just in case
			}
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
		}
	}

}
