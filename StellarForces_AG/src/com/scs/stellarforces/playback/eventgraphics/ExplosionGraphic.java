package com.scs.stellarforces.playback.eventgraphics;

import com.scs.stellarforces.Statics;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class ExplosionGraphic extends AbstractEventGraphic {
	
	private static Paint paint = new Paint();

	static {
		paint.setARGB(155, 255, 255, 0);
		paint.setStyle(Style.FILL);
		paint.setAntiAlias(true);
		paint.setTextSize(Statics.SCREEN_WIDTH * 0.03f);
	}
	
	
	private int rad;
	
	public ExplosionGraphic(int x, int y, int _rad) {
		super(x, y, 10);
		
		rad = _rad;
	}

	@Override
	public void doDraw(Canvas g) {
		//g.fillOval(x-rad, y-rad, rad*2, rad*2);
		g.drawCircle(x, y, rad, paint);
	}

}
