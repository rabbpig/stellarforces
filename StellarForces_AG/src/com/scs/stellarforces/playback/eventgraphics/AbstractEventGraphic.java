package com.scs.stellarforces.playback.eventgraphics;

import android.graphics.Canvas;
import android.graphics.Point;

public abstract class AbstractEventGraphic extends Point {
	
	private static final long serialVersionUID = 1L;
	
	public int timer;
	
	public AbstractEventGraphic(int x, int y, int tmr) {
		super(x, y);
		timer = tmr;
	}

	public abstract void doDraw(Canvas g);
}
