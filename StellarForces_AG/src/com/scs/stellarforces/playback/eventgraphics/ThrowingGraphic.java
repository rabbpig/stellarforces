package com.scs.stellarforces.playback.eventgraphics;

import com.scs.stellarforces.Statics;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class ThrowingGraphic extends AbstractEventGraphic {
	
	private static Paint paint = new Paint();

	static {
		paint.setARGB(155, 255, 0, 255);
		paint.setStyle(Style.FILL);
		paint.setAntiAlias(true);
		paint.setTextSize(Statics.SCREEN_WIDTH * 0.03f);
	}
	
	
	private int x_off, y_off;
	
	public ThrowingGraphic(int x, int y, int ang, int len) {
		super(x, y, 20);
		
		x_off = (int)(Math.cos(Math.toRadians(ang)) * len);
		y_off = (int)(Math.sin(Math.toRadians(ang)) * len);
	}

	
	@Override
	public void doDraw(Canvas c) {
		//c.drawLine(x, y, x+x_off, y+y_off);
		c.drawLine(x, y, x+x_off, y+y_off, paint);
	}

}
