package com.scs.stellarforces.playback;

import dsr.data.UnitData;

public class PlayBackGameData {
	
	public int max_turns, max_events, total_sides, map_width;
	public UnitData units[];
	public ServerMapSquare map[][];
	public String player_names[] = new String[5];

	public PlayBackGameData() {
		super();
	}

	public int getMapWidth() {
		return map.length;
	}
	
	
	public int getMapHeight() {
		return map[0].length;
	}

}
