package com.scs.stellarforces.playback;

public class ServerMapSquare {

	public int x, y, major_type, owner_side;
	public short texture_code;
	public short raised_texture_code;
	public byte door_type = -1; // See MapDataTable
	public byte deploy_sq_side = -1;
	public byte escape_hatch_side = -1;
	public int destroyed = 0;
	public boolean door_open = false;
	public short scenery_code; // see MapDataTable
	public byte scenery_direction;
	public byte smoke_type;

	public ServerMapSquare(int t, int _x, int _y, int _destroyed, boolean _door_open) {
		x = _x;
		y = _y;
		this.major_type = t;
		destroyed = _destroyed;
		door_open = _door_open;
	}


}
