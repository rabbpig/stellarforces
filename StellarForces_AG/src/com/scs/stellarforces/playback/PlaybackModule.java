package com.scs.stellarforces.playback;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import ssmith.lang.Functions;
import ssmith.util.Interval;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;
import com.scs.stellarforces.playback.eventgraphics.AbstractEventGraphic;
import com.scs.stellarforces.playback.eventgraphics.ExplosionGraphic;
import com.scs.stellarforces.playback.eventgraphics.ShootingGraphic;
import com.scs.stellarforces.playback.eventgraphics.ThrowingGraphic;
import com.scs.stellarforces.playback.eventgraphics.UnitKilledGraphic;

import dsr.data.UnitData;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public class PlaybackModule extends SimpleAbstractModule {

	// Paints
	private static Paint paint_text = new Paint();
	private static Paint paint_smoke = new Paint();
	private static Paint paint_nerve_gas = new Paint();
	private static Paint paint_fire = new Paint();
	private static Paint paint_wall = new Paint();
	private static Paint paint_cpu = new Paint();
	private static Paint paint_destroyed_cpu = new Paint();
	private static Paint paint_door = new Paint();
	private static Paint paint_side1 = new Paint();
	private static Paint paint_side2 = new Paint();
	private static Paint paint_side3 = new Paint();
	private static Paint paint_side4 = new Paint();
	private static Paint paint_direction = new Paint();

	static {
		paint_text.setARGB(200, 255, 255, 255);
		paint_text.setStyle(Style.FILL);
		paint_text.setAntiAlias(true);
		paint_text.setTextSize(Statics.SCREEN_WIDTH * 0.03f);

		paint_smoke.setARGB(255, 200, 200, 200);
		paint_smoke.setStyle(Style.FILL);

		paint_nerve_gas.setARGB(255, 0, 200, 0);
		paint_nerve_gas.setStyle(Style.FILL);

		paint_fire.setARGB(255, 255, 50, 50);
		paint_fire.setStyle(Style.FILL);

		paint_wall.setARGB(255, 0, 255, 0);
		paint_wall.setStyle(Style.FILL);

		paint_cpu.setARGB(255, 255, 0, 255);
		paint_cpu.setStyle(Style.FILL);

		paint_destroyed_cpu.setARGB(255, 155, 0, 155);
		paint_destroyed_cpu.setStyle(Style.FILL);

		paint_door.setARGB(255, 155, 155, 155);
		paint_door.setStyle(Style.FILL);

		paint_side1.setARGB(255, 255, 0, 0);
		paint_side1.setStyle(Style.FILL);
		paint_side1.setTextSize(Statics.SCREEN_WIDTH * 0.03f);

		paint_side2.setARGB(255, 255, 255, 0);
		paint_side2.setStyle(Style.FILL);
		paint_side2.setTextSize(Statics.SCREEN_WIDTH * 0.03f);

		paint_side3.setARGB(255, 0, 0, 255);
		paint_side3.setStyle(Style.FILL);
		paint_side3.setTextSize(Statics.SCREEN_WIDTH * 0.03f);

		paint_side4.setARGB(255, 0, 255, 255);
		paint_side4.setStyle(Style.FILL);
		paint_side4.setTextSize(Statics.SCREEN_WIDTH * 0.03f);

		paint_direction.setARGB(200, 255, 255, 255);
		paint_direction.setStyle(Style.FILL);
		paint_direction.setAntiAlias(true);
		paint_direction.setStrokeWidth(3);

	}


	public static int ARENA_WIDTH = 650, ARENA_HEIGHT=650;
	private static final long LOOP_DELAY = 35;
	private static final long EVENT_DELAY = 100;

	// Stages
	private static final int GETTING_GAME_DATA = 0;
	private static final int READY = 1;

	private int stage;
	private int game_id = -1, login_id = 2;
	private PlayBackGameData game_data = null;
	private GetEventDataThread event_data_thread = null;
	private int event_data_pos;
	private Interval next_event_int = new Interval(EVENT_DELAY, false);
	private Button icon_pause, icon_restart, icon_ffwd, icon_super_ffwd, icon_rew, icon_slowmo, icon_play;
	private int sq_size = -1;
	private boolean paused = false, slowmo = false;
	private GetGameDataThreadForPlayback game_data_thread;
	private int rewind_count = 0;
	private boolean ffwd = false;
	private ArrayList<AbstractEventGraphic> graphics;
	private boolean run_to_end = false;
	private VerticalFlowLayout vfl;


	public PlaybackModule(AbstractActivity act, AbstractModule return_to,  int _game_id, int _login_id) {
		super(-1);

		this.mod_return_to = return_to;

		if (Statics.GetTypeface(act) != null) {
			paint_text.setTypeface(Statics.GetTypeface(act));
		}

		ARENA_WIDTH = (int)Statics.SCREEN_HEIGHT;
		ARENA_HEIGHT = (int)Statics.SCREEN_HEIGHT;

		this.game_id = _game_id;
		this.login_id = _login_id;

		vfl = new VerticalFlowLayout("vfl", 5) ;
		Bitmap bmp = Statics.img_cache.getImage(R.drawable.button_blue, Statics.SCREEN_WIDTH-ARENA_WIDTH, Statics.SCREEN_HEIGHT/8);
		icon_restart = new Button("Restart", null, paint_text, bmp);
		vfl.attachChild(icon_restart);
		icon_rew = new Button("Rewind", null, paint_text, bmp);
		vfl.attachChild(icon_rew);
		icon_pause = new Button("Pause", null, paint_text, bmp);
		vfl.attachChild(icon_pause);
		icon_play = new Button("Play", null, paint_text, bmp);
		vfl.attachChild(icon_play);
		icon_slowmo = new Button("SlowMo", null, paint_text, bmp);
		vfl.attachChild(icon_slowmo);
		icon_ffwd = new Button("Ffwd", null, paint_text, bmp);
		vfl.attachChild(icon_ffwd);
		icon_super_ffwd = new Button("To End", null, paint_text, bmp);
		vfl.attachChild(icon_super_ffwd);

		vfl.setLocation(ARENA_WIDTH, 0);

		Thread t = new Thread(this, "PlaybackModule");
		t.start();

	}


	private void startFromBeginning() {
		AbstractActivity act = Statics.act;

		stage = GETTING_GAME_DATA;
		event_data_pos = 0;
		graphics = new ArrayList<AbstractEventGraphic>();
		game_data_thread = new GetGameDataThreadForPlayback(act, game_id);
		vfl.removeFromParent();
	}


	public void run() {
		startFromBeginning();

		try {

			boolean stop_now = false;
			while (!stop_now) {
				AbstractActivity act = Statics.act;

				long start = System.currentTimeMillis();


				// See if gamedata thread has finished
				if (stage == GETTING_GAME_DATA) {
					if (game_data_thread != null && game_data_thread.isAlive() == false) {
						this.game_data = game_data_thread.game_data;
						game_data_thread = null;
						if (game_data != null) {
							sq_size = ARENA_WIDTH / game_data.map_width;
							event_data_thread = new GetEventDataThread(act, game_id, login_id, game_data.max_turns);
							stage = READY; // Must be after we create the thread

							if (vfl.parent == null) {
								this.stat_node.attachChild(vfl);
								this.stat_node.updateGeometricState();
							}
						} else {
							// No such game
						}
					}
				} else {
					if (!paused) {
						if (this.event_data_thread != null) {
							if (next_event_int.hitInterval() || ffwd || rewind_count > 0) {
								if (rewind_count > 0) {
									rewind_count--;
								} 
								while (event_data_pos < this.event_data_thread.al_event_data.size()) { // Have we got any more turn data to get?
									PlaybackTurnData turn_data = this.event_data_thread.al_event_data.get(event_data_pos);
									// process this piece of turn data
									boolean did_something_happen = processEvent(turn_data);
									if (did_something_happen) { // Update graphics
										break; // Something happened so drop out so we loop around the main loop again
									} else {
										this.nextEvent();
									}
								}
								if (event_data_pos >= this.event_data_thread.al_event_data.size()) {
									this.run_to_end = false;
								}
								this.updateGraphics();
								this.nextEvent();
							}
						}
					}
				}

				if (this.run_to_end == false) {
					long wait = LOOP_DELAY - System.currentTimeMillis() + start;
					Functions.delay(wait);
				} else {
					Thread.yield();
				}
			}
		} catch (Exception e) {
			AbstractActivity.HandleError(e);
		}
	}


	private void updateGraphics() {
		synchronized (graphics) {
			for (int i=0 ; i<this.graphics.size() ; i++) {
				AbstractEventGraphic current_event_graphic = this.graphics.get(i);
				current_event_graphic.timer--;
				if (current_event_graphic.timer <= 0) {
					this.graphics.remove(i);
					i--;
				}
			}
		}
	}


	private void nextEvent() {
		if (rewind_count > 0 && event_data_pos > 0) {
			this.event_data_pos--;
		} else if (event_data_pos < this.event_data_thread.al_event_data.size()) {
			this.event_data_pos++;
		}

	}


	private void setSlowMo(boolean b) {
		this.slowmo = b;
		if (slowmo) {
			next_event_int.setInterval(EVENT_DELAY*5, true);
		} else {
			next_event_int.setInterval(EVENT_DELAY, true);
		}

	}


	/**
	 * Returns true if we pause after it.
	 */
	private boolean processEvent(PlaybackTurnData turn_data) {
		try {
			UnitData unit = null;
			switch (turn_data.event_type) {
			case UnitHistoryTable.UH_UNIT_MOVEMENT:
				unit = UnitData.GetUnitDataFromID(game_data.units, turn_data.unit_id);
				unit.map_x = turn_data.mapx;
				unit.map_y = turn_data.mapy;
				unit.angle = turn_data.angle;
				//if (unit.getStatus() == UnitsTable.ST_AWAITING_DEPLOYMENT) {
				unit.setStatus(UnitsTable.ST_DEPLOYED); // Just in case, and needed for Reanimator
				//}
				return true;
			case UnitHistoryTable.UH_EXPLOSION:
				this.graphics.add(new ExplosionGraphic(turn_data.mapx*sq_size, turn_data.mapy*sq_size, turn_data.rad*sq_size));
				return true;
			case UnitHistoryTable.UH_WALL_DESTROYED:
				game_data.map[turn_data.mapx][turn_data.mapy].destroyed = 1;
				return false;
			case UnitHistoryTable.UH_UNIT_KILLED:
				unit = UnitData.GetUnitDataFromID(game_data.units, turn_data.unit_id);
				unit.map_x = turn_data.mapx;
				unit.map_y = turn_data.mapy;
				unit.angle = turn_data.angle;
				if (rewind_count <= 1) {
					unit.setStatus(UnitsTable.ST_DEAD);
					this.graphics.add(new UnitKilledGraphic(turn_data.mapx*sq_size, turn_data.mapy*sq_size, sq_size));
				} else { // Going backwards to make alive
					unit.setStatus(UnitsTable.ST_DEPLOYED);
				}
				return true;
			case UnitHistoryTable.UH_SHOT_FIRED:
				unit = UnitData.GetUnitDataFromID(this.game_data.units, turn_data.unit_id);
				// Update the unit
				unit.map_x = turn_data.mapx;
				unit.map_y = turn_data.mapy;
				int ang = turn_data.angle;
				if (ang == 0) {
					ang = unit.angle;
				}
				int length = turn_data.rad;
				if (length == 0) {
					length = 2; // Default
				}
				length = length * sq_size;
				this.graphics.add(new ShootingGraphic(turn_data.mapx*sq_size+(sq_size/2), turn_data.mapy*sq_size+(sq_size/2), ang, length));
				return true;
			case UnitHistoryTable.UH_ITEM_THROWN:
				unit = UnitData.GetUnitDataFromID(this.game_data.units, turn_data.unit_id);
				// Update the unit
				unit.map_x = turn_data.mapx;
				unit.map_y = turn_data.mapy;
				ang = turn_data.angle;
				if (ang == 0) {
					ang = unit.angle;
				}
				length = turn_data.rad;
				if (length == 0) {
					length = 2; // Default
				}
				if (length < 60) { // Since the Android client was sending length in Pixels
					length = length * sq_size;
				}
				this.graphics.add(new ThrowingGraphic(turn_data.mapx*sq_size+(sq_size/2), turn_data.mapy*sq_size+(sq_size/2), ang, length));
				return true;
			case UnitHistoryTable.UH_UNIT_DEPLOYED:
				unit = UnitData.GetUnitDataFromID(game_data.units, turn_data.unit_id);
				unit.map_x = turn_data.mapx;
				unit.map_y = turn_data.mapy;
				unit.angle = turn_data.angle;
				unit.setStatus(UnitsTable.ST_DEPLOYED);
				return true;
			case UnitHistoryTable.UH_UNIT_ESCAPED:
				unit = UnitData.GetUnitDataFromID(game_data.units, turn_data.unit_id);
				if (rewind_count <= 1) {
					unit.setStatus(UnitsTable.ST_ESCAPED);
				} else {// Going backwards
					unit.map_x = turn_data.mapx;
					unit.map_y = turn_data.mapy;
					unit.angle = turn_data.angle;
					unit.setStatus(UnitsTable.ST_DEPLOYED);
				}
				return true;
				/*case UnitHistoryTable.UH_SEEN_CLOSE_COMBAT:
			return false;
		case UnitHistoryTable.UH_SEEN_ITEM_THROWN:
			return false;*/
			case UnitHistoryTable.UH_COMPUTER_DESTROYED:
				if (rewind_count <= 1) {
					game_data.map[turn_data.mapx][turn_data.mapy].destroyed = 1;
				} else {// Going backwards
					game_data.map[turn_data.mapx][turn_data.mapy].destroyed = 0;
				}
				return true;
			case UnitHistoryTable.UH_FLAG_LOCATION:
				return false;
			case UnitHistoryTable.UH_DOOR_OPENED:
				return false;
			case UnitHistoryTable.UH_DOOR_CLOSED:
				return false;
			case UnitHistoryTable.UH_GRENADE_PRIMED:
				return false;
			case UnitHistoryTable.UH_BLOB_SPLIT:
				return false;
			case UnitHistoryTable.UH_SMOKE_CREATED:
				game_data.map[turn_data.mapx][turn_data.mapy].smoke_type = EquipmentTypesTable.ET_SMOKE_GRENADE;
				return false;
			case UnitHistoryTable.UH_SMOKE_REMOVED:
				game_data.map[turn_data.mapx][turn_data.mapy].smoke_type = -1;
				return false;
			case UnitHistoryTable.UH_NERVE_GAS_CREATED:
				game_data.map[turn_data.mapx][turn_data.mapy].smoke_type = EquipmentTypesTable.ET_NERVE_GAS;
				return false;
			case UnitHistoryTable.UH_NERVE_GAS_REMOVED:
				game_data.map[turn_data.mapx][turn_data.mapy].smoke_type = -1;
				return false;
			case UnitHistoryTable.UH_FIRE_CREATED:
				game_data.map[turn_data.mapx][turn_data.mapy].smoke_type = EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE;
				return false;
			case UnitHistoryTable.UH_FIRE_REMOVED:
				game_data.map[turn_data.mapx][turn_data.mapy].smoke_type = -1;
				return false;
			case UnitHistoryTable.UH_MAPSQUARE_CHANGED:
				game_data.map[turn_data.mapx][turn_data.mapy].major_type = turn_data.rad;
				return true;
			default:
				//throw new RuntimeException("Unknown event type:" + turn_data.event_type);
				System.err.println("Unknown event type: " + turn_data.event_type);
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}

	}


	public void doDraw(Canvas c, long interpol) {
		super.doDraw(c, interpol);

		if (stage == GETTING_GAME_DATA) {
			c.drawText("Please wait - getting game data...", 10, paint_text.getTextSize(), paint_text);
		} else {
			if (this.run_to_end == false) {
				this.drawMap(c);
				this.drawUnits(c);
				synchronized (graphics) {
					for (int i=0 ; i<this.graphics.size() ; i++) {
						AbstractEventGraphic current_event_graphic = this.graphics.get(i);
						current_event_graphic.doDraw(c);
					}
				}
			}

			// Draw text
			if (event_data_thread.al_event_data.size() < this.game_data.max_events) {
				c.drawText("Events downloaded: " + event_data_thread.al_event_data.size() + "/" + this.game_data.max_events, 10, paint_text.getTextSize(), paint_text);
			}
			if (this.paused) {
				c.drawText("PAUSED", 10, paint_text.getTextSize()*4, paint_text);
			} else if (event_data_pos >= this.event_data_thread.al_event_data.size() && this.event_data_thread.al_event_data.size() > 0) { // Have we reached the end?
				c.drawText("THE END", 10, paint_text.getTextSize()*4, paint_text);
				/*} else if (slowmo) {
					g.drawString("Slow-Mo", 10, 60);*/
			} else {
				c.drawText("Now showing event " + event_data_pos + "/" + this.event_data_thread.al_event_data.size(), 10, paint_text.getTextSize()*2, paint_text);
			}

			// Player names
			try {
				if (game_data.player_names != null) {
					if (game_data.total_sides >= 1) {
						c.drawText(game_data.player_names[1], 10, Statics.SCREEN_HEIGHT - paint_side1.getTextSize(), paint_side1);
					}
					if (game_data.total_sides >= 2) {
						c.drawText(game_data.player_names[2], Statics.SCREEN_WIDTH/4, Statics.SCREEN_HEIGHT - paint_side1.getTextSize(), paint_side2);
					}
					if (game_data.total_sides >= 3) {
						c.drawText(game_data.player_names[3], Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT - paint_side1.getTextSize(), paint_side3);
					}
					if (game_data.total_sides >= 4) {
						c.drawText(game_data.player_names[4], Statics.SCREEN_WIDTH*0.75f, Statics.SCREEN_HEIGHT - paint_side1.getTextSize(), paint_side4);
					}
				}
			} catch (NullPointerException ex) {
				// Do nothing
			}
		}

	}


	private void drawMap(Canvas c) {
		int size = game_data.map.length; 

		Rect r = new Rect();
		for (int y=0 ; y<size ; y++) {
			for (int x=0 ; x<size ; x++) {
				r.set(x*sq_size, y*sq_size, (x+1)*sq_size, (y+1)*sq_size);
				if (game_data.map[x][y].smoke_type == EquipmentTypesTable.ET_SMOKE_GRENADE) {
					//g.setColor(MapImageAbstract.SMOKE);
					//c.drawr.drawr.drawRect(x * sq_size, y * sq_size, sq_size-1, sq_size-1);
					c.drawRect(r, paint_smoke);
				} else if (game_data.map[x][y].smoke_type == EquipmentTypesTable.ET_NERVE_GAS) {
					//g.setColor(MapImageAbstract.NERVE_GAS);
					//g.drawRect(x * sq_size, y * sq_size, sq_size-1, sq_size-1);
					c.drawRect(r, paint_nerve_gas);
				} else if (game_data.map[x][y].smoke_type == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
					//g.setColor(MapImageAbstract.NERVE_GAS);
					//g.drawRect(x * sq_size, y * sq_size, sq_size-1, sq_size-1);
					c.drawRect(r, paint_fire);
				}
				if (game_data.map[x][y].major_type == MapDataTable.MT_WALL) {
					if (game_data.map[x][y].destroyed == 0) {
						//g.setColor(MapImageAbstract.GREEN_FLOOR);
						//g.fillRect((x * sq_size), (y * sq_size), sq_size-1, sq_size-1);
						c.drawRect(r, paint_wall);
					}
				} else if (game_data.map[x][y].major_type == MapDataTable.MT_NOTHING) {
					//g.setColor(Color.DARK_GRAY);
					//g.fillRect((x * sq_size), (y * sq_size), sq_size, sq_size);
				} else if (game_data.map[x][y].major_type == MapDataTable.MT_COMPUTER) {
					if (game_data.map[x][y].destroyed == 0) {
						//g.setColor(MapImageAbstract.OPP_COMPUTER_COL);
						c.drawRect(r, paint_cpu);
					} else {
						//g.setColor(MapImageAbstract.OPP_DESTROYED_COMPUTER_COL);
						c.drawRect(r, paint_destroyed_cpu);
					}
					//g.fillRect((x * sq_size), (y * sq_size), sq_size-1, sq_size-1);
				} else if (game_data.map[x][y].door_type > 0) {
					//g.setColor(Color.DARK_GRAY);
					//g.fillRect((x * sq_size), (y * sq_size), sq_size-1, sq_size-1);
					c.drawRect(r, paint_door);
				}
			}
		}
	}


	private void drawUnits(Canvas c) {
		// Draw units
		RectF r = new RectF();
		for (int i=0 ; i<this.game_data.units.length ; i++) {
			UnitData unit = this.game_data.units[i];
			float extra = 0;
			if (unit.model_type == UnitsTable.MT_QUEEN_ALIEN || unit.model_type == UnitsTable.MT_SCIENTIST) {
				extra = sq_size * 0.5f;
			}
			r.set(unit.map_x * sq_size-extra, unit.map_y * sq_size-extra, (unit.map_x+1) * sq_size+extra, (unit.map_y+1) * sq_size+extra);
			Paint p = getPaintForSide(unit.getSide());//g.setColor(MapImageAbstract.GetColourForSide(unit.getSide()));
			if (unit.getStatus() == UnitsTable.ST_DEAD) {
				//c.drawOval(r, p);
			} else if (unit.getStatus() == UnitsTable.ST_DEPLOYED) {
				c.drawOval(r, p);
				// Draw direction
				int sx = unit.map_x * sq_size + (sq_size/2);
				int sy = unit.map_y * sq_size + (sq_size/2);
				c.drawLine(sx, sy, sx+GetXOffSetFromAngle(unit.angle, sq_size), sy+GetYOffSetFromAngle(unit.angle, sq_size), paint_direction);
			} else {
				continue; // Don't draw if escaped
			}

			// Draw unit numbers
			// g.drawString("" + unit.order_by, (unit.map_x * sq_size) + 3, (unit.map_y * sq_size) + 12);
		}
	}


	public static int GetXOffSetFromAngle(int ang, int sq_size) {
		switch (ang) {
		case 90:
			return 0;
		case 135:
			return -sq_size;
		case 180:
			return -sq_size;
		case 225:
			return -sq_size;
		case 270:
			return 0;
		case 315:
			return sq_size;
		case 0:
			return sq_size;
		case 45:
			return sq_size;
		default:
			System.err.println("Unknown angle: " + ang);
			return 0;
		}
	}


	public static int GetYOffSetFromAngle(int ang, int sq_size) {
		switch (ang) {
		case 90:
			return sq_size;
		case 135:
			return sq_size;
		case 180:
			return 0;
		case 225:
			return -sq_size;
		case 270:
			return -sq_size;
		case 315:
			return -sq_size;
		case 0:
			return 0;
		case 45:
			return sq_size;
		default:
			System.err.println("Unknown angle: " + ang);
			return 0;
		}
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		if (c == this.icon_pause) {
			paused = true;
			this.run_to_end = false;
		} else if (c == this.icon_play) {
			paused = false;
			this.rewind_count = 0;
			this.ffwd = false;
			this.run_to_end = false;
			this.setSlowMo(false);
		} else if (c == this.icon_restart) {
			this.startFromBeginning();
			this.paused = false;
			this.run_to_end = false;
		} else if (c == this.icon_ffwd) {
			this.ffwd = true;
			this.rewind_count = 0;
			this.paused = false;
			this.setSlowMo(false);
			this.run_to_end = false;
		} else if (c == this.icon_super_ffwd) {
			this.ffwd = true;
			this.rewind_count = 0;
			this.paused = false;
			this.setSlowMo(false);
			this.run_to_end = true;
		} else if (c == this.icon_rew) {
			this.rewind_count = 20;
			this.ffwd = false;
			this.paused = false;
			this.setSlowMo(false);
			this.run_to_end = false;
		} else if (c == this.icon_slowmo) {
			this.paused = false;
			this.setSlowMo(!this.slowmo);
			this.run_to_end = false;
		}

	}


	private Paint getPaintForSide(int side) {
		switch (side) {
		case 1:
			return paint_side1;
		case 2:
			return paint_side2;
		case 3:
			return paint_side3;
		case 4:
			return paint_side4;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}
}
