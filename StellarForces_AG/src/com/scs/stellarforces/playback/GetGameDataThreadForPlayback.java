package com.scs.stellarforces.playback;

import ssmith.android.framework.AbstractActivity;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.playback.commfuncs.GameDataCommFunctions;

import dsr.comms.WGet_SF;

public class GetGameDataThreadForPlayback extends Thread {
	
	public PlayBackGameData game_data;
	private int game_id;
	private AbstractActivity app;
	
	public GetGameDataThreadForPlayback(AbstractActivity _app, int gid) {
		super("UpdateDataThread");
		this.setDaemon(true);
		
		app = _app;
		game_id = gid;
		
		start();
	}
	
	public void run() {
		try {
			WGet_SF wg = new WGet_SF(app, null, WGet_SF.T_OTHER, Statics.URL_FOR_CLIENT + "/appletcomm/PlaybackComms.cls", GameDataCommFunctions.GetGameDataRequest(game_id), true);
			String response = wg.getResponse();
			game_data = GameDataCommFunctions.DecodeResponse(response);
			
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
		}
	}

}
