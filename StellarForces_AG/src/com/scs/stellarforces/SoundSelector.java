package com.scs.stellarforces;

import ssmith.android.framework.AbstractActivity;
import ssmith.lang.NumberFunctions;

import com.scs.stellarforces.main.lite.R;

public class SoundSelector {
	
	public static void PlayCoughSound(AbstractActivity act) {
		if (NumberFunctions.rnd(1, 2) == 1) {
			act.playSound(R.raw.cough1);
		} else {
			act.playSound(R.raw.cough2);
		}
	}

}
