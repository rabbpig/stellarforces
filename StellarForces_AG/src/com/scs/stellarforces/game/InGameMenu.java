package com.scs.stellarforces.game;

import ssmith.android.lib2d.Node;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.GUIFunctions;
import ssmith.android.lib2d.layouts.GridLayout;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.scs.stellarforces.Statics;

public class InGameMenu extends Node {

	public static final int STATUS_HIDDEN = 0;
	public static final int STATUS_SHOWN = 1;
	
	private static final float MOVE_SPEED = Statics.SCREEN_HEIGHT * 0.1f;

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.4f;
	private static final float ICON_HEIGHT = ICON_WIDTH / 5;

	private int status = STATUS_HIDDEN;

	private static Paint paint_menu_text = new Paint();
	private static Paint paint_background = new Paint();

	static {
		paint_menu_text.setARGB(255, 255, 255, 255);
		paint_menu_text.setAntiAlias(true);
		//paint_menu_text.setStyle(Style.STROKE);
		paint_menu_text.setTextSize(GUIFunctions.GetTextSizeToFit("Advanced ScannerX", ICON_WIDTH, ICON_HEIGHT));

		paint_background.setARGB(200, 255, 255, 255);
		paint_background.setAntiAlias(true);
		//paint_background.setStyle(Style.STROKE);
		//paint_background.setTextSize(GUIFunctions.GetTextSizeToFit("Advanced ScannerX", ICON_WIDTH, ICON_HEIGHT));
	}


	public InGameMenu(GameModule _game, Typeface tf) {
		super("InGameMenu");

		paint_menu_text.setTypeface(tf);

		GridLayout menu_node = new GridLayout("Menu", ICON_WIDTH, ICON_HEIGHT, (int)(Statics.SCREEN_HEIGHT * 0.005f));
		menu_node.attachChild(new Button(""+GameModule.MENU_ADVANCED_SCANNER, "Advanced Scanner", paint_background, paint_menu_text, _game.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT)), 0, 0);
		menu_node.attachChild(new Button(""+GameModule.MENU_GAME_LOG, "Game Log", paint_background, paint_menu_text, _game.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT)), 1, 0);
		menu_node.attachChild(new Button(""+GameModule.MENU_START_TUTORIAL, "Tutorial", paint_background, paint_menu_text, _game.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT)), 0, 1);
		menu_node.attachChild(new Button(""+GameModule.MENU_GRID, "Toggle Grid", paint_background, paint_menu_text, _game.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT)), 1, 1);
		menu_node.attachChild(new Button(""+GameModule.MENU_MISSION_DESC, "Mission File", paint_background, paint_menu_text, _game.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT)), 0, 2);
		menu_node.attachChild(new Button(""+GameModule.MENU_UNIT_STATS, "All Unit Stats", paint_background, paint_menu_text, _game.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT)), 1, 2);
		menu_node.attachChild(new Button(""+GameModule.MENU_GAME_DETAILS, "Game Details", paint_background, paint_menu_text, _game.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT)), 0, 3);
		menu_node.attachChild(new Button(""+GameModule.MENU_MSG_OPPONENT, "Msg Opponent", paint_background, paint_menu_text, _game.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT)), 1, 3);
		menu_node.attachChild(new Button(""+GameModule.MENU_INSTRUCTIONS, "Instructions", paint_background, paint_menu_text, _game.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT)), 0, 4);
		menu_node.attachChild(new Button(""+GameModule.MENU_GAME_CHAT, "Game Chat", paint_background, paint_menu_text, _game.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT)), 1, 4);
		if (_game.game_data.getComradeSide() > 0) {
			menu_node.attachChild(new Button(""+GameModule.MENU_MSG_COMRADE, "Msg Comrade", paint_background, paint_menu_text, _game.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT)), 1, 5);
		}
		
		menu_node.updateGeometricState();
		
		this.attachChild(menu_node);
		
		this.updateGeometricState();

		this.setLocation(Statics.SCREEN_WIDTH - (ICON_WIDTH*2), Statics.SCREEN_HEIGHT);
		
		this.updateGeometricState();

		
	}


	public void process() {
		// Do we need to move
		switch (status) {
		case STATUS_HIDDEN:
			if (this.getWorldY() < Statics.SCREEN_HEIGHT) {
				this.setLocation(this.getLocation().x, this.getLocation().y + MOVE_SPEED);
				this.parent.updateGeometricState();
			}
			break;
		case STATUS_SHOWN:
			if (this.getWorldBounds().bottom > Statics.SCREEN_HEIGHT) {
				this.setLocation(this.getLocation().x, this.getLocation().y - MOVE_SPEED);
				this.parent.updateGeometricState();
			}
			break;
		default:
			throw new RuntimeException("Unknown status: " + status);
		}

	}

	
	public void show() {
		this.status = STATUS_SHOWN;
	}


	public void hide() {
		this.status = STATUS_HIDDEN;
	}
	
	
	public void toggle() {
		if (status == STATUS_SHOWN) {
			status = STATUS_HIDDEN;
		} else {
			status = STATUS_SHOWN;
		}
	}
	
	
	public void instaHide() {
		this.hide();
		this.setLocation(this.getLocation().x, this.getLocation().y + this.getHeight() + Statics.SCREEN_HEIGHT);
		this.updateGeometricState();
	}
	
	
	public int getStatus() {
		return this.status;
	}

	
}
