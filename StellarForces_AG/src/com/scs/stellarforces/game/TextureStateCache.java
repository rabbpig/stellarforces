package com.scs.stellarforces.game;

import ssmith.lang.NumberFunctions;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

import dsrwebserver.tables.MapDataTable;

public class TextureStateCache {

	// Texture types
	public static final short TEX_INTERIOR1 = 1;
	public static final short TEX_MOONROCK = 2;
	public static final short TEX_CORRIDOR1 = 3;
	public static final short TEX_INTERIOR2 = 4;
	public static final short TEX_RUBBLE_1 = 5;
	public static final short TEX_PATH = 6;
	public static final short TEX_GRASS = 7;
	public static final short TEX_ROAD = 8;
	public static final short TEX_MUD = 9;
	public static final short TEX_WOODEN_FLOOR = 10;
	public static final short TEX_WATER = 11;
	public static final short TEX_BEACH = 12;
	public static final short TEX_SAND1 = 13;
	public static final short TEX_SAND2 = 14;
	public static final short TEX_WOODEN_PLANKS = 15;
	public static final short TEX_MOONBASE_BLUE = 16;
	public static final short TEX_CORRUGATED_WALL = 17;
	public static final short TEX_MUDTRACK_EW = 18;
	public static final short TEX_MUDTRACK_NS = 19;
	public static final short TEX_RAILWAY_EW = 20;
	public static final short TEX_RAILWAY_NS = 21;
	public static final short TEX_MUDTRACKS_RB = 22;
	public static final short TEX_MUDTRACKS_LT = 23;
	public static final short TEX_SNOW = 24;
	public static final short TEX_SPACEWALL = 25;
	public static final short TEX_STONE_TILES = 26;
	public static final short TEX_MUDTRACKS_RT = 27;
	public static final short TEX_CARPET1 = 28;
	public static final short TEX_WOODEN_FLOOR2 = 29;
	public static final short TEX_SECTOR1 = 30;
	public static final short TEX_STREET = 31;
	public static final short TEX_STREET_ZEBRA_LR = 32;
	public static final short TEX_MUDTRACKS_UD = 33;
	public static final short TEX_MUDTRACKS_UDR = 34;
	public static final short TEX_MUDTRACKS_LR = 35;
	public static final short TEX_MUDTRACKS_LB = 36;
	public static final short TEX_STREET_ZEBRA_UD = 37;
	public static final short TEX_YELLOW_LINE_INNER_TR = 38;
	public static final short TEX_YELLOW_LINE_INNER_TL = 39;
	public static final short TEX_YELLOW_LINE_INNER_BR = 40;
	public static final short TEX_YELLOW_LINE_INNER_BL = 41;
	public static final short TEX_YELLOW_LINE_OUTER_BL = 42;
	public static final short TEX_YELLOW_LINE_OUTER_BR = 43;
	public static final short TEX_YELLOW_LINE_OUTER_TL = 44;
	public static final short TEX_YELLOW_LINE_OUTER_TR = 45;
	public static final short TEX_YELLOW_LINE_L = 46;
	public static final short TEX_YELLOW_LINE_R = 47;
	public static final short TEX_YELLOW_LINE_T = 48;
	public static final short TEX_YELLOW_LINE_B = 49;
	public static final short TEX_ALIEN_SKIN = 50;
	public static final short TEX_BULKHEAD = 51;
	public static final short TEX_SPACESHIP_WALL = 52;
	public static final short TEX_TELEPORTER = 53;
	public static final short TEX_COUCH = 54;
	public static final short TEX_METAL_FLOOR5 = 55;
	public static final short TEX_METAL_FLOOR6 = 56;
	public static final short TEX_METAL_FLOOR15 = 57;
	public static final short TEX_HEDGE = 58;
	public static final short TEX_FLOORHATCH = 59;
	public static final short TEX_METAL_FLOOR41 = 60;
	public static final short TEX_LAB_FLOOR1 = 61;
	public static final short TEX_LAB_FLOOR2 = 62;
	public static final short TEX_ROAD_WHITE_LINE_UD = 63;
	public static final short TEX_ROAD_WHITE_LINE_LR = 64;
	public static final short TEX_SECTOR2 = 65;
	public static final short TEX_SECTOR3 = 66;
	public static final short TEX_SECTOR4 = 67;
	public static final short TEX_ESCAPE_HATCH = 68;
	public static final short TEX_BRICKS = 69;
	public static final short TEX_WHITE_LINE_LR = 70;
	public static final short TEX_WHITE_LINE_UD = 71;
	public static final short TEX_RUBBLE_2 = 72;
	public static final short TEX_RUBBLE_3 = 73;
	public static final short TEX_RUBBLE_4 = 74;
	public static final short TEX_CELLS3 = 75;
	public static final short TEX_ALIEN_PURPLE = 76;
	public static final short TEX_ALIEN_GREEN = 77;
	public static final short TEX_CARVED_SANDSTONE = 78;
	public static final short TEX_HAZARD_TOP = 79;
	public static final short TEX_HAZARD_LEFT = 80;
	public static final short TEX_HAZARD_RIGHT = 81;
	public static final short TEX_STAIRS_UP = 82;
	public static final short TEX_STAIRS_DOWN = 83;
	public static final short TEX_POWER_UNIT = 84;
	public static final short TEX_MEDI_BAY = 85;
	public static final short TEX_CLONE_GENERATOR = 86;
	public static final short TEX_GUN_VENDING_MACHINE = 87;
	public static final short TEX_GRENADE_VENDING_MACHINE = 88;
	public static final short TEX_RUBBLE_1_TRANSP = 89;
	public static final short TEX_RUBBLE_2_TRANSP = 90;
	public static final short TEX_RUBBLE_3_TRANSP = 91;
	public static final short TEX_RUBBLE_4_TRANSP = 92;
	public static final short TEX_ALIEN_COLONY = 93;
	public static final short TEX_SHADOW_L_SHAPE = 94;
	public static final short TEX_SHADOW_RIGHT = 95;
	public static final short TEX_SHADOW_SQUARE = 96;
	public static final short TEX_SHADOW_TOP = 97;
	public static final short MAX_TEX_NUM = 97;
	// IF YOU ADD ANY HERE, ADD THEM TO TextureStateCache IN THE SERVER!


	public static int GetTexResourceID(int tex) {
		switch (tex) {
		case TEX_INTERIOR1: return R.drawable.metalfloor1;
		case TEX_MOONROCK: return R.drawable.moonrock;
		case TEX_CORRIDOR1: return R.drawable.corridor;
		case TEX_INTERIOR2: return R.drawable.floor3;
		case TEX_RUBBLE_1: return R.drawable.rubble_1;
		case TEX_RUBBLE_2: return R.drawable.rubble_2;
		case TEX_RUBBLE_3: return R.drawable.rubble_3;
		case TEX_RUBBLE_4: return R.drawable.rubble_4;
		case TEX_PATH: return R.drawable.road1;
		case TEX_GRASS: return R.drawable.grass;
		case TEX_ROAD: return R.drawable.road2;
		case TEX_MUD: return R.drawable.mud;
		case TEX_WOODEN_FLOOR: return R.drawable.floor02;
		case TEX_WATER: return R.drawable.water;
		case TEX_BEACH: return R.drawable.beach;
		case TEX_SAND1: return R.drawable.sand1;
		case TEX_SAND2: return R.drawable.sand2;
		case TEX_WOODEN_PLANKS: return R.drawable.wooden_planks_lr;
		case TEX_MOONBASE_BLUE: return R.drawable.spacewall2; // was .moonbase_ceiling;
		case TEX_CORRUGATED_WALL: return R.drawable.wall2;
		case TEX_MUDTRACK_EW: return R.drawable.mudtrack_ew;
		case TEX_MUDTRACK_NS: return R.drawable.mudtrack_ns;
		case TEX_RAILWAY_EW: return R.drawable.railway_ew;
		case TEX_RAILWAY_NS: return R.drawable.railway_ns;
		case TEX_MUDTRACKS_RB: return R.drawable.mudtracks_br;
		case TEX_MUDTRACKS_LT: return R.drawable.mudtracks_tl;
		case TEX_SNOW: return R.drawable.snow;
		case TEX_SPACEWALL: return R.drawable.spacewall;
		case TEX_STONE_TILES: return R.drawable.stone_tiles;
		case TEX_MUDTRACKS_RT: return R.drawable.mudtracks_tr;
		case TEX_CARPET1: return R.drawable.carpet006;
		case TEX_WOODEN_FLOOR2: return R.drawable.wood_b_9;
		case TEX_SECTOR1: return R.drawable.sector1;
		case TEX_STREET: return R.drawable.street001;
		case TEX_STREET_ZEBRA_LR: return R.drawable.street010_lr;
		case TEX_MUDTRACKS_UD: return R.drawable.mudtracks_tb;
		case TEX_MUDTRACKS_UDR: return R.drawable.mudtracks_tbr;
		case TEX_MUDTRACKS_LR: return R.drawable.mudtracks_lr;
		case TEX_MUDTRACKS_LB: return R.drawable.mudtracks_bl;
		case TEX_STREET_ZEBRA_UD: return R.drawable.street010_ud;
		case TEX_YELLOW_LINE_INNER_TR: return R.drawable.yell_2ln_crn_ne_g;
		case TEX_YELLOW_LINE_INNER_TL: return R.drawable.yell_2ln_crn_nw_g;
		case TEX_YELLOW_LINE_INNER_BR: return R.drawable.yell_2ln_crn_se_g;
		case TEX_YELLOW_LINE_INNER_BL: return R.drawable.yell_2ln_crn_sw_g;
		case TEX_YELLOW_LINE_OUTER_BL: return R.drawable.yell_2ln_crnw_ne_g;
		case TEX_YELLOW_LINE_OUTER_BR: return R.drawable.yell_2ln_crnw_nw_g;
		case TEX_YELLOW_LINE_OUTER_TL: return R.drawable.yell_2ln_crnw_se_g;
		case TEX_YELLOW_LINE_OUTER_TR: return R.drawable.yell_2ln_crnw_sw_g;
		case TEX_YELLOW_LINE_L: return R.drawable.yell_2ln_l;
		case TEX_YELLOW_LINE_R: return R.drawable.yell_2ln_r;
		case TEX_YELLOW_LINE_T: return R.drawable.yell_2ln_t;
		case TEX_YELLOW_LINE_B: return R.drawable.yell_2ln_b;
		case TEX_ALIEN_SKIN: return R.drawable.alienskin2;
		case TEX_BULKHEAD: return R.drawable.bulkhead;
		case TEX_SPACESHIP_WALL: return R.drawable.spaceship_wall;
		case TEX_TELEPORTER: return R.drawable.teleporter;
		case TEX_COUCH: return R.drawable.couch_e;
		case TEX_METAL_FLOOR5: return R.drawable.floor5;
		case TEX_METAL_FLOOR6: return R.drawable.floor006;
		case TEX_METAL_FLOOR15: return R.drawable.floor015;
		case TEX_HEDGE: return R.drawable.hedge02;
		case TEX_FLOORHATCH: return R.drawable.floorhatch;
		case TEX_METAL_FLOOR41: return R.drawable.floor0041;
		case TEX_LAB_FLOOR1: return R.drawable.lab_floor1;
		case TEX_LAB_FLOOR2: return R.drawable.lab_floor2;
		case TEX_ROAD_WHITE_LINE_UD: return R.drawable.street001_wl_ud;
		case TEX_ROAD_WHITE_LINE_LR: return R.drawable.street001_wl_lr;
		case TEX_SECTOR2: return R.drawable.sector2;
		case TEX_SECTOR3: return R.drawable.sector3;
		case TEX_SECTOR4: return R.drawable.sector4;
		case TEX_ESCAPE_HATCH: return R.drawable.escape_hatch;
		case TEX_BRICKS: return R.drawable.bricks;
		case TEX_WHITE_LINE_LR: return R.drawable.white_line_lr;
		case TEX_WHITE_LINE_UD: return R.drawable.white_line_ud;
		case TEX_CELLS3: return R.drawable.cells3;
		case TEX_ALIEN_PURPLE: return R.drawable.alien_text_purple;
		case TEX_CARVED_SANDSTONE: return R.drawable.carvedsandstone;
		case TEX_HAZARD_TOP: return R.drawable.hazard_top;
		case TEX_HAZARD_LEFT: return R.drawable.hazard_left;
		case TEX_HAZARD_RIGHT: return R.drawable.hazard_right;
		case TEX_STAIRS_UP: return R.drawable.stairs_up;
		case TEX_STAIRS_DOWN: return R.drawable.stairs_down;
		case TEX_POWER_UNIT: return R.drawable.powerunit;
		case TEX_MEDI_BAY: return R.drawable.medibay;
		case TEX_CLONE_GENERATOR: return R.drawable.clone_generator;
		case TEX_GUN_VENDING_MACHINE: return R.drawable.gun_vending_machine;
		case TEX_GRENADE_VENDING_MACHINE: return R.drawable.grenade_vending_machine;
		case TEX_RUBBLE_1_TRANSP: return R.drawable.rubble_1_transp;
		case TEX_RUBBLE_2_TRANSP: return R.drawable.rubble_2_transp;
		case TEX_RUBBLE_3_TRANSP: return R.drawable.rubble_3_transp;
		case TEX_RUBBLE_4_TRANSP: return R.drawable.rubble_4_transp;
		case TEX_ALIEN_COLONY: return R.drawable.alien_colony;
		case TEX_SHADOW_L_SHAPE: return R.drawable.shadow_l_shape;
		case TEX_SHADOW_RIGHT: return R.drawable.shadow_right;
		case TEX_SHADOW_SQUARE: return R.drawable.shadow_square;
		case TEX_SHADOW_TOP: return R.drawable.shadow_top;
		default:
			if (Statics.RELEASE_MODE == false) {
				throw new RuntimeException("Unknown sq type: " + tex);
			} else {
				return R.drawable.railway_ew; // Default
			}
		}
	}


	/**
	 * Return -1 if we can't handle it yet.
	 * 
	 */
	public static int GetSceneryResourceID(short code) {
		switch (code) {
		case MapDataTable.FLOWERPOT: return R.drawable.plant;
		case MapDataTable.FLOWERPOT2: return R.drawable.plant2;
		case MapDataTable.DESK: return R.drawable.desk_s;
		case MapDataTable.PIPES_L: return R.drawable.pipes_l;
		case MapDataTable.PIPES_R: return R.drawable.pipes_r;
		case MapDataTable.RUBBLE: return R.drawable.rubble;
		case MapDataTable.RUBBLE_RED: return R.drawable.rubble_red;
		case MapDataTable.RUBBLE_YELLOW: return R.drawable.rubble_yellow;
		case MapDataTable.RUBBLE_WHITE: return R.drawable.rubble_white;
		case MapDataTable.CHAIR_T: return R.drawable.chair_t;
		case MapDataTable.CHAIR_B: return R.drawable.chair_b;
		case MapDataTable.CHAIR2_L: return R.drawable.chair2_l;
		case MapDataTable.CHAIR2_R: return R.drawable.chair2_r;
		case MapDataTable.CRYO_CHAMBER: return R.drawable.cryo_chamber;
		case MapDataTable.COMPUTER_SCREEN1: return R.drawable.computer_screen1;
		case MapDataTable.PLANT3: return R.drawable.plant3;
		case MapDataTable.RED_WALL_PANEL: return R.drawable.red_wall_panel;
		case MapDataTable.GREEN_WALL_PANEL: return R.drawable.green_wall_panel;
		case MapDataTable.DAMAGED_FLOOR: return R.drawable.damaged_floor;
		case MapDataTable.DAMAGED_FLOOR2: return R.drawable.damaged_floor2;
		case MapDataTable.DAMAGED_FLOOR3: return R.drawable.damaged_floor3;
		case MapDataTable.DAMAGED_FLOOR4: return R.drawable.damaged_floor4;
		case MapDataTable.DAMAGED_FLOOR5: return R.drawable.damaged_floor5;
		case MapDataTable.DAMAGED_FLOOR6: return R.drawable.damaged_floor6;
		case MapDataTable.DAMAGED_FLOOR7: return R.drawable.damaged_floor7;
		case MapDataTable.GRILL: return R.drawable.grill;
		case MapDataTable.SINGLE_PIPE_L: return R.drawable.single_pipe_l;
		case MapDataTable.SINGLE_PIPE_R: return R.drawable.single_pipe_r;
		case MapDataTable.HORIZONTAL_PIPE: return R.drawable.pipe_horiz;
		case MapDataTable.PLANT4: return R.drawable.plant4;
		case MapDataTable.PLANT5: return R.drawable.plant5;
		case MapDataTable.PLANT6: return R.drawable.plant6;
		case MapDataTable.PLANT7: return R.drawable.plant7;
		case MapDataTable.REPLICATOR: return R.drawable.replicator;
		case MapDataTable.CRACK1: return R.drawable.crack1;
		case MapDataTable.CRACK2: return R.drawable.crack2;
		case MapDataTable.WEED1: return R.drawable.weed1;
		case MapDataTable.WEED2: return R.drawable.weed2;
		case MapDataTable.GRAFFITI_DIE: return R.drawable.graffiti_die;
		case MapDataTable.GRAFFITI_HELP: return R.drawable.graffiti_help;
		case MapDataTable.GRAFFITI_BRAINS: return R.drawable.graffiti_brains;
		case MapDataTable.BED_TOP: return R.drawable.bed_top;
		case MapDataTable.BED_BOTTOM: return R.drawable.bed_bottom;
		default:
			return -1;
		}
	}


	public static short GetRandomRubbleTex() {
		short nums[] = {TEX_RUBBLE_1, TEX_RUBBLE_2, TEX_RUBBLE_3, TEX_RUBBLE_4};
		return nums[NumberFunctions.rnd(0, 3)];
	}


	public static short GetRandomRubbleTexTransp() {
		short nums[] = {TEX_RUBBLE_1_TRANSP, TEX_RUBBLE_2_TRANSP, TEX_RUBBLE_3_TRANSP, TEX_RUBBLE_4_TRANSP};
		return nums[NumberFunctions.rnd(0, 3)];
	}


}
