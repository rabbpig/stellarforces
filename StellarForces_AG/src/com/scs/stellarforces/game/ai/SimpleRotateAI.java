package com.scs.stellarforces.game.ai;

import ssmith.lang.NumberFunctions;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;

import dsr.data.AppletMapSquare;
import dsr.data.UnitData;
import dsrwebserver.tables.MapDataTable;

public class SimpleRotateAI extends AbstractAI {

	private int num_rotations;

	public SimpleRotateAI(UnitData unit) {
		super(unit);
	}


	// Return false if nothing to do
	@Override
	public boolean process(GameModule game) {
		if (Statics.DEBUG) {
			game.root_cam.lookAt(unit.model, true);
		}

		boolean facing_wall = false;
		boolean facing_door = false;
		AppletMapSquare sq = this.unit.model.getSquareInFrontOfUnit_MaybeNULL();
		if (sq != null) {
			if (sq.major_type == MapDataTable.MT_WALL || sq.major_type == MapDataTable.MT_COMPUTER) {
				facing_wall = true;
			} else if (sq.door_type > 0) {
				facing_door = true;
			}
		}

		if (super.getSeenEnemy(game) == null) { // If we cant see any enemies...
			if (facing_wall == false) { // Not facing wall
				if (unit.getAPs() <= unit.opp_fire_aps_req+6) {
					// If we're about to use up our APs, stop now
					return false;
				}

				// If we've turned enough times, end now
				if (num_rotations > NumberFunctions.rnd(10, 16)) { 
					return false;
				}

				// We're not facing a door, so keep turning
				if (facing_door == false || num_rotations == 0) { // Turn at least once
					unit.turnBy(45);
					num_rotations++;
					return true;
				} else {
					// We are facing a door, so stop now
					return false;
				}
			} else { // Facing a wall
				unit.turnBy(45);
				num_rotations++;
				return true;
			}
		} else { // We can see an enemy!
			if (unit.getAPs() >= this.getShotAPs()) {
				super.shootAtEnemyEnemies(game);
				return true;
			} else {
				// Can't shoot any more!
				return false;
			}
		}
	}

}
