package com.scs.stellarforces.game.ai;

import ssmith.android.lib2d.Ray;

import com.scs.stellarforces.game.GameModule;

import dsr.data.UnitData;
import dsrwebserver.tables.UnitsTable;

public abstract class AbstractAI {

	public static final int AI_ROTATE = 1;
	public static final int AI_DESTROY_COMPUTERS = 2;

	protected UnitData unit;

	public AbstractAI(UnitData _unit) {
		super();

		unit = _unit;
	}


	public abstract boolean process(GameModule game);


	public void shootAtEnemyEnemies(GameModule game) {
		UnitData enemy = this.getSeenEnemy(game);
		if (enemy != null) {
			// Shoot!
			boolean can_target_see = game.canUnitSeeUnit(enemy, this.unit, true);
			Ray targetting_ray = new Ray();
			targetting_ray.origin = unit.model.getWorldCentre_CreatesNew();
			targetting_ray.direction = enemy.model.getWorldCentre_CreatesNew().copy().subtract(unit.model.getWorldCentre_CreatesNew()).normalize();
			game.shoot(this.unit, game.getAimedShotAccuracy(this.unit), this.getShotAPs(), can_target_see, targetting_ray);
		}

	}


	public UnitData getSeenEnemy(GameModule game) {
		// Loop through enemy units
		for (int i_enemy=0 ; i_enemy<game.units.length ; i_enemy++) {
			if (game.game_data.areSidesFriends(game.units[i_enemy].getSide(), this.unit.getSide()) == false) {
				if (game.units[i_enemy].getStatus() == UnitsTable.ST_DEPLOYED) {
					// Can OUR unit see the enemy unit?
					if (game.canUnitSeeUnit(this.unit, game.units[i_enemy], true)) {
						return game.units[i_enemy];
					}
				}
			}

		}
		return null;
	}


	protected int getShotAPs() {
		if (this.unit.current_item != null) {
			return this.unit.current_item.aimed_shot_aps;
		} else {
			return 999;
		}
	}


}