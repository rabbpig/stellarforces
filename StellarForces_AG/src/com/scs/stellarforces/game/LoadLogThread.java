package com.scs.stellarforces.game;

import java.io.IOException;

import ssmith.android.framework.AbstractActivity;

import com.scs.stellarforces.Statics;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.WGet_SF;

public class LoadLogThread extends Thread {
	
	private AbstractActivity act;
	private int gameid;
	public String cache = null;
	
	public LoadLogThread(AbstractActivity _act, int _gameid) {
		super("LoadLogThread");
		
		act = _act;
		gameid = _gameid;
		
		this.setDaemon(true);
		
		start();
	}
	
	
	public void run() {
		try {
			WGet_SF wget = new WGet_SF(this.act, null, WGet_SF.T_OTHER, Statics.URL_FOR_CLIENT + "/androidcomm/GetGameLog.cls?android_login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&android_pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD) + "&gameid=" + gameid, null, true);
			if (wget.getResponseCode() == 200) {
				cache = wget.getResponse();
			}
			//showLog(wget.getResponse());
		} catch (IOException ex) {
			//ErrorReporter.getInstance().handleException(ex);
			//this.displayMessage(ex.toString());
			//showLog(ex.toString());
		}

	}

}
