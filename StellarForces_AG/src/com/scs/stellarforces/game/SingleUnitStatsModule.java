package com.scs.stellarforces.game;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

import dsr.data.EquipmentData;
import dsr.data.UnitData;

public class SingleUnitStatsModule extends SimpleScrollingAbstractModule {
	
	private UnitData unit;
	private EquipmentData[] equipment;
	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();
	private static Paint paint_small_text = new Paint();

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.06f));

		paint_small_text.setARGB(255, 255, 255, 255);
		paint_small_text.setAntiAlias(true);
		paint_small_text.setTextSize(Statics.GetHeightScaled(0.04f));
	}


	public SingleUnitStatsModule(AbstractModule ret, UnitData _unit, EquipmentData[] _equipment) {
		super(-1);
		
		this.mod_return_to = ret;
		unit = _unit;
		equipment = _equipment;
		
		this.setBackground(Statics.BACKGROUND_R);
	}
	
	
	@Override
	public void started() {
		AbstractActivity act = Statics.act;
		
		this.root_node.detachAllChildren();
		
		VerticalFlowLayout layout = new VerticalFlowLayout("vfl", Statics.SCREEN_HEIGHT * 0.005f);
		Label name = new Label("name", unit.name, null, paint_large_text);
		layout.attachChild(name);
		name = new Label("name", "Health: " + unit.getHealth() + "/" + unit.getMaxHealth(), null, paint_normal_text);
		layout.attachChild(name);
		name = new Label("name", "APS: " + unit.aps + "/" + unit.max_aps, null, paint_normal_text);
		layout.attachChild(name);
		name = new Label("name", "Shot Skill: " + unit.shot_skill, null, paint_normal_text);
		layout.attachChild(name);
		name = new Label("name", "Combat Skill: " + unit.combat_skill, null, paint_normal_text);
		layout.attachChild(name);
		name = new Label("name", "Strength: " + unit.strength, null, paint_normal_text);
		layout.attachChild(name);
		name = new Label("name", "Energy: " + unit.curr_energy, null, paint_normal_text);
		layout.attachChild(name);
		name = new Label("name", "Morale: " + unit.curr_morale, null, paint_normal_text);
		layout.attachChild(name);

		if (unit.can_use_equipment) {
			name = new Label("name", act.getString(R.string.unit_is_carrying) + ":-", null, paint_large_text);
			layout.attachChild(name);
			String s = GameModule.GetUnitsEquipmentAsString(unit, equipment);
			MultiLineLabel mll = new MultiLineLabel("equipment", s, null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.8f);
			layout.attachChild(mll);
			
		}
		
		this.root_node.attachChild(layout);
		
		this.root_node.updateGeometricState();
		this.root_cam.lookAt(layout, true);
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		this.returnTo();
	}
	

}
