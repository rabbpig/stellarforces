package com.scs.stellarforces.game;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

public final class Tutorial {

	private GameModule game;
	private int stage = 0;
	private ArrayList<String> text = new ArrayList<String>();

	public Tutorial(GameModule m) {
		AbstractActivity act = Statics.act;
		
		game = m;

		text.add(act.getString(R.string.tutorial_0));
		text.add(act.getString(R.string.tutorial_1));
		text.add(act.getString(R.string.tutorial_2));
		text.add(act.getString(R.string.tutorial_3));
		text.add(act.getString(R.string.tutorial_4));
		text.add(act.getString(R.string.tutorial_5));
		text.add(act.getString(R.string.tutorial_6));
		text.add(act.getString(R.string.tutorial_7));
		//if (game.getCurrentUnit() != null) {
		//if (game.getCurrentUnit().current_item == null) {
		text.add(act.getString(R.string.tutorial_8));
		//}
		//}
		text.add(act.getString(R.string.tutorial_9));
		text.add(act.getString(R.string.tutorial_10));
		//if (game.getCurrentUnit() != null) {
		//if (game.getCurrentUnit().current_item != null) {
		text.add(act.getString(R.string.tutorial_11));
		text.add(act.getString(R.string.tutorial_12));
		text.add(act.getString(R.string.tutorial_13));
		text.add(act.getString(R.string.tutorial_14));
		text.add(act.getString(R.string.tutorial_15));
		//}
		//}
		text.add(act.getString(R.string.tutorial_16));
		text.add(act.getString(R.string.tutorial_17));
		text.add(act.getString(R.string.tutorial_18));
		text.add(act.getString(R.string.tutorial_19));
		text.add(act.getString(R.string.tutorial_20));
		text.add(act.getString(R.string.tutorial_21));
		text.add(act.getString(R.string.tutorial_22));
		text.add(act.getString(R.string.tutorial_23));
		text.add(act.getString(R.string.tutorial_24));
		text.add(act.getString(R.string.tutorial_25));
		text.add(act.getString(R.string.tutorial_26));
		text.add(act.getString(R.string.tutorial_27));
		//}
	}


	public String getText() {
		AbstractActivity act = Statics.act;
		
		if (stage < text.size()) {
			String s = text.get(stage);
			stage++;
			if (game.game_data.mission_type == 2 && stage == 1) {
				stage++; // Skip this one.
				return getText();
			}
			if (game.game_data.mission_type == 1 && stage == 2) {
				stage++; // Skip this one.
				return getText();
			}
			return s;
		} else {
			return act.getString(R.string.tutorial_28);
		}
	}

}

