package com.scs.stellarforces.game;

import dsr.data.EquipmentData;
import dsr.data.UnitData;

public class ExplosionPendingData {
	
	public EquipmentData eq;
	public UnitData caused_by;
	
	public ExplosionPendingData(EquipmentData _eq, UnitData _caused_by) {
		eq = _eq;
		caused_by = _caused_by;
	}

}
