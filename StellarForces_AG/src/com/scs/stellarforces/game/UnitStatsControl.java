package com.scs.stellarforces.game;

import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.ToggleButton;
import ssmith.android.lib2d.layouts.HorizontalFlowGridLayout;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

public class UnitStatsControl extends HorizontalFlowGridLayout {

	private static final float OVERALL_WIDTH = Statics.SCREEN_WIDTH * 0.90f;
	private static float ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT;

	private static Paint paint_unit_name = new Paint();
	private static Paint paint_armour_name = new Paint();

	static {
		paint_unit_name.setARGB(255, 255, 255, 255);
		paint_unit_name.setAntiAlias(true);
		paint_unit_name.setTextSize(Statics.GetHeightScaled(0.06f));

		paint_armour_name.setARGB(255, 255, 255, 255);
		paint_armour_name.setAntiAlias(true);
		paint_armour_name.setTextSize(Statics.GetHeightScaled(0.03f));
	}


	public UnitStatsControl(float GAP, String name, String health, String aps, String shot_skill, String cc_skill, String str) {
		this(GAP, name, health, aps, shot_skill, cc_skill, str, "", "");
	}
	
	
	public UnitStatsControl(float GAP, String name, String health, String aps, String shot_skill, String cc_skill, String str, String energy, String morale) {
		super("UnitStatsControl", GAP);

		ARMOUR_ICON_WIDTH = OVERALL_WIDTH * 0.6f / 7;
		ARMOUR_ICON_HEIGHT = ARMOUR_ICON_WIDTH * 0.9f;

		Label l = new Label("Unit_name", name, null, paint_unit_name, false);
		l.setSize(OVERALL_WIDTH * 0.4f, ARMOUR_ICON_HEIGHT);
		this.attachChild(l);

		
		ToggleButton b = new ToggleButton(health, null, null, paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
		this.attachChild(b);
		b = new ToggleButton(aps, null, null, paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
		this.attachChild(b);
		b = new ToggleButton(shot_skill, null, null, paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
		this.attachChild(b);
		b = new ToggleButton(cc_skill, null, null, paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
		this.attachChild(b);
		b = new ToggleButton(str, null, null, paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
		this.attachChild(b);
		if (energy.length() > 0) {
		b = new ToggleButton(energy, null, null, paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
		this.attachChild(b);
		}
		if (morale.length() > 0) {
			b = new ToggleButton(morale, null, null, paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
			this.attachChild(b);
		}

		this.updateGeometricState(); // to get size

	}


}

