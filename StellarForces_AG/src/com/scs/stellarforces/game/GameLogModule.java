package com.scs.stellarforces.game;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractComplexModule;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import ssmith.android.lib2d.shapes.Geometry;
import ssmith.util.IDisplayMessages;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public class GameLogModule extends AbstractComplexModule implements IDisplayMessages {

	private Label label2;

	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));
	}


	public GameLogModule(AbstractActivity act, AbstractModule _mod_return_to, String log) {
		super(-1);

		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.mod_return_to = _mod_return_to;
		
		super.dont_scroll_lr = true;
		
		this.setBackground(Statics.BACKGROUND_R);

		// Need this to show any wget messages
		label2 = new Label("display", "", null, paint_normal_text);
		label2.setCentre(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2);
		stat_node.attachChild(label2);
		
		stat_node.updateGeometricState();

		this.showLog(log);

	}


	/*@Override
	public void started() {
		if (cache == null) {
			try {
				WGet_SF wget = new WGet_SF(this.act, this, WGet_SF.T_OTHER, Statics.URL_FOR_CLIENT + "/androidcomm/GetGameLog.cls?android_login=" + CommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&android_pwd=" + CommFuncs.URLEncodeString(Statics.LAST_PWD) + "&gameid=" + gameid, null, true);
				if (wget.getResponseCode() == 200) {
					cache = wget.getResponse();
				}
				showLog(wget.getResponse());
			} catch (IOException ex) {
				//ErrorReporter.getInstance().handleException(ex);
				this.displayMessage(ex.toString());
				showLog(ex.toString());
			}

		}
	}*/


	private void showLog(String log) {
		this.root_node.removeAllChildren();

		Label l = new Label("Title", "Game Log", 0, 0, null, paint_large_text, true);
		l.setCentre(Statics.SCREEN_WIDTH/2, 0);//paint_large_text.getTextSize());
		this.root_node.attachChild(l);

		MultiLineLabel mll = new MultiLineLabel("Log", log, null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
		mll.setLocation(5, l.getHeight() * 2);//.setCentre(Statics.SCREEN_WIDTH/2, l.getHeight() * 2);
		this.root_node.attachChild(mll);
		this.root_node.updateGeometricState();
		
		this.root_cam.lookAt(mll.getWorldCentreX(), mll.getWorldBounds().bottom, true);

	}


	@Override
	public boolean componentClicked(Geometry c) {
		this.returnTo();
		return true;
	}


	@Override
	public void updateGame(long interpol) {
		// Do nothing

	}


	@Override
	public void displayMessage(String s) {
		label2.setText(s);

	}
	
	
	public boolean playMusic() {
		// Override if required
		return false;
		
	}
	

}
