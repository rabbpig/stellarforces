package com.scs.stellarforces.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.ErrorReporter;
import ssmith.android.framework.modules.AbstractComplexModule;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.ConfirmModule;
import ssmith.android.io.IOFunctions;
import ssmith.android.lib2d.MyPickResults;
import ssmith.android.lib2d.MyPointF;
import ssmith.android.lib2d.PickData;
import ssmith.android.lib2d.Ray;
import ssmith.android.lib2d.Spatial;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.shapes.Geometry;
import ssmith.android.lib2d.shapes.Line;
import ssmith.android.util.ImageCache;
import ssmith.lang.GeometryFuncs;
import ssmith.lang.NumberFunctions;
import ssmith.util.IDisplayMessages;
import ssmith.util.Interval;
import ssmith.util.PointByte;
import ssmith.util.TSArrayList;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.RectF;
import android.net.Uri;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.UpdateServerThread;
import com.scs.stellarforces.graphics.Bullet;
import com.scs.stellarforces.graphics.BulletLaser;
import com.scs.stellarforces.graphics.CCExplosion;
import com.scs.stellarforces.graphics.DummyWall;
import com.scs.stellarforces.graphics.EquipmentModel;
import com.scs.stellarforces.graphics.Explosion;
import com.scs.stellarforces.graphics.FireExtinguisherEffect;
import com.scs.stellarforces.graphics.FlameThrowerEffect;
import com.scs.stellarforces.graphics.GameObject;
import com.scs.stellarforces.graphics.GasCannister;
import com.scs.stellarforces.graphics.JokeSpider;
import com.scs.stellarforces.graphics.MapNode;
import com.scs.stellarforces.graphics.MapSquareImage;
import com.scs.stellarforces.graphics.TeleportEffect;
import com.scs.stellarforces.graphics.gui.HUD;
import com.scs.stellarforces.graphics.gui.IconNode;
import com.scs.stellarforces.graphics.icons.AbstractIcon;
import com.scs.stellarforces.graphics.icons.ShowEnemyIcon;
import com.scs.stellarforces.graphics.units.AbstractUnit;
import com.scs.stellarforces.graphics.units.Blob;
import com.scs.stellarforces.instructions.InstructionsModule;
import com.scs.stellarforces.main.lite.R;
import com.scs.stellarforces.message.SendMessageModule;
import com.scs.stellarforces.start.CurrentGameDetailsModule;
import com.scs.stellarforces.start.GetGamesModule;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.EquipmentDataComms;
import dsr.comms.EventDataComms;
import dsr.comms.GameDataComms;
import dsr.comms.GetAllGameData;
import dsr.comms.MapDataComms;
import dsr.comms.SetStatComms;
import dsr.comms.UnitDataComms;
import dsr.comms.VisibleEnemyComms;
import dsr.data.AppletMapSquare;
import dsr.data.ClientMapData;
import dsr.data.EquipmentData;
import dsr.data.GameData;
import dsr.data.UnitData;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public final class GameModule extends AbstractComplexModule implements IDisplayMessages {

	private static final String CONFIRM_END_TURN = "end_turn";
	private static final String CONFIRM_START_TUTORIAL = "start_tutorial";
	private static final String CONFIRM_REMOVE_ITEM = "remove_item";

	// Menu options
	public static final int MENU_ADVANCED_SCANNER = 2;
	public static final int MENU_GRID = 3;
	public static final int MENU_GAME_LOG = 4;
	public static final int MENU_START_TUTORIAL = 5;
	//public static final int MENU_HELP_MODE = 6;
	public static final int MENU_MISSION_DESC = 7;
	public static final int MENU_GAME_CHAT = 8;
	public static final int MENU_UNIT_STATS = 9;
	public static final int MENU_GAME_DETAILS = 10;
	public static final int MENU_MSG_OPPONENT = 11;
	public static final int MENU_MSG_COMRADE = 12;
	public static final int MENU_INSTRUCTIONS = 13;

	public static final boolean VERBOSE_COMMS = false;

	// Game settings
	private static final byte MIN_SHOT_ACC = 15;
	private static final byte MAX_SHOT_ACC = 85; // Don't make too high otherwise unit can be picked off at distance easily
	private static final byte OPP_FIRE_ACC_REDUCTION = 15;
	private static final int POINT_BLANK_DAMAGE_BONUS = 5;
	public static final float MIN_DAMAGE_REDUCTION_RANGE = 10;
	public static final int STRENGTH_TO_DESTROY_COMP = 5;
	private static final int OPP_CC_COST = 8;
	private static final long TIME_BETWEEN_EXPLOSIONS = 2000;

	// Client stages
	//private static final byte APP_LOADING_AVAILABLE_GAMES = 0;
	//private static final byte APP_PLAYER_SELECT_GAMES = 1;
	//private static final byte APP_LOADING_GAME_DATA = 2;
	//private static final byte APP_LOADING_MODELS = 3;
	private static final byte APP_STARTED = 4;
	private static final byte APP_TURN_ENDED_SHOWING_EXPLOSIONS = 5;
	private static final byte APP_TURN_ENDED_MOVE_AI = 6;
	private static final byte APP_TURN_ENDED_WAIT_FOR_COMMS = 7;
	private static final byte APP_TURN_FINISHED_COMPLETELY = 8;
	private static final byte APP_CRASHED = 9;
	private byte client_mode = APP_STARTED;

	// Menu modes
	public static final byte MM_NO_UNIT_SELECTED = 1;
	public static final byte MM_UNIT_SELECTED = 2;
	public static final byte MM_EQUIPMENT_MENU = 3;
	public static final byte MM_CHANGE_ITEM = 4;
	public static final byte MM_PICKUP_ITEM = 5;
	public static final byte MM_SHOOTING = 6;
	public static final byte MM_THROW = 7;
	//public static final byte MM_TELEPORT = 8;
	public static final byte MM_PRIME_GRENADE = 9;
	public static final byte MM_TURN_FINISHED_NO_MENU = 10;
	//public static final byte MM_CONFIRM_END_TURN = 11;

	// AP cost
	public static final byte APS_CHANGE_ITEM = 13;
	public static final byte APS_PICKUP_ITEM = 8;
	public static final byte APS_DROP_ITEM = 3;
	public static final byte APS_REMOVE_ITEM = 8;
	public static final byte APS_PRIME = 10;
	public static final byte APS_ACTIVATE = 10;
	public static final byte APS_THROW = 10;
	public static final byte APS_CATCH = 3;
	public static final byte APS_USE_MEDIKIT = 20;
	public static final byte APS_OPEN_DOOR = 8;
	public static final byte APS_USE_SCANNER = 8;
	public static final byte APS_TELEPORT = 40;
	public static final byte APS_CREATE_ABSORB = 10;
	//public static final byte APS_BUILD = 40; // Keep high so they don't build loads
	public static final byte APS_DISMANTLE = 15;

	// Blob
	public static final byte APS_SPLIT = 10;
	public static final byte APS_ABSORB = 10;
	public static final byte APS_EXPLODE = 20;

	// AE Shots
	public static final byte AE_ACC_SHOT_APS = 25;
	public static final byte AE_SNAP_SHOT_APS = 15;
	public static final byte AE_AUTO_SHOT_APS = 7;

	public GameData game_data;
	public UnitData current_unit;
	private boolean check_our_init_visible_units;
	public UnitData[] units;
	public ClientMapData mapdata;
	public EquipmentData[] equipment;
	public boolean help_mode_on = true;
	public HUD hud;
	public IconNode icon_table;
	private Line shot_line;
	private Spatial highlighter;
	private long time_since_last_explosion;
	private TSArrayList<IProcessable> objects_for_processing = new TSArrayList<IProcessable>();
	public MapNode map_model;
	public int next_to_deploy = -1;
	private ArrayList<ExplosionPendingData> explosions_to_show = new ArrayList<ExplosionPendingData>();
	private ArrayList<AbstractIcon> orig_visible_enemy_icons = new ArrayList<AbstractIcon>();
	private RectF signal_rect = new RectF(0, 0, 10f, 10f);
	public static boolean show_grid;
	private UpdateServerThread update_server_thread;
	private boolean wait_for_data_to_be_sent = false;
	public boolean show_tutorial = false;
	private Tutorial tutorial;
	private GameLogModule game_log_module;
	private boolean showing_please_wait = false;
	private boolean game_finished = false;
	private Button menu_icon, end_turn_icon, centre_on_unit;
	public InGameMenu submenu;
	public static ImageCache img_cache;
	public boolean ap_lock = false;
	public boolean question = false;
	private LoadLogThread load_log;
	private boolean mentioned_ai = false;
	private boolean no_ai = true;
	private ArrayList<Integer> units_deployed = new ArrayList<Integer>();  // We can only undeploy units we have deployed (i.e. not auto-deployed)
	private ArrayList<Integer> next_deploy_dir = new ArrayList<Integer>();
	public boolean is_april_fools_day = false;
	private boolean moving_cam_to_explosion = true;
	private Interval ambience_interval = new Interval(1000 * 30, true);
	private Interval check_shot_line_int = new Interval(300, false);

	private static int de_offset = 0;
	private static float de_arr[] = {5, 10, 10, 10};

	private static Paint paint_white_line = new Paint();
	public static Paint paint_yellow_box = new Paint();
	private static Paint paint_shot_line_unblocked = new Paint();
	private static Paint paint_shot_line_blocked = new Paint();

	static {
		paint_white_line.setARGB(255, 255, 255, 255);
		paint_white_line.setAntiAlias(true);
		paint_white_line.setStyle(Style.STROKE);

		paint_yellow_box.setARGB(255, 255, 255, 0);
		paint_yellow_box.setAntiAlias(false);
		paint_yellow_box.setStyle(Style.FILL);

		paint_shot_line_unblocked.setARGB(255, 0, 255, 0);
		paint_shot_line_unblocked.setAntiAlias(false);
		paint_shot_line_unblocked.setStyle(Style.STROKE);
		paint_shot_line_unblocked.setStrokeWidth(Statics.SCREEN_WIDTH / 200f);

		paint_shot_line_blocked.setARGB(255, 255, 0, 0);
		paint_shot_line_blocked.setAntiAlias(false);
		paint_shot_line_blocked.setStyle(Style.STROKE);
		paint_shot_line_blocked.setStrokeWidth(Statics.SCREEN_WIDTH / 200f);
	}


	public GameModule(AbstractActivity act, GetAllGameData all_game_data) {
		super(Statics.MOD_MENU);

		if (Statics.GetTypeface(act) != null) {
			paint_white_line.setTypeface(Statics.GetTypeface(act));
		}

		Calendar c = Calendar.getInstance();
		is_april_fools_day = (c.get(Calendar.MONTH) == Calendar.APRIL && c.get(Calendar.DAY_OF_MONTH) == 1);

		if (img_cache == null) {
			img_cache = new ImageCache(act.getBaseContext().getResources());
		}

		update_server_thread = new UpdateServerThread(this);

		// Get all the data from the setup thread
		this.game_data = all_game_data.game_data;
		//this.units = (UnitData[])(all_game_data.units.toArray());
		this.units = new UnitData[0];
		this.units = all_game_data.units.toArray(units);

		this.mapdata = all_game_data.getMap();
		this.equipment = all_game_data.equipment;
		//this.game_data.seen_enemies = all_game_data.seen_enemies;
		//this.game_data.heard_enemies = all_game_data.heard_enemies;

		check_our_init_visible_units = true;

		// Stat node
		this.stat_node.detachAllChildren();

		menu_icon = new Button("Menu", AbstractIcon.paint_transp, AbstractIcon.paint_white_text, Statics.img_cache.getImage(R.drawable.menu_frame_blue, Statics.ICONS_WIDTH, Statics.ICONS_HEIGHT));
		menu_icon.setLocation(Statics.SCREEN_WIDTH - Statics.ICONS_WIDTH, 0);
		stat_node.attachChild(menu_icon);

		if (game_data.game_status != GamesTable.GS_DEPLOYMENT) {
			end_turn_icon = new Button("End\nTurn", AbstractIcon.paint_transp, AbstractIcon.paint_white_text, Statics.img_cache.getImage(R.drawable.menu_frame_red, Statics.ICONS_WIDTH, Statics.ICONS_HEIGHT));
			end_turn_icon.setLocation(Statics.SCREEN_WIDTH-Statics.ICONS_WIDTH, Statics.SCREEN_HEIGHT-Statics.ICONS_HEIGHT);
			stat_node.attachChild(end_turn_icon);
		}

		if (Statics.USE_NEW_MOVEMENT_ICONS == 1) {
			centre_on_unit = new Button("Centre\non Unit", AbstractIcon.paint_transp, AbstractIcon.paint_white_text, Statics.img_cache.getImage(R.drawable.menu_frame_yellow, Statics.ICONS_WIDTH, Statics.ICONS_HEIGHT));
			centre_on_unit.setLocation(Statics.SCREEN_WIDTH-(Statics.ICONS_WIDTH*2), Statics.SCREEN_HEIGHT-Statics.ICONS_HEIGHT);
			stat_node.attachChild(centre_on_unit);
		}

		icon_table = new IconNode(this);
		hud = new HUD(this, icon_table, paint_white_line.getTypeface());
		stat_node.attachChild(hud);
		submenu = new InGameMenu(this, paint_white_line.getTypeface());
		stat_node.attachChild(submenu);
		stat_node.updateGeometricState();
		stat_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);

		this.objects_for_processing.add(hud);

		//hud.appendText("Welcome to Stellar Forces for Android.");

		this.root_node.detachAllChildren();

		createMapModel();

		// Attach unit models to root node
		for(int i=0 ; i<units.length ; i++) {
			UnitData unit = units[i];
			// Create the model
			if (unit.getStatus() == UnitsTable.ST_DEPLOYED) {
				unit.model = AbstractUnit.Factory(this, unit);
				root_node.attachChild(unit.model);
				unit.model.visible = false; // By default
				unit.updateModelFromUnitData();
			}
		}

		// Attach visible equipment models to root node
		for(int i=0 ; i<equipment.length ; i++) {
			EquipmentData equip = equipment[i];
			if (equip.getUnitID() <= 0 && equip.destroyed == false) {
				try {
					PointByte p = EquipmentData.GetEquipmentMapSquare(this.mapdata, equip);
					if (p != null) {
						equip.model = EquipmentModel.Factory(this, equip, p.x, p.y);
						this.attachToRootNode(equip.model, false);
						if (game_data.is_advanced == 1 && equip.seen_by_side[game_data.our_side] == 0) {
							//if (equip.g.equipcode.equalsIgnoreCase("FLAG") == false && equip.equipcode.equalsIgnoreCase("EGG") == false) { // Always show flags/eggs
							equip.model.visible = false;
							//}
						}
					}
				} catch (RuntimeException ex) {
					ErrorReporter.getInstance().handleSilentException(ex);
					// It's invalid (unit -1, mapsquare -1, -1, so make it disappear.  No!  In case it's a flag 
					// equip.destroyed = true;  
					//this.updateEquipmentOnServer(equip, (byte)0, (byte)0);
				}
			}
		}

		UnitData unit_to_be_selected = null;
		// Show our units
		for(int i=0 ; i<units.length ; i++) {
			UnitData unit = units[i];
			if (this.game_data.areSidesFriends(unit.getSide(), game_data.our_side)) {
				if (unit.getStatus() == UnitsTable.ST_DEPLOYED) {
					unit.model.visible = true;
					unit.updateModelFromUnitData();
					if (game_data.game_status == GamesTable.GS_STARTED) {
						if (this.game_data.turn_side == this.game_data.our_side) {
							if (unit.getSide() == game_data.our_side) {
								// Select our first unit
								if (unit_to_be_selected == null) {
									unit_to_be_selected = unit;
								}
								// Create their icon
								//SelectOurUnitIcon icon = new SelectOurUnitIcon(this, unit);
								//this.icon_table.our_unit_icons.add(icon);
							}
						}
					}
				}
			} else {
				// Create the enemy's icon
				new ShowEnemyIcon(this, unit);
			}
		}

		root_node.updateGeometricState();

		this.root_cam.lookAt(this.root_node, false); // Just in case we don't need to look at anything

		if (game_data.game_status == GamesTable.GS_DEPLOYMENT) {
			this.updateMenu(MM_NO_UNIT_SELECTED);//MM_DEPLOYING_UNITS);
			this.getNextUnitToDeploy();
			this.addToHUD(act.getString(R.string.deployment_squares));
			this.addToHUD(act.getString(R.string.right_click_to_deploy));
			//this.setNextDeployDir();
		} else if (game_data.game_status == GamesTable.GS_STARTED && unit_to_be_selected != null) { // Need to check if unit_to_be_selected is not null in case they don't have any units left (i.e. client crashed after unit died but before ending turn)
			recalcVisibleEnemiesAndOppFire(false, null);
			this.selectOurUnit(unit_to_be_selected, true);
			this.updateMenu(MM_UNIT_SELECTED); // Must be after we've reclac'd visible enemies so we can show their icons.
			if (this.game_data.max_turns == this.game_data.turn_no) {
				this.addToHUD(act.getString(R.string.last_turn));
			}
		} else if (game_data.game_status == GamesTable.GS_FINISHED) {
			// Do nothing?
		} else {
			this.updateMenu(MM_NO_UNIT_SELECTED);
			this.addToHUD(act.getString(R.string.no_units));
		}

		this.loadTheLog();

		if (game_data.is_practise == 1 && isThereAnAISide() && game_data.game_status == GamesTable.GS_STARTED) {
			AbstractModule m = new ConfirmModule(act, this, "Start Tutorial?", "Would you like to turn Tutorial Mode on to help you play the game?", Statics.BACKGROUND_R, CONFIRM_START_TUTORIAL);
			this.getThread().setNextModule(m);
		}

	}


	private int getNextDeployDir() {
		while (this.next_deploy_dir.size() <= 0) {
			this.next_deploy_dir.add(NumberFunctions.rnd(0, 7) * 45);
		}
		return this.next_deploy_dir.remove(this.next_deploy_dir.size() - 1);
	}


	private void loadTheLog() {
		AbstractActivity act = Statics.act;

		load_log = new LoadLogThread(act, this.game_data.game_id);

	}


	@Override
	public void started() {
		if (Statics.data.containsKey(CONFIRM_END_TURN)) {
			if (Statics.data.get(CONFIRM_END_TURN).equalsIgnoreCase(ConfirmModule.YES)) {
				showExplosionsAndEndTurn();
			} else {
				this.returnTo();
			}
		} else if (Statics.data.containsKey(ScannerModule.SCANNER_COORDS)) {
			try {
				String data[] = Statics.data.get(ScannerModule.SCANNER_COORDS).split(",");
				int x = NumberFunctions.ParseInt(data[0]);
				int y = NumberFunctions.ParseInt(data[1]);
				this.root_cam.lookAt(x*Statics.SQ_SIZE, y*Statics.SQ_SIZE, false);
				this.rootCamMoved(0, 0);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else if (Statics.data.containsKey(CONFIRM_START_TUTORIAL)) {
			try {
				if (Statics.data.get(CONFIRM_START_TUTORIAL).equalsIgnoreCase(ConfirmModule.YES)) {
					this.startTutorial();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else if (Statics.data.containsKey(CONFIRM_REMOVE_ITEM)) {
			try {
				if (Statics.data.get(CONFIRM_REMOVE_ITEM).equalsIgnoreCase(ConfirmModule.YES)) {
					this.removeCurrentItem();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		Statics.data.clear();
	}


	@Override
	public void stopped() {  // Note that this GETS CALLED WHEN SHOWING THE SCANNER!
		//img_cache.clear();

	}


	public void updateMenu() {
		this.updateMenu(icon_table.menu_mode);
	}


	private void updateMenu(byte menu) {
		this.icon_table.updateMenu(menu);

		this.hud.updateStatusBar();

		if (menu == GameModule.MM_NO_UNIT_SELECTED || game_data.game_status == GamesTable.GS_DEPLOYMENT) {
			hud.setMovementIconsVisible(false);
		} else {
			if (this.current_unit != null) {
				hud.setMovementIconsVisible(true);
			}
		}

		// Remove targetting line if applic
		if (icon_table.menu_mode != MM_SHOOTING && icon_table.menu_mode != MM_THROW) { // && icon_table.menu_mode != MM_TELEPORT) {
			if (shot_line != null) {
				shot_line.removeFromParent();
				shot_line = null;
			}
		}
	}


	public static int getAimedShotAccuracy(UnitData unit) {
		float acc = Math.max(MIN_SHOT_ACC, (unit.shot_skill + unit.current_item.aimed_shot_acc));
		return (byte)Math.min(MAX_SHOT_ACC, acc);
	}


	public static int getSnapShotAccuracy(UnitData unit) { 
		float acc =  Math.max(MIN_SHOT_ACC, (unit.shot_skill/2) + unit.current_item.snap_shot_acc);
		return (byte)Math.min(MAX_SHOT_ACC, acc);
	}


	public static int getAutoShotAccuracy(UnitData unit) {
		float acc =  Math.max(MIN_SHOT_ACC, (unit.shot_skill/3) + unit.current_item.auto_shot_acc);
		return (byte)Math.min(MAX_SHOT_ACC, acc);
	}


	@Override
	public boolean componentClicked(Geometry g) {
		AbstractActivity act = Statics.act;

		try { // Have we clicked on a submenu item?
			if (g.parent != null) {
				if (g.parent.parent == submenu) {
					if (g instanceof Button) {
						Button b = (Button) g;
						menuPressed(b);
						return true;
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		if (this.wait_for_data_to_be_sent) {
			this.addToHUD("Please wait...");
			this.showing_please_wait = true;
			return true;
		}

		if (this.client_mode == APP_TURN_FINISHED_COMPLETELY) {
			this.returnTo();
			return true;
		}

		IOFunctions.Vibrate(act.getBaseContext(), Statics.VIBRATE_LEN);

		if (g == menu_icon) {
			this.submenu.toggle();
			return true;
		} else if (g == end_turn_icon) {
			askIfEndTurn();
			return true;
		} else if (g == centre_on_unit) {
			selectOurUnit(current_unit, true);
			return true;
		} else if (g instanceof AbstractIcon) {
			AbstractIcon ai = (AbstractIcon)g;
			boolean ret = ai.mouseClicked();
			if (this.question) {
				this.question = false;
			}
			return ret;
		} else if (g instanceof AbstractUnit) {
			AbstractUnit unit_model = (AbstractUnit)g;
			if (unit_model.unit_data.getSide() == this.game_data.our_side) {
				this.selectOurUnit(unit_model.unit_data, true);
			} else if (game_data.areSidesFriends(unit_model.unit_data.getSide(), this.game_data.our_side)) {
				if (unit_model.visible) {
					this.addToHUD(act.getString(R.string.friendly_unit));
					// Say what they are using
					if (unit_model.unit_data.current_item != null) {
						EquipmentData eq = unit_model.unit_data.current_item;
						this.addToHUD("Using " + eq.getName(true) + " ");
					} else if (unit_model.unit_data.can_use_equipment) {
						this.addToHUD("Using " + act.getString(R.string.nothing));
					}
					return true;
				}
			} else { // Enemy unit
				if (unit_model.visible) {
					showEnemyDetails(unit_model);
					return true;
				}
			}
		} else if (g instanceof MapSquareImage) {
			MapSquareImage mi = (MapSquareImage)g;
			if (this.game_data.game_status == GamesTable.GS_DEPLOYMENT) {
				if (next_to_deploy >= 0) {
					AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(mi.x, mi.y); 
					if (sq.deploy_sq_side > 0) {
						this.deployUnit(sq);
						return true;
					} else {
						this.addToHUD(act.getString(R.string.not_deploy_sq));
						// Notice we DON'T return
					}
				}
			}
			AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(mi.x, mi.y);
			if (sq != null) {
				if (sq.door_type > 0) {
					this.addToHUD(act.getString(R.string.is_door));
				}
				if (sq.smoke_type == EquipmentTypesTable.ET_NERVE_GAS) {
					this.addToHUD(act.getString(R.string.is_nerve_gas));
				}
				if (sq.smoke_type == EquipmentTypesTable.ET_SMOKE_GRENADE) {
					this.addToHUD(act.getString(R.string.is_smoke));
				}
				if (sq.smoke_type == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
					this.addToHUD("That is fire");
				}
				if (sq.escape_hatch_side > 0) {
					this.addToHUD(act.getString(R.string.is_escape_hatch));
				}
				if (sq.major_type == MapDataTable.MT_COMPUTER) {
					this.addToHUD("That is a computer");
				}
				if (sq.major_type == MapDataTable.MT_WALL) {
					this.addToHUD("That is a wall");
				}
				if (sq.major_type == MapDataTable.MT_FLOOR) {
					String side = "";
					if (sq.owner_side > 0) {
						side = " owned by ";
						if (sq.owner_side == game_data.our_side) {
							side = side + "you";
						} else {
							side = side + game_data.GetPlayersNameFromSide(sq.owner_side);
						}
					}
					switch (sq.texture_code) {
					case TextureStateCache.TEX_MEDI_BAY: this.addToHUD("That is a Medi-Bay" + side);  break;
					case TextureStateCache.TEX_GUN_VENDING_MACHINE: this.addToHUD("That is a Gun Vending Machine" + side);  break;
					case TextureStateCache.TEX_GRENADE_VENDING_MACHINE: this.addToHUD("That is a Grenade Vending Machine" + side);  break;
					case TextureStateCache.TEX_CLONE_GENERATOR: this.addToHUD("That is a Clone Generator" + side);  break;
					case TextureStateCache.TEX_POWER_UNIT: this.addToHUD("That is a Power Unit" + side);  break;
					case TextureStateCache.TEX_ALIEN_COLONY: this.addToHUD("That is an alien egg" + side);  break;
					}
				}
				String str_sq = sq.getListOfEquipment(this.game_data.is_advanced, this.game_data.our_side);
				if (str_sq.length() > 0) {
					this.addToHUD(str_sq);
				}
				return false; // Since there may be another click to select the unit!
			}

		}
		return false;
	}


	private void showEnemyDetails(AbstractUnit unit_model) {
		AbstractActivity act = Statics.act;

		this.addToHUD("Side " + unit_model.unit_data.getSide() + " #" + unit_model.unit_data.order_by +", owned by " + game_data.GetPlayersNameFromSide(unit_model.unit_data.getSide()));
		// Say what they are using
		if (unit_model.unit_data.current_item != null) {
			EquipmentData eq = unit_model.unit_data.current_item;
			this.addToHUD("Using " + eq.getName(false) + " ");
		} else if (unit_model.unit_data.can_use_equipment) {
			this.addToHUD("Using " + act.getString(R.string.nothing));
		}
		this.addToHUD("Protection: " + unit_model.unit_data.protection);
		try {
			if (is_april_fools_day) {
				int x = NumberFunctions.rnd(0, 10);
				if (x == 0) {
					if (unit_model.unit_data.model_type == UnitsTable.MT_MALE_SIDE_1 || unit_model.unit_data.model_type == UnitsTable.MT_MALE_SIDE_2 || unit_model.unit_data.model_type == UnitsTable.MT_MALE_SIDE_3 || unit_model.unit_data.model_type == UnitsTable.MT_MALE_SIDE_4) {
						this.addToHUD("He's not scared of you.");
					} else if (unit_model.unit_data.model_type == UnitsTable.MT_ZOMBIE) {
						this.addToHUD("Zombie says 'brainzzz!");
					} else if (unit_model.unit_data.model_type == UnitsTable.MT_ALIEN_TYRANT) {
						this.addToHUD("The alien only wants to be friends.");
					}
				}
			}
		} catch (Exception ex) {
			ErrorReporter.getInstance().handleSilentException(ex);
		}
	}


	private boolean deployUnit(AppletMapSquare sq) {
		AbstractActivity act = Statics.act;

		if (sq.deploy_sq_side > 0) {
			if (this.game_data.areSidesFriends(sq.deploy_sq_side, game_data.our_side)) {
				UnitData unit = units[next_to_deploy];
				// Check there isn't already a unit there!
				UnitData other = this.getUnitAt(sq.x, sq.y);
				if (other == null) {
					act.playSound(R.raw.teleport);
					// Deploy unit!
					unit.model = AbstractUnit.Factory(this, unit);
					root_node.attachChild(unit.model);
					unit.setTargetMapLocation(this, sq.x, sq.y, sq);
					unit.setAngle(this.getNextDeployDir());
					unit.setStatus(UnitsTable.ST_DEPLOYED);
					this.updateUnitOnServer(unit);
					this.addToHUD(act.getString(R.string.unit_deployed));
					units_deployed.add(next_to_deploy);

					TeleportEffect fx = new TeleportEffect(unit.model);
					this.root_node.attachChild(fx);
					fx.updateGeometricState();

					this.getNextUnitToDeploy();
					return true;
				} else {
					this.addToHUD(act.getString(R.string.already_a_unit));
				}
			} else {
				this.addToHUD(act.getString(R.string.not_your_deploy_sq));
			}
		} else {
			this.addToHUD(act.getString(R.string.not_deploy_sq));
		}
		return false;
	}


	@Override
	public void updateGame(long interpol) {
		AbstractActivity act = Statics.act;

		this.submenu.process();

		if (ambience_interval.hitInterval()) {
			if (NumberFunctions.rnd(1, 3) == 1) {
				playAmbience();
			}
		}

		if (is_april_fools_day || Statics.DEBUG_APRIL_FOOLS) {
			if (NumberFunctions.rnd(1, 2000) == 1) {
				new JokeSpider(this);
			}
		}

		if (this.shot_line != null) { // Don't bother unless we're showing the shot line
			de_offset++;
			if (de_offset >= de_arr.length) {
				de_offset = 0;
			}
			paint_shot_line_unblocked.setPathEffect(new DashPathEffect(de_arr, de_offset));
			paint_shot_line_blocked.setPathEffect(new DashPathEffect(de_arr, de_offset));
		}

		if (wait_for_data_to_be_sent) {
			if (this.update_server_thread.isSendingData()) {
				return;
			} else {
				wait_for_data_to_be_sent = false;
			}
		} else {
			if (this.showing_please_wait) {
				if (client_mode != APP_TURN_ENDED_MOVE_AI) {
					this.addToHUD(act.getString(R.string.ready));
				}
				showing_please_wait = false;
			}
		}

		objects_for_processing.refresh();

		this.time_since_last_explosion += interpol;

		// If there's any queued explosions and we're not showing one, show it.
		if (this.areExplosionsScheduled()) {
			if (time_since_last_explosion >= TIME_BETWEEN_EXPLOSIONS) {
				ExplosionPendingData epd = this.getNextExplosion();
				if (moving_cam_to_explosion) {
					Point p = epd.eq.getMapLocation(units, mapdata);
					int mapx = p.x;
					int mapy = p.y;
					this.root_cam.lookAt(mapx * Statics.SQ_SIZE, mapy * Statics.SQ_SIZE, false);
					moving_cam_to_explosion = false;
				} else {
					//p("Showing explosion (" + this.getNumScheduledExplosions() + " remaining)...");
					if (root_cam.isMoving() == false) {
						this.removeNextExplosion();
						explodeGrenade(epd.eq, epd.caused_by);
						time_since_last_explosion = 0;
						moving_cam_to_explosion = true;
					}
				}
			}
		} else if (client_mode == APP_TURN_ENDED_SHOWING_EXPLOSIONS) {
			if (update_server_thread.isSendingData() == false) {
				this.recalcVisibleEnemiesAndOppFire(false, null); // See any units that might have become visible due to explosions and walls going down.
				//this.updateGameDataOnServer(GameDataComms.TURN_ENDED);
				this.client_mode = APP_TURN_ENDED_MOVE_AI;
				this.waitForPendingUpdateRequests();
			}
		} else if (client_mode == APP_TURN_ENDED_MOVE_AI) {
			no_ai = true;
			for (int i=0 ; i<this.units.length ; i++) {
				if (units[i].getSide() != this.game_data.our_side) {
					if (units[i].hasAI()) {
						if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
							if (mentioned_ai == false) {
								this.addToHUD("Moving AI Units...");
								mentioned_ai = true;
							}
							if (units[i].processAI(this)) { // units[i].name
								no_ai = false;
								break;
							}
						}	
					}
				}
			}
			if (no_ai) {
				/*if (mentioned_ai == false) {
					this.addToHUD("Finished moving AI units.");
					mentioned_ai = true;
				}*/
				this.updateGameDataOnServer(GameDataComms.TURN_ENDED);
				this.client_mode = APP_TURN_ENDED_WAIT_FOR_COMMS;
			}
		} else if (client_mode == APP_TURN_ENDED_WAIT_FOR_COMMS) {
			if (update_server_thread.isSendingData() == false) {
				client_mode = APP_TURN_FINISHED_COMPLETELY;
				act.playSound(R.raw.end_turn);
				if (game_data.game_status == GamesTable.GS_DEPLOYMENT) {
					this.addToHUD(act.getString(R.string.all_deployed));
					if (game_data.getOpponentsName().equalsIgnoreCase("no-one") && game_data.num_players > 1) {
						this.addToHUD("You will need to wait for another player to accept your game and deploy before you can take a turn.");
					}
					this.addToHUD(act.getString(R.string.press_any_key));
				} else {
					this.hud.appendText(act.getString(R.string.press_any_key));
					if (game_finished == false) {
						if (no_ai) {
							this.hud.appendText("Your opponent has been informed that it is now their turn.");
						}
					}
				}
			}
		}

		for (int i=0 ; i<objects_for_processing.size() ; i++) {
			IProcessable obj = objects_for_processing.get(i);
			obj.process(interpol);
		}

		if (this.update_server_thread.isAlive() == false) {
			if (this.client_mode != GameModule.APP_CRASHED) {
				this.addToHUD("PLEASE RESTART CLIENT");
				this.client_mode = GameModule.APP_CRASHED;
			}
		}

	}


	private void waitForPendingUpdateRequests() {
		AbstractActivity act = Statics.act;

		wait_for_data_to_be_sent = true;
		if (update_server_thread.isSendingData()) {
			this.addToHUD(act.getString(R.string.please_wait));
			showing_please_wait = true;
		}
	}


	@Override
	public void doDraw(Canvas g, long interpol) {
		super.doDraw(g, interpol);

		if (this.shot_line != null) {
			updateShotLine(false);
		}
		if (icon_table.menu_mode == MM_THROW) {
			float range = GetMaxRangeSq(this) * Statics.SQ_SIZE;
			g.drawCircle(this.current_unit.model.getWorldCentreX()-this.root_cam.left, this.current_unit.model.getWorldCentreY()-this.root_cam.top, range, paint_shot_line_blocked);
		} else if (icon_table.menu_mode == MM_SHOOTING) {
			if (this.current_unit.current_item != null) {
				float range = this.current_unit.current_item.range_sq * Statics.SQ_SIZE;
				if (range > 0) {
					g.drawCircle(this.current_unit.model.getWorldCentreX()-this.root_cam.left, this.current_unit.model.getWorldCentreY()-this.root_cam.top, range, paint_shot_line_blocked);
				}
			}
		}
		/*if (icon_table.menu_mode == MM_TELEPORT) {
			float range = GetMaxTeleportRangeSq() * Statics.SQ_SIZE;
			g.drawCircle(this.current_unit.model.getWorldCentreX()-this.root_cam.left, this.current_unit.model.getWorldCentreY()-this.root_cam.top, range, paint_shot_line);

		}*/
		if (update_server_thread.isSendingData()) {
			g.drawRect(signal_rect, paint_yellow_box);
		}

	}


	public void addToProcess(IProcessable o) {
		this.objects_for_processing.add(o);
	}


	public void removeFromProcess(IProcessable o) {
		this.objects_for_processing.remove(o);
	}


	@Override
	public boolean onBackPressed() {
		AbstractActivity act = Statics.act;

		IOFunctions.Vibrate(act.getBaseContext(), Statics.VIBRATE_LEN);
		if (this.submenu != null) {
			if (this.submenu.getStatus() == InGameMenu.STATUS_SHOWN) {
				this.submenu.hide();
				return true;
			}
		}
		if (update_server_thread.isSendingData() && this.client_mode != APP_CRASHED) {
			this.hud.appendText("Please wait - sending data...");
			return true;
		}
		if (this.hud.getMenuMode() > MM_NO_UNIT_SELECTED) {
			this.cancelMenu();
		} else {
			if (this.client_mode == APP_TURN_FINISHED_COMPLETELY) {
				this.returnTo();
			} else {
				if (game_data.game_status == GamesTable.GS_DEPLOYMENT) {
					this.returnTo();
				} else {
					askIfEndTurn();
				}
			}
		}
		return true;
	}


	public void askIfEndTurn() {
		AbstractActivity act = Statics.act;

		AbstractModule m = new ConfirmModule(act, this, "End Your Turn?", "Do you want to finish your turn?", Statics.BACKGROUND_R, CONFIRM_END_TURN);
		this.getThread().setNextModule(m);

	}


	public void updateEquipmentOnServer(EquipmentData eq, int x, int y) {
		/*if (AppletMain.VERBOSE_COMMS) {
			p("Updating equipment on server.");
		}*/
		String req = EquipmentDataComms.GetEquipmentUpdateRequest(eq, x, y, game_data.game_id);
		update_server_thread.addRequest(req);
	}


	public void addToHUD(String s) {
		if (hud != null) {
			hud.appendText(s);
		}
	}


	public void addToHUD(String s, boolean pri) {
		hud.appendText(s);
	}


	public void updateUnitOnServer(UnitData unit) {
		/*if (VERBOSE_COMMS) {
			p("Updating unit " + unit.name + " data on server.");
		}*/
		String req = UnitDataComms.GetUnitUpdateRequest(unit);
		update_server_thread.addRequest(req);
	}


	public void updateVisibleUnitOnServer(UnitData enemy, UnitData seen_by) { 
		/*if (VERBOSE_COMMS) {
			p("Sending visible enemy " + enemy.name + " seen by " + seen_by + " data to server.");
		}*/
		String req = VisibleEnemyComms.GetVisibleEnemyRequest(enemy, seen_by);
		update_server_thread.addRequest(req);
	}


	public void sendEventToServer_UnitMoved(UnitData unit, boolean seen_by_sides[]) {
		/*if (VERBOSE_COMMS) {
			p("Sending unit " + unit.name + " moved event to server.");
		}*/
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_UNIT_MOVEMENT, unit.unitid, unit.map_x, unit.map_y, 0, 0, seen_by_sides, 0, 0);
		update_server_thread.addRequest(req);

	}


	public void sendEventToServer_Explosion(int x, int z, int rad, byte type) {
		/*if (VERBOSE_COMMS) {
			p("Sending explosion event to server.");
		}*/
		boolean seen_by_sides[] = {true, true, true, true}; // All sides get informed
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_EXPLOSION, -1, x, z, rad, type, seen_by_sides, 0, 0);
		update_server_thread.addRequest(req);
	}


	public void sendEventToServer_MapSquareChanged(int x, int z, int old_major_type, int new_major_type, int tex, int side) {
		/*if (VERBOSE_COMMS) {
			p("Sending explosion event to server.");
		}*/
		boolean seen_by_sides[] = {true, true, true, true}; // All sides get informed
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_MAPSQUARE_CHANGED, -1, x, z, old_major_type, new_major_type, seen_by_sides, tex, side);
		update_server_thread.addRequest(req);
	}


	public void sendEventToServer_ShotFired(UnitData unit, int ang, int len, boolean seen_by_sides[]) {
		/*if (VERBOSE_COMMS) {
			p("Sending shot_fired event to server.");
		}*/
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_SHOT_FIRED, unit.unitid, unit.getMapX(), unit.getMapY(), ang, len, seen_by_sides, 0, 0);
		update_server_thread.addRequest(req);
	}


	public void sendEventToServer_GrenadePrimed(UnitData unit, int turns, boolean seen_by_sides[]) {
		/*if (VERBOSE_COMMS) {
			p("sendEventToServer_GrenadePrimed");
		}*/
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_GRENADE_PRIMED, unit.unitid, unit.getMapX(), unit.getMapY(), turns, -1, seen_by_sides, 0, 0);
		update_server_thread.addRequest(req);
	}


	private void sendEventToServer_CloseCombat(UnitData shooter, boolean seen_by_side[]) {
		/*if (VERBOSE_COMMS) {
			p("Sending close_combat event to server.");
		}*/
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_CLOSE_COMBAT, shooter.unitid, shooter.getMapX(), shooter.getMapY(), 0, 0, seen_by_side, 0, 0);
		update_server_thread.addRequest(req);
	}


	public void sendEventToServer_ItemThrown(UnitData shooter, int ang, int dist, boolean seen_by_side[]) {
		/*if (VERBOSE_COMMS) {
			p("Sending item_thrown event to server.");
		}*/
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_ITEM_THROWN, shooter.unitid, shooter.getMapX(), shooter.getMapY(), ang, dist, seen_by_side, 0, 0);
		update_server_thread.addRequest(req);
	}


	public void sendEventToServer_UnitTeleported(UnitData shooter, boolean seen_by_side[]) {
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_UNIT_TELEPORTED, shooter.unitid, shooter.getMapX(), shooter.getMapY(), 0, 0, seen_by_side, 0, 0);
		update_server_thread.addRequest(req);
	}


	public void sendEventToServer_ItemDropped(UnitData shooter, boolean seen_by_side[]) {
		/*if (VERBOSE_COMMS) {
			p("Sending item_dropped event to server.");
		}*/
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_ITEM_DROPPED, shooter.unitid, shooter.getMapX(), shooter.getMapY(), 0, 0, seen_by_side, 0, 0);
		update_server_thread.addRequest(req);
	}


	public void sendEventToServer_ItemPickedUp(UnitData unit, boolean seen_by_side[]) {
		/*if (VERBOSE_COMMS) {
			p("Sending item_picked_up event to server.");
		}*/
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_ITEM_PICKED_UP, unit.unitid, unit.getMapX(), unit.getMapY(), 0, 0, seen_by_side, 0, 0);
		update_server_thread.addRequest(req);
	}


	public void sendEventToServer_UnitKilled(UnitData unit_killed, int attacked_by_side, int attacked_by_unitid, int form_of_attack, int amt, boolean seen_by_side[]) {
		/*if (VERBOSE_COMMS) {
			p("Sending unit_killed event to server.");
		}*/
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_UNIT_KILLED, unit_killed.unitid, unit_killed.getMapX(), unit_killed.getMapY(), attacked_by_side, attacked_by_unitid, seen_by_side, form_of_attack, amt);
		update_server_thread.addRequest(req);
	}


	private void sendEventToServer_BlobSplit(UnitData unit, boolean seen_by_side[], int x, int z) {
		if (VERBOSE_COMMS) {
			p("Sending blob split event to server.");
		}
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_BLOB_SPLIT, unit.unitid, unit.getMapX(), unit.getMapY(), x, z, seen_by_side, 0, 0);
		update_server_thread.addRequest(req);
	}


	public void sendEventToServer_UnitEscaped(UnitData unit_killed, boolean seen_by_side[]) {
		if (VERBOSE_COMMS) {
			p("Sending unit_escaped event to server.");
		}
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_UNIT_ESCAPED, unit_killed.unitid, unit_killed.getMapX(), unit_killed.getMapY(), 0, 0, seen_by_side, 0, 0);
		update_server_thread.addRequest(req);
	}


	public void sendEventToServer_UnitWounded(UnitData unit_killed, int attacked_by_side, int attacked_by_unitid, int form_of_attack, int amt, boolean seen_by_side[]) {
		if (VERBOSE_COMMS) {
			p("Sending unit_wounded event to server.");
		}
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_UNIT_WOUNDED, unit_killed.unitid, unit_killed.getMapX(), unit_killed.getMapY(), attacked_by_side, attacked_by_unitid, seen_by_side, form_of_attack, amt);
		update_server_thread.addRequest(req);
	}


	public void setResPointsForSideOnServer(int side, int amt) {
		String req = SetStatComms.GetSetStatRequest(game_data.game_id, game_data.gamecode, side, SetStatComms.STAT_RES_POINTS, amt);
		update_server_thread.addRequest(req);
	}


	public void updateGameDataOnServer(int type) {
		if (VERBOSE_COMMS) {
			p("Updating game data on server.");
		}
		String req = GameDataComms.GetGameUpdateRequest(game_data.game_id, game_data.gamecode, game_data.our_side, type);
		update_server_thread.addRequest(req);
	}


	public void updateMapOnServer(AppletMapSquare sq, int destroyed_by_side) {
		if (VERBOSE_COMMS) {
			p("Updating map square on server.");
		}

		String req = MapDataComms.GetMapDataUpdateRequest(this.game_data.game_id, this.game_data.gamecode, sq, destroyed_by_side);
		update_server_thread.addRequest(req);
	}


	public static void p(Object o) {
		AbstractActivity.Log(o.toString());
	}


	public void sideHasWon(byte side) {
		AbstractActivity act = Statics.act;

		this.addToHUD("**********************");
		if (game_data.areSidesFriends(side, this.game_data.our_side)) {
			this.addToHUD(act.getString(R.string.you_have_won));
			act.playSound(R.raw.fanfare);
			this.showToast(act.getString(R.string.you_have_won));
		} else {
			this.addToHUD(act.getString(R.string.you_have_lost));
			//this.playSound(SoundEffects.LOST);
			this.showToast(act.getString(R.string.you_have_lost));
		}
		this.addToHUD("**********************");
		game_finished = true;
	}


	public void gameIsDraw() {
		AbstractActivity act = Statics.act;

		this.addToHUD("**********************");
		this.addToHUD(act.getString(R.string.game_drawn));
		this.showToast(act.getString(R.string.game_drawn));
		this.addToHUD("**********************");
		game_finished = true;
	}


	public void checkForDeathGrenadesOrAlienExplode(UnitData caused_by_unit) {
		if (caused_by_unit.model_type == UnitsTable.MT_ALIEN_TYRANT) {
			int ACID_BLOOD_DAMAGE = 40;
			this.normalExplosion(caused_by_unit.map_x, caused_by_unit.map_y, NumberFunctions.rndByte(1, 3), ACID_BLOOD_DAMAGE, caused_by_unit, false, true);
		} else if (caused_by_unit.model_type == UnitsTable.MT_QUEEN_ALIEN) {
			int ACID_BLOOD_DAMAGE = 80;
			this.normalExplosion(caused_by_unit.map_x, caused_by_unit.map_y, NumberFunctions.rndByte(2, 4), ACID_BLOOD_DAMAGE, caused_by_unit, false, true);
		} else if (caused_by_unit.model_type == UnitsTable.MT_BLOB) {
			// NO as it harms other blobs blobDoExplode(caused_by_unit);
		} else {
			for (int i=0 ; i<caused_by_unit.items.size() ; i++) {
				EquipmentData eq = caused_by_unit.items.get(i);
				if (eq.major_type == EquipmentTypesTable.ET_DEATH_GRENADE) {
					if (eq.primed) {
						eq.primed = false;
						this.updateEquipmentOnServer(eq, (byte)-1, (byte)-1);
						this.scheduleExplosion(eq, caused_by_unit);
					}
				}
			}
		}
	}


	public UnitData getCurrentUnit() {
		return this.current_unit;
	}


	public boolean[] whichSidesCanSeeUnit(UnitData shooter) {
		boolean side_can_see_shooter[] = new boolean[game_data.num_players+1];
		boolean sides_checked[] = new boolean[game_data.num_players+1];
		game_data.side_sides[shooter.getSide()].setArray(sides_checked, true);
		side_can_see_shooter[shooter.getSide()] = true;

		for (int i=0 ; i<this.units.length ; i++) {
			// Have we checked this side yet?
			if (sides_checked[units[i].getSide()] == false) {
				if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
					if (units[i].canUnitSee(shooter)) {
						sides_checked[units[i].getSide()] = true;
						side_can_see_shooter[units[i].getSide()] = true;
					}
				}
			}
		}
		return side_can_see_shooter;
	}


	public boolean canAnyOtherSideSeeUnit(UnitData unit) {
		for (int i=0 ; i<this.units.length ; i++) {
			if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
				if (units[i].getSide() != unit.getSide()) {
					if (units[i].canUnitSee(unit)) {
						return true;
					}
				}
			}
		}
		return false;
	}


	public void unitKilled(UnitData unit) {
		icon_table.our_unit_icons.remove(unit.icon);
		deselectUnitIfCurrent(unit);

		// Drop equipment
		EquipmentData equip;
		for (int i=0 ; i<equipment.length ; i++) {
			equip = equipment[i];
			if (equip.getUnitID() == unit.unitid) {
				unit.removeItem(this, equip, false);
				equip.seen_by_side[unit.getSide()] = 1;
				this.mapdata.getSq_MaybeNULL(unit.getMapX(), unit.getMapY()).addEquipment(equip);
				this.updateEquipmentOnServer(equip, unit.getMapX(), unit.getMapY());
				equip.model = EquipmentModel.Factory(this, equip, unit.getMapX(), unit.getMapY());
				this.attachToRootNode(equip.model, true);
			}
		}
	}


	public void attachToRootNode(Spatial s, boolean update) {
		attachToRootNode(1, s, update); // Add just in front of map
	}


	public void attachToRootNode_Top(Spatial s, boolean update) {
		this.attachToRootNode(this.root_node.getNumChildren(), s, update);
	}


	public void attachToRootNode(int pos, Spatial s, boolean update) {
		this.root_node.attachChild(pos, s);
		if (update) {
			this.root_node.updateGeometricState();
		}
	}


	public void unitEscaped(UnitData unit) {
		AbstractActivity act = Statics.act;

		// playSound(unit, SpeechPlayer.EV_UNIT_ESCAPED);
		icon_table.our_unit_icons.remove(unit.icon);
		this.deselectUnitIfCurrent(unit);
		this.addToHUD(unit.name + " " + act.getString(R.string.has_escaped));
	}


	public void recalcVisibleEnemiesAndOppFire(boolean opp_fire, UnitData unit_that_moved) {
		AbstractActivity.Log("recalcVisibleEnemiesAndOppFire()");

		// Store a list of who we can already see
		orig_visible_enemy_icons.clear();
		orig_visible_enemy_icons.addAll(icon_table.visible_enemy_icons);

		boolean can_see_all_enemies = game_data.side_see_enemies[game_data.our_side] == 1;
		icon_table.visible_enemy_icons.clear();

		// Check if there is any visible equipment
		if (game_data.is_advanced == 1) { // Why not see corpses?
			for (int j_friend=0 ; j_friend<units.length ; j_friend++) { // Loop through our units
				if (this.game_data.areSidesFriends(units[j_friend].getSide(), game_data.our_side)) {
					if (units[j_friend].getStatus() == UnitsTable.ST_DEPLOYED) {
						for (int e=0 ; e<equipment.length ; e++) { // Loop through equipment
							if (equipment[e].getUnitID() <= 0 && equipment[e].destroyed == false) {
								if (equipment[e].seen_by_side[game_data.our_side] == 0) {
									if (canUnitSeeEquipment(units[j_friend], equipment[e])) {
										equipment[e].model.visible = true;
										equipment[e].seen_by_side[game_data.our_side] = 1;
										PointByte p = EquipmentData.GetEquipmentMapSquare(this.mapdata, equipment[e]);
										if (p != null) {
											this.updateEquipmentOnServer(equipment[e], p.x, p.y);
										}
									}
								}
							}
						}
					}
				}
			}
		}


		// Clear all "can_see" vars
		for (int i=0 ; i<this.units.length ; i++) {
			units[i].clearSeenUnits();
		}

		// Loop through enemy units
		for (int i_enemy=0 ; i_enemy<this.units.length ; i_enemy++) {
			if (this.game_data.areSidesFriends(units[i_enemy].getSide(), game_data.our_side) == false) {
				boolean can_enemy_see_all_us = game_data.side_see_enemies[units[i_enemy].getSide()] == 1;
				boolean has_enemy_fired = false;
				if (units[i_enemy].getStatus() == UnitsTable.ST_DEPLOYED) {
					if (units[i_enemy].has_been_seen == false && can_see_all_enemies == false) { // Only hide enemies we haven't already seen
						units[i_enemy].model.visible = false;
					} else if (can_see_all_enemies) {
						units[i_enemy].model.visible = true;
					}

					// Loop through our units
					for (int j_friend=0 ; j_friend<units.length ; j_friend++) {
						/*if (units[i_enemy].name.equalsIgnoreCase("Master Sergeant Kidman") && units[j_friend].name.equalsIgnoreCase("Master Sergeant Kidman")) {
							System.out.println("REMOVE");
						}*/
						if (units[j_friend].getSide() == game_data.our_side) {
							if (units[j_friend].getStatus() == UnitsTable.ST_DEPLOYED) {
								// Can OUR unit see the enemy unit?
								if (canUnitSeeUnit(units[j_friend], units[i_enemy], true) || can_see_all_enemies) {
									markUnitAsSeen(units[i_enemy], units[j_friend]);
								}
								// Can our unit BE SEEN BY the enemy unit?
								if (canUnitSeeUnit(units[i_enemy], units[j_friend], true) || can_enemy_see_all_us) {
									this.markUnitAsSeen(units[j_friend], units[i_enemy]);
									if (check_our_init_visible_units) {
										units[i_enemy].can_see_at_start.add(units[j_friend]);
										//Keep going so we mark all our units as seen who are seen
									}
									if (has_enemy_fired == false) {
										if (opp_fire && this.client_mode < APP_TURN_ENDED_SHOWING_EXPLOSIONS) { // Don't do opp fire if turn ending!
											if (units[j_friend] == unit_that_moved || unit_that_moved == null) {
												if (units[i_enemy].opp_fire_01 != 0) { // Are they on opp fire?
													if (game_data.is_snafu == 0 || game_data.snafu_will_opp_fire_on_side[units[i_enemy].getSide()] == 1) {
														if (units[i_enemy].current_item != null && units[i_enemy].current_item.major_type == EquipmentTypesTable.ET_GUN && units[i_enemy].current_item.auto_shot_acc > 0) { // Can it opp fire?
															if (isUnitAdjToFriendlyUnitHeCanSee(units[j_friend]) == false) {
																// Check they weren't initially visible
																if (units[i_enemy].can_see_at_start.contains(units[j_friend]) == false) {
																	// Check range of gun
																	float dist = units[i_enemy].model.getMapDistTo(units[j_friend].model);
																	if (units[i_enemy].current_item.range_sq <= 0 || dist <= units[i_enemy].current_item.range_sq) {
																		this.makeOppFireShot(units[i_enemy], units[j_friend]);
																		has_enemy_fired = true; // Stop them shooting any more, but still check for visible units
																	}
																}
															}
														} else if (units[i_enemy].current_item == null || units[i_enemy].current_item.major_type == EquipmentTypesTable.ET_CC_WEAPON) {
															if (units[i_enemy].model.getMapDistTo(units[j_friend].model) < 1.7f) { // Was 2f
																// Check they weren't initially visible
																if (units[i_enemy].can_see_at_start.contains(units[j_friend]) == false) {
																	if (units[i_enemy].checkAndReduceAPs(this, OPP_CC_COST)) {
																		// Close combat!
																		this.closeCombat(units[i_enemy], units[j_friend]);
																	}
																}
															}
														}
													}

												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		check_our_init_visible_units = false;
		updateEnemyIcons();
	}


	@Override
	public void displayMessage(String s) {
		this.hud.appendText(s);
	}


	public boolean isAnyFriendlyUnitHoldingPrimedGrenade() {
		for (int i=0 ; i<this.units.length ; i++) {
			if (units[i].getSide() == this.game_data.our_side) {
				if (units[i].current_item != null) {
					EquipmentData eq = units[i].current_item;
					if (eq.major_type == EquipmentTypesTable.ET_GRENADE || eq.major_type == EquipmentTypesTable.ET_SMOKE_GRENADE || eq.major_type == EquipmentTypesTable.ET_NERVE_GAS) {
						if (eq.primed) {
							if (eq.explode_turns < this.game_data.num_players) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}


	private void getNextUnitToDeploy() {
		AbstractActivity act = Statics.act;

		// Find the next unit to deploy
		next_to_deploy = -1;

		for (int i=0 ; i<this.units.length ; i++) {
			if (units[i].getSide() == game_data.our_side) {
				if (units[i].getStatus() == UnitsTable.ST_AWAITING_DEPLOYMENT) {
					next_to_deploy = i;
					break;
				}
			}
		}

		this.hud.clearText();
		if (next_to_deploy == -1) {
			// All have been deployed!
			this.finishedDeployment();
		} else {
			this.addToHUD(act.getString(R.string.unit) + " " + units[next_to_deploy].order_by + "/" + game_data.getUnitsRemaining() + ":");
			this.addToHUD(act.getString(R.string.select_deploy_sq) + " " + units[next_to_deploy].name);
			if (units[next_to_deploy].can_use_equipment) {
				this.addToHUD(act.getString(R.string.unit_protection) + ": " + units[next_to_deploy].protection);
				this.addToHUD(act.getString(R.string.unit_is_carrying) + ":-");
				this.addToHUD(GetUnitsEquipmentAsString(units[next_to_deploy], this.equipment).toString());
			}
		}
		this.hud.updateStatusBar();
		this.updateMenu();
	}


	public void finishedDeployment() {
		updateGameDataOnServer(GameDataComms.DEPLOYED);
		waitForPendingUpdateRequests();
		client_mode = APP_TURN_ENDED_WAIT_FOR_COMMS;
	}


	public static String GetUnitsEquipmentAsString(UnitData unit, EquipmentData[] equipment) {
		StringBuffer str = new StringBuffer();
		boolean any = false;
		for (int i=0 ; i<equipment.length ; i++) {
			EquipmentData equip = equipment[i];
			if (equip.getUnitID() == unit.unitid && equip.destroyed == false) {
				str.append(equip.getName(true));
				if (equip == unit.current_item) {
					str.append("*");
				}
				str.append(", ");
				any = true;
			}
		}
		if (!any) {
			str.append("Nothing.");
		} else {
			str.delete(str.length()-2, str.length());
		}
		return str.toString();
	}


	public void selectOurUnit(UnitData unit, boolean force) {
		AbstractActivity act = Statics.act;

		if (this.current_unit == unit && force == false) {
			return; // Do nothing
		}
		if (unit == null) {
			return;
		}
		if (this.game_data.turn_side == this.game_data.our_side) { // Just in case it's not!
			if (unit.getSide() == this.game_data.our_side) {
				if (unit.hasAI() == false) {
					if (unit.getStatus() == UnitsTable.ST_DEPLOYED) {
						this.root_cam.lookAt(unit.model, false);
						current_unit = unit;
						this.hud.clearText();
						this.addToHUD(unit.name + " (" + unit.order_by + ") " + act.getString(R.string.selected));
						if (unit.can_use_equipment) {
							this.addToHUD(act.getString(R.string.unit_is_carrying) + ":-");
							this.addToHUD(GetUnitsEquipmentAsString(unit, equipment));
							if (help_mode_on) {
								if (this.current_unit.current_item == null) {
									this.addToHUD(act.getString(R.string.help) + ": " + act.getString(R.string.not_using_anything));
									this.addToHUD(act.getString(R.string.help) + ": " + act.getString(R.string.select_equip));
								}
							}
							if (this.current_unit.current_item != null) {
								if (current_unit.current_item.major_type == EquipmentTypesTable.ET_GRENADE || current_unit.current_item.major_type == EquipmentTypesTable.ET_NERVE_GAS) {
									if (current_unit.current_item.primed) {
										this.addToHUD(act.getString(R.string.warning) + ": " + act.getString(R.string.holding_primed_grenade), true);
									}
								} else if (current_unit.current_item.major_type == EquipmentTypesTable.ET_GUN) {
									if (current_unit.current_item.getAmmo() <= 0) {
										this.addToHUD(act.getString(R.string.warning) + ": " + act.getString(R.string.out_of_ammo));
									}
									if (unit.aps >= unit.opp_fire_aps_req) {
										if (isUnitAdjToFriendlyUnitHeCanSee(unit)) {
											this.addToHUD(act.getString(R.string.warning) + ": " + act.getString(R.string.will_not_opp_fire), true);
										}
									}
								}
							} else {
								this.addToHUD(act.getString(R.string.warning) + ": " + act.getString(R.string.not_using_anything), true);
							}
							if (current_unit.on_fire > 0) {
								this.addToHUD("UNIT IS ON FIRE!", true);
								act.playSound(R.raw.fire);
							}
						}
						if (is_april_fools_day) {
							int x = NumberFunctions.rnd(0, 10);
							if (x == 0) {
								if (current_unit.model_type == UnitsTable.MT_MALE_SIDE_1 || current_unit.model_type == UnitsTable.MT_MALE_SIDE_2 || current_unit.model_type == UnitsTable.MT_MALE_SIDE_3 || current_unit.model_type == UnitsTable.MT_MALE_SIDE_4) {
									this.addToHUD("Unit has wet himself.");
								} else if (current_unit.model_type == UnitsTable.MT_ZOMBIE) {
									this.addToHUD("Unit has become vegetarian.");
								} else if (current_unit.model_type == UnitsTable.MT_ALIEN_TYRANT) {
									//this.addToHUD("He only wants to be friends.");
								}
							}
						}

						// Warnings
						if (this.game_data.is_advanced == 1) {
							if (this.current_unit.panicked != 0) {
								this.addToHUD(act.getString(R.string.unit_panicked));
							} else if (this.current_unit.curr_morale <= UnitsTable.SCARED_LEVEL) {
								this.addToHUD(act.getString(R.string.warning) + ": " + act.getString(R.string.unit_scared), true);
							}
							if (this.current_unit.curr_energy <= 0) {
								this.addToHUD(act.getString(R.string.unit_exhausted));
							} else if (this.current_unit.curr_energy < this.current_unit.max_aps) {
								this.addToHUD(act.getString(R.string.warning) + ": " + act.getString(R.string.unit_tired), true);
							}
						}

						this.hud.updateStatusBar();

						this.updateMenu(MM_UNIT_SELECTED);

						updateEnemyIcons();

						if (Statics.USE_NEW_MOVEMENT_ICONS == 1) {
							this.hud.setMovementIconsVisible(true);
							this.hud.new_movement.updateMovementIcons(unit);
						}
					}
				}
			}
		}
	}


	/**
	 * This sets enemy icons to be red/purple depending if the current unit can see the enemy
	 */
	private void updateEnemyIcons() {
		if (this.current_unit != null) {
			for (AbstractIcon icon : icon_table.visible_enemy_icons) {
				ShowEnemyIcon ic = (ShowEnemyIcon) icon;

				if (this.current_unit.canUnitSee(ic.getUnit())) {
					ic.setFlashing(true);
				} else {
					ic.setFlashing(false);
				}
			}
		}
	}


	public void showUnitStats(UnitData unit) {
		if (unit.getSide() == this.game_data.our_side) {
			super.getThread().setNextModule(new SingleUnitStatsModule(this, unit, equipment));
		}
	}


	public void activateEquipment() {
		AbstractActivity act = Statics.act;

		EquipmentData eq = this.current_unit.current_item; 
		if (eq != null) {
			if (eq.major_type == EquipmentTypesTable.ET_DEATH_GRENADE) {
				if (eq.primed == false) {
					if (this.current_unit.checkAndReduceAPs(this, APS_ACTIVATE)) {
						if (this.current_unit != null) { // In case killed by nerve gas
							this.updateUnitOnServer(this.current_unit);
							this.addToHUD(act.getString(R.string.item_activated));
							eq.primed = true;
							eq.last_unit_to_touch = this.current_unit.unitid; 
							this.updateEquipmentOnServer(eq, (byte)-1, (byte)-1);
							hud.updateStatusBar();
							icon_table.updateMenu(MM_UNIT_SELECTED);
						}
					}
				} else {
					this.addToHUD(act.getString(R.string.already_activated));
				}
			} else {
				this.addToHUD(act.getString(R.string.cannot_be_activated));
			}
		}
	}


	public void cancelMenu() {
		if (icon_table.menu_mode == MM_UNIT_SELECTED) {// || icon_table.menu_mode == MM_CONFIRM_END_TURN) {
			this.current_unit = null;
			this.updateMenu(MM_NO_UNIT_SELECTED);
			this.removeHighlighter();
		} else {
			this.updateMenu(MM_UNIT_SELECTED);
		}
	}


	private void removeHighlighter() {
		if (highlighter != null) {
			highlighter.removeFromParent();
			highlighter = null;
		}
	}


	public void dropCurrentItem() {
		AbstractActivity act = Statics.act;

		// If it's an egg, check there is not already an egg
		EquipmentData eq = this.current_unit.current_item;
		if (eq.major_type == EquipmentTypesTable.ET_EGG) {
			// Check there is enough room
			AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(this.current_unit.map_x, this.current_unit.map_y);
			if (sq.containsType(EquipmentTypesTable.ET_EGG)) {
				this.addToHUD(act.getString(R.string.no_room));
				return;
			}
		}
		if (this.current_unit.checkAndReduceAPs(this, APS_DROP_ITEM)) {
			if (this.current_unit != null) { // In case killed by nerve gas
				this.current_unit.removeCurrentItem(this);

				this.mapdata.getSq_MaybeNULL(current_unit.getMapX(), current_unit.getMapY()).addEquipment(eq);

				eq.model = EquipmentModel.Factory(this, eq, current_unit.getMapX(), current_unit.getMapY());
				this.attachToRootNode(eq.model, true);

				// Send message
				boolean who_can_see[] = this.whichSidesCanSeeUnit(this.current_unit);
				// Send item dropped event to server
				this.sendEventToServer_ItemDropped(this.current_unit, who_can_see);

				eq.seen_by_side[current_unit.getSide()] = 1;
				this.updateEquipmentOnServer(eq, current_unit.getMapX(), current_unit.getMapY());
				this.updateUnitOnServer(this.current_unit);
				this.updateMenu(MM_UNIT_SELECTED);

				this.hud.appendText("Item dropped.");
			}
		}
	}

	public void askIfRemoveItem() {
		AbstractActivity act = Statics.act;

		AbstractModule m = new ConfirmModule(act, this, "Remove Current Item?", "Do you want to remove the unit's current item?", Statics.BACKGROUND_R, CONFIRM_REMOVE_ITEM);
		this.getThread().setNextModule(m);
	}


	public void removeCurrentItem() {
		if (this.current_unit.checkAndReduceAPs(this, APS_REMOVE_ITEM)) {
			if (this.current_unit != null) { // In case killed by nerve gas
				this.current_unit.current_item = null;
				this.updateUnitOnServer(this.current_unit);
				this.updateMenu(MM_UNIT_SELECTED);
			}
		}
	}


	public void absorbeCorpse() {
		AbstractActivity act = Statics.act;

		if (this.current_unit != null) {
			// Check there is a corpse
			boolean corpse_found = false;
			EquipmentData equip;
			ArrayList<EquipmentData> al = mapdata.getSq_MaybeNULL(this.current_unit.getMapX(), this.current_unit.getMapY()).getEquipment_MaybeNULL();
			if (al != null) {
				for (int i=0 ; i<al.size() ; i++) {
					equip = al.get(i);
					if (EquipmentTypesTable.CORPSES.contains(equip.major_type)) {
						corpse_found = true;
						if (this.current_unit.checkAndReduceAPs(this, APS_ABSORB)) {
							if (this.current_unit != null) { // In case killed by nerve gas after checkAndReduceAPs()
								if (equip.model != null) {
									equip.model.removeFromParent();
									equip.model = null;
								}
								equip.destroyed = true;
								this.mapdata.getSq_MaybeNULL(current_unit.getMapX(), current_unit.getMapY()).removeEquipment(equip);

								// Now NOT seen by any side
								for (int s=1 ; s<=4 ; s++) {
									equip.seen_by_side[s] = 0;
								}
								this.updateEquipmentOnServer(equip, current_unit.getMapX(), current_unit.getMapY());

								if (this.current_unit.model_type == UnitsTable.MT_BLOB) {
									this.current_unit.incHealth(15, true);
									this.current_unit.strength += 15;
									this.current_unit.combat_skill += 15;
									this.updateUnitOnServer(this.current_unit);

									Blob b = (Blob)this.current_unit.model;
									b.recreateBitmaps(this, this.current_unit);
								} else {
									if (game_data.can_build_and_dismantle == 1) {
										if (current_unit.model_type == UnitsTable.MT_ALIEN_TYRANT || current_unit.model_type == UnitsTable.MT_QUEEN_ALIEN) {
											this.game_data.res_points[game_data.our_side] += 1;
											setResPointsForSideOnServer(game_data.our_side, this.game_data.res_points[game_data.our_side]);
										}
									}
								}
								this.addToHUD("Corpse absorbed.");

								/*try {
							// Send message
							boolean who_can_see[] = this.whichSidesCanSeeUnit(this.current_unit);
							// send item picked up event to server
							this.sendEventToServer_BlobAbsorbed(this.current_unit, who_can_see);
						} catch (Exception ex) {
							AppletMain.HandleError(this, ex);
						}*/
								this.updateMenu(MM_UNIT_SELECTED);
								return;
							}
						}
					}
				}
			}

			if (corpse_found == false) {
				this.addToHUD(act.getString(R.string.no_corpse));
			}
		}
	}


	public void splitBlob() {
		AbstractActivity act = Statics.act;

		if (this.current_unit.getHealth() >= 6) {
			// Check there is an empty square
			AppletMapSquare sq = null;
			for (int z=this.current_unit.getMapY()-1 ; z<=this.current_unit.getMapY()+1 ; z++) {
				for (int x=this.current_unit.getMapX()-1 ; x<=this.current_unit.getMapX()+1 ; x++) {
					AppletMapSquare sq_tmp = mapdata.getSq_MaybeNULL(x, z);
					if (sq_tmp != null) {
						if (sq_tmp.major_type == MapDataTable.MT_FLOOR && sq_tmp.door_type <= 0) {
							if (this.getUnitAt(x, z) == null) {
								sq = sq_tmp;
							}
						}
					}
				}
			}
			if (sq != null) {
				if (this.current_unit.checkAndReduceAPs(this, APS_SPLIT)) {
					if (this.current_unit != null) { // In case killed by nerve gas
						this.current_unit.setHealth(this.current_unit.getHealth()/2);
						this.current_unit.setMaxHealth((this.current_unit.getMaxHealth()/2));
						this.current_unit.combat_skill -= this.current_unit.combat_skill/2;
						this.current_unit.strength -= this.current_unit.strength/2;
						Blob b = (Blob)this.current_unit.model;
						b.recreateBitmaps(this, this.current_unit);
						this.updateUnitOnServer(this.current_unit);

						game_data.units_remaining[current_unit.getSide()]++;

						// Send message
						boolean who_can_see[] = this.whichSidesCanSeeUnit(this.current_unit);
						// send item picked up event to server
						this.sendEventToServer_BlobSplit(this.current_unit, who_can_see, sq.x, sq.y);

						// Create new unit - must be after we've updated the original unit!
						UnitData unit = this.current_unit.clone();
						//unit.unitid = new_id;
						unit.map_x = sq.x;
						unit.map_y = sq.y;
						unit.aps = 0;
						unit.num = units.length;
						try {
							// Add unit to units[] array
							UnitData[] new_units = new UnitData[units.length+1];
							for (int i=0 ; i<units.length ; i++) {
								new_units[i] = units[i];
							}
							new_units[new_units.length-1] = unit;
							units = new_units;
						} catch (Exception ex) {
							AbstractActivity.HandleError(ex);
						}

						unit.model = AbstractUnit.Factory(this, unit);
						root_node.attachChild(unit.model);
						unit.updateModelFromUnitData();
						root_node.updateGeometricState();
						this.recalcVisibleEnemiesAndOppFire(true, null);
						this.addToHUD("Blob has split.");
						this.updateMenu(MM_UNIT_SELECTED);
					}
				}
			} else {
				this.addToHUD(act.getString(R.string.no_room));
			}
		} else {
			this.addToHUD("Blob is too small to split!  6 HP required.");
		}
	}


	public UnitData getUnitAt(int map_x, int map_z) {
		for(int i=0 ; i<units.length ; i++) {
			UnitData unit = units[i];
			if (unit.getStatus() == UnitsTable.ST_DEPLOYED) {
				if (unit.getMapX() == map_x && unit.getMapY() == map_z) {
					return unit;
				}
			}
		}
		return null;
	}


	public UnitData getDeadUnitAt(int map_x, int map_z) {
		for(int i=0 ; i<units.length ; i++) {
			UnitData unit = units[i];
			if (unit.getStatus() == UnitsTable.ST_DEAD) {
				if (unit.getMapX() == map_x && unit.getMapY() == map_z) {
					return unit;
				}
			}
		}
		return null;
	}


	public void showPrimeMenu() {
		AbstractActivity act = Statics.act;

		icon_table.less_icon.action = icon_table.do_prime_icon;
		icon_table.more_icon.action = icon_table.do_prime_icon;
		this.updateMenu(MM_PRIME_GRENADE);
		//this.addToHUD("");
		this.addToHUD(act.getString(R.string.priming_help) + ":-");
		this.addToHUD("0 - " + act.getString(R.string.ph_end_of_turn));
		if (this.game_data.num_players == 2) {
			this.addToHUD("1 - " + act.getString(R.string.ph_end_opp_turn));
			this.addToHUD("2 - " + act.getString(R.string.ph_end_next_turn));
			this.addToHUD("4 - End of your second next turn");
		} else if (this.game_data.num_players == 3) {
			this.addToHUD("1 - " + act.getString(R.string.ph_end_opp1_turn));
			this.addToHUD("2 - " + act.getString(R.string.ph_end_opp2_turn));
			this.addToHUD("3 - " + act.getString(R.string.ph_end_next_turn));
			this.addToHUD("6 - End of your second next turn");
		} else if (this.game_data.num_players == 4) {
			this.addToHUD("1 - " + act.getString(R.string.ph_end_opp1_turn));
			this.addToHUD("2 - " + act.getString(R.string.ph_end_opp2_turn));
			this.addToHUD("3 - " + act.getString(R.string.ph_end_opp3_turn));
			this.addToHUD("4 - " + act.getString(R.string.ph_end_next_turn));
			this.addToHUD("8 - End of your second next turn");
		}
		this.addToHUD(act.getString(R.string.ph_and_so_on));
	}


	/*public void showConfirmEndTurnMenu() {
		waitForPendingUpdateRequests();
		this.updateMenu(MM_CONFIRM_END_TURN);
	}*/


	private void blobDoExplode(UnitData blob) {
		int DAMAGE = blob.getBlobExplodeDamage();//.strength * 2;
		int RAD = blob.getBlobExplodeRad(); //.strength/10;
		this.normalExplosion(blob.map_x, blob.map_y, RAD, DAMAGE, blob, false, false); // Was alien acid, changed 29/11/13

	}


	public void blobAttemptExplode() {
		if (this.current_unit.checkAndReduceAPs(this, APS_EXPLODE)) {
			if (this.current_unit != null) { // In case killed by nerve gas
				UnitData this_unit = this.current_unit; // Need this as current_unit is set to null when unit is killed
				this.current_unit.damage(this, this.current_unit.getHealth(), this.current_unit, UnitDataComms.FOA_EXPLOSION);
				this.updateUnitOnServer(this_unit);
				this.blobDoExplode(this_unit);
			}
		}
	}


	/**
	 * This gets called when a player has definitely confirmed they are ending their turn.
	 * 
	 */
	public void showExplosionsAndEndTurn() {
		this.hud.clearText();
		this.addToHUD("Please wait...");
		this.current_unit = null;

		this.time_since_last_explosion = 0; // So we show explosion straight away
		this.client_mode = APP_TURN_ENDED_SHOWING_EXPLOSIONS; // Need this first so we know if we can show explosions

		this.updateMenu(MM_TURN_FINISHED_NO_MENU);

		// Check for units in nerve gas & fire
		for (int i=0 ; i<this.units.length ; i++) {
			if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
				if (units[i].aps > 0) { // Only harm units with APs left
					AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(units[i].map_x, units[i].map_y);
					if (sq != null) {
						if (sq.smoke_type == EquipmentTypesTable.ET_NERVE_GAS) {
							int dam = this.randomizeDamage(units[i].aps);
							units[i].aps = 0; // Stop them getting harmed more and also from doing anything
							UnitData unit = null;
							if (sq.smoke_caused_by > 0) {
								unit = UnitData.GetUnitDataFromID(units, sq.smoke_caused_by);
							}
							units[i].damage(this, dam, unit, UnitDataComms.FOA_NERVE_GAS);
						} else if (sq.smoke_type == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
							if (units[i].on_fire == 0) {
								units[i].on_fire = 1;
								this.updateUnitOnServer(units[i]);
							}							
						}
					}
					// Do this after we've checked if they are in a flaming square as we might have just set them alight
					if (units[i].on_fire != 0) {
						int dam = this.randomizeDamage(units[i].aps/2);
						units[i].aps = 0; // Stop them getting harmed more and also from doing anything
						//units[i].on_fire = 1;
						units[i].damage(this, dam, null, UnitDataComms.FOA_FIRE);
					}
				}
			}
		}


		// Find any grenades that might want to explode
		int turns_in_advance_to_check = getTurnsInAdvanceToCheck(); // If the next player is dead, we want to explode their grenades now since they won't run the client to explode them at the end of their turn.

		for (int i=0 ; i<this.equipment.length ; i++) {
			EquipmentData eq = (EquipmentData)this.equipment[i];
			if (eq.destroyed == false) {
				if (eq.primed) {
					if (eq.explode_turns <= turns_in_advance_to_check) {
						if (eq.major_type == EquipmentTypesTable.ET_GRENADE || eq.major_type == EquipmentTypesTable.ET_EXPLOSIVES || eq.major_type == EquipmentTypesTable.ET_SMOKE_GRENADE || eq.major_type == EquipmentTypesTable.ET_NERVE_GAS || eq.major_type == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
							UnitData caused_by = null;
							if (eq.last_unit_to_touch > 0) {
								caused_by = UnitData.GetUnitDataFromID(units, eq.last_unit_to_touch);
							}
							this.scheduleExplosion(eq, caused_by);
						}
					}
				}
			}
		}
	}


	private int getTurnsInAdvanceToCheck() {
		if (this.game_data.num_players <= 2) {
			return 0;
		} else {
			int ret = 0;
			int s = this.game_data.our_side;
			for (int i=0 ; i<this.game_data.num_players ; i++) {
				s++;
				if (s > this.game_data.num_players) {
					s = 1;
				}
				if (game_data.units_remaining[s] > 0) {
					break;
				}
				ret++;
			}
			return ret;
		}
	}


	/**
	 * This should only be called when checking explosions_to_show
	 * @param grenade
	 * @param caused_by_unit
	 */
	private void explodeGrenade(EquipmentData grenade, UnitData caused_by_unit) { // We must pass the causer sep as it may be caused by an explosion of another grenade!
		this.waitForPendingUpdateRequests();

		Point p = grenade.getMapLocation(units, mapdata);
		int mapx = p.x;
		int mapz = p.y;

		grenade.equipmentDestroyed(this);
		if (grenade.major_type == EquipmentTypesTable.ET_SMOKE_GRENADE || grenade.major_type == EquipmentTypesTable.ET_NERVE_GAS || grenade.major_type == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
			this.smokeGrenadeOrNerveGasExplosion(mapx, mapz, grenade.explosion_rad, grenade.major_type, caused_by_unit);
		} else { // Normal grenade or explosive
			this.normalExplosion(mapx, mapz, grenade.explosion_rad, grenade.explosion_dam, caused_by_unit, grenade.major_type == EquipmentTypesTable.ET_EXPLOSIVES, false);
		}
		if (grenade.model != null) { // Might be being held by a unit, so there is no model!
			grenade.model.removeFromParent();
		}
	}


	private void smokeGrenadeOrNerveGasExplosion(int mapx, int mapz, int radius, byte eq_type, UnitData caused_by_unit) {
		AbstractActivity act = Statics.act;

		this.addToHUD(act.getString(R.string.been_explosion));
		this.sendEventToServer_Explosion(mapx, mapz, radius, eq_type);

		HashSet<AppletMapSquare> changed_squares = new HashSet<AppletMapSquare>();
		int caused_by_unitid = -1;
		if (caused_by_unit != null) {
			caused_by_unitid = caused_by_unit.unitid;
		}
		int tot_existing_smoke = 0;
		for (int r=0 ; r<=radius ; r++) {
			tot_existing_smoke += checkForSmokeAccess_init(mapx, mapz, r, radius, eq_type, caused_by_unitid, changed_squares);
		}
		if (tot_existing_smoke > 0) {
			for (int r=0 ; r<=radius*4 ; r++) {
				if (tot_existing_smoke > 0) {
					tot_existing_smoke = checkForSmokeAccess_extra(mapx, mapz, r, r+1, eq_type, caused_by_unitid, changed_squares, tot_existing_smoke);
				}
			}
		}
		this.createMapModel(); // To show smoke

		root_node.updateGeometricState();

		this.showExplosion(mapx, mapz, radius, eq_type);

	}


	/**
	 * Try not to call this directly, call ScheduleExplosion
	 * 
	 */
	private void normalExplosion(int mapx, int mapy, int map_radius, int damage, UnitData caused_by_unit, boolean explosives, boolean alien_acid) {
		AbstractActivity act = Statics.act;

		if (map_radius <= 0) {
			map_radius = 1;
		}
		// Vibrate cam
		this.root_cam.vibrate(Statics.CAM_VIB_DUR);

		// These must be here so they get called when it's a rocket launcher
		this.addToHUD(act.getString(R.string.been_explosion));
		this.sendEventToServer_Explosion(mapx, mapy, map_radius, EquipmentTypesTable.ET_GRENADE);

		// Loop through units and see if they are hit.
		// We do this first as if we remove the walls first we won't know who is protected
		for (int i=0 ; i<this.units.length ; i++) {
			if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
				if (units[i].model_type == UnitsTable.MT_ALIEN_TYRANT || units[i].model_type == UnitsTable.MT_QUEEN_ALIEN) {
					if (alien_acid) {
						continue;
					}
				}

				float dist = units[i].model.getMapDistTo(mapx, mapy); //units[i].model.getWorldCentreX() / Statics.SQ_SIZE;
				if (dist <= map_radius) {
					int wall_count = units[i].model.getExplosionWallProtectionCount(mapx, mapy); 
					//boolean hit = units[i].model.isHitByExplosion(mapx, mapy);//.getCanSeeCount(mapx, mapy, -1, false, 2, true, false);
					boolean hit = true; // Default for weak walls
					if (this.game_data.wall_type == AbstractMission.INDESTRUCTABLE_WALLS) {
						hit = wall_count <= 0;
					} else if (this.game_data.wall_type == AbstractMission.STRONG_WALLS) {
						hit = wall_count <= 1;
					}
					if (explosives || hit) { // || this.game_data.wall_type != AbstractMission.INDESTRUCTABLE_WALLS) {
						//p("------------------------");
						//p("Unit " + units[i].name + " caught in explosion.");
						//p("Initial damage: " + damage);
						int dam = this.randomizeDamage(damage);
						// Adjust damage because wall is in the way?
						/*if (explosives == false && hit && this.game_data.wall_type == AbstractMission.STRONG_WALLS) {
							p("Unit " + units[i].name + " is protected by a wall");
							dam = (short)(dam / 2);
						}*/
						if (dist >= 1) { // we're not standing on it/holding it! 
							int x = units[i].map_x - mapx;
							int y = units[i].map_y - mapy;
							float ang = GeometryFuncs.NormalizeAngle(GeometryFuncs.GetAngleFromDirection(x, y));
							float angle_mult = this.getAdjustmentForAngle(ang, units[i].angle);
							dam -= (int)((float)units[i].protection * angle_mult);
						} else {
							p("Unit is on top of grenade!");
						}
						p("Final damage: " + dam);
						units[i].damage(this, dam, caused_by_unit, UnitDataComms.FOA_EXPLOSION);
					}
				}
			}
		}

		// Loop through equipment and see if they are hit - must be after we've checked units as otherwise the equip won't have a model.
		for (int i=0 ; i<this.equipment.length ; i++) {
			if (equipment[i].destroyed == false && equipment[i].getUnitID() <= 0 && equipment[i].indestructable == 0 && equipment[i].model != null) { 
				float dist = equipment[i].model.getMapDistTo(mapx, mapy);
				if (dist <= map_radius) {
					int wall_count = equipment[i].model.getExplosionWallProtectionCount(mapx, mapy); 
					boolean hit = true; // Default for weak walls
					if (this.game_data.wall_type == AbstractMission.INDESTRUCTABLE_WALLS) {
						hit = wall_count <= 0;
					} else if (this.game_data.wall_type == AbstractMission.STRONG_WALLS) {
						hit = wall_count <= 1;
					}
					if (explosives || hit) {
						int rnd = NumberFunctions.rnd(1, 100);
						if (equipment[i].major_type == EquipmentTypesTable.ET_GRENADE || equipment[i].major_type == EquipmentTypesTable.ET_EXPLOSIVES || equipment[i].major_type == EquipmentTypesTable.ET_GAS_CANNISTER || equipment[i].major_type == EquipmentTypesTable.ET_SMOKE_GRENADE || equipment[i].major_type == EquipmentTypesTable.ET_NERVE_GAS) {
							//p("Chain reaction: " + rnd);
							if (rnd <= 50) { // Only 50% chance of exploding
								// This grenade is caused by the same unit as the original explosion, not the unit holding it.
								this.scheduleExplosion(equipment[i], caused_by_unit);
							} else {
								//NO!  equipment[i].equipmentDestroyed(this);
							}
						} else {
							if (rnd <= 50) { // Only 50% chance of being destroyed
								equipment[i].equipmentDestroyed(this);
							}
						}
					}
				}
			}
		}

		// Check for destroyed walls - AFTER WE'VE CHECKED FOR DESTROYED UNITS!
		boolean any_walls_destroyed = false;
		int side = -1;
		if (caused_by_unit != null) {
			side = caused_by_unit.getSide();
		}

		if ((this.game_data.wall_type == AbstractMission.WEAK_WALLS || explosives) && alien_acid == false) {			
			any_walls_destroyed = checkForWallDamage_WEAK_WALLS(mapx, mapy, map_radius, side);
		} else if (this.game_data.wall_type == AbstractMission.STRONG_WALLS) {			
			//any_walls_destroyed = checkForWallDamage_STRONG_WALLS(mapx, mapy, map_radius, side);
			for (int i=map_radius ; i>=0 ; i--) { // Start from the outside to ensure outer walls don't get damaged if there is another wall in the way
				boolean any_dam = checkForWallDamage_STRONG_WALLS(mapx, mapy, i, map_radius, side);
				any_walls_destroyed = any_walls_destroyed || any_dam;
			}
		} else if (this.game_data.wall_type == AbstractMission.INDESTRUCTABLE_WALLS || alien_acid) {			
			// Check for destroyed doors etc...
			for (int i=map_radius ; i>=0 ; i--) { // Start from the outside to ensure outer walls don't get damaged if there is another wall in the way
				boolean any_dam = checkForWallDamage_SOLID_WALLS(mapx, mapy, i, map_radius, side);
				any_walls_destroyed = any_walls_destroyed || any_dam;
			}
		}

		if (any_walls_destroyed) {
			this.createMapModel();//changed_squares);
		}

		root_node.updateGeometricState();

		this.showExplosion(mapx, mapy, map_radius, EquipmentTypesTable.ET_GRENADE);

	}


	private boolean checkForWallDamage_WEAK_WALLS(int mapx, int mapz, int this_rad, int caused_by_side) {//, GameObject gren, EquipmentData eq, int caused_by_side, HashSet<AppletMapSquare> changed_squares) {
		boolean any_walls_destroyed = false;

		for (int z=mapz-this_rad ; z<=mapz+this_rad ; z++) {
			for (int x=mapx-this_rad ; x<=mapx+this_rad ; x++) {
				AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(x, z);
				if (sq != null) {
					double dist = GeometryFuncs.distance(x, z, mapx, mapz);
					if (dist <= this_rad) {
						boolean is_this_destroyed = false;

						if (sq.major_type == MapDataTable.MT_WALL) {
							is_this_destroyed = true;
						} else if (sq.major_type == MapDataTable.MT_FLOOR) {
							if (sq.door_type > 0) {
								sq.door_type = -1;
							}
							is_this_destroyed = true; // Destroy scenery as well
							if (sq.smoke_type > 0) {
								if (NumberFunctions.rnd(1, 2) == 1) {
									sq.smoke_type = 0;
								}
							}
						} else if (sq.major_type == MapDataTable.MT_COMPUTER && sq.destroyed == 0) {
							is_this_destroyed = true;
							this.addToHUD("Computer destroyed!");
						}

						// Update server if any squares were destroyed
						if (is_this_destroyed) {
							if (sq.major_type != MapDataTable.MT_COMPUTER) { // We need to keep computers so we can calc VPs
								sq.major_type = MapDataTable.MT_FLOOR;
								sq.texture_code = TextureStateCache.GetRandomRubbleTex();
								sq.scenery_code = 0;
								sq.raised_texture_code = 0;
								any_walls_destroyed = true;
							}
							sq.destroyed = 1;
							this.updateMapOnServer(sq, caused_by_side);

						}
					}
				}
			}
		}
		return any_walls_destroyed;
	}


	private boolean checkForWallDamage_STRONG_WALLS(int epicentre_mapx, int epicentre_mapz, int this_rad, int max_rad, int caused_by_side) {//, GameObject gren, EquipmentData eq, int caused_by_side, HashSet<AppletMapSquare> changed_squares) {
		boolean any_walls_destroyed = false;

		for (int z=epicentre_mapz-this_rad ; z<=epicentre_mapz+this_rad ; z++) {
			for (int x=epicentre_mapx-this_rad ; x<=epicentre_mapx+this_rad ; x++) {

				if (z == epicentre_mapz-this_rad || z == epicentre_mapz+this_rad || x == epicentre_mapx-this_rad || x == epicentre_mapx+this_rad) {

					// Check the wall has LOS
					if (this.mapdata.isLOS_Faces(epicentre_mapx, epicentre_mapz, x, z, true, true) == false) {
						continue;
					}
					double dist = GeometryFuncs.distance(x, z, epicentre_mapx, epicentre_mapz);
					if (dist <= max_rad) {
						AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(x, z);
						if (sq != null) {
							boolean is_this_destroyed = false;
							if (sq.major_type == MapDataTable.MT_WALL) {
								is_this_destroyed = true;
							} else if (sq.major_type == MapDataTable.MT_FLOOR) {
								if (sq.door_type > 0) {
									sq.door_type = -1;
								}
								is_this_destroyed = true; // Destroy scenery as well
								if (sq.smoke_type > 0) {
									if (NumberFunctions.rnd(1, 2) == 1) {
										sq.smoke_type = 0;
									}
								}
							} else if (sq.major_type == MapDataTable.MT_COMPUTER && sq.destroyed == 0) {
								is_this_destroyed = true;
								this.addToHUD("Computer destroyed!");
							}

							// Update server if any squares were destroyed
							if (is_this_destroyed) {
								if (sq.major_type != MapDataTable.MT_COMPUTER) { // We need to keep computers to calc VPs
									sq.major_type = MapDataTable.MT_FLOOR;
									sq.texture_code = TextureStateCache.GetRandomRubbleTex();//TEX_RUBBLE;
									sq.scenery_code = 0;
									sq.raised_texture_code = 0;
									any_walls_destroyed = true;
								}
								sq.destroyed = 1;
								this.updateMapOnServer(sq, caused_by_side);
							}
						}
					}
				}
			}
		}
		return any_walls_destroyed;
	}


	private boolean checkForWallDamage_SOLID_WALLS(int mapx, int mapz, int this_rad, int max_rad, int caused_by_side) {//, GameObject gren, EquipmentData eq, int caused_by_side, HashSet<AppletMapSquare> changed_squares) {
		boolean any_walls_destroyed = false;
		for (int z=mapz-this_rad ; z<=mapz+this_rad ; z++) {
			for (int x=mapx-this_rad ; x<=mapx+this_rad ; x++) {
				if (x == mapx-this_rad || x == mapx+this_rad || z == mapz-this_rad || z == mapz+this_rad) { // Only check the current radius
					AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(x, z);
					if (sq != null) {
						// Check the wall has LOS
						if (this.mapdata.isLOS_Faces(mapx, mapz, x, z, true, true) == false) {
							continue;
						}
						boolean is_this_destroyed = false;
						double dist = GeometryFuncs.distance(x, z, mapx, mapz);
						if (dist <= max_rad) {
							if (sq.major_type == MapDataTable.MT_NOTHING || sq.major_type == MapDataTable.MT_WALL) {
								// Do nothing as walls are not destroyable
							} else if (sq.major_type == MapDataTable.MT_FLOOR) {
								if (sq.door_type > 0) {
									sq.door_type = -1;
									is_this_destroyed = true;
									sq.texture_code = TextureStateCache.GetRandomRubbleTex();//TEX_RUBBLE;
								}
								if (sq.smoke_type > 0) {
									if (NumberFunctions.rnd(1, 2) == 1) {
										sq.smoke_type = 0;
									}
								}
							} else if (sq.major_type == MapDataTable.MT_COMPUTER && sq.destroyed == 0) {
								sq.destroyed = 1;
								is_this_destroyed = true;
								this.addToHUD("Computer destroyed!");
							}
							// Now update the server if anything has changed
							if (is_this_destroyed) { 
								sq.scenery_code = 0;
								sq.raised_texture_code = 0;
								this.updateMapOnServer(sq, caused_by_side);
								any_walls_destroyed = true;
							}
						}
					}
				}
			}
		}
		return any_walls_destroyed;
	}


	/**
	 * This returns how many squares already had smoke
	 */
	private int checkForSmokeAccess_init(int mapx, int mapz, int this_rad, int max_rad, byte eq_type, int caused_by_unitid, HashSet<AppletMapSquare> changed_squares) {
		int existing_smoke_count = 0;
		for (int z=mapz-this_rad ; z<=mapz+this_rad ; z++) {
			for (int x=mapx-this_rad ; x<=mapx+this_rad ; x++) {
				if (x == mapx-this_rad || x == mapx+this_rad || z == mapz-this_rad || z == mapz+this_rad) { // Only check the current radius
					if (GeometryFuncs.distance(mapx, mapz, x, z) <= max_rad) { // Ensure a circle of smoke
						AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(x, z);
						if (sq != null) {
							if (this.mapdata.getBlockCountInLOS_Pixels_Centre(mapx, mapz, x, z, true, false) <= 0) {
								if (sq.major_type == MapDataTable.MT_FLOOR) {
									if (sq.smoke_type > 0) {
										existing_smoke_count++;
										/*if (eq_type != EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE && NumberFunctions.rnd(1, 2) == 1) { // Do we replace it?
											continue;
										}*/
									}
									sq.smoke_type = eq_type;
									sq.smoke_caused_by = caused_by_unitid;
									this.updateMapOnServer(sq, 0);
									changed_squares.add(sq);
								}
							}
						}
					}
				}
			}
		}
		return existing_smoke_count;
	}


	/**
	 * This returns how many smoke squares remaining to be added
	 */
	private int checkForSmokeAccess_extra(int mapx, int mapz, int this_rad, int max_rad, byte eq_type, int caused_by_unitid, HashSet<AppletMapSquare> changed_squares, int extra_count) {
		if (extra_count <= 0) {
			return 0;
		}
		for (int z=mapz-this_rad ; z<=mapz+this_rad ; z++) {
			for (int x=mapx-this_rad ; x<=mapx+this_rad ; x++) {
				if (x == mapx-this_rad || x == mapx+this_rad || z == mapz-this_rad || z == mapz+this_rad) { // Only check the current radius
					if (GeometryFuncs.distance(mapx, mapz, x, z) <= max_rad) { // Ensure a circle of smoke
						AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(x, z);
						if (sq != null) {
							if (this.mapdata.getBlockCountInLOS_Pixels_Centre(mapx, mapz, x, z, true, false) <= 0) {
								if (sq.major_type == MapDataTable.MT_FLOOR) {
									if (sq.smoke_type > 0) {
										// Already smoke here!
										if (eq_type != EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE && NumberFunctions.rnd(1, 3) >= 2) { // Do we replace it?
											continue;
										}
										extra_count++;
									}
									sq.smoke_type = eq_type;
									sq.smoke_caused_by = caused_by_unitid;
									this.updateMapOnServer(sq, 0);
									changed_squares.add(sq);
									extra_count--;
									if (extra_count == 0) {
										return 0;
									}
								}
							}
						}
					}
				}
			}
		}
		return extra_count;
	}


	/**
	 * Only call this if it's the end of the turn!  NO!  What about death grenades?
	 */
	private void showExplosion(int mapx, int mapy, float map_radius, byte gren_type) {
		AbstractActivity act = Statics.act;

		if (gren_type == EquipmentTypesTable.ET_SMOKE_GRENADE) {
			act.playSound(R.raw.smokegrenadehiss);
		} else if (gren_type == EquipmentTypesTable.ET_NERVE_GAS) {
			act.playSound(R.raw.nervegashiss);
		} else {
			act.playSound(R.raw.explosion1);
		}

		// Stop the camera moving back onto the unit
		this.current_unit = null;
		this.updateMenu(MM_NO_UNIT_SELECTED);// MM_TURN_FINISHED_NO_MENU - No!  as it might be during the game, not at the end of the turn!

		// Look at the explosion
		this.root_cam.lookAt(mapx * Statics.SQ_SIZE, mapy * Statics.SQ_SIZE, true);

		IOFunctions.Vibrate(act.getBaseContext(), 200);
		float rad = map_radius * Statics.SQ_SIZE;
		new Explosion(this, (mapx * Statics.SQ_SIZE)+(Statics.SQ_SIZE/2), (mapy * Statics.SQ_SIZE)+(Statics.SQ_SIZE/2), rad);
	}


	private int randomizeDamage(int dam) {
		return (int)((float)dam * (NumberFunctions.rndFloat(.5f, 1.5f)));		
	}


	public void openDoor() {
		AbstractActivity act = Statics.act;

		AppletMapSquare sq = this.current_unit.model.getSquareInFrontOfUnit_MaybeNULL();
		if (sq != null) {
			if (sq.door_type > 0) {
				if (sq.door_open == false) {
					if (this.current_unit.checkAndReduceAPs(this, APS_OPEN_DOOR)) {
						this.updateUnitOnServer(this.current_unit);
						act.playSound(R.raw.door);

						if (sq.door != null) {
							sq.door.startOpening();
							sq.door.opposite.startOpening();
						}
						sq.door_open = true;

						this.updateMapOnServer(sq, -1);
						recalcVisibleEnemiesAndOppFire(true, null); //this.current_unit);  MUST BE NULL AS THE UNIT THAT OPENED THE DOOR MAY NOT BE THE NEWLY VISIBLE ONE
						this.updateMenu(MM_UNIT_SELECTED);

					}				
				} else {
					this.addToHUD(act.getString(R.string.door_already_open));
				}
			} else {
				this.addToHUD(act.getString(R.string.no_door));
			}
		}
	}


	public void closeDoor() {
		AbstractActivity act = Statics.act;

		AppletMapSquare sq = this.current_unit.model.getSquareInFrontOfUnit_MaybeNULL();
		if (sq != null) {
			if (sq.door_type > 0) {
				if (sq.door_open) {
					UnitData unit = getUnitAt(sq.x, sq.y);
					if (unit == null) {
						// Check there's no equipment
						ArrayList<EquipmentData> al = sq.getEquipment_MaybeNULL();
						if (al == null || al.size() == 0) {
							if (this.current_unit.checkAndReduceAPs(this, APS_OPEN_DOOR)) {
								this.updateUnitOnServer(this.current_unit);
								act.playSound(R.raw.door);

								if (sq.door != null) {
									sq.door.startClosing();
									sq.door.opposite.startClosing();
								}
								sq.door_open = false;

								this.updateMapOnServer(sq, -1);
								this.updateMenu(MM_UNIT_SELECTED);

							}				
						} else {
							this.addToHUD(act.getString(R.string.equipment_in_the_way));
						}
					} else {
						this.addToHUD(act.getString(R.string.already_a_unit));
					}
				} else {
					this.addToHUD(act.getString(R.string.door_already_closed));
				}
			} else {
				this.addToHUD(act.getString(R.string.no_door));
			}
		}
	}


	public void changeCurrentEquipment(EquipmentData eq) {
		if (this.current_unit != null) {
			if (this.current_unit.current_item != null) {
				if (eq == this.current_unit.current_item) { // Are they changing to the same item?
					this.updateMenu(MM_UNIT_SELECTED);
					return; // Do nothing
				}
			}

			int cost = APS_CHANGE_ITEM;
			// Inc cost if they're holding something - NO!  TO EXPENSIVE!
			/*if (this.current_unit.current_item != null) {
			cost = cost * 2;
			}*/
			if (this.current_unit.checkAndReduceAPs(this, cost)) {
				current_unit.current_item = eq;
				this.updateMenu(MM_UNIT_SELECTED);

				this.updateEquipmentOnServer(eq, current_unit.getMapX(), current_unit.getMapY());
				this.updateUnitOnServer(this.current_unit);
			}
		}
	}


	public void escape() {
		AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(this.current_unit.map_x, this.current_unit.map_y);
		if (sq != null) {
			if (sq.escape_hatch_side == this.current_unit.getSide() || sq.escape_hatch_side > 4) { // > 4 = all sides
				this.current_unit.escaped(this);
			}
		}
	}


	public void pickupEquipment(EquipmentData eq) {
		AbstractActivity act = Statics.act;

		if (this.current_unit != null) {
			// Check we can carry it (due to weight)
			if (eq.weight <= this.current_unit.strength) {
				int cost = APS_PICKUP_ITEM;
				// Adjust APs, depends on if they were holding anything already
				if (this.current_unit.current_item != null) {
					cost = cost * 2;
				}
				if (this.current_unit.checkAndReduceAPs(this, cost)) {
					eq.model.removeFromParent(); // error here if the equipment is destroyed
					eq.model = null;
					eq.setUnitID(this.current_unit.unitid);
					AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(current_unit.getMapX(), current_unit.getMapY());
					if (sq != null) {
						sq.removeEquipment(eq);
					}

					this.current_unit.current_item = eq;
					this.current_unit.items.add(eq);

					// Now NOT seen by any side
					for (int s=1 ; s<=4 ; s++) {
						eq.seen_by_side[s] = 0;
					}

					this.updateMenu(MM_UNIT_SELECTED);
					this.updateEquipmentOnServer(eq, current_unit.getMapX(), current_unit.getMapY());
					this.updateUnitOnServer(this.current_unit);

					// Send message
					boolean who_can_see[] = this.whichSidesCanSeeUnit(this.current_unit);
					// send item picked up event to server
					this.sendEventToServer_ItemPickedUp(this.current_unit, who_can_see);

				}
			} else {
				this.addToHUD(act.getString(R.string.too_heavy));
			}
		}
	}


	public void makeAimedShot() {
		AbstractActivity act = Statics.act;

		this.addToHUD(act.getString(R.string.attempting_aimed));
		this.shoot(this.current_unit, getAimedShotAccuracy(this.current_unit), this.current_unit.current_item.aimed_shot_aps, true, null);
	}


	public void makeSnapShot() {
		AbstractActivity act = Statics.act;

		this.addToHUD(act.getString(R.string.attempting_snap));
		this.shoot(this.current_unit, getSnapShotAccuracy(this.current_unit), this.current_unit.current_item.snap_shot_aps, true, null);
	}


	public void makeAutoShot() {
		AbstractActivity act = Statics.act;

		this.addToHUD(act.getString(R.string.attempting_auto));
		this.shoot(this.current_unit, getAutoShotAccuracy(this.current_unit), this.current_unit.current_item.auto_shot_aps, true, null);
	}


	public void shoot(UnitData shooter, int acc, int ap_cost, boolean show_bullet, Ray targetting_ray) {
		AbstractActivity act = Statics.act;

		EquipmentData gun = shooter.current_item;
		if (gun != null) {
			if (gun.major_type == EquipmentTypesTable.ET_GUN) {
				if (gun.getAmmo() > 0 || Statics.DEBUG) {
					if (targetting_ray == null) {
						targetting_ray = new Ray(shot_line);
					}
					// Check they are shooting in the direction they are facing!
					float ang = targetting_ray.getAngle();
					float diff = GeometryFuncs.GetDiffBetweenAngles(ang, shooter.getAngle());
					if (diff < Statics.VIEW_ANGLE) {
						if (shooter.checkAndReduceAPs(this, ap_cost)) {
							this.updateUnitOnServer(shooter);

							if (!Statics.DEBUG) {
								gun.decAmmo();
								this.updateEquipmentOnServer(gun, (byte)-1, (byte)-1);
							}
							hud.updateStatusBar(); // Update ammo status

							if (show_bullet == false) {
								IOFunctions.Vibrate(act.getBaseContext(), Statics.VIBRATE_LEN);
							}

							Ray actual_shot_ray = new Ray(targetting_ray);

							// Is it an accurate shot?
							float angle_adj = 0;
							int rnd = NumberFunctions.rnd(0, 100);
							if (rnd < acc) {
								angle_adj = NumberFunctions.rndFloat(0, 0.5f); // Was 0.4f
							} else {
								float badness = (rnd - acc) / 3;
								/*if (badness > 30) {
									badness = 30;
								}*/
								angle_adj = NumberFunctions.rndFloat(2f, 2f+badness);
							}

							if (angle_adj != 0) {
								if (NumberFunctions.rnd(0, 1) == 1) {
									angle_adj = angle_adj * -1;
								}
								actual_shot_ray.setDirection(GeometryFuncs.AdjustVectorByAngle(actual_shot_ray.getDirection(), angle_adj));
							}
							MyPickResults results = new MyPickResults(shooter.model, null);
							root_node.findPick(actual_shot_ray, results);

							// Add walls to the pick results.
							mapdata.canSee(actual_shot_ray, results, true, false);

							results.sort();
							boolean bullet_created = false;
							if (results.getNumber() > 0) {
								for (int i=0 ; i<results.getNumber() ; i++) {
									PickData o = results.getPickData(i);
									GameObject target = results.getGameObject(i);
									if (target.can_be_shot) {
										// Check it's not a hedge or something
										if (o.getDistance() >= (1.5f * Statics.SQ_SIZE)) {
											if (target.isChanceofNotHitting()) {
												int rnd2 = NumberFunctions.rnd(1, 100);
												if (rnd2 < target.getChanceofNotHitting()) {
													continue;
												}
											}
										}

										// They have hit something!
										boolean seen[] = this.whichSidesCanSeeUnit(shooter);
										this.sendEventToServer_ShotFired(shooter, (int)actual_shot_ray.getAngle(), (int)(o.getDistance() / Statics.SQ_SIZE), seen);

										if (gun.range_sq > 0 && o.getDistance() > (gun.range_sq * Statics.SQ_SIZE)) {
											// Out of range
										} else {
											// Launch bullet
											MyPointF vdiff = actual_shot_ray.getDirection().multiply(o.getDistance());
											MyPointF end = actual_shot_ray.getOrigin().add(vdiff);
											bullet_created = true;
											if (gun.code.equalsIgnoreCase(EquipmentTypesTable.CD_FIRE_EXTINGUISHER)) {
												new FireExtinguisherEffect(this, shooter.model.getWorldTopLeft_CreatesNew(), end);
												act.playSound(R.raw.smokegrenadehiss);
												shooter.on_fire = 0;
												if (target instanceof AbstractUnit) {
													AbstractUnit unit_target = (AbstractUnit)target;
													if (unit_target.unit_data.on_fire != 0) {
														unit_target.unit_data.on_fire = 0;
														this.updateUnitOnServer(unit_target.unit_data);
														this.addToHUD("The fire has been extinguished.");
													}
												}
												break;
											} else if (gun.code.equalsIgnoreCase(EquipmentTypesTable.CD_FLAMETHROWER)) {
												new FlameThrowerEffect(this, shooter.model.getWorldTopLeft_CreatesNew(), end);
												act.playSound(R.raw.fire);
												if (target instanceof AbstractUnit) {
													AbstractUnit unit_target = (AbstractUnit)target;
													if (unit_target.unit_data.on_fire == 0) {
														if (unit_target.unit_data.model_type != UnitsTable.MT_ANGEL) {
															unit_target.unit_data.on_fire = 1;
														}
														act.playSound(R.raw.fire);
														this.updateUnitOnServer(unit_target.unit_data);
														this.addToHUD("A unit has been set on fire!");
													}
												}
												break;
											} else {
												if (gun.ammo_type_id != 2) {
													new BulletLaser(this, shooter.model.getWorldCentre_CreatesNew(), end, false, show_bullet);
												} else {
													new Bullet(this, shooter.model.getWorldCentre_CreatesNew(), end, true, show_bullet, R.drawable.laser_bolt, 3, Statics.BULLET_SIZE);
												}
												act.playSound(R.raw.laser1);

												if (target instanceof AbstractUnit) {
													AbstractUnit unit_target = (AbstractUnit)target;
													if (shooter == this.current_unit) { // Check it's not Opp Fire or something
														this.addToHUD(act.getString(R.string.you_have_shot) + " " + unit_target.unit_data.name);
													}
													int dam = gun.shot_damage;
													dam = randomizeDamage(dam);
													//p("Damage: " + dam);
													float ang_adj = this.getAdjustmentForAngle(GeometryFuncs.GetAngleFromDirection(actual_shot_ray.direction.x, actual_shot_ray.direction.y), unit_target.unit_data.getAngle());
													//p("Full Protec: " + unit_target.unit_data.protection);
													float protec = (float)unit_target.unit_data.protection * ang_adj;
													//p("Protec adjusted for angle: " + protec);
													dam = (int)(dam - protec);
													int map_dist = (int)shooter.model.getMapDistTo(target);
													if (map_dist < 2) {
														dam += POINT_BLANK_DAMAGE_BONUS;
														p("Short-range bonus: " + POINT_BLANK_DAMAGE_BONUS);
													} else if (map_dist >= MIN_DAMAGE_REDUCTION_RANGE) {
														if (gun.ammo_type_id != 2) { // Lasers don't lose damage
															int max_reduction = dam/2;
															int reduction = (int)((map_dist - MIN_DAMAGE_REDUCTION_RANGE) / 2);

															if (reduction > max_reduction) {
																reduction = max_reduction;
															}
															dam -= reduction;
															p("Range reduction: " + reduction);
														}
													}
													unit_target.unit_data.damage(this, dam, shooter, UnitDataComms.FOA_SHOT);
												} else if (target instanceof GasCannister) {
													GasCannister gc = (GasCannister)target;
													this.scheduleExplosion(gc.eq, shooter);
													gc.removeFromParent();
												} else if (target instanceof DummyWall) {
													DummyWall w = (DummyWall)target;
													if (w.sq.major_type == MapDataTable.MT_COMPUTER) {
														this.computerDestroyed(w.sq, shooter);
														//AppletMain.p("Bullet hit wall.");
													} else {
														if (shooter == this.current_unit) { // Check it's not Opp Fire or something
															this.addToHUD(act.getString(R.string.shot_wall));
															if (is_april_fools_day) {
																int x = NumberFunctions.rnd(0, 10);
																if (x == 0) {
																	this.addToHUD("Did you miss deliberately?");
																}
															}
														}
													}
												} else {
													// Nothing happens
												}
												if (gun.explodes) {
													MyPointF vdiff2 = actual_shot_ray.getDirection().multiply(o.getDistance()-1); // Notice we move a square back to get a better explosion!
													MyPointF end2 = actual_shot_ray.getOrigin().add(vdiff2);
													this.normalExplosion((int)(end2.x / Statics.SQ_SIZE), (int)(end2.y / Statics.SQ_SIZE), gun.explosion_rad, gun.explosion_dam, shooter, false, false);
													this.recalcVisibleEnemiesAndOppFire(true, null);
												} else {
													this.recalcVisibleEnemiesAndOppFire(true, shooter); // Must be shooter otherwise we could get caught in a loop!
												}
												break;
											}
										}
									}
								}
							}
							if (bullet_created == false) { // Off the edge of the map
								// Launch bullet
								if (gun.code.equalsIgnoreCase(EquipmentTypesTable.CD_FIRE_EXTINGUISHER)) {
									MyPointF vdiff = actual_shot_ray.getDirection().multiply(Statics.SQ_SIZE * gun.range_sq);
									MyPointF end = actual_shot_ray.getOrigin().add(vdiff);
									new FireExtinguisherEffect(this, shooter.model.getWorldTopLeft_CreatesNew(), end);
									act.playSound(R.raw.smokegrenadehiss);
									shooter.on_fire = 0;
								} else if (gun.code.equalsIgnoreCase(EquipmentTypesTable.CD_FLAMETHROWER)) {
									MyPointF vdiff = actual_shot_ray.getDirection().multiply(Statics.SQ_SIZE * gun.range_sq);
									MyPointF end = actual_shot_ray.getOrigin().add(vdiff);
									new FlameThrowerEffect(this, shooter.model.getWorldTopLeft_CreatesNew(), end);
									act.playSound(R.raw.fire);
								} else {
									MyPointF vdiff = actual_shot_ray.getDirection().multiply(Statics.SQ_SIZE * mapdata.getMapHeight());
									MyPointF end = actual_shot_ray.getOrigin().add(vdiff);
									if (gun.ammo_type_id != 2) {
										new BulletLaser(this, shooter.model.getWorldCentre_CreatesNew(), end, false, !show_bullet);
									} else {
										new Bullet(this, shooter.model.getWorldCentre_CreatesNew(), end, false, !show_bullet, R.drawable.laser_bolt, 3, Statics.BULLET_SIZE);
									}
									act.playSound(R.raw.laser1);
								}
								bullet_created = true;
								boolean seen[] = this.whichSidesCanSeeUnit(shooter);
								this.sendEventToServer_ShotFired(shooter, (int)GeometryFuncs.GetAngleFromDirection(actual_shot_ray.direction.x, actual_shot_ray.direction.y), 10, seen);
								if (is_april_fools_day) {
									int x = NumberFunctions.rnd(0, 10);
									if (x == 0) {
										this.addToHUD("My granny could have done better.");
									}
								}
							}
						} else {
							/*if (DEBUG) {
								p("Not enough APs!");
							}*/
						}
					} else {
						if (shooter == this.current_unit) { // Check it's not Opp Fire or something
							this.addToHUD(act.getString(R.string.not_in_view));
							if (help_mode_on) {
								this.addToHUD(act.getString(R.string.white_line_help));
							}
						}
						/*if (DEBUG) {
							p("Angle too far");
						}*/
					}
				} else {
					if (shooter == this.current_unit) { // Check it's not Opp Fire or something
						this.addToHUD(act.getString(R.string.out_of_ammo));
						//playSound(shooter, R.raw SpeechPlayer.EV_OUT_OF_AMMO);
					}
					/*if (DEBUG) {
						p("Not enough Ammo");
					}*/
				}
			} else {
				/*if (DEBUG) {
					p("No gun");
				}*/
			}
		} else {
			/*if (DEBUG) {
				p("No equipment");
			}*/

		}
	}


	/**
	 * Returns false if looped round
	 * @return
	 */
	public boolean selectNextUnit() {
		if (game_data.game_status == GamesTable.GS_STARTED) {
			int curr_id = 0;
			if (this.current_unit != null) {
				curr_id = this.current_unit.num;
			}
			for (int i=curr_id+1 ; i<this.units.length ; i++) {
				if (units[i].getSide() == this.game_data.our_side) {
					if (units[i].hasAI() == false) {
						if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
							this.selectOurUnit(units[i], false);
							return true;
						}
					}
				}
			}
			// Start again from the beginning
			for (int i=0 ; i<this.units.length ; i++) {
				if (units[i].getSide() == this.game_data.our_side) {
					if (units[i].hasAI() == false) {
						if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
							this.selectOurUnit(units[i], false);
							return false;
						}
					}
				}
			}
		}
		return false;
	}


	public void selectPrevUnit() {
		if (game_data.game_status == GamesTable.GS_STARTED) {
			int curr_id = 0;
			if (this.current_unit != null) {
				curr_id = this.current_unit.num;
			}
			for (int i=curr_id-1 ; i>=0 ; i--) {
				if (units[i].getSide() == this.game_data.our_side) {
					if (units[i].hasAI() == false) {
						if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
							this.selectOurUnit(units[i], false);
							return;
						}
					}
				}
			}
			for (int i=this.units.length-1 ; i>=0 ; i--) {
				if (units[i].getSide() == this.game_data.our_side) {
					if (units[i].hasAI() == false) {
						if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
							this.selectOurUnit(units[i], false);
							return;
						}
					}
				}
			}
		}
	}


	private void createMapModel() {//HashSet<AppletMapSquare> changed_squares) {
		if (mapdata == null) {
			throw new RuntimeException("mapdata is null!");
		}

		// Update the model
		if (map_model != null) {
			this.map_model.removeFromParent();
			this.map_model = null;
			System.gc(); // What the hell, it might save some memory
		}

		map_model = new MapNode(this);
		this.attachToRootNode(0, map_model, true);
		map_model.updateGeometricState();
	}


	public void reload() {
		AbstractActivity act = Statics.act;

		if (this.current_unit != null) {
			EquipmentData gun = this.current_unit.current_item; 
			if (gun != null) {
				if (gun.major_type == EquipmentTypesTable.ET_GUN) {
					EquipmentData ammo = this.findAmmo(gun.ammo_type_id);
					if (ammo != null) {
						if (this.current_unit.checkAndReduceAPs(this, gun.reload_cost)) {
							while (gun.getAmmoCapacity() > gun.getAmmo() && ammo.getAmmo() > 0) {
								gun.incAmmo();
								ammo.decAmmo();
							}
							this.updateUnitOnServer(this.current_unit);
							this.updateEquipmentOnServer(gun, (byte)-1, (byte)-1);
							this.updateEquipmentOnServer(ammo, (byte)-1, (byte)-1);
							if (ammo.getAmmo() <= 0) {
								ammo.equipmentDestroyed(this);
							}

							this.updateMenu(MM_UNIT_SELECTED);
							this.addToHUD(act.getString(R.string.weapon_reloaded));
						}
					} else {
						this.addToHUD(act.getString(R.string.no_clip_found));
					}
				}
			}
		} else {
			// Trying to reload but no unit selected.
			this.updateMenu(GameModule.MM_NO_UNIT_SELECTED);
		}
	}


	private EquipmentData findAmmo(int type) {
		EquipmentData eq_most_ammo = null;
		for (int i=0 ; i<this.current_unit.items.size() ; i++) {
			EquipmentData eq = this.current_unit.items.get(i);
			if (eq.major_type == EquipmentTypesTable.ET_AMMO_CLIP) {
				if (eq.ammo_type_id == type) {
					if (eq_most_ammo == null) {
						eq_most_ammo = eq;
					} else {
						if (eq_most_ammo.ammo_capacity < eq.ammo_capacity) {
							eq_most_ammo = eq;
						}
					}
					//return eq;
				}
			}
		}
		return eq_most_ammo;
	}


	public void useMedikit(boolean self) {
		AbstractActivity act = Statics.act;

		if (this.current_unit != null) {
			EquipmentData eq = this.current_unit.current_item; 
			if (eq != null) {
				if (eq.major_type == EquipmentTypesTable.ET_MEDIKIT) {
					UnitData target_unit = this.current_unit;
					if (self == false) {
						target_unit = this.current_unit.model.getUnitInFrontOfUnit_MaybeNULL();
						if (target_unit == null) {
							this.addToHUD("Comrade not found!");
							return;
						}
					}
					if (target_unit.getHealth() < target_unit.max_health) {
						if (this.current_unit.checkAndReduceAPs(this, APS_USE_MEDIKIT)) {
							this.current_unit.removeItem(this, eq, false);
							target_unit.incHealth(eq.shot_damage);
							this.addToHUD(act.getString(R.string.medikit_used));
							this.updateUnitOnServer(this.current_unit);
							if (this.current_unit != target_unit) {
								this.updateUnitOnServer(target_unit);
							}
							eq.equipmentDestroyed(this);
							this.updateMenu(MM_UNIT_SELECTED);
						}
					} else {
						this.addToHUD(act.getString(R.string.at_max_health));
					}
				} else {
					this.addToHUD(act.getString(R.string.not_a_medikit));
				}
			} else {
				this.addToHUD(act.getString(R.string.no_unit_selected));
			}
		}
	}


	public void heal() {
		AbstractActivity act = Statics.act;

		if (this.current_unit != null) {
			UnitData target_unit = this.current_unit.model.getUnitInFrontOfUnit_MaybeNULL();
			if (target_unit == null) {
				this.addToHUD("No-one found to heal!");
				return;
			}
			if (target_unit.getHealth() < target_unit.max_health/2) {
				if (this.current_unit.checkAndReduceAPs(this, APS_USE_MEDIKIT)) {
					target_unit.setHealth(target_unit.max_health/2);
					this.updateUnitOnServer(target_unit);
					this.updateUnitOnServer(this.current_unit);
					this.updateMenu(MM_UNIT_SELECTED);
				}
			} else {
				this.addToHUD("That unit cannot be healed any further.");
			}
		} else {
			this.addToHUD(act.getString(R.string.no_unit_selected));
		}
	}


	public void useScanner() {
		if (this.current_unit != null) {
			EquipmentData eq = this.current_unit.current_item; 
			if (eq != null) {
				if (eq.major_type == EquipmentTypesTable.ET_SCANNER) {
					if (this.current_unit.checkAndReduceAPs(this, APS_USE_SCANNER)) {
						this.updateUnitOnServer(this.current_unit);

						// Get distance
						int sd_mapx = -1, sd_mapz = -1;
						EquipmentData stardrive = null;
						for (int i=0 ; i<this.equipment.length ; i++) {
							stardrive = equipment[i];
							if (stardrive.major_type == EquipmentTypesTable.ET_FLAG || stardrive.major_type == EquipmentTypesTable.ET_STARDRIVE) {
								//if (stardrive.destroyed == false) {
								if (stardrive.getUnitID() > 0) {
									UnitData unit = UnitData.GetUnitDataFromID(units, stardrive.getUnitID());
									sd_mapx = unit.getMapX();
									sd_mapz = unit.getMapY();
								} else {
									PointByte p = EquipmentData.GetEquipmentMapSquare(mapdata, stardrive);
									if (p != null) {
										sd_mapx = p.x;
										sd_mapz = p.y;
									}
								}
								break;
							}
						}
						if (stardrive != null && sd_mapx >= 0 && sd_mapz >= 0) {
							int dist = (int)GeometryFuncs.distance(this.current_unit.getMapX(), this.current_unit.getMapY(), sd_mapx, sd_mapz);
							if (stardrive.major_type == EquipmentTypesTable.ET_FLAG) {
								this.addToHUD("The flag is" + " " + dist + " metres away");
							} else if (stardrive.major_type == EquipmentTypesTable.ET_STARDRIVE) {
								this.addToHUD("The stardrive is " + dist + " metres away");
							} else {
								this.addToHUD("Unknown item: " + stardrive.major_type);
							}
						} else {
							this.addToHUD("Nothing found");
						}
						//this.playSound(this.current_unit, SpeechPlayer.EV_USED_MEDIKIT);
					}
				} else {
					this.addToHUD("That is not a scanner!");
				}
			}
		} else {
			this.addToHUD("No unit selected.");
		}
	}


	public void computerDestroyed(AppletMapSquare sq, UnitData shooter) {
		AbstractActivity act = Statics.act;

		if (sq.destroyed != 1) {
			if (shooter == this.current_unit) { // Check it's not Opp Fire or something
				this.addToHUD(act.getString(R.string.computer_destroyed));
				//playSound(shooter, R.raw.);
			}
			sq.destroyed = 1;
			updateMapOnServer(sq, shooter.getSide());
			new CCExplosion(this, sq.x*Statics.SQ_SIZE, sq.y*Statics.SQ_SIZE);
		}
	}


	private static float getAdjustmentForAngle(float source_ang, float target_ang) {
		float ang_diff = GeometryFuncs.GetDiffBetweenAngles(source_ang, target_ang);
		if (ang_diff >= 130) {
			return 1f;
		} else if (ang_diff >= 40) {
			return 0.5f;
		} else {
			return .25f;
		}
	}


	private void scheduleExplosion(EquipmentData eq, UnitData caused_by) {
		//eq.equipmentDestroyed(this);  // NO!  As this remove is from the unit holding it!
		eq.destroyed = true;// Stop it being added again!
		this.explosions_to_show.add(new ExplosionPendingData(eq, caused_by));
	}


	public void showChangeUnitsItemMenu() {
		this.updateMenu(MM_CHANGE_ITEM);
	}


	public void showPickupItemMenu() {
		this.updateMenu(MM_PICKUP_ITEM);
	}


	public void toggleScanner() {
		AbstractActivity act = Statics.act;

		this.getThread().setNextModule(new ScannerModule(act, this));
	}


	public void showSelectShotTypeMenu() {
		AbstractActivity act = Statics.act;

		this.updateMenu(MM_SHOOTING);
		this.createShotLine();
		this.addToHUD(act.getString(R.string.drag_to_aim_shot));
	}


	private void createShotLine() {
		shot_line = new Line("Shot_Line", paint_shot_line_unblocked);
		attachToRootNode_Top(shot_line, true);
		this.updateShotLine(true);

	}


	public void showThrowMenu() {
		AbstractActivity act = Statics.act;

		this.updateMenu(MM_THROW);

		this.createShotLine();

		// Create throw radius
		/*float range = GetMaxRangeSq(this) * Statics.SQ_SIZE;
		this.throw_ranger = new Rectangle("ThrowRange", null, Statics.img_cache.getImage(R.drawable.throw_ranger, range*2, range*2));
		this.throw_ranger.setByLTWH(this.current_unit.model.getWorldCentreX() - range, this.current_unit.model.getWorldCentreY() - range, range*2, range*2);
		this.attachToRootNode(throw_ranger, true);
		this.throw_ranger.updateGeometricState();*/

		this.addToHUD(act.getString(R.string.drag_to_aim_throw));
		if (this.current_unit.current_item != null) {
			if (this.current_unit.current_item.major_type == EquipmentTypesTable.ET_GRENADE || this.current_unit.current_item.major_type == EquipmentTypesTable.ET_SMOKE_GRENADE || this.current_unit.current_item.major_type == EquipmentTypesTable.ET_NERVE_GAS) {
				if (this.current_unit.current_item.primed == false) {
					this.addToHUD(act.getString(R.string.warning) + ": " + act.getString(R.string.grenade_not_primed), true);
				}
			}
		}
	}


	/*public void showTeleportMenu() {
		if (this.current_unit.power_points > 0) {
			this.updateMenu(MM_TELEPORT);

			this.createShotLine();

			this.addToHUD("Drag to aim teleport");
		} else {
			this.addToHUD("You have no Power Points left.");
		}
	}*/


	public void closeCombat(UnitData attacker, UnitData defender) {
		AbstractActivity act = Statics.act;

		waitForPendingUpdateRequests();

		// send CC event to server
		boolean who_can_see[] = this.whichSidesCanSeeUnit(attacker);
		who_can_see[defender.getSide()] = true; // Ensure we tell the defender as presumably they felt the attack
		this.sendEventToServer_CloseCombat(attacker, who_can_see);

		act.playSound(R.raw.punch);

		new CCExplosion(this, defender.model.getWorldCentreX(), defender.model.getWorldCentreY());

		int att_rating = attacker.combat_skill;
		if (attacker.current_item != null) {
			att_rating += attacker.current_item.cc_acc;
		}
		float angle_mult = this.getAdjustmentForAngle(attacker.angle, defender.angle);
		int def_rating = (int)((float)defender.combat_skill * angle_mult); // Adjust by the angle we're attacking from

		int tot = (att_rating*2) - def_rating;

		/*AppletMain.p("-------------------");
		AppletMain.p("Att: " + att_rating);
		AppletMain.p("Def: " + def_rating);
		AppletMain.p("%: " + tot);
		 */
		int rnd = NumberFunctions.rnd(1, 100);

		if (rnd < tot) {
			// Success!
			ccDamage(attacker, defender, angle_mult);
		} else {
			//p("Defender won");
			if (defender.canUnitSee(attacker)) {
				ccDamage(defender, attacker, angle_mult);
			} else {
				//p("Defender cannot see attacker");
			}
		}

	}


	private void ccDamage(UnitData victor, UnitData loser, float angle_mult) {
		int damage = victor.strength;
		if (victor.current_item != null) {
			damage += victor.current_item.cc_damage;
		}
		damage = this.randomizeDamage(damage);
		//p("Rnd Dam: " + damage);
		damage = (int)(damage - (loser.protection * angle_mult));
		//p("Dam after armour: " + damage);
		loser.damage(this, damage, victor, UnitDataComms.FOA_MELEE);
		this.hud.updateStatusBar(); //  Show damage if our unit is harmed
	}


	private boolean isUnitAdjToFriendlyUnitHeCanSee(UnitData unit) {
		for (int i_enemy=0 ; i_enemy<units.length ; i_enemy++) {
			if (units[i_enemy] != unit) {
				if (this.game_data.areSidesFriends(units[i_enemy].getSide(), unit.getSide())) {
					if (units[i_enemy].getStatus() == UnitsTable.ST_DEPLOYED) {
						if (units[i_enemy].model.getMapDistTo(unit.model) <= 1.5f) {
							if (unit.model.canSee(units[i_enemy].model, unit.getAngle(), true)) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}


	public boolean areExplosionsScheduled() {
		return this.explosions_to_show.size() > 0;
	}


	private ExplosionPendingData getNextExplosion() {
		Comparator<ExplosionPendingData> comparator = new Comparator<ExplosionPendingData>()
		{
			@Override
			public int compare(ExplosionPendingData o1, ExplosionPendingData o2)
			{
				return (int)(o1.eq.primed_time - o2.eq.primed_time);
			}

			/*@Override
			public boolean equals(ExplosionPendingData o2)
			{
				return false;
			}*/
		};
		Collections.sort(explosions_to_show, comparator);
		return this.explosions_to_show.get(0);
	}


	private ExplosionPendingData removeNextExplosion() {
		return this.explosions_to_show.remove(0);
	}


	public void unitFinishedMoving(UnitData unit) {
		recalcVisibleEnemiesAndOppFire(true, unit);
		if (this.icon_table.menu_mode == GameModule.MM_PICKUP_ITEM) {
			this.icon_table.menu_mode = GameModule.MM_UNIT_SELECTED;
		}
		checkIfUnitsOnFireAndNearAnotherUnit(unit);

		updateMenu(); // In case there's something to pick up.  Must be before we check for visible enemies though in case of Opp Fire.  NO - MUST BE AFTER SO THE ENEMIES ICONS ARE ADDED TO THE HUD!
	}


	public void checkIfUnitsOnFireAndNearAnotherUnit(UnitData unit) {
		AbstractActivity act = Statics.act;

		try {
			// Check if they are near a unit on fire
			for (int i=0 ; i<this.units.length ; i++) {
				if (units[i] != unit) {
					if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
						if (unit.on_fire != 0 || units[i].on_fire != 0) {
							if (unit.model_type != UnitsTable.MT_ANGEL && units[i].model_type != UnitsTable.MT_ANGEL) {
								if (NumberFunctions.rnd(1, 3) == 1) {
									float dist = unit.getDistanceTo(units[i]);
									if (dist <= 1.1f) { // Don't catch fire diagonally
										unit.on_fire = 1;
										this.updateUnitOnServer(unit);
										units[i].on_fire = 1;
										this.updateUnitOnServer(units[i]);
										act.playSound(R.raw.fire);
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ErrorReporter.getInstance().handleSilentException(ex);
		}

	}

	public boolean isObjectUpdating(IProcessable i) {
		return this.objects_for_processing.contains(i);
	}


	private boolean canUnitSeeEquipment(UnitData unit, EquipmentData eq) {
		if (eq.model == null) { // Shouldn't happen unless equipment is corrupt, i.e. not carried but not on map square
			return false;
		}
		return (unit.model.canSee(eq.model, unit.getAngle(), false));
	}


	public boolean canUnitSeeUnit(UnitData friend, UnitData enemy, boolean do_units_block) {
		if (friend.getStatus() == UnitsTable.ST_DEPLOYED && enemy.model != null) {
			return friend.model.canSee(enemy.model, friend.getAngle(), do_units_block);
		}
		return false;
	}


	private void markUnitAsSeen(UnitData unit, UnitData seen_by) {
		AbstractActivity act = Statics.act;

		updateVisibleUnitOnServer(unit, seen_by); // Need to call this each time to tell opponents which enemies they can see! 

		if (unit.has_been_seen == false) { // First time they have been seen
			boolean seen_by_sides[] = {false, false, false, false, false};
			seen_by_sides[seen_by.getSide()] = true;
			this.sendEventToServer_UnitMoved(unit, seen_by_sides);
		}

		unit.has_been_seen = true;
		seen_by.setCanSee(unit);
		if (unit.getSide() != game_data.our_side) {
			if (unit.model != null) {
				unit.model.visible = true;
			}
			if (icon_table.visible_enemy_icons.contains(unit.icon) == false) {
				icon_table.visible_enemy_icons.add(unit.icon);
				if (this.orig_visible_enemy_icons.contains(unit.icon) == false) {
					act.playSound(R.raw.enemyalert);
				}
			}
		}
	}


	public void makeOppFireShot(UnitData shooter, UnitData target) {
		Ray targetting_ray = new Ray();
		targetting_ray.origin = shooter.model.getWorldCentre_CreatesNew();
		targetting_ray.direction = target.model.getWorldCentre_CreatesNew().copy().subtract(shooter.model.getWorldCentre_CreatesNew()).normalize();
		this.shoot(shooter, getSnapShotAccuracy(shooter) - OPP_FIRE_ACC_REDUCTION, shooter.current_item.snap_shot_aps, false, targetting_ray);
	}


	private void deselectUnitIfCurrent(UnitData unit) {
		if (this.current_unit == unit) {
			this.current_unit = null;
			this.removeHighlighter();
			this.updateMenu(MM_NO_UNIT_SELECTED);
		}
	}


	public void lookAtEnemyUnit(UnitData unit) {
		//addToHUD(act.getString(R.string.showing_enemy) + " " + unit.name);
		//this.current_unit = null;  No!
		//this.updateMenu(MM_NO_UNIT_SELECTED);
		this.showEnemyDetails(unit.model);
		this.root_cam.lookAt(unit.model, false);
		this.removeHighlighter();
		if (Statics.USE_NEW_MOVEMENT_ICONS == 1) {
			hud.setMovementIconsVisible(false);
		}
	}


	private void updateShotLine(boolean check_length) {
		this.shot_line.setXYXY(this.current_unit.model.getWorldCentreX(), this.current_unit.model.getWorldCentreY(), this.root_cam.getActualCentre().x, this.root_cam.getActualCentre().y);
		this.shot_line.updateGeometricState();

		if (check_shot_line_int.hitInterval()) {
			shot_line.setPaint(paint_shot_line_unblocked); // Default
			MyPickResults results = new MyPickResults(null, null);
			Ray ray = new Ray(this.shot_line);
			mapdata.canSee(ray, results, false, false);
			if (results.size() > 0) {
				for (int i=0 ; i<results.getNumber() ; i++) {
					PickData o = results.getPickData(i);
					// Is the target further than our distance?
					if (o.getDistance() <= shot_line.getLength()) { // has it hit something?
						shot_line.setPaint(paint_shot_line_blocked);
						break;
					}
				}
			}
		}

		if (check_length && this.shot_line.getLength() < Statics.SQ_SIZE/2) {
			// Move the camera
			Point p = AbstractUnit.GetDirFromAngle(this.current_unit.angle);
			this.root_cam.moveCam(p.x * Statics.SQ_SIZE*2, p.y * Statics.SQ_SIZE*2, false);
		}

	}


	public void throwItem() {
		AbstractActivity act = Statics.act;

		if (this.current_unit.model != null && this.shot_line != null) {
			float selected_dist_sq = this.shot_line.getLength() / Statics.SQ_SIZE;
			float max_range_sq = GetMaxRangeSq(this);// * Statics.SQ_SIZE;
			if (selected_dist_sq > max_range_sq) {
				selected_dist_sq = max_range_sq;
			}
			if (this.current_unit.current_item != null) {
				// Check they are shooting in the direction they are facing!
				Ray targetting_ray = new Ray(shot_line);
				float ang = targetting_ray.getAngle();// GeometryFuncs.GetAngleOfRayAlongYAxis(this.targetting_ray);
				float diff = GeometryFuncs.GetDiffBetweenAngles(ang, this.current_unit.getAngle());
				if (diff < Statics.VIEW_ANGLE) {
					if (this.current_unit.checkAndReduceAPs(this, APS_THROW)) {
						act.playSound(R.raw.throwitem);
						this.hud.appendText(act.getString(R.string.item_thrown));					
						EquipmentData obj_thrown = this.current_unit.current_item;
						this.current_unit.removeCurrentItem(this);

						MyPickResults results = new MyPickResults(this.current_unit.model, null);
						results.clear();

						// Adjust by accuracy angle
						float ang_rnd = (100-this.current_unit.combat_skill) / 12;
						//int angle = Functions.rnd(-3, 3);
						float angle = NumberFunctions.rndFloat(-ang_rnd, ang_rnd);
						targetting_ray.setDirection(GeometryFuncs.AdjustVectorByAngle(targetting_ray.getDirection(), angle));

						// Adjust distance by random amount
						float dist_rnd = (100-this.current_unit.combat_skill) / 220f;
						selected_dist_sq = (selected_dist_sq * NumberFunctions.rndFloat(1f-dist_rnd, 1f+dist_rnd));
						p("Dist changed to " + selected_dist_sq);

						float selected_dist_px = selected_dist_sq * Statics.SQ_SIZE;

						boolean object_landed = false;
						root_node.findPick(targetting_ray, results);
						// Check for walls
						mapdata.canSee(targetting_ray, results, true, false);
						results.sort();
						if (results.getNumber() > 0) {
							for (int i=0 ; i<results.getNumber() ; i++) {
								PickData o = results.getPickData(i);
								// Is the target further than our distance?
								if (o.getDistance() <= selected_dist_px) { // has it hit something?
									GameObject target = results.getGameObject(i);
									if (target instanceof AbstractUnit) {
										AbstractUnit catcher = (AbstractUnit)target;
										selected_dist_px = o.getDistance();
										boolean caught = false;
										if (catcher.unit_data.current_item == null && catcher.unit_data.can_use_equipment) { // Can it catch it?
											// Can the catcher see the thrower?
											if (this.canUnitSeeUnit(catcher.unit_data, this.current_unit, true)) {
												if (catcher.unit_data.checkAndReduceAPs(this, APS_CATCH)) {
													catcher.unit_data.current_item = obj_thrown;
													catcher.unit_data.items.add(obj_thrown);
													this.updateUnitOnServer(catcher.unit_data);
													obj_thrown.setUnitID(catcher.unit_data.unitid);
													this.updateEquipmentOnServer(obj_thrown, (byte)-1, (byte)-1);
													this.addToHUD(act.getString(R.string.caught_by) + " " + catcher.unit_data.name);
													caught = true;
												}
											}
										} 
										if (caught == false) { // Catcher is unable to catch - Dropped at the feet
											this.addToHUD(act.getString(R.string.has_hit) + " " + catcher.unit_data.name + ".");
											this.putEquipmentOnFloor(obj_thrown, catcher.unit_data.getMapX(), catcher.unit_data.getMapY());
										}
										object_landed = true;
										break;
									} else if (target.can_be_shot && target.getChanceofNotHitting() <= 0) {
										selected_dist_px = o.getDistance();// - (0.5f * Statics.SQ_SIZE); // In case it hits door, move back.  But not too far! (was -1f)
										objectThrownToTheFloor(obj_thrown, selected_dist_px, targetting_ray);
										object_landed = true;
										break;
									}
								} else { // landed in middle of room
									break;
								}

							} // End of loop
						}
						if (object_landed == false) { // None of the collisions were valid
							objectThrownToTheFloor(obj_thrown, selected_dist_px, targetting_ray);

						}

						// Send message
						boolean who_can_see[] = this.whichSidesCanSeeUnit(this.current_unit);
						this.sendEventToServer_ItemThrown(this.current_unit, (int)ang, (int)selected_dist_sq, who_can_see);

						this.shot_line.removeFromParent();
						shot_line = null;

						// Launch bullet
						MyPointF vdiff = targetting_ray.getDirection().multiply(selected_dist_px);
						MyPointF end = targetting_ray.getOrigin().add(vdiff);
						if (obj_thrown.equipment_type_id == EquipmentTypesTable.ET_GRENADE) {
							new Bullet(this, this.current_unit.model.getWorldCentre_CreatesNew(), end, false, true, R.drawable.grenade, 1, Statics.BULLET_SIZE*4);
						} else {
							new Bullet(this, this.current_unit.model.getWorldCentre_CreatesNew(), end, false, true, R.drawable.crate, 1, Statics.BULLET_SIZE*2);
						}

						this.updateMenu(MM_UNIT_SELECTED);
					} else {
						this.addToHUD(act.getString(R.string.not_enough_aps));
					}
				} else {
					this.addToHUD(act.getString(R.string.not_in_view));
					if (help_mode_on) {
						this.addToHUD(act.getString(R.string.white_line_help));
					}
				}
			} else {
				this.addToHUD(act.getString(R.string.unit_carrying_nothing));
			}
		}
	}


	/*public void teleport() {
		AbstractActivity act = Statics.act;

		if (this.current_unit.model != null && this.shot_line != null) {
			if (this.current_unit.power_points > 0) {
				float selected_dist_sq = this.shot_line.getLength() / Statics.SQ_SIZE;
				// Check they are shooting in the direction they are facing!
				Ray targetting_ray = new Ray(shot_line);
				if (this.current_unit.checkAndReduceAPs(this, APS_TELEPORT)) {  
					this.hud.appendText("Teleporting...");

					float selected_dist_px = selected_dist_sq * Statics.SQ_SIZE;

					// Send message
					boolean who_can_see[] = this.whichSidesCanSeeUnit(this.current_unit);
					this.sendEventToServer_UnitTeleported(this.current_unit, who_can_see);

					this.current_unit.power_points--;
					unitTeleportToEmptySquare(this.current_unit, selected_dist_px, targetting_ray);

					this.shot_line.removeFromParent();
					shot_line = null;

					// Launch bullet
					MyPointF vdiff = targetting_ray.getDirection().multiply(selected_dist_px);
					MyPointF end = targetting_ray.getOrigin().add(vdiff);
					new Bullet(this, this.current_unit.model.getWorldCentre_CreatesNew(), end, false, true, R.drawable.teleport_effect, 1, Statics.BULLET_SIZE*2);

					this.updateMenu(MM_UNIT_SELECTED);
				} else {
					this.addToHUD(act.getString(R.string.not_enough_aps));
				}
			} else {
				this.addToHUD("You have no Power Points left.");
			}
		}
	}*/


	/*public void teleport_OLD() {
		if (this.current_unit.model != null && this.shot_line != null) {
			if (this.current_unit.power_points > 0) {
				float selected_dist_sq = this.shot_line.getLength() / Statics.SQ_SIZE;
				float max_range_sq = GetMaxTeleportRangeSq();
				if (selected_dist_sq > max_range_sq) {
					selected_dist_sq = max_range_sq;
				}
				// Check they are shooting in the direction they are facing!
				Ray targetting_ray = new Ray(shot_line);
				float ang = targetting_ray.getAngle();// GeometryFuncs.GetAngleOfRayAlongYAxis(this.targetting_ray);
				float diff = GeometryFuncs.GetDiffBetweenAngles(ang, this.current_unit.getAngle());
				if (diff < Statics.VIEW_ANGLE) {
					if (this.current_unit.checkAndReduceAPs(this, APS_TELEPORT, true)) {  
						this.hud.appendText("Teleporting...");

						MyPickResults results = new MyPickResults(this.current_unit.model, null);
						results.clear();

						float selected_dist_px = selected_dist_sq * Statics.SQ_SIZE;

						root_node.findPick(targetting_ray, results);
						// Check for walls
						mapdata.canSee(targetting_ray, results, true, false);
						results.sort();
						if (results.getNumber() > 0) {
							for (int i=0 ; i<results.getNumber() ; i++) {
								PickData o = results.getPickData(i);
								// Is the target further away than our max distance?
								if (o.getDistance() <= selected_dist_px) { // has it hit something?
									GameObject target = results.getGameObject(i);
									if (target instanceof AbstractUnit || (target.can_be_shot && target.getChanceofNotHitting() <= 0)) {
										selected_dist_px = o.getDistance();// - (0.5f * Statics.SQ_SIZE); // In case it hits door, move back.  But not too far! (was -1f)
										//objectThrownToTheFloor(obj_thrown, selected_dist_px, targetting_ray);
										//object_landed = true;
										break;
									}
								} else { // landed in middle of room
									break;
								}

							} // End of loop
						}
						// Send message
						boolean who_can_see[] = this.whichSidesCanSeeUnit(this.current_unit);
						this.sendEventToServer_UnitTeleported(this.current_unit, who_can_see);

						this.current_unit.power_points--;
						unitTeleportToEmptySquare(this.current_unit, selected_dist_px, targetting_ray);

						this.shot_line.removeFromParent();
						shot_line = null;

						// Launch bullet
						MyPointF vdiff = targetting_ray.getDirection().multiply(selected_dist_px);
						MyPointF end = targetting_ray.getOrigin().add(vdiff);
						new Bullet(this, this.current_unit.model.getWorldCentre_CreatesNew(), end, false, true, R.drawable.teleport_effect, 1, Statics.BULLET_SIZE*2);

						this.updateMenu(MM_UNIT_SELECTED);
					} else {
						this.addToHUD(act.getString(R.string.not_enough_aps));
					}
				} else {
					this.addToHUD(act.getString(R.string.not_in_view));
					if (help_mode_on) {
						this.addToHUD(act.getString(R.string.white_line_help));
					}
				}
			} else {
				this.addToHUD("You have no Power Points left.");
			}
		}
	}*/


	private void objectThrownToTheFloor(EquipmentData obj_thrown, float selected_dist_px, Ray targetting_ray) {
		int x, y;
		while (true) {
			MyPointF pos_diff = targetting_ray.getDirection().multiply(selected_dist_px);
			MyPointF end_pos = targetting_ray.getOrigin().add(pos_diff);
			x = (int)end_pos.x;
			y = (int)end_pos.y;
			AppletMapSquare sq = this.mapdata.getSqFromPixels_MaybeNULL(x, y);
			if (sq != null) { // In case it's off the edge of the map
				if (sq.major_type == MapDataTable.MT_FLOOR) {
					if (sq.door_type <= 0 || sq.door_open) {
						break;
					}
				}
			}
			selected_dist_px -= (Statics.SQ_SIZE * 0.5f);
		}

		this.putEquipmentOnFloor(obj_thrown, (byte)(x/Statics.SQ_SIZE), (byte)(y/Statics.SQ_SIZE));

	}


	private void putEquipmentOnFloor(EquipmentData obj_thrown, int mapx, int mapz) {
		this.mapdata.getSq_MaybeNULL(mapx, mapz).addEquipment(obj_thrown);
		obj_thrown.seen_by_side[this.current_unit.getSide()] = 1;
		this.updateEquipmentOnServer(obj_thrown, mapx, mapz);
		obj_thrown.model = EquipmentModel.Factory(this, obj_thrown, mapx, mapz);
		this.attachToRootNode(obj_thrown.model, true);

	}


	private void unitTeleportToEmptySquare(UnitData unit, float selected_dist_px, Ray targetting_ray) {
		int x, y;
		boolean found = false;
		while (true) {
			MyPointF pos_diff = targetting_ray.getDirection().multiply(selected_dist_px);
			MyPointF end_pos = targetting_ray.getOrigin().add(pos_diff);
			x = (int)end_pos.x;
			y = (int)end_pos.y;
			AppletMapSquare sq = this.mapdata.getSqFromPixels_MaybeNULL(x, y);
			if (sq != null) { // In case it's off the edge of the map
				if (sq.major_type == MapDataTable.MT_FLOOR) {
					if (sq.door_type <= 0 || sq.door_open) {
						if (this.getUnitAt(x, y) == null) {
							found = true;
							break;
						}
					}
				}
			}
			selected_dist_px -= (Statics.SQ_SIZE * 0.5f); // Move back and try again
		}

		if (found) {
			unit.setTargetMapLocation(this, (int)(x/Statics.SQ_SIZE), (int)(y/Statics.SQ_SIZE), this.mapdata.getSq_MaybeNULL(x, y));
			this.updateUnitOnServer(unit);
			this.addToHUD("Unit teleported.");
		} else {
			this.addToHUD("No space to teleport!");
		}

	}


	/*public void createOrAbsorb() {
		Point p = AbstractUnit.GetDirFromAngle(this.current_unit.getAngle());
		Point new_pos = new Point(current_unit.getMapX() + p.x, current_unit.getMapY() + p.y);
		AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(new_pos.x, new_pos.y);
		if (sq != null) {
			UnitData opponent = getUnitAt(new_pos.x, new_pos.y);
			if (opponent == null) {
				if (sq.major_type == MapDataTable.MT_WALL) {
					if (this.current_unit.checkAndReduceAPs(this, APS_CREATE_ABSORB)) {
						// Destroy wall
						this.current_unit.power_points++;
						this.updateUnitOnServer(this.current_unit);

						sq.major_type = MapDataTable.MT_FLOOR;
						sq.texture_code = TextureStateCache.GetRandomRubbleTex();//TEX_RUBBLE;
						this.updateMapOnServer(sq, this.current_unit.getSide());
						this.createMapModel();
						this.sendEventToServer_MapSquareChanged(new_pos.x, new_pos.y, MapDataTable.MT_WALL, sq.major_type, (int)sq.texture_code, this.current_unit.getSide());
						this.addToHUD("Wall absorbed.");
					}
				} else if (sq.major_type == MapDataTable.MT_FLOOR && sq.door_type <= 0) {
					if (this.current_unit.power_points > 0) {
						if (this.current_unit.checkAndReduceAPs(this, APS_CREATE_ABSORB)) {
							// Build wall
							this.current_unit.power_points--;
							this.updateUnitOnServer(this.current_unit);

							sq.major_type = MapDataTable.MT_WALL;
							sq.texture_code = TextureStateCache.TEX_CELLS3;//.GetRandomRubbleTex();
							this.updateMapOnServer(sq, this.current_unit.getSide());
							this.createMapModel();
							this.sendEventToServer_MapSquareChanged(new_pos.x, new_pos.y, MapDataTable.MT_FLOOR, sq.major_type, (int)sq.texture_code, this.current_unit.getSide());
							this.addToHUD("Wall created.");
						}
					} else {
						this.addToHUD("You don't have any power points.");
					}
				}
			} else {
				this.addToHUD("There is an alien in the way.");
			}
		}
	}*/


	public void buildStructure(int tex_type) {
		AbstractActivity act = Statics.act;

		Point p = AbstractUnit.GetDirFromAngle(this.current_unit.getAngle());
		Point new_pos = new Point(current_unit.getMapX() + p.x, current_unit.getMapY() + p.y);
		AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(new_pos.x, new_pos.y);
		if (sq != null) {
			// check we're an engineer
			if (this.current_unit.unit_type == UnitsTable.UT_ENGINEER || this.current_unit.model_type == UnitsTable.MT_QUEEN_ALIEN) {
				// Check the area is clear
				UnitData opponent = getUnitAt(new_pos.x, new_pos.y);
				if (opponent == null) { 
					if (sq.major_type == MapDataTable.MT_FLOOR && sq.door_type <= 0) {
						// Check there's no other structure:-
						int test = SelectStructureToBuildModule.GetResPtsForDismantledStructure(sq);
						if (test <= 1) {
							int cost = SelectStructureToBuildModule.GetRPCostToBuildStructure(tex_type);
							if (this.game_data.getResPointsForOurSide() >= cost || Statics.DEBUG) {
								int build_aps = SelectStructureToBuildModule.GetAPCostToBuildStructure(tex_type); // APS_BUILD;
								/*if (this.current_unit.unit_type == UnitsTable.MT_QUEEN_ALIEN) {
									build_aps = build_aps / 2;
								}*/
								if (this.current_unit.checkAndReduceAPs(this, build_aps)) {
									act.playSound(R.raw.construction);
									this.updateUnitOnServer(this.current_unit);

									if (tex_type == TextureStateCache.TEX_MOONBASE_BLUE) { // Walls are walls, other types of installation are floors.
										sq.major_type = MapDataTable.MT_WALL;
										int tc = mapdata.getAdjacentTexCode(sq, MapDataTable.MT_WALL);
										if (tc <= 0) {
											tc = TextureStateCache.TEX_MOONBASE_BLUE;
										}
										sq.texture_code = tc;
									} else {
										sq.major_type = MapDataTable.MT_FLOOR;
										sq.texture_code = tex_type;
									}
									sq.owner_side = this.current_unit.getSide();
									sq.raised_texture_code = 0;
									sq.scenery_code = 0;
									sq.destroyed = 0;
									this.updateMapOnServer(sq, this.current_unit.getSide());
									this.createMapModel();

									this.sendEventToServer_MapSquareChanged(new_pos.x, new_pos.y, MapDataTable.MT_FLOOR, sq.major_type, tex_type, this.current_unit.getSide());
									this.addToHUD(SelectStructureToBuildModule.TexToString(tex_type) + " built.");

									this.game_data.res_points[game_data.our_side] -= cost;
									setResPointsForSideOnServer(game_data.our_side, this.game_data.res_points[game_data.our_side]);

								}
							} else {
								this.addToHUD("You don't have enough Resource Points (" + cost + " > " + this.game_data.getResPointsForOurSide() + ")");
							}
						} else {
							this.addToHUD("There is already a structure there.");
						}
					} else {
						this.addToHUD("You can only build on floor squares which do not have a door.");
					}
				} else {
					this.addToHUD("There is a unit in the way.");
				}
			} else {
				this.addToHUD("This unit is not an engineer.");
			}
		}

	}


	public void dismantleStructure() {
		AbstractActivity act = Statics.act;

		Point p = AbstractUnit.GetDirFromAngle(this.current_unit.getAngle());
		Point new_pos = new Point(current_unit.getMapX() + p.x, current_unit.getMapY() + p.y);
		AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(new_pos.x, new_pos.y);
		if (sq != null) { 
			boolean engineer = (this.current_unit.unit_type == UnitsTable.UT_ENGINEER);
			int value = SelectStructureToBuildModule.GetResPtsForDismantledStructure(sq);
			if (value > 0) {
				//int ap_cost = engineer ? APS_DISMANTLE : APS_DISMANTLE/2;
				if (this.current_unit.checkAndReduceAPs(this, APS_DISMANTLE)) {
					if (engineer) {
						this.game_data.res_points[game_data.our_side] += value;
						setResPointsForSideOnServer(game_data.our_side, this.game_data.res_points[game_data.our_side]);
					}
					act.playSound(R.raw.dismantle);
					this.updateUnitOnServer(this.current_unit);

					// Get tex code
					int tc = mapdata.getAdjacentTexCode(sq, MapDataTable.MT_FLOOR);
					if (tc <= 0) {
						tc = TextureStateCache.TEX_CORRIDOR1;
					}
					sq.texture_code = tc;

					sq.major_type = MapDataTable.MT_FLOOR;
					sq.texture_code = tc;//TextureStateCache.TEX_CORRIDOR1;
					sq.door_type = 0;
					sq.door_open = false;
					sq.raised_texture_code = 0;
					sq.scenery_code = 0;
					sq.destroyed = 1; // Must make it destroyed otherwise playback is wrong as it's not shown from the start
					this.updateMapOnServer(sq, this.current_unit.getSide());

					this.createMapModel();
					this.sendEventToServer_MapSquareChanged(new_pos.x, new_pos.y, MapDataTable.MT_WALL, sq.major_type, sq.texture_code, this.current_unit.getSide());
					this.addToHUD("Installation dismantled.");
					
					new CCExplosion(this, sq.x*Statics.SQ_SIZE, sq.y*Statics.SQ_SIZE);

					recalcVisibleEnemiesAndOppFire(true, null);

				}
			} else {
				this.addToHUD("There is no structure there.");
			}
		}

	}


	public void primeGrenade(byte turns) {
		AbstractActivity act = Statics.act;

		EquipmentData eq = this.current_unit.current_item;
		if (eq.primed == false) {
			if (this.current_unit.checkAndReduceAPs(this, APS_PRIME)) {
				eq.primed = true;
				eq.explode_turns = turns;
				eq.primed_time = System.currentTimeMillis();
				//eq.caused_by_side = this.current_unit.side;
				eq.last_unit_to_touch = this.current_unit.unitid;
				this.updateEquipmentOnServer(eq, (byte)-1, (byte)-1);
				this.updateUnitOnServer(this.current_unit);
				this.addToHUD(act.getString(R.string.grenade_primed, "" + turns));
				//this.playSound(this.current_unit, R.raw.p);

				boolean who_can_see[] = whichSidesCanSeeUnit(this.current_unit);
				this.sendEventToServer_GrenadePrimed(this.current_unit, turns, who_can_see);
			}
		} else {
			this.addToHUD(act.getString(R.string.already_primed));
		}
		this.updateMenu(MM_UNIT_SELECTED);
	}


	public void stopGameDueToError() {
		this.client_mode = APP_CRASHED;
	}


	private void toggleGrid() {
		show_grid = !show_grid;
	}


	public void showEquipmentMenu() {
		this.updateMenu(MM_EQUIPMENT_MENU);
	}


	public static int GetMaxRangeSq(GameModule game) {
		if (game.getCurrentUnit() != null) {
			int dist = (game.getCurrentUnit().strength - game.getCurrentUnit().current_item.weight);
			if (dist < 3) {
				dist = 3;
			}
			return dist;
		} else {
			return 0;
		}
	}


	private void startTutorial() {
		AbstractActivity act = Statics.act;

		this.show_tutorial = !this.show_tutorial;
		if (this.show_tutorial) {
			tutorial = new Tutorial(this);
			this.addToHUD(act.getString(R.string.tutorial_0));
		}
		this.updateMenu(); // To show "Next Tutorial"
	}


	public void nextTutorial() {
		this.addToHUD("---");
		this.addToHUD(this.tutorial.getText());
	}


	private void showGameLog() {
		AbstractActivity act = Statics.act;

		if (this.load_log != null) {
			if (this.load_log.isAlive() == false) {
				// Check it loaded correctly
				if (load_log.cache == null) {
					loadTheLog();
					this.hud.appendText("Please wait, there was a problem loading the log...");
				} else {
					game_log_module = new GameLogModule(act, this, load_log.cache);
					this.getThread().setNextModule(game_log_module);
				}
			} else {
				this.hud.appendText("Please wait, log not loaded yet...");
			}
		}
	}


	private void showAdvancedScanner() {
		AbstractActivity act = Statics.act;

		try {
			String url = Statics.URL_FOR_CLIENT + "/androidcomm/ViewScanner.cls?android_login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&android_pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD) + "&gameid=" + this.game_data.game_id; 
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
		}

	}


	private void showGameChat() {
		AbstractActivity act = Statics.act;

		try {
			String url = Statics.URL_FOR_CLIENT + "/dsr/forums/ForumPostingsPage.cls?topic=" + game_data.forum_id + "&android_login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&android_pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD);
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
		}

	}


	private void showMissionDesc() {
		AbstractActivity act = Statics.act;

		try {
			String url = Statics.URL_FOR_CLIENT + "/dsr/missiondescriptions.cls?type=" + game_data.mission_type + "&android_login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&android_pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD); 
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			act.getBaseContext().startActivity(intent);
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
		}

	}


	public boolean menuPressed(Button b) {
		AbstractActivity act = Statics.act;

		IOFunctions.Vibrate(act.getBaseContext(), Statics.VIBRATE_LEN);

		this.submenu.instaHide();
		int cmd = NumberFunctions.ParseInt(b.getActionCommand());
		switch (cmd) {
		case MENU_ADVANCED_SCANNER:
			showAdvancedScanner();
			return true;
		case MENU_GRID:
			toggleGrid();
			return true;
		case MENU_GAME_LOG:
			showGameLog();
			return true;
		case MENU_MISSION_DESC:
			showMissionDesc();
			return true;
		case MENU_START_TUTORIAL:
			startTutorial();
			this.updateMenu(); // To show "Next Tutorial" if started
			return true;
		case MENU_GAME_CHAT:
			showGameChat();
			return true;
		case MENU_UNIT_STATS:
			this.getThread().setNextModule(new AllUnitStatsModule(act, this, game_data, new ArrayList<UnitData>(Arrays.asList(units)), equipment));
			return true;
		case MENU_GAME_DETAILS:
			this.getThread().setNextModule(new CurrentGameDetailsModule(act, this, game_data, true));
			return true;
		case MENU_MSG_OPPONENT:
			this.getThread().setNextModule(new SendMessageModule(act, this, game_data.getOpponentsName(), "Game " + this.game_data.game_id + ": " + game_data.mission_name + ", " + game_data.getPlayersNames(), ""));
			return true;
		case MENU_MSG_COMRADE:
			this.getThread().setNextModule(new SendMessageModule(act, this, game_data.getComradesName(), "Game " + this.game_data.game_id + ": " + game_data.mission_name + ", " + game_data.getPlayersNames(), ""));
			return true;
		case MENU_INSTRUCTIONS:
			this.getThread().setNextModule(new InstructionsModule(act, this));
			return true;
		default:
			throw new RuntimeException("Unknown menu item: " + cmd) ;
		}
	}


	@Override
	protected boolean returnTo() {
		AbstractActivity act = Statics.act;

		this.update_server_thread.stop_now = true;
		//Statics.ResumeMusic(this.act.getBaseContext());
		boolean autoselect = false; //Statics.FULL_VERSION == false;
		super.getThread().setNextModule(new GetGamesModule(act, autoselect));
		return true;

	}


	/*private void calcDistanceToClosestEnemy() {
		if (this.current_unit != null) {
			float max_dist_px = 9999f;
			boolean found = false;
			for (int i_enemy=0 ; i_enemy<this.units.length ; i_enemy++) {
				if (this.game_data.areSidesFriends(units[i_enemy].getSide(), game_data.our_side) == false) {
					if (units[i_enemy].getStatus() == UnitsTable.ST_DEPLOYED) {
						float dist_px = this.current_unit.getDistanceTo(units[i_enemy]);
						if (dist_px < max_dist_px) {
							found = true;
							max_dist_px = dist_px;
						}
					}
				}
			}
			if (found) {
				this.addToHUD("Distance to closest enemy:" + (int)(max_dist_px/Statics.SQ_SIZE));
			}
		}
	}*/


	public void undoDeployment() {
		AbstractActivity act = Statics.act;

		boolean found = false;
		// Choose the previously deployed unit
		for (int i=this.units.length-1 ; i>= 0 ; i--) {
			if (units[i].getSide() == game_data.our_side) {
				if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
					if (units_deployed.contains(i)) { // Have we deployed them?
						next_to_deploy = i;
						found = true;
						break;
					}
				}
			}
		}

		if (found) {
			UnitData unit = units[next_to_deploy];
			// Deploy unit!
			root_node.detachChild(unit.model);
			unit.setStatus(UnitsTable.ST_AWAITING_DEPLOYMENT);
			this.updateUnitOnServer(unit);
			this.addToHUD(act.getString(R.string.unit_undeployed));
			this.getNextUnitToDeploy();
			this.next_deploy_dir.add(unit.angle);
		} else {
			this.addToHUD("No unit can be undeployed");
		}
	}


	public AppletMapSquare findEndStairsSquare(AppletMapSquare sq_start) {
		// Squares must be on the same X or Y coord

		int looking_for = TextureStateCache.TEX_STAIRS_DOWN;
		if (sq_start.texture_code == TextureStateCache.TEX_STAIRS_DOWN) {
			looking_for = TextureStateCache.TEX_STAIRS_UP;
		}
		double closest_dist = 9999;
		int closest_x = -1;
		int closest_y = -1;

		// Search X
		int x = sq_start.x;
		for (int y=0 ; y<mapdata.getMapHeight() ; y++) {
			AppletMapSquare sq = mapdata.getSq_MaybeNULL(x, y);
			//if (sq != null) {
			if (sq.texture_code == looking_for) {
				double dist = GeometryFuncs.distance(sq_start.x, sq_start.y, sq.x, sq.y);
				if (dist < closest_dist) {
					closest_dist = dist;
					closest_x = sq.x;
					closest_y = sq.y;
				}
			}
		}
		// Search Y
		int y = sq_start.y;
		for (x=0 ; x<mapdata.getMapWidth() ; x++) {
			AppletMapSquare sq = mapdata.getSq_MaybeNULL(x, y);
			if (sq.texture_code == looking_for) {
				double dist = GeometryFuncs.distance(sq_start.x, sq_start.y, sq.x, sq.y);
				if (dist < closest_dist) {
					closest_dist = dist;
					closest_x = sq.x;
					closest_y = sq.y;
				}
			}
		}


		if (closest_x >= 0) {
			return mapdata.getSq_MaybeNULL(closest_x, closest_y);
		} else {
			return null;
		}

	}


	public void showBuildStructureMenu() {
		AbstractActivity act = Statics.act;

		this.getThread().setNextModule(new SelectStructureToBuildModule(act, this));
	}


	public void layEgg() {
		this.buildStructure(TextureStateCache.TEX_ALIEN_COLONY);
	}


	private boolean isThereAnAISide() {
		for (int i=0 ; i<this.units.length ; i++) {
			if (units[i].hasAI()) {
				return true;
			}
		}
		return false;
	}


	private void playAmbience() {
		AbstractActivity act = Statics.act;

		if (this.is_april_fools_day) {
			if (NumberFunctions.rnd(1, 5) == 1) {
				act.playSound(R.raw.fart);
			}
		}

		if (game_data.mission_type == 1 || game_data.mission_type == 3 || game_data.mission_type == 21 || game_data.mission_type == 29 || game_data.mission_type == 39 || game_data.mission_type == 45 || game_data.mission_type == 46 || game_data.mission_type == 50 || game_data.mission_type == 61 || game_data.mission_type == 66 || game_data.mission_type == 84 || game_data.mission_type == 85 || game_data.mission_type == 90) {
			act.playSound(R.raw.outside_ambience);
		} else if (game_data.mission_type == 2 || game_data.mission_type == 5 || game_data.mission_type == 12 || game_data.mission_type == 23 || game_data.mission_type == 30 || game_data.mission_type == 31 || game_data.mission_type == 40 || game_data.mission_type == 44 || game_data.mission_type == 51 || game_data.mission_type == 54 || game_data.mission_type == 65 || game_data.mission_type == 74 || game_data.mission_type == 80 || game_data.mission_type == 81) {
			act.playSound(R.raw.computerambience);
		} else if (game_data.mission_type == 7 || game_data.mission_type == 18 || game_data.mission_type == 27 || game_data.mission_type == 43) {
			act.playSound(R.raw.mines_ambience);
		} else if (game_data.mission_type == 82 || game_data.mission_type == 87) {
			act.playSound(R.raw.reanim_ambience);
		} else if (game_data.mission_type == 6 || game_data.mission_type == 24 || game_data.mission_type == 32 || game_data.mission_type == 42 || game_data.mission_type == 47 || game_data.mission_type == 48 || game_data.mission_type == 52 || game_data.mission_type == 62 || game_data.mission_type == 73) {
			act.playSound(R.raw.insectsambience);
		} else if (game_data.mission_type == 14 || game_data.mission_type == 60) {
			act.playSound(R.raw.horrorambience);
		} else {
			act.playSound(R.raw.space_ambience);
		}
	}


	public boolean playMusic() {
		// Override if required
		return false;

	}


	@Override
	public void rootCamMoved(float offx, float offy) {
		if (this.current_unit != null) {
			if (Statics.USE_NEW_MOVEMENT_ICONS == 1) {
				float dist = GeometryFuncs.distance(current_unit.model.getWorldX(), current_unit.model.getWorldY(), this.root_cam.left+(Statics.SCREEN_WIDTH/2), root_cam.top + (Statics.SCREEN_HEIGHT/2));
				if (dist > Statics.SQ_SIZE*2) {
					this.hud.setMovementIconsVisible(false);
				} else {
					this.hud.setMovementIconsVisible(true);
				}
			}
		}
	}
	
	
}

