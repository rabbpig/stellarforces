package com.scs.stellarforces.game;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.MultiLineLabel;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

import dsr.data.EquipmentData;
import dsr.data.GameData;
import dsr.data.UnitData;
import dsrwebserver.tables.UnitsTable;

public class AllUnitStatsModule extends SimpleScrollingAbstractModule {

	private static final float GAP = Statics.SCREEN_WIDTH * 0.005f;

	private ArrayList<UnitData> units;
	private GameData game_data;
	private static Paint paint_normal_text = new Paint();

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.06f));

	}


	public AllUnitStatsModule(AbstractActivity _act, AbstractModule _ret, GameData _game_data, ArrayList<UnitData> _units, EquipmentData[] equipment) {
		super(-1);

		this.mod_return_to = _ret;

		if (Statics.tf != null) {
			paint_normal_text.setTypeface(Statics.tf);
		}

		game_data = _game_data;
		units = _units;

		this.setBackground(Statics.BACKGROUND_R);

		this.root_node.removeAllChildren();

		VerticalFlowLayout vfl = new VerticalFlowLayout("vfl", GAP);

		UnitStatsControl unit_ctrl = null;
		if (game_data.is_advanced == 0) {
			unit_ctrl = new UnitStatsControl(GAP, "Name", "Hlth", "APs", "Shot", "CC", "Str");
		} else {
			unit_ctrl = new UnitStatsControl(GAP, "Name", "Hlth", "APs", "Shot", "CC", "Str", "Nrg", "Mrl");
		}

		vfl.attachChild(unit_ctrl);

		for (UnitData unit : units) {
			if (unit.getSide() == game_data.our_side) {
				if (unit.getHealth() > 0) {
					String name = unit.name;
					if (unit.unit_type == UnitsTable.UT_ENGINEER) {
						name = name + " (Eng)";
					}
					if (game_data.is_advanced == 0) {
						unit_ctrl = new UnitStatsControl(GAP, unit.order_by + ": " + name, unit.getHealth()+"/"+unit.getMaxHealth(), ""+unit.aps + "/" + unit.max_aps, ""+unit.shot_skill, ""+unit.combat_skill, ""+unit.strength);
					} else {
						unit_ctrl = new UnitStatsControl(GAP, unit.order_by + ": " + name, unit.getHealth()+"/"+unit.getMaxHealth(), ""+unit.aps, ""+unit.shot_skill, ""+unit.combat_skill, ""+unit.strength, ""+unit.curr_energy, ""+unit.curr_morale);
					}
					vfl.attachChild(unit_ctrl);
					if (unit.can_use_equipment) {
						String s = "";
						if (unit.protection > 0) {
							s = s + "Prot: " + unit.protection + " - ";
						}
						if (equipment != null) { // Are we still equipping?
							s = s + GameModule.GetUnitsEquipmentAsString(unit, equipment);
						}
						MultiLineLabel mll = new MultiLineLabel("eq", s, null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
						vfl.attachChild(mll);
					}
					if (unit.model_type == UnitsTable.MT_BLOB) {
						String s = "If exploded, rad=" + unit.getBlobExplodeRad() + ", dam=" + unit.getBlobExplodeDamage();
						MultiLineLabel mll = new MultiLineLabel("eq", s, null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
						vfl.attachChild(mll);
					}
				}
			}
		}
		vfl.updateGeometricState();

		this.root_node.attachChild(vfl);
		this.root_node.updateGeometricState();
		this.root_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);

	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		this.returnTo();

	}



}

