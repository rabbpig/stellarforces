package com.scs.stellarforces.game;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.MyEvent;
import ssmith.android.framework.modules.AbstractSingleScreenModule;
import ssmith.android.lib2d.gui.GUIFunctions;
import ssmith.android.util.Timer;
import ssmith.util.PointByte;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

import dsr.comms.DataTable;
import dsr.data.AppletMapSquare;
import dsr.data.EquipmentData;
import dsr.data.UnitData;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitsTable;
import dsrwebserver.tables.VisibleEnemiesTable;

public class ScannerModule extends AbstractSingleScreenModule {

	public static final String SCANNER_COORDS = "Scanner_Coords";

	private static final String SCANNER_TITLE = "Strategic Scanner";
	private static final String SCANNER_MSG = "Press the scanner to view that area";

	private static Paint paint_title_text = new Paint();
	private static Paint paint_small_text = new Paint();

	private static Paint walls_simple = new Paint();
	private static Paint walls_blur = new Paint(); 
	private static Paint paint_walls = new Paint(); // Wall

	private static Paint paint_destroyed_cpu = new Paint(); // destroyed computer
	private static Paint paint_cpu = new Paint(); // computer/water
	private static Paint paint_installation = new Paint(); // computer/water
	private static Paint paint_comrades = new Paint();
	private static Paint paint_eggs = new Paint();
	private static Paint paint_our_units = new Paint(); // our units
	private static Paint paint_enemy = new Paint(); // enemy
	private static Paint white = new Paint(); // selected unit
	private static Paint paint_door = new Paint();
	private static Paint paint_black = new Paint();

	private static Paint paint_heard_move_enemy = new Paint();
	private static Paint paint_heard_shooting_enemy = new Paint();
	private static Paint paint_seen_enemy = new Paint();

	private ArrayList<Point> eggs;

	static {
		paint_destroyed_cpu.setARGB(255, 50, 50, 100);
		paint_cpu.setARGB(255, 50, 50, 255);
		paint_installation.setARGB(255, 150, 50, 55);
		paint_comrades.setARGB(255, 155, 100, 155);
		paint_eggs.setARGB(255, 0, 255, 0);
		paint_our_units.setARGB(255, 255, 0, 255);
		paint_enemy.setARGB(255, 255, 0, 0);
		white.setARGB(255, 255, 255, 255);
		paint_door.setARGB(255, 152, 80, 50);
		paint_door.setARGB(255, 0, 0, 0);

		paint_title_text.setARGB(155, 255, 255, 255);
		paint_title_text.setAntiAlias(true);
		//paint_title_text.setStyle(Style.STROKE);
		paint_title_text.setTextSize(GUIFunctions.GetTextSizeToFit(SCANNER_TITLE, Statics.SCREEN_WIDTH/2, -1));

		paint_small_text.setARGB(200, 255, 255, 255);
		paint_small_text.setAntiAlias(true);
		//paint_small_text.setStyle(Style.STROKE);
		paint_small_text.setTextSize(GUIFunctions.GetTextSizeToFit(SCANNER_MSG, Statics.SCREEN_WIDTH/2, -1));

		paint_heard_move_enemy.setARGB(255, 150, 150, 150);
		paint_heard_shooting_enemy.setARGB(255, 255, 255, 255);
		paint_seen_enemy.setARGB(255, 150, 0, 0);
	}


	private float sq_size, offset_x, offset_y;
	private GameModule game;
	private RectF tmp_rect = new RectF();
	private boolean draw_units = true; // For flashing units
	private Timer unit_flash = new Timer(500);
	private float draw_upto_x, draw_upto_y;

	public ScannerModule(AbstractActivity act, GameModule _return_to) {
		super(-1);

		if (Statics.GetTypeface(act) != null) {
			paint_title_text.setTypeface(Statics.GetTypeface(act));
			paint_small_text.setTypeface(Statics.GetTypeface(act));
		}

		act.playSound(R.raw.teleport);

		paint_walls.setARGB(255, 0, 180, 0);

		this.mod_return_to = _return_to;
		game = _return_to;

		// Cache location of eggs
		eggs = new ArrayList<Point>();
		for (int i=0 ; i<game.equipment.length  ; i++) {
			EquipmentData eq = game.equipment[i];
			if (eq.destroyed == false) {
				if (eq.major_type == EquipmentTypesTable.ET_EGG || eq.major_type == EquipmentTypesTable.ET_FLAG) {
					int x = -1;
					int y = -1;
					// Is it being carried by a unit?
					if (eq.getUnitID() > 0) {
						UnitData unit = UnitData.GetUnitDataFromID(game.units, eq.getUnitID());
						x = unit.getMapX();
						y = unit.getMapY();
					} else {
						PointByte p = EquipmentData.GetEquipmentMapSquare(game.mapdata, eq);
						if (p == null) { // Problem with the data
							continue;
						}
						x = p.x;
						y = p.y;
					}
					eggs.add(new Point(x, y));
				}
			}
		}

		sq_size = Statics.SCREEN_HEIGHT / game.mapdata.getMapHeight();
		offset_x = (Statics.SCREEN_WIDTH-(game.mapdata.getMapWidth()*sq_size))/2;
		offset_y = (Statics.SCREEN_HEIGHT-(game.mapdata.getMapHeight()*sq_size))/2;

		createPaints();
		
		// Remove dead heard enemies
		DataTable heard_enemies = game.game_data.heard_enemies;
		UnitData units[] = game.units;
		heard_enemies.moveBeforeFirst();
		while (heard_enemies.moveNext()) {
			UnitData unit = UnitData.GetUnitDataFromID(units, heard_enemies.getInt("UnitID"));
			if (unit.getStatus() != UnitsTable.ST_DEPLOYED) {
				heard_enemies.removeCurrentRow();
				heard_enemies.movePrev();
			}
		}

	}


	private void createPaints() {
		// Walls
		walls_simple = new Paint();
		walls_simple.setAntiAlias(true);
		walls_simple.setDither(true);
		walls_simple.setColor(Color.argb(248, 155, 255, 55));
		walls_simple.setStrokeWidth(sq_size);// Statics.SCREEN_HEIGHT * 0.002f);
		walls_simple.setStyle(Paint.Style.FILL);
		walls_simple.setStrokeJoin(Paint.Join.MITER);
		walls_simple.setStrokeCap(Paint.Cap.ROUND);

		walls_blur = new Paint();
		walls_blur.set(walls_simple);
		walls_blur.setStyle(Paint.Style.FILL);
		walls_blur.setColor(Color.argb(235, 74, 255, 155));
		walls_blur.setStrokeWidth(sq_size * 1.4f);//Statics.SCREEN_HEIGHT * 0.02f);
		walls_blur.setMaskFilter(new BlurMaskFilter((sq_size * 1.4f), BlurMaskFilter.Blur.NORMAL));

	}


	@Override
	public void started() {
		draw_upto_x = 0;
		draw_upto_y = 0;
	}


	@Override
	public void doDraw(Canvas g, long interpol) {
		if (draw_upto_x <= Statics.SCREEN_WIDTH) {
			draw_upto_x += Statics.SCREEN_WIDTH/10f;
		}
		if (draw_upto_y <= Statics.SCREEN_HEIGHT) {
			draw_upto_y += Statics.SCREEN_HEIGHT/10f;
		}

		if (unit_flash.hasHit()) {
			draw_units = !draw_units;
		}

		g.drawText(SCANNER_TITLE, 10, paint_title_text.getTextSize(), paint_title_text);
		g.drawText(SCANNER_MSG, 10, Statics.SCREEN_HEIGHT - (paint_small_text.getTextSize()*1.1f), paint_small_text);

		g.drawLine(draw_upto_x, 0, draw_upto_x, Statics.SCREEN_HEIGHT, walls_simple);
		g.drawLine(draw_upto_x, 0, draw_upto_x, Statics.SCREEN_HEIGHT, walls_blur);
		g.drawLine(0, draw_upto_y, Statics.SCREEN_WIDTH, draw_upto_y, walls_simple);
		g.drawLine(0, draw_upto_y, Statics.SCREEN_WIDTH, draw_upto_y, walls_blur);

		//g.translate((Statics.SCREEN_WIDTH-(game.mapdata.getMapWidth()*sq_size))/2, (Statics.SCREEN_HEIGHT-(game.mapdata.getMapHeight()*sq_size))/2);
		g.translate(offset_x, offset_y);

		// Draw the walls
		for (int y2 = 0 ; y2<game.mapdata.getMapHeight() ; y2++) {
			for (int x2 = 0 ; x2<game.mapdata.getMapWidth() ; x2++) {
				AppletMapSquare sq = game.mapdata.getSq_MaybeNULL(x2, y2);
				tmp_rect.set(x2*sq_size, y2*sq_size, (x2*sq_size)+sq_size, (y2*sq_size)+sq_size);
				if (tmp_rect.left > draw_upto_x || tmp_rect.top > draw_upto_y) {
					continue;
				}
				if (sq.escape_hatch_side > 0) {
					g.drawRect(tmp_rect, white);
				} else if (game.game_data.game_status == GamesTable.GS_DEPLOYMENT && game.game_data.areSidesFriends(sq.deploy_sq_side, game.game_data.our_side) && sq.major_type == MapDataTable.MT_FLOOR) {
					g.drawRect(tmp_rect, paint_enemy);
				} else if (sq.major_type == MapDataTable.MT_COMPUTER) {
					if (sq.destroyed == 0) {
						g.drawRect(tmp_rect, paint_cpu);
					} else {
						g.drawRect(tmp_rect, paint_destroyed_cpu);
					}
				} else if (sq.major_type == MapDataTable.MT_WALL) {
					g.drawRect(tmp_rect, paint_walls);
					//g.drawRect(tmp_rect, walls_simple);
					//g.drawRect(tmp_rect, walls_blur);
					g.drawPoint((tmp_rect.left + tmp_rect.right)/2, (tmp_rect.top+tmp_rect.bottom)/2, walls_blur);
				} else if (sq.major_type == MapDataTable.MT_FLOOR) {
					if (sq.texture_code == TextureStateCache.TEX_WATER) {
						g.drawRect(tmp_rect, paint_cpu);
					} else if (sq.texture_code == TextureStateCache.TEX_CLONE_GENERATOR || sq.texture_code == TextureStateCache.TEX_GUN_VENDING_MACHINE || sq.texture_code == TextureStateCache.TEX_GRENADE_VENDING_MACHINE || sq.texture_code == TextureStateCache.TEX_MEDI_BAY || sq.texture_code == TextureStateCache.TEX_POWER_UNIT || sq.texture_code == TextureStateCache.TEX_ALIEN_COLONY) {
						//if (game.game_data.is_advanced == 0 || sq.owner_side == game.game_data.our_side) {
						g.drawRect(tmp_rect, paint_installation);
						//}
					} else if (sq.door_type > 0 && sq.door_open == false) {
						g.drawRect(tmp_rect, paint_door);
					}
					/*if (game.game_data.show_ceilings == 0) {
						g.drawRect(tmp_rect, paint_walls);
					}*/
				} else if (sq.major_type == MapDataTable.MT_NOTHING) { // For LMS missions etc...
					g.drawRect(tmp_rect, paint_walls);
				}
			}
		}

		if (draw_units) {
			// Draw heard enemy units
			DataTable heard_enemies = game.game_data.heard_enemies;
			heard_enemies.moveBeforeFirst();
			while (heard_enemies.moveNext()) {
				tmp_rect.set(heard_enemies.getInt("MapX")*sq_size, heard_enemies.getInt("MapY") * sq_size, (heard_enemies.getInt("MapX")*sq_size)+sq_size, (heard_enemies.getInt("MapY")*sq_size)+sq_size);
				//if (game.game_data.areSidesFriends(heard_enemies.getInt("Side"), game.game_data.our_side)) {
				if (heard_enemies.getInt("VisibleType") == VisibleEnemiesTable.VT_HEARD_MOVING) {
					g.drawOval(tmp_rect, paint_heard_move_enemy);
				} else {
					g.drawOval(tmp_rect, paint_heard_shooting_enemy); // Heard shooting
				}
				//}
			}		

			// Draw seen enemy units
			DataTable seen_enemies = game.game_data.seen_enemies;
			seen_enemies.moveBeforeFirst();
			while (seen_enemies.moveNext()) {
				tmp_rect.set(seen_enemies.getInt("MapX")*sq_size, seen_enemies.getInt("MapY") * sq_size, (seen_enemies.getInt("MapX")*sq_size)+sq_size, (seen_enemies.getInt("MapY")*sq_size)+sq_size);
				//if (game.game_data.areSidesFriends(seen_enemies.getInt("Side"), game.game_data.our_side)) {
				g.drawOval(tmp_rect, paint_seen_enemy); // Heard shooting 
				//}
			}		
		}

		// Draw eggs/flag
		for (Point p : eggs) {
			tmp_rect.set(p.x*sq_size, p.y * sq_size, (p.x*sq_size)+sq_size, (p.y*sq_size)+sq_size);
			g.drawOval(tmp_rect, paint_eggs);

		}


		// Draw our units
		if (draw_units) {
			for (int i=0 ; i<game.units.length  ; i++) {
				UnitData unit = game.units[i];
				if (unit.getStatus() == UnitsTable.ST_DEPLOYED) {
					tmp_rect.set(unit.map_x*sq_size, unit.map_y * sq_size, (unit.map_x*sq_size)+sq_size, (unit.map_y*sq_size)+sq_size);
					if (game.game_data.areSidesFriends(unit.getSide(), game.game_data.our_side)) {
						if (unit == game.getCurrentUnit()) {
							g.drawOval(tmp_rect, white);
						} else {
							if (unit.getSide() == game.game_data.our_side) {
								g.drawOval(tmp_rect, paint_our_units);
							} else {
								g.drawOval(tmp_rect, paint_comrades);
							}
						}
						if (unit.unit_type == UnitsTable.UT_ENGINEER) {
							float inset = sq_size / 4;
							tmp_rect.set((unit.map_x*sq_size)+inset, (unit.map_y * sq_size)+inset, (unit.map_x*sq_size)+sq_size-inset, (unit.map_y*sq_size)+sq_size-inset);
							g.drawOval(tmp_rect, paint_black);
						}
					} else {
						if (unit.model != null) {
							if (unit.model.visible) {
								g.drawOval(tmp_rect, paint_enemy);
							}
						}
					}
				}
			}
		}
	}


	@Override
	public boolean processEvent(MyEvent evt) throws Exception {
		float x = evt.getX() - offset_x;
		float y = evt.getY() - offset_y;
		int sx = (int)(x / sq_size);
		int sy = (int)(y / sq_size);
		Statics.data.clear();
		if (sx >= 0 && sx < game.mapdata.getMapWidth()) {
			if (sy >= 0 && sy < game.mapdata.getMapHeight()) {
				Statics.data.put(SCANNER_COORDS, sx + "," + sy);
			}
		}
		return super.processEvent(evt);
	}


	public boolean playMusic() {
		// Override if required
		return false;

	}



}
