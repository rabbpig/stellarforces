package com.scs.stellarforces.game;

public interface IProcessable {

	public void process(long interpol);
	
}
