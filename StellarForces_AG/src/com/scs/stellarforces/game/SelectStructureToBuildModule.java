package com.scs.stellarforces.game;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractOptionsModule2;
import ssmith.lang.NumberFunctions;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

import dsr.data.AppletMapSquare;
import dsrwebserver.tables.MapDataTable;

public class SelectStructureToBuildModule extends AbstractOptionsModule2 {

	private static Paint paint_normal_text = new Paint();

	private GameModule game;

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));

	}

	public SelectStructureToBuildModule(AbstractActivity act, GameModule mod) {
		super(act, -1, 2, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, Statics.SCREEN_WIDTH * .4f, Statics.SCREEN_HEIGHT/7), -1, false, "Select Structure (" + mod.game_data.getResPointsForOurSide() + " Res Points)", true);

		if (Statics.GetTypeface(act) != null) {
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.mod_return_to = mod;
		game = mod;

		this.setBackground(Statics.BACKGROUND_R);
	}


	@Override
	public void getOptions() {
		int data[] = {TextureStateCache.TEX_CLONE_GENERATOR, TextureStateCache.TEX_GUN_VENDING_MACHINE, TextureStateCache.TEX_GRENADE_VENDING_MACHINE, TextureStateCache.TEX_MEDI_BAY, TextureStateCache.TEX_MOONBASE_BLUE};
		for (int i : data) {
			addOption(TexToString(i) + " (" + GetRPCostToBuildStructure(i) + ")", i);
		}
		/*addOption(TexToString(TextureStateCache.TEX_CLONE_GENERATOR) + " (" + GetCostForStructure)", TextureStateCache.TEX_CLONE_GENERATOR);
		addOption(TexToString(TextureStateCache.TEX_GUN_VENDING_MACHINE) + " ()", TextureStateCache.TEX_GUN_VENDING_MACHINE);
		addOption(TexToString(TextureStateCache.TEX_GRENADE_VENDING_MACHINE) + " ()", TextureStateCache.TEX_GRENADE_VENDING_MACHINE);
		addOption(TexToString(TextureStateCache.TEX_MEDI_BAY) + " ()", TextureStateCache.TEX_MEDI_BAY);
		addOption(TexToString(TextureStateCache.TEX_MOONBASE_BLUE) + " ()", TextureStateCache.TEX_MOONBASE_BLUE);*/
	}


	@Override
	public void optionSelected(int idx) {
		String cmd = super.getActionCommand(idx);
		game.buildStructure(NumberFunctions.ParseInt(cmd));
		this.returnTo();
	}


	public static String TexToString(int tex) {
		switch (tex) {
		case TextureStateCache.TEX_MEDI_BAY: return "Medi-Bay";
		case TextureStateCache.TEX_GUN_VENDING_MACHINE: return "Gun Vending Machine";
		case TextureStateCache.TEX_GRENADE_VENDING_MACHINE: return "Grenade Vending Machine";
		case TextureStateCache.TEX_CLONE_GENERATOR: return "Clone Generator";
		case TextureStateCache.TEX_MOONBASE_BLUE: return "Wall";
		case TextureStateCache.TEX_POWER_UNIT: return "Power Unit";
		case TextureStateCache.TEX_ALIEN_COLONY: return "Alien Egg";
		default: throw new RuntimeException("Unknown structure type: " + tex);
		}
	}


	public static int GetResPtsForDismantledStructure(AppletMapSquare sq) {
		int res = 0;
		if (sq.door_type > 0) {
			res = 1;
		} else if (sq.major_type == MapDataTable.MT_WALL) {
			res = 1;
		} else {
			/*switch (sq.texture_code) {
			case TextureStateCache.TEX_MEDI_BAY: res = 4; break;
			case TextureStateCache.TEX_GUN_VENDING_MACHINE: res = 5; break;
			case TextureStateCache.TEX_GRENADE_VENDING_MACHINE: res = 5; break;
			case TextureStateCache.TEX_CLONE_GENERATOR: res = 6; break;
			case TextureStateCache.TEX_MOONBASE_BLUE: res = 1; break;
			case TextureStateCache.TEX_POWER_UNIT: res = 1; break;
			//default: return 1; // Default for a wall   
			//throw new RuntimeException("Unknown structure type: " + tex);
			}*/
			res = GetRPCostToBuildStructure(sq.texture_code);
		}
		if (res > 1) {
			res--; // get one point less for dismantling
		}
		return res;
	}


	public static int GetRPCostToBuildStructure(int tex) {
		int res = 0;
		switch (tex) {
		case TextureStateCache.TEX_MEDI_BAY: res = 3; break;
		case TextureStateCache.TEX_GUN_VENDING_MACHINE: res = 5; break;
		case TextureStateCache.TEX_GRENADE_VENDING_MACHINE: res = 5; break;
		case TextureStateCache.TEX_CLONE_GENERATOR: res = 6; break;
		case TextureStateCache.TEX_MOONBASE_BLUE: res = 1; break;
		case TextureStateCache.TEX_POWER_UNIT: res = 1; break;
		case TextureStateCache.TEX_ALIEN_COLONY: res = 1; break;
		default: return 0; // Default for a wall   
		//throw new RuntimeException("Unknown structure type: " + tex);
		}
		return res;
	}


	public static int GetAPCostToBuildStructure(int tex) {
		int res = 0;
		switch (tex) {
		case TextureStateCache.TEX_MEDI_BAY: res = 20; break;
		case TextureStateCache.TEX_ALIEN_COLONY: res = 20; break;
		case TextureStateCache.TEX_MOONBASE_BLUE: res = 10; break;
		default: return 40;
		}
		return res;
	}


	public boolean playMusic() {
		// Override if required
		return false;
		
	}
	

}
