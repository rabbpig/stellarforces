package com.scs.stellarforces.message;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.MultiLineLabel;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class ViewMessageModule extends SimpleScrollingAbstractModule {

	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));

	}

	private DataTable messages_dt;
	private Button cmd_reply;

	
	public ViewMessageModule(AbstractActivity act, AbstractModule _return_to, DataTable _messages_dt) {
		super(-1);

		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.mod_return_to = _return_to;
		messages_dt = _messages_dt;

		this.setBackground(Statics.BACKGROUND_R);

		StringBuffer text = new StringBuffer();
		try {
			text.append("From: " + AbstractCommFuncs.URLDecodeString(messages_dt.getString("FromName")) + "\n");
			text.append("Date: " + messages_dt.getString("DateCreated") + "\n");
			text.append("Subject: " + AbstractCommFuncs.URLDecodeString(messages_dt.getString("Subject")) + "\n\n");
			text.append(AbstractCommFuncs.URLDecodeString(messages_dt.getString("Message")) + "\n\n");
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
		}

		MultiLineLabel label2 = new MultiLineLabel("credits", text.toString(), null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
		label2.setLocation(10, Statics.SCREEN_HEIGHT * 0.2f);
		root_node.attachChild(label2);

		cmd_reply = new Button("Reply", null, paint_large_text, Statics.img_cache.getImage(Statics.BUTTON_R, Statics.SCREEN_WIDTH * 0.9f, Statics.SCREEN_HEIGHT/6f));
		this.root_node.attachChild(cmd_reply);
		
		this.root_node.updateGeometricState();

		this.root_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);

		start();
	}


	public void run() {
		AbstractActivity act = Statics.act;
		
		try {
			// Mark it as read
			if (messages_dt.getInt("Read") == 0) {
				new WGet_SF(act, null, "cmd=" + MiscCommsPage.MESSAGE_READ + "&version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD) + "&msgid=" + messages_dt.getInt("MessageID"));
				messages_dt.setInt("Read", 1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		AbstractActivity act = Statics.act;
		
		if (c == cmd_reply) {
			String subject = AbstractCommFuncs.URLDecodeString(messages_dt.getString("Subject"));
			if (subject.toLowerCase().startsWith("re:") == false) {
				subject = "Re: " + subject;
			}
			this.getThread().setNextModule(new SendMessageModule(act, this, AbstractCommFuncs.URLDecodeString(messages_dt.getString("FromName")), subject, AbstractCommFuncs.URLDecodeString(messages_dt.getString("Message"))));
		} else {
			this.returnTo();
		}

	}

}

