package com.scs.stellarforces.message;

import java.io.UnsupportedEncodingException;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractMultilineOptionsModule2;
import ssmith.lang.NumberFunctions;
import android.graphics.Paint;
import android.graphics.Paint.Style;

import com.scs.stellarforces.Statics;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class ListMessagesModule extends AbstractMultilineOptionsModule2 { //AbstractOptionsModule2 {

	private DataTable messages_dt;

	private static Paint paint_free_text = new Paint();
	private static Paint paint_background = new Paint();

	static {
		paint_free_text.setARGB(255, 0, 0, 0);
		paint_free_text.setAntiAlias(true);
		//paint_free_text.setStyle(Style.STROKE);
		paint_free_text.setTextSize(Statics.SCREEN_WIDTH * 0.03f);

		paint_background.setARGB(155, 255, 255, 255);
		paint_background.setAntiAlias(true);
		paint_background.setStyle(Style.FILL);
	}


	public ListMessagesModule(AbstractActivity act) {
		//super(_act, Statics.MOD_MENU, 1, paint_free_text, Statics.img_cache.getImage(Statics.BUTTON_R, Statics.SCREEN_WIDTH*0.95f, Statics.SCREEN_HEIGHT/6), -1, false, "Messages", true);
		super(act, Statics.MOD_MENU, paint_background, paint_free_text, -1, false, "Messages", Statics.SCREEN_WIDTH * 0.9f, true);

		if (Statics.GetTypeface(act) != null) {
			paint_free_text.setTypeface(Statics.GetTypeface(act));
		}

		this.setBackground(Statics.BACKGROUND_R);

	}


	@Override
	public void started() {
		AbstractActivity act = Statics.act;
		
		if (messages_dt == null) {
			this.showPleaseWait("Getting messages..."); // showPleaseWait() sometimes gets stuck
			try {
				WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.GET_MESSAGES + "&version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
				String response = wc.getResponse();
				messages_dt = new DataTable(response);
				this.dismissPleaseWait();
			} catch (Exception ex) {
				this.dismissPleaseWait();
				AbstractActivity.HandleError(ex);
			}
		} else {
			setOptions(); // Re-list messages, but don't get them from the server
		}
	}


	@Override
	public void getOptions() {
		addOption("New Message");
		if (messages_dt != null) {
			messages_dt.moveBeforeFirst();
			while (messages_dt.moveNext()) {
				StringBuffer s = new StringBuffer();
				try {
					s.append(messages_dt.getString("DateCreated") + " ");
					if (messages_dt.getInt("FromID") > 0) {
						s.append("From " + AbstractCommFuncs.URLDecodeString(messages_dt.getString("FromName")) + " ");
					}
					s.append(" - " + AbstractCommFuncs.URLDecodeString(messages_dt.getString("Subject")));// + " (" + messages_dt.getInt("DaysOld") + " days old)");
				} catch (UnsupportedEncodingException ex) {
					AbstractActivity.HandleError(ex);
				}
				if (messages_dt.getInt("Read") == 0) {
					s.append(" [UNREAD]");
				} else {
					s.append(" [read]");
				}
				addOption(s.toString(), messages_dt.getInt("MessageID"));
			}
		}
		addOption("New Message");
	}


	@Override
	public void optionSelected(int idx) {
		AbstractActivity act = Statics.act;
		
		int id = NumberFunctions.ParseInt(super.getActionCommand(idx));
		if (messages_dt.find("MessageID", id)) {
		//if (idx > 0 && idx < messages_dt.size()) {
			//messages_dt.moveTo(idx+1);
			this.getThread().setNextModule(new ViewMessageModule(act, this, messages_dt));
		} else {
			// Start new message
			this.getThread().setNextModule(new SendMessageModule(act, this, "", "", ""));
		}
	}

}


