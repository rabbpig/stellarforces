package com.scs.stellarforces.message;

import ssmith.android.framework.ErrorReporter;
import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.keyboard.KeyboardActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.GUIFunctions;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import ssmith.android.lib2d.gui.TextBox;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.text.InputType;

import com.scs.stellarforces.Statics;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class SendMessageModule extends SimpleScrollingAbstractModule {

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * .8f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT/7;

	private static final String CMD_TO = "to";
	private static final String CMD_SUBJECT = "subject";
	private static final String CMD_MSG = "msg";

	private Label lbl_to, lbl_subject;
	private MultiLineLabel mll_msg;
	private TextBox txt_to, txt_subject;
	private Button cmd_edit_msg, cmd_send;
	private String to, subject, msg = "", reply_text;

	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(GUIFunctions.GetTextSizeToFit("stephen.carlylesmith@googlemail.com", ICON_WIDTH, ICON_HEIGHT));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		//paint_normal_text.setStyle(Style.STROKE);
		paint_normal_text.setTextSize(paint_large_text.getTextSize()/2);
	}


	public SendMessageModule(AbstractActivity act, AbstractModule return_to, String _to, String _subject, String _reply_text) {
		super(-1);

		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.mod_return_to = return_to;
		to = _to;
		subject = _subject;
		reply_text = _reply_text;

		this.setBackground(Statics.BACKGROUND_R);
		
	}


	/**
	 * This is called when the module is activated.
	 */
	@Override
	public void started() {
		// Have we been passed any data?
		if (Statics.data.containsKey(CMD_TO)) {
			to = Statics.data.get(CMD_TO);//this.txt_to.setText(Statics.data.get(CMD_TO));
		} else if (Statics.data.containsKey(CMD_SUBJECT)) {
			subject = Statics.data.get(CMD_SUBJECT);
		} else if (Statics.data.containsKey(CMD_MSG)) {
			msg = Statics.data.get(CMD_MSG);
		}
		Statics.data.clear();

		this.root_node.detachAllChildren();
		Bitmap bmp = Statics.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT);
		lbl_to = new Label("to", "To:", ICON_WIDTH, ICON_HEIGHT, null, paint_large_text, false);
		txt_to = new TextBox("to", to, null, paint_large_text, -1, bmp);
		lbl_subject = new Label("subject", "Subject:", ICON_WIDTH, ICON_HEIGHT, null, paint_large_text, false);
		txt_subject = new TextBox("subject", subject, null, paint_large_text, -1, bmp);
		mll_msg = new MultiLineLabel("msg", msg, null, paint_large_text, true, Statics.SCREEN_WIDTH * 0.9f);
		cmd_edit_msg = new Button("OK", "Edit Message Body", null, paint_large_text, bmp); 
		cmd_send = new Button("OK", "Send Message", null, paint_large_text, bmp); 

		VerticalFlowLayout layout = new VerticalFlowLayout("Menu", 5);
		layout.attachChild(lbl_to);
		layout.attachChild(txt_to);
		layout.attachChild(lbl_subject);
		layout.attachChild(txt_subject);
		layout.attachChild(mll_msg);
		layout.attachChild(cmd_edit_msg);
		layout.attachChild(cmd_send);

		this.root_node.attachChild(layout);
		this.root_node.updateGeometricState();

		this.root_cam.lookAt(layout, true);

		if (this.reply_text.length() > 0 && msg.length() == 0) {
			// Auto-click edit body, why not?
			showKeyboard_New(CMD_MSG, this.mll_msg.getText(), false, InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
		}

	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		AbstractActivity act = Statics.act;
		
		if (c == txt_to) {
			showKeyboard_New(CMD_TO, this.txt_to.getText(), true, InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
		} else if (c == txt_subject) {
			showKeyboard_New(CMD_SUBJECT, this.txt_subject.getText(), true, InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT);
		} else if (c == cmd_edit_msg) {
			showKeyboard_New(CMD_MSG, this.mll_msg.getText(), false, InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
		} else if (c == cmd_send) {
			String msg_text = this.mll_msg.getText().trim();
			if (msg_text.length() > 0) {
				this.showPleaseWait("Sending message...");
				try {
					try {
						if (reply_text.length() > 0) {
							reply_text = "> " + reply_text;
							reply_text = reply_text.replaceAll("\n", "\n> ");
							msg_text = msg_text + "\n\n" + reply_text;
						}
					} catch (Exception ex) {
						ErrorReporter.getInstance().handleSilentException(ex);
					}
					WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.SEND_MESSAGE + "&version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD) + "&toname=" + AbstractCommFuncs.URLEncodeString(this.txt_to.getText()) + "&subject=" + AbstractCommFuncs.URLEncodeString(this.txt_subject.getText()) + "&msg=" + AbstractCommFuncs.URLEncodeString(msg_text));
					String response = wc.getResponse();
					if (response.length() == 0 || response.equalsIgnoreCase("OK")) {
						this.dismissPleaseWait();
						this.showToast("Message Sent");
					} else {
						//throw new IOException(response);
						this.dismissPleaseWait();
						this.showToast(response);
					}
				} catch (Exception ex) {
					AbstractActivity.HandleError(ex);
				} finally {
					//this.dismissPleaseWait();
				}
				this.returnTo();
			} else {
				this.showToast("There is no message text!");
			}
		}
	}


	/*private void showKeyboard_Mine(String code, String curr_val) {
		Statics.data.put("code", code);
		KeyboardModule keyboard = new KeyboardModule(act, this, false);
		keyboard.setText(curr_val);
		super.getThread().setNextModule(keyboard);
	}*/


	private void showKeyboard_New(String code, String curr_val, boolean cr_to_end, int hint) {
		AbstractActivity act = Statics.act;
		
		Statics.data.put("code", code);
		Statics.data.put("text", curr_val);
		if (cr_to_end) {
			Statics.data.put(KeyboardActivity.CR_TO_END, "yes");
		} else {
			Statics.data.put(KeyboardActivity.CR_TO_END, "no");
		}
		act.showKeyboard(hint);
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			started(); // populate data
		}
	}

}
