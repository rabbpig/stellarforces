package com.scs.stellarforces.universe;

import java.io.IOException;
import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractPleaseWaitModule;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.start.ErrorModule;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsr.data.UnitData;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class GetUniverseEquipmentDataModule extends AbstractPleaseWaitModule {

	private DataTable available_armour_dt, available_equip_dt;
	//private int gameid;
	private ArrayList<UnitData> units;
	private UniverseMapPlanetIcon planet;

	public GetUniverseEquipmentDataModule(AbstractActivity _act, UniverseMapPlanetIcon _planet, ArrayList<UnitData> _units) {
		super(_act, -1);

		//gameid = _gameid;
		units = _units;
		planet =_planet;

		this.setBackground(Statics.BACKGROUND_R);

		start();
	}


	public void run() {
		AbstractActivity act = Statics.act;
		
		try {
			// Get equipment list
			super.displayMessage("Getting equipment list...");
			//WGet_SF wc = new WGet_SF(act, this, "cmd=" + MiscCommsPage.GET_EQUIPMENT_LIST + "&login=" + CommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + CommFuncs.URLEncodeString(Statics.LAST_PWD) + "&gid=" + gameid);
			WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.GET_UNI_INVENTORY_DATA + "&comms_version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
			String response = wc.getResponse();
			if (response.length() > 0) {
				available_equip_dt = new DataTable(response);
				super.displayMessage("Getting armour list...");
				wc = new WGet_SF(act, this, "cmd=" + MiscCommsPage.GET_ARMOUR_LIST + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD) + "&gid=" + planet.gameid);
				response = wc.getResponse();
				if (response.length() > 0) {
					available_armour_dt = new DataTable(response);
					SelectUniverseArmourModule sel_armour = new SelectUniverseArmourModule(act, planet, units, available_armour_dt, available_equip_dt);
					super.getThread().setNextModule(sel_armour);
					return;
				}
			}
			super.getThread().setNextModule(new ErrorModule(act, Statics.MOD_MENU, "Failed!", "Sorry, I failed to get the equipment data."));
		} catch (IOException ex) {
			this.getThread().setNextModule(new ErrorModule(act, Statics.MOD_MENU, ex));
		}

	}



}
