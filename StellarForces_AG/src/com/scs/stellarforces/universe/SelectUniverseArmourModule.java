package com.scs.stellarforces.universe;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.layouts.HorizontalFlowGridLayout;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.AllUnitStatsModule;
import com.scs.stellarforces.main.lite.R;
import com.scs.stellarforces.start.CurrentGameDetailsModule;
import com.scs.stellarforces.start.equip.ArmourHelpModule;
import com.scs.stellarforces.start.equip.GetEquipmentDataModule;
import com.scs.stellarforces.start.equip.UnitArmourSelectionControl;

import dsr.comms.DataTable;
import dsr.data.GameData;
import dsr.data.UnitData;

public class SelectUniverseArmourModule extends SimpleScrollingAbstractModule {

	private static final float GAP = Statics.SCREEN_WIDTH * 0.01f;

	private ArrayList<UnitData> units;
	private Button finished, help_button, finished2, unit_stats, game_details, armour_stats;
	private DataTable equipment_dt, armour_dt;
	//public static EquipmentData[] bought_equipment;
	private GameData game_data;
	private Label creds_left;
	private int creds_remaining, num_units;
	private AllUnitStatsModule stats_mod;
	private UniverseMapPlanetIcon planet;

	private static Paint paint_normal_text = new Paint();

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.065f));

	}


	public SelectUniverseArmourModule(AbstractActivity _act, UniverseMapPlanetIcon _planet, ArrayList<UnitData> _units, DataTable _armour_dt, DataTable _equip_dt) {
		super(-1);

		planet = _planet;
		units = _units;
		armour_dt = _armour_dt;
		equipment_dt = _equip_dt;

		this.setBackground(Statics.BACKGROUND_R);

		this.root_node.removeAllChildren();

		VerticalFlowLayout vfl = new VerticalFlowLayout("vfl", GAP);

		creds_left = new Label("creds_left", "Filled in later..", null, paint_normal_text, false);
		vfl.attachChild(creds_left);

		HorizontalFlowGridLayout hfgl2 = new HorizontalFlowGridLayout("hfgl", Statics.SCREEN_WIDTH * 0.02f);
		finished = new Button("Buy Selected Armour", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_green, Statics.SCREEN_WIDTH*.45f, Statics.SCREEN_HEIGHT/7f));
		hfgl2.attachChild(finished);
		help_button = new Button("Help", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_green, Statics.SCREEN_WIDTH*.45f, Statics.SCREEN_HEIGHT/7f));
		hfgl2.attachChild(help_button);
		vfl.attachChild(hfgl2);

		
		num_units = 0;
		for (UnitData unit : units) {
			if (unit.getSide() == game_data.our_side) {
				if (unit.can_use_equipment) {
					if (unit.can_deploy > 0) {
						if (unit.can_equip > 0) {
						UnitArmourSelectionControl unit_ctrl = new UnitArmourSelectionControl(unit, armour_dt, GAP, game_data);
						vfl.attachChild(unit_ctrl);
						num_units++;
						}
					}
				}
			}
		}

		HorizontalFlowGridLayout hfgl = new HorizontalFlowGridLayout("hfgl", Statics.SCREEN_WIDTH * 0.02f);

		unit_stats = new Button("Unit Stats", null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, Statics.SCREEN_WIDTH*.3f, Statics.SCREEN_HEIGHT/7f));
		hfgl.attachChild(unit_stats);
		armour_stats = new Button("Armour Stats", null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, Statics.SCREEN_WIDTH*.3f, Statics.SCREEN_HEIGHT/7f));
		hfgl.attachChild(armour_stats);
		game_details = new Button("Game Details", null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, Statics.SCREEN_WIDTH*.3f, Statics.SCREEN_HEIGHT/7f));
		hfgl.attachChild(game_details);
		hfgl.updateGeometricState();
		
		vfl.attachChild(hfgl);

		finished2 = new Button("Finished!", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_green, Statics.SCREEN_WIDTH*.95f, Statics.SCREEN_HEIGHT/7f));
		vfl.attachChild(finished2);

		vfl.updateGeometricState();

		this.root_node.attachChild(vfl);
		this.root_node.updateGeometricState();
		this.root_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);

		this.updateCreds();

	}


	@Override
	public void started() {
		if (game_data.creds <= 0) {
			finished();
		}
		this.updateCreds(); // In case we've come back from equipping
	}
	
	
	private void finished() {
		AbstractActivity act = Statics.act;
		
		this.getThread().setNextModule(new EquipUniverseUnitsModule(act, planet, this, game_data, units, armour_dt, equipment_dt, num_units));
	
	}
	
	
	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		AbstractActivity act = Statics.act;
		
		if (c == finished || c == finished2) {
			finished();
		} else {
			if (c instanceof Label) {
				showUnitStats();
			} else if (c == help_button) {
				ArmourHelpModule mod = new ArmourHelpModule(act, this);
				this.getThread().setNextModule(mod);
			} else if (c == unit_stats) {
				showUnitStats();
			} else if (c == game_details) {
				CurrentGameDetailsModule mod = new CurrentGameDetailsModule(act, this, game_data, false);
				this.getThread().setNextModule(mod);
			} else if (c == armour_stats) {
				Context mContext = act.getBaseContext();
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.stellarforces.com/dsr/armourdetails.cls"));
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				mContext.startActivity(intent);
			} else {
				UnitArmourSelectionControl parent = (UnitArmourSelectionControl) c.getParent();
				parent.selectArmour(c);
				updateCreds();
			}
		}

	}


	/*private void saveArmour() throws UnknownHostException, IOException {
		// Save armour
		try {
			this.showPleaseWait("Saving armour...");
			StringBuffer str = new StringBuffer();
			for (UnitData unit : units) {
				if (unit.getSide() == game_data.our_side) {
					String req = UnitDataComms.GetUnitUpdateRequest(unit);
					str.append(req + MiscCommsPage.SEP);
				}
			}
			if (str.length() > 0) {
				new WGet_SF(act, null, str.toString());
			}
		} catch (Exception ex) {
			this.dismissPleaseWait();
			this.showToast(ex.toString());
		} finally {
			this.dismissPleaseWait();
		}

	}*/


	private void showUnitStats() {
		AbstractActivity act = Statics.act;
		
		if (stats_mod == null) {
			stats_mod = new AllUnitStatsModule(act, this, game_data, units, null);
		}
		this.getThread().setNextModule(stats_mod);

	}


	private void updateCreds() {
		creds_remaining = GetEquipmentDataModule.GetCredsRemaining(game_data, units, armour_dt, equipment_dt);
		if (num_units != 0) {
			this.creds_left.setText("Buy Armour (Creds Left: " + this.creds_remaining + ", " + (creds_remaining/num_units) + " per unit)");
		}
	}


}
