package com.scs.stellarforces.universe;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractOptionsModule2;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public class UniverseMenuModule extends AbstractOptionsModule2 {

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.48f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT * 0.15f;

	private static final String CMD_MAP = "Starmap";
	private static final String CMD_SQUAD = "Squad";
	private static final String CMD_TRADE = "Trade";
	private static final String CMD_INVENTORY = "Inventory";
	private static final String CMD_TREATIES = "Treaties";
	//private static final String CMD_CONTRACTS = "Contracts";
	//private static final String CMD_REFRESH_DATA = "Refresh Data";

	private static Paint paint_normal_text = new Paint();

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.055f));

	}

	public UniverseMenuModule(AbstractActivity act) {
		super(act, Statics.MOD_MENU, 2, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT), 0, false, "", false);

		this.setBackground(Statics.BACKGROUND_R);

		start();
	}
	

	@Override
	public void getOptions() {
		super.addOption(CMD_MAP);
		super.addOption(CMD_SQUAD);
		super.addOption(CMD_INVENTORY);
		//super.addOption(CMD_TRADE);
		//super.addOption(CMD_CONTRACTS);
		//super.addOption(CMD_REFRESH_DATA);
	}

	
	@Override
	public void optionSelected(int idx) {
		AbstractActivity act = Statics.act;
		
		String cmd = super.getActionCommand(idx);
		if (cmd.equalsIgnoreCase(CMD_MAP)) {
			this.getThread().setNextModule(new UniverseMapModule());
		} else if (cmd.equalsIgnoreCase(CMD_SQUAD)) {
			this.getThread().setNextModule(new UniverseSquadModule(this, false, null));
		} else if (cmd.equalsIgnoreCase(CMD_INVENTORY)) {
			this.getThread().setNextModule(new InventoryModule(act, this));
		} else if (cmd.equalsIgnoreCase(CMD_TREATIES)) {
			this.getThread().setNextModule(new TreatiesModule(act, this));
		} else if (cmd.equalsIgnoreCase(CMD_TRADE)) {
			this.getThread().setNextModule(new TradeMenuModule(act));
		/*} else if (cmd.equalsIgnoreCase(CMD_REFRESH_DATA)) {
			this.getThread().setNextModule(new RefreshUniverseDataModule(act));*/
		}
	}



}
