package com.scs.stellarforces.universe;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.AbstractOptionsModule2;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class InventoryModule extends AbstractOptionsModule2 {

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.4f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT * 0.15f;

	private DataTable eq_data;

	private static Paint paint_normal_text = new Paint();

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.055f));

	}


	public InventoryModule(AbstractActivity act, AbstractModule return_to) {
		super(act, -1, 1, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT), 0, false, "", false);

		this.mod_return_to = return_to;

		this.setBackground(Statics.BACKGROUND_R);
	}


	@Override
	public void started() {
		AbstractActivity act = Statics.act;

		//if (eq_data == null) {
		this.showPleaseWait("Getting inventory data...");
		try {
			WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.GET_UNI_INVENTORY_DATA + "&comms_version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
			String response = wc.getResponse();
			eq_data = new DataTable(response);
			super.setOptions();
		} catch (Exception ex) {
			this.dismissPleaseWait();
			AbstractActivity.HandleError(ex);
		} finally {
			this.dismissPleaseWait();
		}
		//}
	}


	@Override
	public void getOptions() {
		if (eq_data.size() > 0) {
			eq_data.moveBeforeFirst();
			while (eq_data.moveNext()) {
				super.addOption(eq_data.getString("Name") + ": " + eq_data.getInt("Qty"));
			}
		} else {
			super.addOption("You have nothing in your inventory.");
		}
	}


	@Override
	public void optionSelected(int idx) {
		// Do nothing
	}


}
