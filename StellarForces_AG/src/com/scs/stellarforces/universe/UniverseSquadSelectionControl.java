package com.scs.stellarforces.universe;

import ssmith.android.lib2d.gui.CheckBox;
import ssmith.android.lib2d.gui.ToggleButton;
import ssmith.android.lib2d.layouts.HorizontalFlowGridLayout;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

public class UniverseSquadSelectionControl extends HorizontalFlowGridLayout {

	public static final float OVERALL_WIDTH = Statics.SCREEN_WIDTH * 0.90f;
	//private static float ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT;
	public static float ARMOUR_ICON_WIDTH = OVERALL_WIDTH * 0.5f / 7;
	public static float ARMOUR_ICON_HEIGHT = Statics.SCREEN_HEIGHT/8f;


	public static Paint paint_unit_name = new Paint();
	public static Paint paint_armour_name = new Paint();

	private CheckBox tb_unit_name;
	public int uniunitid;
	private boolean select;

	static {
		paint_unit_name.setARGB(255, 255, 255, 255);
		paint_unit_name.setAntiAlias(true);
		paint_unit_name.setTextSize(Statics.GetHeightScaled(0.06f));

		paint_armour_name.setARGB(255, 255, 255, 255);
		paint_armour_name.setAntiAlias(true);
		paint_armour_name.setTextSize(Statics.GetHeightScaled(0.03f));
	}


	public UniverseSquadSelectionControl(float GAP, boolean _select, int _uniunitid, String name, String health, String aps, String shot_skill, String cc_skill, String str, String energy, String morale) {
		super("UniverseSquadSelectionControl", GAP);

		if (_uniunitid <= 0) {
			throw new RuntimeException("Zero _uniunitid!");
		}

		select = _select;
		uniunitid = _uniunitid;


		tb_unit_name = new CheckBox(name, null, paint_unit_name, Statics.img_cache.getImage(Statics.BUTTON_R, OVERALL_WIDTH * 0.5f, ARMOUR_ICON_HEIGHT), false);
		//tb_unit_name.setSize(OVERALL_WIDTH * 0.6f, ARMOUR_ICON_HEIGHT);
		this.attachChild(tb_unit_name);

		// why are these togglebuttons?
		ToggleButton b = new ToggleButton(health, null, null, paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
		this.attachChild(b);
		b = new ToggleButton(aps, null, null, paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
		this.attachChild(b);
		b = new ToggleButton(shot_skill, null, null, paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
		this.attachChild(b);
		b = new ToggleButton(cc_skill, null, null, paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
		this.attachChild(b);
		b = new ToggleButton(str, null, null, paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
		this.attachChild(b);
		b = new ToggleButton(energy, null, null, paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
		this.attachChild(b);
		b = new ToggleButton(morale, null, null, paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, ARMOUR_ICON_WIDTH, ARMOUR_ICON_HEIGHT));
		this.attachChild(b);

		this.updateGeometricState(); // to get size

	}


	public boolean isChecked() {
		return this.tb_unit_name.isChecked();
	}


}

