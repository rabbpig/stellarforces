package com.scs.stellarforces.universe;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractOptionsModule2;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

import dsr.data.EquipmentData;
import dsr.data.UnitData;

public class RemoveUniverseItemModule extends AbstractOptionsModule2 {

	private UnitData unit;
	private EquipUniverseUnitControl ctrl;
	private EquipUniverseUnitsModule parent;

	private static Paint paint_normal_text = new Paint();

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));

	}

	public RemoveUniverseItemModule(AbstractActivity act, EquipUniverseUnitsModule equip_mod, UnitData _unit, EquipUniverseUnitControl _ctrl) {
		super(act, -1, 2, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, Statics.SCREEN_WIDTH * .4f, Statics.SCREEN_HEIGHT/7), -1, false, "Select Item to Remove", true);

		this.mod_return_to = equip_mod;

		unit = _unit;
		ctrl = _ctrl;
		parent = equip_mod;

		this.setBackground(Statics.BACKGROUND_R);
	}


	@Override
	public void getOptions() {
		for (EquipmentData eq : unit.items) {
			//if (eq.destroyed == false) {
			//if (eq.getUnitID() == unit.unitid) {
			addOption(eq.getName(false));
			//}
			//}
		}
		if (areThereAnyOptions() == false) {
			this.returnTo();
		}
	}


	@Override
	public void optionSelected(int idx) {
		String cmd = super.getButtonText(idx);
		for (EquipmentData eq : unit.items) {
			//if (eq.destroyed == false) { // Ignore destroyed items
			//if (eq.getUnitID() == unit.unitid) {
			if (eq.getName(false).equalsIgnoreCase(cmd)) {
				// Update inventory
				parent.addItemToInv(eq);
				
				// Remove from unit
				unit.items.remove(eq);
				//eq.destroyed = true;
				//eq.setUnitID(-1); // so it "disappears" when we send it to the server.
				ctrl.updateItemList();
				break; // Stop us selling the same item
			}
			//}
			//}
		}

		this.returnTo();
	}

}
