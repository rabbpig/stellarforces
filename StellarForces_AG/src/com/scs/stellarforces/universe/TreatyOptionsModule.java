package com.scs.stellarforces.universe;

import java.io.IOException;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.AbstractOptionsModule2;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class TreatyOptionsModule extends AbstractOptionsModule2 {

	private static final String CMD_ACCEPT = "Accept Treaty";
	private static final String CMD_DECLINE = "Decline Treaty";
	private static final String CMD_CANCEL = "Cancel Treaty";


	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.4f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT * 0.15f;

	//private DataTable treaty_data;
	private boolean accepted;
	private int treatyid;

	private static Paint paint_normal_text = new Paint();

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.055f));

	}


	public TreatyOptionsModule(AbstractActivity act, AbstractModule return_to, int _treatyid, boolean _accepted) {
		super(act, Statics.MOD_START, 2, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT), 0, false, "", false);

		this.mod_return_to = return_to;

		treatyid = _treatyid;
		accepted = _accepted;

		this.setBackground(Statics.BACKGROUND_R);
	}


	@Override
	public void getOptions() {
		if (accepted == false) {
			super.addOption(CMD_ACCEPT);
			super.addOption(CMD_DECLINE);
		} else {
			super.addOption(CMD_CANCEL);
		}

	}


	@Override
	public void optionSelected(int idx) {
		AbstractActivity act = Statics.act;
		
		String cmd = super.getActionCommand(idx);
		String req_end = "&tid=" + this.treatyid + "&comms_version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD);
		try {
		if (accepted == false) {
			// todo - send message to other player letting them know
			if (cmd.equalsIgnoreCase(CMD_ACCEPT)) {
				WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.ACCEPT_TREATY + req_end);
				if (wc.getResponseCode() == 200) {
					this.returnTo();
				}
			} else if (cmd.equalsIgnoreCase(CMD_DECLINE)) {
				WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.DECLINE_TREATY + req_end);
				if (wc.getResponseCode() == 200) {
					this.returnTo();
				}
			}
		} else {
			if (cmd.equalsIgnoreCase(CMD_CANCEL)) {
				WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.CANCEL_TREATY + req_end);
				if (wc.getResponseCode() == 200) {
					this.returnTo();
				}
			}
		}
		} catch (IOException ex) {
			AbstractActivity.HandleError(ex);
		}
	}

}
