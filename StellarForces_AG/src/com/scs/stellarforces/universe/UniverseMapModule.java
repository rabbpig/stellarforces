package com.scs.stellarforces.universe;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractComplexModule;
import ssmith.android.lib2d.shapes.Geometry;

import com.scs.stellarforces.Statics;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class UniverseMapModule extends AbstractComplexModule { // todo - show key

	public static final float PLANET_SIZE = Statics.SCREEN_HEIGHT/7f;

	private static DataTable uni_data;

	public UniverseMapModule() {
		super(Statics.MOD_UNIVERSE_MENU);
		
		this.setBackground(Statics.BACKGROUND_R);
		
	}


	@Override
	public void started() {
		AbstractActivity act = Statics.act;
		
		if (uni_data == null) { // todo - fresh after a time as well - and all the others!
			this.showPleaseWait("Getting universe map data...");
			try {
				WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.GET_UNIVERSE_MAP_DATA + "&comms_version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
				String response = wc.getResponse();
				//if (response.equalsIgnoreCase(MiscCommsPage.ERROR) == false) {
					uni_data = new DataTable(response);
					this.loadControls();
				/*} else {
					throw new IOException("Error from server.");
				}*/
			} catch (Exception ex) {
				this.dismissPleaseWait();
				AbstractActivity.HandleError(ex);
			} finally {
				this.dismissPleaseWait();
			}
		} else {
			this.loadControls();
		}
	}


	private void loadControls() {
		uni_data.moveBeforeFirst();
		while (uni_data.moveNext()) {
			UniverseMapPlanetIcon planet = new UniverseMapPlanetIcon(uni_data.getInt("MapX") * PLANET_SIZE, uni_data.getInt("MapY") * PLANET_SIZE, PLANET_SIZE, uni_data.getString("Name"), uni_data.getInt("Units"), uni_data.getInt("GameID"), uni_data.getInt("OwnerID"), uni_data.getInt("FriendlyStatus"), uni_data.getInt("CanDeploy"));
			this.root_node.attachChild(planet);
		}
		
		this.root_node.updateGeometricState();
	}


	@Override
	public boolean componentClicked(Geometry c) {
		AbstractActivity act = Statics.act;
		
		if (c instanceof UniverseMapPlanetIcon) {
			UniverseMapPlanetIcon planet = (UniverseMapPlanetIcon)c;
			this.getThread().setNextModule(new UniverseMapPlanetDetailsModule(act, planet, this));
			return true;
		}
		return false;
	}


	@Override
	public void updateGame(long interpol) {
		// Do nothing

	}

}
