package com.scs.stellarforces.universe;

import ssmith.android.lib2d.Camera;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import ssmith.android.lib2d.layouts.GridLayout;
import ssmith.android.lib2d.layouts.HorizontalFlowGridLayout;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

import dsr.data.EquipmentData;
import dsr.data.GameData;
import dsr.data.UnitData;

public class EquipUniverseUnitControl extends VerticalFlowLayout {
	
	private static final float WIDTH = Statics.SCREEN_WIDTH * 0.24f;/// 4;
	private static final float HEIGHT = Statics.SCREEN_HEIGHT * 0.13f;///5;//NEXT_PREV_WIDTH * 0.65f;
	
	private EquipUniverseUnitsModule parent;
	private UnitData unit;
	//private DataTable equipment_dt;
	private Label unit_name;
	private MultiLineLabel bought_items;
	private Button buy, remove_item;

	private static Paint paint_unit_name = new Paint();
	private static Paint paint_normal_text = new Paint();

	static {
		paint_unit_name.setARGB(255, 255, 255, 255);
		paint_unit_name.setAntiAlias(true);
		paint_unit_name.setTextSize(Statics.GetHeightScaled(0.06f));
		paint_unit_name.setStrokeWidth(Statics.SCREEN_WIDTH / 250f);

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.04f));

	}


	public EquipUniverseUnitControl(EquipUniverseUnitsModule _parent, UnitData _unit, float GAP, GameData game_data) { // DataTable _equipment_dt, 
		super("EquipUniverseUnitControl", GAP);

		parent = _parent;
		unit = _unit;
		//equipment_dt = _equipment_dt;

		/*Label l = new Label("Unit_name", unit.name, null, paint_unit_name, false);
		l.setSize(Statics.SCREEN_WIDTH * 0.4f, NEXT_PREV_HEIGHT);
		l.collides = true; // TO select unit stats
		this.attachChild(l);
		*/
		
		// Row 1
		GridLayout hfgl1 = new GridLayout("hfgl1", WIDTH, HEIGHT, GAP);
		unit_name = new Label("unit_name", unit.order_by + ":" + unit.name, null, paint_unit_name, false);
		hfgl1.attachChild(unit_name, 0, 0);
		buy = new Button("Buy Item", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_green, WIDTH, HEIGHT));
		hfgl1.attachChild(buy, 2, 0);
		remove_item = new Button("Remove Item", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_blue, WIDTH, HEIGHT));
		hfgl1.attachChild(remove_item, 3, 0);
		this.attachChild(hfgl1);

		// Row 2
		HorizontalFlowGridLayout hfgl2 = new HorizontalFlowGridLayout("hfgl1", GAP);
		bought_items = new MultiLineLabel("bought_items", "", null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.7f);
		hfgl2.attachChild(bought_items);
		this.attachChild(hfgl2);
		
		this.updateItemList();

	}
	
	
	public void updateItemList() {
		// Show equipment
		StringBuffer str = new StringBuffer();
		//str.append("Unit has: ");
		boolean any = false;
		for (EquipmentData eq : unit.items) {
			//if (eq.destroyed == false) {
				//if (eq.getUnitID() == unit.unitid) {
					str.append(eq.getName(false) + ", ");
					any = true;
				//}
			//}
		}
		if (any) {
			str.delete(str.length()-2, str.length());
		} else {
			str.append("*Nothing*");
		}
		bought_items.setText(str.toString());

	}
	
	
	public void handleClick(AbstractComponent c) throws Exception {
		if (c == this.buy) {
			parent.chooseItem(this.unit);
			this.updateItemList();
		} else if (c == this.remove_item) {
			parent.removeItem(unit, this);
			//this.updateItemList();
		}
	}
	
	
	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		g.drawBitmap(Statics.img_cache.getImage(R.drawable.button_blue_darker, this.getWidth(), this.getHeight()), this.getScreenX(cam), this.getScreenY(cam), null);

		super.doDraw(g, cam, interpol);
		
		//g.drawLine(this.getScreenX(cam), this.getScreenY(cam)+this.getHeight(), this.getScreenX(cam) + this.getWidth(), this.getScreenY(cam) + this.getHeight(), paint_unit_name);
	}	

}
