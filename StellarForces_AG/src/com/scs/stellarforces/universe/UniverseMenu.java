package com.scs.stellarforces.universe;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractOptionsModule2;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public class UniverseMenu extends AbstractOptionsModule2 {

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.4f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT * 0.15f;

	private static final String CMD_MAP = "Universe Map";
	private static final String CMD_TREATIES = "Treaties";
	private static final String CMD_TRADE = "Trade";
	private static final String CMD_CONTRACTS = "Contracts";
	private static final String CMD_SQUAD = "Squad";

	private static Paint paint_normal_text = new Paint();

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.055f));

	}

	public UniverseMenu(AbstractActivity act) {
		super(act, Statics.MOD_START, 2, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT), 0, false, "", false);

		this.setBackground(Statics.BACKGROUND_R);

		start();
	}
	

	@Override
	public void getOptions() {
		super.addOption(CMD_MAP);
		super.addOption(CMD_TREATIES);
		super.addOption(CMD_TRADE);
		super.addOption(CMD_CONTRACTS);
		super.addOption(CMD_SQUAD);
	}

	
	@Override
	public void optionSelected(int idx) {
		AbstractActivity act = Statics.act;
		
		String cmd = super.getActionCommand(idx);
		if (cmd.equalsIgnoreCase(CMD_MAP)) {
			this.getThread().setNextModule(new UniverseMapModule());
		} else if (cmd.equalsIgnoreCase(CMD_TREATIES)) {
			//todo
		} else if (cmd.equalsIgnoreCase(CMD_TRADE)) {
			//todo
		} else if (cmd.equalsIgnoreCase(CMD_SQUAD)) {
			//todo
		}
	}



}
