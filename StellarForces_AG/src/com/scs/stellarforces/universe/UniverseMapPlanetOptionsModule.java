package com.scs.stellarforces.universe;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.AbstractOptionsModule2;
import ssmith.android.framework.modules.ConfirmModule;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public class UniverseMapPlanetOptionsModule extends AbstractOptionsModule2 {

	private static final String OPT_SEND_UNITS = "Send Units";
	private static final String OPT_EXTRACT_UNITS = "Extract Units";

	private static final String CONFIRM_EXTRACT = "conf_extract";

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.8f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT * 0.15f;
	
	private UniverseMapPlanetIcon planet;

	private static Paint paint_normal_text = new Paint();
	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.055f));

	}
	
	public UniverseMapPlanetOptionsModule(AbstractActivity act, AbstractModule return_to, UniverseMapPlanetIcon _planet) {
		super(act, -1, 1, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT), 0, false, "", false);

		this.mod_return_to = return_to;
		planet = _planet;
		
		this.setBackground(Statics.BACKGROUND_R);
	}

	
	@Override
	public void started() {
		super.started();

		// Have we come back from "confirmed"?
		if (Statics.data.containsKey(CONFIRM_EXTRACT)) {
			String s = Statics.data.get(CONFIRM_EXTRACT);
			Statics.data.clear();
			if (s.equalsIgnoreCase("yes")) {
				// todo
				this.returnTo();
			}
		}
	}
	
	
	@Override
	public void getOptions() {
		if (planet.can_deploy == 1) {
			this.addOption(OPT_SEND_UNITS);
		}
		if (planet.num_our_units > 0) {
			this.addOption(OPT_EXTRACT_UNITS);
		}
	}

	
	@Override
	public void optionSelected(int idx) {
		AbstractActivity act = Statics.act;
		
		String cmd = super.getActionCommand(idx);
		if (cmd.equalsIgnoreCase(OPT_SEND_UNITS)) {
			this.getThread().setNextModule(new UniverseSquadModule(this, true, planet));
		} else if (cmd.equalsIgnoreCase(OPT_EXTRACT_UNITS)) {
			this.getThread().setNextModule(new ConfirmModule(act, this, "Are You Sure?", "Are you sure you wish to extract all your units?", Statics.BACKGROUND_R, CONFIRM_EXTRACT));
		}
		
	}
	

}
