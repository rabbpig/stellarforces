package com.scs.stellarforces.universe;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.AbstractOptionsModule2;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class TreatiesModule extends AbstractOptionsModule2 {

	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.4f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT * 0.15f;

	private DataTable treaty_data;

	private static Paint paint_normal_text = new Paint();

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.055f));

	}


	public TreatiesModule(AbstractActivity act, AbstractModule return_to) {
		super(act, -1, 2, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT), 0, false, "", false);

		this.mod_return_to = return_to;

		this.setBackground(Statics.BACKGROUND_R);
	}


	@Override
	public void started() {
		AbstractActivity act = Statics.act;
		
		if (treaty_data == null) {
			this.showPleaseWait("Getting treaty data..."); // refresh after a while
			try {
				WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.GET_TREATIES_DATA + "&comms_version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
				if (wc.getResponseCode() == 200) {
					String response = wc.getResponse();
					treaty_data = new DataTable(response);
					this.setOptions();
				}
			} catch (Exception ex) {
				this.dismissPleaseWait();
				AbstractActivity.HandleError(ex);
			} finally {
				this.dismissPleaseWait();
			}
		}
	}


	@Override
	public void getOptions() {
		treaty_data.moveBeforeFirst();
		while (treaty_data.moveNext()) {
			String s = "Treaty with " + treaty_data.getString("ComradeName");
			if (treaty_data.getInt("Accepted") != 1) {
				s = s + " (Pending)";
			}
			super.addOption(s, treaty_data.getInt("UniverseTreatyID"));
		}
	}


	@Override
	public void optionSelected(int idx) {
		AbstractActivity act = Statics.act;
		
		int cmd = Integer.parseInt(super.getActionCommand(idx));
		treaty_data.find("UniverseTreatyID", cmd);
		this.getThread().setNextModule(new TreatyOptionsModule(act, this, treaty_data.getInt("UniverseTreatyID"), treaty_data.getInt("Accepted") == 1));
	}

}
