package com.scs.stellarforces.universe;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public class UniverseMapPlanetDetailsModule extends SimpleScrollingAbstractModule {
	
	private static final float ICON_WIDTH = Statics.SCREEN_WIDTH * 0.4f;
	private static final float ICON_HEIGHT = Statics.SCREEN_HEIGHT * 0.15f;

	private UniverseMapPlanetIcon planet;
	private Button btn_show_full;

	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();
	
	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));

	}


	public UniverseMapPlanetDetailsModule(AbstractActivity act, UniverseMapPlanetIcon _planet, AbstractModule _return_to) {
		super(-1);
		
		planet = _planet;
		this.mod_return_to = _return_to;

		this.setBackground(Statics.BACKGROUND_R);

		Label l = new Label("Title", planet.getName(), 0, 0, null, paint_large_text, true);
		l.setCentre(Statics.SCREEN_WIDTH/2, paint_large_text.getTextSize());
		this.root_node.attachChild(l);

		StringBuffer str = new StringBuffer();
		
		if (planet.friendly_status == UniverseMapPlanetIcon.FS_OWNED_BY_US) {
			str.append("You own this planet.");
		} else if (planet.friendly_status == UniverseMapPlanetIcon.FS_OWNED_BY_COMRADE) {
			str.append("This planet is owned by a comrade.");
		} else if (planet.friendly_status == UniverseMapPlanetIcon.FS_OWNED_BY_ENEMY) {
			str.append("This planet is owned by an enemy.");
		} else if (planet.friendly_status == UniverseMapPlanetIcon.FS_UNOWNED) {
			str.append("This planet is unowned.");
		}
		
		MultiLineLabel label2 = new MultiLineLabel("credits", str.toString(), null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
		label2.setLocation(10, Statics.SCREEN_HEIGHT * 0.15f);
		root_node.attachChild(label2);

		btn_show_full = new Button("Options", "Options", 0, 0, null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R, ICON_WIDTH, ICON_HEIGHT));
		btn_show_full.setLocation(Statics.SCREEN_WIDTH - ICON_WIDTH, Statics.SCREEN_HEIGHT -ICON_HEIGHT);
		this.stat_node.attachChild(btn_show_full);
		
		this.stat_node.updateGeometricState();
		this.root_node.updateGeometricState();
	}
	

	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		AbstractActivity act = Statics.act;
		
		if (c == this.btn_show_full) {
			this.getThread().setNextModule(new UniverseMapPlanetOptionsModule(act, this, planet));
		}
		
	}

}
