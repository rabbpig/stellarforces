package com.scs.stellarforces.universe;

import ssmith.android.lib2d.Camera;
import ssmith.android.lib2d.shapes.AbstractRectangle;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

public class UniverseMapPlanetIcon extends AbstractRectangle {

	public static final int FS_UNOWNED = 0;
	public static final int FS_OWNED_BY_US = 1;
	public static final int FS_OWNED_BY_COMRADE = 2;
	public static final int FS_OWNED_BY_ENEMY = 3;
	// ## IF YOU ADD TO THIS, ADD TO UniverseComms.class

	private static Bitmap bmp_planet;
	public int num_our_units, gameid, ownerid, friendly_status, can_deploy;
	private String text = "";

	private static Paint paint_normal_text = new Paint();

	static {
		bmp_planet = Statics.img_cache.getImage(R.drawable.saturn, UniverseMapModule.PLANET_SIZE, UniverseMapModule.PLANET_SIZE);

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.05f));
	}

	
	public UniverseMapPlanetIcon(float x, float y, float size, String name, int _units, int _gameid, int _ownerid, int _friendly_status, int _can_deploy) {
		super(name, null);

		num_our_units = _units;
		gameid = _gameid;
		ownerid = _ownerid;
		friendly_status = _friendly_status;
		can_deploy = _can_deploy;
		
		text = FriendlyStatus2String(friendly_status);

		this.setByLTRB(x, y, x+size, y+size);
	}


	public int getGameID() {
		return this.gameid;
	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		g.drawBitmap(bmp_planet, this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);
		if (num_our_units > 0) {
			g.drawText(num_our_units+"", this.world_bounds.left - cam.left, this.world_bounds.top - cam.top + paint_normal_text.getTextSize(), paint_normal_text);
		}
		g.drawText(text, this.world_bounds.left - cam.left, this.world_bounds.top - cam.top + UniverseMapModule.PLANET_SIZE, paint_normal_text);
	}


	public static String FriendlyStatus2String(int i) {
		switch (i) {
		case FS_UNOWNED: return "U";
		case FS_OWNED_BY_US: return "O";
		case FS_OWNED_BY_COMRADE: return "T";
		case FS_OWNED_BY_ENEMY: return "E";
		default: throw new RuntimeException("Unknown Friendly Status: " + i);
		}

	}

}
