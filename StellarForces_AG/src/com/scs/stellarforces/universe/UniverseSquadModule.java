package com.scs.stellarforces.universe;

import java.io.IOException;
import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.framework.modules.SimpleScrollingAbstractModule;
import ssmith.android.lib2d.Spatial;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.CheckBox;
import ssmith.android.lib2d.gui.ToggleButton;
import ssmith.android.lib2d.layouts.HorizontalFlowGridLayout;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.campaign.CampUnitSelectionControl;
import com.scs.stellarforces.main.lite.R;

import dsr.comms.AbstractCommFuncs;
import dsr.comms.DataTable;
import dsr.comms.WGet_SF;
import dsr.data.UnitData;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class UniverseSquadModule extends SimpleScrollingAbstractModule {

	//private static final String CONFIRM_SQUAD = "conf_squad";

	private static final float GAP = Statics.SCREEN_WIDTH * 0.005f;

	private static Paint paint_normal_text = new Paint();
	private DataTable uni_squad_dt;
	private static boolean select_squad;
	private Button finished, finished2;
	private VerticalFlowLayout vfl;
	//private int gameid; // For when selecting units to deploy.  -1 otherwise
	private UniverseMapPlanetIcon planet;

	static {
		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.07f));

	}


	public UniverseSquadModule(AbstractModule return_to, boolean _select, UniverseMapPlanetIcon _planet) {
		super(-1);

		this.mod_return_to = return_to;
		select_squad = _select;
		//gameid = _gameid;
		planet = _planet;

		this.setBackground(Statics.BACKGROUND_R);


	}


	@Override
	public void started() {
		AbstractActivity act = Statics.act;
		
		// Have we come back from "confirmed"?
		/*if (Statics.data.containsKey(CONFIRM_SQUAD)) {
			String s = Statics.data.get(CONFIRM_SQUAD);
			Statics.data.clear();
			if (s.equalsIgnoreCase("yes")) {
				this.finish();
				return;
			}
		}*/

		this.showPleaseWait("Getting squad roster..."); // todo - only refresh this every so often?
		try {
			WGet_SF wc = new WGet_SF(act, null, "cmd=" + MiscCommsPage.GET_UNI_SQUAD_LIST + "&comms_version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD));
			String response = wc.getResponse();
			if (response != null && response.length() > 0) {
				if (response.equalsIgnoreCase("error") == false) {
					uni_squad_dt = new DataTable(response);
				} else {
					throw new IOException("Invalid data for universe squad");
				}
			} else {
				throw new IOException("Invalid data for universe squad");
			}
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
			return;
		} finally {
			this.dismissPleaseWait();
		}

		this.root_node.removeAllChildren();

		vfl = new VerticalFlowLayout("vfl", GAP);

		if (select_squad) {
			finished = new Button("Deploy Selected Units", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_green, Statics.SCREEN_WIDTH, Statics.SCREEN_HEIGHT/7f));
			vfl.attachChild(finished);
		}

		HorizontalFlowGridLayout heading = new HorizontalFlowGridLayout("heading", GAP);
		Button tb_unit_name = new Button("Name", null, UniverseSquadSelectionControl.paint_unit_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.OVERALL_WIDTH * 0.5f, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		//tb_unit_name.setSize(OVERALL_WIDTH * 0.6f, ARMOUR_ICON_HEIGHT);
		heading.attachChild(tb_unit_name);

		// why are these togglebuttons?
		ToggleButton b = new ToggleButton("Hlth", null, null, UniverseSquadSelectionControl.paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		heading.attachChild(b);
		b = new ToggleButton("APs", null, null, UniverseSquadSelectionControl.paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		heading.attachChild(b);
		b = new ToggleButton("Shot", null, null, UniverseSquadSelectionControl.paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		heading.attachChild(b);
		b = new ToggleButton("CC", null, null, UniverseSquadSelectionControl.paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		heading.attachChild(b);
		b = new ToggleButton("Str", null, null, UniverseSquadSelectionControl.paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		heading.attachChild(b);
		b = new ToggleButton("Nrg", null, null, UniverseSquadSelectionControl.paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		heading.attachChild(b);
		b = new ToggleButton("Mrl", null, null, UniverseSquadSelectionControl.paint_armour_name, Statics.img_cache.getImage(Statics.BUTTON_R, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT), Statics.img_cache.getImage(R.drawable.button_blue_darker, CampUnitSelectionControl.ARMOUR_ICON_WIDTH, CampUnitSelectionControl.ARMOUR_ICON_HEIGHT));
		heading.attachChild(b);
		heading.updateGeometricState(); // to get size
		vfl.attachChild(heading);

		uni_squad_dt.moveBeforeFirst();
		while (uni_squad_dt.moveNext()) {
			UniverseSquadSelectionControl unit_ctrl = new UniverseSquadSelectionControl(GAP, select_squad, uni_squad_dt.getInt("UniverseUnitID"), uni_squad_dt.getString("Name"), ""+uni_squad_dt.getInt("MaxHealth"), ""+uni_squad_dt.getInt("MaxAPs"), ""+uni_squad_dt.getInt("ShotSkill"), ""+uni_squad_dt.getInt("CombatSkill"), ""+uni_squad_dt.getInt("Strength"), ""+uni_squad_dt.getInt("MaxEnergy"), ""+uni_squad_dt.getInt("MaxMorale"));
			vfl.attachChild(unit_ctrl);
		}
		vfl.updateGeometricState();

		HorizontalFlowGridLayout hfgl = new HorizontalFlowGridLayout("hfgl", Statics.SCREEN_WIDTH * 0.02f);
		hfgl.updateGeometricState();
		vfl.attachChild(hfgl);

		if (select_squad) {
			finished2 = new Button("Deploy Selected Units", null, paint_normal_text, Statics.img_cache.getImage(R.drawable.button_green, Statics.SCREEN_WIDTH, Statics.SCREEN_HEIGHT/7f));
			vfl.attachChild(finished2);
		}

		this.root_node.attachChild(vfl);
		this.root_node.updateGeometricState();
		this.root_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);
	}


	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		if (c instanceof CheckBox) {
			CheckBox cb = (CheckBox)c;
			cb.toggle();
		} else if (c == finished || c == finished2) {
			//confirmIfFinished();
			this.finish();
		}

	}


	/*private void confirmIfFinished() {
		this.getThread().setNextModule(new ConfirmModule(act, this, "Are You Sure?", "Are you sure you have definitely finished selecting your units?  You can't go back afterwards!", Statics.BACKGROUND_R, CONFIRM_SQUAD));
	}*/


	private void finish() {
		AbstractActivity act = Statics.act;
		
		// Tell the server who we've selected
		ArrayList<UnitData> units = new ArrayList<UnitData>();
		for (Spatial s : vfl.getChildren()) {
			if (s instanceof UniverseSquadSelectionControl) {
				UniverseSquadSelectionControl cb = (UniverseSquadSelectionControl)s;
				if (cb.isChecked()) {
					uni_squad_dt.find("UniverseUnitID", cb.uniunitid);
					UnitData unit = new UnitData(cb.uniunitid, uni_squad_dt.getString("Name"));
					units.add(unit);
				}
			}
		}

		super.getThread().setNextModule(new GetUniverseEquipmentDataModule(act, planet, units));

	}


}

