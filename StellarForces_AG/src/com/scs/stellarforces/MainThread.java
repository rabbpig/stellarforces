package com.scs.stellarforces;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.MyEvent;
import ssmith.android.framework.keyboard.KeyboardActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.lang.Functions;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.KeyEvent;
import android.view.SurfaceHolder;

import com.scs.stellarforces.start.ErrorModule;
import com.scs.stellarforces.start.StartupModule;

/**
 * This class should be usable by ANY android game.
 *
 */
public final class MainThread extends Thread {

	private static final long ONBACK_GAP = 200;

	private static Paint paint_black_fill = new Paint();

	private ArrayList<MyEvent> events = new ArrayList<MyEvent>();

	/** Indicate whether the surface has been created & is ready to draw */
	protected boolean mRun = false;

	/** Handle to the surface manager object we interact with */
	private SurfaceHolder mSurfaceHolder;

	private boolean paused = false;

	public AbstractModule module;
	public String error = "";
	public AbstractModule next_module;
	private long last_onback_pressed;
	
	//public static boolean show_keyboard = false;
	//public static boolean showing_keyboard = false;

	static {
		paint_black_fill.setARGB(255, 0, 0, 0);
		paint_black_fill.setAntiAlias(true);
		paint_black_fill.setStyle(Style.FILL);

	}

	
	public MainThread() {
		super("MainThread");

		synchronized (events) {
			events.clear();
		}

	}


	@Override
	public void run() {
		while (mRun) {
			AbstractActivity act = Statics.act;
			
			long start = System.currentTimeMillis();
			if (paused == false && mSurfaceHolder != null && Statics.initd && KeyboardActivity.showing_keyboard == false) {
				updateGame();
				doDrawing();
			}
			if (KeyboardActivity.show_keyboard && KeyboardActivity.showing_keyboard == false && this.module != null) {
				KeyboardActivity.show_keyboard = false;
				Intent i = new Intent(act, KeyboardActivity.class);  
				act.startActivityForResult(i, 1);
				KeyboardActivity.showing_keyboard = true;
			}
			long diff = System.currentTimeMillis() - start;
			Functions.delay(Statics.LOOP_DELAY - diff);
		}
		AbstractActivity.Log("Thread ended...");
	}


	public void doDrawing() {
		Canvas c = null;
		try {
			c = mSurfaceHolder.lockCanvas(null);
			if (c != null) { // In case the surface is the "sleep" surface
				synchronized (mSurfaceHolder) {
					//doDraw(c);//interpol);
					c.drawRect(0, 0, Statics.SCREEN_WIDTH, Statics.SCREEN_HEIGHT, paint_black_fill);

					if (module != null) {
						module.doDraw(c, Statics.LOOP_DELAY);
					}
				}
			}
		} catch (IllegalStateException ex) {
			// Do nothing
			ex.printStackTrace();
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
		} finally {
			// do this in a finally so that if an exception is thrown
			// during the above, we don't leave the Surface in an
			// inconsistent state
			if (c != null) {
				try {
					mSurfaceHolder.unlockCanvasAndPost(c); // Errors with android.view.Surface$CompatibleCanvas@46174090
				} catch (Exception ex) {
					ex.printStackTrace();
					paused = false; // Otherwise it gets stuck   paused = false;
				}
			}
		}
	}


	public void setPause(boolean p) {
		this.paused = p;
	}
	
	
	public boolean isPaused() {
		return this.paused;
	}


	public boolean onKeyDown(int keyCode, KeyEvent msg) {
		if (this.module != null) {
			return this.module.onKeyDown(keyCode, msg);
		}
		return false;
	}


	public boolean onKeyUp(int keyCode, KeyEvent msg) {
		if (this.module != null) {
			return this.module.onKeyUp(keyCode, msg);
		}
		return false;
	}


	public boolean onBackPressed() {
		if (this.module != null) {
			if (System.currentTimeMillis() > this.last_onback_pressed + ONBACK_GAP) {
				last_onback_pressed = System.currentTimeMillis();
				return this.module.onBackPressed();
			} else {
				return true;
			}
		} else {
			return false;
		}
	}


	public void addMotionEvent(MyEvent ev) {
		synchronized (events) {
			this.events.add(ev);
		}
	}


	/**
	 * Used to signal the thread whether it should be running or not.
	 * Passing true allows the thread to run; passing false will shut it
	 * down if it's already running. Calling start() after this was most
	 * recently called with false will result in an immediate shutdown.
	 * 
	 * @param b true to run, false to shut down
	 */
	public void setRunning(boolean b) {
		mRun = b;
	}


	public void setNextModule(AbstractModule m) {
		if (this.next_module == null || m instanceof ErrorModule) {  // So if we've got one lined up, it doesn't get overridden
			this.next_module = m;
		}
	}


	public void doDraw(Canvas c) {
		// Clear the background
		c.drawRect(0, 0, Statics.SCREEN_WIDTH, Statics.SCREEN_HEIGHT, paint_black_fill);

		// Draw any error
		/*if (error.length() > 0) {
			c.drawText(error, 5, this.view.canvas_height - 20, Painters.paint_white_line);
		} else {
			//c.drawText("no error", 5, 20, Painters.paint_white_line);
		}*/

		if (module != null) {
			module.doDraw(c, Statics.LOOP_DELAY);
		}
		/*if (Statics.DEBUG) {
			Bitmap bmp = Statics.img_cache.getImage(R.drawable.unit_highlighter, 50, 50);
			if (bmp != null) {
				c.drawBitmap(bmp, 10, 10, null);
			}
			bmp = Statics.img_cache.getImage(R.drawable.human_s2_n, 50, 50);
			if (bmp != null) {
				c.drawBitmap(bmp, 100, 100, null);
			}
			bmp = Statics.img_cache.getImage(R.drawable.human_s1_n, 50, 50);
			if (bmp != null) {
				c.drawBitmap(bmp, 200, 200, null);
			}
		}*/
	}


	protected void updateGame() {
		if (next_module != null) {
			if (Statics.initd) { // Don't do anything until we've got the display system!
				if (this.module != null) {
					this.module.stopped();
				}
				this.module = next_module;
				next_module = null;
				this.module.started();
				synchronized (events) {
					this.events.clear();
				}
			}
		}

		if (module != null) {
			// Process events
			if (this.events.size() > 0) {
				while (this.events.size() > 0) {
					MyEvent ev = null;
					synchronized (events) {
						ev = this.events.remove(0);
					}
					try {
						if (ev != null) {
							if (module.processEvent(ev)) {
								synchronized (events) {
									this.events.clear();
								}
							}
						}
					} catch (Exception ex) {
						AbstractActivity.HandleError(ex);
					}
				}
			}
			module.updateGame(Statics.LOOP_DELAY);
		} else {
			if (Statics.initd) { // Don't do anything until we've got the display system!
				// Load default module
				//this.setNextModule(new IntroModule(Statics.act)); Uses up too much memory?
				this.setNextModule(new StartupModule(Statics.act));
			}

		}
	}


	public void setSurfaceHolder(SurfaceHolder holder) {
		this.mSurfaceHolder = holder;
	}

}

