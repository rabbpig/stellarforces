package com.scs.stellarforces;

import java.io.File;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.android.util.ImageCache;
import ssmith.lang.NumberFunctions;
import ssmith.util.MyHashtable;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.scs.stellarforces.main.lite.R;
import com.scs.stellarforces.message.ListMessagesModule;
import com.scs.stellarforces.start.FreeWarningModule;
import com.scs.stellarforces.start.GetGamesModule;
import com.scs.stellarforces.start.LoginModule;
import com.scs.stellarforces.start.MenuModule;
import com.scs.stellarforces.start.MoreMenuModule;
import com.scs.stellarforces.start.RegisterLoginWithServerModule;
import com.scs.stellarforces.start.RegisterModule2;
import com.scs.stellarforces.start.StartOrJoinGameModule;
import com.scs.stellarforces.start.StartupModule;
import com.scs.stellarforces.start.ValidateLoginModule;
import com.scs.stellarforces.start.joingame.ListAvailableGamesModule;
import com.scs.stellarforces.start.newgame.StartNewGameModule;
import com.scs.stellarforces.start.newgame.StartPractiseGameModule;
import com.scs.stellarforces.universe.UniverseMenuModule;

import dsr.comms.DataTable;

public final class Statics {

	public static final String OVERRIDE_URL_FOR_CLIENT = ""; //"http://10.0.0.18:8083"; // Do NOT add a slash to the end
	public static final boolean USE_DEF_LOGIN = false;
	public static final boolean DEBUG = false;
	public static final boolean RELEASE_MODE = true; // False makes the code stricter to find errors.
	public static final boolean ANDROID_MARKET = true;
	public static final boolean UNIVERSE_MODULE = false; // New module
	public static final boolean DEBUG_APRIL_FOOLS = false;

	public static final String DEF_LOGIN = "";
	public static final String DEF_PWD = "";


	// Modules
	public static final int MOD_START = 0;
	public static final int MOD_LOGIN = 1;
	public static final int MOD_GET_PLAYERS_GAMES = 2;
	public static final int MOD_MENU = 4;
	public static final int MOD_VALIDATE_LOGIN = 5;
	public static final int MOD_ENTER_NEW_LOGIN_FOR_REGISTER = 6;
	public static final int MOD_REGISTER_NEW_LOGIN_WITH_SERVER = 7;
	public static final int MOD_LIST_AVAILABLE_GAMES = 8; // For joining a game
	public static final int MOD_START_OR_JOIN_GAME = 9;
	public static final int MOD_START_NEW_GAME = 10;
	public static final int MOD_LIST_MESSAGES = 11;
	public static final int MOD_START_PRACTISE_GAME = 12;
	public static final int MOD_MORE = 13;
	public static final int MOD_UNIVERSE_MENU = 14;

	public static String URL_FOR_CLIENT;// = "http://www.stellarforces.com"; // Do NOT add a slash to the end
	public static int VERSION_NUM;
	public static String NAME;
	public static String LAST_LOGIN = "";
	public static String LAST_PWD = "";
	public static String EMAIL; //= "support@stellarforces.com";
	public static final String PREFS_NAME = "sf_prefs.txt";

	// Settings
	public static int VIBRATE_ON_PRESS = 0;
	public static int MUTE_MUSIC = 0;
	public static int MUTE_SFX = 0;
	public static int USE_NEW_MOVEMENT_ICONS = 0;

	public static float COMMS_VERSION = 1.86f; // Update for equipping
	public static final int LOOP_DELAY = 30;
	public static final int VIEW_ANGLE = 49; // Don't make 45 as it means the white line must be exactly inside the field of view
	public static final int VIBRATE_LEN = 35;
	public static final float LABEL_SPACING = 1.2f;
	public static final String USER_AGENT = "StellarForces Client"; // Use this so the server knows it's Android!
	public static boolean FULL_VERSION = false; // Gets changed automatically
	public static boolean CHECK_FOR_TURNS = true;
	public static String MARKET_URL_FREE_VERSION, MARKET_URL_FULL_VERSION;
	public static int BACKGROUND_R, BUTTON_R, BUTTON_R_OVAL;
	public static String FREE_TEXT = null;
	public static final long CAM_VIB_DUR = 1000l;
	public static final int TYPE_SFX = R.raw.type;
	public static final int MUSIC_R = -1;//R.raw.laser_squad_mission_recalled_oc_remix;

	public static DataTable players_data_tbl;
	public static DataTable treaty_tbl;

	public static Typeface tf;

	// Fields for passing data
	public static MyHashtable<String, String> data = new MyHashtable<String, String>(); // For passing data

	// Sizes
	public static float SCREEN_WIDTH;
	public static float SCREEN_HEIGHT;

	private static final float ICONS_WIDTH_PCENT = 12f;
	public static float ICONS_WIDTH;
	private static final float ICONS_HEIGHT_PCENT = 15f;
	public static float ICONS_HEIGHT;

	private static final float SQ_SIZE_PCENT = 10f;
	public static float SQ_SIZE;
	public static int SQ_SIZE_INT;

	private static final float BULLET_SIZE_PCENT = 1f;
	public static float BULLET_SIZE;
	private static final float BULLET_MOVE_DIST_PCENT = 0.15f; // Was 0.1f;
	public static float BULLET_MOVE_DIST;
	private static final float UNIT_SPEED_PCENT = 0.05f;
	public static float UNIT_SPEED;
	//private static final float EXPLODING_CTRLS_PCENT = 5f;
	//public static float EXPLODING_CTRLS_SPEED;

	public static boolean initd = false;
	public static ImageCache img_cache;
	public static AbstractActivity act;
	public static int clientid = NumberFunctions.rnd(1, 999999);


	public static void init(float screen_width, float screen_height, Context context) {
		if (initd == false) {
			img_cache = new ImageCache(context.getResources());

			if (act != null) {
				Point dim = act.getScreenSize();
				if (dim != null) {
					screen_width = dim.x;
					screen_height = dim.y;
				}
			}

			if (screen_width < screen_height) {
				// Swap them
				float tmp = screen_width;
				screen_width = screen_height;
				screen_height = tmp;
			}

			if (OVERRIDE_URL_FOR_CLIENT.length() > 0) {
				Statics.URL_FOR_CLIENT = OVERRIDE_URL_FOR_CLIENT;
			}

			SCREEN_WIDTH = screen_width;
			SCREEN_HEIGHT = screen_height;

			ICONS_WIDTH = screen_width * (ICONS_WIDTH_PCENT/100);
			ICONS_HEIGHT = screen_height * (ICONS_HEIGHT_PCENT/100);

			SQ_SIZE = screen_height * (SQ_SIZE_PCENT/100);
			SQ_SIZE_INT = (int)Math.ceil(Statics.SQ_SIZE);
			BULLET_SIZE = screen_height * (BULLET_SIZE_PCENT/100);
			BULLET_MOVE_DIST = screen_height * (BULLET_MOVE_DIST_PCENT/100);
			UNIT_SPEED = screen_height * (UNIT_SPEED_PCENT/100);

			initd = true;
		}
	}


	public static float GetHeightScaled(float frac) {
		return SCREEN_HEIGHT * frac;
	}


	public static float GetWidthScaled(float frac) {
		return SCREEN_WIDTH * frac;
	}


	public static void LoadPrefs(Context c) {
		SharedPreferences settings = c.getSharedPreferences(PREFS_NAME, 0);
		Statics.LAST_LOGIN = settings.getString("last_login", "").trim();
		Statics.LAST_PWD = settings.getString("last_pwd", "").trim();
		Statics.VIBRATE_ON_PRESS = settings.getInt("vibrate_on_press", 0);
		Statics.MUTE_MUSIC = settings.getInt("mute_music", 0);
		Statics.MUTE_SFX = settings.getInt("mute_sfx", 0);
		Statics.USE_NEW_MOVEMENT_ICONS = settings.getInt("use_new_movement_icons", 0);
	}


	public static void SavePrefs(Context c) {
		// Save preferences 
		SharedPreferences settings = c.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();

		editor.putString("last_login", Statics.LAST_LOGIN);
		editor.putString("last_pwd", Statics.LAST_PWD);
		editor.putInt("vibrate_on_press", Statics.VIBRATE_ON_PRESS);
		editor.putInt("mute_music", Statics.MUTE_MUSIC);
		editor.putInt("mute_sfx", Statics.MUTE_SFX);
		editor.putInt("use_new_movement_icons", Statics.USE_NEW_MOVEMENT_ICONS);

		// Don't forget to commit your edits!!!
		editor.commit();

	}


	/**
	 * We need this to avoid having to create a new module for the return_to
	 */
	public static AbstractModule GetModule(int type) {
		switch (type) {
		case MOD_START:
			return new StartupModule(act);
		case MOD_LOGIN:
			return new LoginModule(act);
		case MOD_GET_PLAYERS_GAMES:
			return new GetGamesModule(act, false);
		case MOD_MENU:
			return new MenuModule(act);
		case MOD_VALIDATE_LOGIN:
			return new ValidateLoginModule(act);
		case MOD_ENTER_NEW_LOGIN_FOR_REGISTER:
			return new RegisterModule2(act);
		case MOD_REGISTER_NEW_LOGIN_WITH_SERVER:
			return new RegisterLoginWithServerModule(act);
		case MOD_LIST_AVAILABLE_GAMES:
			return new ListAvailableGamesModule(act);
		case MOD_START_OR_JOIN_GAME:
			return new StartOrJoinGameModule(act);
		case MOD_START_NEW_GAME:
			return new StartNewGameModule(act);
		case MOD_LIST_MESSAGES:
			return new ListMessagesModule(act);
		case MOD_START_PRACTISE_GAME:
			return new StartPractiseGameModule(act);
		case MOD_MORE:
			return new MoreMenuModule(act);
		case MOD_UNIVERSE_MENU:
			return new UniverseMenuModule(act);
			/*case MOD_CAMP_MENU:
			return new CampaignMenuModule(act);*/
		default:
			throw new RuntimeException("Unknown module type: " + type);
		}
	}


	public static File GetExtStorageDir() {
		File f = new File(Environment.getExternalStorageDirectory() + "/" + NAME.replaceAll(" ", ""));// + "/");
		if (f.exists() == false) {
			f.mkdirs();
		}
		if (f.exists() == false) {
			throw new RuntimeException("Unable to save to ext storage");
		}
		return f;

	}


	public static AbstractModule GetFreeWarningModule(AbstractModule return_to) {
		return new FreeWarningModule(act, return_to);
	}


	public static float GetButtonSpacing() {
		return Statics.SCREEN_HEIGHT/60;
	}


	public static Typeface GetTypeface(Activity act) {
		try {
			if (tf == null) {
				tf = Typeface.createFromAsset(act.getApplicationContext().getAssets(), "fonts/JLSDataGothicC_NC.otf");
			}
			return tf;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;

	}


}
