package com.scs.stellarforces;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.io.IOFunctions;
import ssmith.lang.Functions;

import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.main.lite.R;

import dsr.comms.WGet_SF;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public final class UpdateServerThread extends Thread {

	private static String REQ_FILENAME;// = Statics.GetExtStorageDir() + "/" + "data_to_send.txt"; // This must be syncd!
	private static String REQ_FILENAME_EXTRA; // = Statics.GetExtStorageDir() + "/" + "data_to_send_extra.txt"; // This must be syncd!

	private GameModule game;
	private volatile boolean processing = false;
	public volatile boolean stop_now = false;
	private ArrayList<String> reqs; // This gets used if we can't create files
	private Object file_lock = new Object();
	private boolean use_file = false;

	public UpdateServerThread(GameModule _game) {
		super("UpdateServer");

		game = _game;

		if (file_lock == null) {
			file_lock = new Object(); // Just in case (Java bug?)
		}

		// Check we can write files
		synchronized (file_lock) {
			try {
				REQ_FILENAME = Statics.GetExtStorageDir() + "/" + "data_to_send.txt"; // This must be syncd!
				REQ_FILENAME_EXTRA = Statics.GetExtStorageDir() + "/" + "data_to_send_extra.txt"; // This must be syncd!
				File f = new File(REQ_FILENAME);
				if (f.exists()) {
					use_file = true;
				} else {
					IOFunctions.AppendLog(REQ_FILENAME, "", false);
					String s = IOFunctions.LoadText(REQ_FILENAME); // To check we can read it.
					new File(REQ_FILENAME).delete();
					use_file = true;
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		if (use_file == false) {
			reqs = new ArrayList<String>();
		}

		this.setDaemon(false);
		setProcessing(true); // So we signal to other programs, in case there is some leftover data from last time.
		start();
	}


	public void addRequest(String s) {
		setProcessing(true);

		synchronized (file_lock) {
			if (use_file) {
				try {
					IOFunctions.AppendLog(REQ_FILENAME_EXTRA, s + MiscCommsPage.SEP, false);
				} catch (IOException e) {
					e.printStackTrace();
					use_file = false;
					reqs = new ArrayList<String>();
				}
			} else {
				reqs.add(s);
			}
		}
	}


	public boolean isSendingData() {
		return processing;
	}


	public void run() {
		try {
			while (stop_now == false || isDataWaiting()) {
				AbstractActivity act = Statics.act;
				
				if (isDataWaiting()) {
					setProcessing(true);
					while (isDataWaiting()) {
						StringBuffer data = new StringBuffer();
						if (this.use_file) {
							synchronized (file_lock) {
								if (new File(REQ_FILENAME).exists() == false) {
									// Move the data from the extra file to the current one
									String tmp = IOFunctions.LoadText(REQ_FILENAME_EXTRA);
									IOFunctions.AppendLog(REQ_FILENAME, tmp, false);
									new File(REQ_FILENAME_EXTRA).delete();
								}
								data.append(IOFunctions.LoadText(REQ_FILENAME));
							}
						} else {
							synchronized (file_lock) {
								for (int i=0 ; i<reqs.size() ; i++) {
									data.append(reqs.get(i) + MiscCommsPage.SEP);
								}
								reqs.clear();
							}
						}
						sendData(data.toString());
						// We will only get this far if we got a response from the server
						if (this.use_file) {
							new File(REQ_FILENAME).delete(); //IOFunctions.SaveText(REQ_FILENAME, "", false); // Empty the file
						}
					}
				}
				setProcessing(false);
				Functions.delay(1000);
			}
			System.out.println("Finished");
		} catch (IOException ex) {
			AbstractActivity act = Statics.act;
			
			// Don't bother sending me the error!
			this.processing = false;
			game.addToHUD("* " + act.getString(R.string.restart_client) + " *");
			game.stopGameDueToError();
		} catch (Exception ex) {
			AbstractActivity act = Statics.act;
			
			this.processing = false;
			AbstractActivity.HandleError(ex);
			game.addToHUD("* " + act.getString(R.string.restart_client) + " *");
			game.stopGameDueToError();
		}
	}


	private void setProcessing(boolean p) {
		this.processing = p;
	}


	private void sendData(String post_data) throws UnknownHostException, IOException {
		AbstractActivity act = Statics.act;
		
		WGet_SF wc = new WGet_SF(act, game, post_data);
		String tmp_response = wc.getResponse();
		String responses[] = tmp_response.split(MiscCommsPage.SEP, -1);
		for (int i=0 ; i<responses.length ; i++) {
			String response = responses[i];
			if (response.equalsIgnoreCase(MiscCommsPage.OK)) {
				// Do nothing
			} else if (response.equalsIgnoreCase(MiscCommsPage.DRAW)) {
				game.gameIsDraw();
			} else if (response.equalsIgnoreCase(MiscCommsPage.SIDE_1_WINS)) {
				game.sideHasWon((byte)1);
			} else if (response.equalsIgnoreCase(MiscCommsPage.SIDE_2_WINS)) {
				game.sideHasWon((byte)2);
			} else if (response.equalsIgnoreCase(MiscCommsPage.SIDE_3_WINS)) {
				game.sideHasWon((byte)3);
			} else if (response.equalsIgnoreCase(MiscCommsPage.SIDE_4_WINS)) {
				game.sideHasWon((byte)4);
			} else if (response.toLowerCase().startsWith(MiscCommsPage.MSG)) {
				game.addToHUD(response.substring(MiscCommsPage.MSG.length()));
			} else {
				//System.err.println("Unexpected response from server: " + response);
				if (game != null) {
					game.addToHUD(act.getString(R.string.unexpected_response) + ":");
					game.addToHUD("'" + response + "'");
					game.addToHUD("* " + act.getString(R.string.restart_client) + " *");
					game.stopGameDueToError();
				}
				this.processing = false;
				//return false;
			}
		}
		//return true;
	}


	private boolean isDataWaiting() {
		if (this.use_file) {
			return new File(REQ_FILENAME).exists() || new File(REQ_FILENAME_EXTRA).exists();
		} else {
			return reqs.size() > 0;
		}
	}

}
