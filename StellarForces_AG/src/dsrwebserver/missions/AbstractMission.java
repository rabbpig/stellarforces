package dsrwebserver.missions;

import ssmith.util.MyList;

/**
 * All these are copied from the server software.
 *
 */
public class AbstractMission {

	// These are the only missions allowed on the 
	//public static final int THE_ASSASSINS = 1; // Kill a certain unit
	//public static final int MOONBASE_ASSAULT = 2;
	public static MyList<Integer> allowed_mission = MyList.CreateFromCSVInts("1,2,45,76,77,79,84");

	// Consts
	public static final boolean NO_CEILINGS = false;
	public static final boolean SHOW_CEILINGS = true;

	public static final int INDESTRUCTABLE_WALLS = 0;
	public static final int STRONG_WALLS = 1;
	public static final int WEAK_WALLS = 2;
	
	// Missions
	public static final int ALIENS = 62;


}
