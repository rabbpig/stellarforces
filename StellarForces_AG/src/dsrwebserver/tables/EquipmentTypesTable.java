package dsrwebserver.tables;

import ssmith.util.MyList;

public class EquipmentTypesTable {

	public static MyList<Integer> CORPSES;

	// These are the major types
	public static final byte ET_GUN = 1;
	public static final byte ET_GRENADE = 2;
	public static final byte ET_MEDIKIT = 3;
	public static final byte ET_CC_WEAPON = 4;
	public static final byte ET_AMMO_CLIP = 5;
	//public static final byte unused = 6;
	//public static final byte ET_PORTA_PORTER_TRIGGER = 7;
	//public static final byte ET_PORTA_PORTER_LANDER = 8;
	public static final byte ET_DEATH_GRENADE = 9;
	//public static final byte ET_ADRENALIN_SHOT = 10;
	public static final byte ET_EXPLOSIVES = 11; // Can destroy walls even in non-destroyable maps!
	public static final byte ET_FLAG = 12;
	public static final byte ET_EGG = 13;
	public static final byte ET_HUMAN_CORPSE_1 = 14;
	public static final byte ET_HUMAN_CORPSE_2 = 15;
	public static final byte ET_HUMAN_CORPSE_3 = 16;
	public static final byte ET_HUMAN_CORPSE_4 = 17;
	public static final byte ET_SCIENTIST_CORPSE = 18;
	public static final byte ET_ALIEN_CORPSE = 19;
	public static final byte ET_ALIEN_QUEEN_CORPSE = 20;
	public static final byte ET_SMOKE_GRENADE = 21;
	public static final byte ET_NERVE_GAS = 22;
	public static final byte ET_GAS_CANNISTER = 23;
	public static final byte ET_STARDRIVE = 24;
	public static final byte ET_SCANNER = 25;
	public static final byte ET_CRAB_CORPSE = 26;
	public static final byte ET_GHOUL_CORPSE = 27;
	public static final byte ET_GHOUL_QUEEN_CORPSE = 28;
	public static final byte ET_ZOMBIE_CORPSE = 29;
	public static final byte ET_CLONE_CORPSE_1 = 30;
	public static final byte ET_CLONE_CORPSE_2 = 31;
	public static final byte ET_CLONE_CORPSE_3 = 32;
	public static final byte ET_CLONE_CORPSE_4 = 33;
	public static final byte ET_INCENDIARY_FIRE_GRENADE = 34; // For smoke_type
	public static final byte ET_MAX_TYPES = 34;
	

	// Codes
	public static final String CD_EXPLOSIVE = "EXPLOSIVE";
	public static final String CD_ROCKET_LAUNCHER = "ROCKET_LAUNCHER";
	public static final String CD_ROCKET = "ROCKET";
	public static final String CD_AUTOCANNON = "AUTOCANNON";
	public static final String CD_FLAG = "FLAG";
	public static final String CD_EGG = "EGG";
	public static final String CD_SCIENTIST_CORPSE = "SCIENTISTCORPSE";
	public static final String CD_HUMAN_CORPSE_SIDE = "HUMANCORPSE";
	public static final String CD_ALIEN_CORPSE = "ALIENCORPSE";
	public static final String CD_SP30 = "SP30";
	public static final String CD_MK1 = "MK1";
	public static final String CD_AP50 = "AP50";
	public static final String CD_MACHINE_CLIP = "MACHINE_CLIP";
	public static final String CD_PISTOL_CLIP = "PISTOL_CLIP";
	public static final String CD_GASCANNISTER = "GASCANNISTER"; 
	public static final String CD_POWER_SWORD = "POWER_SWORD"; 
	public static final String CD_STARDRIVE = "STARDRIVE";
	public static final String CD_NERVE_GAS = "NERVEGAS";
	public static final String CD_SMOKE_GRENADE = "SMOKEGRENADE";
	public static final String CD_SCANNER = "SCANNER";
	public static final String CD_DEATHGRENADE = "DEATHGRENADE";
	public static final String CD_BLASTER = "BLASTER";
	public static final String CD_FLAMETHROWER = "FLAMETHROWER";
	public static final String CD_FIRE_EXTINGUISHER = "FIREEXT";
	public static final String CD_INCENDIARY_GRENADE = "INCENDIARYGREN";
	public static final String CD_SNIPERRIFLE = "SNIPERRIFLE";
	// ## ! IF YOU ADD ANY HERE, ADD THEM TO THE SERVER AS WELL! 

	
	static {
		CORPSES = MyList.CreateFromInts(ET_HUMAN_CORPSE_1, ET_HUMAN_CORPSE_2, ET_HUMAN_CORPSE_3, ET_HUMAN_CORPSE_4, ET_SCIENTIST_CORPSE, ET_ALIEN_CORPSE, ET_ALIEN_QUEEN_CORPSE, ET_ZOMBIE_CORPSE, ET_CLONE_CORPSE_1, ET_CLONE_CORPSE_2, ET_CLONE_CORPSE_3, ET_CLONE_CORPSE_4);
	}
	
}
