package dsrwebserver.tables;

public class VisibleEnemiesTable {

	// Visible types
	public static final int VT_SEEN = 0;
	public static final int VT_HEARD_MOVING = 1;
	public static final int VT_HEARD_SHOOTING = 2;

}
