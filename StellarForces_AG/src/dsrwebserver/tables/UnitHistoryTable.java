package dsrwebserver.tables;

public class UnitHistoryTable {

	public static final int UH_UNIT_MOVEMENT = 1;
	public static final int UH_EXPLOSION = 2;
	public static final int UH_WALL_DESTROYED = 3;
	public static final int UH_UNIT_KILLED = 4;
	public static final int UH_SHOT_FIRED = 5;
	public static final int UH_UNIT_DEPLOYED = 6;
	public static final int UH_UNIT_ESCAPED = 7;
	public static final int UH_COMPUTER_DESTROYED = 10;
	public static final int UH_FLAG_LOCATION = 11;
	
	public static final int UH_UNIT_WOUNDED = 17;
	public static final int UH_ITEM_DROPPED = 18;
	public static final int UH_ITEM_THROWN = 19;
	public static final int UH_ITEM_PICKED_UP = 20;
	public static final int UH_CLOSE_COMBAT = 21;
	public static final int UH_DOOR_OPENED = 24;
	public static final int UH_DOOR_CLOSED = 25;
	public static final int UH_GRENADE_PRIMED = 26;
	public static final int UH_BLOB_SPLIT = 27;
	public static final int UH_SMOKE_CREATED = 28;
	public static final int UH_SMOKE_REMOVED = 29;
	public static final int UH_NERVE_GAS_CREATED = 30;
	public static final int UH_NERVE_GAS_REMOVED = 31;
	public static final int UH_UNIT_TELEPORTED = 32;
	public static final int UH_MAPSQUARE_CHANGED = 33;
	public static final int UH_FIRE_CREATED = 34;
	public static final int UH_FIRE_REMOVED = 35;
	// If you add any here, add them to the UnitHistoryTable on the server and AppletPlayback
	

}
