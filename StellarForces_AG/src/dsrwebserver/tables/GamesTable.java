package dsrwebserver.tables;

public class GamesTable {

	// Game status
	public static final int GS_DEPLOYMENT = 20; // Deployment
	public static final int GS_STARTED = 30;
	public static final int GS_FINISHED = 40;

	// Special Power
	public static final int SP_WALK_THROUGH_WALLS = 1; // Done, to test
	public static final int SP_KNOW_DISTANCE = 2; // Done, tested
	public static final int SP_TELEPORT = 3; // Done, tested
	public static final int SP_CONVERT_ENERGY = 4;
	// ## IF YOU ADD ANY HERE, ADD THEM TO GamesTable on the Server!


	public static String SpecialPower2String(int type) {
		switch (type) {
		case SP_WALK_THROUGH_WALLS: return "Walk Through Walls";
		case SP_KNOW_DISTANCE: return "Know Distance";
		case SP_TELEPORT: return "Teleport";
		case SP_CONVERT_ENERGY: return "Convert Energy";
		default: return "Unknown!";
		}
	}

}
