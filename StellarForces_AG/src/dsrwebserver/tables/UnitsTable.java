package dsrwebserver.tables;

public class UnitsTable {

	// Morale
	public static final int SCARED_LEVEL = 5;

	// Model Types - these are used in the Missions.csv!
	public static final byte MT_MALE_SIDE_1 = 1;
	public static final byte MT_MALE_SIDE_2 = 2;
	public static final byte MT_ALIEN_TYRANT = 3;
	public static final byte MT_SCIENTIST = 4;
	public static final byte MT_MALE_SIDE_3 = 5;
	public static final byte MT_MALE_SIDE_4 = 6;
	public static final byte MT_QUEEN_ALIEN = 7;
	public static final byte MT_BLOB = 8;
	public static final byte MT_CRAB = 9;
	public static final byte MT_GHOUL = 10;
	public static final byte MT_GHOUL_QUEEN = 11;
	public static final byte MT_ZOMBIE = 12;
	public static final byte MT_CLONE = 13;
	public static final byte MT_ANGEL = 14;

	
	// Statuses
	public static final byte ST_AWAITING_DEPLOYMENT = 1;
	public static final byte ST_DEPLOYED = 2;
	public static final byte ST_DEAD = 3;
	public static final byte ST_ESCAPED = 4;
	public static final byte ST_NO_LONGER_EXISTS = 5; // i.e. merged blobs
	public static final byte ST_CREATED_MID_MISSION = 6; // i.e. a new alien from impreg


	// Unit types
	public static final int UT_NORMAL = 0;
	public static final int UT_ENGINEER = 1;


	// Skills
	public static final int SK_NONE = 0;
	public static final int SK_MEDIC = 1;
	public static final int SK_STEALTH = 2;
	public static final int SK_ENGINEER = 3;
	
	public static String Skill2String(int skill) {
		switch (skill) {
		case SK_MEDIC: return "MEDIC";
		case SK_STEALTH: return "STEALTH";
		case SK_ENGINEER: return "ENG";
		default: return "UNKNOWN";
		}
	}

}
