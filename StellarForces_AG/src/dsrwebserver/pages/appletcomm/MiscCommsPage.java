package dsrwebserver.pages.appletcomm;

public class MiscCommsPage {

	public static final String SEP = "~";

	// Return codes
	public static final String SIDE_1_WINS = "s1w";
	public static final String SIDE_2_WINS = "s2w";
	public static final String SIDE_3_WINS = "s3w";
	public static final String SIDE_4_WINS = "s4w";
	public static final String OK = "ok";
	public static final String DRAW = "draw";
	public static final String MSG = "msg";
	//public static final String ERROR = "error";

	public static final String GET = "get";
	public static final String PUT = "put";

	// Main commands - IF YOU ADD ANY, ADD TO THE SERVER MiscCommsPage!
	public static final String CHECK_VERSION2 = "checkver2";
	public static final String VALIDATE_LOGIN = "val";
	public static final String AUTOREGISTER = "autoregister";
	public static final String GET_PLAYERS_DATA = "get_players_data";
	
	public static final String PLAYERS_GAMES = "games";
	public static final String GAME_DATA = "gamedata";
	public static final String UNIT_DATA = "unitdata";
	public static final String EQUIPMENT_DATA = "equipdata";
	public static final String REMOVE_EQUIPMENT_DATA = "rem_equip";
	public static final String MAP_DATA = "mapdata";
	public static final String VISIBLE_ENEMY_DATA = "visen_data";
	public static final String EVENT_DATA = "event_data";
	public static final String GET_LATEST_VERSION_NUM = "latest_ver";
	public static final String PLAYERS_FINISHED_GAME_DATA = "fin_game_data";
	public static final String REGISTER_LOGIN = "register_login";
	public static final String GET_SEEN_ENEMY_DATA = "seen_enemy_data";
	public static final String GET_HEARD_ENEMY_DATA = "heard_enemy_data";

	public static final String GET_EQUIPMENT_LIST = "equipment_list";
	public static final String GET_ARMOUR_LIST = "armour_list";
	
	public static final String GET_NEW_GAMES_LIST = "new_games_list";
	public static final String JOIN_NEW_GAME = "join_game";
	public static final String CREATE_NEW_GAME = "create_game";
	public static final String GET_MISSION_LIST = "mission_list";
	//public static final String START_PRACTISE_GAME_ = "start_practise";
	public static final String START_PRACTISE_GAME_WITH_AI = "start_practise_with_ai";
	public static final String START_NEW_SAME_SIDES = "start_new_same_sides";
	public static final String START_NEW_OPP_SIDES = "start_new_opp_sides";

	public static final String GET_NUM_UNREAD_MESSAGES = "get_num_unread_messages";
	public static final String GET_MESSAGES = "get_messages";
	public static final String MESSAGE_READ = "message_read";
	public static final String SEND_MESSAGE = "send_message";
	
	public static final String GET_FINISHED_GAME_LIST = "fin_games_list";
	public static final String GET_ALL_FINISHED_GAME_LIST = "all_fin_games_list";
	public static final String GET_MSG_FROM_SERVER = "get_server_msg";
	public static final String GET_LEAGUE_TABLE = "get_league_table";
	
	public static final String JOIN_CAMPAIGN = "join_campaign";
	public static final String GET_CAMP_SQUAD_LIST = "get_camp_squad_list";
	public static final String SEND_CAMP_SQUAD_SELECTION = "send_camp_squad_selection";

	public static final String SET_STAT = "set_stat";

	public static final String GET_UNIVERSE_MAP_DATA = "get_uni_map_data";
	public static final String GET_TREATIES_DATA = "get_treaties";
	public static final String GET_UNI_SQUAD_LIST = "get_uni_squad_list";
	public static final String SEND_UNI_SQUAD_SELECTION = "send_uni_squad_sel";
	public static final String GET_CONTRACTS_DATA = "get_contracts_data";
	public static final String GET_UNI_INVENTORY_DATA = "get_uni_inv_data";

	public static final String OFFER_TREATY = "offer_treaty";
	public static final String ACCEPT_TREATY = "accept_treaty";
	public static final String DECLINE_TREATY = "decline_treaty";
	public static final String CANCEL_TREATY = "cancel_treaty";
	
	// Main commands - IF YOU ADD ANY, ADD TO THE SERVER MiscCommsPage!

}
