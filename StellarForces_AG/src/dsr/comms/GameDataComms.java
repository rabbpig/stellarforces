package dsr.comms;

import com.scs.stellarforces.Statics;

import dsrwebserver.pages.appletcomm.MiscCommsPage;


public final class GameDataComms {

	// These funcs are for updating the server from the client

	// Update Types
	public static final int DEPLOYED = 1;
	public static final int TURN_ENDED = 2;
	public static final int EQUIPPED = 3;


	// This is called by the applet
	public static String GetGameUpdateRequest(int gameid, String gamecode, int side, int type) {
		return "cmd=" + MiscCommsPage.GAME_DATA + "&version=" + Statics.COMMS_VERSION + "&getput=" + MiscCommsPage.PUT + "&gameid=" + gameid + "&gc=" + gamecode + "&side=" + side + "&type=" + type + "&event_time=" + System.currentTimeMillis();
	}



}
