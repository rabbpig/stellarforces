package dsr.comms;

import java.io.IOException;
import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.util.IDisplayMessages;

import com.scs.stellarforces.Statics;

import dsr.data.ClientMapData;
import dsr.data.EquipmentData;
import dsr.data.GameData;
import dsr.data.UnitData;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public final class GetAllGameData extends Thread {

	private ClientMapData mapdata;
	public ArrayList<UnitData> units;
	public GameData game_data;
	public EquipmentData equipment[];

	private IDisplayMessages id;
	private boolean success = false;
	private AbstractActivity act;

	public GetAllGameData(AbstractActivity _act, GameData _game_data, boolean thread, IDisplayMessages _id) throws IOException {
		super();

		act = _act;
		game_data = _game_data;
		id = _id;

		if (thread) {
			this.setDaemon(true);
			start();
		} else {
			run();
		}
	}


	public void run() {
		try {
			this.getMapData(); // Must be before getting the equipment and getting units
			this.getUnitData(); // Must be before getting the equipment
			this.getEquipmentData(); // Must be after getMapData since we use that to place the equipment
			this.getSeenEnemies();
			this.getHeardEnemies();
			success = true;
		} catch (Exception ex) {
			AbstractActivity.HandleError(ex);
		}
	}


	private void getMapData() throws Exception {
		int retries = 3;
		while (true) {
			retries--;
			writeText("Transporting to mission location...");
			WGet_SF wc = new WGet_SF(act, id, MapDataComms.GetMapDataRequest(game_data.game_id));
			if (wc.getResponseCode() == 200) {
				String response = wc.getResponse();
				try {
					mapdata = MapDataComms.DecodeMapDataResponse(response);
					break;
				} catch (Exception ex) {
					// Loop around
				}
			}
			if (retries <= 0) {
				throw new RuntimeException("Error getting map data!");
			} else {
				writeText("Problem getting map data - retrying...");
			}
		}
		writeText("Journey complete.");
	}


	private void getUnitData() throws IOException {
		int retries = 3;
		while (true) {
			retries--;
			writeText("Units awaking from hypersleep...");
			WGet_SF wc = new WGet_SF(act, id, UnitDataComms.GetUnitDataRequest(game_data.game_id, game_data.gamecode, Statics.LAST_LOGIN, Statics.LAST_PWD));
			if (wc.getResponseCode() == 200) {
				String response = wc.getResponse();
				try {
					units = UnitDataComms.DecodeUnitDataResponse(response);
					break;
				} catch (Exception ex) {
					// Loop around
				}
			}
			if (retries <= 0) {
				throw new RuntimeException("Error getting unit data!");
			} else {
				writeText("Problem getting map data - retrying...");
			}
		}
		writeText("All units ready.");
	}


	private void getEquipmentData() throws IOException {
		int retries = 3;
		while (true) {
			retries--;
			writeText("Units are checking their equipment...");
			WGet_SF wc = new WGet_SF(act, id, EquipmentDataComms.GetEquipmentDataRequest(game_data.game_id, game_data.gamecode));
			if (wc.getResponseCode() == 200) {
				String response = wc.getResponse();
				try {
					equipment = EquipmentDataComms.DecodeEquipmentDataResponse(mapdata, units, response);
					break;
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			if (retries <= 0) {
				throw new RuntimeException("Error getting equipment data!");
			} else {
				writeText("Problem getting equipment data - retrying...");
			}
		}
		writeText("All equipment okay.");
	}


	private void getSeenEnemies() throws IOException {
		int retries = 3;
		while (true) {
			retries--;
			writeText("Getting intelligence data...");
			WGet_SF wc = new WGet_SF(act, id, "cmd=" + MiscCommsPage.GET_SEEN_ENEMY_DATA + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD) + "&gameid=" + game_data.game_id);
			if (wc.getResponseCode() == 200) {
				String response = wc.getResponse();
				try {
					game_data.seen_enemies = new DataTable(response);
					break;
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			if (retries <= 0) {
				throw new RuntimeException("Error getting intelligence data!");
			} else {
				writeText("Problem getting intelligence data - retrying...");
			}
		}
		writeText("Got intelligence data");
	}


	private void getHeardEnemies() throws IOException {
		int retries = 3;
		while (true) {
			retries--;
			writeText("Getting more intelligence data...");
			WGet_SF wc = new WGet_SF(act, id, "cmd=" + MiscCommsPage.GET_HEARD_ENEMY_DATA + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD) + "&gameid=" + game_data.game_id);
			if (wc.getResponseCode() == 200) {
				String response = wc.getResponse();
				try {
					game_data.heard_enemies = new DataTable(response);
					break;
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			if (retries <= 0) {
				throw new RuntimeException("Error getting more intelligence data!");
			} else {
				writeText("Problem getting more intelligence data - retrying...");
			}
		}
		writeText("Got more intelligence data");
	}


	public ClientMapData getMap() {
		return mapdata;
	}


	public boolean wasSuccessful() {
		return this.success;
	}


	private void writeText(String s) {
		if (id != null) {
			id.displayMessage(s);
		}
	}

}
