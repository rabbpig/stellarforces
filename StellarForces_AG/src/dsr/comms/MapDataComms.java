package dsr.comms;

import ssmith.lang.NumberFunctions;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.exceptions.DodgyDataException;

import dsr.data.AppletMapSquare;
import dsr.data.ClientMapData;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public final class MapDataComms {

	// These are for updating the applet from the server

	// Called by the applet
	public static String GetMapDataRequest(int gameid) {
		return "cmd=" + MiscCommsPage.MAP_DATA + "&version=" + Statics.COMMS_VERSION + "&getput=" + MiscCommsPage.GET + "&gid=" + gameid;
	}
	

	// Called by the applet
	public static ClientMapData DecodeMapDataResponse(String response) {
		//response = CommFuncs.Decode(response);
		response = response.replaceAll("\r\n", "");
		String data[] = response.split("\\|", -1);
		int w = -1;
		int pos = 0;
		AppletMapSquare[][] mapdata = null;
		while (true) {
			//AppletMain.ps(".");//data=" + data[pos]);
			/*if (progress != null) {
				progress.displayProgress(pos + "/" + data.length);
			}*/
			
			if (data[pos].equalsIgnoreCase("width")) {
				w = NumberFunctions.ParseInt(data[pos+1]);
				pos += 2;
			} else if (data[pos].equalsIgnoreCase("height")) {
				int h = NumberFunctions.ParseInt(data[pos+1]);
				mapdata = new AppletMapSquare[w][h];
				pos += 2;
			} else if (data[pos].equalsIgnoreCase("ms")) {
				int id = NumberFunctions.ParseInt(data[pos+1]);
				byte x = NumberFunctions.ParseByte(data[pos+2]);
				byte z = NumberFunctions.ParseByte(data[pos+3]);
				byte type = NumberFunctions.ParseByte(data[pos+4]);
				byte door = NumberFunctions.ParseByte(data[pos+5]);
				byte deploy_sq_side = NumberFunctions.ParseByte(data[pos+6]);
				byte escape_hatch_side = NumberFunctions.ParseByte(data[pos+7]);
				short floor_tex = NumberFunctions.ParseShort(data[pos+8]);
				byte destroyed = NumberFunctions.ParseByte(data[pos+9]);
				byte door_open = 0;
				short scenery_code = 0;
				byte scenery_direction = 0;
				short raised_floor_tex = 0;
				byte smoke_type = -1;
				int smoke_caused_by = -1;
				byte owner_side = 0;
				try {
					door_open = NumberFunctions.ParseByte(data[pos+10]);
					scenery_code = NumberFunctions.ParseShort(data[pos+11]);
					scenery_direction = NumberFunctions.ParseByte(data[pos+12]);
					raised_floor_tex = NumberFunctions.ParseShort(data[pos+13]);
					smoke_type = NumberFunctions.ParseByte(data[pos+14]);
					smoke_caused_by = NumberFunctions.ParseInt(data[pos+15]);
					owner_side = NumberFunctions.ParseByte(data[pos+16]);
					
					pos += 17;
				} catch (Exception ex) { 
					pos += 10;
				}

				AppletMapSquare ms = new AppletMapSquare(id, type, x, z, deploy_sq_side, escape_hatch_side, floor_tex, destroyed, door_open, raised_floor_tex, smoke_type, smoke_caused_by, owner_side);
				ms.door_type = door;
				ms.scenery_code = scenery_code;
				//ms.scenery_direction = scenery_direction;

				mapdata[x][z] = ms;
			} else {
				//throw new RuntimeException("Unknown data decoding map data");
				//AppletMain.p("Ignoring map data.");
				pos++;
			}
			if (pos >= data.length) {
				break;
			}
		}

		// Check we got some valid data
		if (mapdata == null) {
			throw new DodgyDataException("Error decoding map data");
		} else {
			return new ClientMapData(mapdata);
		}

	}


	//------------------------------------------------------------------------------

	// These are for updating the server from the applet

	// This is called by the applet
	public static String GetMapDataUpdateRequest(int gameid, String gamecode, AppletMapSquare sq, int caused_by_side) {
		StringBuffer str = new StringBuffer();
		str.append(gameid + "|");
		str.append(gamecode + "|");
		str.append(sq.x + "|");
		str.append(sq.y + "|");
		str.append(sq.major_type + "|");
		str.append(sq.destroyed + "|");
		str.append(sq.texture_code + "|");
		str.append(sq.door_type + "|");
		str.append((sq.door_open ?  1:0) + "|");
		str.append(caused_by_side + "|");
		str.append(sq.smoke_type + "|");
		str.append(sq.smoke_caused_by + "|");
		str.append(System.currentTimeMillis() + "|");
		str.append(sq.owner_side + "|");
		str.append(sq.raised_texture_code + "|");
		str.append(sq.scenery_code + "|");

		return "cmd=" + MiscCommsPage.MAP_DATA + "&version=" + Statics.COMMS_VERSION + "&getput=" + MiscCommsPage.PUT + "&data=" + str.toString();
	}



}
