package dsr.comms;

import java.util.ArrayList;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.universe.UniverseMapPlanetIcon;

import dsr.data.EquipmentData;
import dsr.data.UnitData;
import dsrwebserver.pages.appletcomm.MiscCommsPage;


public class UniverseComms {
	
	public static String GetSendUnitsToPlanetCommand(UniverseMapPlanetIcon planet, ArrayList<UnitData> units) {
		StringBuffer str = new StringBuffer();
		for (UnitData unit : units) {
			str.append(unit.uni_unitid + "|" + unit.items.size());
			for (EquipmentData eq : unit.items) {
				str.append("|" + eq.equipment_type_id);
			}
			str.append("\n");
		}
		
		return "cmd=" + MiscCommsPage.SEND_UNI_SQUAD_SELECTION + "&version=" + Statics.COMMS_VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_LOGIN) + "&pwd=" + AbstractCommFuncs.URLEncodeString(Statics.LAST_PWD) + "&gid=" + planet.gameid + "&data=" + str.toString();
	}

}
