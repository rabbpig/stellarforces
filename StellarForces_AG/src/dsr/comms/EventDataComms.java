package dsr.comms;

import ssmith.dbs.SQLFuncs;

import com.scs.stellarforces.Statics;

import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class EventDataComms {

	private EventDataComms() {

	}

	//------------------------------------------------------------------------------

	// These are for updating the server from the applet

	// This is called by the applet
	public static String GetNewEventRequest(int gameid, String gamecode, int type, int unitid, int x, int z, int data1, int data2, boolean seen_by_sides[], int data3, int data4) {
		StringBuffer str = new StringBuffer();
		str.append(gameid + "|");
		str.append(gamecode + "|");
		str.append(type + "|");
		str.append(unitid + "|");
		str.append(x + "|");
		str.append(z + "|");
		str.append(data1 + "|"); // Grenade expl: Radius, unit killed: killed by, seen by side, shot angle, attacked by side, thrown angle, prime turns
		str.append(data2 + "|"); // shot length, attack by unit, throw dist, expl type
		if (seen_by_sides != null) {
			for (int i=1 ; i<seen_by_sides.length ; i++) {
				str.append(SQLFuncs.b201(seen_by_sides[i]) + "|");
			}
			// Add extra |0
			for (int i=seen_by_sides.length ; i <= 4 ; i++) {
				str.append("0|");
			}
		} else {
			str.append("0|0|0|0|");
		}
		str.append(data3 + "|"); // form of attack?
		str.append(data4 + "|"); // amount wounded?
		str.append(System.currentTimeMillis() + "|");

		return "cmd=" + MiscCommsPage.EVENT_DATA + "&version=" + Statics.COMMS_VERSION + "&getput=" + MiscCommsPage.PUT + "&data=" + str.toString();
	}


}
