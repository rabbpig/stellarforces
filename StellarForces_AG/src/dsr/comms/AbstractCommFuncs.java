package dsr.comms;

import java.io.UnsupportedEncodingException;

import dsrwebserver.pages.appletcomm.MiscCommsPage;
import ssmith.android.framework.ErrorReporter;
import ssmith.io.Base64;
import ssmith.lang.NumberFunctions;

/**
 * To encrypt the comms between the server and the client, you must create a class called "CommFuncs.java" in the same directory as this class, and extending
 * this class.  You must also do the same in the server code.  If you don't do this, the software will default to "no encryption".
 * 
 * The encoding functions in this class must work with the encoding functions in the server, otherwise the client and
 * server won't be able to understand each other.
 * 
 */
public abstract class AbstractCommFuncs {

	private static AbstractCommFuncs comm_funcs;

	public abstract String specialEncode(String s);

	public abstract String specialDecode(String s);

	public static AbstractCommFuncs GetCommFuncs() {
		if (comm_funcs == null) {
			try {
				comm_funcs = (DefaultCommFuncs)Class.forName("dsr.comms.CommFuncs").newInstance();
			} catch (Exception e) {
				e.printStackTrace();
				comm_funcs = new DefaultCommFuncs();
				System.out.println("Warning: using default Comm Funcs");
			}
		}
		return comm_funcs;
	}

	
	public static String URLEncodeString(String s) {
		if (s != null && s.length() > 0) {
			try {
				return java.net.URLEncoder.encode(s, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return s;
	}


	public static String URLDecodeString(String s) throws UnsupportedEncodingException {
		if (s != null && s.length() > 0) {
			try {
				return java.net.URLDecoder.decode(s, "UTF-8");
			} catch (java.lang.IllegalArgumentException ex) {
				ErrorReporter.getInstance().handleSilentException(ex);
				ex.printStackTrace();
				System.err.println("String was '" + s + "'");
			}
		}
		return s;
	}


	public static boolean IsResponseGood(String s) {
		// Have we received multiple types of data?
		if (s.indexOf("|") < 0) { // No
			return s.equalsIgnoreCase(MiscCommsPage.OK) || s.equalsIgnoreCase("200");
		} else {
			String s2[] = s.split("\\|");
			if (NumberFunctions.IsNumeric(s2[0])) {
				return NumberFunctions.ParseInt(s2[0]) == 200;
			} else {
				return false;
			}
		}
	}



}

