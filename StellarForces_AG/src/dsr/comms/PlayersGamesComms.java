package dsr.comms;

import java.util.ArrayList;

import ssmith.lang.NumberFunctions;
import ssmith.util.MyList;

import com.scs.stellarforces.Statics;

import dsr.data.GameData;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

/**
 * This is for the client to get a list of the players game.
 * 
 */
public final class PlayersGamesComms {

	// This is for data request by the client

	// Called by the client to get game data for all the player's current games
	public static String GetPlayersGamesRequest(String login, String pwd) {
		return "cmd=" + MiscCommsPage.PLAYERS_GAMES + "&version=" + Statics.COMMS_VERSION + "&getput=" + MiscCommsPage.GET + "&login=" + AbstractCommFuncs.URLEncodeString(login) + "&pwd=" + AbstractCommFuncs.URLEncodeString(pwd) + "&time=" + System.currentTimeMillis();
	}


	// Called by the client to get game data on a specific game
	public static String GetPlayersGamesRequest(String login, String pwd, int gameid) {
		return "cmd=" + MiscCommsPage.PLAYERS_FINISHED_GAME_DATA + "&version=" + Statics.COMMS_VERSION + "&getput=" + MiscCommsPage.GET + "&login=" + AbstractCommFuncs.URLEncodeString(login) + "&pwd=" + AbstractCommFuncs.URLEncodeString(pwd) + "&gameid=" + gameid + "&time=" + System.currentTimeMillis();
	}


	// Called by the client
	public static ArrayList<GameData> DecodeGameDataResponse(String response) {
		String data[] = response.split("\\|", -1);
		ArrayList<GameData> games = new ArrayList<GameData>();
		if (data.length > 1) {
			int pos=0;
			while (true) {
				if (data[pos].equalsIgnoreCase("qty")) {
					//int tot_games = NumberFunctions.ParseInt(data[pos+1]);
					pos += 2;
				} else if (data[pos].equalsIgnoreCase("game")) {
					GameData gamedata = new GameData();
					gamedata.game_id = NumberFunctions.ParseInt(data[pos+1]);
					gamedata.gamecode = data[pos+2];
					gamedata.game_status = NumberFunctions.ParseByte(data[pos+3]);
					gamedata.turn_no = NumberFunctions.ParseInt(data[pos+4]);
					gamedata.our_side = Byte.parseByte(data[pos+5]);
					gamedata.mission_type = Short.parseShort(data[pos+6]);
					gamedata.turn_side = NumberFunctions.ParseByte(data[pos+7]);
					gamedata.forum_id = NumberFunctions.ParseInt(data[pos+8]);
					gamedata.mission_name = data[pos+9];
					//gamedata.show_ceilings = NumberFunctions.ParseByte(data[pos+10]);
					gamedata.max_turns = NumberFunctions.ParseByte(data[pos+11]);
					gamedata.wall_type = Byte.parseByte(data[pos+12]); 
					gamedata.our_name = data[pos+13];
					gamedata.num_players = NumberFunctions.ParseByte(data[pos+14]);
					gamedata.players_name_by_side[1] = data[pos+15];
					gamedata.players_name_by_side[2] = data[pos+16];
					gamedata.players_name_by_side[3] = data[pos+17];
					gamedata.players_name_by_side[4] = data[pos+18];
					gamedata.min_client_version = Float.parseFloat(data[pos+19]);
					//gamedata.map_model_type = NumberFunctions.ParseByte(data[pos+20]);
					gamedata.side_see_enemies[1] = NumberFunctions.ParseByte(data[pos+21]);
					gamedata.side_see_enemies[2] = NumberFunctions.ParseByte(data[pos+22]);
					gamedata.side_see_enemies[3] = NumberFunctions.ParseByte(data[pos+23]);
					gamedata.side_see_enemies[4] = NumberFunctions.ParseByte(data[pos+24]);
					gamedata.is_snafu = NumberFunctions.ParseByte(data[pos+25]);
					gamedata.snafu_will_opp_fire_on_side[1] = NumberFunctions.ParseByte(data[pos+26]);
					gamedata.snafu_will_opp_fire_on_side[2] = NumberFunctions.ParseByte(data[pos+27]);
					gamedata.snafu_will_opp_fire_on_side[3] = NumberFunctions.ParseByte(data[pos+28]);
					gamedata.snafu_will_opp_fire_on_side[4] = NumberFunctions.ParseByte(data[pos+29]);
					gamedata.is_advanced = NumberFunctions.ParseByte(data[pos+30]);

					gamedata.side_sides[1] = MyList.CreateFromCSVInts(data[pos+31]);
					gamedata.side_sides[2] = MyList.CreateFromCSVInts(data[pos+32]);
					gamedata.side_sides[3] = MyList.CreateFromCSVInts(data[pos+33]);
					gamedata.side_sides[4] = MyList.CreateFromCSVInts(data[pos+34]);

					/*gamedata.ai_for_side[1] = NumberFunctions.ParseByte(data[pos+35]);
					gamedata.ai_for_side[2] = NumberFunctions.ParseByte(data[pos+36]);
					gamedata.ai_for_side[3] = NumberFunctions.ParseByte(data[pos+37]);
					gamedata.ai_for_side[4] = NumberFunctions.ParseByte(data[pos+38]);
					 */
					gamedata.has_side_equipped[1] = NumberFunctions.ParseByte(data[pos+39]);
					gamedata.has_side_equipped[2] = NumberFunctions.ParseByte(data[pos+40]);
					gamedata.has_side_equipped[3] = NumberFunctions.ParseByte(data[pos+41]);
					gamedata.has_side_equipped[4] = NumberFunctions.ParseByte(data[pos+42]);

					gamedata.has_side_deployed[1] = NumberFunctions.ParseByte(data[pos+43]);
					gamedata.has_side_deployed[2] = NumberFunctions.ParseByte(data[pos+44]);
					gamedata.has_side_deployed[3] = NumberFunctions.ParseByte(data[pos+45]);
					gamedata.has_side_deployed[4] = NumberFunctions.ParseByte(data[pos+46]);

					gamedata.creds = NumberFunctions.ParseInt(data[pos+47]);

					gamedata.vps[1] = NumberFunctions.ParseInt(data[pos+48]);
					gamedata.vps[2] = NumberFunctions.ParseInt(data[pos+49]);
					gamedata.vps[3] = NumberFunctions.ParseInt(data[pos+50]);
					gamedata.vps[4] = NumberFunctions.ParseInt(data[pos+51]);

					gamedata.mission1liner = data[pos+52];

					gamedata.side_names[1] = data[pos+53];
					gamedata.side_names[2] = data[pos+54];
					gamedata.side_names[3] = data[pos+55];
					gamedata.side_names[4] = data[pos+56];

					gamedata.units_remaining[1] = NumberFunctions.ParseByte(data[pos+57]);
					gamedata.units_remaining[2] = NumberFunctions.ParseByte(data[pos+58]);
					gamedata.units_remaining[3] = NumberFunctions.ParseByte(data[pos+59]);
					gamedata.units_remaining[4] = NumberFunctions.ParseByte(data[pos+60]);

					/*gamedata.special_power[1] = NumberFunctions.ParseByte(data[pos+61]);
					gamedata.special_power[2] = NumberFunctions.ParseByte(data[pos+62]);
					gamedata.special_power[3] = NumberFunctions.ParseByte(data[pos+63]);
					gamedata.special_power[4] = NumberFunctions.ParseByte(data[pos+64]);*/

					gamedata.is_camp_game = NumberFunctions.ParseByte(data[pos+65]);

					gamedata.chosen_camp_units[1] = NumberFunctions.ParseByte(data[pos+66]);
					gamedata.chosen_camp_units[2] = NumberFunctions.ParseByte(data[pos+67]);
					gamedata.chosen_camp_units[3] = NumberFunctions.ParseByte(data[pos+68]);
					gamedata.chosen_camp_units[4] = NumberFunctions.ParseByte(data[pos+69]);

					gamedata.min_camp_units[1] = NumberFunctions.ParseByte(data[pos+70]);
					gamedata.min_camp_units[2] = NumberFunctions.ParseByte(data[pos+71]);
					gamedata.min_camp_units[3] = NumberFunctions.ParseByte(data[pos+72]);
					gamedata.min_camp_units[4] = NumberFunctions.ParseByte(data[pos+73]);

					gamedata.days_waiting = NumberFunctions.ParseInt(data[pos+74]);
					gamedata.is_practise = NumberFunctions.ParseByte(data[pos+75]);

					gamedata.max_protection[1] = NumberFunctions.ParseByte(data[pos+76]);
					gamedata.max_protection[2] = NumberFunctions.ParseByte(data[pos+77]);
					gamedata.max_protection[3] = NumberFunctions.ParseByte(data[pos+78]);
					gamedata.max_protection[4] = NumberFunctions.ParseByte(data[pos+79]);

					gamedata.res_points[1] = NumberFunctions.ParseInt(data[pos+80]);
					gamedata.res_points[2] = NumberFunctions.ParseInt(data[pos+81]);
					gamedata.res_points[3] = NumberFunctions.ParseInt(data[pos+82]);
					gamedata.res_points[4] = NumberFunctions.ParseInt(data[pos+83]);

					gamedata.can_build_and_dismantle = NumberFunctions.ParseByte(data[pos+84]);

					try {
						gamedata.max_camp_units[1] = NumberFunctions.ParseByte(data[pos+85]);
						gamedata.max_camp_units[2] = NumberFunctions.ParseByte(data[pos+86]);
						gamedata.max_camp_units[3] = NumberFunctions.ParseByte(data[pos+87]);
						gamedata.max_camp_units[4] = NumberFunctions.ParseByte(data[pos+88]);

						pos += 89;
					} catch (Exception ex) {
						ex.printStackTrace();
						pos += 85;
					}

					games.add(gamedata);
				} else {
					pos++; // You may have something wrong if
				}

				if (pos >= data.length) {
					break;
				}
			}
		}

		if (games == null) {
			throw new RuntimeException("Error decoding game data");
		} else {
			return games;
		}
	}


}
