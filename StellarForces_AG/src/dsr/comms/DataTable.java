package dsr.comms;

import java.util.ArrayList;

import com.scs.stellarforces.Statics;

import ssmith.lang.NumberFunctions;

/**
 * This class takes data along the lines of "a|b|c" and turns into a pseudo-sql table
 *
 */
public class DataTable extends ArrayList<String[]> {

	private static final long serialVersionUID = 1L;

	private String[] col_names;
	private int current_row = 0, saved_row = -1;
	private int push_row = -1;
	private String raw_data, data_preview;

	public DataTable(String data) {
		super();
		
		raw_data = data;
		data_preview = data;
		if (data_preview.length() > 40) {
			data_preview = data_preview.substring(0, 39);
		}

		String lines[] = data.split("\n");

		// First line is the col names.
		col_names = lines[0].split("\\|", -1);

		for (int i=1 ; i<lines.length ; i++) {
			String fields[] = lines[i].split("\\|", -1);
			if (NumberFunctions.mod(fields.length - col_names.length) <= 1) {
				this.add(fields);
			} else {
				if (Statics.RELEASE_MODE == false) {
					throw new RuntimeException("Discrepancy between # columns and # values.  Data is:\n\n--\n" + data + "\n--");
				}
			}
		}

		this.moveBeforeFirst(); // Ready for looping through it
	}


	public boolean moveNext() {
		current_row++;
		return current_row < this.size();
	}


	public boolean movePrev() {
		current_row--;
		return current_row >= 0;
	}


	public void moveFirst() {
		this.current_row = 0;
	}


	public void moveBeforeFirst() {
		this.current_row = -1;
	}


	public void moveLast() {
		this.current_row = this.size()-1;
	}


	public int getInt(String field) {
		return NumberFunctions.ParseInt(this.getString(field));
	}


	public void setInt(String field, int val) {
		this.setString(field, ""+val);
	}


	public byte getByte(String field) {
		return Byte.parseByte(this.getString(field));
	}


	public short getShort(String field) {
		return Short.parseShort(this.getString(field));
	}


	public String getString(String field) {
		if (this.current_row < 0) {
			throw new RuntimeException("Data not at valid row! (" + this.current_row + ")");
		}

		for (int i=0 ; i<col_names.length ; i++) { // this
			if (col_names[i].equalsIgnoreCase(field)) {
				String data[] = this.get(this.current_row);
				return data[i];
			}
		}
		throw new RuntimeException("Field '" + field + "' not found in '" + data_preview + "'!");
	}


	public void setString(String field, String val) {
		if (this.current_row < 0) {
			throw new RuntimeException("Data not at valid row! (" + this.current_row + ")");
		}

		for (int i=0 ; i<col_names.length ; i++) {
			if (col_names[i].equalsIgnoreCase(field)) {
				String data[] = this.get(this.current_row);
				data[i] = val;
				// Replace the row
				this.remove(current_row);
				this.add(current_row, data);
				return;
			}
		}
		throw new RuntimeException("Field '" + field + "' not found!");
	}


	public boolean find(String col, int val) {
		return find(col, ""+val);
	}


	public boolean find(String col1, String val1, String col2, String val2) {
		this.moveBeforeFirst();
		while (this.moveNext()) {
			if (this.getString(col1).equalsIgnoreCase(val1) && this.getString(col2).equalsIgnoreCase(val2)) {
				return true;
			}
		}
		return false;
	}
	
	
	public boolean find(String col, String val) {
		this.moveBeforeFirst();
		while (this.moveNext()) {
			if (this.getString(col).equalsIgnoreCase(val)) {
				return true;
			}
		}
		return false;
	}
	
	
	public void moveTo(int idx) {
		this.current_row = idx;
	}
	
	
	public void saveCurrentPos() {
		this.saved_row = this.current_row;
	}


	public void restoreCurrentPos() {
		this.current_row = this.saved_row;
	}
	
	
	public boolean containsColumn(String name) {
		for (int i=0 ; i<col_names.length ; i++) {
			if (col_names[i].equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}
	
	
	public void pushRow() {
		this.push_row = this.current_row;
	}
	
	
	public void popRow() {
		this.current_row = this.push_row;
		push_row = -1;
	}
	
	
	public void removeCurrentRow() {
		super.remove(this.current_row);
	}
	
}
