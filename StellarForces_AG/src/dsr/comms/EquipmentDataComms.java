package dsr.comms;

import java.util.ArrayList;

import ssmith.dbs.SQLFuncs;
import ssmith.lang.NumberFunctions;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.exceptions.DodgyDataException;

import dsr.data.ClientMapData;
import dsr.data.EquipmentData;
import dsr.data.UnitData;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public final class EquipmentDataComms {

	// Called by the client to request the equipment data.
	public static String GetEquipmentDataRequest(int gameid, String gamecode) {
		return "cmd=" + MiscCommsPage.EQUIPMENT_DATA + "&version=" + Statics.COMMS_VERSION + "&getput=" + MiscCommsPage.GET + "&gid=" + gameid + "&gc=" + gamecode;
	}


	// Called by the client to decode the server's response.
	public static EquipmentData[] DecodeEquipmentDataResponse(ClientMapData map, ArrayList<UnitData> units, String response) {
		String data[] = response.split("\\|", -1);
		int pos=0;
		int num = 0;
		EquipmentData equipment[] = null;
		while (true) {
			//main.addToHUD("Processing data: " + pos);
			//AppletMain.ps(".");//data=" + data[pos]);
			if (data[pos].equalsIgnoreCase("qty")) {
				int u = NumberFunctions.ParseInt(data[pos+1]);
				equipment = new EquipmentData[u];
				pos += 2;
			} else if (data[pos].equalsIgnoreCase("equipment")) {
				equipment[num] = new EquipmentData();
				equipment[num].equip_id = NumberFunctions.ParseInt(data[pos+1]);
				equipment[num].equipcode = data[pos+2];
				equipment[num].setName(data[pos+3]);
				equipment[num].setUnitID(NumberFunctions.ParseInt(data[pos+4]));
				equipment[num].major_type = NumberFunctions.ParseByte(data[pos+5]);
				int x = NumberFunctions.ParseInt(data[pos+6]);
				int y = NumberFunctions.ParseInt(data[pos+7]);
				equipment[num].shot_damage = NumberFunctions.ParseShort(data[pos+8]);
				equipment[num].cc_damage = NumberFunctions.ParseShort(data[pos+9]);
				equipment[num].aimed_shot_acc = NumberFunctions.ParseByte(data[pos+10]);
				equipment[num].aimed_shot_aps = NumberFunctions.ParseByte(data[pos+11]);
				equipment[num].snap_shot_acc = NumberFunctions.ParseByte(data[pos+12]);
				equipment[num].snap_shot_aps = NumberFunctions.ParseByte(data[pos+13]);
				equipment[num].auto_shot_acc = NumberFunctions.ParseByte(data[pos+14]);
				equipment[num].auto_shot_aps = NumberFunctions.ParseByte(data[pos+15]);
				equipment[num].explosion_rad = NumberFunctions.ParseByte(data[pos+16]);
				equipment[num].cc_acc = NumberFunctions.ParseByte(data[pos+17]);
				equipment[num].primed = NumberFunctions.ParseInt(data[pos+18]) == 1;
				equipment[num].explode_turns = NumberFunctions.ParseByte(data[pos+19]);
				equipment[num].explosion_dam = NumberFunctions.ParseShort(data[pos+20]);
				equipment[num].setAmmo(NumberFunctions.ParseByte(data[pos+21]));
				equipment[num].ammo_type_id = NumberFunctions.ParseInt(data[pos+22]);
				equipment[num].ammo_capacity = NumberFunctions.ParseByte(data[pos+23]);
				equipment[num].weight = NumberFunctions.ParseByte(data[pos+24]);
				equipment[num].destroyed = NumberFunctions.ParseInt(data[pos+25]) == 1;
				equipment[num].explodes = NumberFunctions.ParseInt(data[pos+26]) == 1;
				equipment[num].reload_cost = NumberFunctions.ParseInt(data[pos+27]);
				equipment[num].indestructable = NumberFunctions.ParseByte(data[pos+29]);
				equipment[num].last_unit_to_touch = NumberFunctions.ParseInt(data[pos+30]);
				equipment[num].seen_by_side[1] = NumberFunctions.ParseByte(data[pos+31]);
				equipment[num].seen_by_side[2] = NumberFunctions.ParseByte(data[pos+32]);
				equipment[num].seen_by_side[3] = NumberFunctions.ParseByte(data[pos+33]);
				equipment[num].seen_by_side[4] = NumberFunctions.ParseByte(data[pos+34]);
				equipment[num].equipment_type_id = NumberFunctions.ParseInt(data[pos+35]);
				equipment[num].range_sq = NumberFunctions.ParseByte(data[pos+36]);
				try {
					equipment[num].code = data[pos+37];
					equipment[num].primed_time = Long.parseLong(data[pos+38]);

					pos += 38;
				} catch (Exception ex) {
					pos += 37;
				}

				// Set last_unit_to_touch to unitid if there is one and last_unit_to_touch is zero (as it needs filling in)
				if (equipment[num].last_unit_to_touch <= 0 && equipment[num].getUnitID() > 0) {
					equipment[num].last_unit_to_touch = equipment[num].getUnitID();
				}
				/*if (equipment[num].destroyed) {
					if (equipment[num].indestructable != 0) { NO as there will be no model if it's destroyed, so the client will error.
						equipment[num].destroyed = false;
					}
				}*/
				if (equipment[num].destroyed == false) {
					// Add the equipment to the map or the unit
					if (equipment[num].getUnitID() > 0) { // Carried by a unit
						UnitData unit = UnitData.GetUnitDataFromID(units, equipment[num].getUnitID());
						unit.items.add(equipment[num]);
						// Is it there current item?
						if (unit.current_item_id_TMP == equipment[num].equip_id) {
							unit.current_item = equipment[num];
						}
					} else if (x >= 0 && y >= 0) {
						// Add it to the map 
						if (map != null) { // Map is null if we're decoding equipment data for the equip modules
							//if (x >= 0 && y >= 0) {
							map.getSq_MaybeNULL(x, y).addEquipment(equipment[num]);
							//}
						}
					} else {
						equipment[num].destroyed = true; // No mapsquare and no unit!
					}
				}

				num++;
			} else {
				// Extra fields have obviously been added, so this version of the client ignores them.
				//AppletMain.p("Ignoring equipment data.");
				pos++;
			}
			if (pos >= data.length) {
				break;
			}
		}
		if (equipment == null) {
			throw new DodgyDataException("Error decoding equipment data");
		} else {
			return equipment;
		}
	}


	//------------------------------------------------------------------------------

	// These are for updating the server from the applet

	// Called by the client to update the server
	public static String GetEquipmentUpdateRequest(EquipmentData eq, int mapx, int mapy, int gameid) {
		StringBuffer str = new StringBuffer();
		str.append(eq.equip_id + "|");
		str.append(eq.equipcode + "|");
		str.append(eq.getUnitID() + "|");
		str.append(mapx + "|");
		str.append(mapy + "|");
		str.append(eq.getAmmo() + "|");
		str.append(eq.explode_turns + "|");
		str.append(SQLFuncs.b201(eq.primed) + "|");
		str.append(SQLFuncs.b201(eq.destroyed) + "|");
		str.append(gameid + "|");
		str.append(eq.last_unit_to_touch + "|");
		str.append(eq.seen_by_side[1] + "|");
		str.append(eq.seen_by_side[2] + "|");
		str.append(eq.seen_by_side[3] + "|");
		str.append(eq.seen_by_side[4] + "|");
		str.append(System.currentTimeMillis() + "|");
		str.append(eq.new_item + "|");
		str.append(eq.equipment_type_id + "|");
		str.append(eq.primed_time + "|");

		/*if (eq.getUnitID() == -1 && mapx == -1 && mapy == -1 && eq.destroyed == false) {
			throw new RuntimeException("Equipment put in limbo!:" + eq.equip_id);
		}*/

		return "cmd=" + MiscCommsPage.EQUIPMENT_DATA + "&version=" + Statics.COMMS_VERSION + "&getput=" + MiscCommsPage.PUT + "&data=" + str.toString();
	}


}