package dsr.comms;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.io.HttpRequest;
import ssmith.util.IDisplayMessages;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.exceptions.FailedToConnectException;
import com.scs.stellarforces.main.lite.R;

public class WGet_SF {

	private static final String USER_AGENT = Statics.USER_AGENT + "_" + Statics.VERSION_NUM;

	public static int T_STD = 0; // Default MiscDataComms
	public static int T_OTHER = 3; // It's called by the applet!

	private static final int MAX_TRIES = 5;
	private static final int CONNECT_TIMEOUT = 1000 * 30; //60 * 2; // Don't timeout too soon otherwise the data gets duped
	private static final int READ_TIMEOUT = 1000 * 60 * 2; // Don't timeout too soon otherwise the data gets duped

	private String s_url;
	private String text_response = "";
	private int response_code;
	private String redirect_to = "";
	private String post_data = "";
	private int tries_remaining = MAX_TRIES;
	private IDisplayMessages show_msg;
	private AbstractActivity act;
	private static final Object LOCK = new Object();


	/**
	 * Call this for std comms data
	 */
	public WGet_SF(AbstractActivity _act, IDisplayMessages _owner, String _post_data) throws UnknownHostException, IOException {
		this(_act, _owner, _post_data, false);
	}

	public WGet_SF(AbstractActivity _act, IDisplayMessages _owner, String _post_data, boolean allow_repeats) throws UnknownHostException, IOException {
		this(_act, _owner, T_STD, null, _post_data, allow_repeats);
	}


	/**
	 * Call this for anything else data
	 */
	public WGet_SF(AbstractActivity _act, IDisplayMessages _owner, int type, String full_url, String _post_data, boolean allow_repeats) throws UnknownHostException, IOException {
		super();

		act =_act;
		show_msg = _owner;

		// Encode data?
		if (_post_data != null) {
			if (_post_data.length() > 0) {
				post_data = AbstractCommFuncs.GetCommFuncs().specialEncode(_post_data);
			}
		}

		if (type == T_STD) {
			s_url = Statics.URL_FOR_CLIENT + "/appletcomm/MiscCommsPage.cls";
		} else {
			s_url = full_url;
		}

		synchronized (LOCK) {
			long time = System.currentTimeMillis();
			while (tries_remaining > 0) { // In case of re-trying
				tries_remaining--;
				try {
					while (true) { // In case of redirects
						this.post(s_url, time, allow_repeats);
						if ((this.response_code == 302 || this.response_code == 303 || this.response_code == 307) && this.redirect_to.length() > 0) {
							// Redirect ourselves
							this.s_url = redirect_to;
							redirect_to = "";
						} else if (response_code == 200 || response_code == -1) { // Android sometimes returns -1?
							if (tries_remaining+1 < MAX_TRIES) { // Show text to say we've connected after earlier failure
								if (act != null) {
									displayMessage(act.getString(R.string.connected_to_server));
								}
							}
							tries_remaining = 0; // So we drop out
							break;
							/*} else if (response_code == 500) {
							throw new RuntimeException("Server error");*/
						} else {
							/*try {
								throw new RuntimeException("Retrying (Retries: " + this.tries_remaining + ")");
							} catch (Exception ex) {
								ErrorReporter.getInstance().handleSilentException(ex);
							}*/
							throw new RuntimeException("Unwanted response code: " + this.response_code);
						}
					}
				} catch (IOException ex) { 
					if (act != null) {
						displayMessage(" *" + ex.getMessage() + " *");
					}
				} catch (RuntimeException ex) {
					ex.printStackTrace();
					if (act != null) {
						displayMessage(" *" + ex.getMessage() + " *");
					}
					// Loop round
					/*} catch (java.io.FileNotFoundException ex) {
					if (act != null) {
						displayMessage(act.getString(R.string.page_not_found) + ": " + ex);//.toString());  Don't use toString as 'ex' might be null!
					}
					throw ex; // No point re-trying
				/*} catch (Exception ex) { 
					if (act != null) {
						displayMessage(act.getString(R.string.error) + ":" + ex);//.toString()));  Don't use toString as 'ex' might be null!
					}*/
					// Loop round
				}
				if (tries_remaining > 0) {
					if (act != null) {
						displayMessage("* " + act.getString(R.string.retrying) + "...");
					}
				}
				// loop around again - and tell me
			}
			if (getResponseCode() != 200 && getResponseCode() != -1) { // Sometimes android returns -1
				if (getResponseCode() > 0 && getResponseCode() != 500 && getResponseCode() != 504 && getResponseCode() != 403 && getResponseCode() != 503) {
					throw new IOException("Got response " + getResponseCode());
				} else { // No response
					throw new FailedToConnectException();
				}
			}
		}
	}


	private void displayMessage(String s) {
		if (this.show_msg != null) {
			this.show_msg.displayMessage(s);
		}
	}


	public int getResponseCode() {
		return this.response_code;
	}


	public String getResponse() {
		return text_response.toString();
	}


	private void post(String server, long time, boolean allow_repeats) throws IOException {
		HttpRequest http_req = null;
		try {
			Map<String, String> data = new HashMap<String, String>();
			data.put("post", post_data);
			data.put("clientid", Statics.clientid+"");
			data.put("time", time+"");
			data.put("android_version", Statics.VERSION_NUM+"");
			HttpRequest.keepAlive(false);
			//HttpRequest.keepAlive(true);
			http_req = HttpRequest.post(server).header("User-Agent", USER_AGENT);//.form(data);
			http_req.useCaches(false);
			http_req.connectTimeout(CONNECT_TIMEOUT);
			http_req.readTimeout(READ_TIMEOUT);
			http_req.form(data);
			response_code = http_req.code();
			if (response_code == 200 || response_code == -1) {
				this.response_code = 200;
				this.text_response = http_req.body();
				this.text_response = AbstractCommFuncs.GetCommFuncs().specialDecode(this.text_response);
			} else if (response_code == 302 || response_code == 303 || response_code == 307) { // Found
				this.redirect_to = http_req.location();
			}
		} finally {
			//http_req.disconnect();
		}
	}


}

