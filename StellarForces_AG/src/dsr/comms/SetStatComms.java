package dsr.comms;

import com.scs.stellarforces.Statics;

import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class SetStatComms {

	// These funcs are for updating the server from the client

	// Update Types
	public static final int STAT_RES_POINTS = 1;
	// ## IF YOU ADD ANY HERE, ADD TO THE SERVER! # !!

	// This is called by the applet
	public static String GetSetStatRequest(int gameid, String gamecode, int side, int type, int amt) {
		return "cmd=" + MiscCommsPage.SET_STAT + "&version=" + Statics.COMMS_VERSION + "&gameid=" + gameid + "&gc=" + gamecode + "&side=" + side + "&type=" + type + "&amt=" + amt;
	}

}
