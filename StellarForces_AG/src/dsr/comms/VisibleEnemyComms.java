package dsr.comms;

import com.scs.stellarforces.Statics;

import dsr.data.UnitData;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class VisibleEnemyComms {

	// These are for updating the server from the applet

	// This is called by the applet
	public static String GetVisibleEnemyRequest(UnitData enemy, UnitData seen_by) {
		StringBuffer str = new StringBuffer();
		str.append(enemy.unitid + "|");
		str.append(enemy.unitcode + "|");
		str.append(seen_by.unitid + "|");
		str.append(seen_by.unitcode + "|");
		str.append(System.currentTimeMillis() + "|");

		return "cmd=" + MiscCommsPage.VISIBLE_ENEMY_DATA + "&version=" + Statics.COMMS_VERSION + "&getput=" + MiscCommsPage.PUT + "&data=" + str.toString();
	}


}
