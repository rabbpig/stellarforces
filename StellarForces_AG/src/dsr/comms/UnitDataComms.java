package dsr.comms;

import java.io.IOException;
import java.util.ArrayList;

import ssmith.lang.NumberFunctions;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.exceptions.DodgyDataException;

import dsr.data.UnitData;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public final class UnitDataComms {

	public static final int HEAR_ENEMY_DISTANCE = 3;
	public static final int HEAR_DOOR_DISTANCE = 3;
	public static final int QUEEN_ALIEN_HEAR_DIST = 10;

	// Forms of attack
	public static final int FOA_SHOT = 1;
	public static final int FOA_MELEE = 2;
	public static final int FOA_EXPLOSION = 3;
	public static final int FOA_CRUSHED = 4; // As in Gladiator mission
	public static final int FOA_NERVE_GAS = 5;
	public static final int FOA_MERGED = 6;
	public static final int FOA_FIRE = 7;
	//# If you add any here, add them to UnitDataComms on the server!!  ##

	// These are for updating the client from the server

	/*public static String GetUnitDataRequest_ORIG(int gameid, String gamecode, int loginid, String logincode) {
		return "cmd=" + MiscCommsPage.UNIT_DATA + "&version=" + Statics.COMMS_VERSION + "&getput=" + MiscCommsPage.GET + "&gid=" + gameid + "&gc=" + gamecode + "&loginid=" + loginid + "&logincode=" + logincode;
	}*/


	public static String GetUnitDataRequest(int gameid, String gamecode, String login, String pwd) {
		return "cmd=" + MiscCommsPage.UNIT_DATA + "&version=" + Statics.COMMS_VERSION + "&getput=" + MiscCommsPage.GET + "&gid=" + gameid + "&gc=" + gamecode + "&login=" + AbstractCommFuncs.URLEncodeString(login) + "&pwd=" + AbstractCommFuncs.URLEncodeString(pwd);
	}


	// This is called by the client to decode server unit data
	public static ArrayList<UnitData> DecodeUnitDataResponse(String response) throws IOException {
		response = AbstractCommFuncs.URLDecodeString(response);
		String data[] = response.split("\\|", -1);
		int pos=0;
		int num = 0; // Must start from 0!
		UnitData unit = null;
		ArrayList<UnitData> al_units = new ArrayList<UnitData>();
		while (true) {
			if (data[pos].equalsIgnoreCase("qty")) {
				int u = NumberFunctions.ParseInt(data[pos+1]);
				unit = new UnitData(u);
				pos += 2;
			} else if (data[pos].equalsIgnoreCase("unit")) {
				unit = new UnitData(num);
				unit.unitid = NumberFunctions.ParseInt(data[pos+1]);
				unit.unitcode = data[pos+2];
				unit.name = data[pos+3];
				unit.setSide(NumberFunctions.ParseByte(data[pos+4]));
				unit.setStatus(NumberFunctions.ParseByte(data[pos+5]));
				byte x = NumberFunctions.ParseByte(data[pos+6]);
				byte z = NumberFunctions.ParseByte(data[pos+7]);
				unit.setAngle(NumberFunctions.ParseInt(data[pos+8]));
				unit.model_type = NumberFunctions.ParseByte(data[pos+9]);
				unit.setAPs(NumberFunctions.ParseByte(data[pos+10]));
				unit.max_aps = NumberFunctions.ParseShort(data[pos+11]);
				unit.current_item_id_TMP = NumberFunctions.ParseInt(data[pos+12]);
				unit.protection = NumberFunctions.ParseShort(data[pos+13]);
				unit.opp_fire_01 = NumberFunctions.ParseByte(data[pos+14]);
				unit.shot_skill = NumberFunctions.ParseShort(data[pos+15]);
				unit.combat_skill = NumberFunctions.ParseShort(data[pos+16]);
				unit.can_use_equipment = NumberFunctions.ParseInt(data[pos+17]) == 1;
				unit.strength = NumberFunctions.ParseShort(data[pos+18]);
				unit.setHealth(NumberFunctions.ParseShort(data[pos+19]));
				unit.opp_fire_aps_req = NumberFunctions.ParseShort(data[pos+20]);
				unit.max_health = NumberFunctions.ParseShort(data[pos+21]);
				unit.order_by = NumberFunctions.ParseInt(data[pos+22]);
				//units.opp_fire_type = NumberFunctions.ParseByte(data[pos+23]);
				unit.curr_morale = NumberFunctions.ParseShort(data[pos+24]);
				unit.curr_energy = NumberFunctions.ParseShort(data[pos+25]);
				unit.panicked = NumberFunctions.ParseByte(data[pos+26]);
				unit.armour_type_id = NumberFunctions.ParseByte(data[pos+27]);
				unit.can_deploy = NumberFunctions.ParseByte(data[pos+28]);
				try {
					//unit.shot_damage = NumberFunctions.ParseByte(data[pos+29]);
					//unit.power_points = NumberFunctions.ParseByte(data[pos+30]);
					unit.setAIType(NumberFunctions.ParseInt(data[pos+31]));
					unit.unit_type = NumberFunctions.ParseByte(data[pos+32]);
					unit.on_fire = NumberFunctions.ParseByte(data[pos+33]);
					unit.can_equip = NumberFunctions.ParseByte(data[pos+34]);
					unit.skillid = NumberFunctions.ParseByte(data[pos+35]);

					pos += 35;
				} catch (Exception ex) {
					pos += 29;
				}

				unit.setTargetMapLocation(null, x, z, null);
				al_units.add(unit);
				num++;
			} else {
				//AppletMain.p("Ignoring unit data.");
				pos++;
			}
			if (pos >= data.length) {
				break;
			}
		}
		if (al_units.size() == 0) {
			throw new DodgyDataException("Error decoding unit data");
		} else {
			return al_units;
		}
	}


	//------------------------------------------------------------------------------

	// These are for updating the server from the applet

	// This is called by the client to update the server
	public static String GetUnitUpdateRequest(UnitData unit) {
		StringBuffer str = new StringBuffer();
		str.append(unit.unitid + "|");
		str.append(unit.unitcode + "|");
		str.append(unit.getMapX() + "|");
		str.append(unit.getMapY() + "|");
		str.append(unit.getAngle() + "|");
		str.append(unit.getStatus() + "|");
		if (unit.current_item != null) {
			str.append(unit.current_item.equip_id + "|");
		} else {
			str.append("0|");
		}
		str.append(unit.getAPs() + "|");
		str.append(unit.getHealth() + "|");
		str.append("0|"); // unit.opp_fire_type + 
		str.append(System.currentTimeMillis() + "|");
		str.append(unit.getMaxHealth() + "|");
		str.append(unit.combat_skill + "|");
		str.append(unit.strength + "|");
		str.append(unit.armour_type_id + "|");
		str.append(unit.shot_skill + "|");
		str.append("0|");
		str.append(unit.protection + "|");
		str.append("0|");
		str.append(unit.on_fire + "|");

		return "cmd=" + MiscCommsPage.UNIT_DATA + "&version=" + Statics.COMMS_VERSION + "&getput=" + MiscCommsPage.PUT + "&data=" + str.toString();
	}


}
