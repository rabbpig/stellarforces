package dsr.data;

import ssmith.util.MyList;
import dsr.comms.DataTable;

public final class GameData {

	public int game_id;
	public String gamecode, mission_name, our_name;
	private String opponents_names;
	public String[] players_name_by_side = new String[5]; // Starts at 1! 
	public byte game_status;
	public byte turn_side, max_turns;
	public int turn_no;
	public byte our_side;
	public byte wall_type; // Strong/weak etc...
	public short mission_type;
	public byte is_snafu = 0, is_advanced = 0, num_players = 0;
	//public byte show_ceilings;
	public byte side_see_enemies[] = new byte[5];
	public byte snafu_will_opp_fire_on_side[] = new byte[5];
	public float min_client_version;
	public MyList<Integer> side_sides[] = new MyList[5]; // Starts at 1! 
	public byte has_side_equipped[] = new byte[5]; // Starts at 1!
	public byte has_side_deployed[] = new byte[5]; // Starts at 1!
	public int forum_id, creds;
	public int vps[] = new int[5]; // Starts at 1! 
	public String mission1liner;
	public String[] side_names = new String[5]; // Starts at 1! 
	public byte units_remaining[] = new byte[5]; // Starts at 1!
	//public byte special_power[] = new byte[5]; // Starts at 1!
	public int is_camp_game = 0;
	public byte chosen_camp_units[] = new byte[5]; // Starts at 1!
	public byte min_camp_units[] = new byte[5]; // Starts at 1!
	public byte max_camp_units[] = new byte[5]; // Starts at 1!
	public int days_waiting = 0;
	public byte is_practise = 0;
	public byte max_protection[] = new byte[5]; // Starts at 1!
	public int res_points[] = new int[5]; // Starts at 1!
	public byte can_build_and_dismantle = 0;
	public DataTable seen_enemies, heard_enemies;

	public String GetPlayersNameFromSide(int side) {
		String result;
		if (side == our_side) {
			result = our_name;
		} else {
			result = players_name_by_side[side];
		}
		if (result != null) {
			return result;
		} else {
			return opponents_names;
		}
	}


	public String getFullTitle() {
		StringBuffer str = new StringBuffer();
		str.append(mission_name);
		str.append(" vs ");
		str.append(getOpponentsName());
		str.append(", turn ");
		str.append(turn_no);
		return str.toString();
	}


	public String getPlayersNames() {
		StringBuffer str = new StringBuffer();
		for (int i=1 ; i < players_name_by_side.length ; i++) {
			if (players_name_by_side[i] != null) {
				if (players_name_by_side[i].length() > 0) {
					str.append(players_name_by_side[i] + ", ");
				}
			}
		}
		if (str.length() >= 2) {
			str.delete(str.length()-2, str.length());
		}
		return str.toString();
	}


	public String getOpponentsName() {
		if (num_players == 1) {
			return "No-one";
		}
		if (opponents_names == null) {
			StringBuffer str = new StringBuffer();
			for (int i=1 ; i < players_name_by_side.length ; i++) {
				if (i != our_side) {
					if (players_name_by_side[i] != null) {
						if (players_name_by_side[i].length() > 0) {
							str.append(players_name_by_side[i] + ", ");
						}
					}
				}
			}
			if (str.length() >= 2) {
				str.delete(str.length()-2, str.length());
				opponents_names = str.toString();
			} else {
				opponents_names = "No-one";

			}
		}
		return opponents_names;
	}


	public String toString() {
		return this.getFullTitle();
	}


	public boolean areSidesFriends(int s1, int s2) {
		if (s1 == s2) {
			return true;
		}
		if (s1 < 0 || s2 < 0) {
			return false;
		}
		if (this.side_sides[s1] != null) {
			return this.side_sides[s1].contains(s2);
		} else {
			return false;
		}
	}


	public byte getComradeSide() {
		for (byte s=1 ; s<= this.num_players ; s++) {
			if (s != this.our_side) {
				if (this.areSidesFriends(s, our_side)) {
					return s;
				}
			}
		}
		return -1;
	}
	
	
	public String getComradesName() {
		return this.players_name_by_side[getComradeSide()];
	}
	

	/*public int getOurSpecialPower() {
		return this.special_power[this.our_side];
	}*/
	

	public int getUnitsRemaining() {
		return this.units_remaining[this.our_side];
	}
	

	public boolean haveWeEquipped() {
		return this.has_side_equipped[this.our_side] != 0;
	}
	

	public boolean haveWeDeployed() {
		return this.has_side_deployed[this.our_side] != 0;
	}
	

	public boolean haveWeChosenCampUnits() {
		return this.chosen_camp_units[this.our_side] != 0;
	}
	

	public int getMaxProtectionForOurSide() {
		return this.max_protection[this.our_side];
	}
	

	public int getResPointsForOurSide() {
		return this.res_points[this.our_side];
	}
	

	public boolean isItOurTurn() {
		return this.turn_side == this.our_side;
	}
	
	
	
}
