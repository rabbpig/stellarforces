package dsr.data;

import ssmith.util.PointByte;
import android.graphics.Point;

import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.graphics.GameObject;

import dsrwebserver.tables.EquipmentTypesTable;

public final class EquipmentData {

	public String equipcode;
	private String name;
	public String code;
	public int equip_id;
	public byte major_type, weight, range_sq;
	private int unitid; // Which unit is carrying it.
	private byte ammo;
	public byte ammo_capacity;
	public int ammo_type_id;
	public int shot_damage, cc_damage;
	public byte cc_acc;
	public int explode_turns, explosion_rad;
	public int explosion_dam;
	public boolean primed, destroyed, explodes;
	public byte aimed_shot_acc, aimed_shot_aps;
	public byte snap_shot_acc, snap_shot_aps;
	public byte auto_shot_acc, auto_shot_aps;
	public int reload_cost, last_unit_to_touch;
	public byte indestructable;
	public GameObject model;
	public byte seen_by_side[] = new byte[5];
	public byte new_item = 0;
	public int equipment_type_id;
	public long primed_time;
	
	public EquipmentData() {
		super();
	}

	public String toString() {
		return "Equipment: " + name + " (unit: " + this.unitid + ") " + (this.destroyed?"D":"-");
	}


	public static PointByte GetEquipmentMapSquare(ClientMapData map, EquipmentData eq) {
		for (int z=0 ; z<map.getMapHeight() ; z++) {
			for (int x=0 ; x<map.getMapWidth() ; x++) {
				AppletMapSquare sq = map.getSq_MaybeNULL(x, z);
				if (sq.contains(eq)) {
					return new PointByte(x, z);
				}
			}
		}
		return null;//throw new RuntimeException("MapSquare not found for EquipmentID : " + eq.equip_id);
	}


	public String getName(boolean detailed) {
		if (detailed) {
			if (this.major_type == EquipmentTypesTable.ET_DEATH_GRENADE && primed) {
				return this.name + " (Prmd)";
			} else if ((this.major_type == EquipmentTypesTable.ET_GRENADE || this.major_type == EquipmentTypesTable.ET_SMOKE_GRENADE || this.major_type == EquipmentTypesTable.ET_NERVE_GAS || this.major_type == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) && primed) {
				return this.name + " (Prmd:" + this.explode_turns + ")";
			} else if (this.major_type == EquipmentTypesTable.ET_GUN || this.major_type == EquipmentTypesTable.ET_AMMO_CLIP) {
				return name + " (" + this.ammo + "/" + this.ammo_capacity + ")";
			} else {
				return this.name;
			}
		} else {
			return this.name;
		}
	}


	public void setName(String s) {
		name = s;
	}


	public int getUnitID() {
		return this.unitid;
	}


	public void removeFromUnit() {
		this.unitid = -1;
	}


	public void setUnitID(int id) {
		this.unitid = id;
	}


	public static EquipmentData GetEquipmentData(EquipmentData equips[], int equipid, String equipcode) {
		for (int i=0 ; i<equips.length ; i++) {
			EquipmentData equip = equips[i];
			if (equip.equip_id == equipid && equip.equipcode.equalsIgnoreCase(equipcode)) {
				return equip;
			}
		}
		throw new RuntimeException("EquipmentData not found: " + equipid);
	}


	public byte getAmmo() {
		return this.ammo;
	}

	
	public byte getAmmoCapacity() {
		return this.ammo_capacity;
	}
	

	public void decAmmo() {
		this.ammo--;
	}

	
	public void incAmmo() {
		this.ammo++;
	}

	
	public void setAmmo(byte a) {
		this.ammo = a;
	}


	public void equipmentDestroyed(GameModule main) {
		if (this.indestructable == 0) {
			this.unitid = -1;
			this.destroyed = true;
			main.updateEquipmentOnServer(this, (byte)-1, (byte)-1);
			if (model != null) { // Might be being held by a unit, so there is no model!
				model.removeFromParent();
			}
		}
	}
	
	
	public Point getMapLocation(UnitData[] units, ClientMapData mapdata) {
		int mapx, mapz;
		if (this.getUnitID() > 0) {
			UnitData unit = UnitData.GetUnitDataFromID(units, this.getUnitID());
			mapx = unit.getMapX();
			mapz = unit.getMapY();
		} else {
			PointByte p = EquipmentData.GetEquipmentMapSquare(mapdata, this);
			if (p == null) {// Problem with the data
				return null;
			}
			mapx = p.x;
			mapz = p.y;
		}
		return new Point(mapx, mapz);
	}

}
