package dsr.data;

import java.util.ArrayList;

import com.scs.stellarforces.graphics.SlidingDoor;

import dsrwebserver.tables.MapDataTable;

public final class AppletMapSquare {

	public int x, y, major_type;
	public byte owner_side;
	public int texture_code; // See TextureStateCache
	public short raised_texture_code;
	public byte door_type = -1; // See MapDataTable
	public byte deploy_sq_side = -1;
	public byte escape_hatch_side  = -1; // If it > 4, any side can escape
	public byte destroyed = 0;
	public boolean door_open = false;
	public short scenery_code; // see MapDataTable

	public int sq_id;
	public SlidingDoor door;
	private ArrayList<EquipmentData> equipment;
	public byte smoke_type; // e.g. EquipmentTypesTable.ET_NERVE_GAS
	public int smoke_caused_by;

	public AppletMapSquare(int id, byte t, byte _x, byte _y, byte deploy, byte escape, short floor_tex, byte _destroyed, byte _door_open, short raised_tex, byte _smoke_type, int _smoke_caused_by, byte owner_side) {
		super();
		
		x = _x;
		y = _y;
		this.major_type = t;
		destroyed = _destroyed;
		door_open = _door_open == 1;

		sq_id = id;
		this.deploy_sq_side = deploy;
		this.escape_hatch_side = escape;
		this.texture_code = floor_tex;
		this.raised_texture_code = raised_tex;
		this.smoke_type = _smoke_type;
		this.smoke_caused_by = _smoke_caused_by;
		this.owner_side = owner_side;
		
	}


	public String toString() {
		return "Mapsquare (" + this.x + ", " + this.y + ")";
	}


	public void addEquipment(EquipmentData eq) {
		if (this.equipment == null) {
			this.equipment = new ArrayList<EquipmentData>(5);
		}
		this.equipment.add(eq);
	}


	public void removeEquipment(EquipmentData eq) {
		this.equipment.remove(eq);
		if (this.equipment.size() == 0) {
			this.equipment = null;
		}
	}


	public ArrayList<EquipmentData> getEquipment_MaybeNULL() {
		return this.equipment;
	}


	public boolean contains(EquipmentData eq) {
		if (this.equipment != null) {
			return this.equipment.contains(eq);
		} 
		return false;
	}


	public String getListOfEquipment(int advanced_mode, int side) {
		if (equipment != null) {
			StringBuffer str = new StringBuffer();
			for(int i=0 ; i<equipment.size() ; i++) {
				EquipmentData eq = equipment.get(i);
				// Only show if we can see it (if advanced mode) if (is_advanced == false || ) {
				if (advanced_mode != 1 || eq.seen_by_side[side] != 0) {
					str.append(eq.getName(false) + ", ");
				}
			}
			if (str.length() > 2) {
				str.delete(str.length()-2, str.length());
			}
			return str.toString();
		} else {
			return "";
		}
	}


	public boolean containsType(int type) {
		if (equipment != null) {
			for(int i=0 ; i<equipment.size() ; i++) {
				EquipmentData eq = equipment.get(i);
				if (eq.major_type == type) {
					return true;
				}
			}
		}
		return false;
	}

	
	public boolean castsShadow() {
		return this.major_type == MapDataTable.MT_WALL || this.major_type == MapDataTable.MT_COMPUTER;
	}

}
