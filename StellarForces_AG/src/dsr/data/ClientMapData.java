package dsr.data;

import ssmith.android.lib2d.MyPickResults;
import ssmith.android.lib2d.MyPointF;
import ssmith.android.lib2d.PickData;
import ssmith.android.lib2d.Ray;
import ssmith.android.lib2d.shapes.Line;
import ssmith.util.MyList;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.TextureStateCache;
import com.scs.stellarforces.graphics.DummyWall;

import dsrwebserver.tables.MapDataTable;

public final class ClientMapData {

	private AppletMapSquare[][] mapdata;
	private static MyList<Integer> ignore_floor_tex;

	static {
		ignore_floor_tex = MyList.CreateFromInts(TextureStateCache.TEX_CLONE_GENERATOR, TextureStateCache.TEX_GUN_VENDING_MACHINE, TextureStateCache.TEX_GRENADE_VENDING_MACHINE, TextureStateCache.TEX_MEDI_BAY, TextureStateCache.TEX_RUBBLE_1, TextureStateCache.TEX_RUBBLE_2, TextureStateCache.TEX_RUBBLE_3, TextureStateCache.TEX_RUBBLE_4, TextureStateCache.TEX_MOONROCK, TextureStateCache.TEX_ALIEN_COLONY);
	}


	public ClientMapData(AppletMapSquare[][] data) {
		mapdata = data;
	}

	public AppletMapSquare getSqFromPixels_MaybeNULL(int px, int pz) {
		return getSq_MaybeNULL((int)(px/Statics.SQ_SIZE), (int)(pz/Statics.SQ_SIZE));
	}


	public AppletMapSquare getSq_MaybeNULL(int x, int z) {
		try {
			return mapdata[x][z];
		} catch (java.lang.ArrayIndexOutOfBoundsException ex) {
			//AppletMain.p("Error: sq " + x + "," + z + " not found.");
			return null;
		}
	}


	public int getMapWidth() {
		return mapdata.length;
	}


	public int getMapHeight() {
		return mapdata[0].length;
	}


	public void canSee(Ray r, MyPickResults results, boolean ignore_smoke, boolean ignore_cpu) {
		if (r.getDirection().x != r.getDirection().x || r.getDirection().y != r.getDirection().y) { // Crazy check for NaN
			return;
		}
		float px = r.getOrigin().x; 
		float py = r.getOrigin().y;

		float jump = Statics.SQ_SIZE/4;
		float dist = 0;

		int iterations = 0;

		int last_x = -1, last_y = -1;
		while (true) {
			px += r.getDirection().x * jump;
			py += r.getDirection().y * jump;

			int mx = (int)(px / Statics.SQ_SIZE);
			int my = (int)(py / Statics.SQ_SIZE);

			if (iterations > 1) { // So we can shoot round corners
				if (mx != last_x || my != last_y) {
					last_x = mx;
					last_y = my;
					AppletMapSquare sq = this.getSq_MaybeNULL(mx, my);
					if (sq != null) {
						if ((sq.door_type > 0 && sq.door_open == false) || // Door and it's closed 
								sq.major_type == MapDataTable.MT_NOTHING ||  // Nothing, required for LMS-type missions
								sq.major_type == MapDataTable.MT_WALL ||  // Wall
								(sq.major_type == MapDataTable.MT_COMPUTER && ignore_cpu == false) ||
								sq.smoke_type > 0 && ignore_smoke == false) {
							// Ignore smoke squares unless it's on a closed door?
							if (sq.smoke_type > 0 && ignore_smoke) {
								if (sq.door_type <= 0 || sq.door_open){
									continue;
								}
							}
							// Add blockage
							PickData p = new PickData(new DummyWall(null, sq, mx, my), dist);
							results.add(p);
							return; // Stop at first one
						}
					} else {
						return;
					}
				}
			}

			dist += jump;
			iterations++;
		}
	}


	public int getBlockCountInLOS_Pixels_Centre(int map_sx, int map_sy, int map_ex, int map_ey, boolean ignore_smoke, boolean ignore_cpu) {
		float sx = (map_sx * Statics.SQ_SIZE) + (Statics.SQ_SIZE * .5f);
		float sy = (map_sy * Statics.SQ_SIZE) + (Statics.SQ_SIZE * .5f);
		float ex = (map_ex * Statics.SQ_SIZE) + (Statics.SQ_SIZE * .5f);
		float ey = (map_ey * Statics.SQ_SIZE) + (Statics.SQ_SIZE * .5f);
		return getBlockCountInLOS_Pixels(map_ex, map_ey, sx, sy, ex, ey, ignore_smoke, ignore_cpu);
	}


	/**
	 * This will check all four faces of a square to see if it can be seen
	 * @param map_sx
	 * @param map_sy
	 * @param map_ex
	 * @param map_ey
	 * @param ignore_smoke
	 * @param ignore_cpu
	 * @return
	 */
	public boolean isLOS_Faces(int map_sx, int map_sy, int map_ex, int map_ey, boolean ignore_smoke, boolean ignore_cpu) {
		// Left
		float sx = (map_sx * Statics.SQ_SIZE) + (Statics.SQ_SIZE * .5f);
		float sy = (map_sy * Statics.SQ_SIZE) + (Statics.SQ_SIZE * .5f);
		float ex = (map_ex * Statics.SQ_SIZE);
		float ey = (map_ey * Statics.SQ_SIZE) + (Statics.SQ_SIZE * .5f);
		boolean b = getBlockCountInLOS_Pixels(map_ex, map_ey, sx, sy, ex, ey, ignore_smoke, ignore_cpu) <= 0;
		if (b == false) {
			// top
			ex = (map_ex * Statics.SQ_SIZE) + (Statics.SQ_SIZE * .5f);
			ey = (map_ey * Statics.SQ_SIZE);
			b = getBlockCountInLOS_Pixels(map_ex, map_ey, sx, sy, ex, ey, ignore_smoke, ignore_cpu) <= 0;
			if (b == false) {
				// right
				ex = (map_ex * Statics.SQ_SIZE) + (Statics.SQ_SIZE) - 1;
				ey = (map_ey * Statics.SQ_SIZE) + (Statics.SQ_SIZE * .5f);
				b = getBlockCountInLOS_Pixels(map_ex, map_ey, sx, sy, ex, ey, ignore_smoke, ignore_cpu) <= 0;
				if (b == false) {
					// bottom
					ex = (map_ex * Statics.SQ_SIZE) + (Statics.SQ_SIZE * .5f);
					ey = (map_ey * Statics.SQ_SIZE) + (Statics.SQ_SIZE) - 1;
					b = getBlockCountInLOS_Pixels(map_ex, map_ey, sx, sy, ex, ey, ignore_smoke, ignore_cpu) <= 0;
					if (b == false) {
						return false;
					}
				}
			}
		}
		return true;
	}


	private int getBlockCountInLOS_Pixels(int map_ex, int map_ey, float pixel_sx, float pixel_sy, float pixel_ex, float pixel_ey, boolean ignore_smoke, boolean ignore_cpu) {
		int count = 0;
		if (pixel_sx != pixel_ex || pixel_sy != pixel_ey) { // Only bother checking if there's a difference, otherwise get NaN errors!
			Line l = new Line("LOS", new MyPointF(pixel_sx, pixel_sy), new MyPointF(pixel_ex, pixel_ey), null);

			float px = l.getWorldStart().x; 
			float py = l.getWorldStart().y;

			MyPointF dir = l.getVector().normalize();
			float jump = Statics.SQ_SIZE/4;
			float dist = 0;

			int last_x = -1, last_y = -1;
			while (dist <= l.getLength()) {
				px += dir.x * jump;
				py += dir.y * jump;

				int mx = (int)(px / Statics.SQ_SIZE);
				int my = (int)(py / Statics.SQ_SIZE);

				if (mx != last_x || my != last_y) {
					last_x = mx;
					last_y = my;

					// Is this the target square?
					if (mx == map_ex && my == map_ey) {
						break;
					}

					AppletMapSquare sq = this.getSq_MaybeNULL(mx, my);
					if (sq != null) {
						if ((sq.door_type > 0 && sq.door_open == false) || // Door and it's closed 
								sq.major_type == MapDataTable.MT_NOTHING ||  // Nothing, required for LMS-type missions
								sq.major_type == MapDataTable.MT_WALL ||  // Wall
								(sq.major_type == MapDataTable.MT_COMPUTER && ignore_cpu == false) ||
								sq.smoke_type > 0 && ignore_smoke == false) {
							// Ignore smoke squares unless it's on a closed door?
							if (sq.smoke_type > 0 && ignore_smoke) {
								if (sq.door_type <= 0 || sq.door_open){
									continue;
								}
							}
							// Add blockage
							//PickData p = new PickData(new DummyWall(null, sq, mx, my), dist);
							//results.add(p);
							count++; //return false; // Stop at first one
						}
					} else {
						break; //return true;
					}
				}

				dist += jump;

			}
		}
		return count; //true;
	}


	public int getAdjacentTexCode(AppletMapSquare sq, int major_type) {
		for (int y=sq.y-1 ; y<=sq.y+1 ; y++) {
			for (int x=sq.x-1 ; x<=sq.x+1 ; x++) {
				if (x != sq.x || y != sq.y && (x == sq.x || y == sq.y)) {
					AppletMapSquare adj = this.getSq_MaybeNULL(x, y);
					if (adj != null) {
						if (major_type == MapDataTable.MT_FLOOR) {
							if (ignore_floor_tex.contains(adj.texture_code)) {
								continue;
							}
						}
						if (adj.major_type == major_type) {
							return adj.texture_code;
						}
					}
				}
			}
		}
		return -1;
	}


}
