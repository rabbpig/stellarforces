package dsr.data;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.lang.GeometryFuncs;
import ssmith.lang.NumberFunctions;

import com.scs.stellarforces.SoundSelector;
import com.scs.stellarforces.Statics;
import com.scs.stellarforces.game.GameModule;
import com.scs.stellarforces.game.ai.AbstractAI;
import com.scs.stellarforces.game.ai.SimpleRotateAI;
import com.scs.stellarforces.graphics.AbstractCorpse;
import com.scs.stellarforces.graphics.ExpandingTextLabel;
import com.scs.stellarforces.graphics.Ghost;
import com.scs.stellarforces.graphics.icons.AbstractIcon;
import com.scs.stellarforces.graphics.units.AbstractUnit;
import com.scs.stellarforces.graphics.units.Blob;
import com.scs.stellarforces.main.lite.R;

import dsr.comms.UnitDataComms;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.UnitsTable;

public final class UnitData implements Cloneable {

	public int num, unitid, order_by;
	public byte model_type; // E.g. alien
	public byte unit_type; // E.g. engineer
	public byte on_fire;
	private byte side, status;
	public String name, unitcode;
	public int map_x, map_y;
	public int angle; // 0 is facing right/East!!
	public int aps, max_aps, opp_fire_aps_req, protection, armour_type_id; 
	public byte can_deploy, can_equip, opp_fire_01;
	public int max_health, shot_skill, combat_skill, strength, skillid; // , power_points  shot_damage, 
	private int health;
	public ArrayList<EquipmentData> items = new ArrayList<EquipmentData>();
	public EquipmentData current_item;
	public int current_item_id_TMP; // Only used when setting up the data!
	public boolean has_been_seen = false; // So we know if an enemy unit has been seen.
	public boolean can_use_equipment;
	public AbstractUnit model;
	public AbstractIcon icon;
	public ArrayList<UnitData> can_see_at_start = new ArrayList<UnitData>();
	private ArrayList<UnitData> can_see = new ArrayList<UnitData>(); // Which units can they see
	public int curr_morale, curr_energy;
	public byte panicked;
	private AbstractAI ai;
	public int uni_unitid;

	public UnitData(int n) {
		super();
		num = n;
	}


	public UnitData(int _uni_unitid, String _name) { 
		super();

		uni_unitid = _uni_unitid;
		this.name = _name;
	}


	public UnitData clone() {
		try {
			return (UnitData)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}


	public byte getSide() {
		return side;
	}


	public void setSide(byte b) {
		side = b;
	}


	public String toString() {
		return "Unit: " + name + " (S:" + this.side + " ID:" + this.unitid + ")";
	}


	public void clearSeenUnits() {
		this.can_see.clear();

	}


	public void setCanSee(UnitData unit) {
		this.can_see.add(unit);
	}


	public boolean canUnitSee(UnitData unit) {
		if (this.on_fire == 1) {
			return false;
		}
		return this.can_see.contains(unit);
	}


	public UnitData getFirstVisibileEnemy() {
		if (this.can_see.size() > 0) {
			return this.can_see.get(0);
		} else {
			return null;
		}
	}


	public int getStatus() {
		return this.status;
	}


	public void setStatus(byte i) {
		status = i;
	}


	public static UnitData GetUnitDataFromID(UnitData units[], int unitid) {
		for (int i=0 ; i<units.length ; i++) {
			UnitData unit = units[i];
			if (unit.unitid == unitid) {
				return unit;
			}
		}
		throw new RuntimeException("UnitData '" + unitid + "' not found");
	}


	public static UnitData GetUnitDataFromID(ArrayList<UnitData> units, int unitid) {
		for (int i=0 ; i<units.size() ; i++) {
			UnitData unit = units.get(i);
			if (unit.unitid == unitid) {
				return unit;
			}
		}
		throw new RuntimeException("UnitData '" + unitid + "' not found");
	}


	public static UnitData GetUnitDataFromMapSq(UnitData units[], int mx, int my, byte side, boolean error) {
		for (int i=0 ; i<units.length ; i++) {
			UnitData unit = units[i];
			if (unit.map_x == mx && unit.map_y == my && (side <= 0 || unit.side == side)) {
				return unit;
			}
		}
		if (error) {
			throw new RuntimeException("UnitData not found at " + mx + ", " + my);
		} else {
			return null;
		}
	}


	public void damage(GameModule game, int amt, UnitData by_unit, int form_of_attack) {
		AbstractActivity act = Statics.act;
		
		if (form_of_attack  == UnitDataComms.FOA_NERVE_GAS) {
			if (this.model_type == UnitsTable.MT_ALIEN_TYRANT || this.model_type == UnitsTable.MT_QUEEN_ALIEN || this.model_type == UnitsTable.MT_ANGEL) {
				return; // Cannot be harmed
			} else {
				SoundSelector.PlayCoughSound(act);
			}
		}

		if (isAlive()) {
			int by_side = -1;
			int by_unitid = -1;
			String by_name = "fate";
			if (by_unit != null) {
				by_side = by_unit.side;
				by_unitid = by_unit.unitid;
				by_name = by_unit.name;
			}
			boolean who_can_see[] = game.whichSidesCanSeeUnit(this);
			boolean can_we_see = who_can_see[game.game_data.our_side];
			/*if (can_we_see && form_of_attack != UnitDataComms.FOA_MERGED) {
				// Show damage
				game.root_node.attachChild(new ExpandingTextLabel(this.model, ""+amt));
				game.root_node.updateGeometricState();
			}*/
			if (amt > 0) {
				this.incHealth(-amt);
				if (this.health > 0) {
					if (can_we_see && form_of_attack != UnitDataComms.FOA_MERGED) {
						// Show damage
						game.root_node.attachChild(new ExpandingTextLabel(this.model, ""+amt));
						game.root_node.updateGeometricState();
					}
					game.sendEventToServer_UnitWounded(this, by_side, by_unitid, form_of_attack, amt, who_can_see);
					if (can_we_see) {
						if (this.model_type == UnitsTable.MT_ALIEN_TYRANT || this.model_type == UnitsTable.MT_QUEEN_ALIEN) { // Only play if we've done the attack
							act.playSound(R.raw.alienhit1);
						} else if (this.model_type == UnitsTable.MT_BLOB) {
							act.playSound(R.raw.blobmove);
						} else {
							act.playSound(R.raw.hit1);
						}
					}

				} else { // Killed!
					game.game_data.units_remaining[side]--;
					if (can_we_see && form_of_attack != UnitDataComms.FOA_MERGED) {
						// Show damage
						game.root_node.attachChild(new ExpandingTextLabel(this.model, "DEAD"));
						game.root_node.updateGeometricState();
						new Ghost(game, model.getWorldCentreX(), model.getWorldCentreY(), Statics.SQ_SIZE);

					}
					if (status != UnitsTable.ST_DEAD && status != UnitsTable.ST_NO_LONGER_EXISTS) {// In case they've already been killed (i.e. shot dead, then caught in their own death grenade)
						if (form_of_attack == UnitDataComms.FOA_MERGED) {
							this.status = UnitsTable.ST_NO_LONGER_EXISTS;
						} else {
							this.status = UnitsTable.ST_DEAD;
						}

						game.sendEventToServer_UnitKilled(this, by_side, by_unitid, form_of_attack, amt, who_can_see);

						game.checkForDeathGrenadesOrAlienExplode(this);// Must be before the unit is killed as items are dropped, and thus a death grenade won't be activated!
						game.unitKilled(this);
						this.health = 0;

						if (can_we_see) {
							if (this.model_type == UnitsTable.MT_ALIEN_TYRANT || this.model_type == UnitsTable.MT_QUEEN_ALIEN) { // Only play if we've done the attack
								act.playSound(R.raw.aliendeath4);
							} else {
								act.playSound(R.raw.deathscream1);
							}
						}

						this.model.removeFromParent();

						this.model = null;
						AbstractCorpse c = AbstractCorpse.CorpseFactory(game, this);
						if (c != null) {
							game.attachToRootNode(1, c, true);
						}

						// Create a temp equipment so blobs can absorb it - only humans?
						if (this.model_type != UnitsTable.MT_BLOB) { // && this.model_type != UnitsTable..MT_MALE_SIDE_2 || this.model_type == UnitsTable.MT_MALE_SIDE_3 || this.model_type == UnitsTable.MT_MALE_SIDE_4 || this.model_type == UnitsTable.MT_SCIENTIST || this.model_type == UnitsTable.MT_CLONE) {
							//if (this.model_type == UnitsTable.MT_MALE_SIDE_1 || this.model_type == UnitsTable.MT_MALE_SIDE_2 || this.model_type == UnitsTable.MT_MALE_SIDE_3 || this.model_type == UnitsTable.MT_MALE_SIDE_4 || this.model_type == UnitsTable.MT_SCIENTIST || this.model_type == UnitsTable.MT_CLONE) {
							EquipmentData tmp = new EquipmentData();
							tmp.seen_by_side[1] = 1; 
							tmp.seen_by_side[2] = 1; 
							tmp.seen_by_side[3] = 1; 
							tmp.seen_by_side[4] = 1;
							tmp.model = c;
							tmp.setName("Corpse");
							tmp.major_type = (byte)EquipmentTypesTable.CORPSES.get(0).intValue(); // Just choose the first
							game.mapdata.getSq_MaybeNULL(this.map_x, this.map_y).addEquipment(tmp);
						}

						game.recalcVisibleEnemiesAndOppFire(false, null); // NOTICE WE DON'T DO OPP FIRE SINCE THEY ALL SHOOT AGAIN, EVEN THOSE WITH NOTHING TO DO WITH THIS DEATH

						game.updateMenu(); // To remove the enemy unit icon
					}
				}
				// Note that we must update the server AFTER we've updated the unit's data locally!
				if (can_we_see) {
					if (form_of_attack == UnitDataComms.FOA_EXPLOSION) {
						game.addToHUD(this.name + " " + act.getString(R.string.wounded) + " " + amt + " " + act.getString(R.string.by) + " " + act.getString(R.string.explosion));
					} else if (form_of_attack == UnitDataComms.FOA_NERVE_GAS) {
						game.addToHUD(this.name + " " + act.getString(R.string.wounded) + " " + amt + " " + act.getString(R.string.by) + " " + act.getString(R.string.nerve_gas));
					} else if (form_of_attack == UnitDataComms.FOA_FIRE) {
						game.addToHUD(this.name + " " + act.getString(R.string.wounded) + " " + amt + " " + act.getString(R.string.by) + " fire");
					} else if (form_of_attack == UnitDataComms.FOA_MERGED) {
						// Show nothing
					} else {
						game.addToHUD(this.name + " " + act.getString(R.string.wounded) + " " + amt + " " + act.getString(R.string.by) + " " + by_name);
					}
				}
				game.updateUnitOnServer(this);
				if (can_we_see) {
					if (health <= 0) {
						// Write this after we've shown how much they were wounded by
						if (form_of_attack == UnitDataComms.FOA_MERGED) {
							game.addToHUD(name + " " +  act.getString(R.string.absorbed));
						} else {
							game.addToHUD(name + " " +  act.getString(R.string.killed));
						}
					}
				}
			} else { // Not harmed
				if (can_we_see) {
					if (form_of_attack == UnitDataComms.FOA_EXPLOSION) {
						game.addToHUD(this.name + " " + act.getString(R.string.not_harmed) + " " + act.getString(R.string.explosion));
					} else if (form_of_attack == UnitDataComms.FOA_NERVE_GAS) {
						game.addToHUD(this.name + " " + act.getString(R.string.not_harmed) + " " + act.getString(R.string.nerve_gas));
					} else if (form_of_attack == UnitDataComms.FOA_FIRE) {
						game.addToHUD(this.name + " " + act.getString(R.string.not_harmed) + " by fire");
					} else {
						game.addToHUD(this.name + " " + act.getString(R.string.not_harmed) + " " + by_name);
					}
				}
				game.sendEventToServer_UnitWounded(this, by_side, by_unitid, form_of_attack, 0, who_can_see);
			}
		}
	}


	public void escaped(GameModule main) {
		main.unitEscaped(this);
		this.status = UnitsTable.ST_ESCAPED;

		this.model.removeFromParent();
		this.model = null;

		main.updateUnitOnServer(this);
		boolean seen_by_side[] = main.whichSidesCanSeeUnit(this);
		main.sendEventToServer_UnitEscaped(this, seen_by_side);
	}


	public boolean isAlive() {
		return this.status != UnitsTable.ST_DEAD && this.status != UnitsTable.ST_NO_LONGER_EXISTS; // Don't check health as this may be neg, but unit still alive (due to bug) this.health > 0;
	}


	public void setTargetMapLocation(GameModule game, int _mapx, int _mapz, AppletMapSquare sq) {
		AbstractActivity act = Statics.act;
		
		map_x = _mapx;
		map_y = _mapz;
		if (model != null) {
			if (this.status == UnitsTable.ST_AWAITING_DEPLOYMENT) {
				// Just deployed
				updateModelFromUnitData();
			} else { // Must have walked
				game.addToProcess(this.model);
				this.model.anim_walk();
			}
		}

		if (sq != null) {
			// Open the door
			if (sq.door_type > 0) {
				if (sq.door_open == false) {
					act.playSound(R.raw.door);

					if (sq.door != null) {
						sq.door.startOpening();
						sq.door.opposite.startOpening();
					}
					sq.door_open = true;

					game.updateMapOnServer(sq, -1);
				}
			}
		}
	}


	public void turnBy(int a) {
		this.angle += a;
		this.setAngle(angle);
	}


	public void setAngle(int a) {
		this.angle = a;
		this.angle = GeometryFuncs.NormalizeAngle(this.angle);
		if (model != null) {
			updateModelFromUnitData();
		}
	}


	public int getMapX() {
		return this.map_x;
	}


	public int getMapY() {
		return this.map_y;
	}


	public int getAngle() {
		return this.angle;
	}


	/**
	 * This updates the unit's location and direction based on their unitdata
	 */
	public void updateModelFromUnitData() {
		model.setLocation(map_x * Statics.SQ_SIZE, map_y * Statics.SQ_SIZE);
		updateAngleFromUnitData();
		model.updateGeometricState();
	}


	public void updateAngleFromUnitData() {
		this.model.current_bmp = this.model.bmps[(int)(this.angle/45)];

	}


	public int getHealth() {
		return this.health;
	}


	public int getMaxHealth() {
		return this.max_health;
	}


	public void setHealth(int h) {
		this.health = h;

		// Update the model
		if (this.model instanceof Blob) {
			Blob b = (Blob)this.model;
			//b.updateSphere();
		}
	}


	public boolean checkAndReduceAPs(GameModule game, int amt) {
		AbstractActivity act = Statics.act;
		
		if (Statics.DEBUG) {
			return true;
		}
		// See if we're an angel who can be seen
		if (this.model_type == UnitsTable.MT_ANGEL) {
			if (game.canAnyOtherSideSeeUnit(this)) {
				game.addToHUD("You can be seen, so you cannot move!");
				return false;
			}
		}
		if (this.aps >= amt) {
			// Warn if going below opp fire APs
			if (this == game.getCurrentUnit()) {
				if (game.help_mode_on) {
					if (this.aps >= this.opp_fire_aps_req && this.aps-amt < this.opp_fire_aps_req) {
						if (game.ap_lock) {
							act.playSound(R.raw.aps_out);
							game.addToHUD("AP Lock On - Turn off to perform action!");
							return false;
						}
						game.addToHUD(act.getString(R.string.no_longer_opp_fire));
					}
				}
			}
			this.aps -= amt;
			this.checkForNerveGasAndFire(game, amt);
			game.hud.updateStatusBar();
			//if (this != game.current_unit || game.current_unit != null) { // In case killed by nerve gas
			if (this.getHealth() > 0) { // In case killed by nerve gas
				return true;
			} else {
				game.addToHUD("Unit has died.");
				return false;
			}
		} else {
			if (this == game.getCurrentUnit()) {
				act.playSound(R.raw.aps_out);
				game.addToHUD(act.getString(R.string.not_enough_aps) + " (" + amt + ")");
			}
			return false;
		}
	}


	private void checkForNerveGasAndFire(GameModule game, int amt) {
		AppletMapSquare sq = game.mapdata.getSq_MaybeNULL(this.getMapX(), this.getMapY());
		if (sq != null) {
			if (sq.smoke_type == EquipmentTypesTable.ET_NERVE_GAS) {
				UnitData unit_causer = null;
				if (sq.smoke_caused_by > 0) {
					unit_causer = UnitData.GetUnitDataFromID(game.units, sq.smoke_caused_by);
				}
				damage(game, (short)amt, unit_causer, UnitDataComms.FOA_NERVE_GAS);
			} else if (sq.smoke_type == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
				UnitData unit_causer = UnitData.GetUnitDataFromID(game.units, sq.smoke_caused_by);
				if (this.model_type != UnitsTable.MT_ANGEL) {
					this.on_fire = 1;
				}
				short fire_amt = (short)(amt/2);
				if (fire_amt < 1) {
					fire_amt = 1;
				}
				damage(game, fire_amt, unit_causer, UnitDataComms.FOA_FIRE);
			}
		}

		if (this.on_fire == 0) {
			// Check if stood near fire
			if (model_type != UnitsTable.MT_ANGEL) {
				for (int y=this.getMapY()-1 ; y<=this.getMapY()+1 ; y++) {
					for (int x=this.getMapX()-1 ; x<=this.getMapX()+1 ; x++) {
						if (x == this.getMapX() || y == this.getMapY()) { // Don't catch fire diagonally
							sq = game.mapdata.getSq_MaybeNULL(x, y);
							if (sq != null) {
								if (sq.smoke_type == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
									if (NumberFunctions.rnd(1, 2) == 1) {
										this.on_fire = 1;
										game.addToHUD("Unit has caught fire!");
										break;
									}
								}
							}
						}
					}
				}
			}
		} else {
			// Unit is already on fire
			short fire_amt = (short)(amt/2);
			if (fire_amt < 1) {
				fire_amt = 1;
			}
			damage(game, fire_amt, this, UnitDataComms.FOA_FIRE);
		}
	}


	public int getAPs() {
		return this.aps;
	}


	public void setAPs(int a) {
		this.aps = a;
	}


	public void incAPs(int a) {
		this.aps += a;
	}


	public void removeCurrentItem(GameModule main) {
		this.removeItem(main, this.current_item, true);
	}


	public void incHealth(int amt) {
		incHealth(amt, false);
	}


	public void incHealth(int amt, boolean override_max) {
		this.health = (this.health + amt);
		if (this.health > this.max_health) {
			if (override_max) {
				this.max_health = this.health;
			} else {
				this.health = this.max_health;
			}
		}

		// Update the model
		/*if (this.health > 0) {
			if (this.model instanceof Blob) {
				Blob b = (Blob)this.model;
				//b.updateSphere();
			}
		}*/
	}


	public void setMaxHealth(int amt) {
		this.max_health = amt;
	}


	public void removeItem(GameModule main, EquipmentData eq, boolean update_server) {
		this.items.remove(eq);
		if (this.current_item == eq) {
			this.current_item = null;
			if (update_server) {
				main.updateUnitOnServer(this);
			}
		}
		eq.removeFromUnit();

	}


	public int getBlobExplodeDamage() {
		return strength * 2;
	}


	public int getBlobExplodeRad() {
		return strength/10;
	}


	/**
	 * Return false if nothing to do
	 * @return
	 */
	public boolean processAI(GameModule game) {
		return ai.process(game);
	}


	public boolean hasAI() {
		return ai != null;
	}


	public void setAIType(int type) {
		//this.ai_type = type;
		if (type > 0) {
			switch (type) {
			case AbstractAI.AI_ROTATE:
				this.ai = new SimpleRotateAI(this);
				break;
			default: 
				throw new RuntimeException("Unknown AI type: " + type);
			}
		}
	}


	// This function is useful to get distances from dead units (that don't have a model)
	public float getDistanceTo(UnitData other) {
		return (float)GeometryFuncs.distance(this.map_x, this.map_y, other.map_x, other.map_y);
	}



}
