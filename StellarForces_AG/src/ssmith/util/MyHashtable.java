package ssmith.util;

import java.util.Hashtable;

public class MyHashtable<E1, E2> extends Hashtable<E1, E2> {
	
	private static final long serialVersionUID = 1L;

	public MyHashtable() {
		
	}
	
	
	public String get(String s) {
		if (super.containsKey(s)) {
			return (String) super.get(s);
		} else {
			return "";
		}
	}

}
