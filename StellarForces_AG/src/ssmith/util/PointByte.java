package ssmith.util;

public class PointByte {
	
	public byte x, y;
	
	public PointByte(byte _x, byte _y) {
		x = _x;
		y = _y;
	}

	public PointByte(int _x, int _y) {
		this((byte)_x, (byte)_y);
	}

}
