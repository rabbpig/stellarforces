package ssmith.android.util;

public class Timer {
	
	private long interval;
	private long TIME;
	
	public Timer(long _interval) {
		super();
		
		TIME = System.currentTimeMillis() + _interval;
		interval = _interval;
	}
	
	
	public boolean hasHit() {
		boolean result = false;
		if (TIME < System.currentTimeMillis()) {
			TIME = System.currentTimeMillis() + interval;
			result = true;
		}
		return result;
	}
	
	
	public long getTimeRemaining() {
		return TIME - System.currentTimeMillis();
	}

}
