package ssmith.android.util;

public interface IUpdateProgress {

	public void updateProgress(int curr, int max);
}
