package ssmith.android.framework;

import org.acra.*;

public class ErrorReporter {

	private ErrorReporter() {
		
	}
	
	
	public static org.acra.ErrorReporter getInstance() {
		return ACRA.getErrorReporter();
	}

}
