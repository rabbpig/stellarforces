package ssmith.android.framework;

import org.acra.ACRA;

import ssmith.android.framework.keyboard.KeyboardActivity;
import ssmith.android.media.MP3Player;
import ssmith.android.media.SoundManager;
import ssmith.lang.DateFunctions;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

import com.scs.stellarforces.MainThread;
import com.scs.stellarforces.Statics;
import com.scs.stellarforces.exceptions.FailedToConnectException;
import com.scs.stellarforces.main.lite.R;
import com.scs.stellarforces.start.AppRater;
import com.scs.stellarforces.start.ErrorModule;


public abstract class AbstractActivity extends Activity implements Thread.UncaughtExceptionHandler {

	public static MainThread thread; // thread must be here as this is the only constant class
	public static SoundManager sounds;
	private static MP3Player mp3_player;

	public AbstractActivity() {
		super();
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		AbstractActivity.Log("Activity.onCreate...");

		super.onCreate(savedInstanceState);

		Statics.act = this;

		// remove title bar
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		// remove status bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		Thread.setDefaultUncaughtExceptionHandler(this);

		try {
			Statics.VERSION_NUM = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionCode;
			Statics.NAME = getResources().getString(R.string.app_name);
		} catch (NameNotFoundException e) {
			HandleError(e);
		}

		// tell system to use the layout defined in our XML file
		setContentView(R.layout.lunar_layout);

		if (thread != null) {
			if (thread.isAlive() == false) {
				thread = null;
			}
		}
		if (thread == null) {
			thread = new MainThread();
			thread.setRunning(true);
			thread.start();
		}

		if (sounds == null) {
			sounds = new SoundManager(this.getBaseContext());
		}
		
		try {
			if (Statics.ANDROID_MARKET) {
				AppRater.app_launched(this);
			}
		} catch (Exception ex) {
			ACRA.getErrorReporter().handleSilentException(ex);
		}
		Statics.LoadPrefs(this.getBaseContext());
	}


	/**
	 * Invoked when the Activity loses user focus.
	 */
	@Override
	protected void onPause() {
		//AbstractActivity.Log("Activity.onPause...");
		super.onPause();
		thread.setPause(true);
		Statics.SavePrefs(this.getBaseContext());
		pauseMusic();
	}


	/**
	 * 
	 */
	@Override
	protected void onStart() {
		//AbstractActivity.Log("Activity.onStart...");
		super.onStart();
		//Statics.ResumeMusic(this.getBaseContext());
	}


	/**
	 * Invoked when the Activity loses user focus.
	 */
	@Override
	protected void onResume() {
		//AbstractActivity.Log("Activity.onResume...");
		super.onResume();
		thread.setPause(false);
		if (thread.module != null) {
			if (thread.module.playMusic() ) {
				resumeMusic(this.getBaseContext());
			}
		}
		//Statics.LoadSoundManager(this.getBaseContext()); // Since there is a new Context
	}


	@Override
	protected void onStop() {
		//AbstractActivity.Log("Activity.onStop...");
		super.onStop();
		thread.setPause(true);
		pauseMusic();
		//Statics.StopAllSoundEffects();
	}


	@Override
	protected void onDestroy() {
		//AbstractActivity.Log("Activity.onDestroy...");
		//Statics.StopMusic();

		try {
			this.startService();
		} catch (Exception e) {
			ErrorReporter.getInstance().handleSilentException(e);
			e.printStackTrace();
		}

		super.onDestroy();
	}


	public static void Log(String text) {
		if (Statics.RELEASE_MODE == false) {
			Log.d(Statics.NAME, text);
		}
	}


	public static void HandleError(Throwable t) {
		try {
			if (t instanceof FailedToConnectException == false) {
				ErrorReporter.getInstance().handleSilentException(t);
			}
			if (Statics.RELEASE_MODE == false) {
				Log.e(Statics.NAME, t.toString());
			}
			t.printStackTrace();
			if (Statics.act != null) {
				AbstractActivity.thread.setNextModule(new ErrorModule(Statics.act, Statics.MOD_START, t));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		HandleError(ex);
	}


	@Override
	public void onBackPressed() {
		if (thread != null) {
			boolean res = thread.onBackPressed();
			if (res == false) {
				super.onBackPressed();
			}
		}
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (thread != null) {
			if (thread.module != null) {
				thread.module.onActivityResult(requestCode, resultCode, data);
				return;
			} else {
				if (Statics.RELEASE_MODE == false) {
					throw new RuntimeException("Apologies, current module is null.");
				}
			}
		} else {
			if (Statics.RELEASE_MODE == false) {
				throw new RuntimeException("Apologies, thread is null.");
			}
		}
	}


	public String getString(int R, String... params) {
		String orig = getResources().getString(R);
		for (int i=0 ; i<params.length ; i++) {
			orig = orig.replaceAll("%", params[i]);
		}
		return orig;
	}


	public void startService() {
		Intent myIntent = new Intent("com.scs.stellarforces.alert.UpdateService");
		PendingIntent pendingIntent = PendingIntent.getService(AbstractActivity.this, 0, myIntent, 0);
		AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
		alarmManager.setRepeating(AlarmManager.RTC, System.currentTimeMillis() + (DateFunctions.HOUR), AlarmManager.INTERVAL_HALF_DAY, pendingIntent);
	}


	public boolean isWifiValid() {
		ConnectivityManager conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		State wifi = conMan.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();
		return (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING);
	}


	/*public void showToast(final String msg) {
		runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(Statics.act, msg, Toast.LENGTH_SHORT).show();
			}
		});
	}


	public void showPleaseWait(final String msg) {
		//this.dismissPleaseWait(); // Just in case
		if (please_wait_pending) {
			return; // Already got one
		}

		please_wait_pending = true;
		runOnUiThread(new Runnable() {
			public void run() {
				progressDialog = ProgressDialog.show(AbstractActivity.this, "Please wait...", msg);
			}
		});
	}


	public void dismissPleaseWait() {
		if (please_wait_pending) {
			// Wait for it to appear so we can close it
			while (progressDialog == null) {
				Functions.delay(100);
			}
		}
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
		please_wait_pending = false;
	}


	public void showProgressBar(final String msg) {
		this.dismissPleaseWait();
		runOnUiThread(new Runnable() {
			public void run() {
				progressDialog = new ProgressDialog(AbstractActivity.this);//);.show(AbstractActivity.this, "Please wait...", msg);
				progressDialog.setMessage(msg);
				progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				progressDialog.setCancelable(false);
				progressDialog.setProgress(0);
				//ProgressThread progressThread = new ProgressThread(handler);
				//progressThread.start();
				progressDialog.show();

			}
		});
	}


	// Define the Handler that receives messages from the thread and update the progress
	final Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			int total = msg.arg1;
			progressDialog.setProgress(total);
		}
	};


	public void updateProgress(int total) {
		if (progressDialog != null) {
			progressDialog.setProgress(total);
		}
	}


	public boolean isShowingProgressDialog() {
		return this.progressDialog != null;
	}


	 */


	public void showKeyboard(int hint) {
		//this.thread.show_keyboard = true;
		KeyboardActivity.ShowKeyboard(hint);
	}

	
	private void startMusic(Context c) {
		try {
			if (Statics.MUTE_MUSIC == 0 && Statics.MUSIC_R > 0) {
				mp3_player = new MP3Player();
				mp3_player.loadMP3(c, Statics.MUSIC_R);
				mp3_player.play();
			}
		} catch (Exception ex) {
			ErrorReporter.getInstance().handleSilentException(ex);
		}
	}


	public void pauseMusic() {
		try {
			if (mp3_player != null) {
				mp3_player.pause();
			}
		} catch (Exception ex) {
			ErrorReporter.getInstance().handleSilentException(ex);
		}
	}


	public void resumeMusic(Context c) {
		try {
			if (Statics.MUTE_MUSIC == 0) {
				if (mp3_player != null) {
					mp3_player.resume();
				} else {
					startMusic(c);
				}
			}
		} catch (Exception ex) {
			ErrorReporter.getInstance().handleSilentException(ex);
		}
	}


	public void playSound(int r) {
		try {
			if (sounds != null) {
				//if (Statics.act.thread.isPaused() == false) { // Don't play sounds if activity not shown!
					sounds.playSound(r);
				//}
			}
		} catch (Exception ex) {
			ErrorReporter.getInstance().handleSilentException(ex);
		}
	}


	public Point getScreenSize() {
		int widthPixels = -1;
		int heightPixels = -1;
		WindowManager w = this.getWindowManager();
		Display d = w.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		d.getMetrics(metrics);
		// since SDK_INT = 1;
		widthPixels = metrics.widthPixels;
		heightPixels = metrics.heightPixels;
		// includes window decorations (statusbar bar/menu bar)
		if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17) {
			try {
				widthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(d);
				heightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(d);
			} catch (Exception ignored) {
				// Do nothing
			}
		}
		// includes window decorations (statusbar bar/menu bar)
		if (Build.VERSION.SDK_INT >= 17) {
			try {
				Point realSize = new Point();
				Display.class.getMethod("getRealSize", Point.class).invoke(d, realSize);
				widthPixels = realSize.x;
				heightPixels = realSize.y;
			} catch (Exception ignored) {
				// Do nothing
			}
		}
		if (widthPixels > 0 && heightPixels > 0) {
			return new Point(widthPixels,heightPixels);
		} else {
			return null;
		}
	}
}

