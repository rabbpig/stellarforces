package ssmith.android.framework;

public class DontStopThreadException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public DontStopThreadException(String s) {
		super(s);
	}

}
