package ssmith.android.framework;


import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.scs.stellarforces.Statics;

/**
 * This gets recreated when the game is re-opened!
 *
 */
public final class GameView extends SurfaceView implements SurfaceHolder.Callback {

	private SurfaceHolder holder;

	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);

		// register our interest in hearing about changes to our surface
		holder = super.getHolder();
		holder.addCallback(this);

		setFocusable(true); // make sure we get key events
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent msg) {
	    /*if (keyCode == KeyEvent.KEYCODE_BACK) {
	        AbstractActivity.Log("Back button pressed");
	    }*/
		if (AbstractActivity.thread != null) {
			return AbstractActivity.thread.onKeyDown(keyCode, msg);
		}
		return false;
	}


	@Override
	public boolean onKeyUp(int keyCode, KeyEvent msg) {
		if (AbstractActivity.thread != null) {
			return AbstractActivity.thread.onKeyUp(keyCode, msg);
		}
		return false;
	}


	/**
	 * Standard window-focus override. Notice focus lost so we can pause on
	 * focus lost. e.g. user switches to take a call.
	 */
	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		if (AbstractActivity.thread != null) {
			AbstractActivity.thread.setPause(!hasWindowFocus);
		}
	}


	/* Callback invoked when the surface dimensions change. */
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		// synchronized to make sure these all change atomically
		synchronized (holder) {
			Statics.init(width, height, this.getContext());
		}
	}


	/*
	 * Callback invoked when the Surface has been created and is ready to be
	 * used.
	 */
	public void surfaceCreated(SurfaceHolder holder) {
		// Attach ourselves to the thread
		AbstractActivity.thread.setSurfaceHolder(holder);
		//AbstractActivity.thread.setPause(false);  No!  As it restarts the app even if we're supposed to be paused
	}


	/*
	 * Callback invoked when the Surface has been destroyed and must no longer
	 * be touched. WARNING: after this method returns, the Surface/Canvas must
	 * never be touched again!
	 */
	public void surfaceDestroyed(SurfaceHolder holder) {
		AbstractActivity.thread.setPause(true);
	}


	/**
	 * 0, 2, 2, 2, 2, 2, 2, 1
	 * 0 = DOWN
	 * 2 = MOVE
	 * 1 = UP
	 */
	public boolean onTouchEvent(MotionEvent event) {
		//Log.d(Statics.NAME, "Action: " + event.getAction() + " - Coords: " + event.getX() + "," + event.getY());
		if (AbstractActivity.thread != null) {
			AbstractActivity.thread.addMotionEvent(new MyEvent(event.getAction(), event.getX(), event.getY()));
			return true;
		}
		return false;
	}


}
