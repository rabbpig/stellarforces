package ssmith.android.framework.keyboard;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.scs.stellarforces.Statics;

public class KeyboardActivity extends Activity implements DialogInterface.OnKeyListener {
	
	public static final String CR_TO_END = "cr_to_end";

	private AlertDialog.Builder alert;
	private EditText input;
	private boolean cr_to_end = true;

	public static boolean show_keyboard = false;
	public static boolean showing_keyboard = false;
	private static int text_hint = -1;

	public static void ShowKeyboard(int hint) {
		if (showing_keyboard == false) {
			show_keyboard = true;
			text_hint = hint;
		}
	}
	
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.showKeyboard_New("", "existing text", false);
	}


	private void showKeyboard_New(String code, String curr_val, boolean mask) {
		alert = new AlertDialog.Builder(this);

		//alert.setTitle("Title");
		//alert.setMessage("Message");

		alert.setOnKeyListener(this);

		// Set an EditText view to get user input 
		input = new EditText(this);
		if (Statics.data.containsKey("text")) {
			input.setText(Statics.data.get("text").toString());
		}
		if (Statics.data.containsKey(CR_TO_END)) {
			cr_to_end = Statics.data.get(CR_TO_END).equalsIgnoreCase("yes");
		}
		alert.setView(input);

		if (text_hint > 0) {
			input.setInputType(text_hint); // InputType.
		}

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				complete(dialog);
			}
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Intent returnIntent = new Intent();
				//returnIntent.putExtra("result", input.getText().toString());
				setResult(Activity.RESULT_CANCELED, returnIntent);
				// Cancelled
				//dialog.cancel();
				finish();
			}
		});

		alert.show();

		InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

	}


	@Override
	public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
		if (keyCode == 66) {
			if (event.getAction() == KeyEvent.ACTION_UP) {
				if (cr_to_end) {
					complete(dialog);
					return true;
				}
			}
		}
		return false;
	}


	private void complete(DialogInterface dialog) {
		InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(input.getWindowToken(), 0);

		// Store the result to be passed back
		String code = Statics.data.get("code").toString();
		Statics.data.clear();
		Statics.data.put(code, input.getText().toString());

		Intent returnIntent = new Intent();
		setResult(RESULT_OK, returnIntent);
		
		dialog.cancel();
		
		finish();
	}

	
	public void finish() {
		showing_keyboard = false;
		super.finish();
	}
	

}
