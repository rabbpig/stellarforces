package ssmith.android.framework.modules;

import com.scs.stellarforces.Statics;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.MyEvent;
import ssmith.android.io.IOFunctions;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.lang.GeometryFuncs;
import android.graphics.PointF;
import android.view.MotionEvent;

public abstract class SimpleScrollingAbstractModule extends AbstractModule {

	private static final float MIN_DRAG_DIST = Statics.SCREEN_HEIGHT/25;

	private PointF last_down_screen = new PointF();
	private boolean is_dragging = false;
	private boolean can_drag = true;
	protected boolean limit_dragging = true; // Ensure the root node can only stay within screen boundaries

	public SimpleScrollingAbstractModule(int am) {
		super(am);
	}


	@Override
	public void updateGame(long interpol) {
		if (limit_dragging) {
			can_drag = this.root_node.getWorldBounds().top < this.root_cam.top || this.root_node.getWorldBounds().bottom > this.root_cam.bottom;
		}
	}


	@Override
	public boolean processEvent(MyEvent ev) throws Exception {
		AbstractActivity act = Statics.act;
		
		if (ev.getAction() == MotionEvent.ACTION_UP) { // Note we only catch UP so we don't select two options at the same time!
			last_down_screen.y = ev.getY();

			if (is_dragging == false) { // Check for an icon pressed
				// Adjust for camera location

				AbstractComponent c = super.GetComponentAt(this.stat_node, ev.getX(), ev.getY());
				if (c == null) {
					// Check root node
					float x = ev.getX() + root_cam.left;
					float y = ev.getY() + this.root_cam.top;
					c = super.GetComponentAt(this.root_node, x, y);
				}
				if (c != null) {
					IOFunctions.Vibrate(act.getBaseContext(), Statics.VIBRATE_LEN);
					handleClick(c);
					return true;
				} else {
					noComponentClicked();
				}
			} 
			is_dragging = false;
		} else if (ev.getAction() == MotionEvent.ACTION_MOVE) { // Dragging!
			if (can_drag) {
				float offy = last_down_screen.y - ev.getY();
				double dist = GeometryFuncs.distance(0, 0, 0, offy);
				if (dist > MIN_DRAG_DIST || is_dragging) {
					this.root_cam.moveCam(0, offy);
					if (limit_dragging) {
						if (this.root_node.getHeight() > Statics.SCREEN_HEIGHT) { // Do we need to scroll
							if (this.root_cam.top < this.root_node.getWorldY()) {
								this.root_cam.moveCam(0, this.root_node.getWorldY() - this.root_cam.top);
							} else if (this.root_cam.bottom > this.root_node.getWorldBounds().bottom) {
								this.root_cam.moveCam(0, this.root_node.getWorldBounds().bottom - this.root_cam.bottom);
							}
						}
					} else {
						// Allow scrolling all the way to the opposite edge
						if (this.root_node.getHeight() > Statics.SCREEN_HEIGHT) { // Do we need to scroll
							if (this.root_cam.bottom < this.root_node.getWorldY()) {
								this.root_cam.moveCam(0, this.root_node.getWorldY() - this.root_cam.bottom);
							} else if (this.root_cam.top > this.root_node.getWorldBounds().bottom) {
								this.root_cam.moveCam(0, this.root_node.getWorldBounds().bottom - this.root_cam.top);
							}
						}
					}
					is_dragging = true;
					last_down_screen.y = ev.getY();
				}
			}
		} else if (ev.getAction() == MotionEvent.ACTION_DOWN) { // Dragging!
			last_down_screen.y = ev.getY();
		}	
		return false;			
	}


	public abstract void handleClick(AbstractComponent c) throws Exception;


	public void noComponentClicked() {
		// Override if required
	}


}
