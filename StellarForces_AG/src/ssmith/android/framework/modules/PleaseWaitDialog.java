package ssmith.android.framework.modules;

import ssmith.android.lib2d.Node;
import ssmith.android.lib2d.gui.GUIFunctions;
import ssmith.android.lib2d.gui.Label;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Typeface;

import com.scs.stellarforces.Statics;

public class PleaseWaitDialog extends Node {

	//private static final float ICON_WIDTH = Statics.SCREEN_WIDTH/2;

	private static Paint paint_menu_text = new Paint();
	private static Paint paint_paper = new Paint();

	static {
		paint_menu_text.setARGB(255, 255, 255, 255);
		paint_menu_text.setAntiAlias(true);
		paint_menu_text.setTextSize(200);

		paint_paper.setARGB(135, 0, 0, 0);
		paint_paper.setAntiAlias(true);
		paint_paper.setStyle(Style.FILL);
	}


	public PleaseWaitDialog(String msg, Typeface tf) {
		super("PleaseWaitDialog");

		if (tf != null) {
			paint_menu_text.setTypeface(tf);
		}

		float new_size = GUIFunctions.GetTextSizeToFit(msg, Statics.SCREEN_WIDTH * 0.75f, -1);
		if (new_size < paint_menu_text.getTextSize()) {
			paint_menu_text.setTextSize(new_size);
		}
		Label l = new Label("pw", msg, paint_paper, paint_menu_text);
		l.setLocation(0, Statics.SCREEN_HEIGHT - l.getHeight());
		this.attachChild(l);

		this.updateGeometricState();

	}

}
