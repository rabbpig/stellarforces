package ssmith.android.framework.modules;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.io.IOFunctions;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineLabel;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public class ConfirmModule extends SimpleAbstractModule {
	
	public static final String YES = "yes";
	public static final String NO = "no";

	private static float ICON_WIDTH = Statics.SCREEN_WIDTH/3;
	private static float ICON_HEIGHT = Statics.SCREEN_HEIGHT/7;
	
	private static Paint paint_large_text = new Paint();
	private static Paint paint_normal_text = new Paint();
	
	private Button cmd_yes, cmd_no;
	private String cmd;

	static {
		paint_large_text.setARGB(255, 255, 255, 255);
		paint_large_text.setAntiAlias(true);
		//paint_large_text.setStyle(Style.STROKE);
		paint_large_text.setTextSize(Statics.GetHeightScaled(0.09f));

		paint_normal_text.setARGB(255, 255, 255, 255);
		paint_normal_text.setAntiAlias(true);
		paint_normal_text.setTextSize(Statics.GetHeightScaled(0.08f));

	}
	
	
	public ConfirmModule(AbstractActivity act, AbstractModule _return_to, String title, String text, int background_r, String _cmd) {
		super(-1);
		
		if (Statics.GetTypeface(act) != null) {
			paint_large_text.setTypeface(Statics.GetTypeface(act));
			paint_normal_text.setTypeface(Statics.GetTypeface(act));
		}

		this.mod_return_to = _return_to;
		cmd = _cmd;

		this.setBackground(background_r);

		showMsg(title, text);
	}
	

	private void showMsg(String title, String text) {
		Label l = new Label("Title", title, 0, 0, null, paint_large_text, true);
		l.setCentre(Statics.SCREEN_WIDTH/2, paint_large_text.getTextSize());
		this.stat_node.attachChild(l);

		MultiLineLabel label2 = new MultiLineLabel("credits", text, null, paint_normal_text, true, Statics.SCREEN_WIDTH * 0.9f);
		label2.setLocation(10, Statics.SCREEN_HEIGHT * 0.3f);
		stat_node.attachChild(label2);
		
		cmd_yes = new Button("Yes", "Yes", null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		cmd_yes.setLocation(Statics.SCREEN_WIDTH * 0.1f, Statics.SCREEN_HEIGHT * 0.8f);
		stat_node.attachChild(cmd_yes);
		
		cmd_no = new Button("No", "No", null, paint_normal_text, Statics.img_cache.getImage(Statics.BUTTON_R_OVAL, ICON_WIDTH, ICON_HEIGHT));
		cmd_no.setLocation(Statics.SCREEN_WIDTH * 0.6f, Statics.SCREEN_HEIGHT * 0.8f);
		stat_node.attachChild(cmd_no);
		
		this.stat_node.updateGeometricState();
		this.stat_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);
	}

	
	@Override
	public void handleClick(AbstractComponent c) throws Exception {
		if (c == cmd_yes) {
			Statics.data.clear();
			Statics.data.put(cmd, YES);
			this.returnTo();
		} else if (c == cmd_no) {
			this.onBackPressed();
		}
		
	}


	@Override
	public boolean onBackPressed() {
		AbstractActivity act = Statics.act;
		
		IOFunctions.Vibrate(act.getBaseContext(), Statics.VIBRATE_LEN);
		Statics.data.clear();
		Statics.data.put(cmd, NO);
		return returnTo();
	}


}

