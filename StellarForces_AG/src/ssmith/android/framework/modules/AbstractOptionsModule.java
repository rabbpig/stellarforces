package ssmith.android.framework.modules;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.MyEvent;
import ssmith.android.io.IOFunctions;
import ssmith.android.lib2d.Spatial;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.Button;
import ssmith.android.lib2d.gui.GUIFunctions;
import ssmith.android.lib2d.layouts.GridLayout;
import ssmith.android.lib2d.shapes.Geometry;
import ssmith.lang.GeometryFuncs;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.PointF;
import android.view.MotionEvent;

import com.scs.stellarforces.Statics;

public abstract class AbstractOptionsModule extends AbstractModule {

	private static final float MIN_DRAG_DIST = 5f;
	private static final float MAX_DRAG_DIST = 60f;

	private ArrayList<String> options;
	private PointF last_down_screen = new PointF();
	private boolean is_dragging = false;
	private int cols;
	private Bitmap bmp;
	private Paint paint_ink;
	private boolean can_drag, auto_select;
	protected int show; // -1, 0 or 1

	public AbstractOptionsModule(int _return_to, int _cols, Paint _paint_ink, Bitmap _bmp, int _show, boolean _auto_select) {
		super(_return_to);

		cols =_cols;
		paint_ink = _paint_ink;
		bmp = _bmp;
		show = _show;
		auto_select = _auto_select;
	}


	public abstract ArrayList<String> getOptions();

	public abstract void optionSelected(String cmd);


	private void setOptions() {
		options = getOptions();

		// Create initial menu
		// this.stat_node.removeAllChildren();  NO!  As we may have added some other controls

		if (options != null) {
			int curr_col = 1;
			int curr_row = 1;
			String longest = "";
			GridLayout menu_node = new GridLayout("Menu", bmp.getWidth(), bmp.getHeight(), Statics.GetButtonSpacing());
			for (int i=0 ; i<options.size() ; i++) {
				Button b = new Button(options.get(i), options.get(i), null, paint_ink, bmp);
				menu_node.attachChild(b, curr_col, curr_row);

				curr_col++;
				if (curr_col > cols) {
					curr_col = 1;
					curr_row++;
				}
				if (options.get(i).length() > longest.length()) {
					longest = options.get(i);
				}

			}
			paint_ink.setTextSize(GUIFunctions.GetTextSizeToFit(longest, bmp.getWidth(), bmp.getHeight()));

			// Loop through children and reposition text now that the text size has changed
			for (Spatial g : menu_node.getChildren()) {
				if (g instanceof Button) {
					Button b = (Button)g;
					b.calcTextOffset();
				}
			}

			this.root_node.attachChild(menu_node);
			root_node.updateGeometricState();

			/*if (show_end) {
				stat_cam.lookAt(menu_node.getWorldCentreX(), menu_node.getHeight(), true);
			} else {
				stat_cam.lookAt(menu_node, true);
			}*/
			can_drag = menu_node.getHeight() > Statics.SCREEN_HEIGHT;
			if (can_drag) {
				if (show == 1) {
					root_cam.lookAt(menu_node.getWorldCentreX(), menu_node.getHeight(), true);
				} else if (show == -1) {
					root_cam.lookAt(menu_node.getWorldCentreX(), menu_node.getWorldY() + (Statics.SCREEN_HEIGHT/2), true);
				} else if (show == 0) {
					root_cam.lookAt(menu_node, true);
				} else {
					// Do nothing - we might be returning and want to view where we were before
				}
			} else {
				root_cam.lookAt(menu_node, true);
			}

			show = 999; // stop us moving next time

		}
	}


	@Override
	public boolean processEvent(MyEvent ev) throws Exception {
		AbstractActivity act = Statics.act;
		
		if (ev.getAction() == MotionEvent.ACTION_UP) { // Note we only catch UP so we don't select two options at the same time!
			last_down_screen.y = ev.getY();

			if (is_dragging == false) { // Check for an icon pressed
				// Adjust for camera location
				float x = ev.getX() + root_cam.left;
				float y = ev.getY() + this.root_cam.top;

				ArrayList<Geometry> colls = this.root_node.getCollidersAt(x, y);
				if (colls.size() > 0) {
					for (Geometry g : colls) {
						if (g instanceof AbstractComponent) {
							AbstractComponent b = (AbstractComponent)g;
							String selected_cmd = b.getActionCommand();
							if (selected_cmd.length() > 0) { // In case it's a textbox or something
								IOFunctions.Vibrate(act.getBaseContext(), Statics.VIBRATE_LEN);
								act.playSound(Statics.TYPE_SFX);
								this.selectOption(selected_cmd);
								return true;
							}
						}
					}
				}
			} 
			is_dragging = false;
		} else if (ev.getAction() == MotionEvent.ACTION_MOVE) { // Dragging!
			if (can_drag) {
				float offy = last_down_screen.y - ev.getY();
				double dist = GeometryFuncs.distance(0, 0, 0, offy);
				if (dist > MIN_DRAG_DIST && dist < MAX_DRAG_DIST) {//|| is_dragging) {
					/*if (NumberFunctions.mod(offy) > 40f) {
						Log.d(Statics.NAME, "Action: " + ev.getAction());
						Log.d(Statics.NAME, "BIG!!!!:" + offy);
					}*/
					this.root_cam.moveCam(0, offy);
					is_dragging = true;
				}
				last_down_screen.y = ev.getY();
			}
		}	
		return false;			
	}


	private void selectOption(String cmd) {
		this.optionSelected(cmd);
		/*if (this.getThread().next_module != null) { // Have we chosen another module?
			this.startExploding();
		}*/

	}


	@Override
	public void updateGame(long interpol) {
		if (options == null) {
			this.setOptions();
			// See if there's only one option.
			if (auto_select) {
				if (options != null) {
					if (options.size() == 1) {
						this.selectOption(options.get(0));
					}
				}
			}
		}
	}


	@Override
	public boolean onBackPressed() {
		returnTo();
		return true;
	}

}
