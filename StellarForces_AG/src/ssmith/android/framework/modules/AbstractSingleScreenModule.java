package ssmith.android.framework.modules;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.MyEvent;
import ssmith.android.io.IOFunctions;
import android.view.MotionEvent;

import com.scs.stellarforces.Statics;

/**
 * This is a module that will return to the previous module as soon as the screen is touched.
 * @author stevec
 *
 */
public abstract class AbstractSingleScreenModule extends AbstractModule {

	private static final long PAUSE_DURATION = 500;

	private long start_time;

	public AbstractSingleScreenModule(int _return_to) {
		super(_return_to);

		start_time = System.currentTimeMillis();
		
		stat_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);
	}

	
	@Override
	public boolean processEvent(MyEvent evt) throws Exception {
		AbstractActivity act = Statics.act;
		
		if (System.currentTimeMillis() > start_time + PAUSE_DURATION) {
			if (evt.getAction() == MotionEvent.ACTION_UP) {
				IOFunctions.Vibrate(act.getBaseContext(), Statics.VIBRATE_LEN);
				returnTo();
				return true;
			}
		}
		return false;
	}

	
	@Override
	public void updateGame(long interpol) {
		// Do nothing

	}
	
	
}
