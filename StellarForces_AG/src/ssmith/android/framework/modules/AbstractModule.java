package ssmith.android.framework.modules;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.MyEvent;
import ssmith.android.io.IOFunctions;
import ssmith.android.lib2d.Camera;
import ssmith.android.lib2d.Node;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.shapes.Geometry;
import ssmith.lang.Functions;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.KeyEvent;

import com.scs.stellarforces.MainThread;
import com.scs.stellarforces.Statics;

public abstract class AbstractModule extends Thread {

	//public AbstractActivity act;

	public Camera root_cam;
	public Node root_node = new Node("root_node");
	public Node stat_node = new Node("stat_node");
	protected Camera stat_cam; // For stat node so some graphics are always drawn at the same pos
	private Bitmap background;
	protected int i_return_to;
	public AbstractModule mod_return_to;

	private PleaseWaitDialog please_wait_dialog;
	public static String debug_text = "";
	
	private static Paint paint_small_text = new Paint();

	static {
		paint_small_text.setARGB(255, 255, 255, 255);
		paint_small_text.setAntiAlias(true);
		paint_small_text.setTextSize(Statics.SCREEN_WIDTH * 0.05f);
	}

	public AbstractModule(int _return_to) {
		super("AbstractModule_Thread");

		i_return_to = _return_to;

		root_cam = new Camera();
		root_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true); // Default positions
		stat_cam = new Camera();
		stat_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true); // Default positions
	}


	public MainThread getThread() {
		return AbstractActivity.thread;// this.view.thread;
	}


	/**
	 * If true is returned, all remaining events are removed
	 * 
	 * @param evt
	 * @return
	 * @throws Exception
	 */
	public abstract boolean processEvent(MyEvent evt) throws Exception;


	public void started() {
		// Override if reqd.
	}


	public void stopped() {
		// Override if reqd.
	}


	public void setBackground(int key) {
		background = Statics.img_cache.getImage(key, Statics.SCREEN_WIDTH, Statics.SCREEN_HEIGHT);
	}


	public void doDraw(Canvas c, long interpol) {
		if (this.background != null) {
			/*if (this.background.isRecycled()) {
				throw new RuntimeException("Recycled bitmap!"); // this
			}*/
			c.drawBitmap(this.background, 0, 0, null);
		}

		root_cam.update(interpol);
		root_node.doDraw(c, root_cam, interpol);

		stat_cam.update(interpol);
		stat_node.doDraw(c, stat_cam, interpol);

		if (Statics.RELEASE_MODE == false || Statics.DEBUG) {
			c.drawText(debug_text, 5, 200, paint_small_text);
		}
		
	}


	public abstract void updateGame(long interpol);


	public AbstractComponent GetComponentAt(Node node, float x, float y) {
		ArrayList<Geometry> colls = node.getCollidersAt(x, y);
		if (colls.size() > 0) {
			for (Geometry g : colls) {
				if (g instanceof AbstractComponent) {
					AbstractComponent b = (AbstractComponent)g;
					return b;
				}
			}
		}
		return null;
	}


	public boolean onKeyDown(int keyCode, KeyEvent msg) {
		// Override if req'd.
		return false;
	}


	public boolean onKeyUp(int keyCode, KeyEvent msg) {
		// Override if req'd.
		return false;
	}


	/**
	 * Override if required.
	 * @return
	 */
	public boolean onBackPressed() {
		AbstractActivity act = Statics.act;
		
		IOFunctions.Vibrate(act.getBaseContext(), Statics.VIBRATE_LEN);
		return returnTo();
	}


	protected boolean returnTo() {
		//this.ready_to_exit = true; // Override!
		if (this.mod_return_to != null) {
			this.getThread().setNextModule(this.mod_return_to);
			return true;
		} else if (i_return_to >= 0) {
			AbstractModule m = Statics.GetModule(i_return_to);
			this.getThread().setNextModule(m);
			return true;
		}
		return false;

	}


	public void showPleaseWait(String msg) {
		this.dismissPleaseWait();
		if (please_wait_dialog == null) {
			please_wait_dialog = new PleaseWaitDialog(msg, Statics.tf);
			this.stat_node.attachChild(please_wait_dialog);
			this.stat_node.updateGeometricState();
			this.getThread().doDrawing();
		}
	}
	
	
	public void dismissPleaseWait() {
		if (please_wait_dialog != null) {
			please_wait_dialog.removeFromParent();
			please_wait_dialog = null;
			this.stat_node.updateGeometricState();
			this.getThread().doDrawing();
		}
		
	}
	
	
	public void showToast(String s) {
		this.showPleaseWait(s);
		Runnable r = new Runnable()  {
			public void run() {
				Functions.delay(1000*2);
				dismissPleaseWait();
			}
		};
		Thread t = new Thread(r);
		t.setName("ShowToastThread");
		t.start();
	}
	
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Override if required
	}
	

	// Does this module want to play music?
	public boolean playMusic() {
		// Override if required
		return true;
		
	}
	
}
