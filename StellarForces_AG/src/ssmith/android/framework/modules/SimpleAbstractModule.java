package ssmith.android.framework.modules;

import com.scs.stellarforces.Statics;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.MyEvent;
import ssmith.android.io.IOFunctions;
import ssmith.android.lib2d.gui.AbstractComponent;
import android.view.MotionEvent;


/**
 * This is for modules that have no dragging and use the 2D GUI.
 *
 */
public abstract class SimpleAbstractModule extends AbstractModule {

	private static final long CLICK_DURATION = 200;

	private long start_time = 0;

	public SimpleAbstractModule(int am) {
		super(am);
	}


	@Override
	public void started() {
		start_time = System.currentTimeMillis();
	}


	@Override
	public void updateGame(long interpol) {
		// Do nothing
	}


	public boolean processEvent(MyEvent evt) throws Exception {
		AbstractActivity act = Statics.act;
		
		//if (this.started_exploding == false) { // Don't accept input when exploding
			long time = System.currentTimeMillis() - start_time; 
			if (time > CLICK_DURATION) {
				if (evt.getAction() == MotionEvent.ACTION_UP) {
					// Adjust for camera location
					float x = evt.getX() + stat_cam.left;
					float y = evt.getY() + this.stat_cam.top;
					AbstractComponent c = super.GetComponentAt(this.stat_node, x, y);
					if (c != null) {
						IOFunctions.Vibrate(act.getBaseContext(), Statics.VIBRATE_LEN);
						act.playSound(Statics.TYPE_SFX);
						handleClick(c);
						return true;
					} else {
						return noComponentPressed();
					}
				}
				return false; // Don't remove the ACTION_UP!
			}
		//}
		return true;
	}


	protected abstract void handleClick(AbstractComponent c) throws Exception;

	
	protected boolean noComponentPressed() {
		return true; // Override if required
	}

}
