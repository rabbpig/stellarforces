package ssmith.android.framework.modules;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.MyEvent;
import ssmith.android.io.IOFunctions;
import ssmith.android.lib2d.gui.AbstractComponent;
import ssmith.android.lib2d.gui.GUIFunctions;
import ssmith.android.lib2d.gui.Label;
import ssmith.android.lib2d.gui.MultiLineButton;
import ssmith.android.lib2d.layouts.VerticalFlowLayout;
import ssmith.android.lib2d.shapes.Geometry;
import ssmith.lang.GeometryFuncs;
import android.graphics.Paint;
import android.graphics.PointF;
import android.view.MotionEvent;

import com.scs.stellarforces.Statics;

public abstract class AbstractMultilineOptionsModule2 extends AbstractModule {

	private static final float MIN_DRAG_DIST = Statics.SCREEN_HEIGHT/46f; //15f;
	//private static final float MAX_DRAG_DIST = Statics.SCREEN_HEIGHT/6f; //60f;

	private static Paint paint_title = new Paint();

	static {
		paint_title.setARGB(200, 130, 130, 130);
		paint_title.setAntiAlias(true);
		//paint_title.setStyle(Style.STROKE);
	}

	private ArrayList<String> al_button_texts, al_action_cmd_texts;
	private PointF last_down_screen = new PointF();
	private boolean is_dragging = false;
	private Paint paint_paper, paint_ink;
	private boolean can_drag, auto_select, remember_position;//, trunc_names;
	protected int show; // -1, 0 or 1
	protected Label lbl_title;
	private float width;


	public AbstractMultilineOptionsModule2(AbstractActivity act, int _return_to, Paint _paint_paper, Paint _paint_ink, int _show, boolean _auto_select, String title, float _width, boolean _remember_position) {
		super(_return_to);

		if (Statics.GetTypeface(act) != null) {
			paint_title.setTypeface(Statics.GetTypeface(act));
		}

		paint_ink = _paint_ink;
		paint_paper = _paint_paper;
		show = _show;
		auto_select = _auto_select;
		width = _width;
		remember_position = _remember_position;

		lbl_title = new Label("title", "                                               ", null, paint_title, false);
		lbl_title.setLocation(Statics.SCREEN_WIDTH * 0.05f, Statics.SCREEN_HEIGHT * 0.05f);
		lbl_title.updateGeometricState();
		this.stat_node.attachChild(lbl_title);
		this.stat_node.updateGeometricState();

		this.setTitle(title);

		this.stat_cam.lookAt(Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, true);
	}


	protected void setTitle(String title) {
		paint_title.setTextSize(GUIFunctions.GetTextSizeToFit(title, Statics.SCREEN_WIDTH * 0.75f, -1));
		lbl_title.setText(title);
		lbl_title.updateGeometricState();
	}


	public abstract void getOptions();


	public abstract void optionSelected(int idx);


	protected void setOptions() {
		al_button_texts = new ArrayList<String>();
		al_action_cmd_texts = new ArrayList<String>();
		getOptions();
		this.root_node.removeAllChildren();
		if (al_button_texts.size() > 0) {
			//String longest = "";
			VerticalFlowLayout menu_node = new VerticalFlowLayout("Menu", Statics.SCREEN_HEIGHT * 0.02f);
			for (int i=0 ; i<al_button_texts.size() ; i++) {
				String action_cmd = this.al_action_cmd_texts.get(i);
				if (action_cmd.length() == 0) {
					action_cmd = i + "_" + al_button_texts.get(i);
					al_action_cmd_texts.remove(i);
					al_action_cmd_texts.add(i, action_cmd);
				}
				String button_text = al_button_texts.get(i);
				//button_text = AbstractMultilineOptionsModule.FormatText(button_text, paint_ink, 4, Statics.SCREEN_WIDTH*0.8f);
				MultiLineButton b = new MultiLineButton("mlb", action_cmd, button_text, paint_paper, paint_ink, true, width, true);
				b.setCollides(true);
				menu_node.attachChild(b);

				/*if (al_button_texts.get(i).length() > longest.length()) {
					longest = al_button_texts.get(i);
				}*/

			}
			this.root_node.attachChild(menu_node);
			root_node.updateGeometricState();

			can_drag = menu_node.getHeight() > Statics.SCREEN_HEIGHT;
			if (can_drag) {
				if (show == 1) {
					root_cam.lookAt(menu_node.getWorldCentreX(), menu_node.getHeight(), true);
				} else if (show == -1) {
					root_cam.lookAt(menu_node.getWorldCentreX(), menu_node.getWorldY() + (Statics.SCREEN_HEIGHT/2), true);
				} else if (show == 0) {
					root_cam.lookAt(menu_node, true);
				} else {
					// Do nothing - we might be returning and want to view where we were before
				}
			} else {
				root_cam.lookAt(menu_node, true);
			}

			if (remember_position) {
				show = 999; // stop us moving next time so we are at the same position
			}

		}
	}


	@Override
	public boolean processEvent(MyEvent ev) throws Exception {
		if (ev.getAction() == MotionEvent.ACTION_UP) { // Note we only catch UP so we don't select two options at the same time!
			last_down_screen.y = ev.getY();

			if (is_dragging == false) { // Check for an icon pressed
				// Adjust for camera location
				float x = ev.getX() + root_cam.left;
				float y = ev.getY() + this.root_cam.top;

				ArrayList<Geometry> colls = this.root_node.getCollidersAt(x, y);
				if (colls.size() > 0) {
					for (Geometry g : colls) {
						if (g instanceof AbstractComponent) {
							AbstractComponent b = (AbstractComponent)g;
							String selected_cmd = b.getActionCommand();
							if (selected_cmd.length() > 0) { // In case it's a textbox or something
								this.selectOption(this.al_action_cmd_texts.indexOf(selected_cmd));
								return true;
							}
						}
					}
				}
			} 
			is_dragging = false;
		} else if (ev.getAction() == MotionEvent.ACTION_MOVE) { // Dragging!
			float offy = last_down_screen.y - ev.getY();
			AbstractActivity.Log("Dragging (" + offy + ")");
			double dist = GeometryFuncs.distance(0, 0, 0, offy);
			if (dist > MIN_DRAG_DIST || is_dragging) {// && dist < MAX_DRAG_DIST) {
				if (can_drag) {
					this.root_cam.moveCam(0, offy);
					if (this.root_node.getHeight() > Statics.SCREEN_HEIGHT) {
						if (this.root_cam.top < this.root_node.getWorldY()) {
							this.root_cam.moveCam(0, this.root_node.getWorldY() - this.root_cam.top);
						} else if (this.root_cam.bottom > this.root_node.getWorldBounds().bottom) {
							this.root_cam.moveCam(0, this.root_node.getWorldBounds().bottom - this.root_cam.bottom);
						}
					}
				}
				is_dragging = true;
			}
			last_down_screen.y = ev.getY();
		} else if (ev.getAction() == MotionEvent.ACTION_DOWN) {
			last_down_screen.y = ev.getY();
		}	
		return false;			
	}


	private void selectOption(int idx) {
		AbstractActivity act = Statics.act;
		
		IOFunctions.Vibrate(act.getBaseContext(), Statics.VIBRATE_LEN);
		this.optionSelected(idx);

	}


	@Override
	public void updateGame(long interpol) {
		if (al_button_texts == null) {
			this.setOptions();
			// See if there's only one option.
			if (auto_select) {
				if (al_button_texts != null) {
					if (al_button_texts.size() == 1) {
						this.selectOption(0);
					}
				}
			}
		}
	}


	@Override
	public boolean onBackPressed() {
		returnTo();
		return true;
	}


	protected void addOption(String s) {
		addOption(s, ""+this.al_action_cmd_texts.size());
	}


	protected void addOption(String text, String cmd) {
		this.addOption(this.al_action_cmd_texts.size(), text, cmd);
	}


	protected void addOption(int pos, String text, String cmd) {
		this.al_button_texts.add(pos, text);
		this.al_action_cmd_texts.add(pos, cmd);
	}


	protected void addOption(String text, int cmd) {
		this.al_button_texts.add(text);
		this.al_action_cmd_texts.add(""+cmd);
	}


	public int getNumOfOptions() {
		return this.al_button_texts.size();
	}


	public boolean areThereAnyOptions() {
		return this.al_button_texts.size() > 0;
	}


	public String getButtonText(int idx) {
		return this.al_button_texts.get(idx);
	}


	public String getActionCommand(int idx) {
		return this.al_action_cmd_texts.get(idx);
	}


	public void removeOption(int idx) {
		this.al_button_texts.remove(idx);
		this.al_action_cmd_texts.remove(idx);
	}


}
