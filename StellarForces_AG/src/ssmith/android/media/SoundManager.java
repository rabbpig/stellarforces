package ssmith.android.media;

import java.util.HashMap;
import ssmith.android.framework.ErrorReporter;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;

import com.scs.stellarforces.Statics;

public class SoundManager { //extends Thread {

	private SoundPool mSoundPool;
	private HashMap<Integer, Integer> mSoundPoolMap;
	//private AudioManager mAudioManager;
	private Context mContext;
	private float streamVolume;
	//private ArrayList<Integer> sounds;

	public SoundManager(Context theContext) {//, ArrayList<Integer> _sounds) { 
		//super("SoundManager");

		mContext = theContext;
		//sounds = _sounds;

		mSoundPoolMap = new HashMap<Integer, Integer>();

		//this.createSoundPool();
		mSoundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);

		// Preload the sounds
		//start();

		//mAudioManager = (AudioManager)mContext.getSystemService(Context.AUDIO_SERVICE);
		streamVolume = 0.99f; //mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		//streamVolume = streamVolume / mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

		mSoundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
			@Override
			public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
				if (status == 0) {
					soundPool.play(sampleId, streamVolume, streamVolume, 1, 0, 1f);
					//boolean loaded = true;
				}
			}
		});

	}


	/*	private void createSoundPool() {
	}
	 */

	/*public void run() {
		for (int i : sounds) {
			this.loadSound(i);
		}

	}*/


	private int loadSound(int res_id) {
		int sound_id = -1;
		if (mSoundPoolMap.containsKey(res_id) == false) {
			if (mSoundPool != null) {
				sound_id = mSoundPool.load(this.mContext, res_id, 1);
				mSoundPoolMap.put(res_id, sound_id);
			}
		} else {
			sound_id = mSoundPoolMap.get(res_id);
		}
		return sound_id;
	}



	public int playSound(int res_id) {
		try {
			if (Statics.MUTE_SFX == 1) {
				return -1;
			}
			int sound_id = loadSound(res_id);
			if (mSoundPool != null) {
				int id = mSoundPool.play(sound_id, streamVolume, streamVolume, 1, 0, 1f);
				return id;
			} else {
				return -1;
			}
		} catch (Exception ex) {
			ErrorReporter.getInstance().handleSilentException(ex);
		}
		return -1;
	}


	public void stopSound(int stream) {
		if (stream >= 0) {
			mSoundPool.stop(stream);
		}
	}


	public void stopAllSounds() {
		try {
			this.mSoundPool.release();
			this.mSoundPool = null;
		} catch (Exception ex) {
			ErrorReporter.getInstance().handleSilentException(ex);
		}

	}


}
