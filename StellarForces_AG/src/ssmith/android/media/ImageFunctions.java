package ssmith.android.media;

import ssmith.android.util.IUpdateProgress;
import android.graphics.AvoidXfermode;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.graphics.Shader.TileMode;

public class ImageFunctions {


	public static Bitmap MakeMutable_DestroysOrig(Bitmap src, Bitmap.Config cfg) {
		Bitmap bmp_foreground = src.copy(cfg, true);
		if (bmp_foreground != src) {
		src.recycle();
		System.gc();
		}
		return bmp_foreground;

	}


	/**
	 * This will always return a bitmap of RGB_565
	 * 
	 */
	public static Bitmap ScaleBitmap_MaxSize_RGB565(Bitmap bmp, float w, float h) {
		Bitmap bmp1 = ScaleBitmap_WidthOnly_RGB565(bmp, (int)w);
		int h1 = bmp1.getWidth() * bmp1.getHeight();
		Bitmap bmp2 = ScaleBitmap_HeightOnly_RGB565(bmp, (int)h);
		int h2 = bmp2.getWidth() * bmp2.getHeight();

		if (h1 > h2) {
			return bmp2;
		} else {
			return bmp1;
		}
	}


	/**
	 * This one keeps the proportions.
	 */
	public static Bitmap ScaleBitmap_WidthOnly_RGB565(Bitmap bmp, float w) { // Can't do local
		float scale = w / bmp.getWidth();
		float h = bmp.getHeight() * scale;
		Bitmap bmp2 = Bitmap.createScaledBitmap(bmp, (int)w, (int)h, true); // Always returns RGB_565
		return bmp2;
	}


	/**
	 * This one keeps the proportions.
	 */
	public static Bitmap ScaleBitmap_HeightOnly_RGB565(Bitmap bmp, float h) { // Can't do local
		float scale = h / bmp.getHeight();
		float w = bmp.getWidth() * scale;
		Bitmap bmp2 = Bitmap.createScaledBitmap(bmp, (int)w, (int)h, true); // Always returns RGB_565
		return bmp2;
	}


	/**
	 * This will always return a bitmap of RGB_565
	 * 
	 */
	public static Bitmap ScaleBitmap_MaxSize_Local_RGB565(Bitmap bmp, float w, float h) {
		Bitmap bmp1 = ScaleBitmap_WidthOnly_Local_RGB565(bmp, (int)w);
		int h1 = bmp1.getWidth() * bmp1.getHeight();
		Bitmap bmp2 = ScaleBitmap_HeightOnly_Local_RGB565(bmp, (int)h);
		int h2 = bmp2.getWidth() * bmp2.getHeight();

		if (h1 > h2) {
			return bmp2;
		} else {
			return bmp1;
		}
	}


	/**
	 * This one keeps the proportions.
	 */
	public static Bitmap ScaleBitmap_WidthOnly_Local_RGB565(Bitmap bmp, float w) { // Can't do local
		float scale = w / bmp.getWidth();
		float h = bmp.getHeight() * scale;
		Bitmap bmp2 = Bitmap.createScaledBitmap(bmp, (int)w, (int)h, true); // Always returns RGB_565
		if (bmp2 != bmp) {
			bmp.recycle();
		}
		return bmp2;
	}


	/**
	 * This one keeps the proportions.
	 */
	public static Bitmap ScaleBitmap_HeightOnly_Local_RGB565(Bitmap bmp, float h) { // Can't do local
		float scale = h / bmp.getHeight();
		float w = bmp.getWidth() * scale;
		Bitmap bmp2 = Bitmap.createScaledBitmap(bmp, (int)w, (int)h, true); // Always returns RGB_565
		if (bmp2 != bmp) {
			bmp.recycle();
		}
		return bmp2;
	}


	public static Bitmap CombineBitmaps(Bitmap bmp_background_rgb565, Bitmap bmp_foreground_argb8888) {
		if (bmp_background_rgb565.getConfig().name().equalsIgnoreCase(Bitmap.Config.RGB_565.name()) == false) {
			throw new InvalidImageConfigException("Invalid bitmap type for bmp_background: " + bmp_background_rgb565.getConfig().name());
		}
		// Only the foreground needs to be transparent
		if (bmp_foreground_argb8888.getConfig().name().equalsIgnoreCase(Bitmap.Config.ARGB_8888.name()) == false) {
			throw new InvalidImageConfigException("Invalid bitmap type for foreground: " + bmp_foreground_argb8888.getConfig().name());
		}
		Bitmap bmOverlay = Bitmap.createBitmap(bmp_background_rgb565.getWidth(), bmp_background_rgb565.getHeight(), bmp_background_rgb565.getConfig());
		Canvas canvas = new Canvas(bmOverlay);
		canvas.drawBitmap(bmp_background_rgb565, new Matrix(), null);
		canvas.drawBitmap(bmp_foreground_argb8888, new Matrix(), null); // bmp_foreground.getPixel(0,0)
		return bmOverlay;
	}


	public static void CombineBitmaps_Local(Bitmap bmp_background, Bitmap bmp_foreground) {
		// Only the foreground needs to be transparent
		if (bmp_foreground.getConfig().name().equalsIgnoreCase(Bitmap.Config.ARGB_8888.name()) == false) {
			throw new RuntimeException("Invalid bitmap type for foreground: " + bmp_foreground.getConfig().name());
		}
		Canvas canvas = new Canvas(bmp_background); // bmp_background.getConfig()
		canvas.drawBitmap(bmp_foreground, new Matrix(), null);
		//return bmp_background;
	}


	public static Bitmap ManualPixelAdjust_DestroysOrig(Bitmap src, boolean inner, int from_rgb[], int to, int threshold, IUpdateProgress prog) {
		return ManualPixelAdjust_DestroysOrig(src, inner, 0, 0, src.getWidth(), src.getHeight(), from_rgb, to, threshold, prog);
	}


	/**
	 * 
	 * @param src
	 * @param sx
	 * @param sy
	 * @param ex
	 * @param ey
	 * @param from_rgb
	 * @param to
	 * @param threshold If < 0, does ALL pixels
	 * @param prog
	 * @return
	 */
	public static Bitmap ManualPixelAdjust_DestroysOrig(Bitmap src2, boolean inner, int _sx, int _sy, int _ex, int _ey, int from_rgb[], int to, int threshold, IUpdateProgress prog) {
		int sx = Math.min(_sx, _ex);
		int sy = Math.min(_sy, _ey);
		int ex = Math.max(_sx, _ex);
		int ey = Math.max(_sy, _ey);

		if (sx < 0) {
			sx = 0;
		}
		if (sy < 0) {
			sy = 0;
		}
		if (ex > src2.getWidth()) {
			ex = src2.getWidth();
		}
		if (ey > src2.getHeight()) {
			ey = src2.getHeight();
		}

		// Need this bit for some phones
		//src.setHasAlpha()
		int tmp[] = new int[src2.getWidth() * src2.getHeight()];
		src2.getPixels(tmp, 0, src2.getWidth(), 0, 0, src2.getWidth(), src2.getHeight());
		Bitmap src = Bitmap.createBitmap(src2.getWidth(), src2.getHeight(), Bitmap.Config.ARGB_8888);
		if (src2 != src) {
			src2.recycle();
		}
		src.setPixels(tmp, 0, src.getWidth(), 0, 0, src.getWidth(), src.getHeight());
		tmp = null;
		System.gc();

		if (inner) {
			for(int y = sy ; y < ey ; y++) {
				for(int x = sx ; x < ex ;x++) {
					int pixel = src.getPixel(x, y);
					if (pixel != to) {
						if (threshold < 0 || Match(pixel, from_rgb, threshold)) { //src.setPixel(0,0, 0)
							src.setPixel(x, y, to); // src.getPixel(3,0)
						}
					}
				}
				if (prog != null) {
					prog.updateProgress(y-sy, ey-sy);
				}
			} 
		} else {
			for(int y = 0 ; y < src.getHeight() ; y++) {
				for(int x = 0 ; x < src.getWidth() ;x++) {
					if ((x < sx || x > ex) || (y < sy || y > ey)) {
						int pixel = src.getPixel(x, y);
						if (pixel != to) {
							if (threshold < 0 || Match(pixel, from_rgb, threshold)) { 
								src.setPixel(x, y, to);
							}
						}
					}
				}
				if (prog != null) {
					prog.updateProgress(y, src.getHeight());
				}
			} 

		}

		return src;
	}


	private static boolean Match(int pixel, int FROM_COLOR[], int THRESHOLD) {
		//There may be a better way to match, but I wanted to do a comparison ignoring
		//transparency, so I couldn't just do a direct integer compare.
		/*if (pixel == 0) { // Nasty hack to change black
			return true;
		} else {*/
		return Math.abs(Color.red(pixel) - FROM_COLOR[0]) < THRESHOLD &&
				Math.abs(Color.green(pixel) - FROM_COLOR[1]) < THRESHOLD &&
				Math.abs(Color.blue(pixel) - FROM_COLOR[2]) < THRESHOLD;
		//}
	}


	public static void ChangeColours_Local(Bitmap bmp_foreground, int change_from, Paint change_to, int threshold) {
		// then, set the Xfermode of the paint to AvoidXfermode
		// removeColor is the color that will be replaced with the pain't color
		// 0 is the tolerance (in this case, only the color to be removed is targetted)
		// Mode.TARGET means pixels with color the same as removeColor are drawn on

		change_to.setXfermode(new AvoidXfermode(change_from, threshold, AvoidXfermode.Mode.TARGET));

		//change_to.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));

		// draw transparent on the "red" pixels
		Canvas c = new Canvas(bmp_foreground);
		c.drawPaint(change_to);

		// mb should now have transparent pixels where they were red before
		//return bmp_foreground;
	}


	public static Bitmap CopyBitmap_(Bitmap bmp)  {
		return Bitmap.createScaledBitmap(bmp, bmp.getWidth(), bmp.getHeight(), false);
	}


	public static void MakeSepia_Local(Bitmap src, IUpdateProgress prog) {
		//Bitmap bmp_foreground = src;
		for(int y = 0 ; y < src.getHeight() ; y++) {
			for(int x = 0 ; x < src.getWidth() ;x++) {
				int pixel = src.getPixel(x, y);
				float inputRed = Color.red(pixel);
				float inputGreen = Color.green(pixel);
				float inputBlue = Color.blue(pixel);

				double outputRed = (inputRed * .393) + (inputGreen *.769) + (inputBlue * .189);
				if (outputRed > 255) {
					outputRed = 255;
				}

				double outputGreen = (inputRed * .349) + (inputGreen *.686) + (inputBlue * .168);
				if (outputGreen > 255) {
					outputGreen = 255;
				}

				double outputBlue = (inputRed * .272) + (inputGreen *.534) + (inputBlue * .131);		
				if (outputBlue > 255) {
					outputBlue = 255;
				}

				src.setPixel(x, y, Color.argb(Color.alpha(pixel), (int)outputRed, (int)outputGreen, (int)outputBlue));

			}
			if (prog != null) {
				prog.updateProgress(y, src.getHeight());
			}
		}
		//return bmp_foreground;

	}


	public static void ChangeBrightnessAndContrast_Local(Bitmap src, float bright, float contr, IUpdateProgress prog) {
		Canvas canvas = new Canvas(src);
		Paint paint = new Paint();
		ColorMatrix cm = new ColorMatrix();

		cm.set(new float[] { 
				contr, 0, 0, 0, bright, 
				0, contr, 0, 0, bright, 
				0, 0, contr, 0, bright, 
				0, 0, 0, 1, 0 });

		paint.setColorFilter(new ColorMatrixColorFilter(cm));
		Matrix matrix = new Matrix();
		canvas.drawBitmap(src, matrix, paint);
	}


	public static void MakeBrighter_Local_OLD(Bitmap src, float diff, IUpdateProgress prog) {
		//Bitmap bmp_foreground = src;//src.copy(Bitmap.Config.ARGB_8888, true);
		for(int y = 0 ; y < src.getHeight() ; y++) {
			for(int x = 0 ; x < src.getWidth() ;x++) {
				int pixel = src.getPixel(x, y);
				float inputRed = Color.red(pixel);
				float inputGreen = Color.green(pixel);
				float inputBlue = Color.blue(pixel);

				double outputRed = inputRed * diff;
				if (outputRed > 255) {
					outputRed = 255;
				}

				double outputGreen = inputGreen * diff;
				if (outputGreen > 255) {
					outputGreen = 255;
				}

				double outputBlue = inputBlue * diff;		
				if (outputBlue > 255) {
					outputBlue = 255;
				}

				src.setPixel(x, y, Color.argb(Color.alpha(pixel), (int)outputRed, (int)outputGreen, (int)outputBlue));
			}
			if (prog != null) {
				prog.updateProgress(y, src.getHeight());
			}
		}
	}


	public static void AdjustHue_Local(Bitmap src, float diff, IUpdateProgress prog) {
		//Bitmap bmp_foreground = src;//src.copy(Bitmap.Config.ARGB_8888, true);
		for(int y = 0 ; y < src.getHeight() ; y++) {
			for(int x = 0 ; x < src.getWidth() ;x++) {
				int newPixel = hueChange(src.getPixel(x,y), diff);
				src.setPixel(x, y, newPixel);
			}
			if (prog != null) {
				prog.updateProgress(y, src.getHeight());
			}
		}
	}


	private static int hueChange(int startpixel, float deg){
		float[] hsv = new float[3];       //array to store HSV values
		Color.colorToHSV(startpixel,hsv); //get original HSV values of pixel
		hsv[0]=hsv[0]+deg;                //add the shift to the HUE of HSV array
		hsv[0]=hsv[0]%360;                //confines hue to values:[0,360]
		return Color.HSVToColor(Color.alpha(startpixel),hsv);
	}


	public static void MakeGreyscale_Local(Bitmap bmp) {
		//Bitmap bmp_new = bmp;
		Canvas c = new Canvas(bmp);
		Paint paint = new Paint();
		ColorMatrix cm = new ColorMatrix();
		cm.setSaturation(0);
		ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
		paint.setColorFilter(f);
		c.drawBitmap(bmp, 0, 0, paint);
	}


	/*public static void MakeMisc1_Local(Bitmap bmp) {
		//Bitmap bmp_new = bmp;
		Canvas c = new Canvas(bmp);
		Paint paint = new Paint();
		ColorMatrix cm = new ColorMatrix();
		cm.setSaturation(0.1f);
		ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
		paint.setColorFilter(f);
		c.drawBitmap(bmp, 0, 0, paint);
	}


	public static void MakeMisc2_Local(Bitmap bmp) {
		//Bitmap bmp_new = bmp;
		Canvas c = new Canvas(bmp);
		Paint paint = new Paint();
		ColorMatrix cm = new ColorMatrix();
		cm.setSaturation(0f);
		ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
		paint.setColorFilter(f);
		c.drawBitmap(bmp, 0, 0, paint);
	}*/


	public static Bitmap ApplyReflection_RUBBISH(Bitmap originalImage) {
		// gap space between original and reflected
		final int reflectionGap = 4;
		// get image size
		int width = originalImage.getWidth();
		int height = originalImage.getHeight();          

		// this will not scale but will flip on the Y axis
		Matrix matrix = new Matrix();
		matrix.preScale(1, -1);

		// create a Bitmap with the flip matrix applied to it.
		// we only want the bottom half of the image
		Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0, height/2, width, height/2, matrix, false);          

		// create a new bitmap with same width but taller to fit reflection
		Bitmap bitmapWithReflection = Bitmap.createBitmap(width, (height + height/2), Config.ARGB_8888);

		// create a new Canvas with the bitmap that's big enough for
		// the image plus gap plus reflection
		Canvas canvas = new Canvas(bitmapWithReflection);
		// draw in the original image
		canvas.drawBitmap(originalImage, 0, 0, null);
		// draw in the gap
		Paint defaultPaint = new Paint();
		canvas.drawRect(0, height, width, height + reflectionGap, defaultPaint);
		// draw in the reflection
		canvas.drawBitmap(reflectionImage,0, height + reflectionGap, null);

		// create a shader that is a linear gradient that covers the reflection
		Paint paint = new Paint();
		LinearGradient shader = new LinearGradient(0, originalImage.getHeight(), 0,
				bitmapWithReflection.getHeight() + reflectionGap, 0x70ffffff, 0x00ffffff,
				TileMode.CLAMP);
		// set the paint to use this shader (linear gradient)
		paint.setShader(shader);
		// set the Transfer mode to be porter duff and destination in
		paint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
		// draw a rectangle using the paint with our linear gradient
		canvas.drawRect(0, height, width, bitmapWithReflection.getHeight() + reflectionGap, paint);

		return bitmapWithReflection;
	}



	public static Bitmap Rotate(Bitmap b, int degrees, boolean destroy_orig) {
		Matrix m = new Matrix();
		m.setRotate(degrees, (float) b.getWidth() / 2, (float) b.getHeight() / 2);
		Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
		if (destroy_orig && b != b2) {
			b.recycle();
		}
		return b2;
	}


}
