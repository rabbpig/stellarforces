package ssmith.android.media;

public class InvalidImageConfigException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public InvalidImageConfigException(String s) {
		super(s);
	}

}
