package ssmith.android.lib2d.shapes;

import ssmith.android.lib2d.Camera;
import android.graphics.Bitmap;
import android.graphics.Canvas;

public class BitmapRectangle extends AbstractRectangle { // This is much the same as BitmapComponent

	public Bitmap bmp;

	public BitmapRectangle(String name, Bitmap _bmp, float x, float y) {
		super(name, null, x, y, _bmp.getWidth(), _bmp.getHeight());

		bmp = _bmp;
	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		/*if (Statics.RELEASE_MODE == false) {
			if (this.needs_updating) {
				throw new RuntimeException(this.name + " needs updating!");
			}
		}*/

		if (bmp != null) {
			if (this.visible) {
				if (bmp.isRecycled()) {
					throw new RuntimeException("Trying to draw recycled bitmap '" + name + "'");
				}
				g.drawBitmap(bmp, this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);
			}
		}
	}

}
