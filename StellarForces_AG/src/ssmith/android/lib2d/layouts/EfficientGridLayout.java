package ssmith.android.lib2d.layouts;

import java.util.ArrayList;

import ssmith.android.framework.AbstractActivity;
import ssmith.android.lib2d.Camera;
import ssmith.android.lib2d.Node;
import ssmith.android.lib2d.shapes.AbstractRectangle;
import ssmith.android.lib2d.shapes.Geometry;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Paint.Style;

import com.scs.stellarforces.Statics;

public class EfficientGridLayout extends Node {

	private static Paint default_paint = new Paint();

	static {
		default_paint.setARGB(255, 255, 255, 255);
		default_paint.setAntiAlias(true);
		default_paint.setStyle(Style.STROKE);
	}

	protected AbstractRectangle blocks[][];
	private float tile_size;
	private int draw_width;
	private int draw_height;
	private int blocks_width, blocks_height;

	public EfficientGridLayout(int w, int h, float _tile_size) {
		super("EfficientGridLayout");

		blocks_width = w;
		blocks_height = h;
		blocks = new AbstractRectangle[w][h];
		tile_size = _tile_size;//(int)Math.floor(_tile_size);

		local_bounds = new RectF(0, 0, blocks_width * tile_size, blocks_height * tile_size);
	}


	/**
	 * Only GameModule should call this as we'll need to check shadows etc...
	 * @param b
	 * @param x
	 * @param y
	 */
	public void setRectAtMap(AbstractRectangle b, int x, int y) {
		blocks[x][y] = b;
		if (b != null) {
			b.setByLTRB(x*tile_size, y*tile_size, ((x+1)*tile_size)-1, ((y+1)*tile_size)-1);
			b.updateGeometricState();
		}
	}


	public void removeRectAtMap(int x, int y) {
		blocks[x][y] = null;
	}


	public AbstractRectangle getRectAtPixel(float x, float y) {
		int x2 = (int)(x / Statics.SQ_SIZE);
		int y2 = (int)(y / Statics.SQ_SIZE);
		if (x2 >=  0 && y2 >= 0 && x2 < blocks.length && y2 < blocks[0].length) {
			return blocks[x2][y2];
		} else {
			return null;
		}
	}


	public AbstractRectangle getBlockAtMap_MaybeNull(int x, int y) {
		try {
			return blocks[x][y];
		} catch (ArrayIndexOutOfBoundsException ex) {
			// Do nothing
		}
		return null;
	}


	@Override
	protected void getCollidersAt(float x, float y, ArrayList<Geometry> list_of_colliders) {
		ArrayList<Geometry> colls = this.getCollidersAt(x, y);
		list_of_colliders.addAll(colls);
	}


	public ArrayList<Geometry> getCollidersAt(float x, float y) {
		return getColliders(new RectF(x, y, x+1, y+1));
	}


	public ArrayList<Geometry> getColliders(RectF rect) {
		ArrayList<Geometry> colls = new ArrayList<Geometry>();
		//colls.clear();

		draw_width = (int)((rect.right - rect.left) / tile_size);
		draw_height = (int)((rect.bottom - rect.top) / tile_size);

		int s_x = (int)(rect.left / tile_size);
		int s_y = (int)(rect.top / tile_size);

		for (int y=s_y ; y<=s_y + draw_height+1 ; y++) {
			if (y < blocks[0].length) {
				for (int x=s_x ; x<=s_x + draw_width+1 ; x++) {
					if (x >= 0 && y >= 0 && x < blocks.length) {
						if (blocks[x][y] != null) {
							AbstractRectangle block = blocks[x][y];
							if (RectF.intersects(rect, block.getWorldBounds())) {
								colls.add(block);
							}
						}
					}
				}
			}
		}
		return colls;
	}


	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		draw_width = (int)((cam.right - cam.left) / tile_size);
		draw_height = (int)((cam.bottom - cam.top) / tile_size);

		int s_x = (int)(cam.left / tile_size);
		int s_y = (int)(cam.top / tile_size);

		//objects_being_drawn = 0;

		for (int y=s_y ; y<=s_y + draw_height+1 ; y++) {
			if (y >= 0 && y < blocks[0].length) {
				for (int x=s_x ; x<=s_x + draw_width+1 ; x++) {
					if (x >= 0 && x < blocks.length) {
						try {
							if (blocks[x][y] != null) {
								AbstractRectangle block = blocks[x][y];
								block.doDraw(g, cam, interpol);
							}
						} catch (ArrayIndexOutOfBoundsException ex) {
							AbstractActivity.HandleError(ex);
						}
					}
				}
			}
		}

	}


	public int getBlocksWidth() {
		return this.blocks_width;
	}


	public int getBlocksHeight() {
		return this.blocks_height;
	}


	@Override
	public void updateGeometricState() {
		// Note that we DONT change our local bounds.
		// Now update our world bounds
		this.world_bounds.left = local_bounds.left + this.parent_world_coords.x;// + this.local_coords.x;
		this.world_bounds.top = local_bounds.top + this.parent_world_coords.y;// + this.local_coords.y;
		this.world_bounds.right = local_bounds.right + this.parent_world_coords.x;// + this.local_coords.x;
		this.world_bounds.bottom = local_bounds.bottom + this.parent_world_coords.y;// + this.local_coords.y;

		this.needs_updating = false;
	}

}
