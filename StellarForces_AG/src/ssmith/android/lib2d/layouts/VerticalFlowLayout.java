package ssmith.android.lib2d.layouts;

import ssmith.android.lib2d.Node;
import ssmith.android.lib2d.Spatial;

public class VerticalFlowLayout extends Node {

	private float spacing;
	private float curr_y_pos = 0;
	

	public VerticalFlowLayout(String name, float space) {
		super(name);
		spacing = space;

		curr_y_pos = space;
	}


	public void attachChild(Spatial child) {
		super.attachChild(child);

		child.updateGeometricState();  // In case not done, so we can get width/height if it's a node

		float left = 0;//spacing;
		float top = curr_y_pos;
		//float right = child.getWidth();
		//float bottom = curr_y_pos + child.getHeight();
		//child.setByLTRB(left, top, right, bottom);
		child.setLocation(left, top); // SCS
		this.updateGeometricState(); // Recalc sizes ready for next item
		curr_y_pos += child.getHeight() + spacing;

	}


}
