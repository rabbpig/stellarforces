package ssmith.android.lib2d;

import ssmith.lang.GeometryFuncs;
import ssmith.lang.NumberFunctions;
import android.graphics.PointF;
import android.graphics.RectF;

import com.scs.stellarforces.Statics;

public class Camera extends RectF {

	public int zoom = 1;
	private PointF target_point = new PointF(); // Where we're aiming at
	private boolean lock_to_target; // if false, camera slides to the target
	private boolean moving = false; // If not locked, are we actually moving?
	private MyPointF actual_point = new MyPointF(); // What we're currently looking at
	private float last_dist = -1;

	private boolean vibrating = false;
	private long vibrate_until;

	public Camera() {
	}


	public MyPointF getActualCentre() {
		return this.actual_point;
	}


	public void moveCam(float offx, float offy) {
		this.moveCam(offx, offy, true);
	}
	
	
	public void moveCam(float offx, float offy, boolean lock) {
		/*this.actual_point.x += offx;
		this.actual_point.y += offy;*/
		lock_to_target = lock;
		if (lock) {
			actual_point.x += offx;
			actual_point.y += offy;
			moving = false;
			this.updateWindow();
		} else {
			target_point.x += offx;
			target_point.y += offy;
			moving = true;
			last_dist = -1;
		}
	}


	public void lookAt(float x, float y, boolean lock) {
		lock_to_target = lock;
		if (lock) {
			actual_point.x = x;
			actual_point.y = y;
			moving = false;
			this.updateWindow();
		} else {
			target_point.x = x;
			target_point.y = y;
			moving = true;
			last_dist = -1;
		}
	}


	public void lookAt(Spatial s, boolean lock) {
		if (Statics.RELEASE_MODE == false) {
			if (s.needs_updating) {
				throw new RuntimeException("Spatial " + s + " needs updating!");
			}
		} else {
			s.updateGeometricState();
		}
		this.lookAt(s.getWorldCentreX(), s.getWorldCentreY(), lock);
	}


	public void update(long interpol) {
		/*if (this.right - this.left <= 0) {
			//throw new RuntimeException("Camera not looking at anything!");
		}*/
		if (vibrating) {
			if (System.currentTimeMillis() > this.vibrate_until) {
				this.vibrating = false;
			} else {
				float diff = Statics.SCREEN_WIDTH / 100f;
				this.moveCam(NumberFunctions.rndFloat(-diff, diff), NumberFunctions.rndFloat(-diff, diff));
			}

		} else {
			if (lock_to_target == false) {
				if (moving) {
					float dist = (float)GeometryFuncs.distance(actual_point.x, actual_point.y, target_point.x, target_point.y);
					if ((dist > last_dist && this.last_dist >= 0) || dist <= 1) {
						lookAt(target_point.x, target_point.y, true);
					} else {
						float off_x = (target_point.x - actual_point.x) / 4;
						float off_y = (target_point.y - actual_point.y) / 4;
						actual_point.x += off_x;
						actual_point.y += off_y;

						this.updateWindow();
					}
					this.last_dist = dist;
				}
			}
		}
	}
	
	
	public boolean isMoving() {
		return lock_to_target == false;// && moving == false; 
	}


	private void updateWindow() {
		this.left = actual_point.x - ((Statics.SCREEN_WIDTH/2) / this.zoom);
		this.top = actual_point.y - ((Statics.SCREEN_HEIGHT/2) / this.zoom);
		this.right = actual_point.x + ((Statics.SCREEN_WIDTH/2) / this.zoom);
		this.bottom = actual_point.y + ((Statics.SCREEN_HEIGHT/2) / this.zoom);
	}


	public void vibrate(long time) {
		this.vibrating = true;
		this.vibrate_until = System.currentTimeMillis() + time;
	}

}
