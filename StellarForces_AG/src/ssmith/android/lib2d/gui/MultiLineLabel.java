package ssmith.android.lib2d.gui;

import java.util.ArrayList;

import ssmith.android.lib2d.Node;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;

public class MultiLineLabel extends Node {

	private Paint paint, ink;
	private boolean word_wrap;
	private float wrap_width;
	protected StringBuffer str;

	public MultiLineLabel(String name, String text, Paint _paint, Paint _ink, boolean _word_wrap, float _wrap_width) {
		super(name);

		paint = _paint;
		ink = _ink;
		word_wrap = _word_wrap;
		wrap_width = _wrap_width;

		str = new StringBuffer(text);
		this.showText();
	}


	protected void showText() {
		this.detachAllChildren();
		String text = str.toString().trim();
		if (word_wrap) {
			ArrayList<String> al_lines = new ArrayList<String>();
			while (true) {
				int chars_to_show = ink.breakText(text, true, wrap_width, null);
				int cr_pos = text.indexOf("\n"); // Is there a CR in the text we've got?
				if (cr_pos >= 0 && cr_pos < chars_to_show) {
					chars_to_show = cr_pos;
				}
				if (chars_to_show >= text.length()) { // Are we trying to show more text than is remaining?
					if (text.length() > 0) {
						al_lines.add(text.substring(0, text.length())); // WAS: chars));
					}
					break;
				} else {
					// Did we end on a CR?
					if (text.substring(chars_to_show, chars_to_show+1).equalsIgnoreCase("\n") == false) {
						// Go back to the prev space
						int space_pos = text.substring(0, chars_to_show).lastIndexOf(" ");
						if (space_pos >= 0) {
							chars_to_show = space_pos;
						}
					}

					al_lines.add(text.substring(0, chars_to_show));
					text = text.substring(chars_to_show+1);
				}
			}
			showLines(al_lines);
		} else {
			String lines[] = text.split("\\n");
			ArrayList<String> al_lines = new ArrayList<String>();
			for (int i=0 ; i<lines.length ; i++) {
				al_lines.add(lines[i]);
			}
			showLines(al_lines);
		}
	}


	private void showLines(ArrayList<String> lines) {
		this.removeAllChildren();
		int y = 0;
		for (String s : lines) {
			Label l = new Label(this.name + "_sublabel", s, 0, y, this.wrap_width, this.paint, ink, false);
			this.attachChild(l);
			l.updateGeometricState(); // ?
			y += (ink.getTextSize() * Statics.LABEL_SPACING);
		}
		this.updateGeometricState();

	}


	public void appendText(String s) {
		str.append(s);
		this.showText();
	}


	public void setText(String s) {
		this.str = new StringBuffer(s);
		this.showText();
	}
	
	
	public String getText() {
		return str.toString();
	}
	
	
	public void delete(int start, int end) {
		str.delete(start, end);
		this.showText();
	}
	
	
}
