package ssmith.android.lib2d.gui;

import ssmith.android.lib2d.Camera;
import android.graphics.Bitmap;
import android.graphics.Canvas;

public class BitmapComponent extends AbstractComponent {
	
	private Bitmap bmp;
	
	/**
	 * This is used when using layouts, as they layout controls the size and location.
	 */
	public BitmapComponent(String name, String cmd, Bitmap _bmp) {
		this(name, cmd, _bmp, 0, 0);
	}
	
	
	public BitmapComponent(String name, String cmd, Bitmap _bmp, float x, float y) {
		super(name, cmd, x, y, _bmp.getWidth(), _bmp.getHeight(), null, _bmp);
		
		bmp = _bmp;
	}

	
	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		if (this.visible) {
			g.drawBitmap(bmp, this.world_bounds.left - cam.left, this.world_bounds.top - cam.top, paint);
		}
	}


}
