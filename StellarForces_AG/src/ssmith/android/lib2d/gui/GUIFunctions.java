package ssmith.android.lib2d.gui;

import android.graphics.Paint;
import android.graphics.Rect;


public class GUIFunctions {

	private static Paint dummy_paint = new Paint();

	static {
		dummy_paint.setARGB(255, 0, 0, 0);
		dummy_paint.setAntiAlias(true);
	}
	
	
	private GUIFunctions() {
		
	}
	
	
	/*public static float GetTextSizeToFit(String text, float max_width) {
		int size = 200;// Max size
		max_width = max_width * 0.9f; // Fit inside frame
		while (size > 2) {
			dummy_paint.setTextSize(size);
			if (dummy_paint.measureText(text) < max_width) {
				break;  //return size;
			}
			size--;
		}
		return size;
	}*/
	
	
	public static float GetTextSizeToFit(String text, float max_width, float max_height) {
		int size = 200;// Max size
		max_width = max_width * 0.9f; // Fit inside frame
		max_height = max_height * 0.8f; // Fit inside frame
		Rect bounds = new Rect();
		while (size > 2) {
			dummy_paint.setTextSize(size);
			dummy_paint.getTextBounds(text, 0, text.length(), bounds);
			if (bounds.right-bounds.left < max_width && (bounds.bottom-bounds.top < max_height || max_height <= 0)) {
				break;  //return size;
			}
			size--;
		}
		return size;
	}

}
