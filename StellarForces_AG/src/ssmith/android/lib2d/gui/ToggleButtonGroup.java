package ssmith.android.lib2d.gui;

import java.util.ArrayList;

public class ToggleButtonGroup extends ArrayList<ToggleButton> {

	public void buttonClicked(ToggleButton btn) {
		for (ToggleButton other : this) {
			other.setSelected(false, false);
		}
		btn.setSelected(true, false);
	}


	public ToggleButton getSelected() {
		for (ToggleButton other : this) {
			if (other.isSelected()) {
				return other;
			}
		}
		return null;
	}
}
