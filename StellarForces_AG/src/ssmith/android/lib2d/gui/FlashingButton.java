package ssmith.android.lib2d.gui;

import ssmith.android.lib2d.Camera;
import ssmith.android.util.Timer;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class FlashingButton extends Button {

	public Bitmap bmp1, bmp2;
	private Timer timer;

	public FlashingButton(String _text, Paint paint, Paint ink, Bitmap bmp, Bitmap _bmp2, int interval) {
		super(_text, paint, ink, bmp);

		bmp1 = bmp;
		this.setupFlashing(_bmp2, interval);
		//bmp2 = _bmp2;
		//timer = new Timer(interval);
	}


	public void setupFlashing(Bitmap _bmp2, int interval) {
		bmp2 = _bmp2;
		timer = new Timer(interval);
		
	}
	
	
	public void stopFlashing() {
		this.bmp_background = bmp1; 
		bmp2 = null;
		timer = null;
	}
	
	
	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		super.doDraw(g, cam, interpol);
		if (bmp2 != null && timer != null) {
			if (timer.hasHit()) {
				if (super.bmp_background == bmp1) {
					super.bmp_background = bmp2;
				} else {
					super.bmp_background = bmp1;
				}
			}
		}
	}


}
