package ssmith.android.lib2d.gui;

import com.scs.stellarforces.Statics;

import android.graphics.Paint;

public class Label extends AbstractTextComponent {

	
	public Label(String name, String text, Paint paint, Paint ink) {
		this(name, text, 0, 0, paint, ink, true);
	}

	
	public Label(String name, String text, Paint paint, Paint ink, boolean centre) {
		this(name, text, 0, 0, paint, ink, centre);
	}

	
	public Label(String name, String text, float x, float y, Paint paint, Paint ink, boolean centre) {
		this(name, text, x, y, ink.measureText(text)*1.05f, paint, ink, centre);
		
	}


	public Label(String name, String text, float x, float y, float w, Paint paint, Paint ink, boolean centre) {
		super(name, "", text, x, y, w, ink.getTextSize() * Statics.LABEL_SPACING, paint, ink, null, centre);
		
		this.collides = false;
	}

}
