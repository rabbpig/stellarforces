package ssmith.android.lib2d.gui;

import ssmith.android.lib2d.Camera;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.scs.stellarforces.Statics;
import com.scs.stellarforces.main.lite.R;

public class CheckBox extends Button {
	
	private boolean ticked;
	private Bitmap tick_bmp;

	public CheckBox(String text, Paint paint, Paint ink, Bitmap bmp, boolean _ticked) {
		super(text, paint, ink, bmp);
		
		this.setChecked(_ticked);
		
	}
	
	
	public void toggle() {
		this.setChecked(!ticked);
	}
	
	
	public void setChecked(boolean b) {
		ticked = b;
		if (ticked) {
			tick_bmp = Statics.img_cache.getImage(R.drawable.tick, this.bmp_background.getHeight(), this.bmp_background.getHeight());
		} else {
			tick_bmp = Statics.img_cache.getImage(R.drawable.cross, this.bmp_background.getHeight(), this.bmp_background.getHeight());
		}

	}

	
	@Override
	public void doDraw(Canvas g, Camera cam, long interpol) {
		super.doDraw(g, cam, interpol);
		this.drawTick(g, cam);
	}


	protected void drawTick(Canvas g, Camera cam) {
		if (this.visible) {
			g.drawBitmap(tick_bmp, super.getScreenX(cam) + this.local_rect.width() - (this.ink.getTextSize()*3), super.getScreenY(cam), ink);
		}

	}
	
	
	public boolean isChecked() {
		return this.ticked;
	}

}

