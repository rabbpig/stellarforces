package ssmith.android.lib2d.gui;

import android.graphics.Bitmap;
import android.graphics.Paint;

public class Button extends AbstractTextComponent {
	
	/**
	 * Use this one when using a layout
	 * @param cmd
	 * @param _text
	 * @param paint
	 */
	public Button(String _text, Paint paint, Paint ink, Bitmap bmp) {
		this(_text + "_Btn", _text, _text, 0, 0, paint, ink, bmp);
	}
	
	
	/**
	 * Use this one when using a layout
	 * @param cmd
	 * @param _text
	 * @param paint
	 */
	public Button(String cmd, String _text, Paint paint, Paint ink, Bitmap bmp) {
		this(cmd + "_Btn", cmd, _text, 0, 0, paint, ink, bmp);
	}

	
	public Button(String cmd, String _text, float x, float y, Paint paint, Paint ink, Bitmap bmp) {
		this(cmd + "_Btn", cmd, _text, x, y, paint, ink, bmp);
	}
	
	
	public Button(String name, String cmd, String _text, float x, float y, Paint paint, Paint ink, Bitmap bmp) {
		super(name, cmd, _text, x, y, bmp.getWidth(), bmp.getHeight(), paint, ink, bmp, true);
	}

	
	public Button(String name, String cmd, String _text, float x, float y, float w, float h, Paint paint, Paint ink, Bitmap bmp) {
		super(name, cmd, _text, x, y, w, h, paint, ink, bmp, true);
	}
	
	

}
