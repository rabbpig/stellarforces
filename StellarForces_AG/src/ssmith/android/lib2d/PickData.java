package ssmith.android.lib2d;

import com.scs.stellarforces.graphics.GameObject;

public class PickData {
	
	public GameObject game_obj;
	public float dist;
	
	public PickData(GameObject _o, float _dist) {
		super();
		
		game_obj = _o;
		dist = _dist;
	}
	
	
	public float getDistance() {
		return dist;
	}
	
	
	public String toString() {
		return game_obj.toString() + " (" + dist + ")";
	}

}
