package ssmith.android.lib2d;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.scs.stellarforces.graphics.GameObject;

public class MyPickResults extends ArrayList<PickData> implements Comparator<PickData> {
	
	private static final long serialVersionUID = 1L;
	
	private GameObject ignore;
	private GameObject looking_for; // Need this in case the object we're looking for doesn't block view (i.e. can't normally be seen)
	private boolean sorted = false;
	
	public MyPickResults(GameObject _ignore, GameObject _looking_for) {
		super();
		
		ignore = _ignore;
		looking_for = _looking_for;
	}
	
	
	public boolean add(PickData p) {
		if (p.game_obj != ignore && (p.game_obj.blocks_view || p.game_obj == looking_for)) {
			sorted = false;
			return super.add(p);
		}
		return false;
	}
	
	
	public GameObject getGameObject(int i) {
		if (sorted == false) {
			throw new RuntimeException("List not sorted!");
		}
		PickData p = super.get(i);
		return p.game_obj;
	}
	
	
	public int getNumber() {
		return this.size();
	}
	
	
	public PickData getPickData(int i) {
		return super.get(i);
	}
	
	
	public void sort() {
		sorted = true;
		Collections.sort(this, this);
	}


	@Override
	public int compare(PickData p1, PickData p2) {
		if (p1.dist < p2.dist) {
			return -1;
		} else if (p1.dist > p2.dist) {
			return 1;
		} else {
			return 0;
		}
	}

}
