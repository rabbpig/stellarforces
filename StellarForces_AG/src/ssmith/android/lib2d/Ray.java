package ssmith.android.lib2d;

import ssmith.android.lib2d.shapes.Line;
import ssmith.lang.GeometryFuncs;

public class Ray {
	
	public MyPointF origin;
	public MyPointF direction;
	
	public Ray() {
		this(new MyPointF(), new MyPointF());
	}
	
	
	public Ray(Ray r) {
		this(r.origin, r.direction);
	}
	
	
	public Ray(Line l) {
		this(l.getWorldStart(), l.getWorldEnd().subtract(l.getWorldStart()).normalizeLocal());
	}
	
	
	public Ray(MyPointF _origin, MyPointF _direction) {
		super();
		
		origin = _origin;
		this.setDirection(_direction);
	}
	
	
	public MyPointF getDirection() {
		return this.direction;
	}
	
	
	public MyPointF getOrigin() {
		return this.origin;
	}
	
	
	public float getAngle() {
		return GeometryFuncs.GetAngleFromDirection(direction.x, direction.y);
	}
	
	
	public void setDirection(MyPointF dir) {
		this.direction = dir.normalize();
	}
	
	
	public String toString() {
		return "o: " + this.origin.toString() + " / dir: " + this.direction.toString();
	}

}
