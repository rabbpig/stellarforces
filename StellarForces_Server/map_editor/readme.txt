STELLAR FORCES MAP EDITOR
-----------------------------

There are some example maps in this directory.  Get the latest map files from http://www.stellarforces.com/serverdata/new_maps

Edit these and then email them to Stephen Smith at admin@stellarforces.com for inclusion in Stellar Forces.

If you have any questions or problems, just email Stephen Smith at admin@stellarforces.com.

