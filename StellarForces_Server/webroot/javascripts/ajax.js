// JavaScript Document
function ajax(url, vars, callbackFunction) {
	var request = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("MSXML2.XMLHTTP.3.0");
 
//alert(url);
	request.open("POST", url, true);
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
 
        request.onreadystatechange = function()  {
//alert(request.readyState + ' ' + request.status);
                 if (request.readyState == 4 && request.status == 200) {
                         if (request.responseText){
							//alert(request.responseText);
							callbackFunction(request.responseText);
							//return request.responseText;
                        }
                }
        }
        request.send(vars);
}

