function hideshow(elem, height) {
	if (document.getElementById(elem).style.visibility == 'hidden') {
		document.getElementById(elem).style.visibility = 'visible';
		document.getElementById(elem).style.height = height + 'px';
	} else {
		document.getElementById(elem).style.visibility = 'hidden';
		document.getElementById(elem).style.height = '0px';
	}
}


