The escape shuttle of the Ambassador has crashed on the remote moon of Evdimou.  He is of vital importance to the peace talks and must be rescued before he can be assassinated!


Side 1:
The Laser Squad win by either killing all the Marsec Corp or getting the Ambassador to safety.


Side 2:
The Marsec Corp win by killing the Ambassador.  He is known to be in the centre of the map somewhere in the wreckage of his shuttle.


