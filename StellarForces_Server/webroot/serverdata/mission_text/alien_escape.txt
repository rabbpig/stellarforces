A hastily assembled squad is sent to investigate a distress call from the colony base on LV-427. When they arrive the base looks deserted and a recon squad is warily sent to confirm the cause. Suddenly an Alien force ambushes them and they must fight their way back to the extraction point!

<i>Mission created by Deadlime.</i>


Side 1: (Laser Squad)

At least one of the squad must make it to one of the escape elevators.

Side 2: (The Aliens)

The Aliens must kill all of the Laser Squad or prevent them from escaping before all of the turns expire.  <b>The aliens will also impregnate any humans they kill which will produce a new alien on the following turn.</b>

