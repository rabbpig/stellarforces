<img src="/images/sterner_regnix.jpg" vspace=10 hspace=10 align="right" />

After a deal with devil has gone wrong, the Angels of Death have been called upon to extract payment from Sterner Regnix.  They have located him on the distant planet of Artic.


Side 1:
The Angels win by killing Sterner Regnix before the time runs out.  <b>The Angels are indestructible and deadly in combat, but they cannot do anything if they can be seen by anyone on the opposite side, apart from close-combat opportunity attack.</b>


Side 2:
Sterner Regnix must get from the left hand side to the far right of the map to escape and win.


