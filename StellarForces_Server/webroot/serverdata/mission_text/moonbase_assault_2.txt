The moonbase on Arid-6 holds security information on the infamous 'Rebelstar.' A Data Backup system in the moonbase sub-station holds the key data. Laser Squad and Marsec Corp forces occupy the sub-station in a tense stand-off and need to destroy the computers controlled by their opponent in order to be able to transmit what data they have.

Side 1:
The Laser Squad will receive 8 VP's for each computer that is destroyed.  

Side 2:
The Marsec Corporation will receive 8 VP's for each computer that is destroyed.  


<img src="/images/mapimages/moonbase_assault2.jpg" vspace=10 hspace=10 />
