A riot at the Proxima Centauri Space Port Command Centre has dragged in several warring gangs keen to settle old scores. However the Space Port has become structurally unsound and is now disintegrating around the remaining teams. The only chance of survival is to evacuate to an escape ship via the teleport pads. 

The winner is the first player to escape 3 units.  Each side's teleport pad is located at the opposite corner of the map to where they start.

