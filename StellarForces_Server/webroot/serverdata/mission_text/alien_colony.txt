<img src="/images/alien-egg.jpg" align="right" alt="Alien Egg" vspace=10 hspace=10 />

Alien Colony is very similar to the existing Colonization missions, except two sides are aliens!  The instructions for the humans can be found in the Colonizations mission descriptions.  The rules for the aliens are as follows:-

* The alien queen can build eggs if they have enough resource points.  They will hatch after 1 or 2 turns and then disappear.  This is the only "structure" that the aliens can build.
* If the queen is killed, a random other alien will turn into the queen with 2 turns.  However, will only have the same stats as a normal alien, although they will then be able to lay eggs.
* The aliens get resource points for absorbing corpses.

The winner is the last colony with any units remaining.

