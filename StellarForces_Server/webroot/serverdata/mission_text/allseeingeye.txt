An enemy psychic has been stranded on the remote moon of Metaxa.  He is trying to escape by getting to the extraction location on the other side of the map!

Notes
The single unit on side 1 can see the location of all the units on side 1 <b>at all times</b>.

Side 1:
You will win if you get to the extraction location on the other side of the map, or you kill all of side 2.

Side 2:
You will win if you kill the psychic.

