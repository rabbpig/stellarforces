package dsrcreateclient;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import ssmith.io.EasyIn;
import ssmith.io.IOFunctions;
import ssmith.io.TextFile;
import dsr.SharedStatics;
import dsrwebserver.DSRWebServer;

public class CreateClient {

	// Use VERSION rather than MAX_UPGRADE_VERSION so we can create the new client before people start trying to download it
	private static final String UPDATE_ROOT = "./webroot/client_update_" + SharedStatics.VERSION + "/"; 
	private static final String LAST_FILE = "bin/dsr/AppletMain.class"; 

	public CreateClient() {
		try {
			float ver = SharedStatics.VERSION;
			String dir = "./webroot/client_update_" + ver;

			/*if (AppletMain.VERSION < AppletMain.MAX_UPGRADE_VERSION) {
				p("The applet version does not equal the upgrade-to version!");
				return;
			}*/
			if (SharedStatics.DEBUG) {
				p("The client is set to debug!");
				return;
			}
			if (SharedStatics.URL_FOR_CLIENT.equalsIgnoreCase(SharedStatics.WEBSITE_LINK) == false) {
				p("The applet is not looking at the right URL for the server!");
				return;
			}

			p("Press enter to create the client in directory " + dir);
			EasyIn.readString();

			File fdir = new File(dir);
			if (fdir.isDirectory() == false) {
				fdir.mkdir();
			} else {
				p("Directory " + dir + " already exists.  Please delete it.");
				return;
			}

			// Copy dirs
			IOFunctions.CopyDirectory(new File("bin/dsr"), new File(UPDATE_ROOT + "bin/dsr"), true);
			IOFunctions.CopyDirectory(new File("bin/dsrwebserver"), new File(UPDATE_ROOT + "bin/dsrwebserver"), true);
			IOFunctions.CopyDirectory(new File("bin/ssmith"), new File(UPDATE_ROOT + "bin/ssmith"), true);
			IOFunctions.CopyDirectory(new File("bin/dsrmapeditor"), new File(UPDATE_ROOT + "bin/dsrmapeditor"), true);
			IOFunctions.CopyDirectory(new File("data"), new File(UPDATE_ROOT + "data"), true);
			IOFunctions.CopyDirectory(new File("libs"), new File(UPDATE_ROOT + "libs"), true);
			IOFunctions.CopyDirectory(new File("map_editor"), new File(UPDATE_ROOT + "map_editor"), true);

			createUpdateFilesList();

			p("Client " + ver + " created.");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public void createUpdateFilesList() {
		try {
			String filename = UPDATE_ROOT + "filelist.txt";
			p("CreateUpdateFilesList started...");
			StringBuffer filelist = new StringBuffer();
			
			readDir(filelist, new File(UPDATE_ROOT));
			appendFile(filelist, UPDATE_ROOT + LAST_FILE); // Notice we do AppletMain last so the version num only increases if successful.

			if (filelist.length() > 0) { // Have we created the upgrade yet?
				TextFile.QuickWrite(filename, filelist.toString(), true);
			} else {
				p("WARNING: No client upgrade files found in " + UPDATE_ROOT);
			}
			p("CreateUpdateFilesList finished.");
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	private void readDir(StringBuffer filelist, File dir) throws NoSuchAlgorithmException, IOException {
		p("Getting MD5 for " + dir.toString());
		if (dir.exists() && dir.canRead()) {
			if (dir.getName().startsWith(".") == false) { // Ignore .svn etc..
				if (dir.isDirectory()) {
					File files[] = dir.listFiles();
					for (int f=0 ; f<files.length ; f++) {
						readDir(filelist, files[f]);
					}
				} else { // Not a dir, is a file
					if (dir.getPath().replaceAll("\\\\", "/").endsWith(LAST_FILE) == false) {
						appendFile(filelist, dir.getPath());
					}
				}
			}
		}
	}


	private void appendFile(StringBuffer filelist, String file) throws NoSuchAlgorithmException, IOException {
		try {
			String md5 = IOFunctions.GetMD5(file);
			String trunc_path = file.replaceAll("\\\\", "/").substring(UPDATE_ROOT.length());
			filelist.append(trunc_path + "|" + md5 + "\n");
		} catch (java.io.FileNotFoundException ex) {
			ex.printStackTrace();
		}
	}


	public static void p(Object o) {
		System.out.println(o);
	}


	public static void main(String args[]) {
		new CreateClient();
	}

}
