package dsrwebserver;

import java.io.File;

import ssmith.lang.Dates;
import ssmith.lang.Functions;
import dsrwebserver.tables.VisibleEnemiesTable;

public final class StayConnected extends Thread {

	public StayConnected() {
		super("StayConnected");
		
		this.setDaemon(true);
		this.setPriority(Thread.MIN_PRIORITY+1);
		start();
	}

	public void run() {
		try {
			while (true) {
				VisibleEnemiesTable.DeleteOldRecs(DSRWebServer.dbs);

				try{
					// Check disk space
					long total = new File("/").getTotalSpace();
					long space = new File("/").getFreeSpace();

					if ((float)space / (float)total < 0.1f) {
						DSRWebServer.SendEmailToAdmin("Low disk space", "There is not much disk space left!");
					}
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex, true);
				}

				/*File f = new File(DSRWebServer.STOP_FILE); 
				if (f.exists()) {
					DSRWebServer.p("Server stopping...");
					f.delete();
					DSRWebServer.stopServer();
					return;
				}*/
				Functions.delay(Dates.HOUR * 5);
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
			new StayConnected().start();
		}
	}

}
