package dsrwebserver;

import java.io.File;

import ssmith.io.IOFunctions;
import ssmith.util.StringParser;

public class UploadFileData {
	
	private String orig_filename, disposition, type, name;
	private File new_file;
	
	public String getDisposition() {
		return this.disposition;
	}
	
	
	public boolean hasFilename() {
		return this.orig_filename != null;
	}
	
	
	public String getName() {
		return this.name;
	}
	
	
	public String getFileExtention() {
		return IOFunctions.GetFilenameExtension(orig_filename);
	}
	
	
	public void setDisposition(String s) {
		this.disposition = s;
		
		// extract filename and form name
		StringParser sp = new StringParser(s);
		sp.runTo(";");
		while (sp.hasMore()) {
			String key = sp.runTo("=");
			String val = sp.runTo(";").replaceAll("\"", "");
			if (key.equalsIgnoreCase("name")) {
				this.name = val;
			} else if (key.equalsIgnoreCase("filename")) {
				this.orig_filename = val;
			}
		}
	}

	
	public void setType(String s) {
		this.type = s;
	}
	
	
	public void setNewFilename(String s) {
		new_file = new File(s);
	}


	public String getNewFilenameAndPath() {
		return new_file.getAbsolutePath();
	}
	
	
	public String getType() {
		return type;
	}
	
	
	public String getNewFilenameOnly() {
		return new_file.getName();
	}
	
	
	public String getOriginalFilename() {
		return this.orig_filename;
	}
	
	
}
