package dsrwebserver.pages;

import ssmith.html.HTMLFunctions;
import dsrwebserver.DSRWebServer;

public class fb extends AbstractHTMLPage {
	
	public fb() {
		super();
	}

	
	@Override
	public void process() throws Exception {
		this.setTitle("Home");

		StringBuffer str = new StringBuffer();
		
		HTMLFunctions.Heading(str, 1, "Welcome to " + DSRWebServer.TITLE);
		
		this.body_html.append(str);

	}
	
}
