package dsrwebserver.pages;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import ssmith.lang.Functions;
import dsrwebserver.ClientConnection;

public final class DirectoryListingPage extends AbstractHTMLPage implements Comparator<File> {

	private String str_url;

	public DirectoryListingPage() {
		super();
	}

	public void process() throws IOException, SQLException {
		str_url = this.headers.getGetValueAsString("url");
		this.setTitle(str_url);
		String full_path = "webroot" + str_url;
		File file = new File(full_path);
		String redir_to_web_page = "";

		// Check if we can list the directory
		if (file.isDirectory()) {
			boolean allow_listing = true;
			File files[] = file.listFiles();
			Arrays.sort(files, this);
			// Check if a directory listing is allowed.
			for (int i=0 ; i<files.length ; i++) {
				if (files[i].getName().equalsIgnoreCase(".denylistdir")) {
					allow_listing = false;
					break;
					/*} else if (files[i].getName().equalsIgnoreCase("index.html")) {
					redir_to_web_page = url + "/index.html";*/
				}
			}

			if (allow_listing) {
				StringBuffer str = new StringBuffer();
				HTMLFunctions.Heading(str, 1, HTMLFunctions.s2HTML("Directory " + str_url));
				HTMLFunctions.StartTable(str);

				HTMLFunctions.StartRow(str);
				String parent = new File(str_url).getParent();
				if (parent != null) { 
					parent = parent.replaceAll("\\\\", "/");
					HTMLFunctions.AddCell(str, "<a href=\"" + parent + "\">[up]</a>");
					HTMLFunctions.EndRow(str);


					for (int i=0 ; i<files.length ; i++) {
						if (files[i].isHidden() == false && files[i].getName().startsWith(".") == false) {
							HTMLFunctions.StartRow(str);
							String full_url = Functions.AppendSlashToEnd(str_url) + URLEncoder.encode(files[i].getName(), "UTF-8").replaceAll("\\+", "%20");
							if (files[i].isDirectory() == false) {
								HTMLFunctions.AddCell(str, "<a href=\"" + full_url + "\" rel=\"nofollow\">" + HTMLFunctions.s2HTML(files[i].getName()) + "</a> [<a href=\"" + full_url + "\" target=\"_blank\" rel=\"nofollow\">new window</a>]");
								HTMLFunctions.AddCell(str, (files[i].length()/1024) + " Kb");
								HTMLFunctions.AddCell(str, "&nbsp;");
								HTMLFunctions.AddCell(str, Dates.FormatDate(new Date(files[i].lastModified()), "dd MMM yyyy HH:mm"));
							} else {
								HTMLFunctions.AddCell(str, "<a href=\"" + full_url + "\"  rel=\"nofollow\">" + HTMLFunctions.s2HTML(files[i].getName()) + "</a>");
								HTMLFunctions.AddCell(str, "[dir]");
							}
							HTMLFunctions.EndRow(str);
						}
					}
					HTMLFunctions.EndTable(str);
				}
				body_html.append(str);
				AddStdFooter(body_html);
			} else if (redir_to_web_page.length() > 0) {
				this.redirectTo_Using303(redir_to_web_page);
			} else {
				this.setRedirectInCodeTo("AccessDeniedPage" + ClientConnection.DEF_EXT);
			}
		} else {
			throw new RuntimeException("Trying to list a non-dir directory.");
		}

	}

	public int compare(File o1, File o2) {
		File f1 = (File)o1;
		File f2 = (File)o2;
		return f1.getName().compareTo(f2.getName());
	}

}
