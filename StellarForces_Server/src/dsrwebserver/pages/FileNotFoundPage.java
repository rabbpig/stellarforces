package dsrwebserver.pages;

import ssmith.html.HTMLFuncs_old;
import ssmith.html.HTMLFunctions;

public final class FileNotFoundPage extends AbstractHTMLPage {
	
	public FileNotFoundPage() { // THIS CANNOT HAVE ANY PARAMS!
		super();
		this.setHTTPCode(404);
		
		send_google_analytics = false;
		
	}

	public void process() throws Exception {
		/*try {
			DSRWebServer.p("FNF Referrer: " + super.headers.getReferer());
		} catch (Exception ex) {
			ex.printStackTrace();
		}*/

		this.setTitle("File not found");
		//this.redirectTo_Using303("/");

		body_html.append(HTMLFuncs_old.Title(1, this.getHTTPErrorCode() + " Error"));
		body_html.append(HTMLFuncs_old.Para("We apologise, but this address is not on this server."));
		HTMLFunctions.Para(body_html, "You can get to the main page <a href=\"/\">here</a>.");

		AddStdFooter(body_html);
	}

}
