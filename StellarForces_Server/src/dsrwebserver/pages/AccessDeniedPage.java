package dsrwebserver.pages;

import dsrwebserver.HTTPHeaders;
import ssmith.html.HTMLFuncs_old;

public final class AccessDeniedPage extends AbstractHTMLPage {

	public AccessDeniedPage() {
		super();
		
		this.setHTTPCode(HTTPHeaders.HTTP_ACCESS_DENIED);
		send_google_analytics = false;
		
	}

	
	public void process() throws Exception {
		this.setTitle("Access Denied");
		//super.insertDefaultTitleHTML();
		body_html.append(HTMLFuncs_old.Title(1, "Access Denied"));
		body_html.append(HTMLFuncs_old.Para("This directory cannot be listed.  If it is a directory, which it might not be."));
		//HTMLFunctions.Heading(body_html, 5, "Generated at " + Functions.FormatDate(new Date(), "dd MMM yyyy HH:mm"));
		AddStdFooter(body_html);

		//body_html.append(AbstractHTMLPage.getFooterHTML());
	}

}
