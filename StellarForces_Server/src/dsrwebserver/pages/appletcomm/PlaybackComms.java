package dsrwebserver.pages.appletcomm;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import dsr.comms.AbstractCommFuncs;
import dsrwebserver.DSRWebServer;
import dsrwebserver.pages.AbstractPage;
import dsrwebserver.pages.dsr.playbackapplet.commfuncs.GameDataCommFunctions;
import dsrwebserver.pages.dsr.playbackapplet.commfuncs.GameDataCommFunctions2;
import dsrwebserver.pages.dsr.playbackapplet.commfuncs.TurnDataCommFunctions;

public class PlaybackComms extends AbstractPage { // Do not rename this as it's called by the Android client

	// Main commands
	public static final String GET_PLAYBACK_GAME_DATA = "getgamedata";
	public static final String GET_PLAYBACK_GAME_DATA2 = "getgamedata2";
	public static final String GET_PLAYBACK_TURN_DATA = "getturndata";

	// Return codes
	public static final String OK = "ok";
	//public static final String ERROR = "error";

	private String response = ""; 

	public PlaybackComms() {
		this.content_type = "text/text";
	}
	
	
	@Override
	public void process() throws Exception {
		String post_raw = this.headers.getPostValueAsString("post");
		String post_b64_decoded = AbstractCommFuncs.GetCommFuncs().specialDecode(post_raw);

		String post_a[] = post_b64_decoded.split(MiscCommsPage.SEP);
		try {
			for(int i=0 ; i<post_a.length ; i++) {
				HashMap<String, String> hashmap = MiscCommsPage.ExtractParams(post_a[i]);
				this.processCmd(hashmap);
			}
			// If everything's okay, send OK.
			if (response == null) {
				response = "";
			}
			if (response.length() == 0) {
				response = OK;
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
			response = ex.toString();
		}
		this.content_length = response.length();
	}


	/**
	 * We only append something if it's not an OK.
	 * 
	 */
	private void processCmd(HashMap<String, String> hashmap) throws SQLException {
		String cmd = hashmap.get("cmd"); 
		
		if (cmd == null) {
			//throw new RuntimeException("No data from client");
		} else if (cmd.equalsIgnoreCase(GET_PLAYBACK_GAME_DATA)) {
			response = GameDataCommFunctions.GetGameDataResponse(dbs, hashmap);
		} else if (cmd.equalsIgnoreCase(GET_PLAYBACK_GAME_DATA2)) {
			response = GameDataCommFunctions2.GetGameDataResponse(dbs, hashmap);
		} else if (cmd.equalsIgnoreCase(GET_PLAYBACK_TURN_DATA)) {
			response = TurnDataCommFunctions.GetTurnDataResponse(dbs, hashmap);
		} else {
			throw new RuntimeException("Unknown command: " + cmd);
		}
	}

	
	@Override
	protected void writeContent() throws IOException {
		this.writeString(response.toString());
	}

}
