package dsrwebserver.pages.appletcomm;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;

import ssmith.lang.Functions;
import ssmith.util.KeyValuePair;
import dsr.SharedStatics;
import dsr.comms.AbstractCommFuncs;
import dsr.comms.CampaignComms;
import dsr.comms.EquipmentDataComms;
import dsr.comms.EquipmentListComms;
import dsr.comms.EventDataComms;
import dsr.comms.FinishedGamesComms;
import dsr.comms.GameDataComms;
import dsr.comms.MapDataComms;
import dsr.comms.MessagesComms;
import dsr.comms.NewGamesComms;
import dsr.comms.OtherComms;
import dsr.comms.PlayersGamesComms;
import dsr.comms.SetStatComms;
import dsr.comms.UnitDataComms;
import dsr.comms.UniverseComms;
import dsr.comms.VisibleEnemyComms;
import dsrwebserver.DSRWebServer;
import dsrwebserver.pages.AbstractPage;
import dsrwebserver.tables.LoginLogTable;
import dsrwebserver.tables.LoginsTable;

public final class MiscCommsPage extends AbstractPage { // Do not rename this as it's called by the Android client

	public static final String SEP = "~";

	// Return codes
	public static final String SIDE_1_WINS = "s1w";
	public static final String SIDE_2_WINS = "s2w";
	public static final String SIDE_3_WINS = "s3w";
	public static final String SIDE_4_WINS = "s4w";
	public static final String OK = "ok";
	public static final String DRAW = "draw";
	public static final String MSG = "msg";
	public static final String ERROR = "error";

	public static final String GET = "get";
	public static final String PUT = "put";

	// Main commands
	// IF YOU ADD ANY, ADD THEM TO THE ANDROID CLIENT MiscCommsPage!!
	public static final String CHECK_VERSION2 = "checkver2";
	public static final String VALIDATE_LOGIN = "val";
	public static final String AUTOREGISTER = "autoregister";
	public static final String GET_PLAYERS_DATA = "get_players_data";
	
	public static final String PLAYERS_GAMES = "games";
	public static final String GAME_DATA = "gamedata";
	public static final String UNIT_DATA = "unitdata";
	public static final String EQUIPMENT_DATA = "equipdata";
	public static final String REMOVE_EQUIPMENT_DATA = "rem_equip";
	public static final String MAP_DATA = "mapdata";
	public static final String VISIBLE_ENEMY_DATA = "visen_data";
	public static final String EVENT_DATA = "event_data";
	public static final String GET_LATEST_VERSION_NUM = "latest_ver";
	public static final String PLAYERS_FINISHED_GAME_DATA = "fin_game_data";
	public static final String REGISTER_LOGIN = "register_login";
	public static final String GET_SEEN_ENEMY_DATA = "seen_enemy_data";
	public static final String GET_HEARD_ENEMY_DATA = "heard_enemy_data";

	public static final String GET_EQUIPMENT_LIST = "equipment_list";
	public static final String GET_ARMOUR_LIST = "armour_list";

	public static final String GET_NEW_GAMES_LIST = "new_games_list";
	public static final String JOIN_NEW_GAME = "join_game";
	public static final String CREATE_NEW_GAME = "create_game";
	public static final String GET_MISSION_LIST = "mission_list";
	public static final String START_PRACTISE_GAME = "start_practise";
	public static final String START_PRACTISE_GAME_WITH_AI = "start_practise_with_ai";
	public static final String START_NEW_SAME_SIDES = "start_new_same_sides";
	public static final String START_NEW_OPP_SIDES = "start_new_opp_sides";

	public static final String GET_NUM_UNREAD_MESSAGES = "get_num_unread_messages";
	public static final String GET_MESSAGES = "get_messages";
	public static final String MESSAGE_READ = "message_read";
	public static final String SEND_MESSAGE = "send_message";

	public static final String GET_FINISHED_GAME_LIST = "fin_games_list";
	public static final String GET_ALL_FINISHED_GAME_LIST = "all_fin_games_list";
	public static final String GET_MSG_FROM_SERVER = "get_server_msg";
	public static final String GET_LEAGUE_TABLE = "get_league_table";

	public static final String JOIN_CAMPAIGN = "join_campaign";
	public static final String GET_CAMP_SQUAD_LIST = "get_camp_squad_list";
	public static final String SEND_CAMP_SQUAD_SELECTION = "send_camp_squad_selection";

	public static final String SET_STAT = "set_stat";

	public static final String GET_UNIVERSE_MAP_DATA = "get_uni_map_data";
	public static final String GET_TREATIES_DATA = "get_treaties";
	public static final String GET_UNI_SQUAD_LIST = "get_uni_squad_list";
	public static final String SEND_UNI_SQUAD_SELECTION = "send_uni_squad_sel";
	public static final String GET_CONTRACTS_DATA = "get_contracts_data";
	public static final String GET_UNI_INVENTORY_DATA = "get_uni_inv_data";
	
	public static final String OFFER_TREATY = "offer_treaty";
	public static final String ACCEPT_TREATY = "accept_treaty";
	public static final String DECLINE_TREATY = "decline_treaty";
	public static final String CANCEL_TREATY = "cancel_treaty";
	// IF YOU ADD ANY, ADD THEM TO THE ANDROID CLIENT MiscCommsPage!!

	private String response = ""; 
	private static volatile Hashtable<String, Long> last_time = new Hashtable<String, Long>(); 

	public MiscCommsPage() {
		this.content_type = "text/text";

		addHTTPHeader("Cache-Control: no-cache");
		addHTTPHeader("Pragma: no-cache");
		Calendar c1 = Calendar.getInstance();
		String s_date = COOKIE_DATE_FORMAT.format(c1.getTime());
		addHTTPHeader("Expires: " + s_date);

	}


	public static String GetWinnerCodeFromWinner(int i) {
		switch (i) {
		case 1:
			return SIDE_1_WINS;
		case 2:
			return SIDE_2_WINS;
		case 3:
			return SIDE_3_WINS;
		case 4:
			return SIDE_4_WINS;
		default:
			throw new RuntimeException("Unknown winning side");
		}
	}

	
	@Override
	public void process() throws Exception {
		boolean from_android = false;
		try {
			from_android = super.conn.headers.getUserAgent().toLowerCase().indexOf("apache-httpclient") >= 0 || super.conn.headers.getUserAgent().length() == 0 || super.conn.headers.getUserAgent().toLowerCase().indexOf("stellarforces") >= 0;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		String post_raw = this.headers.getPostValueAsString("post");
		String post_b64_decoded = AbstractCommFuncs.GetCommFuncs().specialDecode(post_raw);//.replaceAll("\n", replacement);
		int android_version = 0;
		try {
			android_version = Functions.ParseInt(this.headers.getPostValueAsString("android_version"));
		} catch (Exception ex) {
			// Do nothing
		}
		boolean encode = true; //(android_version >= 94);
		try {
			String clientid = this.headers.getPostValueAsString("clientid");
			if (clientid.length() > 0) {
				long time = Long.parseLong(this.headers.getPostValueAsString("time"));
				// Check time is after last request
				long prev_time = 0;
				if (last_time.containsKey(clientid)) {
					prev_time = last_time.get(clientid);
				}
				last_time.remove(clientid);
				last_time.put(clientid, time);
				if (prev_time > 0 && prev_time >= time) {
					// Dupe request!  Ignore it
					//DSRWebServer.SendEmailToAdmin("Request ignored", "Yes it was!");
					response = ERROR + ": invalid time";
					this.content_length = response.length(); // We want the client to know we've errored so they don't retry
					return;
					//throw new RuntimeException("Dupe request ignored");  No!  We don't want the client to know we've errored as we don't want them to retry.
				}
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		String post_a[] = post_b64_decoded.split(SEP);
		try {
			for(int i=0 ; i<post_a.length ; i++) {
				if (post_a[i].trim().length() > 0) {
					HashMap<String, String> hashmap = ExtractParams(post_a[i]); // .trim()
					/*if (hashmap.size() == 0) {
					throw new RuntimeException("No data from client (hashmap)");
				}*/
					this.processCmd(hashmap, from_android);//cmd, get_or_put, login, pwd, data);
				}
			}
			// If everything's okay, send OK.
			if (response.length() == 0) {
				response = OK;
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
			response = ERROR + ": " + ex.getMessage();
		}
		
		if (encode) {
			response = AbstractCommFuncs.GetCommFuncs().specialEncode(response);
		}
		this.content_length = response.length(); // System.out.println(response);
	}


	/**
	 * This does the same as decodeParams, but doesn't httpdecode the values.
	 */
	public static HashMap<String, String> ExtractParams(String s) {
		char[] data = s.toCharArray();
		HashMap<String, String> hm = new HashMap<String, String>();

		StringBuffer str_key = new StringBuffer();
		StringBuffer str_val = new StringBuffer();
		boolean getting_key = true;  // Start getting the key
		int pos = 0;
		while (pos < data.length) {
			char x = data[pos];
			if (getting_key) {
				if (x == '=') {
					getting_key = false; // Now getting the value
				} else {
					str_key.append(x);
				}
			} else { // Getting the value
				if (x == '&') {
					KeyValuePair kvp = new KeyValuePair(str_key.toString(), str_val.toString());
					hm.put(kvp.key.toLowerCase(), kvp.value.toString());
					str_key.delete(0, str_key.length());
					str_val.delete(0, str_val.length());
					getting_key = true;
				} else {
					str_val.append(x);
				}
			}
			pos = pos + 1;
		}

		KeyValuePair kvp = new KeyValuePair(str_key.toString(), str_val.toString());
		hm.put(kvp.key.toLowerCase(), kvp.value.toString());
		return hm;
	}

	/**
	 * We only append something if it's not an OK.
	 * @throws UnsupportedEncodingException 
	 * 
	 */
	private void processCmd(HashMap<String, String> hashmap, boolean from_android) throws IOException, SQLException, UnsupportedEncodingException {
		String cmd = hashmap.get("cmd");
		float pc_version = 0f;
		try { 
			pc_version = Float.parseFloat(hashmap.get("version"));
		} catch (Exception ex) {
			// Do nothing
		}
		float comms_version = 0f;
		try { 
			comms_version = Float.parseFloat(hashmap.get("comms_version"));
		} catch (Exception ex) {
			// Do nothing
		}
		int android_version = 0;
		try { 
			android_version = Functions.ParseInt(hashmap.get("android_version"));
		} catch (Exception ex) {
			// Do nothing
		}
		String login = AbstractCommFuncs.URLDecodeString(hashmap.get("login"));
		if (login != null) {
			login = login.trim();
		}
		String pwd = AbstractCommFuncs.URLDecodeString(hashmap.get("pwd"));
		String get_or_put = hashmap.get("getput");

		if (from_android == false) {
			if (login != null) {
				if (login.indexOf("@") > 0) { // Email address
					login = login.replaceAll(" ", "+"); // Since PC client doesn't encode!
				}
			}
		}

		if (cmd == null) {
			// Do nothing
		} else if (cmd.equalsIgnoreCase(CHECK_VERSION2)) {
			if (pc_version >= SharedStatics.MIN_CLIENT_VERSION) {
				if (pc_version < SharedStatics.MAX_UPGRADE_VERSION) {
					response = "200|A new version of the client is available!";
				} else {
					response = "200|";
				}
			} else {
				//AppletMain.p("CLIENT VERSION TO LOW: " + v);
				response = "404|Minimum version required: " + SharedStatics.MIN_CLIENT_VERSION;
			}
		} else if (cmd.equalsIgnoreCase(AUTOREGISTER)) {
			response = OtherComms.AutoRegister(dbs, hashmap);
		} else if (cmd.equalsIgnoreCase(GET_LATEST_VERSION_NUM)) {
			response = "" + SharedStatics.MAX_UPGRADE_VERSION;
		} else if (cmd.equalsIgnoreCase(VALIDATE_LOGIN)) {
			// Log it
			int llid = -1;
			try {
				String ip = this.headers.getXForwardedFor();
				llid = LoginLogTable.AddRec(dbs, login, pwd, ip);
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}
			if (LoginsTable.DoesLoginExistAndEnabled(dbs, login, pwd) == false) {
				LoginLogTable.SetSuccessful(dbs, llid, false);
				//DSRWebServer.SendEmailToAdmin("Login failed", "'" + login + "'/'" + pwd + "'");
				response = ERROR + ": Invalid login/pwd";
			} else {
				LoginLogTable.SetSuccessful(dbs, llid, true);
				LoginsTable logintable = new LoginsTable(dbs);
				logintable.selectUser(login, pwd);
				logintable.setLastLoginDate();
				logintable.close();
			}
		} else if (cmd.equalsIgnoreCase(PLAYERS_GAMES)) {
			response = PlayersGamesComms.GetPlayersGamesAsResponse(dbs, login, pwd, hashmap, -1, from_android);
		} else if (cmd.equalsIgnoreCase(PLAYERS_FINISHED_GAME_DATA)) {
			int gameid = Integer.parseInt(hashmap.get("gameid"));
			response = PlayersGamesComms.GetPlayersGamesAsResponse(dbs, login, pwd, hashmap, gameid, from_android);
		} else if (cmd.equalsIgnoreCase(GAME_DATA)) {
			if (get_or_put.equalsIgnoreCase(MiscCommsPage.GET)) {
				throw new RuntimeException("Not required");
			} else if (get_or_put.equalsIgnoreCase(MiscCommsPage.PUT)) {
				response = GameDataComms.DecodeGameUpdateRequest(dbs, hashmap); // Checks gamecode
			} else {
				throw new RuntimeException("Unknown get_or_put");
			}
		} else if (cmd.equalsIgnoreCase(UNIT_DATA)) {
			if (get_or_put.equalsIgnoreCase(MiscCommsPage.GET)) {
				response = UnitDataComms.GetUnitDataAsResponse(dbs, hashmap).toString(); // Checks gamecode
			} else if (get_or_put.equalsIgnoreCase(MiscCommsPage.PUT)) {
				UnitDataComms.DecodeUnitUpdateRequest(dbs, hashmap); // checks unit code!
			} else {
				throw new RuntimeException("Unknown get_or_put");
			}
		} else if (cmd.equalsIgnoreCase(EQUIPMENT_DATA)) {
			if (get_or_put.equalsIgnoreCase(MiscCommsPage.GET)) {
				response = EquipmentDataComms.GetEquipmentDataAsResponse(dbs, hashmap);
			} else if (get_or_put.equalsIgnoreCase(MiscCommsPage.PUT)) {
				EquipmentDataComms.DecodeEquipmentUpdateRequest(dbs, hashmap);
			} else {
				throw new RuntimeException("Unknown get_or_put");
			}
		} else if (cmd.equalsIgnoreCase(REMOVE_EQUIPMENT_DATA)) {
			response = EquipmentDataComms.GetRemoveEquipmentDataAsResponse(dbs, hashmap);
			/*} else if (cmd.equalsIgnoreCase(FINISHED_EQUIPPING)) {
			response = EquipmentDataComms.GetFinishedEquippingAsResponse(dbs, version, hashmap);*/
		} else if (cmd.equalsIgnoreCase(MAP_DATA)) {
			if (get_or_put.equalsIgnoreCase(MiscCommsPage.GET)) {
				response = MapDataComms.GetMapDataAsResponse(dbs, hashmap).toString();
			} else if (get_or_put.equalsIgnoreCase(MiscCommsPage.PUT)) {
				MapDataComms.DecodeMapDataUpdateRequest(dbs, hashmap);
			} else {
				throw new RuntimeException("Unknown get_or_put");
			}
		} else if (cmd.equalsIgnoreCase(VISIBLE_ENEMY_DATA)) {
			if (get_or_put.equalsIgnoreCase(MiscCommsPage.GET)) {
				throw new RuntimeException("This bit isn't written yet.  Do I need it?");
			} else if (get_or_put.equalsIgnoreCase(MiscCommsPage.PUT)) {
				VisibleEnemyComms.DecodeVisibleEnemyRequest(dbs, hashmap);
			} else {
				throw new RuntimeException("Unknown get_or_put");
			}
		} else if (cmd.equalsIgnoreCase(EVENT_DATA)) {
			if (get_or_put.equalsIgnoreCase(MiscCommsPage.GET)) {
				throw new RuntimeException("This bit isn't written yet.  Do I need it?");
			} else if (get_or_put.equalsIgnoreCase(MiscCommsPage.PUT)) {
				EventDataComms.DecodeNewEventRequest(dbs, hashmap);
			} else {
				throw new RuntimeException("Unknown get_or_put");
			}
		} else if (cmd.equalsIgnoreCase(REGISTER_LOGIN)) {
			String display_name = AbstractCommFuncs.URLDecodeString(hashmap.get("display_name"));
			LoginsTable login_tbl = new LoginsTable(dbs);
			String warning = login_tbl.createLogin(pwd, display_name, login, false);
			if (warning.length() == 0) {
				/*if (DSRWebServer.ADMIN_LOGIN_ID > 0) {
					MessagesTable.SendMsg(dbs, DSRWebServer.ADMIN_LOGIN_ID, login_tbl.getID(), "Hello " + display_name + " and welcome to " + DSRWebServer.TITLE, "Welcome to " + DSRWebServer.TITLE + ", the turn-based tactical strategy game.  You have been registered with the following details:\n\nLogin: " + login_tbl.getDisplayName() + "\nPassword: " + login_tbl.getPassword() + "\nEmail Address: " + login_tbl.getEmail() + "\n\nIf your email address is incorrect, please just reply with the correct one.  If you have any questions, suggestions or queries, no matter how trivial, please just reply to this or ask on the forums at " + DSRWebServer.NEW_FORUM_LINK + ".  Feel free to join *any* game and play against any player.\n\nThere is now an online tutorial to help you play the game, which can be viewed at http://www.stellarforces.com/dsr/webtutorial.cls .\n\nIf you like the game, please tell others about it and leave a good review.  If you don't, please tell us!\n\nHope you have fun, and good luck!");
				}*/
			} else {
				response = ERROR + ": " + warning;
			}
			login_tbl.close();
		} else if (cmd.equalsIgnoreCase(GET_EQUIPMENT_LIST)) {
			int gameid = Integer.parseInt(hashmap.get("gid"));
			response = EquipmentListComms.GetEquipmentListAsResponse(dbs, login, pwd, gameid);
		} else if (cmd.equalsIgnoreCase(GET_ARMOUR_LIST)) {
			int gameid = Integer.parseInt(hashmap.get("gid"));
			response = EquipmentListComms.GetArmourListAsResponse(dbs, login, pwd, gameid);
		} else if (cmd.equalsIgnoreCase(GET_SEEN_ENEMY_DATA)) {
			response = VisibleEnemyComms.GetSeenEnemyData(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(GET_HEARD_ENEMY_DATA)) {
			response = VisibleEnemyComms.GetHeardEnemyData(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(GET_NEW_GAMES_LIST)) {
			response = NewGamesComms.GetNewGamesListAsResponse(dbs, login, pwd, android_version);
		} else if (cmd.equalsIgnoreCase(JOIN_NEW_GAME)) {
			response = NewGamesComms.GetJoinNewGameAsResponse(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(CREATE_NEW_GAME)) {
			response = NewGamesComms.GetCreateNewGameAsResponse(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(START_PRACTISE_GAME)) {
			response = NewGamesComms.GetStartPractiseGameAsResponse(dbs, login, pwd, hashmap, false);
		} else if (cmd.equalsIgnoreCase(START_PRACTISE_GAME_WITH_AI)) {
			response = NewGamesComms.GetStartPractiseGameAsResponse(dbs, login, pwd, hashmap, true);
		} else if (cmd.equalsIgnoreCase(START_NEW_SAME_SIDES)) {
			response = NewGamesComms.GetStartNewGameSameSides(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(START_NEW_OPP_SIDES)) {
			response = NewGamesComms.GetStartNewGameOppSides(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(GET_MISSION_LIST)) {
			response = NewGamesComms.GetMissionListAsResponse(dbs, hashmap, login, pwd, android_version);
		} else if (cmd.equalsIgnoreCase(GET_NUM_UNREAD_MESSAGES)) {
			response = MessagesComms.GetNumUnreadMessages(dbs, login, pwd);
		} else if (cmd.equalsIgnoreCase(GET_MESSAGES)) {
			response = MessagesComms.GetMessagesListAsResponse(dbs, login, pwd);
		} else if (cmd.equalsIgnoreCase(MESSAGE_READ)) {
			response = MessagesComms.MarkMsgAsRead(dbs, hashmap);
		} else if (cmd.equalsIgnoreCase(SEND_MESSAGE)) {
			response = MessagesComms.SendMessage(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(GET_FINISHED_GAME_LIST)) {
			response = FinishedGamesComms.GetFinishedGamesList(dbs, login, pwd, true);
		} else if (cmd.equalsIgnoreCase(GET_ALL_FINISHED_GAME_LIST)) {
			response = FinishedGamesComms.GetFinishedGamesList(dbs, login, pwd, false);
		} else if (cmd.equalsIgnoreCase(GET_MSG_FROM_SERVER)) {
			response = OtherComms.RespondToServerMsg(dbs, hashmap);
		} else if (cmd.equalsIgnoreCase(GET_LEAGUE_TABLE)) {
			response = OtherComms.GetLeagueTable(dbs, login);
		} else if (cmd.equalsIgnoreCase(GET_PLAYERS_DATA)) {
			response = OtherComms.GetPlayersData(dbs, login, pwd);
		} else if (cmd.equalsIgnoreCase(JOIN_CAMPAIGN)) {
			response = CampaignComms.JoinCampaign(dbs, login, pwd);
		} else if (cmd.equalsIgnoreCase(GET_CAMP_SQUAD_LIST)) {
			response = CampaignComms.GetPlayersCampSquadList(dbs, login, pwd);
		} else if (cmd.equalsIgnoreCase(SEND_CAMP_SQUAD_SELECTION)) {
			response = CampaignComms.SetPlayersCampSquad(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(SET_STAT)) {
			response = SetStatComms.SetStat(dbs, hashmap);
		} else if (cmd.equalsIgnoreCase(GET_UNIVERSE_MAP_DATA)) {
			response = UniverseComms.GetUniverseMapData(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(GET_TREATIES_DATA)) {
			response = UniverseComms.GetTreatyData(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(GET_UNI_SQUAD_LIST)) {
			response = UniverseComms.GetPlayersUniverseSquadList(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(SEND_UNI_SQUAD_SELECTION)) {
			response = UniverseComms.SetUniverseSquadSelection(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(GET_CONTRACTS_DATA)) {
			response = UniverseComms.GetContractsData(dbs, hashmap);
		} else if (cmd.equalsIgnoreCase(GET_UNI_INVENTORY_DATA)) {
			response = UniverseComms.GetUniInventoryData(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(OFFER_TREATY)) {
			response = UniverseComms.OfferTreaty(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(ACCEPT_TREATY)) {
			response = UniverseComms.AcceptTreaty(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(DECLINE_TREATY)) {
			response = UniverseComms.DeclineTreaty(dbs, login, pwd, hashmap);
		} else if (cmd.equalsIgnoreCase(CANCEL_TREATY)) {
			response = UniverseComms.CancelTreaty(dbs, login, pwd, hashmap);
		} else {
			// Do nothing
			//throw new RuntimeException("Unknown command: " + cmd);
		}
	}


	@Override
	protected void writeContent() throws IOException {
		this.writeString(response.toString());
	}


}
