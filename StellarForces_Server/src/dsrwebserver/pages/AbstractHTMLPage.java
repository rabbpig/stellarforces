package dsrwebserver.pages;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import ssmith.util.DiskCache;
import dsrwebserver.ClientConnection;
import dsrwebserver.DSRWebServer;
import dsrwebserver.HTTPHeaders;
import dsrwebserver.pages.dsr.AbstractDSRPage;

public abstract class AbstractHTMLPage extends AbstractDSRPage {

	private static final String DTD = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
	//private static final String DTD = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
	private static final String CTYPE = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n";

	protected StringBuffer body_html;
	protected boolean add_default_html = false;
	private ArrayList<String> head_html = new ArrayList<String>();
	private String title = "";
	protected boolean send_google_analytics = true;
	public long start_time = System.currentTimeMillis();
	protected static DiskCache disk_cache = new DiskCache(DSRWebServer.WEB_CACHE);
	
	public AbstractHTMLPage() {
		super();
		
		body_html = new StringBuffer();

		add_default_html = true;
			this.addHTMLToHead("<link rel=\"stylesheet\" type=\"text/css\" href=\"/stylesheets/default.css\" />"); // NOT FROM KAROO cos of background image
			this.addHTMLToHead("<link rel=\"stylesheet\" type=\"text/css\" href=\"/stylesheets/dropdown.css\" />"); // NOT FROM KAROO cos of background image
			this.addHTMLToHead("<script type=\"text/javascript\" src=\"/javascripts/dropdown.js\"></script>");
			this.addHTMLToHead("<link rel=\"stylesheet\" type=\"text/css\" href=\"/stylesheets/table_grid.css\" />"); // NOT FROM KAROO cos of background image
			this.addHTMLToHead("<link rel=\"stylesheet\" type=\"text/css\" href=\"/stylesheets/table_small.css\" />"); // NOT FROM KAROO cos of background image
		this.addHTMLToHead("<script type=\"text/javascript\" src=\"/javascripts/forms.js\"></script>");

	}

	
	public void setTitle(String t) {
		this.title = DSRWebServer.TITLE + ": " + t;
	}

	
	public String getTitle() {
		return this.title;
	}

	
	public void init(ClientConnection c, HTTPHeaders head) throws SQLException {
		super.init(c, head);

		addHTTPHeader("Cache-Control: no-cache");
		addHTTPHeader("Pragma: nocache");

		this.addHTTPHeader("Transfer-Encoding: chunked");
		this.addHTTPHeader("X-AuthorBoast: Yes laydeez, I wrote this in Java!");
	}

	
	public void addHTMLToHead(String html) {
		this.head_html.add(html);
	}

	
	/**
	 * Override this if you don't want to send HTML.
	 */
	protected void writeContent() throws IOException {
		if (this.add_default_html) {
			add_default_html = false; // So we don't send it again
			StringBuffer str = new StringBuffer();
			str.append(DTD);
			//str.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n<head>\n");
			str.append("<html lang=\"en\">\n<head>\n");
			str.append(CTYPE + "\n");
			str.append("<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"/favicon.ico\" />\n");
			str.append("<title>" + this.title + "</title>\n");
			if (this.head_html.size() > 0) {
				for(int i=0 ; i<this.head_html.size() ; i++) {
					String s = (String)this.head_html.get(i);
					str.append(s + "\n");
				}
			}

			str.append("</head>\n<body>\n");

			sendHTMLChunk(str.toString());
		}
		if (body_html.toString().length() > 0) {
			sendHTMLChunk(body_html.toString());//System.out.println(body_html.toString())
			body_html.delete(0, body_html.length()); // So we don't resend it
		}

		this.sendEndOfHTML();
		conn.getPrintWriter().flush();
	}

	
	public void sendHTMLChunk(String str) throws IOException {
		synchronized (conn) { // So we can have two threads writing
			this.writeLength(str.length());
			this.writeString(str);
			this.writeString("");
		}

	}

	protected void sendEndOfHTML() throws IOException {
		StringBuffer str = new StringBuffer();

		// Add google analytics
		if (send_google_analytics) {
			str.append("<script type=\"text/javascript\">");
			str.append("var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");");
			str.append("document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));");
			str.append("</script>");
			str.append("<script type=\"text/javascript\">");
			str.append("try {");
			str.append("var pageTracker = _gat._getTracker(\"UA-3441289-2\");");
			str.append("pageTracker._trackPageview();");
			str.append("} catch(err) {}</script>");
		}

		str.append("</body>\n</html>\n");
		sendHTMLChunk(str.toString());

		this.writeString("0");
		this.writeString("");
		this.flush();
	}


	private void writeLength(int l) throws IOException {
		this.writeString(Integer.toHexString(l+2));

	}

	
	protected void AddStdFooter(StringBuffer str) {
		HTMLFunctions.Heading(str, 5, "Generated at " + Dates.FormatDate(new Date(), "dd MMM yyyy HH:mm") + " by " + DSRWebServer.WEBSERVER_NAME);
	}


	protected void redirectTo_UsingRefresh(String url, int time) throws SQLException {
		this.addHTMLToHead("<meta http-equiv=\"refresh\" content=\"" + time + "; URL=" + url + " \">");
	}

}
