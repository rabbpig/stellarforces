package dsrwebserver.pages;

import ssmith.html.HTMLFuncs_old;

public final class ErrorPage extends AbstractHTMLPage {

	private Exception ex;
	private String desc;

	public ErrorPage(int code, String d, Exception e) {
		super();
		desc = d;
		ex = e;
		this.setHTTPCode(code);
		send_google_analytics = false;

	}

	public void process() throws Exception {
		this.setTitle(getHTTPErrorCode() +  " Error");
		
		//super.insertDefaultTitleHTML();
		body_html.append(HTMLFuncs_old.Title(1, this.getHTTPErrorCode() + " Error"));
		body_html.append(HTMLFuncs_old.Para("We apologise, but an error of type " + ex.getClass().getName() + " has occurred:"));
		body_html.append(HTMLFuncs_old.Para(desc));
		body_html.append(HTMLFuncs_old.Para("Error details: " + ex.getMessage()));
		body_html.append(HTMLFuncs_old.Para("Stack Trace:"));
		for (int c = 0; c < ex.getStackTrace().length; c++) {
			body_html.append(ex.getStackTrace()[c].getClassName() + ":");
			body_html.append(ex.getStackTrace()[c].getLineNumber() + " - ");
			body_html.append(ex.getStackTrace()[c].getMethodName());
			body_html.append("<br />");
		}
		//HTMLFunctions.Heading(body_html, 5, "Generated at " + Functions.FormatDate(new Date(), "dd MMM yyyy HH:mm"));
		AddStdFooter(body_html);

		//body_html.append(AbstractHTMLPage.getFooterHTML());
	}

}
