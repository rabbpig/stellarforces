package dsrwebserver.pages.androidcomm;

import ssmith.html.HTMLFunctions;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;

public class ViewScanner extends AbstractAndroidPage {

	public ViewScanner() {
		super();

	}

	
	@Override
	public void subprocess() throws Exception {
		try {
			int gameid = this.headers.getGetValueAsInt("gameid");

			GamesTable game = new GamesTable(dbs);
			game.selectRow(gameid);
			if (game.isPlayerInGame(this.this_user.getLoginID())) {
				if (MapDataTable.IsFullyCreated(dbs, MapDataTable.GetMapIDFromGameID(dbs, gameid))) {
					str.append("<img align=left hspace=10 src=\"/dsr/MapImageForCurrentGame.cls?gid=" + gameid + "&android_login=" + login + "&android_pwd=" + pwd + "&key=1\" alt=\"Strategic scanner for current game\" ><br />");
				} else {
					HTMLFunctions.Heading(str, 3, "Map still being generated...");
				}
			} else {
				HTMLFunctions.Para(str, "You are not in that game.");

			}
			game.close();
		} catch (Exception ex) {
			str.append(ex.toString());
		}
	}

}
