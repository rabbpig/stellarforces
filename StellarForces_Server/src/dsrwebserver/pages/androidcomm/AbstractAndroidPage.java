package dsrwebserver.pages.androidcomm;

import dsr.comms.AbstractCommFuncs;
import dsrwebserver.pages.AbstractPage;
import dsrwebserver.tables.LoginsTable;

/**
 * DO NOT USE THIS ANY MORE!
 *
 */
public abstract class AbstractAndroidPage extends AbstractPage {
	
	protected StringBuffer str = new StringBuffer();
	protected LoginsTable this_user;
	protected String login, pwd;
	
	public AbstractAndroidPage() {
		super();
	}


	@Override
	public void process() throws Exception {
		login = AbstractCommFuncs.URLDecodeString(this.headers.getGetValueAsString("android_login"));
		pwd = AbstractCommFuncs.URLDecodeString(this.headers.getGetValueAsString("android_pwd"));
		
		this_user = new LoginsTable(dbs);
		if (this_user.selectUser(login, pwd)) {
			subprocess();
			this.content_length = str.length();
		} else {
			str.append("Invalid credentials: " + login + "/" + pwd);
		}
		
	}

	
	public abstract void subprocess() throws Exception;


	@Override
	protected void writeContent() throws Exception {
		super.writeString(str.toString());
		
	}

}
