package dsrwebserver.pages.androidcomm;

import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.GameLogTable;
import dsrwebserver.tables.GamesTable;

public class GetGameLog extends AbstractAndroidPage {

	public GetGameLog() {
		super();

		super.content_type = "text/text";
	}

	@Override
	public void subprocess() throws Exception {
		try {
			int gameid = this.headers.getGetValueAsInt("gameid");

			GamesTable game = new GamesTable(dbs);
			game.selectRow(gameid);
			AbstractMission mission = AbstractMission.Factory(game.getMissionID());
			if (game.isPlayerInGame(this.this_user.getLoginID())) {
				str.append(GameLogTable.GetLogEntries(dbs, game, game.getTurnNo()-1, super.this_user.getLoginID(), false, mission.isSnafu() == false));
			} else {
				str.append("You are not in that game.");

			}
		} catch (Exception ex) {
			str.append(ex.toString());
		}
	}

}

