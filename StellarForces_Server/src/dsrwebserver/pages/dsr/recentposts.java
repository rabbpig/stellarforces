package dsrwebserver.pages.dsr;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;

import ssmith.dbs.MySQLConnection;
import ssmith.html.HTMLFunctions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.components.Post;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.pages.dsr.forums.ForumMainPage;
import dsrwebserver.pages.dsr.forums.ForumPostingsPage;
import dsrwebserver.tables.LoginsTable;

public class recentposts extends AbstractHTMLPage {

	public recentposts() {
		super();
	}

	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();
		
		ArrayList<Post> posts = new ArrayList<Post>();

		// get PHPBB posts
		MySQLConnection phpdbs = DSRWebServer.getPHPDbs();
		if (phpdbs != null) {
			String sql = "SELECT t.topic_id, t.topic_title, t.topic_last_post_id, t.forum_id, p.post_id, p.poster_id, p.post_time, p.post_text, u.user_id, u.username ";
			sql = sql + " FROM phpbb_topics t, phpbb_forums f, phpbb_posts p, phpbb_users u ";
			sql = sql + " WHERE t.topic_id = p.topic_id AND ";
			sql = sql + " f.forum_id = t.forum_id AND ";
			//sql = sql + " t.forum_id != 4 AND ";
			//sql = sql + " t.topic_status <> 2 AND ";
			sql = sql + " p.poster_id = u.user_id ";
			sql = sql + " ORDER BY p.post_time DESC ";
			sql = sql + " LIMIT 100";
			ResultSet rs = phpdbs.getResultSet(sql);
			while (rs.next()) {
				Post p = new Post();
				p.link = DSRWebServer.NEW_FORUM_LINK + "/viewtopic.php?f=" + rs.getInt("forum_id") + "&t=" + rs.getInt("topic_id") + "&p=" + rs.getInt("post_id") + "#p" + rs.getInt("post_id");
				p.title = rs.getString("topic_title");
				p.text = rs.getString("post_text");
				p.posted_by = rs.getString("username");
				p.ms = (rs.getLong("post_time") * 1000);
				posts.add(p);
			}
		}

		// Get my posts
		String sql = "SELECT ForumPostings.ForumPostingID, ForumPostings.Body, ForumPostings.DateCreated, ForumTopics.Name, ForumTopics.ForumTopicID, Logins.LoginID FROM ForumPostings ";
		sql = sql + " INNER JOIN ForumTopics ON ForumTopics.ForumTopicID = ForumPostings.ForumTopicID ";
		sql = sql + " LEFT JOIN Logins ON Logins.LoginID = ForumPostings.UserID ";
		//sql = sql + " WHERE ForumTopics.ParentForumTopicID <> " + DSRWebServer.GAMES_FORUM_ID;
		sql = sql + " ORDER BY DateCreated DESC LIMIT 100";
		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) {
			Post p = new Post();
			p.link = "/dsr/forums/ForumPostingsPage.cls?topic=" + rs.getInt("ForumTopicID") + "#pid" + rs.getInt("ForumPostingID");
			p.title = rs.getString("Name");
			p.text = ForumPostingsPage.GetEncodedMessage(rs.getString("Body"));
			p.posted_by = LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginID"), true);
			p.ms = rs.getTimestamp("DateCreated").getTime(); // 1388601329000
			//DSRWebServer.p(p.date);
			posts.add(p);
		}

		Collections.sort(posts, new Post());

		str = new StringBuffer();
		//str.append("<table width=200><tr><td><div class=\"structural\">");

		//HTMLFunctions.Heading(str, 2, "<a href=\"" + DSRWebServer.NEW_FORUM_LINK + "/viewforum.php?f=1\">Latest Forums Posts</a>"); 

		int max = Math.min(150, posts.size());
		for (int i=0 ; i<max ; i++) {
			Post p = posts.get(i);
			str.append("<p><div class=\"little2\">");
			str.append("<h3><a href=\"" + p.link + "\">" + HTMLFunctions.HTMLEncode(p.title) + "</a></h3>");
			/*String text = ;
			/*if (text.length() > 800) {
				text = text.substring(0, 799) + "[...]";
			}*/
			str.append(HTMLFunctions.HTMLEncode(p.text) + "<br />");
			str.append("</div><div class=\"little\">Posted by " + p.posted_by + " " + ForumMainPage.GetTimeAgo(p.ms));
			// + " to <a href=\"/dsr/forums/ForumPostingsPage.cls?topic=" + rs.getInt("ForumTopicID") + "#pid" + rs.getInt("ForumPostingID") + "\">" + rs.getString("Name") + "</a>");
			str.append("</div>");
			str.append("</p><hr />");

		}
		//str.append("</div></td></tr>");
		//str.append("</table>");

		this.body_html.append(MainLayout.GetHTML(this, "Most Recent Forum Posts", str));		

	}

}

