package dsrwebserver.pages.dsr;

import ssmith.html.HTMLFunctions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;

public class credits extends AbstractHTMLPage {
	
	public credits() {
		super();
	}
	
	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();
		
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Designed and Programmed by <a href=\"mailto:" + DSRWebServer.EMAIL_ADDRESS + "\">Stephen Smith</a>.");
		HTMLFunctions.AddListEntry(str, "Based on <a href=\"http://en.wikipedia.org/wiki/Laser_Squad\">Laser Squad</a>, designed and programmed by <a href=\"http://en.wikipedia.org/wiki/Julian_Gollop\">Julian Gollop</a>.");
		//HTMLFunctions.AddListEntry(str, "<a href=\"http://www.jmonkeyengine.com/\">JMonkey Engine</a> used for 3D.");
		//HTMLFunctions.AddListEntry(str, "All models taken from <a href=\"http://ufoai.sourceforge.net/\">UFO:AI</a> unless specified.");
		//HTMLFunctions.AddListEntry(str, "Tyrant model by <a href=\"mailto:morourke@_NOSPAM_thegrid.net\">Mike O'Rourke</a>, taken from <a href=\"http://planetquake.gamespy.com/View.php?view=ModeloftheWeek.Detail&id=36\">http://planetquake.gamespy.com/View.php?view=ModeloftheWeek.Detail&id=36</a>.");
		HTMLFunctions.AddListEntry(str, "Music by <a href=\"http://ocremix.org/artist/4513/mark-vera\">Mark Vera</a> <a href=\"http://www.ocremix.org\">OverClocked ReMix</a>.  Credited as per \"http://ocremix.org/info/Content_Policy\".");
		HTMLFunctions.AddListEntry(str, "Lots of playtesting and helpful feedback by CornMaster and Victor.");
		HTMLFunctions.AddListEntry(str, "Thanks to <a href=\"http://www.lasersquad.org/\">William</a> for the Laser Squad shooting algorythm.");
		HTMLFunctions.AddListEntry(str, "Thanks to Victor, Pedro Casaca and Giuseppe Luigi for the Spanish and Portugese translations.");
		HTMLFunctions.AddListEntry(str, "Thanks to Pedro Casaca for the website favicon.");
		HTMLFunctions.AddListEntry(str, "Thanks to Deadlime (Linden Clarke) for design of the campaign mode (including Faction descriptions) and for finding out how to get it to work on 64-bit Windows.");
		HTMLFunctions.AddListEntry(str, "Thanks to Joao Ralha for drawing the image on the Alien Containment mission description page.");
		HTMLFunctions.AddListEntry(str, "Thanks to Deadlime (Linden Clarke) for creating the Facebook page for Stellar Forces and creating the units voice effects.");
		//HTMLFunctions.AddListEntry(str, "Queen Alien 'Xenoid' model by <a href=\"mailto:danbickell_NO_SPAM_loop.com\">Dan Bickell</a>, taken from <a href=\"http://pages.cpsc.ucalgary.ca/~samavati/cpsc453/documents/assign3/models/xenoid/\">http://pages.cpsc.ucalgary.ca/~samavati/cpsc453/documents/assign3/models/xenoid/</a>.");
		HTMLFunctions.AddListEntry(str, "Thanks to Boyflea (Dave Gumble) for ideas for new trophies, lots of extra mission text and grouping the missions by campaign.");
		HTMLFunctions.AddListEntry(str, "Thanks to everyone who has created a new mission.");
		HTMLFunctions.EndUnorderedList(str);
		
		//HTMLFunctions.Heading(str, 3, "Legal Mumbo Jumbo");
		//HTMLFunctions.Para(str, "This game (code and my own artwork) is licensed under <a href=\"http://www.gnu.org/licenses/gpl-2.0.txt\">GPL 2</a>.  If you would like a copy of the source code, just send me an email and I'll create a new release for download.  All copyrights acknowledged.  Any similarities between this and <a href=\"http://en.wikipedia.org/wiki/Laser_Squad\">Laser Squad</a> are entirely deliberate.");

		this.body_html.append(MainLayout.GetHTML(this, "Credits", str));		
	}

}
