package dsrwebserver.pages.dsr;

import ssmith.html.HTMLFunctions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;

public class webtutorial extends AbstractHTMLPage {

	private static final int STARTUP = 0;
	private static final int REGISTER_1 = 1;
	private static final int REGISTER_2 = 2;
	private static final int MENU = 3;
	private static final int CREATE_GAME = 4;
	private static final int CREATE_PRACTISE_GAME = 5;
	private static final int CHOOSE_GAME = 6;
	private static final int DEF_EQUIPMENT = 7;
	private static final int ARMOUR = 8;
	private static final int EQUIPMENT = 9;
	private static final int FINISHED_EQUIPPING = 10;
	private static final int ASK_TUTORIAL = 11;
	private static final int DEPLOY_1 = 12;
	private static final int DEPLOY_2 = 13;
	private static final int DEPLOY_3 = 14;
	private static final int TURN_1_1 = 15;
	private static final int TURN_1_2 = 16;
	private static final int TURN_1_3 = 17;
	private static final int TURN_1_4 = 18;
	private static final int TURN_1_5 = 19;
	private static final int TURN_1_6 = 20;
	private static final int TURN_1_7 = 21;
	private static final int TURN_1_8 = 22;
	private static final int TURN_1_9 = 23;
	private static final int TURN_1_10 = 24;
	private static final int TURN_1_11 = 25;
	private static final int TURN_1_12 = 26;
	private static final int TURN_1_13 = 27;
	private static final int TURN_1_14 = 28;
	private static final int TURN_1_15 = 29;
	private static final int TURN_1_16 = 30;
	private static final int MAX_STAGES = 30;

	public webtutorial() {
		super();
	}

	@Override
	public void process() throws Exception {
		
		int stage = this.headers.getGetValueAsInt("stage");
		
		this.setTitle("Online Tutorial");
		
		StringBuffer str = new StringBuffer();

		HTMLFunctions.Para(str, "This is an online tutorial for playing Stellar Forces on Android.  Click on Next or Previous to go to the next or previous stage of the tutorial.");

		str.append("<a name=\"tut\"></a><table cellpadding=10><tr><td bgcolor=#666666>");
		HTMLFunctions.Heading(str, 3, getTitle(stage));
		HTMLFunctions.Para(str, getText(stage));
		
		if (stage > 0 && stage <= MAX_STAGES) {
			HTMLFunctions.Para(str, "<a href=\"?stage=" + (stage-1) + "\">Previous</a> | <a href=\"?stage=" + (stage+1) + "\">Next</a>");
		} else if (stage > MAX_STAGES) {
			HTMLFunctions.Para(str, "<a href=\"?stage=" + (stage-1) + "\">Previous</a>");
		} else {
			HTMLFunctions.Para(str, "<a href=\"?stage=" + (stage+1) + "\">Next</a>");
		}

		str.append("<img src=\"/tutorial/" + getFilename(stage) + "\" />");

		str.append("</td></tr></table>");


		this.body_html.append(MainLayout.GetHTML(this, "Online Tutorial", str));		
	}


	public String getFilename(int stage) {
		switch (stage) {
		case STARTUP: return "startup.png";
		case REGISTER_1: return "register1.png";
		case REGISTER_2: return "register2.png";
		case MENU: return "menu.png";
		case CREATE_GAME: return "creategame.png";
		case CREATE_PRACTISE_GAME: return "createpractisegame.png";
		case CHOOSE_GAME: return "choosegame.png";
		case DEF_EQUIPMENT: return "def_equipment.png";
		case ARMOUR: return "armour.png";
		case EQUIPMENT: return "equipment.png";
		case FINISHED_EQUIPPING: return "finished_equipping.png";
		case ASK_TUTORIAL: return "ask_tutorial.png";
		case DEPLOY_1: return "deploy_unit1.png";
		case DEPLOY_2: return "deploy_unit2.png";
		case DEPLOY_3: return "deploy_unit3.png";
		case TURN_1_1: return "turn1_1.png";
		case TURN_1_2: return "turn1_2.png";
		case TURN_1_3: return "turn1_3.png";
		case TURN_1_4: return "turn1_4.png";
		case TURN_1_5: return "turn1_5.png";
		case TURN_1_6: return "turn1_6.png";
		case TURN_1_7: return "turn1_7.png";
		case TURN_1_8: return "turn1_8.png";
		case TURN_1_9: return "turn1_9.png";
		case TURN_1_10: return "turn1_10.png";
		case TURN_1_11: return "turn1_11.png";
		case TURN_1_12: return "turn1_12.png";
		case TURN_1_13: return "turn1_13.png";
		case TURN_1_14: return "turn1_14.png";
		case TURN_1_15: return "turn1_15.png";
		case TURN_1_16: return "turn1_16.png";
		default:
			return "";
		}
	}


	public String getTitle(int stage) {
		switch (stage) {
		case STARTUP: return "Startup";
		case REGISTER_1: return "Registering #1";
		case REGISTER_2: return "Registering #2";
		case MENU: return "Main Menu";
		case CREATE_GAME: return "Creating a Game";
		case CREATE_PRACTISE_GAME: return "Creating a Practise Game";
		case CHOOSE_GAME: return "Starting the Game";
		case DEF_EQUIPMENT: return "Default Equipment";
		case ARMOUR: return "Selecting Armour";
		case EQUIPMENT: return "Selecting Equipment";
		case FINISHED_EQUIPPING: return "Finished Equipping";
		case ASK_TUTORIAL: return "Tutorial Mode";
		case DEPLOY_1: return "Deploying #1";
		case DEPLOY_2: return "Deploying #2";
		case DEPLOY_3: return "Deploying #3";
		case TURN_1_1: return "Turn 1";
		case TURN_1_2: return "Turn 1";
		case TURN_1_3: return "Turn 1";
		case TURN_1_4: return "Turn 1";
		case TURN_1_5: return "Turn 1";
		case TURN_1_6: return "Turn 1";
		case TURN_1_7: return "Turn 1";
		case TURN_1_8: return "Turn 1";
		case TURN_1_9: return "Turn 1";
		case TURN_1_10: return "Turn 1";
		case TURN_1_11: return "Turn 1";
		case TURN_1_12: return "Turn 1";
		case TURN_1_13: return "Turn 1";
		case TURN_1_14: return "Turn 1";
		case TURN_1_15: return "Turn 1";
		case TURN_1_16: return "Next Turn";
		default:
			return "Finished";
		}
	}


	public String getText(int stage) {
		switch (stage) {
		case STARTUP: return "If you haven't already, fire up Stellar Forces on your Android device.  If you haven't downloaded it already, get it <a href=\"https://play.google.com/store/apps/details?id=com.scs.stellarforces.main.full\">here</a>.  Note that due to a policy of constant improvement, the screenshots below may differ from what you see on your Android device.</p><p>What is that awful music?  Press to Mute button (bottom right) to turn it off, and then click on 'Register' to register on the game.";
		case REGISTER_1: return "Enter your details by clicking on each of the boxes and typing in the correct answer.  If you try and use details that another player has used, you will be asked to type them in again.  I can tell you now that 'SteveSmith' is already taken";
		case REGISTER_2: return "Here are some details I entered earlier.  Your email address will never be shared; it is needed so that you can receive emails telling you when it is your turn, or when you have won a game.</p><p>Once you have finished, click on 'Register Now'.";
		case MENU: return "Once you have been told that you have succesfully registered, you will be taken to the main menu.  To start a new game, select 'Start or Join Game'.";
		case CREATE_GAME: return "We want to start a practise game for this tutorial (i.e. against the AI, not against a real live person, although playing against a real-live person is thoroughly recommended) so select 'Start Practise Game'.";
		case CREATE_PRACTISE_GAME: return "Click on 'Start Practise Game' again to confirm you definitely want to do this.  Your practise game is created in the blink of an eye, assuming you blink slowly.";
		case CHOOSE_GAME: return "You are then taken to the list of all your current games.   You should only have one in the list unless you got impatient and created one before starting this tutorial.  Click on the game to take your turn.";
		case DEF_EQUIPMENT: return "To save time, you can choose the use the default armour and equipment instead of having to select it all yourself.  Choose Yes for now;  once you become an expert you can try out some of the other items that are available.";
		case ARMOUR: return "Here you choose your unit's armour.  Each row is one of your units.  Select the type of armour you would like for each unit by pressing on the square buttons.  Press 'Buy Selected Armour' when you have finished.</p><p>Different armour costs different amounts, but the more it costs, the more it will protect your unit if they get hit.";
		case EQUIPMENT: return "Here you choose equipment for you units.  You can slide the list of units up and down, and scroll through the available items using the left/right arrows.  To buy an item for a unit, select 'Buy Item' next to the unit you want to buy it for, and it will appear next to that unit.  You can get more information about a piece of equipment by clicking on the text between the arrows.";
		case FINISHED_EQUIPPING: return "Are you sure you have chosen well?";
		case ASK_TUTORIAL: return "As this is a practise game, it is recommended that you have the tutorial mode on.  This will give you helpful hints and instructions while you are playing.";
		case DEPLOY_1: return "You are now ready to deploy your units.  As side 1, you can place your units on any of the orange squares.  If you were side 2, you could place them on any blue squares.  Simply press the square to place a unit.";
		case DEPLOY_2: return "That's units 1, 2 and 3 placed.";
		case DEPLOY_3: return "That's units 4 and 5 placed.  Press the screen to go back to your list of games, and select the game again to take the first turn and begin battle!";
		case TURN_1_1: return "In this mission our objective is to kill Sterner Regnix, who is wearing white.  He is somewhere in the building guarded by 5 bodyguards.  Note that enemy units are only shown if one of your units can see them.</p><p>The white square shows which unit is currently selected.  You can select your other units by either pressing on them or pressing Next or Previous Unit.";
		case TURN_1_2: return "You can move the currently selected unit by pressing the arrows at the bottom-right of the screen.<p></p>Here we have moved two of our units next to the door, and then opened the door.  We have been fortunate that the enemy unit is facing in the wrong direction.  Lets shoot him in the back like the hero that we are!";
		case TURN_1_3: return "Select 'Shot Menu', and then either click on the flashing red icon to aim at the enemy, or drag the screen to aim manually.  Once lined up, select the type of shot you would like to make.  Each type costs a different amount of AP's, but also vary in accuracy.  The % figure shows the chance of getting an accurate shot.  I'll choose a snap-shot.";
		case TURN_1_4: return "As you can see here, I should have chosen a more accurate shot since my unit has accidentally shot and killed a friendly unit.  Damn.";
		case TURN_1_5: return "And the next attempt didn't work much better, and I have killed my own unit again.  (For those curious, this did genuinely happen when I was creating this tutorial - stupid game).";
		case TURN_1_6: return "Okay, lets re-assess the situation using the Scanner, by pressing 'Scanner'.  Here you can see a map of the whole battlefield.  Your units flash pink, and any visible enemy units flash red.  As you can see, I am down to 3 units.  Hopefully you are doing better.  After all, I only wrote this game, it's not like I should be any good at it.";
		case TURN_1_7: return "Maybe the units on the other side of the house will have more luck.";
		case TURN_1_8: return "Opening the door reveals another enemy unit.  As he can see us, he will shoot automatically at us.  His first shot hits one of my units and wounds him by 14 points.  Thank goodness I chose a strong armour.";
		case TURN_1_9: return "Time to take revenge.  I aim the gun and fire a shot.";
		case TURN_1_10: return "It hits the enemy unit, who also fires back!  But misses - phew.";
		case TURN_1_11: return "Unfortunately it wasn't enough to kill him, so I move my other unit in to take another shot.  Again the enemy unit fires, and again they miss.</p><p>All actions cost Action Points (APs).  Units start a turn with a limited amount of action points, and once they are used up they cannot perform any more actions that turn.  The number of action points is shown at the top.  All actins cost points, like shooting, moving and picking up equipment.";
		case TURN_1_12: return "Another shot is fired by both units, but still no-one is killed (yet).  Here I managed to get a screengrab of the bullets as they wizz through the air.";
		case TURN_1_13: return "Time for a tactical withdrawl by moving back out the house.  Now that I've spent all my unit's APs, it's time to end the turn and let the AI take its turn.  This is done by selecting 'End Turn', and then 'End Turn Now'.";
		case TURN_1_14: return "Since we are playing against the AI, they take theit turn immediately.  If you were playing against another player, they would receive an email telling them it is their turn, and you would have to wait for them.</p><p>Needless to say, the first thing the AI does is to shoot the only unit it can see.  And a pretty good shot it is too.";
		case TURN_1_15: return "Fortunately, the other AI unit isn't as accurate, and misses again.";
		case TURN_1_16: return "Now that the AI has finished its turn, you are taken back to the list of games.  To take another turn in the game, simply choose that game.</p><p>Note that if you are playing against a human opponent, you would have to wait until they have taken their turn before the game appears on this list.";
		default:
			return "And that is the end of the tutorial.  If you have any questions, no matter how simple, please don't hesitate to ask them on the <a href=\"" + DSRWebServer.NEW_FORUM_LINK + "\">forums</a>.</p><p>Coming soon: Grenades.";
		}


	}


}

