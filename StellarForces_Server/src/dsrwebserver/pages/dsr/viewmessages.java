package dsrwebserver.pages.dsr;

import java.sql.ResultSet;

import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MessagesTable;

public class viewmessages extends AbstractHTMLPage {

	public viewmessages() {
		super();
	}


	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		if (this.session.isLoggedIn()) {
			String sql = "SELECT * FROM Messages WHERE (ToID = " + this.current_login.getID() + " OR FromID = " + this.current_login.getID() + ") ";
			if (this.current_login.isAdmin()) {
				// Don't show all the welcome messages
				sql = sql + " AND Subject <> 'Welcome to " + DSRWebServer.TITLE + "'";
			}
			sql = sql + " ORDER BY MsgRead, DateCreated DESC LIMIT 50";

			ResultSet rs = dbs.getResultSet(sql);
			//HTMLFunctions.StartTable(str, 3);
			str.append("<table width=\"40%\" class=\"stats\" align=\"left\">");
			//HTMLFunctions.StartTable(str, "stats", "", 0, "", 0);
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCellHeading(str, "From");
			HTMLFunctions.AddCellHeading(str, "To");
			HTMLFunctions.AddCellHeading(str, "Subject");
			//HTMLFunctions.AddCellHeading(str, "Message");
			HTMLFunctions.AddCellHeading(str, "Date");
			HTMLFunctions.AddCellHeading(str, "Unread");
			HTMLFunctions.EndRow(str);
			while (rs.next()) {
				String b_start = "", b_end = "";
				if (rs.getInt("MsgRead") == 0 && rs.getInt("ToID") == this.current_login.getID()) {
					b_start = "<b>";
					b_end = "</b>";
				}
				HTMLFunctions.StartRow(str);
				if (rs.getInt("FromID") > 0) {
					HTMLFunctions.AddCell(str, b_start + LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("FromID"), rs.getInt("FromID") != this.current_login.getID()) + b_end);
				} else {
					HTMLFunctions.AddCell(str, b_start + "n/a" + b_end);
				}
				HTMLFunctions.AddCell(str, b_start + LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("ToID"), rs.getInt("ToID") != this.current_login.getID()));
				String subject = rs.getString("Subject");
				if (subject == null) {
					subject = "[none]";
				} else if (subject.length() <= 0) {
					subject = "[none]";
				}
				HTMLFunctions.AddCell(str, b_start + "<a href=\"?msgid=" + rs.getInt("MessageID") + "\">" + subject + "</a>");
				//HTMLFunctions.AddCell(str, rs.getString("Message"));
				HTMLFunctions.AddCell(str, b_start + "<nobr>" + Dates.FormatDate(rs.getTimestamp("DateCreated"), Dates.UKDATE_FORMAT2_WITH_TIME) + "</nobr>");
				if (rs.getInt("MsgRead") == 0 && rs.getInt("ToID") == this.current_login.getID()) {
					HTMLFunctions.AddCell(str, "<a href=\"?msgid=" + rs.getInt("MessageID") + "\">UNREAD</a>");
				} else {
					HTMLFunctions.AddCell(str, "");
				}
				HTMLFunctions.EndRow(str);
				if (rs.getInt("MsgRead") == 0) {
					str.append("</b>");
				}
			}
			HTMLFunctions.EndTable(str);


			int msgid = this.headers.getGetValueAsInt("msgid");
			if (msgid > 0) { // Viewing a message
				MessagesTable msg = new MessagesTable(dbs);
				msg.selectRow(msgid);
				// Check it's on of ours
				if (msg.getToID() == this.current_login.getID() || msg.getFromID() == this.current_login.getID()) {
					str.append("<table width=\"50%\" align=\"right\" border=\"1\" cellpadding=\"10\"><tr><td>");
					if (msg.getToID() == this.current_login.getID()) {
						MessagesTable.MarkAsRead(dbs, msg.getID());
					}
					//str.append("<hr />");
					if (msg.getToID() == this.current_login.getID() && msg.getFromID() > 0) {
						str.append("[<a href=\"playerspublicpage.cls?next=reply&loginid=" + msg.getFromID() + "&msgid=" + msgid + "\">Reply to this message</a>]<br /><br />");
					}
					if (msg.getFromID() > 0) {
						str.append("From: " + LoginsTable.GetDisplayName_Enc(dbs, msg.getFromID(), true) + "<br />");
					} else {
						// Nothing
					}
					str.append("Date: " + Dates.FormatDate(msg.getDateSent(), Dates.UKDATE_FORMAT2_WITH_TIME) + "<br />");
					str.append("Subject: " + HTMLFunctions.s2HTML(msg.getSubject()) + "<br />");
					str.append("<hr />");
					str.append("<br />");
					//str.append(ForumPostingsPage.GetEncodedMessage(msg.getMessage()));
					//str.append(HTMLFunctions.s2HTML(msg.getMessage()).replaceAll("\n", "<br />") + "<br />");
					str.append(msg.getMessage().replaceAll("\n", "<br />") + "<br />"); // DON'T ENCODE AS WE SEND LINKS IN MSG COMRADE!
					if (msg.getToID() == this.current_login.getID() && msg.getFromID() > 0) {
						str.append("<br /><br />[<a href=\"playerspublicpage.cls?next=reply&loginid=" + msg.getFromID() + "&msgid=" + msgid + "\">Reply to this message</a>]<br />");
					}
					//str.append("<hr />");
					str.append("</tr></td></table>");
				}
				msg.close();
			}

		}

		this.body_html.append(MainLayout.GetHTML(this, "Messages", str));		
	}

}
