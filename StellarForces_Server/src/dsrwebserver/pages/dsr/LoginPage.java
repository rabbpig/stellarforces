package dsrwebserver.pages.dsr;

import ssmith.lang.Functions;
import dsr.comms.AbstractCommFuncs;
import dsrwebserver.DSRWebServer;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.LoginLogTable;
import dsrwebserver.tables.LoginsTable;

public final class LoginPage extends AbstractHTMLPage {

	public LoginPage() {
		super();
	}


	public void process() throws Exception {
		String warning = "";

		String login = this.headers.getGetOrPostValueAsString("Login");
		String pwd = this.headers.getGetOrPostValueAsString("pwd");
		String url = this.headers.getGetOrPostValueAsString("url");
		if (this.headers.doesPostKeyExists("login") || this.headers.doesGetKeyExist("login")) {
			if (login.length() > 0 && pwd.length() > 0) {
				warning = "Sorry, that's an unknown email address or password"; // Just in case
				int llid = -1;
				try {
					String ip = this.headers.getXForwardedFor();
					llid = LoginLogTable.AddRec(dbs, login, pwd, ip);
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
				// Check login
				boolean success = false;
				LoginsTable lt = new LoginsTable(dbs);
				if (lt.selectUser(login, pwd)) {
					if (lt.isDisabled() == false) {
						lt.close();
						success = true;
						LoginLogTable.SetSuccessful(dbs, llid, true);
						current_login.selectUser(login, pwd);
						warning = "";
						session.setLoggedIn(current_login.getLoginID(), true);
						DSRWebServer.p("User " + login + " logged in.");
						if (url.length() > 0) {
							this.redirectTo_Using303(url);
							return;
						}
					} else {
						warning = "Sorry, that login has been disabled.  Please email support at " + DSRWebServer.cfgfile.get("admin_email");
						//DSRWebServer.SendEmailToAdmin("Disabled Login Attempted", "User " + lt.getDisplayName() + " (" + lt.getEmail() + ") has tried to log in.");
					}
				}
				lt.close();
				if (success == false) {
					DSRWebServer.p("Login " + login + "/" + pwd + " failed.");
					LoginLogTable.SetSuccessful(dbs, llid, false);
					Functions.delay(2000);// Stop too many attempts
					this.setHTTPCode(403);
				}
			} else {
				warning = "Please enter a login!";
			}
		}

		if (warning.length() > 0) {
			this.redirectToMainPage("warning=" + AbstractCommFuncs.URLEncodeString(warning));
		} else {
			this.redirectToMainPage("");
		}
	}


}
