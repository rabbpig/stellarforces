package dsrwebserver.pages.dsr;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.GameRequestsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;

public class startpractisemission { 
	
	public static void Start1PlayerPractiseMission(MySQLConnection dbs, int loginid) throws Exception {
		AbstractMission mission = null;
		//if (DSRWebServer.IsSF()) {
		mission = AbstractMission.Factory(AbstractMission.SF_PRACTISE_MISSION);
		/*} else {
			mission = AbstractMission.Factory(AbstractMission.AE_PRACTISE_MISSION);
		}*/
		int p1side = 1;
		int reqid = GameRequestsTable.CreateRequest(dbs, 1, loginid, mission.getType(), p1side, GameRequestsTable.GT_PRACTISE, 0, false, false, 0, "", 100, -1, -1, -1, -1);
		GameRequestsTable requests = new GameRequestsTable(dbs);
		requests.selectRow(reqid);

		GamesTable game = new GamesTable(dbs);
		int gameid = game.createGame(requests.getNumSides(), 0, 0, 0, 0, requests.getMission(), requests.getType(), requests.isAdvancedMode(), requests.isCampaignGame(), 0, -1, -1, -1);
		game.setPlayerID(p1side, loginid);
		requests.setGameID(gameid);
		requests.setFullyAccepted();

		mission.createUnits(dbs, game);
		int mapid = mission.createMapStart(dbs, gameid);
		game.setMapDataID(mapid);
		requests.close();
	}


	public static void Start1PlayerPractiseMissionWithAI(MySQLConnection dbs, int loginid) throws SQLException, FileNotFoundException, IOException {
		AbstractMission mission = null;
		mission = AbstractMission.Factory(AbstractMission.SF_PRACTISE_MISSION_WITH_AI);
		int p1side = 1;
		int reqid = GameRequestsTable.CreateRequest(dbs, 2, loginid, mission.getType(), p1side, GameRequestsTable.GT_PRACTISE, 0, false, false, 0, "", 100, -1, -1, -1, -1);
		GameRequestsTable requests = new GameRequestsTable(dbs);
		requests.selectRow(reqid);

		GamesTable game = new GamesTable(dbs);
		int gameid = game.createGame(requests.getNumSides(), loginid, DSRWebServer.AI_LOGIN_ID, 0, 0, requests.getMission(), requests.getType(), requests.isAdvancedMode(), requests.isCampaignGame(), 0, -1, -1, -1);
		game.setAISide(2);

		requests.setGameID(gameid);
		requests.setFullyAccepted();
		requests.close();
		mission.createUnits(dbs, game);
		//mission.createUnits(dbs, game, 2);
		int mapid = mission.createMapStart(dbs, gameid);
		game.setMapDataID(mapid);

		// Wait for map & game to be created so it appears straight away on players list
		while (MapDataTable.IsFullyCreated(dbs, mapid) == false) {
			Functions.delay(500);
		}
		
	}

}

