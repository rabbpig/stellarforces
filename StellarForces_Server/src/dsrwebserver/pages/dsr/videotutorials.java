package dsrwebserver.pages.dsr;

import ssmith.html.HTMLFunctions;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;

public class videotutorials extends AbstractHTMLPage {

	public videotutorials() {
		super();
	}

	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		HTMLFunctions.Para(str, "Here's a selection of tutorial videos showing how to play Stellar Forces.  More videos will be added soon.");

		HTMLFunctions.Heading(str, 3, "Tutorial Video: Deployment");
		str.append("<object width=\"480\" height=\"385\"><param name=\"movie\" value=\"http://www.youtube.com/v/5c57hvaOT9Y?fs=1&amp;hl=en_GB\"></param><param name=\"allowFullScreen\" value=\"true\"></param><param name=\"allowscriptaccess\" value=\"always\"></param><embed src=\"http://www.youtube.com/v/5c57hvaOT9Y?fs=1&amp;hl=en_GB\" type=\"application/x-shockwave-flash\" allowscriptaccess=\"always\" allowfullscreen=\"true\" width=\"480\" height=\"385\"></embed></object>");
		HTMLFunctions.Para(str, "How to deploy units in Stellar Forces.");
		
		this.body_html.append(MainLayout.GetHTML(this, "Tutorial Videos", str));		
	}

}
