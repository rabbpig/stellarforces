package dsrwebserver.pages.dsr;

import dsrwebserver.pages.AbstractHTMLPage;

public final class logout extends AbstractHTMLPage {

	public logout() {
		super();
	}

	public void process() throws Exception {
		if (session.isLoggedIn()) {
			//cli.remove(current_login.getDisplayName_Enc(true));
			session.setLoggedIn(0, false);
		}
		addCookie(SESSION_COOKIE, "");
		this.redirectTo_Using303("/");
	}


}
