package dsrwebserver.pages.dsr;

import java.awt.Color;
import java.awt.Graphics;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;

import ssmith.image.CustomJPEG;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.MapData;
import dsrwebserver.maps.MapLoader;
import dsrwebserver.maps.ServerMapSquare;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.MapDataTable;

public class MapImageForMission extends MapImageAbstract {

	public MapImageForMission() {
		super();
	}


	public void process() throws Exception {
		this.content_type = "image/jpeg";

		int missionid = conn.headers.getGetValueAsInt("missionid");

		String filename = DSRWebServer.MAP_IMAGE_CACHE + "/missionmap_" + missionid + ".jpg";
		DSRWebServer.p("Map filename:" + filename);
		if (new File(filename).exists()) {
			FileInputStream fis = new FileInputStream(filename);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte b[] = new byte[1000];
			while (true) {
				int len = fis.read(b);
				if (len < 0) {
					break;
				}
				bos.write(b, 0, len);
			}
			data = bos.toByteArray();
			//this.redirectTo_Using303(filename);
			//return;
			fis.close();
		} else {
			AbstractMission mission = AbstractMission.Factory(missionid);

			MapData mapdata = MapLoader.Import(dbs, -1, mission.getMapFilename(), false);
			int smallest_side = mapdata.getMapWidth();
			sq_size = SIZE/smallest_side;

			int img_width = sq_size * smallest_side;
			int img_height = sq_size * smallest_side;

			CustomJPEG jpg = new CustomJPEG(img_width, img_height);
			Graphics g = jpg.getGraphics();
			g.setColor(Color.black);
			g.fillRect(0, 0, img_width-1, img_height-1);

			for (byte y=0 ; y<mapdata.getMapHeight() ; y++) {
				for (byte x=0 ; x<mapdata.getMapWidth() ; x++) {
					ServerMapSquare sq = mapdata.getSq(x, y);

					boolean draw_sq = true;
					if (sq.major_type == MapDataTable.MT_WALL) {
						g.setColor(GREEN_FLOOR);
					} else if (sq.major_type == MapDataTable.MT_COMPUTER) {
						g.setColor(OUR_COMPUTER_COL);
						g.fillRect((sq.x * sq_size)+2, (sq.y * sq_size)+2, sq_size-4, sq_size-4);
						draw_sq = false;
					} else if (sq.deploy_sq_side > 0 && sq.deploy_sq_side <= mission.getNumOfSides()) {
						g.setColor(super.GetColourForSide(sq.deploy_sq_side));
					} else if (sq.major_type == MapDataTable.MT_NOTHING) {
						g.setColor(Color.DARK_GRAY);
					} else if (sq.escape_hatch_side > 0) {
						g.setColor(ESCAPE_HATCH_COL);
					} else {
						draw_sq = false;
					}

					if (draw_sq) {
						g.fillRect(sq.x * sq_size, sq.y * sq_size, sq_size, sq_size);
					}

					// Draw doors
					if (sq.door_type > 0) {
						g.setColor(Color.green); //.darker()
						if (sq.door_type == MapDataTable.DOOR_EW) {
							g.fillRect((sq.x * sq_size), (sq.y * sq_size) + (sq_size/3), sq_size, sq_size/3);
						} else if (sq.door_type == MapDataTable.DOOR_NS) {
							g.fillRect((sq.x * sq_size) + (sq_size/3), sq.y * sq_size, sq_size/3, sq_size);
						} else {
							throw new RuntimeException("Unknown door type: " + sq.door_type);
						}
					}
				}
			}

			data = jpg.generateDataAsJPEG();
			// Save to file for caching
			OutputStream out = new FileOutputStream(filename);
			out.write(data);
			out.flush();
			out.close();

		}

		this.content_length = data.length; //jpg.generateDataAsJPEG().length;
	}


}
