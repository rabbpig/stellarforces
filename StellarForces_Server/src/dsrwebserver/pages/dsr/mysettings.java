package dsrwebserver.pages.dsr;

import ssmith.html.HTMLFunctions;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.UnitNamesTable;

public final class mysettings extends AbstractHTMLPage {
	
	public mysettings() {
		super();
	}

	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();
		if (this.session.isLoggedIn()) {
			String next = this.headers.getPostValueAsString("next");
			String msg = "";
			if (next.equalsIgnoreCase("save")) {
				String login = this.headers.getPostValueAsString("login");
				String pwd = this.headers.getPostValueAsString("pwd");
				//String displayname = this.headers.getPostValueAsString("displayname");
				String location = this.headers.getPostValueAsString("location");
				String aboutme = this.headers.getPostValueAsString("aboutme");
				String website = this.headers.getPostValueAsString("website");
				String camp_team_name = this.headers.getPostValueAsString("camp_team_name");
				String holiday = this.headers.getPostValueAsString("holiday");
				boolean email_on_turn = this.headers.getPostValueAsString("emailonturn").equalsIgnoreCase("true");
				
				UnitNamesTable.AddRecs(dbs, this.current_login.getID(), this.headers.getPostValueAsString("unitnames"));
				msg = this.current_login.updateLogin(pwd, login, location, aboutme, website, email_on_turn, camp_team_name, holiday);
				if (msg.length() == 0) {
					msg = "Your settings have been saved.";
				}
			}
			
			HTMLFunctions.Para(str, "These are your private settings.  To see your public stats, go <a href=\"playerspublicpage.cls?loginid=" + this.current_login.getID() + "\">here</a>.");

			HTMLFunctions.StartForm(str, "Form1", "", "POST");

			HTMLFunctions.StartTable(str);
			
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Login/Email Address: ");
			HTMLFunctions.StartCell(str);
			HTMLFunctions.TextBox(str, "login", this.current_login.getEmail(), false, 100, 50);
			HTMLFunctions.EndCell(str);
			HTMLFunctions.EndRow(str);
			
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Password: ");
			HTMLFunctions.StartCell(str);
			HTMLFunctions.TextBox(str, "pwd", "", false, 64, 30);
			str.append("(Current pwd: '" + this.current_login.getPassword() + "'.  This will only be changed if you enter something here)");
			HTMLFunctions.EndCell(str);
			HTMLFunctions.EndRow(str);
			
			/*HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Display Name: ");
			HTMLFunctions.StartCell(str);
			HTMLFunctions.TextBox(str, "displayname", this.current_login.getDisplayName(), false, 64, 30);
			HTMLFunctions.EndCell(str);
			HTMLFunctions.EndRow(str);
			*/
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Receive Turn Notifications by Email: ");
			HTMLFunctions.StartCell(str);
			HTMLFunctions.CheckBox(str, "emailonturn", "true", this.current_login.emailOnTurn());
			str.append("<i>If you untick this you will <b>not</b> receive any emails to inform you that your opponent has equipped or deployed, or that it is your turn.  Please visit the website regularly if you choose to do this!</i>");
			HTMLFunctions.EndCell(str);
			//HTMLFunctions.AddCell(str, "If you untick this you will <b>not</b> receive any emails to inform you that your opponent has equipped or deployed, or that it is your turn.  Please visit the website regularly if you choose to do this!");
			HTMLFunctions.EndRow(str);
			
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Location: ");
			HTMLFunctions.StartCell(str);
			HTMLFunctions.TextBox(str, "location", ""+this.current_login.getLocation(), false, 64, 30);
			HTMLFunctions.EndCell(str);
			HTMLFunctions.EndRow(str);
			
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "About You:<br /> <i>(HTML is allowed but please be nice)</i>");
			HTMLFunctions.StartCell(str);
			HTMLFunctions.TextArea(str, "aboutme", 4, 40, ""+this.current_login.getAboutMe(), false);
			HTMLFunctions.EndCell(str);
			HTMLFunctions.EndRow(str);
			
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Website: ");
			HTMLFunctions.StartCell(str);
			HTMLFunctions.TextBox(str, "website", ""+this.current_login.getWebsite(), false, 64, 30);
			HTMLFunctions.EndCell(str);
			HTMLFunctions.EndRow(str);
			
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "On Holiday?:<br /> Put some text here");
			HTMLFunctions.StartCell(str);
			HTMLFunctions.TextArea(str, "holiday", 4, 40, ""+this.current_login.getOnHolidayText(), false);
			HTMLFunctions.EndCell(str);
			HTMLFunctions.EndRow(str);
			
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Campaign Squad Name: ");
			HTMLFunctions.StartCell(str);
			HTMLFunctions.TextBox(str, "camp_team_name", ""+this.current_login.getCampTeamName(), false, 64, 30);
			HTMLFunctions.EndCell(str);
			HTMLFunctions.EndRow(str);
			
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Custom Unit Names:<br /><i>(Enter a list of unit names.  These will be used instead of the default unit names in non-campaign missions)</i>");
			HTMLFunctions.StartCell(str);
			HTMLFunctions.TextArea(str, "unitnames", 8, 40, UnitNamesTable.GetAllNames(dbs, this.current_login.getID()), false);
			HTMLFunctions.EndCell(str);
			HTMLFunctions.EndRow(str);
			
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "");
			HTMLFunctions.StartCell(str);
			HTMLFunctions.SubmitButton(str, "Save");
			HTMLFunctions.EndCell(str);
			HTMLFunctions.EndRow(str);
			
			HTMLFunctions.EndTable(str);
			
			if (msg.length() > 0) {
				HTMLFunctions.Para(str, msg);
			}
			
			HTMLFunctions.HiddenValue(str, "next", "save");
			HTMLFunctions.EndForm(str);

		}
		this.body_html.append(MainLayout.GetHTML(this, "My Settings", str));		
		
	}

}
