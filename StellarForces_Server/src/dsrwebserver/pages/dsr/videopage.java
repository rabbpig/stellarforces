package dsrwebserver.pages.dsr;

import ssmith.html.HTMLFunctions;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;

public class videopage extends AbstractHTMLPage {

	public videopage() {
		super();
	}

	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();
		
		HTMLFunctions.Para(str, "Here's a couple of videos showing how Stellar Forces looks and plays.");

		HTMLFunctions.Heading(str, 3, "Gameplay Video 1");
		str.append("<iframe title=\"YouTube video player\" width=\"480\" height=\"390\" src=\"http://www.youtube.com/embed/UyJyI7Ziomo\" frameborder=\"0\" allowfullscreen></iframe>");

		HTMLFunctions.Heading(str, 3, "Gameplay Video 2");
		str.append("<iframe title=\"YouTube video player\" width=\"480\" height=\"390\" src=\"http://www.youtube.com/embed/T4C3P6FGSvw\" frameborder=\"0\" allowfullscreen></iframe>");

		HTMLFunctions.Heading(str, 3, "Gameplay Video: The Assassins Playback");
		str.append("<iframe title=\"YouTube video player\" width=\"480\" height=\"390\" src=\"http://www.youtube.com/embed/3xWPlpz8oxI\" frameborder=\"0\" allowfullscreen></iframe>");
		
		HTMLFunctions.Heading(str, 3, "Gameplay Video: The Assassins.");
		str.append("<object width=\"480\" height=\"385\"><param name=\"movie\" value=\"http://www.youtube.com/v/sH_1FScNUdA?fs=1&amp;hl=en_GB\"></param><param name=\"allowFullScreen\" value=\"true\"></param><param name=\"allowscriptaccess\" value=\"always\"></param><embed src=\"http://www.youtube.com/v/sH_1FScNUdA?fs=1&amp;hl=en_GB\" type=\"application/x-shockwave-flash\" allowscriptaccess=\"always\" allowfullscreen=\"true\" width=\"480\" height=\"385\"></embed></object>");
		HTMLFunctions.Para(str, "Replaying the classic Laser Squad tactic of demolishing the building in The Assassins mission.");
		

		this.body_html.append(MainLayout.GetHTML(this, "Gameplay Videos", str));		
	}

}
