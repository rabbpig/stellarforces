package dsrwebserver.pages.dsr;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import ssmith.dbs.SQLFuncs;
import ssmith.html.HTMLFunctions;
import ssmith.util.Interval;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;

public class miscstatspage extends AbstractHTMLPage {

	private static final int DAYS = 180;

	private static StringBuffer str_league, str_weps;
	private static Interval update_forum_interval = new Interval(1000 * 60 * 10, true);
	private String cutoff;

	public miscstatspage() {
		super();
	}


	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		if (update_forum_interval.hitInterval() || str_league == null) {
			str_league = new StringBuffer();
			str_weps = new StringBuffer();

			HTMLFunctions.Heading(str_league, 5, "Only games from the last " + DAYS + " days are included.");

			Calendar c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, -DAYS);
			cutoff = SQLFuncs.d2sql(c, false);

			// Longest/shortest mission etc...
			/*try {
				GamesTable game = new GamesTable(dbs);
				String name1, name2;
				// Shortest and longest games
				String sql = "SELECT GameID, DATEDIFF(DateTurnStarted, DateCreated) AS Days FROM Games WHERE Sides >= 2 AND GameStatus = " + GamesTable.GS_FINISHED + " AND WinType <> " + GamesTable.WIN_OPPONENT_CONCEDED + " ORDER BY DATEDIFF(DateTurnStarted, DateCreated), DateCreated";
				ResultSet rs = dbs.getResultSet(sql);
				if (rs.next()) {
					game.selectRow(rs.getInt("GameID"));
					name1 = LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(1), true);
					name2 = LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(2), true);
					HTMLFunctions.Para(str_league, "The shortest game so far was between " + name1 + " and " + name2 + " and took " + rs.getInt("Days") + " days.");
				}
				rs.close();

				sql = "SELECT GameID, DATEDIFF(DateTurnStarted, DateCreated) AS Days FROM Games WHERE Sides >= 2 AND GameStatus = " + GamesTable.GS_FINISHED + " AND WinType <> " + GamesTable.WIN_OPPONENT_CONCEDED + " ORDER BY DATEDIFF(DateTurnStarted, DateCreated) DESC, DateCreated";
				rs = dbs.getResultSet(sql);
				if (rs.next()) {
					game.selectRow(rs.getInt("GameID"));
					name1 = LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(1), true);
					name2 = LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(2), true);
					HTMLFunctions.Para(str_league, "The longest game so far was between " + name1 + " and " + name2 + " and took " + rs.getInt("Days") + " days.");
				}

				// Avg duration
				sql = "SELECT AVG(DATEDIFF(DateTurnStarted, DateCreated)) AS Days FROM Games WHERE Sides >= 2 AND GameStatus = " + GamesTable.GS_FINISHED + " AND WinType <> " + GamesTable.WIN_OPPONENT_CONCEDED;// + " ORDER BY DATEDIFF(DateTurnStarted, DateCreated) DESC, DateCreated";
				rs = dbs.getResultSet(sql);
				if (rs.next()) {
					HTMLFunctions.Para(str_league, "The average game duration is " + rs.getInt("Days") + " days.");
				}

				// Shortest and longest games by TURN
				sql = "SELECT GameID, TurnNo FROM Games WHERE Sides >= 2 AND GameStatus = " + GamesTable.GS_FINISHED + " AND WinType <> " + GamesTable.WIN_OPPONENT_CONCEDED + " ORDER BY TurnNo ";
				rs = dbs.getResultSet(sql);
				if (rs.next()) {
					game.selectRow(rs.getInt("GameID"));
					name1 = LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(1), true);
					name2 = LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(2), true);
					HTMLFunctions.Para(str_league, "The game with the least turns was between " + name1 + " and " + name2 + " and took " + rs.getInt("TurnNo") + " turns.");
				}
				rs.close();

				sql = sql + " DESC";
				rs = dbs.getResultSet(sql);
				if (rs.next()) {
					game.selectRow(rs.getInt("GameID"));
					name1 = LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(1), true);
					name2 = LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(2), true);
					HTMLFunctions.Para(str_league, "The game with the most turns was between " + name1 + " and " + name2 + " and took " + rs.getInt("TurnNo") + " turns.");
				}

				str_league.append(str_weps);
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}*/

			// Most popular missions
			try {
				int LIMIT = 20;
				ResultSet rs = dbs.getResultSet("SELECT Mission, Count(*) as X FROM Games WHERE DateTurnStarted >= " + cutoff + " GROUP BY Mission ORDER BY X DESC LIMIT " + LIMIT);
				HTMLFunctions.Heading(str_league, 3, "Top " + LIMIT + " Most Popular Missions (including current games)");// (last " + DAYS + " days)");
				HTMLFunctions.StartOrderedList(str_league);
				while (rs.next()) {
					if (rs.getInt("X") > 0) {
						HTMLFunctions.AddListEntry(str_league, AbstractMission.GetMissionNameFromType(rs.getInt("Mission"), true, false) + " (" + rs.getInt("X") + ")");
					}
				}
				rs.close();
				HTMLFunctions.EndOrderedList(str_league);
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}

			// Most popular weapons
			try {
				String sql = "SELECT Equipment.EquipmentTypeID, Count(*) AS X from Equipment ";
				sql = sql + " INNER JOIN EquipmentTypes ON EquipmentTypes.EquipmentTypeID = Equipment.EquipmentTypeID ";
				sql = sql + " WHERE Equipment.DateCreated >= " + cutoff;
				sql = sql + " AND EquipmentTypes.MajorTypeID IN (1, 2, 3, 4, 5, 9, 10, 21, 22)";
				sql = sql + " GROUP BY Equipment.EquipmentTypeID ORDER BY X DESC";
				ResultSet rs = dbs.getResultSet(sql);
				HTMLFunctions.Heading(str_weps, 3, "Most Popular Weapons");
				HTMLFunctions.StartOrderedList(str_weps);
				while (rs.next()) {
					HTMLFunctions.AddListEntry(str_weps, EquipmentTypesTable.GetNameFromEquipmentType(dbs, rs.getInt("EquipmentTypeID")) + " (" + rs.getInt("X") + ")");
				}
				rs.close();
				HTMLFunctions.EndOrderedList(str_weps);
				
				miscstatspage.str_league.append(miscstatspage.str_weps);
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}

			// Specific mission stats
			HTMLFunctions.Heading(str_league, 3, "Missions Stats");

			HTMLFunctions.StartTable(str_league, "stats", "", 1, "", 5);
			HTMLFunctions.StartRow(str_league);
			HTMLFunctions.AddCellHeading(str_league, "Name");
			HTMLFunctions.AddCellHeading(str_league, "Total Games");
			//HTMLFunctions.AddCellHeading(str_league, "Best Player");
			HTMLFunctions.AddCellHeading(str_league, "Side 1 wins");
			HTMLFunctions.AddCellHeading(str_league, "Side 2 wins");
			HTMLFunctions.AddCellHeading(str_league, "Side 3 wins");
			HTMLFunctions.AddCellHeading(str_league, "Side 4 wins");
			HTMLFunctions.AddCellHeading(str_league, "Draws");
			HTMLFunctions.AddCellHeading(str_league, "Avg Turns");
			HTMLFunctions.AddCellHeading(str_league, "Notes");
			HTMLFunctions.EndRow(str_league);

			StringBuffer str_unplayed_missions = new StringBuffer();

			for (int j=0 ; j<AbstractMission.SF_MISSIONS.length ; j++) {
				int i = AbstractMission.SF_MISSIONS[j];
				if (AbstractMission.IsValidMission(i)) {
					showStatsForMission(str_league, i, str_unplayed_missions);
				}
			}
			HTMLFunctions.EndTable(str_league);


			// Missions no-one plays
			if (this.session.isLoggedIn()) {
				if (this.current_login.isAdmin()) {
					HTMLFunctions.Heading(str_league, 3, "Missions Not Played (in last " + DAYS + " days)");
					HTMLFunctions.StartOrderedList(str_league);
					str_league.append(str_unplayed_missions);
					HTMLFunctions.EndOrderedList(str_league);
				}
			}
		}
		str.append(str_league);

		this.body_html.append(MainLayout.GetHTML(this, "Miscellaneous Statistics", str));		
	}



	private void showStatsForMission(StringBuffer str, int missionid, StringBuffer str_unplayed_missions) throws SQLException {
		AbstractMission mission = AbstractMission.Factory(missionid);
		int s1 = dbs.getScalarAsInt("SELECT Count(*) FROM Games WHERE DateTurnStarted >= " + cutoff + " AND Mission = " + missionid + " AND WinningSide = 1");
		int s2 = dbs.getScalarAsInt("SELECT Count(*) FROM Games WHERE DateTurnStarted >= " + cutoff + " AND Mission = " + missionid + " AND WinningSide = 2");
		int s3 = dbs.getScalarAsInt("SELECT Count(*) FROM Games WHERE DateTurnStarted >= " + cutoff + " AND Mission = " + missionid + " AND WinningSide = 3");
		int s4 = dbs.getScalarAsInt("SELECT Count(*) FROM Games WHERE DateTurnStarted >= " + cutoff + " AND Mission = " + missionid + " AND WinningSide = 4");
		int draws = dbs.getScalarAsInt("SELECT Count(*) FROM Games WHERE DateTurnStarted >= " + cutoff + " AND Mission = " + missionid + " AND WinType IN (" + GamesTable.WIN_DRAW + ", " + GamesTable.WIN_DRAW_MUTUAL_CONCEDE + ")");
		int tot = s1 + s2 + s3 + s4 + draws;
		int at = dbs.getScalarAsInt("SELECT AVG(TurnNo) FROM Games WHERE DateTurnStarted >= " + cutoff + " AND Mission = " + missionid + " AND COALESCE(WinningSide, 0) <> 0");

		if (tot > 0) {
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "<nobr>" + AbstractMission.GetMissionNameFromType(missionid, true, false) + "</nobr>");
			HTMLFunctions.AddCell(str, tot + "");
			// Best player
			/*int best_player = MissionStatsTable.GetBestPlayerForMission(dbs, missionid);
			if (best_player > 0) {
				HTMLFunctions.AddCell(str, LoginsTable.GetDisplayName_Enc(dbs, best_player, true) + " (" + MissionStatsTable.GetTotalVictoriesForPlayerAndMission(dbs, missionid, best_player) + ")");
			} else {
				HTMLFunctions.AddCell(str, "None");
			}*/
			
			if (tot == 0) {
				tot  = 1; // Stop /0;
			}
			int tot_s1 = ((s1*100)/tot);
			int tot_s2 = ((s2*100)/tot);
			HTMLFunctions.AddCell(str, "<nobr>" + s1 + " (" + tot_s1 + "%)</nobr>");
			HTMLFunctions.AddCell(str, "<nobr>" + s2 + " (" + tot_s2 + "%)</nobr>");
			if (mission.getNumOfSides() >= 3) {
				HTMLFunctions.AddCell(str, "<nobr>" + s3 + " (" + ((s3*100)/tot) + "%)</nobr>");
			} else {
				HTMLFunctions.AddCell(str, "");
			}
			if (mission.getNumOfSides() >= 4) {
				HTMLFunctions.AddCell(str, "<nobr>" + s4 + " (" + ((s4*100)/tot) + "%)</nobr>");
			} else {
				HTMLFunctions.AddCell(str, "");
			}
			HTMLFunctions.AddCell(str, "<nobr>" + draws + " (" + ((draws*100)/tot) + "%)</nobr>");
			HTMLFunctions.AddCell(str, "" + at);
			String notes = "";
			if (tot >= 4 && (tot_s1 <= 20 || tot_s2 <= 20)) {
				notes = "Unbalanced?";
			}
			HTMLFunctions.AddCell(str, notes);
			HTMLFunctions.EndRow(str);
		} else {
			HTMLFunctions.AddListEntry(str_unplayed_missions, mission.mission_name);
		}
	}


}

