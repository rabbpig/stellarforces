package dsrwebserver.pages.dsr;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.html.HTMLFunctions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.GameLogTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;

/**
 * This page will either remove a player or concede the game if there is only two players.
 * if lid == 1, the currently logged in player will concede.
 */
public class cancelgame extends AbstractHTMLPage {

	public cancelgame() {
		super();
	}


	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();
		if (this.session.isLoggedIn()) {
			int gid = this.headers.getGetValueAsInt("gid");
			int lid = this.headers.getGetValueAsInt("lid");
			GamesTable game = new GamesTable(dbs);
			game.selectRow(gid);

			if (game.getGameStatus() < GamesTable.GS_FINISHED) {
				if (game.isPlayerInGame(this.current_login.getID()) || this.current_login.isAdmin()) {
					int conceding_player = game.getLoginIDOfWaitingForPlayer();
					if (lid == 1) { // Have we specified a particular player?
						conceding_player = this.current_login.getID();
					}
					int days = game.getDaysSinceTurnStarted();
					if (days >= game.getCancelDays() || conceding_player == this.current_login.getID() || lid > 0) {
						LoginsTable login = new LoginsTable(dbs);
						login.selectRow(conceding_player);
						if (game.getMissionID() != AbstractMission.SF_PRACTISE_MISSION && game.getMissionID() != AbstractMission.SF_PRACTISE_MISSION_WITH_AI) {
							game.setSideHasConceded(GamesTable.GetSideFromPlayerID(dbs, game.getID(), conceding_player));
							login.addConcede();
						}
						ForceConcedeGame(dbs, game, conceding_player);
						if (game.getNumOfSides() == 1) {
							try {
								//game.conceedGame(game.getLoginIDFromSide(1));
								HTMLFunctions.Para(str, "That game has been ended.  Want to <a href=\"GameRequests.cls\">start another game?</a>");
								//GameLogTable.AddRec(dbs, game, -1, -1, "This game has been ended", true, System.currentTimeMillis());
							} catch (Exception ex) {
								DSRWebServer.HandleError(ex, true);
							}
						} else if (game.getNumOfSides() == 2) {
							// Don't send an email as winners/losers get sent an email anyway.
							//int winner = game.getOppositeSidesLoginIDs(conceding_player)[1];
							//game.conceedGame(winner);
							HTMLFunctions.Para(str, "That game has been ended.  Want to <a href=\"GameRequests.cls\">start another game?</a>");
							//GameLogTable.AddRec(dbs, game, -1, -1, "This game has been conceded.  " + LoginsTable.GetDisplayName_Enc(dbs, winner, false) + " has won.", true, System.currentTimeMillis());
						} else { // More than 2 players
							/*game.removePlayersUnits(conceding_player);
							if (game.getGameStatus() == GamesTable.GS_CREATED_DEPLOYMENT) {
								game.setSideHasEquipped(GamesTable.GetSideFromPlayerID(dbs, game.getID(), conceding_player));
								game.setSideHasDeployed(GamesTable.GetSideFromPlayerID(dbs, game.getID(), conceding_player), System.currentTimeMillis());
							} else {
								game.endOfTurn(System.currentTimeMillis());
								game.startOfTurn(System.currentTimeMillis());
							}*/
							//GameLogTable.AddRec(dbs, game, -1, -1, LoginsTable.GetDisplayName_Enc(dbs, conceding_player, false) + " has conceded the game.", true, System.currentTimeMillis());
							if (conceding_player != this.current_login.getID()) {
								//login.sendEmail("Game Cancel", "I'm afraid your units have been removed in your game against " + game.getOpponentsNamesByLoginID(conceding_player, false) + " since it has been at least " + game.getCancelDays() + " days since you took your turn.");
								HTMLFunctions.Para(str, "That player's units have been removed from the game.");
							} else {
								HTMLFunctions.Para(str, "Your units have been removed from the game.  Want to <a href=\"GameRequests.cls\">start another game</a>?");
							}
						}
						login.close();
					} else {
						HTMLFunctions.Para(str, "You cannot cancel this game before " + game.getCancelDays() + " days of waiting.");
					}
				} else {
					HTMLFunctions.Para(str, "You are not waiting for your opponent, they are waiting for you!");
				}
			} else {
				HTMLFunctions.Para(str, "That game has been finished.");
			}

		}
		this.body_html.append(MainLayout.GetHTML(this, "Player Removed", str));		

	}


	public static void ForceConcedeGame(MySQLConnection dbs, GamesTable game, int conceding_player) throws SQLException {
		//int conceding_player = game.getLoginIDOfWaitingForPlayer();
		if (game.getNumOfSides() == 1) {
			// This should never happen as they get deleted automatically.
			try {
				game.conceedGame(game.getLoginIDFromSide(1));
				//HTMLFunctions.Para(str, "That game has been ended.  Want to <a href=\"GameRequests.cls\">start another game?</a>");
				GameLogTable.AddRec(dbs, game, -1, -1, "This game has been ended", true, System.currentTimeMillis());
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}
		} else if (game.getNumOfSides() == 2) {
			// Don't send an email as winners/losers get sent an email anyway.
			int winner = game.getOppositeSidesLoginIDs(conceding_player)[1];
			game.conceedGame(winner);
			//HTMLFunctions.Para(str, "That game has been ended.  Want to <a href=\"GameRequests.cls\">start another game?</a>");
			GameLogTable.AddRec(dbs, game, -1, -1, "This game has been conceded.  " + LoginsTable.GetDisplayName_Enc(dbs, winner, false) + " has won.", true, System.currentTimeMillis());
		} else { // More than 2 players
			game.removePlayersUnits(conceding_player);
			if (game.getGameStatus() == GamesTable.GS_CREATED_DEPLOYMENT) {
				// Mark as equipped/deployed so the game can continue
				game.setSideHasEquipped(GamesTable.GetSideFromPlayerID(dbs, game.getID(), conceding_player));
				game.setSideHasDeployed(GamesTable.GetSideFromPlayerID(dbs, game.getID(), conceding_player), System.currentTimeMillis());
			}
			if (game.getTurnSide() == GamesTable.GetSideFromPlayerID(dbs, game.getID(), conceding_player)) {
				game.endOfTurn(System.currentTimeMillis(), true);
				game.startOfTurn(System.currentTimeMillis());
			}
			GameLogTable.AddRec(dbs, game, -1, -1, LoginsTable.GetDisplayName_Enc(dbs, conceding_player, false) + " has conceded the game.", true, System.currentTimeMillis());
			//if (conceding_player != this.current_login.getID()) {
			LoginsTable login = new LoginsTable(dbs);
			login.selectRow(conceding_player);
			login.sendEmail("Game Conceded", "I'm afraid your units have been removed in your game against " + game.getOpponentsNamesByLoginID(conceding_player, false) + " since it has been at least " + game.getCancelDays() + " days since you took your turn.");
			login.close();
			//HTMLFunctions.Para(str, "That player's units have been removed from the game.");
			//} else {
			//HTMLFunctions.Para(str, "Your units have been removed from the game.  Want to <a href=\"GameRequests.cls\">start another game?</a>");
			//}
		}

	}

}
