package dsrwebserver.pages.dsr;

import java.sql.ResultSet;

import ssmith.dbs.MySQLConnection;
import ssmith.html.HTMLFunctions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.FactionsTable;
import dsrwebserver.tables.LoginsTable;

public class factions extends AbstractHTMLPage {

	public static String DATE_DIFF = "DATEDIFF(Now(), LastLoginDate) < " + FactionsTable.EXPIRY_DAYS;


	public factions() {
		super();
	}


	@Override
	public void process() throws Exception {
			StringBuffer str = new StringBuffer();

			int fid = 0;
			if (this.session.isLoggedIn()) {
				int add = headers.getGetValueAsInt("add");
				if (add == 1 && this.current_login.getFactionID() <= 0) {
					int worst_fid = this.current_login.joinFaction();
					HTMLFunctions.Heading(str, 2, "You have joined the " + FactionsTable.GetName(dbs, worst_fid) + "!");
				}

				fid = headers.getGetValueAsInt("fid");
				if (fid <= 0) {
					fid = this.current_login.getFactionID(); 
				}
			}

			int fid2 = -1;
			if (this.session.isLoggedIn()) {
				fid2 = this.current_login.getFactionID();
			}
			str.append(GetFactionStuff(dbs, fid2)); // fid)); //

			HTMLFunctions.Heading(str, 1, FactionsTable.GetName(dbs, fid));

			str.append("<img src=\"" + DSRWebServer.IMAGES_HOST + "/images/factions/" + FactionsTable.GetImageFilename(dbs, fid) + "\" width=300 align=\"right\" />");

			HTMLFunctions.Para(str, GetText(FactionsTable.GetName(dbs, fid)));

			HTMLFunctions.Para(str, "The following players are in this faction:-");
			// Only show players who have logged in recently
			ResultSet rs = dbs.getResultSet("SELECT * FROM Logins WHERE FactionID = " + fid + " AND " + DATE_DIFF + " ORDER BY DisplayName");
			HTMLFunctions.StartTable(str, "stats", "", 1, "", 5);
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCellHeading(str, "Name");
			HTMLFunctions.EndRow(str);

			while (rs.next()) {
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginID"), true));
				HTMLFunctions.EndRow(str);
			}
			HTMLFunctions.EndTable(str);

			/*HTMLFunctions.Heading(str, 2, "How do I gain points for my faction?");
				HTMLFunctions.Para(str, "There are many ways to gain points that will be added to your faction's points total:");

				HTMLFunctions.Heading(str, 3, "* Win Missions");
				HTMLFunctions.Para(str, "For every mission you have won in the last " + DAYS +  " days you will gain " + leaguetable.WIN_POINTS + " points for your faction.  (Not including campaign missions).");

				HTMLFunctions.Heading(str, 3, "* Be the Best Player at a Mission");
				HTMLFunctions.Para(str, "The best player of a mission is the player that has won that mission the most times in the last " + DAYS + " days, not including campaign games.  For each mission you are the best player, you will gain " + BEST_AT_MISSION_POINTS + " points for your faction.  The current best player is shown on the mission descriptions pages.");

				HTMLFunctions.Heading(str, 3, "* Gain Medals");
				HTMLFunctions.Para(str, "Medals each have a points value, and the total of all your medal's are added to your faction's total.  The points are as follows:-");

				HTMLFunctions.StartTable(str, "stats", "", 1, "", 5);
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCellHeading(str, "Trophy");
				HTMLFunctions.AddCellHeading(str, "Points");
				HTMLFunctions.EndRow(str);

				for (int i=1 ; i<= PlayerTrophiesTable.MAX_TROPHIES ;i++) {
					HTMLFunctions.StartRow(str);
					HTMLFunctions.AddCell(str, PlayerTrophiesTable.GetTrophyName(i));
					HTMLFunctions.AddCell(str, PlayerTrophiesTable.GetFactionPointsForTrophy(i) + "");
					HTMLFunctions.EndRow(str);
				}
				HTMLFunctions.EndTable(str);*/
			/*} else {
			HTMLFunctions.Heading(str, 3, "Sorry, you are not currently in a faction.  This feature is still being developed and eligable players are being added slowly.");
		}*/

			this.body_html.append(MainLayout.GetHTML(this, "Factions", str));
	}


	public static StringBuffer GetFactionStuff(MySQLConnection dbs, int highlight_faction) {
		//if (str_factions_int.hitInterval() || str_factions == null || AppletMain.DEBUG) {
		StringBuffer str = new StringBuffer();

		try {
			HTMLFunctions.StartTable(str, "stats", "", 1, "", 5);
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCellHeading(str, "Faction");
			HTMLFunctions.AddCellHeading(str, "Sectors");
			HTMLFunctions.EndRow(str);

			ResultSet rs_factions = dbs.getResultSet("SELECT * FROM Factions ORDER BY Sectors DESC");
			while (rs_factions.next()) {
				String s = "<a href=\"/dsr/factions.cls?fid=" + rs_factions.getInt("FactionID") + "\">" + HTMLFunctions.s2HTML(rs_factions.getString("Name")) + "</a>";
				if (highlight_faction == rs_factions.getInt("FactionID")) {
					s = s + " - (your faction)";
				}
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, s);
				HTMLFunctions.AddCell(str, "" + rs_factions.getInt("Sectors"));
				HTMLFunctions.EndRow(str);
			}
			HTMLFunctions.EndTable(str);

		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

		return str;
	}


	public static String GetText(String name) {
		if (name.toLowerCase().indexOf("marsec") >= 0) {
			return "The Marsec corporation manufactures the best weapons in the galaxy, but its boss, Sterner Regnix, uses unsavoury methods to extract the best from his top scientists. The use of mind control drugs and cybernetic implants is widely used, but officially denied by Marsec.";
		} else if (name.toLowerCase().indexOf("omni") >= 0) {
			return "The Omni Corporation's hold security information on the 30 billion population of galaxy sector nine. Somewhere in sector nine is the rebel star system, the infamous 'Rebelstar', which still remains a secret from all its enemies. The Omni Corporation represents the biggest threat to the rebels so far, with an increasing amount of data on rebel movements.";
		} else if (name.toLowerCase().indexOf("metallix") >= 0) {
			return "The Metallix corporation specializie in mining precious metals. They have heavily fortified installations on countless moons and planets throughout the galaxy. The increasingly forceful tactics they use to expand their operations see them regularly come into conflict with other factions.";
		} else if (name.toLowerCase().indexOf("globex") >= 0) {
			return "The Globex Corporation dominate the galactic trade markets in all high tech appliances. It is unconfirmed whether their boss, 'Scorpio' still lives, but whether he is alive or not Globex Corporation have not slowed in their relentless drive for ultimate galactic dominance. Their divisions which specialize in destructive interplanetary weather control and bacteriological warfare are currently on the agressive.";
		} else if (name.toLowerCase().indexOf("laser squad") >= 0) {
			return "Laser Squad are a relatively small, but well established group of elite guns for hire. Recently suspicions have developed which suggest that Laser Squad are in league with the rebels. For this reason they have now become distrusted outcasts. Whether this is indeed true or not remains unconfirmed.";
		} else if (name.toLowerCase().indexOf("red shift") >= 0) {
			return "Red Shift are the Rebel's primary elite military division. Their leader and spokesman Joe Capricorn claims they fight for galactic freedom, their aim to end the greed of the warring factions. Their 'Rebelstar' somehow remains mysteriously hidden. Rumours suggesting the Rebels are using unsavoury cloning methods to bolster their numbers, and that their leader Joe Capricorn has been cloned many times over has only added to the determination of those factions loyal to the Federation to put a stop to the rebellion.";
		} else {
			return "";
		}

	}


}
