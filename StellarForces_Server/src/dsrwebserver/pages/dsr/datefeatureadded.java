package dsrwebserver.pages.dsr;


public class datefeatureadded {/*extends AbstractHTMLPage {

	public datefeatureadded() {
		super();
	}

	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		HTMLFunctions.StartTable(str, 10);
		HTMLFunctions.StartRow(str);
		HTMLFunctions.StartCell(str);
		
		HTMLFunctions.Heading(str, 3, "Game Updates");

		HTMLFunctions.StartUnorderedList(str);

		HTMLFunctions.AddListEntry(str, "13/3/12 - New item: Proximity Scanner, for use the The Stardrive.");
		HTMLFunctions.AddListEntry(str, "13/3/12 - Banned items for each mission are now shown on the mission description page.");
		HTMLFunctions.AddListEntry(str, "29/2/12 - It now costs creds to buy extra campaign units.");
		HTMLFunctions.AddListEntry(str, "29/2/12 - Players awards are now shown on the player's public page.");
		HTMLFunctions.AddListEntry(str, "22/2/12 - In campaign mode, an attack is cancelled if the attacker has no units spare when the defender joins.");
		HTMLFunctions.AddListEntry(str, "3/2/12 - <a href=\"/dsr/datefeatureadded.cls#client\">New version of the client released</a>.");
		HTMLFunctions.AddListEntry(str, "31/1/12 - Computers are now not shown on the strategic scanner or intermediate playback for SNAFU missions.");
		HTMLFunctions.AddListEntry(str, "24/1/12 - New version of the client released.");
		HTMLFunctions.AddListEntry(str, "19/1/12 - In The Exterminators, the assassins now have only 500 credits, down from 550.");
		HTMLFunctions.AddListEntry(str, "19/1/12 - In <a href=\"/dsr/missiondescriptions.cls?type=21\">3-Player The Assassins</a>, increase the fugitives creds from 180 to 200.");
		HTMLFunctions.AddListEntry(str, "16/1/12 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=69\">All Seeing Eye</a>.");
		HTMLFunctions.AddListEntry(str, "16/1/12 - Campaign units are only refreshed when you are <i>not</i> fighting in a mission.");
		HTMLFunctions.AddListEntry(str, "13/1/12 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=68\">Capture the FLAG - Close Combat Only</a>.");
		HTMLFunctions.AddListEntry(str, "12/1/12 - You can now defend in multiple campaign mission.");
		HTMLFunctions.AddListEntry(str, "11/1/12 - When selecting units for a campaign mission, you are limited to the number of deployment squares available.");
		HTMLFunctions.AddListEntry(str, "10/1/12 - Can now view the players in the other factions.");
		HTMLFunctions.AddListEntry(str, "9/1/12 - Now shows the total weight of armour and equipment when equipping units.");
		HTMLFunctions.AddListEntry(str, "9/1/12 - Renamed 'Level This' to 'Scorched Earth'.");
		HTMLFunctions.AddListEntry(str, "6/1/12 - Improved the method for selecting units for campaign missions, and the player can choose how many units.");
		HTMLFunctions.AddListEntry(str, "5/1/12 - In <a href=\"/dsr/missiondescriptions.cls?type=6\">The Alien Hordes</a> the aliens now have 16 units.");
		HTMLFunctions.AddListEntry(str, "4/1/12 - In <a href=\"/dsr/missiondescriptions.cls?type=66\">Hideout</a> increased the attackers credits from 200 to 280.");
		HTMLFunctions.AddListEntry(str, "28/12/11 - In <a href=\"/dsr/missiondescriptions.cls?type=23\">Meltdown</a> increased the attackers credits from 320 to 380.");
		HTMLFunctions.AddListEntry(str, "28/12/11 - In <a href=\"/dsr/missiondescriptions.cls?type=7\">Rescue from the Mines</a> increased the rescuers credits from 230 to 300.");
		HTMLFunctions.AddListEntry(str, "18/12/11 - Changed the map in <a href=\"/dsr/missiondescriptions.cls?type=31\">Moonbase Assault 4-Player</a> to make it fairer.");
		HTMLFunctions.AddListEntry(str, "17/12/11 - In <a href=\"/dsr/missiondescriptions.cls?type=67\">The Stardrive</a>, the Stardrive starts in the hands of the defenders.");
		HTMLFunctions.AddListEntry(str, "15/12/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=67\">The Stardrive</a>, created by Deadlime.");
		HTMLFunctions.AddListEntry(str, "14/12/11 - Added Galaxy Map log.");
		HTMLFunctions.AddListEntry(str, "12/12/11 - Smoke and nerve gas is now logged for the game playback.");
		HTMLFunctions.AddListEntry(str, "9/12/11 - <a href=\"/dsr/factionhelp.cls\">Factions</a>");
		HTMLFunctions.AddListEntry(str, "3/12/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=66\">Hideout</a>.");
		HTMLFunctions.AddListEntry(str, "2/12/11 - Unit stat increases are allocated randomly in campaign missions");
		HTMLFunctions.AddListEntry(str, "2/12/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=65\">Superhuman: Moonbase Assault</a>.");
		HTMLFunctions.AddListEntry(str, "2/12/11 - Players can now award an award to other players.");
		HTMLFunctions.AddListEntry(str, "2/12/11 - Any creds not spent in a campaign mission are saved up and added to the next one.");
		HTMLFunctions.AddListEntry(str, "30/11/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=66\">Hideout</a>.");
		HTMLFunctions.AddListEntry(str, "29/11/11 - Any creds not spent in a campaign mission are saved up and added to the next one.");
		HTMLFunctions.AddListEntry(str, "29/11/11 - Unit stat increases are allocated randomly in campaign missions");
		HTMLFunctions.AddListEntry(str, "29/11/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=65\">Superhuman: Moonbase Assault</a>.");
		HTMLFunctions.AddListEntry(str, "29/11/11 - Players can now award an award to other players.");
		HTMLFunctions.AddListEntry(str, "12/8/11 - Smoke & Nerve Gas waits a turn before dissipating.");
		HTMLFunctions.AddListEntry(str, "3/8/11 - It's now possible to add 'holiday text' to your settings if you're going to be away for a while.");
		HTMLFunctions.AddListEntry(str, "3/8/11 - In <a href=\"/dsr/missiondescriptions.cls?type=58\">Denunciation 2v2</a> the Ambassadors start further back.");
		HTMLFunctions.AddListEntry(str, "3/8/11 - Players incur 2 points of inactivity when not playing a campaign game.");
		HTMLFunctions.AddListEntry(str, "16/7/11 - In Rescue from the Mines, the hostage takers now cannot buy grenades.");
		HTMLFunctions.AddListEntry(str, "14/7/11 - Marines now have only 350 credits in <a href=\"/dsr/missiondescriptions.cls?type=62\">Aliens</a>.");
		HTMLFunctions.AddListEntry(str, "6/7/11 - Added more deployment areas for the Laser Squad in Rescue from the Mines.");
		HTMLFunctions.AddListEntry(str, "5/7/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=29\">Sterner's Revenege</a>, created by PCasaca.");
		HTMLFunctions.AddListEntry(str, "5/7/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=64\">Laboratory Attack 2v2</a>.");
		HTMLFunctions.AddListEntry(str, "5/7/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=63\">Escort 2v2</a>.");
		HTMLFunctions.AddListEntry(str, "4/7/11 - There is now a simple 'Message Comrade' option in 2v2 games.");
		HTMLFunctions.AddListEntry(str, "20/6/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=5\">Moonbase Assault 2v2</a>.");
		HTMLFunctions.AddListEntry(str, "17/6/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=62\">Aliens</a>, created by Deadlime.");
		HTMLFunctions.AddListEntry(str, "17/6/11 - Power Swords are now banned in the <a href=\"/dsr/missiondescriptions.cls?type=52\">Alien</a> mission.");
		HTMLFunctions.AddListEntry(str, "8/6/11 - Player who are not playing a campaign mission or have no open campaign game requests will accumulate an inactivity point for each day they are inactive, which are subtracted from the players campaign points total.");
		HTMLFunctions.AddListEntry(str, "26/5/11 - Tweaked mission 'Assassins 2v2', so it's now 3 & 3 against 2 & 5 units.");
		HTMLFunctions.AddListEntry(str, "26/5/11 - A game creators name is now hidden in campaign missions.");
		HTMLFunctions.AddListEntry(str, "26/5/11 - New Trophy: Punctured Heart, for losing 40 missions, suggested by Deadlime.");
		HTMLFunctions.AddListEntry(str, "12/5/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=38\">Scorched Earth</a>.");
		HTMLFunctions.AddListEntry(str, "12/5/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=61\">The Assassins 2v2</a>.");
		HTMLFunctions.AddListEntry(str, "11/5/11 - In 2v2 missions, you can now see the location of enemy units seen or heard by your comrades.");
		HTMLFunctions.AddListEntry(str, "9/5/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=60\">Terrors of the Deep</a>, created by Deadlime.");
		HTMLFunctions.AddListEntry(str, "9/5/11 - Changed 'The Assassins - Outhouses' so the assassins now have 200 creds.");
		HTMLFunctions.AddListEntry(str, "27/4/11 - Added a rudimentary AI and enemies in the practise mission.");
		HTMLFunctions.AddListEntry(str, "24/4/11 - Smoke dissipation speed is now based on the number of sides.");
		HTMLFunctions.AddListEntry(str, "20/4/11 - Opponents can now be restricted when creating a game.");
		HTMLFunctions.AddListEntry(str, "12/4/11 - Points for victory are now 3 + 'the difference in league points between you and your opponent divided by 10'.  For example, if you have 10 league points and your opponent has 30, you will get 5 for a victory and they will get 3.");
		HTMLFunctions.AddListEntry(str, "11/4/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=58\">Denunciation 2v2</a>, created by Deadlime.");
		HTMLFunctions.AddListEntry(str, "11/4/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=59\">A Tale of Two Bases 2v2</a>, created by Deadlime.");
		HTMLFunctions.AddListEntry(str, "7/4/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=57\">The Blob Assassins</a>");
		HTMLFunctions.AddListEntry(str, "7/4/11 - New Trophy: Winners Trophy, for winning 50 missions.");
		HTMLFunctions.AddListEntry(str, "5/4/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=57\">Oddworld</a>, created by PCasaca.");
		HTMLFunctions.AddListEntry(str, "4/4/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=56\">The Big Push</a>, created by Boyflea (Dave Gumble).");
		HTMLFunctions.AddListEntry(str, "17/3/11 - Credits can be adjusted when creating a mission.");
		HTMLFunctions.AddListEntry(str, "17/3/11 - Comments can be added against game requests.");
		HTMLFunctions.AddListEntry(str, "16/3/11 - There are now 3 different types of wall: indestructible, strong and weak.");
		HTMLFunctions.AddListEntry(str, "16/3/11 - Boyflea (Dave Gumble) has improved all the mission descriptions.");
		HTMLFunctions.AddListEntry(str, "15/3/11 - Added medal images provided by Deadlime.");
		//HTMLFunctions.AddListEntry(str, "15/3/11 - Thanks to Boyflea (Dave Gumble), the missions are now grouped into campaigns.");
		HTMLFunctions.AddListEntry(str, "9/3/11 - Smoke now slowly drifts.");
		HTMLFunctions.AddListEntry(str, "9/3/11 - Smoke grenades and nerve gas now only cost 7.");
		HTMLFunctions.AddListEntry(str, "8/3/11 - New Trophy: First Blood, for winning your first mission.");
		HTMLFunctions.AddListEntry(str, "7/3/11 - Smoke now disipates at 20% chance per turn per square.");
		HTMLFunctions.AddListEntry(str, "2/3/11 - In mission Kill Frenzy, players lose VPs for killing their own units.");
		HTMLFunctions.AddListEntry(str, "24/2/11 - You can now enter new player's email addresses in the opponent's field when creating a game.");
		HTMLFunctions.AddListEntry(str, "24/2/11 - Trophies now shown on player's public page.");
		HTMLFunctions.AddListEntry(str, "22/2/11 - Deadlime has kindly modified the Denunciation map.");
		HTMLFunctions.AddListEntry(str, "21/2/11 - Now awards the Long Service Medal and the Honourary Laser Squad Medal.");
		HTMLFunctions.AddListEntry(str, "21/2/11 - In mission 'A Tale of Two Bases', the map layout has been changed to use that of the campaign version.");
		HTMLFunctions.AddListEntry(str, "19/2/11 - Reduced all grenade's power by 10 and increased their cost by 1.");
		HTMLFunctions.AddListEntry(str, "19/2/11 - Increased all weapon's shot accuracy by 5.");
		HTMLFunctions.AddListEntry(str, "17/2/11 - In mission 'Egg Collector', the number of turns is now 30.");
		HTMLFunctions.AddListEntry(str, "16/2/11 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=55\">Kill Frenzy 4-Player</a>.");
		HTMLFunctions.AddListEntry(str, "4/2/11 - In mission 'Egg Hunt', the alien side now has 11 aliens (up from 9) but can no longer impregnate humans.");
		HTMLFunctions.AddListEntry(str, "4/2/11 - Morale is now affected when a unit is wounded and when an undercover agent is activated.");
		HTMLFunctions.AddListEntry(str, "1/2/11 - <b>Units have another new stat: Morale.  This is only used in advanced games and affects units on both sides when another unit dies.</b>");
		HTMLFunctions.AddListEntry(str, "1/2/11 - Added new statistics to a player's public page.");
		HTMLFunctions.AddListEntry(str, "31/1/11 - <b>Units have a new stat: Energy.  This is only used in advanced games.</b>");
		HTMLFunctions.AddListEntry(str, "28/1/11 - Deadlime has tweaked 'The Assassins - Campaign Version' Map and both sides have 320 creds.");
		HTMLFunctions.AddListEntry(str, "27/1/11 - Simplified the league table.");
		HTMLFunctions.AddListEntry(str, "20/1/11 - Have moved the Playback links to the strategic scanner pages.");
		HTMLFunctions.AddListEntry(str, "17/1/11 - It's now no longer possible to delete game requests (<a href=\"http://forums.stellarforces.com/viewtopic.php?f=7&t=80\">reason here</a>) except for campaign missions.");
		HTMLFunctions.AddListEntry(str, "17/1/11 - In Defend the Base, The attackers now have 7 units instead of 8, and are no longer allowed grenades or rockets.");
		HTMLFunctions.AddListEntry(str, "17/1/11 - In Moonbase Assault: Undercover Agent, the attackers start with 10 units and the defenders 6, but 2 attackers are now undercover agents.");
		HTMLFunctions.AddListEntry(str, "14/1/11 - <b>It's now possible to view a playback of a current game up to its current point.</b>");
		HTMLFunctions.AddListEntry(str, "14/1/11 - You can now change you password on the Settings page.");
		HTMLFunctions.AddListEntry(str, "12/1/11 valign=\"top\" - New mission: <a href=\"/dsr/missiondescriptions.cls?type=54\">Moonbase Assault: Undercover Agent</a>.");
		HTMLFunctions.AddListEntry(str, "11/1/11 - In Alien, the Queen now has only 75 APs and the Nostomo crew have 45 creds.");
		HTMLFunctions.AddListEntry(str, "11/1/11 - In SNAFU: Sabotage, the VPs for killing a saboteur has been increased to 25 (and 60 for the last one).");
		HTMLFunctions.AddListEntry(str, "9/1/11 - In SNAFU: Moonbase Assault, the VPs for killing a saboteur has been increased to 25 (and 60 for the last one) and the VPs for destroying computers has been reduced to 5.");
		HTMLFunctions.AddListEntry(str, "7/1/11 - Moved the Ambassador's deployment squares in Denunciation.");
		HTMLFunctions.AddListEntry(str, "5/1/11 - In Escort 2, the fugitives now have 250 credits, up from 220.");
		HTMLFunctions.AddListEntry(str, "4/1/11 - In Alien Containment, the aliens have one less unit and the two human sides have one extra unit each.");
		HTMLFunctions.AddListEntry(str, "4/1/11 - In Moonbase Assault: Campaign Version, the attackers credits have been reduced from 280 to 250.");
		HTMLFunctions.AddListEntry(str, "4/1/11 - In The Exterminators, the defenders now have 250 credits, up from 200.");
		HTMLFunctions.AddListEntry(str, "4/1/11 - In Snafu: Moonbase Assault, Snafu: The Assassins and Snafu: Sabotage, the assassins/saboteurs must get 100 VPs to win, otherwise the highest VPs out of the other players will win.");
		HTMLFunctions.AddListEntry(str, "4/1/11 - You can now 'rewind' the playback of a mission.");
		HTMLFunctions.AddListEntry(str, "3/1/11 - In Prison Riot and Prison Mutiny, the deployment squares have been modified.");
		HTMLFunctions.AddListEntry(str, "3/1/11 - In Denunciation, both sides now have 8 units.");
		HTMLFunctions.AddListEntry(str, "3/1/11 - In SNAFU: Moonbase Assault, all sides now have 4 units.");
		HTMLFunctions.AddListEntry(str, "3/1/11 - In Defend the Base, both sides now have 8 units.");
		HTMLFunctions.AddListEntry(str, "17/12/10 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=53\">Denunciation</a>, created by Deadlime (Linden Clarke).");
		HTMLFunctions.AddListEntry(str, "16/12/10 - When equipping units, unit stats are highlighted based on their level.");
		HTMLFunctions.AddListEntry(str, "15/12/10 - Rocket Launchers can now </i>not</i> be used for opportunity fire.");
		HTMLFunctions.AddListEntry(str, "15/12/10 - Adrenalin Shots are no longer available.");
		HTMLFunctions.AddListEntry(str, "13/12/10 - Unit stats are shown when equipping units.");
		HTMLFunctions.AddListEntry(str, "13/12/10 - A view of the mission map is available when viewing mission details.");
		HTMLFunctions.AddListEntry(str, "9/12/10 - An email is now automatically sent to opponents when a player proposes mutually conceding a game, and it is also added to the game log.");
		HTMLFunctions.AddListEntry(str, "8/12/10 - The Alien Queen can hear 10 squares.");
		HTMLFunctions.AddListEntry(str, "6/12/10 - Emails are now <i>not</i> sent when an opponent has equipped or deployed.");
		HTMLFunctions.AddListEntry(str, "5/12/10 - It's now possible to concede Practise games at any time.");
		HTMLFunctions.AddListEntry(str, "2/12/10 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=52\">Alien</a>, created by Deadlime (Linden Clarke).");
		HTMLFunctions.AddListEntry(str, "1/12/10 - In SNAFU Assassins missions, the number of targets elimited is now shown in the details page.");
		HTMLFunctions.AddListEntry(str, "1/12/10 - Mission playback now has a 'slowmo' option.");
		HTMLFunctions.AddListEntry(str, "29/11/10 - Added missing data to old missions to make for a smoother playback.");
		HTMLFunctions.AddListEntry(str, "26/11/10 - Created a new Las-Rifle weapon.");
		HTMLFunctions.AddListEntry(str, "26/11/10 - Computers and explosions and shooting are now shown in the mission playback.");
		HTMLFunctions.AddListEntry(str, "25/11/10 - You can now playback mission in the strategic scanner.  See the 'Playback' link in <a href=\"/dsr/finishedgames.cls\">My Finished Games</a>.");
		HTMLFunctions.AddListEntry(str, "25/11/10 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=51\">SNAFU: Sabotage</a>.");
		HTMLFunctions.AddListEntry(str, "25/11/10 - Opposing unit's deployment areas are now identified by side.");
		HTMLFunctions.AddListEntry(str, "23/11/10 - Have revamped <a href=\"/dsr/missiondescriptions.cls?type=50\">SNAFU: The Assassins 2</a>: New map and new VP conditions.");
		HTMLFunctions.AddListEntry(str, "22/11/10 - Games can now be 'locked' to multiple opponents.");
		HTMLFunctions.AddListEntry(str, "19/11/10 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=50\">SNAFU: The Assassins 2</a>.");
		HTMLFunctions.AddListEntry(str, "18/11/10 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=49\">Sniper's Paradise</a>, created by PCasaca.");
		HTMLFunctions.AddListEntry(str, "18/11/10 - When a game is over, which player killed which unit is displayed.");	
		HTMLFunctions.AddListEntry(str, "17/11/10 - Now shows campaign unit's stats on the campaign page.");	
		HTMLFunctions.AddListEntry(str, "15/11/10 - F.L.A.G.s and Eggs are always shown on the scanner when playing in advanced mode.");	
		HTMLFunctions.AddListEntry(str, "12/11/10 - Have added the Alien Queen to the <a href=\"/dsr/missiondescriptions.cls?type=47\">Egg Hunt</a> mission.");
		HTMLFunctions.AddListEntry(str, "11/11/10 - Have adjusted the costs of a few weapons.");
		HTMLFunctions.AddListEntry(str, "8/11/10 - In 3+ player games, if a unit is heard, their side is not revealed in the Strategic Scanner.");
		HTMLFunctions.AddListEntry(str, "8/11/10 - Multiple campaign mission requests can now be created.");
		HTMLFunctions.AddListEntry(str, "5/11/10 - In Advanced Mode, dead bodies are not shown on the strategic scanner.");
		HTMLFunctions.AddListEntry(str, "4/11/10 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=48\">Egg Collector</a>.");
		HTMLFunctions.AddListEntry(str, "4/11/10 - SNAFU missions are now always played in Advanced mode.");
		HTMLFunctions.AddListEntry(str, "3/11/10 - Slightly altered the VPs for the two SNAFU missions: the defenders will get 13 VPs for all but the last assassin/saboteur units killed, and 50 VPs for the last remaining unit.");
		HTMLFunctions.AddListEntry(str, "2/11/10 - Reduce units to 6 per side in <a href=\"/dsr/missiondescriptions.cls?type=44\">Operation SNAFU: Moonbase Assault</a>");
		HTMLFunctions.AddListEntry(str, "1/11/10 - Players can now created games, equip and deploy all before an opponent has accepted their game.");
		HTMLFunctions.AddListEntry(str, "1/11/10 - Add new <a href=\"/dsr/miscstatspage.cls\">Top 10 Weapons</a> table.");
		HTMLFunctions.AddListEntry(str, "28/10/10 - Added the first <a href=\"/dsr/videotutorials.cls\">tutorial video</a>.");
		HTMLFunctions.AddListEntry(str, "28/10/10 - Open doors are drawn as open in the strategic scanner.");
		HTMLFunctions.AddListEntry(str, "26/10/10 - New mission created by Deadlime (Linden Clarke): <a href=\"/dsr/missiondescriptions.cls?type=47\">Egg Hunt</a>.");
		HTMLFunctions.AddListEntry(str, "25/10/10 - Units are now automatically holding their first piece of equipment.");
		HTMLFunctions.AddListEntry(str, "19/10/10 - Created some new <a href=\"/dsr/videopage.cls\">gameplay videos</a>.");
		HTMLFunctions.AddListEntry(str, "18/10/10 - Added a new outhouse to <a href=\"/dsr/missiondescriptions.cls?type=3\">The Assassins - Outhouses</a>.");
		HTMLFunctions.AddListEntry(str, "14/10/10 - Second <a href=\"http://stellarforces.blogspot.com/2010/09/whole-new-idea-for-missions.html\">SNAFU</a> mission: <a href=\"/dsr/missiondescriptions.cls?type=46\">Operation SNAFU: The Assassins</a>.");
		HTMLFunctions.AddListEntry(str, "13/10/10 - A new version of the Map Editor has been released.  This now enabled scenery to be added.");
		HTMLFunctions.AddListEntry(str, "13/10/10 - Changed the Last Man Standing missions so the side with the most VPs when the turns expire wins.");
		HTMLFunctions.AddListEntry(str, "12/10/10 - Thanks to Hardwired for creating a new image for the <a href=\"/dsr/missiondescriptions.cls?type=42\">Alien Containment</a> mission page.");
		HTMLFunctions.AddListEntry(str, "12/10/10 - I've added myself (Steve Smith) to the league tables.");
		HTMLFunctions.AddListEntry(str, "08/10/10 - A players public page now shows who their arch-nemesis is.");
		HTMLFunctions.AddListEntry(str, "08/10/10 - The mission descriptions now show how many completed games are required to access a mission.");
		HTMLFunctions.AddListEntry(str, "07/10/10 - The forums now have RSS feeds.");
		HTMLFunctions.AddListEntry(str, "06/10/10 - Have implemented a Practise Mission for new players to get used to the controls and gameplay.");
		HTMLFunctions.AddListEntry(str, "06/10/10 - In <a href=\"/dsr/missiondescriptions.cls?type=3\">The Assassins - Outhouses</a>, have reduced the assassins credits from 200 to 170.");
		HTMLFunctions.AddListEntry(str, "06/10/10 - Have renamed the Hear Enemies option to Advanced Mode.");
		HTMLFunctions.AddListEntry(str, "06/10/10 - In <a href=\"/dsr/missiondescriptions.cls?type=39\">The Assassins - Campaign Version</a>, have increased the assassins credits from 320 to 360.");
		HTMLFunctions.AddListEntry(str, "05/10/10 - Have officialy released the first <a href=\"http://stellarforces.blogspot.com/2010/09/whole-new-idea-for-missions.html\">SNAFU mission</a>: <a href=\"/dsr/missiondescriptions.cls?type=44\">Operation SNAFU: Moonbase Assault</a>.");
		HTMLFunctions.AddListEntry(str, "24/9/10 - New Missions courtesy of Deadlime (Linden Clarke): <b><a href=\"/dsr/missiondescriptions.cls?type=42\">Alien Containment</a></b> and <b><a href=\"/dsr/missiondescriptions.cls?type=43\">Hit &amp; Run 3 - Rescue from the Mines Map</a></b>.");
		HTMLFunctions.AddListEntry(str, "23/9/10 - New forums! <a href=\"" + DSRWebServer.NEW_FORUM_LINK + "\">" + DSRWebServer.NEW_FORUM_LINK + "</a>.  Please log into it using your display name and password, <i>not</i> your email address.");
		HTMLFunctions.AddListEntry(str, "22/9/10 - The address for the wiki is now <a href=\"" + DSRWebServer.WIKI_LINK + "\">" + DSRWebServer.WIKI_LINK + "</a>.");
		HTMLFunctions.AddListEntry(str, "22/9/10 - Players can now choose a name for their campaign squad.");
		HTMLFunctions.AddListEntry(str, "20/9/10 - The order that units are selected for campaign missions can now be set.");
		HTMLFunctions.AddListEntry(str, "20/9/10 - Some advanced missions can now only be played after a player has succesfully played a standard mission.");
		HTMLFunctions.AddListEntry(str, "19/9/10 - Increased the fugutive's credits to 300 in The Assassins - Outhouses.");
		HTMLFunctions.AddListEntry(str, "17/9/10 - In 3-Player Moonbase Assault, the attackers now get 10 VPs per computer, to give more incentive to players to <i>not</i> sit and wait for the other sides to kill each other.");
		HTMLFunctions.AddListEntry(str, "15/9/10 - New campaign version of missions created by Deadlime (Linden Clarke): <b><a href=\"/dsr/missiondescriptions.cls?type=39\">The Assassins</a></b> and <b><a href=\"/dsr/missiondescriptions.cls?type=40\">Moonbase Assault</a></b>.");
		HTMLFunctions.AddListEntry(str, "14/9/10 - For campaign missions, the server keeps track of how many times you have killed your own units.");
		HTMLFunctions.AddListEntry(str, "14/9/10 - In the Capture the F.L.A.G. missions, the F.L.A.G. must now be taken to the opposite side of the map.");
		HTMLFunctions.AddListEntry(str, "10/9/10 - A unit's stats are increased when they go up a rank.");
		HTMLFunctions.AddListEntry(str, "8/9/10 - Lots more existing missions are now available as campaign missions.");
		HTMLFunctions.AddListEntry(str, "6/9/10 - Shows a player's favourite mission on their public page.");
		HTMLFunctions.AddListEntry(str, "6/9/10 - Some 3 and 4-player missions are now available as campaign missions.");
		HTMLFunctions.AddListEntry(str, "6/9/10 - It's now possible to buy a piece of equipment for all units in one go.");
		HTMLFunctions.AddListEntry(str, "3/9/10 - Players can now concede games, and also propose mutually conceding games.");
		HTMLFunctions.AddListEntry(str, "31/8/10 - New Mission: <b><a href=\"/dsr/missiondescriptions.cls?type=3\">The Assassins - Outhouses</a></b>.");
		HTMLFunctions.AddListEntry(str, "27/8/10 - Escape hatches are shown on the strategic scanner to both sides.");
		HTMLFunctions.AddListEntry(str, "27/8/10 - Campaign missions can concede within 7 days.");
		HTMLFunctions.AddListEntry(str, "25/8/10 - New campaign missions: <a href=\"/dsr/missiondescriptions.cls?type=36\">Hit and Run 1 - Breakout Map</a> and <a href=\"/dsr/missiondescriptions.cls?type=37\">Hit and Run 2 - Escort 2 Map</a>");
		//HTMLFunctions.AddListEntry(str, "25/8/10 - There is now a <a href=\"/dsr/campaignleague.cls\">league table of top-ranked units</a>.");
		HTMLFunctions.AddListEntry(str, "25/8/10 - The address for the wiki is now <a href=\"http://www.stellarforceswiki.com\">http://www.stellarforceswiki.com</a>.");
		HTMLFunctions.AddListEntry(str, "23/8/10 - New mission: <a href=\"/dsr/missiondescriptions.cls?type=35\">4-Player Last Man Standing 2</a> (Last Man Standing using the Capture the F.L.A.G. map)");
		HTMLFunctions.AddListEntry(str, "16/8/10 - New missions: <a href=\"/dsr/missiondescriptions.cls?type=33\">F.L.A.G. Heist</a> and <a href=\"/dsr/missiondescriptions.cls?type=34\">4-Player F.L.A.G. Heist</a>");
		HTMLFunctions.AddListEntry(str, "16/8/10 - In <a href=\"/dsr/missiondescriptions.cls?type=27\">Prison Riot</a>, only light and medium armour can be purchased, and prisoners now have 30 creds.");
		HTMLFunctions.AddListEntry(str, "11/8/10 - Wounded units are now given enough AP's to at least reload their weapon.");
		HTMLFunctions.AddListEntry(str, "10/8/10 - Creates new games in a fraction of the time.");
		HTMLFunctions.AddListEntry(str, "8/8/10 - Shows the location of explosions on the strategic scanner.");
		HTMLFunctions.AddListEntry(str, "4/8/10 - Emails informing users of postings to subscribed forums now include a link.");
		HTMLFunctions.AddListEntry(str, "4/8/10 - Sale!  Medi-kits now only cost 4 creds.");
		HTMLFunctions.AddListEntry(str, "30/7/10 - In mission <a href=\"/dsr/missiondescriptions.cls?type=32\">3-Player Co-operative The Alien Hordes</a>, reduced alien units to 12.");
		HTMLFunctions.AddListEntry(str, "30/7/10 - Made the map fairer for all sides in <a href=\"/dsr/missiondescriptions.cls?type=28\">4-Player Capture the F.L.A.G.</a>, and adrenalin shots are no longer allowed.");
		HTMLFunctions.AddListEntry(str, "29/7/10 - In mission <a href=\"/dsr/missiondescriptions.cls?type=30\">3-Player Moonbase Assault</a>, the defenders now have 10 units and 380 creds.");
		HTMLFunctions.AddListEntry(str, "21/7/10 - New Mission: <b><a href=\"/dsr/missiondescriptions.cls?type=32\">3-Player Co-operative The Alien Hordes</a></b>.");
		HTMLFunctions.AddListEntry(str, "20/7/10 - When replying to a message, it now quotes the original message.");
		HTMLFunctions.AddListEntry(str, "17/7/10 - In mission <a href=\"/dsr/missiondescriptions.cls?type=27\">Prison Riot</a>, prisoners are only allowed to buy close-combat weapons, and now have 15 creds.");
		HTMLFunctions.AddListEntry(str, "16/7/10 - New Mission: <b><a href=\"/dsr/missiondescriptions.cls?type=31\">4-Player Moonbase Assault</a></b>.");
		HTMLFunctions.AddListEntry(str, "14/7/10 - New Mission: <b><a href=\"/dsr/missiondescriptions.cls?type=30\">3-Player Moonbase Assault</a></b>.");
		HTMLFunctions.AddListEntry(str, "13/7/10 - The recent game log is now emailed along with the turn reminder.");
		HTMLFunctions.AddListEntry(str, "10/7/10 - Units are now <b>not</b> identified when they are heard.");
		HTMLFunctions.AddListEntry(str, "9/7/10 - In mission <a href=\"/dsr/missiondescriptions.cls?type=27\">Prison Riot</a>, prisoners are no longer allowed to buy explosives.");
		HTMLFunctions.AddListEntry(str, "8/7/10 - New Mission: <b><a href=\"/dsr/missiondescriptions.cls?type=28\">4-Player Capture the F.L.A.G.</a></b>.");
		HTMLFunctions.AddListEntry(str, "6/7/10 - When viewing a game history, it now correctly shows walls before they were destroyed.");
		HTMLFunctions.AddListEntry(str, "9/7/10 - In mission <a href=\"/dsr/missiondescriptions.cls?type=27\">Prison Riot</a>, reduced the creds of the prisoners to 10 and increased the guard's creds to 200.");
		HTMLFunctions.AddListEntry(str, "5/7/10 - Removed a guard unit in mission <a href=\"/dsr/missiondescriptions.cls?type=18\">Prison Mutiny</a>.");
		HTMLFunctions.AddListEntry(str, "5/7/10 - In mission <a href=\"/dsr/missiondescriptions.cls?type=27\">Prison Riot</a>, reduced the creds of the prisoners to 10 and increased the guard's creds to 200.");
		HTMLFunctions.AddListEntry(str, "1/7/10 - New Mission: <b><a href=\"/dsr/missiondescriptions.cls?type=26\">Capture the F.L.A.G.</a></b>.");
		HTMLFunctions.AddListEntry(str, "1/7/10 - If a unit has less than a third of their health remaining, their APs are halved.");
		HTMLFunctions.AddListEntry(str, "1/7/10 - Aliens cannot be heard, even if the Hear Enemies option is on.");
		HTMLFunctions.AddListEntry(str, "1/7/10 - Reduced the size of the map in <a href=\"/dsr/missiondescriptions.cls?type=20\">Impregnator</a> mission.");
		HTMLFunctions.AddListEntry(str, "29/6/10 - Weapons now have a noise range, and if Hear Enemies is selected, shots can be heard within that range.");
		HTMLFunctions.AddListEntry(str, "29/6/10 - New Mission: <b><a href=\"/dsr/missiondescriptions.cls?type=27\">Prison Riot</a></b>.");
		HTMLFunctions.AddListEntry(str, "28/6/10 - In mission <a href=\"/dsr/missiondescriptions.cls?type=24\">Alien Escape</a>, the aliens now start with 10 units.");
		HTMLFunctions.AddListEntry(str, "25/6/10 - New Mission: <b><a href=\"/dsr/missiondescriptions.cls?type=25\">The Exterminators</a></b>.");
		HTMLFunctions.AddListEntry(str, "25/6/10 - Adjusted creds for the <a href=\"/dsr/missiondescriptions.cls?type=18\">Prison Mutiny</a> mission.");
		HTMLFunctions.AddListEntry(str, "24/6/10 - On the map screen, added colour key for players sides.");
		HTMLFunctions.AddListEntry(str, "22/6/10 - Now shows the direction units are facing in the map.");
		HTMLFunctions.AddListEntry(str, "21/6/10 - When equipping units, now shows average creds per unit.");
		HTMLFunctions.AddListEntry(str, "21/6/10 - <a href=\"/dsr/missiondescriptions.cls?type=21\">3-Player The Assassins</a> now runs for 20 turns.");
		HTMLFunctions.AddListEntry(str, "14/6/10 - Player are now informed if they have unread messages.");
		HTMLFunctions.AddListEntry(str, "11/6/10 - Ammo packs can now be automatically purchased with a gun.  Note that the ammo capacity of all guns have been slightly reduced.");
		HTMLFunctions.AddListEntry(str, "11/6/10 - Games now have the option of being able to hear enemy movement.");
		HTMLFunctions.AddListEntry(str, "10/6/10 - In <a href=\"/dsr/missiondescriptions.cls?type=18\">Prison Mutiny</a> mission, the guards now only have 8 units.");
		HTMLFunctions.AddListEntry(str, "10/6/10 - Now doesn't show enemy unit numbers in the map.");
		HTMLFunctions.AddListEntry(str, "10/6/10 - In the <a href=\"/dsr/missiondescriptions.cls?type=23\">Meltdown mission</a>, the number of player-turns before a computer will explode is now random between 3 and 5.");
		HTMLFunctions.AddListEntry(str, "10/6/10 - New mission: <b><a href=\"/dsr/missiondescriptions.cls?type=24\">Alien Escape</a></b>, created by Deadlime (Linden Clarke).");
		HTMLFunctions.AddListEntry(str, "9/6/10 - Added the video created by Krogot.");
		HTMLFunctions.AddListEntry(str, "4/6/10 - New mission: <b><a href=\"/dsr/missiondescriptions.cls?type=18\">Prison Mutiny</a></b>, created by PCasaca.");
		HTMLFunctions.AddListEntry(str, "4/6/10 - The alien units are now numbered to allow them to be differentiated by their player.");
		HTMLFunctions.AddListEntry(str, "3/6/10 - Added new Favicon, thanks to PCasaca.");
		HTMLFunctions.AddListEntry(str, "31/5/10 - New mission: <b><a href=\"/dsr/missiondescriptions.cls?type=20\">Impregnator</a></b>.");
		HTMLFunctions.AddListEntry(str, "19/5/10 - Improved the Miscellaneous Statistics");
		HTMLFunctions.AddListEntry(str, "17/5/10 - New mission: <b><a href=\"/dsr/missiondescriptions.cls?type=23\">Meltdown</a></b> (requires client 1.19 or later).");
		HTMLFunctions.AddListEntry(str, "17/5/10 - New mission: <b><a href=\"/dsr/missiondescriptions.cls?type=19\">Laboratory Attack</a></b>, created by Deadlime (Linden Clarke).");
		HTMLFunctions.AddListEntry(str, "15/5/10 - Removed mission Escort.");
		HTMLFunctions.AddListEntry(str, "15/5/10 - Added some internal walls to mission Escort 2.");
		HTMLFunctions.AddListEntry(str, "14/5/10 - New mission: <b><a href=\"/dsr/missiondescriptions.cls?type=22\">Battle for Sigma 7</a></b>.");
		HTMLFunctions.AddListEntry(str, "10/5/10 - It's now not possible to buy the Adrenalin Shot in the Escort missions.");
		HTMLFunctions.AddListEntry(str, "6/5/10 - Adjusted the max turns on some missions.");
		HTMLFunctions.AddListEntry(str, "6/5/10 - New mission: <b>3-player The Assassins</b>.");
		HTMLFunctions.AddListEntry(str, "5/5/10 - Now shows the total postings in each forum.");
		HTMLFunctions.AddListEntry(str, "5/5/10 - Changed the map image.");
		HTMLFunctions.AddListEntry(str, "30/4/10 - Fixed bug where if all of a player's units escaped, the server thought their opponent had killed them all.");
		HTMLFunctions.AddListEntry(str, "30/4/10 - Fixed bug that meant currently visible enemy units were not being shown on the map.");
		HTMLFunctions.AddListEntry(str, "29/4/10 - Now doesn't post the \"[Enter comment here]\" comment.");
		HTMLFunctions.AddListEntry(str, "22/4/10 - Improved the map generator so now it should definitely not time-out.");
		HTMLFunctions.AddListEntry(str, "22/4/10 - Users can now opt to <i>not</i> receive turn notification emails.");
		HTMLFunctions.AddListEntry(str, "15/4/10 - Modified the random map generation routine for the Last Man Standing missions.");
		HTMLFunctions.AddListEntry(str, "13/4/10 - New missions: <b>3-Player Last Man Standing</b> and <b>4-Player Last Man Standing</b>.");
		HTMLFunctions.AddListEntry(str, "12/4/10 - It's now possible to search the forums.");
		HTMLFunctions.AddListEntry(str, "12/4/10 - Create a new URL for the Wiki: " + DSRWebServer.WIKI_LINK);
		HTMLFunctions.AddListEntry(str, "7/4/10 - New mission: <b>4-Player Escape</b>.");
		HTMLFunctions.AddListEntry(str, "7/4/10 - Players are automatically emailed when a new game request is locked to them.");
		HTMLFunctions.AddListEntry(str, "6/4/10 - Fixed the spelling of 'Seargent' in all the unit names.");
		HTMLFunctions.AddListEntry(str, "6/4/10 - Now only shows locked game requests to the player who the request is waiting for.");
		HTMLFunctions.AddListEntry(str, "1/4/10 - Stopped anonymous forum postings due to spamming.");
		HTMLFunctions.AddListEntry(str, "29/3/10 - Added the ability to play 3-player games, and created a simple 3-player mission.");
		HTMLFunctions.AddListEntry(str, "26/3/10 - Now shows the name of the sides when starting a game.");
		HTMLFunctions.AddListEntry(str, "26/3/10 - Specific opponents can now be chosen for game requests.");
		HTMLFunctions.AddListEntry(str, "22/3/10 - The website now shows who is currently logged in.");
		HTMLFunctions.AddListEntry(str, "16/3/10 - Players can now log in with their display name instead of their email address.");
		HTMLFunctions.AddListEntry(str, "12/2/10 - New mission: <b>Escort 2</b>.");
		HTMLFunctions.AddListEntry(str, "10/3/10 - Amended the deployment area of side 2 in the Moonbase Assault mission.");
		HTMLFunctions.AddListEntry(str, "4/3/10 - Added a 'confirm password' box when registering.");
		HTMLFunctions.AddListEntry(str, "4/3/10 - Name of player to make the latest post in each forum is now shown.");
		HTMLFunctions.AddListEntry(str, "4/3/10 - Map image size increased.");
		HTMLFunctions.AddListEntry(str, "2/3/10 - Users will receive notices of new postings to all forums that they themselves have posted to.");
		HTMLFunctions.AddListEntry(str, "1/3/10 - A lot of the weapon and equipment prices have been recalculated.");
		HTMLFunctions.AddListEntry(str, "1/3/10 - Added a key to the map page.");
		HTMLFunctions.AddListEntry(str, "25/2/10 - When deploying, the map shows the enemy's deployment squares as well as your own.");
		HTMLFunctions.AddListEntry(str, "25/2/10 - Change the league table so that on a points draw, the player with the most wins takes precedence.  On a wins draw, the player who's lost the least number takes precedence.");
		HTMLFunctions.AddListEntry(str, "24/2/10 - Added some more miscellaneous stats to the <a href=\"/dsr/miscstatspage.cls\">Miscellaneous Statistics page</a>.");
		HTMLFunctions.AddListEntry(str, "22/2/10 - New mission: <b>Moonbase Assault 2</b>.");
		HTMLFunctions.AddListEntry(str, "19/2/10 - Added a <a href=\"/dsr/knownissues.cls\">Known Issues</a> page, cos there's a known issue I can't track down.");
		HTMLFunctions.AddListEntry(str, "9/2/10 - Added a brief description to equipment.");
		HTMLFunctions.AddListEntry(str, "9/2/10 - Added a new armour type.");
		HTMLFunctions.AddListEntry(str, "3/2/10 - Now shows the armour details on the armour buying page.");
		HTMLFunctions.AddListEntry(str, "3/2/10 - New mission: <b>Rescue from the Mines</b>.");
		HTMLFunctions.AddListEntry(str, "2/2/10 - Now shows the unit details in the game details page, even if the unit is not deployed yet.");
		HTMLFunctions.AddListEntry(str, "29/1/10 - Added the <a href=\"/dsr/miscstatspage.cls\">Miscellaneous Statistics page</a>.");
		HTMLFunctions.AddListEntry(str, "29/1/10 - Each game is now automatically given its own sub-forum.");
		HTMLFunctions.AddListEntry(str, "29/1/10 - Players can now show their website address in their public page.");
		HTMLFunctions.AddListEntry(str, "29/1/10 - It's now possible to subscribe to forums.");
		HTMLFunctions.AddListEntry(str, "27/1/10 - Sterner Regnix's name <b>cannot</b> now be changed, to avoid confusion.");
		HTMLFunctions.AddListEntry(str, "22/1/10 - It now confirms if you've finished equipping your units.");
		HTMLFunctions.AddListEntry(str, "20/1/10 - Changed the Escort mission so that side 2 now has 9 units instead of 6, and also changed deployment squares.");
		HTMLFunctions.AddListEntry(str, "20/1/10 - The forums are now open to non-users.");
		HTMLFunctions.AddListEntry(str, "19/1/10 - New mission: <b>The Alien Hordes</b>.");
		HTMLFunctions.AddListEntry(str, "19/1/10 - The credits for spending on equipment has been increased on a few of the missions.");
		HTMLFunctions.AddListEntry(str, "18/1/10 - A 'please wait' page is now shown when a game is being created, as this can sometimes take a few minutes.");
		HTMLFunctions.AddListEntry(str, "18/1/10 - Games can be marked as 'practise' to enable players to play without affecting their chances in the league.");
		HTMLFunctions.AddListEntry(str, "18/1/10 - Players can now <a href=\"/dsr/mysettings.cls\">customise their unit names</a>.");
		HTMLFunctions.AddListEntry(str, "15/1/10 - Tweaked the way the league tables are calculated.");
		HTMLFunctions.AddListEntry(str, "15/1/10 - Messages between players now have a subject line.");
		HTMLFunctions.AddListEntry(str, "14/1/10 - Techno Armour now costs 35 (from 30).");
		HTMLFunctions.AddListEntry(str, "12/1/10 - Reformatted weapons, equipment and armour stats pages.");
		HTMLFunctions.AddListEntry(str, "11/1/10 - Stellar Forces now has a <a href=\"" + DSRWebServer.WIKI_LINK + "\">Wiki</a>.");
		HTMLFunctions.AddListEntry(str, "11/1/10 - Now shows a user their sent messages.");
		HTMLFunctions.AddListEntry(str, "9/1/10 - The date of the last forum post is shown in the forums.");
		HTMLFunctions.AddListEntry(str, "8/1/10 - Now shows top players by mission.");
		HTMLFunctions.AddListEntry(str, "8/1/10 - New mission: <b>Escort</b>.");
		HTMLFunctions.AddListEntry(str, "8/1/10 - Now includes more specific instructions on requiring Java.");
		HTMLFunctions.AddListEntry(str, "5/1/10 - Walls can now be destroyed with grenades on certain missions (shown in the mission details).");
		HTMLFunctions.AddListEntry(str, "1/1/10 - Messages sent between players can now be viewed on the website.");
		HTMLFunctions.AddListEntry(str, "29/12/09 - Users can now <a href=\"voteforfeature.cls\">vote for the next major feature</a> of the game.");
		HTMLFunctions.AddListEntry(str, "29/12/09 - Added a new mission: <b>Defend the Base</b>.");
		HTMLFunctions.AddListEntry(str, "28/12/09 - You can now view the historical game map at each turn once a game is finished.");
		HTMLFunctions.AddListEntry(str, "24/12/09 - New mission added: <b>A Tale of Two Moonbases</b>.");
		HTMLFunctions.AddListEntry(str, "23/12/09 - Visible enemy units are now shown on the map and mentioned in the log.");
		HTMLFunctions.AddListEntry(str, "22/12/09 - Added the <a href=\"/dsr/instructions.quickstart.cls\">quickstart</a> instructions.");
		HTMLFunctions.AddListEntry(str, "22/12/09 - Dead units from both sides are now shown on the map.");
		HTMLFunctions.AddListEntry(str, "15/12/09 - Added extra fields to a players 'About Me' page.");
		HTMLFunctions.AddListEntry(str, "15/12/09 - Now keeps track of the total number of times a player has conceded.");
		HTMLFunctions.AddListEntry(str, "11/12/09 - Combined the mission map, units and log into one page.");
		HTMLFunctions.AddListEntry(str, "11/12/09 - The Escape mission map generator now ensures that the escape hatch is a certain distance from the deployment area.");
		HTMLFunctions.AddListEntry(str, "10/12/09 - This page added!");
		HTMLFunctions.AddListEntry(str, "10/12/09 - It's now possible to send messages directly to other players using the website.");
		HTMLFunctions.AddListEntry(str, "10/12/09 - <b>Moonbase Assault</b> mission added.");
		HTMLFunctions.AddListEntry(str, "8/12/09 - Added an (as yet quite bare) <a href=\"hintsandtips.cls\">Hints and Tips</a> page.");
		HTMLFunctions.AddListEntry(str, "7/12/09 - Both players in a game will now get emailed when a game is won/lost.");
		HTMLFunctions.AddListEntry(str, "5/12/09 - Added the <a href=\"controls.cls\">controls</a> page.");
		HTMLFunctions.AddListEntry(str, "3/12/09 - Once a game has finished, all units are shown on the map.");
		HTMLFunctions.AddListEntry(str, "2/12/09 - It's now possible to force a concede where a player has taken too long to take their turn.");
		HTMLFunctions.AddListEntry(str, "2/12/09 - <b>Breakout</b> mission added.");
		HTMLFunctions.AddListEntry(str, "30/11/09 - Added the <a href=\"leaguetable.cls\">League Table</a>.");
		HTMLFunctions.AddListEntry(str, "27/11/09 - Added the <a href=\"/dsr/forums/ForumMainPage.cls\">Forums</a>.");
		HTMLFunctions.AddListEntry(str, "18/11/09 - It's now possible to send a reminder to players who are taking too long to take their turn.");
		
		HTMLFunctions.EndUnorderedList(str);
		
		HTMLFunctions.EndCell(str);
		HTMLFunctions.StartCell(str);
		
		HTMLFunctions.Heading(str, 3, "<a name=\"client\">Client Updates</a>");

		HTMLFunctions.Heading(str, 3, "1.84 (released 3/2/12)");
		HTMLFunctions.StartUnorderedList(str); 
		HTMLFunctions.AddListEntry(str, "Adjusted some AP costs.");
		HTMLFunctions.AddListEntry(str, "Defenders can now harm the attackers in close combat.");
		HTMLFunctions.AddListEntry(str, "Fixed 'catching' bug where it wasn't showing 'change item'.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.83 (released 24/1/12)");
		HTMLFunctions.StartUnorderedList(str); 
		HTMLFunctions.AddListEntry(str, "Close-combat opportunity fire.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.82 (released 7/11/11)");
		HTMLFunctions.StartUnorderedList(str); 
		HTMLFunctions.AddListEntry(str, "Computers are shown on the in-client strategic scanner.");
		HTMLFunctions.AddListEntry(str, "Comrades are shown in a different colour in the in-client strategic scanner.");
		HTMLFunctions.AddListEntry(str, "Escape hatches are shown on the strategic scanner.");
		HTMLFunctions.AddListEntry(str, "Units will no longer escape automatically.");
		HTMLFunctions.AddListEntry(str, "New 'auto-aim' feature.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.81 (released 19/7/11)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "You can click on the strategic scanner in the game to select a unit.");
		HTMLFunctions.AddListEntry(str, "The currently selected unit is highlighted on the strategic scanner.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.80 (released 27/6/11)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "This version is required to play Aliens.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.79 (released 27/6/11)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Opp-fire shots are now 15% less accurate than snapshots.");
		HTMLFunctions.AddListEntry(str, "Blobs can now absorb corpses.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.73 (released 27/4/2011)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Do not need to restart the client between turns.");
		HTMLFunctions.AddListEntry(str, "A strategic scanner is available inside the client.");
		HTMLFunctions.AddListEntry(str, "Now has a rudimentary AI for practise games.");
		HTMLFunctions.AddListEntry(str, "Improved tutorial.");
		HTMLFunctions.AddListEntry(str, "3D options only appear if required.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.72 (released 15/4/2011)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Nerve gas damage is equal to a unit's APs at the end of a turn.");
		HTMLFunctions.AddListEntry(str, "This version is required for the new 2v2 missions.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.71 (released 14/4/2011)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Reduced memory usage.");
		HTMLFunctions.AddListEntry(str, "Highlights warnings in the HUD.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.68 (released 5/4/2011)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Required for new The Big Push mission.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.67 (released 1/4/2011)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "New unit type: Blobs.");
		HTMLFunctions.AddListEntry(str, "Nerve gas damage is now related to the number of APs spent in the nerve gas cloud.");
		HTMLFunctions.AddListEntry(str, "Aliens have 180<sup>o</sup> vision.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.65 (released 21/3/2011)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Players are notified if one of their units see's an enemy unit prime a grenade.");
		HTMLFunctions.AddListEntry(str, "It costs double APs to walk over a hedge or low wall.");
		HTMLFunctions.AddListEntry(str, "Grenades now only destroy the first layer of walls in the blast range in maps with 'strong' walls.");
		HTMLFunctions.AddListEntry(str, "Thrown items no longer land 'in' doors.");
		HTMLFunctions.AddListEntry(str, "Fixed a bug where smoke was protecting units from grenade explosions.");
		HTMLFunctions.AddListEntry(str, "Reduced the memory usage when explosions occur.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.63 (released 12/3/2011)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Warns if a unit is out of ammo.");
		HTMLFunctions.AddListEntry(str, "Units are harmed if they walk into Nerve Gas.");
		HTMLFunctions.AddListEntry(str, "Catchers must be able to see the thrower in order to catch a thrown item.");
		HTMLFunctions.AddListEntry(str, "Improved the playback.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.61 (released 8/3/2011)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "New brick wall scenery.");
		HTMLFunctions.AddListEntry(str, "Grenades not exploded by another explosion are now <i>not</i> destroyed.");
		HTMLFunctions.AddListEntry(str, "Fixed an error caused when a grenade exploded while being held by a unit.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.60 (released 8/3/2011)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "New hedgerow, posts and gas cannister scenery.");
		HTMLFunctions.AddListEntry(str, "Improved floor scenery underneath doors.");
		HTMLFunctions.AddListEntry(str, "Non-laser weapons only lose 1/2pt damage for every square over 10.");
		HTMLFunctions.AddListEntry(str, "Scenery removed when walls are destroyed.");
		HTMLFunctions.AddListEntry(str, "It now costs 3 APs to catch an item.");
		HTMLFunctions.AddListEntry(str, "Rudimentary 3D playback facility.");
		HTMLFunctions.AddListEntry(str, "Map editor included.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.58 (released 23/2/2011)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Explosions and extra scenery are not shown in low-memory mode.");
		HTMLFunctions.AddListEntry(str, "Smoke grenades and nerve gas.");
		HTMLFunctions.AddListEntry(str, "Opportunity fire is now at 10% less accuracy than a snapshot.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.57 (released 17/2/2011)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "It now costs " + AppletMain.APS_CATCH + " to catch a thrown item.");
		HTMLFunctions.AddListEntry(str, "Fixed erroneous random misfit map square tiles.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.56 (released 15/2/2011)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Informs player if unit is exhausted or has panicked.");
		HTMLFunctions.AddListEntry(str, "Now maintains a permanent connection to the server for better response times.");
		HTMLFunctions.AddListEntry(str, "Added more textures.");
		HTMLFunctions.AddListEntry(str, "Reduced memory usage.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.55 (released 31/1/11)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "The size of explosion f/x is related to the power of the grenade.");
		HTMLFunctions.AddListEntry(str, "There is only a 50% chance that an explosion will cause other grenades to explode.");
		//HTMLFunctions.AddListEntry(str, "Map Editor is included in the download.");
		HTMLFunctions.AddListEntry(str, "New 'close combat opportunity attack' feature.");
		HTMLFunctions.AddListEntry(str, "Units will opp-fire on the very first turn.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.53 (released 20/1/11)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Now works through a proxy server.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.52 (released 18/1/11)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Computers can be destroyed by melee attacks.");
		HTMLFunctions.AddListEntry(str, "Explosions are now shown sequentially (rather than all in one go).");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.51 (released 13/1/11)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Changed the font.");
		HTMLFunctions.AddListEntry(str, "Gives explicit warning if a unit will not opp-fire if an adjacent friendly unit is visible.");
		HTMLFunctions.AddListEntry(str, "Fixed 'throwing over dead body' bug.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.5 (released 8/1/11)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Fixed small throwing bug that was causing an error.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.49 (released 7/1/11)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Reduced the amount of memory required.");
		HTMLFunctions.AddListEntry(str, "Fixed a few bugs.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.48 (released 4/1/11)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Added code to enable the playback of a game before it has been finished.  (Actual playback coming soon).");
		HTMLFunctions.AddListEntry(str, "Fixed Death Grenade bug that meant a unit's death was always attributed to their own death grenade.");
		HTMLFunctions.AddListEntry(str, "Fixed throwing distance bug.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.47 (released 15/12/10)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Gives a warning if a player tries to throw an unprimed grenade.");
		HTMLFunctions.AddListEntry(str, "The 'line-of-sight' line can be activated by pressing K.");
		HTMLFunctions.AddListEntry(str, "New accuracy values for snap &amp; auto shots.");
		HTMLFunctions.AddListEntry(str, "Fixed Death Grenade bug.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.46 (released 9th December 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Wounding from grenades is adjusted by the angle of the unit.");
		HTMLFunctions.AddListEntry(str, "Alien death acid splash distance is now randomly adjusted.");
		HTMLFunctions.AddListEntry(str, "Gives better warnings when a unit is holding a primed grenade.");
		HTMLFunctions.AddListEntry(str, "Added starfield scenery.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.45 (released 22nd November 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Laser weapons now do not lose power over distance.");
		HTMLFunctions.AddListEntry(str, "Queen Alien now has an alien voice.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.44 (released 18th November 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Client will auto-update itself.");
		HTMLFunctions.AddListEntry(str, "Unit numbers are shown in the HUD.");
		HTMLFunctions.AddListEntry(str, "New 'water' mapsquare type.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.43 (released 12th November 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Aliens not harmed by alien acid.");
		HTMLFunctions.AddListEntry(str, "Fixed a few bugs.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.42 (released 10th November 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "A better alien egg and new grenade models.");
		HTMLFunctions.AddListEntry(str, "Enemy units are identified by name when clicking on them.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.41 (released 4th November 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Doors can now be manually closed.");
		HTMLFunctions.AddListEntry(str, "New speech effects kindly provided by Deadlime (Linden Clarke).");
		HTMLFunctions.AddListEntry(str, "In advanced games, corpses will now only be shown if seen by a unit.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.40 BETA (released 26th October 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Units now move and animate smoothly.");
		HTMLFunctions.AddListEntry(str, "Added more scenery types.");
		HTMLFunctions.AddListEntry(str, "Help Mode now selectable from login page.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.39 (released 19th October 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Units will now *not* opp-fire if they are adjacent to a friendly unit that they can see.");
		HTMLFunctions.AddListEntry(str, "Won't show extra scenery when running in low-memory mode.");
		HTMLFunctions.AddListEntry(str, "In advanced mode, equipment won't be shown unless it has been seen by a unit.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.38 (released 14th October 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Now informs players if their units see another unit drop, pickup or throw a piece of equipment.");
		HTMLFunctions.AddListEntry(str, "In SNAFU missions, units will not 'melee' attack an opponent's unit unless they have been identified as the enemy.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.36 (released 7th October 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "This version has a \"low memory\" option for computers with lower amounts of memory.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.35 (released 6th October 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "This version is required to play the new Practise Mission.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.34 (released 5th October 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "This version is required to play the new <a href=\"http://stellarforces.blogspot.com/2010/09/whole-new-idea-for-missions.html\">SNAFU</a> missions.");
		HTMLFunctions.AddListEntry(str, "Death grenade-related deaths are now caused by the unit holding the grenade, not the unit that kills the unit holding the grenade.");
		HTMLFunctions.AddListEntry(str, "Unique models for each of the armour types.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.33 (released 29th September 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Will inform the user if a new version is available.");
		HTMLFunctions.AddListEntry(str, "It will log when a player's unit see's another unit shoot.");
		HTMLFunctions.AddListEntry(str, "Aliens cause a shower of acid blood when killed.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.32 (released 20th September 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Shows the maximum distance allowed when throwing.");
		HTMLFunctions.AddListEntry(str, "Checks that the program is allowed to created the required files.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.31 (released 13th September 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "This version is required for campaign missions.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.30 (released 1st September 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Added extra help for new players.");
		HTMLFunctions.AddListEntry(str, "Now works on 64-bit Windows (thanks to Deadlime (Linden Clarke)).");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.29 (released 9th August 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Can now handle connection timeout errors to avoid lost data.");
		HTMLFunctions.AddListEntry(str, "Fixed 'throwing distance' bug.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.28 (released 30th July 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Units now start in a random direction.");
		HTMLFunctions.AddListEntry(str, "It's possible to remove an item without dropping it.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.27 (released 14th July 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "This version is required for the new 3-Player Moonbase Assault.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.26 (released 6th July 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Camera can be locked into position.");
		HTMLFunctions.AddListEntry(str, "This version is required for the Capture the F.L.A.G. mission.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.25 (released 25th June 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Removed the Wait option.");
		HTMLFunctions.AddListEntry(str, "Added more scenery models.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.24 (released 23rd June 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "If there is any excess ammo remaining when reloading, it can be used later.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.23 (released 10th June 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Doesn't show the Wait icon if not required.");
		HTMLFunctions.AddListEntry(str, "The adrenalin shot now re-imburses the unit for the cost of changing item and using it.");
		HTMLFunctions.AddListEntry(str, "Shows the actual cost of picking up an item on the icon, based on whether the unit is already using an item.");
		HTMLFunctions.AddListEntry(str, "Added a few bits of scenery.");
		HTMLFunctions.AddListEntry(str, "Fix bug in missions where aliens can impregnate humans, so it only happens for melee attacks.");
		HTMLFunctions.AddListEntry(str, "It will now say in the log if a unit harmed a unit on their own side.");
		HTMLFunctions.AddListEntry(str, "Fixed the game log as sometimes it's slightly out of order.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.22 (released 2nd June 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "This version is required for the new Alien Escape mission.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.21 (released 1st June 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "This version is required for the new Impregnator mission.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.20 (released 26th May 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Shows a unit's name in a different colour if wounded.");
		HTMLFunctions.AddListEntry(str, "Added a new Wait icon.");
		HTMLFunctions.AddListEntry(str, "A grid can be toggle on/off to help measure distances.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.19 (released 17th May 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "This version is required for the new Meltdown mission.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.18 (released 13th May 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Fixed throwing bug where an object could land 'in' the wall.");
		HTMLFunctions.AddListEntry(str, "Improved Prime Grenade help.");
		HTMLFunctions.AddListEntry(str, "Shows a weapon's ammo when picking it up.");
		HTMLFunctions.AddListEntry(str, "Added some more translated text.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.17 (released 4th May 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Now possible to mute the music (again).");
		HTMLFunctions.AddListEntry(str, "Added Francais and Deutsch translations.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.16 (released 30th April 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Portugese translation added - thanks to PCasaca");
		HTMLFunctions.AddListEntry(str, "Now told what piece of equipment an enemy unit is using.");
		HTMLFunctions.AddListEntry(str, "Improved Spanish translation - thanks to Victor.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.15 (released 27th April 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Reduced cost of picking up and item to 10 APs.");
		HTMLFunctions.AddListEntry(str, "Add Spanish language text.");
		HTMLFunctions.AddListEntry(str, "Opp fire - units will now shoot at newly visible units even if they were seen be one of their own units at the start.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.14 (released 23rd April 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Made close combat more worthwhile.");
		HTMLFunctions.AddListEntry(str, "Music starts immediately.");
		HTMLFunctions.AddListEntry(str, "Fixed bug preventing 3-Player Last Man Standing mission working.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.13 (released 14th April 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Sorted another grenade bug!");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.11 (released 13th April 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Finally sorted the grenade bug!");
		HTMLFunctions.AddListEntry(str, "Now doesn't show a small explosion when throwing an item.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.10 (released 13th April 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Added help text when priming grenades.");
		HTMLFunctions.AddListEntry(str, "Remembers whether doors are open or closed.");
		HTMLFunctions.EndUnorderedList(str);
		
		HTMLFunctions.Heading(str, 3, "1.09 (released 12th April 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Changed the music.");
		HTMLFunctions.AddListEntry(str, "Icon change emphasises that units need to be equipped with guns to be able to shoot.");
		HTMLFunctions.AddListEntry(str, "Now logs which side killed which unit.");
		HTMLFunctions.EndUnorderedList(str);
		
		HTMLFunctions.Heading(str, 3, "1.08 (released 6th April 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Fixed bug that caused client to crash when walking to the edge of the map.");
		HTMLFunctions.AddListEntry(str, "Fixed various minor 3-player bugs.");
		HTMLFunctions.AddListEntry(str, "Now doesn't keep the enemy unit icons for enemies visible at the start of a turn.");
		HTMLFunctions.AddListEntry(str, "Keeps the command window open in order to view any potential error messages.");
		HTMLFunctions.AddListEntry(str, "Logs all error messages to an error_log.txt file.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.07 (released 29th March 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Reduce the AP cost of priming a grenade.");
		HTMLFunctions.AddListEntry(str, "Wounding by an explosion is now randomized in the same way as for shot fire.");
		HTMLFunctions.AddListEntry(str, "Fixed a bug which was causing opportunity-fire shoot to be too accurate.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.06 (released 26th March 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Increased the aimed accuracy.");
		HTMLFunctions.AddListEntry(str, "This version is required for 3+ player missions.");
		HTMLFunctions.AddListEntry(str, "Now shows who shot who in the logs.");
		//HTMLFunctions.AddListEntry(str, "");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.05 (released 19th March 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Improved the walls.");
		HTMLFunctions.AddListEntry(str, "New damage bonus for short-range shots.");
		HTMLFunctions.AddListEntry(str, "Shot damage reduction for long-range shots.");
		HTMLFunctions.AddListEntry(str, "Fixed grenade bug.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.04 (released 16th March 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Units can now open doors without stepping into them.");
		HTMLFunctions.AddListEntry(str, "Increased font size in the HUD.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.03 (released 10th March 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "A unit's strength is now taken into account when throwing.");
		HTMLFunctions.AddListEntry(str, "Improved login windows.");
		HTMLFunctions.AddListEntry(str, "New equipment: Adrenalin shot.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.02 (released 5th March 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Now logs if a unit was shot but not harmed.");
		HTMLFunctions.AddListEntry(str, "Now correctly uses the AP cost of the weapon when reloading.");
		HTMLFunctions.AddListEntry(str, "Added a better front-end to logging in.");
		HTMLFunctions.AddListEntry(str, "Improve explosion algorythm.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "1.01 (released 2nd March 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "A unit's health is shown on the stats bar.");
		HTMLFunctions.AddListEntry(str, "A shot's accuracy is always adjusted by a random fractional amount.");
		HTMLFunctions.AddListEntry(str, "Added 2 more sound effects.");
		HTMLFunctions.AddListEntry(str, "Reduced the potential throwing innaccuracy.");
		HTMLFunctions.AddListEntry(str, "Reduced the maximum shooting innaccuracy.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.57 (released 25th February 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "New equipment: Death Grenades.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.56 (released 23rd February 2010)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Implemented the original Laser Squad shooting algorythm (with a few minor tweaks).");
		HTMLFunctions.AddListEntry(str, "Added Rocket Launchers.");
		HTMLFunctions.AddListEntry(str, "Added sound effects.");
		HTMLFunctions.EndUnorderedList(str);
		
		HTMLFunctions.Heading(str, 3, "0.55 (9/2/10)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Now shows a unit's protection in the HUD.");
		HTMLFunctions.AddListEntry(str, "Simplified weapon accuracy.");
		HTMLFunctions.AddListEntry(str, "New piece of equipment: Portable Teleporter");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.54 (3/2/10)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Armour protection is now reduced if the attack is from the sides or rear.");
		HTMLFunctions.AddListEntry(str, "During close-combat, a defender's combat skill is now reduced if the attack is from the sides or rear.");
		HTMLFunctions.AddListEntry(str, "Bullets are now shown when a shot goes off the edge of the map.");
		HTMLFunctions.AddListEntry(str, "There are now small explosions wherever a bullet hits.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.53 (28/1/10)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Icons for selecting the next/prev unit.");
		HTMLFunctions.AddListEntry(str, "Player is now warned if a unit will not have enough AP's to throw a primed grenade.");
		HTMLFunctions.AddListEntry(str, "Now shows how many turns a grenade is primed for.");
		HTMLFunctions.AddListEntry(str, "Player is now warned if they are ending a turn and a unit is holding a grenade that is about to go off.");
		HTMLFunctions.AddListEntry(str, "Now shows a unit's inventory when the unit is selected.");
		HTMLFunctions.AddListEntry(str, "Also shows a unit's inventory when the unit is being deployed.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.52 (21/1/10)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "The client can now auto-login if required.");
		HTMLFunctions.AddListEntry(str, "Added on-screen icons to control units.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.51 (19/1/10)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Opportunity fire is now slightly less accurate than a snapshot, and all fire is now always adjusted by a small random angle.");
		HTMLFunctions.AddListEntry(str, "Added medi-kits");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.5 (15/1/10)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Improved the 'redefine keys' feature.");
		HTMLFunctions.AddListEntry(str, "Damage is now randomized by 50% - 150%.");
		HTMLFunctions.AddListEntry(str, "Fixed a bug to do with throwing items of equipment.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.49 (11/1/10)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "The client will now make multiple attempts to connect to the server.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.47 (8/1/10)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "A grenades explosion radius is now used to calculate how far it can destroy walls and computers.");
		HTMLFunctions.AddListEntry(str, "Doors will now stay open if there is a corpse in the way.");
		HTMLFunctions.AddListEntry(str, "The keyboard controls can now be redefined.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.46 (5/1/10)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Computers and doors can now be destroyed by a grenade.");
		HTMLFunctions.AddListEntry(str, "It now costs more AP's to walk through a closed door.");
		HTMLFunctions.AddListEntry(str, "Now shows explosions better.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.45 (5/1/10)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "This version is required for walls to be destructable.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.44 (31/12/09)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "It's now not possible to select dead or invisible enemy units.");
		HTMLFunctions.AddListEntry(str, "Fixed a bug where units were using opportunity on any unit they could see, rather than just the unit that has moved.");
		HTMLFunctions.AddListEntry(str, "Fixed a bug where units could see enemy units even when they weren't facing them.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.43 (29/12/09)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "It now won't reduce AP's if a unit tries to throw but the angle is too far.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.42 (28/12/09)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Fixed a major bug.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.Heading(str, 3, "0.41 (23/12/09)");
		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "The camera can now be raised and lowered.");
		HTMLFunctions.AddListEntry(str, "This version of the client is required to show visible enemy units on the map.");
		HTMLFunctions.AddListEntry(str, "Fixed a bug where destroyed computers were being created as undestroyed computers when the client was started.");
		HTMLFunctions.EndUnorderedList(str);

		HTMLFunctions.EndCell(str);
		HTMLFunctions.EndRow(str);
		HTMLFunctions.EndTable(str);

		this.body_html.append(MainLayout.GetHTML(this, "Latest Features", str));
	}
*/
}
