package dsrwebserver.pages.dsr;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.FactionsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MessagesTable;
import dsrwebserver.tables.PlayerAwardsTable;
import dsrwebserver.tables.PlayerTrophiesTable;
import dsrwebserver.tables.ReservePlayersTable;

public final class playerspublicpage extends AbstractHTMLPage {

	public playerspublicpage() {
		super();
	}


	@Override
	public void process() throws Exception {
		int loginid = this.headers.getGetOrPostValueAsInt("loginid");

		LoginsTable login = new LoginsTable(dbs);
		if (login.doesRowExist(loginid)) {
			String next = this.headers.getGetOrPostValueAsString("next");
			String subject = this.headers.getGetOrPostValueAsString("subject").trim();
			String msg = this.headers.getGetOrPostValueAsString("msg").trim();
			String award_name = this.headers.getGetOrPostValueAsString("award_name");
			// Send msg?
			if (this.session.isLoggedIn()) {
				if (next.equalsIgnoreCase("send_msg")) {
					if (msg != null) {
						if (msg.length() > 0) {
							if (this.current_login.getTotalTurns() > 0) { // Prevent spam
								//login.sendEmail("Message from " + this.current_login.getDisplayName() + ": " + subject, "PLEASE DO NOT REPLY TO THIS EMAIL!  You have been sent a message from " + this.current_login.getDisplayName() + ".  To reply, visit the messages page at http://www.stellarforces.com/dsr/viewmessages.cls.  The message is as follows:-\n\n\"" + msg + "\"\n\n");
								// Send email to admin
								//DSRWebServer.SendEmailToAdmin("Msg sent", msg);
								MessagesTable.SendMsg(dbs, this.current_login.getID(), loginid, subject, msg);
								this.redirectTo_Using303("viewmessages.cls");
							}
						}
					}
				} else if (next.equalsIgnoreCase("send_award")) {
					if (PlayerAwardsTable.CanPlayerMakeAward(dbs, this.current_login)) {
						if (award_name.length() > 0) {
							if (this.current_login.getID() != login.getID()) {
								login.sendMsg(-1, "You have won an award!", "PLEASE DO NOT REPLY TO THIS EMAIL!\n\nCongratulations!  " + this.current_login.getDisplayName() + " has sent you an award for " + award_name + ".\n\n");
								this.current_login.setDatelastAwardAwarded();
								PlayerAwardsTable.AddRec(dbs, this.current_login.getID(), login.getID(), award_name);
								this.redirectTo_Using303("playerawards.cls");
							} else {
								award_name = "You cannot send yourself an award!";
							}
						} else {
							award_name = "Please enter some text!";
						}
					} else {
						//HTMLFunctions.Para(str, "You cannot currently send an award.");
					}
				} else if (next.equalsIgnoreCase("change_email")) {
					if (this.current_login.isAdmin()) {
						String email = this.headers.getGetOrPostValueAsString("email").trim();
						msg = login.updateLogin(email);
						if (msg.length() == 0) {
							login.clearDateLastReminder();
						}
					}
				}
				if (this.current_login.isAdmin()) {
					int disable = this.headers.getGetOrPostValueAsInt("disable");
					if (disable > 0) {
						if (LoginsTable.IsLoginDisabled(dbs, disable) == false) {
							LoginsTable.SetLoginDisabled(dbs, disable, 1);
						} else {
							LoginsTable.SetLoginDisabled(dbs, disable, 0);
						}
					}
					int send_invalid_email_msg = this.headers.getGetOrPostValueAsInt("send_invalid_email_msg");
					if (send_invalid_email_msg > 0) {
						if (LoginsTable.HasBeenSentInvalidEmailMsg(dbs, send_invalid_email_msg) == false) {
							MessagesTable.SendMsg(dbs, this.current_login.getID(), send_invalid_email_msg, "Incorrect Email Address", "Hi " + LoginsTable.GetDisplayName(dbs, send_invalid_email_msg) + ", unfortunately the email address '" + LoginsTable.GetEmail(dbs, send_invalid_email_msg) +"' that you provided is not accepting emails, so you will not receive your turn notifications.  Could you reply with a valid email address so we can update your profile?  Note that we never shared players email addresses with anyone else.\n\nThanks in advance,\n\nSteve\nStellar Forces Admin");
							LoginsTable.SetHasBeenSentInvalidEmailMsg(dbs, send_invalid_email_msg);
						}
					}
					/*int send_why_not_playing_msg = this.headers.getGetOrPostValueAsInt("send_why_not_playing_msg");
					if (send_why_not_playing_msg > 0) {
						if (LoginsTable.HasBeenSentWhyNotPlayingMsg(dbs, send_why_not_playing_msg) == false) {
							MessagesTable.SendMsg(dbs, this.current_login.getID(), send_why_not_playing_msg, "Enjoying the Game?", "Hi " + LoginsTable.GetDisplayName(dbs, send_why_not_playing_msg) + ", apologies for the intrusion but I was wondering how you were finding the game?  If you've stopped playing, that's okay, but we would be very grateful if you could let us know the reason so we could improve the game for other players.  Just reply to this to let us know what you think.\n\nThanks in advance!\n\nSteve");
							LoginsTable.SetHasBeenSentWhyNotPlayingMsg(dbs, send_why_not_playing_msg);
						}
					}*/
					int add_to_reserve = this.headers.getGetOrPostValueAsInt("add_to_reserve");
					if (add_to_reserve > 0) {
						if (ReservePlayersTable.IsPlayerOnList(dbs, add_to_reserve) == false) {
							ReservePlayersTable.AddRec(dbs, add_to_reserve);
						} else {
							ReservePlayersTable.DelRec(dbs, add_to_reserve);
						}
					}
				}
			}

			StringBuffer str = new StringBuffer();

			login.selectRow(loginid);

			str.append("<img src=\"/dsr/gettitle.cls?t=" + login.getDisplayName_Enc(false).replaceAll(" ", "%20") + "&h=65\" border=0 /></a><br /><br />");

			if (login.getFactionID() > 0) {
				str.append("<img src=\"" + DSRWebServer.IMAGES_HOST + "/images/factions/" + FactionsTable.GetImageFilename(dbs, login.getFactionID()) + "\" width=200 align=\"right\" />");
			}

			for (int i=1 ; i<= PlayerTrophiesTable.MAX_TROPHIES ; i++) {
				if (PlayerTrophiesTable.DoesPlayerHaveTrophy(dbs, login.getID(), i)) {
					str.append("<img height=\"75\" src=\"" + PlayerTrophiesTable.GetTrophyImageURL(i) + "\" alt=\"" + PlayerTrophiesTable.GetTrophyName(i) + "\" />");
				}
			}

			HTMLFunctions.StartTable(str);
			HTMLFunctions.StartRow(str);
			HTMLFunctions.StartCell(str);

			HTMLFunctions.StartTable(str);

			if (this.session.isLoggedIn()) {
				if (this.current_login.isAdmin()) {
					HTMLFunctions.StartRow(str);
					HTMLFunctions.AddCell(str, "Email/Pwd: ");
					HTMLFunctions.AddCell(str, login.getEmail() + "/" + login.getPassword());
					HTMLFunctions.EndRow(str);
				}
			}
			if (login.getFactionID() > 0) {
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, "Faction: ");
				HTMLFunctions.AddCell(str, FactionsTable.GetName(dbs, login.getFactionID()));// + " (contributes " + login.getFactionPoints() + " points)");
				HTMLFunctions.EndRow(str);
			}
			if (login.isAdmin()) {
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, "This user as an admin user");
				HTMLFunctions.EndRow(str);
			}

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Date Joined: ");
			HTMLFunctions.AddCell(str, "" + Dates.FormatDate(login.getDateJoined(), Dates.UKDATE_FORMAT));
			HTMLFunctions.EndRow(str);

			/*if (this.session.isLoggedIn()) {
				if (this.current_login.isAdmin()) {*/
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Last Date Login Used: ");
			HTMLFunctions.AddCell(str, "" + Dates.FormatDate(login.getLastLoginDate(), Dates.UKDATE_FORMAT));
			HTMLFunctions.EndRow(str);
			/*}
			}*/

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "League Points: ");
			HTMLFunctions.AddCell(str, "" + login.getELOPoints());
			HTMLFunctions.EndRow(str);

			/*HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Games Finished: ");
			HTMLFunctions.AddCell(str, "" + login.getTotalGamesFinished());
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "<nobr>Total Current Games: </nobr>");
			HTMLFunctions.AddCell(str, "" + login.getTotalCurrentGames(true));
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Victories: ");
			HTMLFunctions.AddCell(str, "" + login.getTotalVictories(true, 0, -1, true));
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Draws: ");
			HTMLFunctions.AddCell(str, "" + login.getTotalDraws());
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Defeats: ");
			HTMLFunctions.AddCell(str, "" + login.getTotalDefeats());
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Concedes: ");
			HTMLFunctions.AddCell(str, "" + login.getTotalConcedes());
			HTMLFunctions.EndRow(str);*/

			// Trophies
			if (PlayerTrophiesTable.DoesPlayerHaveAnyTrophy(dbs, login.getID())) {
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, "Trophies:" + helpwindow.GetLink(helpwindow.HC_TROPHIES));
				str.append("<td>");
				for (int i=1 ; i<= PlayerTrophiesTable.MAX_TROPHIES ; i++) {
					if (PlayerTrophiesTable.DoesPlayerHaveTrophy(dbs, login.getID(), i)) {
						str.append(PlayerTrophiesTable.GetTrophyName(i) + "<br>");
						//str.append("<img width=\"150\" src=\"" + PlayerTrophiesTable.GetTrophyImageURL(i) + "\" alt=\"" + PlayerTrophiesTable.GetTrophyName(i) + "\" /><br>");
					}
				}
				str.append("</td>");
				HTMLFunctions.EndRow(str);
			}

			// Awards
			if (PlayerAwardsTable.DoesPlayerHaveAward(dbs, login.getID())) {
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, "Awards:");
				str.append("<td>");
				String sql = "SELECT * FROM PlayerAwards WHERE LoginIDTo = " + login.getID() + " ORDER BY DateCreated DESC";
				ResultSet rs = dbs.getResultSet(sql);
				while (rs.next()) {
					str.append(HTMLFunctions.s2HTML(rs.getString("Name")) + " (from " + LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginIDFrom"), true) + "), ");
				}
				str.delete(str.length()-2, str.length());
				str.append("</td>");
				HTMLFunctions.EndRow(str);
			}

			if (login.getTotalTurns() != 0) {
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, "Average Days Per Turn: ");
				if (login.getTotalTurns() != 0) {
					try {
						float f = (float)login.getTotalDaysTakingTurns() / (float)login.getTotalTurns();
						HTMLFunctions.AddCell(str, "" + f);
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex, true);
					}
				} else {
					HTMLFunctions.AddCell(str, "n/a");
				}
				HTMLFunctions.EndRow(str);
			}

			// Arch nemesis
			try {
				String sql = "select PID, count(*) as Tot from (";
				for (int i=1 ; i<=4 ; i++) {
					sql = sql + " select Player1ID as PID from Games WHERE " + leaguetable.GetQual() + " AND (Player1ID = " + login.getID() + " or Player2ID = " + login.getID() + " or player3ID = " + login.getID() + " or player4id = " + login.getID() + ") and WinningSide <> " + i;
					if (i < 4) {
						sql = sql + " UNION ALL";
					}
				}
				sql = sql + ") X where PID <> " + login.getID() + " and PID <> 0 and PID IS NOT NULL";
				sql = sql + " group by pid order by Tot desc";
				ResultSet rs = dbs.getResultSet(sql);
				if (rs.next()) {
					HTMLFunctions.StartRow(str);
					HTMLFunctions.AddCell(str, "Arch Nemesis " + helpwindow.GetLink(helpwindow.HC_ARCH_NEMESIS) + ": ");
					HTMLFunctions.AddCell(str, LoginsTable.GetDisplayName_Enc(dbs, rs.getInt(1), true));
					HTMLFunctions.EndRow(str);
				}

			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Forum Posts: ");
			int tot = login.getTotalForumPosts();
			try {
				MySQLConnection phpdbs = DSRWebServer.getPHPDbs();
				if (phpdbs != null) {
					tot += phpdbs.getScalarAsInt("SELECT Count(*) FROM phpbb_posts WHERE poster_id = (SELECT user_id FROM phpbb_users WHERE username = '" + login.getDisplayName_Enc(false) + "')");
				}
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}
			HTMLFunctions.AddCell(str, "" + tot);
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Concedes: ");
			HTMLFunctions.AddCell(str, "" + login.getTotalConcedes());
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Favourite Mission: ");
			try {
				HTMLFunctions.AddCell(str, GetMostPopularMission(dbs, login.getID()));
			} catch (Exception ex) {
				//DSRWebServer.HandleError(ex, true);
			}
			HTMLFunctions.EndRow(str);

			if (this.session.isLoggedIn()) {
				if (this.current_login.getID() != login.getID()) {
					HTMLFunctions.StartRow(str);
					HTMLFunctions.AddCell(str, "Points for winning/losing against: ");
					try {
						HTMLFunctions.AddCell(str, leaguetable.GetPointsForWinAgainst(this.current_login.getELOPoints(), login.getELOPoints()) + "/" + leaguetable.GetPointsForLoseAgainst(this.current_login.getELOPoints(), login.getELOPoints()));
					} catch (Exception ex) {
						//DSRWebServer.HandleError(ex, true);
					}
					HTMLFunctions.EndRow(str);
				}
			}

			if (login.getCampTeamName().length() > 0) {
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, "Campaign Squad Name: ");
				HTMLFunctions.AddCell(str, "" + login.getCampTeamName());
				HTMLFunctions.EndRow(str);
			}

			/*HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Has Android client? ");
			if (login.hasAndroid()) {
				HTMLFunctions.AddCell(str, "Yes!");
			} else {
				HTMLFunctions.AddCell(str, "No");
			}					
			HTMLFunctions.EndRow(str);*/

			if (login.getLocation() != null) {
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, "Location: ");
				HTMLFunctions.AddCell(str, "" + login.getLocation());
				HTMLFunctions.EndRow(str);
			}

			if (login.getAboutMe().length() > 0) {
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, "About Me: ");
				HTMLFunctions.AddCell(str, "" + login.getAboutMe());
				HTMLFunctions.EndRow(str);
			}

			if (login.getWebsite().length() > 0) {
				if (login.getWebsite().equalsIgnoreCase("http://") == false) {
					HTMLFunctions.StartRow(str);
					HTMLFunctions.AddCell(str, "Website: ");
					String website = login.getWebsite();
					HTMLFunctions.AddCell(str, "<a href=\"" + website + "\">" + website + "</a>");
					HTMLFunctions.EndRow(str);
				}
			}

			/*HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "<a href=\"leaguepointsgraph.cls?loginid=" + loginid + "\">Show league points graph</a>");
			HTMLFunctions.EndRow(str);
*/
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "<a href=\"finishedgames.cls?opponent=" + loginid + "\">Show your record against this player</a>");
			HTMLFunctions.EndRow(str);

			if (this.session.isLoggedIn()) {
				if (this.current_login.isAdmin()) {
					HTMLFunctions.StartRow(str);
					HTMLFunctions.AddCell(str, "<a href=\"?loginid=" + loginid + "&amp;disable=" + loginid + "\">Disable this login</a>");
					if (login.isDisabled()) {
						HTMLFunctions.AddCell(str, "This login is disabled!");
					}
					HTMLFunctions.EndRow(str);

					HTMLFunctions.StartRow(str);
					if (login.hasBeenSentInvalidEmailMsg()) {
						HTMLFunctions.AddCell(str, "This login has been sent an 'invalid email' msg.");
					} else {
						HTMLFunctions.AddCell(str, "<a href=\"?loginid=" + loginid + "&amp;send_invalid_email_msg=" + loginid + "\">Send 'Invalid Email' Msg</a>");
					}
					HTMLFunctions.EndRow(str);

					/*HTMLFunctions.StartRow(str);
					if (login.hasBeenSentWhyNotPlayingMsg()) {
						HTMLFunctions.AddCell(str, "This login has been sent a 'why not playing' msg.");
					} else {
						HTMLFunctions.AddCell(str, "<a href=\"?loginid=" + loginid + "&amp;send_why_not_playing_msg=" + loginid + "\">Send 'Why Not Playing' Msg</a>");
					}
					HTMLFunctions.EndRow(str);*/

					HTMLFunctions.StartRow(str);
					if (ReservePlayersTable.IsPlayerOnList(dbs, loginid)) {
						HTMLFunctions.AddCell(str, "This login is on the Reserve Players list. <a href=\"?loginid=" + loginid + "&amp;add_to_reserve=" + loginid + "\">remove</a>");
					} else {
						HTMLFunctions.AddCell(str, "<a href=\"?loginid=" + loginid + "&amp;add_to_reserve=" + loginid + "\">Add to Reserve Players List</a>");
					}
					HTMLFunctions.EndRow(str);

				}
			}

			HTMLFunctions.EndTable(str);

			HTMLFunctions.StartCell(str);

			// Game stats
			//HTMLFunctions.StartTable(str);
			HTMLFunctions.StartTable(str, "stats", "", 1, "", 5);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCellHeading(str, "GAME STATS");
			HTMLFunctions.AddCellHeading(str, "Full");
			HTMLFunctions.AddCellHeading(str, "Practise");
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Games Finished: ");
			HTMLFunctions.AddCell(str, "" + login.getTotalGamesFinished(LoginsTable.GameType.ONLY_NON_PRACTISE));
			HTMLFunctions.AddCell(str, "" + login.getTotalGamesFinished(LoginsTable.GameType.ONLY_PRACTISE));
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "<nobr>Total Current Games: </nobr>");
			HTMLFunctions.AddCell(str, "" + login.getTotalCurrentGames(LoginsTable.GameType.ONLY_NON_PRACTISE));
			HTMLFunctions.AddCell(str, "" + login.getTotalCurrentGames(LoginsTable.GameType.ONLY_PRACTISE));
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Victories: ");
			HTMLFunctions.AddCell(str, "" + login.getTotalVictories(LoginsTable.GameType.ONLY_NON_PRACTISE));
			HTMLFunctions.AddCell(str, "" + login.getTotalVictories(LoginsTable.GameType.ONLY_PRACTISE));
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Draws: ");
			HTMLFunctions.AddCell(str, "" + login.getTotalDraws(LoginsTable.GameType.ONLY_NON_PRACTISE));
			HTMLFunctions.AddCell(str, "" + login.getTotalDraws(LoginsTable.GameType.ONLY_PRACTISE));
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Defeats: ");
			HTMLFunctions.AddCell(str, "" + login.getTotalDefeats(LoginsTable.GameType.ONLY_NON_PRACTISE));
			HTMLFunctions.AddCell(str, "" + login.getTotalDefeats(LoginsTable.GameType.ONLY_PRACTISE));
			HTMLFunctions.EndRow(str);

			/*HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Concedes: ");
			HTMLFunctions.AddCell(str, "" + login.getTotalConcedes(LoginsTable.GameType.ONLY_NON_PRACTISE));
			HTMLFunctions.AddCell(str, "" + login.getTotalConcedes(LoginsTable.GameType.ONLY_PRACTISE));
			HTMLFunctions.EndRow(str);*/

			HTMLFunctions.EndTable(str);
			
			
			HTMLFunctions.EndCell(str);

			// Message section
			HTMLFunctions.StartCell(str);

			// Check we haven't just sent a message
			if (this.session.isLoggedIn()) {
				// Change email address
				if (this.current_login.isAdmin()) {
					str.append("Update email address");
					HTMLFunctions.StartForm(str, "Form_email", "", "POST");
					HTMLFunctions.HiddenValue(str, "next", "change_email");
					HTMLFunctions.HiddenValue(str, "loginid", login.getID());
					HTMLFunctions.TextBox(str, "email", "", false, 128, 50);
					HTMLFunctions.SubmitButton(str, "Send");
					HTMLFunctions.EndForm(str);
				}

				// Check we're not sending to ourselves.
				if (this.current_login.getID() != login.getID()) {

					if (this.current_login.getTotalTurns() > 0) { // Prevent spam
						HTMLFunctions.Heading(str, 3, "Send message to " + login.getDisplayName_Enc(false));
						if (next.equalsIgnoreCase("reply")) {
							MessagesTable message = new MessagesTable(dbs);
							message.selectRow(this.headers.getGetValueAsInt("msgid"));
							msg = msg + "\n\n\n-----------------------\n" + login.getDisplayName_Enc(false) + " wrote:\n\n> " + message.getMessage(); 
							login.selectRow(message.getFromID());
							subject = message.getSubject();
							if (subject != null) {
								if (subject.startsWith("Re:") == false) {
									subject = "Re: " + subject;
								}
							}
							message.close();
							
						}
						HTMLFunctions.StartForm(str, "Form1", "", "POST");
						HTMLFunctions.HiddenValue(str, "next", "send_msg");
						HTMLFunctions.HiddenValue(str, "loginid", login.getID());
						str.append("Subject:<br />");
						HTMLFunctions.TextBox(str, "subject", subject, false, 128, 50);
						str.append("<br /><br />Message:<br />");
						HTMLFunctions.TextArea(str, "msg", 10, 60, msg, false);
						str.append("<br />");
						HTMLFunctions.SubmitButton(str, "Send");
						HTMLFunctions.EndForm(str);
					}
					str.append("<hr />");//&nbsp;&nbsp;&nbsp;");
					if (PlayerAwardsTable.CanPlayerMakeAward(dbs, this.current_login)) {
						HTMLFunctions.Heading(str, 3, "Send Award To " + login.getDisplayName_Enc(false));

						HTMLFunctions.StartForm(str, "Form2", "", "POST");
						HTMLFunctions.HiddenValue(str, "next", "send_award");
						HTMLFunctions.HiddenValue(str, "loginid", login.getID());
						str.append("Award for ...<br />");
						HTMLFunctions.TextBox(str, "award_name", award_name, false, 128, 50);
						str.append("<br />");
						HTMLFunctions.SubmitButton(str, "Send");
						HTMLFunctions.EndForm(str);

					} else {
						HTMLFunctions.Para(str, "You can't give an award: either you've given one in the last 7 days or not won for 7 days.");
					}
				}
			}

			HTMLFunctions.EndCell(str);
			HTMLFunctions.EndRow(str);
			HTMLFunctions.EndTable(str);

			this.body_html.append(MainLayout.GetHTML(this, "Stats for player", str));		
		}
		login.close();
	}


	public static String GetMostPopularMission(MySQLConnection dbs, int loginid) throws SQLException {
		String sql = "SELECT Mission, Count(*) FROM Games WHERE " + GamesTable.GetPlayerSubQuery(loginid) + " AND DATEDIFF(NOW(), DateCreated) < " + leaguetable.DAYS + " AND Mission != " + AbstractMission.SF_PRACTISE_MISSION + " AND Mission != " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI + " GROUP BY Mission ORDER BY Count(*) DESC";
		ResultSet rs = dbs.getResultSet(sql);
		if (rs.next()) {
			return AbstractMission.GetMissionNameFromType(rs.getInt("Mission"), true, false);
		} else {
			return "None yet";
		}
	}
}


