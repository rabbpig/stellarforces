package dsrwebserver.pages.dsr.admin;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.html.HTMLFunctions;
import ssmith.io.TextFile;
import ssmith.lang.Functions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.ArmourTypesTable;
import dsrwebserver.tables.EquipmentTypesTable;

public final class createdata extends AbstractHTMLPage {

	public createdata() {
		super();
	}

	@Override
	public void process() throws Exception {
		if (this.session.isLoggedIn()) {
			if (this.current_login.isAdmin()) {
				ImportData(dbs);
				HTMLFunctions.Para(this.body_html, "File imported.");
			}
		}
	}


	public static void ImportData(MySQLConnection dbs) throws FileNotFoundException, IOException, SQLException {
		// Import equipment
		TextFile tf = new TextFile();
		tf.openFile(DSRWebServer.SERVER_DATA_DIR + "equipment.csv", TextFile.READ);
		String header = tf.readLine(); // Header
		// Check header has tabs
		if (header.indexOf("\t") < 0) {
			throw new RuntimeException("No tabs in import file.");
		}
		while (tf.isEOF() == false) {
			String s = tf.readLine();
			if (s.length() > 0 && s.startsWith("#") == false) {
				String data[] = s.split("\t");
				if (data.length > 1) {
					EquipmentTypesTable.AddRec(dbs, Functions.ParseInt(data[0]), data[1].replaceAll("\"", ""), data[2].replaceAll("\"", ""), Functions.ParseInt(data[3]), Functions.ParseInt(data[4]), Functions.ParseInt(data[5]), Functions.ParseInt(data[6]), Functions.ParseInt(data[7]), Functions.ParseInt(data[8]), Functions.ParseInt(data[9]), Functions.ParseInt(data[10]), Functions.ParseInt(data[11]), Functions.ParseInt(data[12]), Functions.ParseInt(data[13]), Functions.ParseInt(data[14]), Functions.ParseInt(data[15]), Functions.ParseInt(data[16]), Functions.ParseInt(data[17]), Functions.ParseInt(data[18]), Functions.ParseInt(data[19]), Functions.ParseInt(data[20]), Float.parseFloat(data[21]), data[22].replaceAll("\"", ""), Functions.ParseInt(data[23]), Functions.ParseInt(data[24]), Functions.ParseInt(data[25]), Functions.ParseInt(data[26]), Functions.ParseInt(data[27]));
				}
			}
		}
		tf.close();

		ArmourTypesTable.AddRec(dbs, "Light", 10, 5, 8, 0);
		ArmourTypesTable.AddRec(dbs, "Medium", 23, 9, 13, 0);
		ArmourTypesTable.AddRec(dbs, "Heavy", 36, 14, 17, 0);
		ArmourTypesTable.AddRec(dbs, "Super-Heavy", 46, 24, 35, 0);
		ArmourTypesTable.AddRec(dbs, "Techno", 36, 5, 26, 1);

	}

}
