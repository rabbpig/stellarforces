package dsrwebserver.pages.dsr.admin;

import java.sql.ResultSet;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.html.HTMLFunctions;

import dsrwebserver.DSRWebServer;
import dsrwebserver.pages.AbstractHTMLPage;

public class updatephpbb extends AbstractHTMLPage {

	public updatephpbb() {
		super();
	}

	@Override
	public void process() throws Exception {
		ResultSet rs = dbs.getResultSet("SELECT * FROM Logins");

		StringBuffer log = new StringBuffer();

		MySQLConnection php = DSRWebServer.getPHPDbs();
		while (rs.next()) {
			//if (rs.getString("DisplayName").equalsIgnoreCase("SteveSmith")) {
			int c = php.runSQLUpdate("UPDATE phpbb_users SET user_Password = MD5(" + SQLFuncs.s2sql(rs.getString("Pwd")) + "), user_email = " + SQLFuncs.s2sql(rs.getString("Login"))  + ", user_email_hash = crc32(" + SQLFuncs.s2sql(rs.getString("Login"))  + ") WHERE UserName = " + SQLFuncs.s2sql(rs.getString("DisplayName"))  + "");// AND  len(user_Password) > 0");
			if (c == 0) {
				log.append("User " + rs.getString("DisplayName") + " not found in phpbb\n");
			}
			//}

			// Bans
			if (rs.getInt("Disabled") == 1) {
				ResultSet rs_php = php.getResultSet("SELECT user_id FROM phpbb_users WHERE username = " + SQLFuncs.s2sql(rs.getString("DisplayName")));
				if (rs_php.next()) {
					int phpuserid = rs_php.getInt("user_id");
					if (php.getScalarAsInt("SELECT Count(*) FROM phpbb_banlist WHERE ban_userid = " + phpuserid) == 0) {
						HTMLFunctions.Para(this.body_html, "Banning " + rs.getString("DisplayName"));
						php.RunIdentityInsert("insert into phpbb_banlist (ban_userid, ban_start, ban_end, ban_exclude) values (" + phpuserid + ", " + System.currentTimeMillis()/1000 + ", 0, false)");
					}
				}
			}

		}

		if (log.length() > 0) {
			DSRWebServer.SendEmailToAdmin("Missing players", log.toString());
		}

		this.body_html.append("Finished");

	}

}