package dsrwebserver.pages.dsr.admin;

import java.sql.ResultSet;

import ssmith.html.HTMLFunctions;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.LoginsTable;

public class sendspam extends AbstractHTMLPage {

	public sendspam() {
		super();
	}
	

	public void process() throws Exception {
		if (this.session.isLoggedIn()) {
			if (this.current_login.isAdmin()) {
				this.setTitle("Send Emails");

				StringBuffer str = new StringBuffer();

				String cmd = this.headers.getGetOrPostValueAsString("cmd");
				if (cmd.length() > 0) {
					//String email_text = TextFile.ReadAll("./emails/email3.txt", "\n", true);

					LoginsTable login = new LoginsTable(dbs);
					login.selectUser("todo", "todo");
					int ss_id = login.getID();

					String sql = "SELECT LoginID FROM Logins WHERE DATEDIFF(now(), LastLoginDate) < 21";
					sql = sql + " AND COALESCE(OptedOutOfEmails, 0) = 0";
					sql = sql + " AND COALESCE(Disabled, 0) = 0";
					sql = sql + " AND TotalTurns > 10";
					ResultSet rs = dbs.getResultSet(sql);
					int count = 0;
					while (rs.next()) {
						login.selectRow(rs.getInt("LoginID"));
						try {
							login.sendMsg(ss_id, "New Knockout Competition", "Hi, this message is to let you know that the third Stellar Force Knockout Competition is about to start!  Everyone is welcome to join by adding their name to the forum at http://www.stellarforces.com/dsr/forums/ForumPostingsPage.cls?topic=14739 .  Good luck!\nSteve");
							HTMLFunctions.Para(str, "Sent msg to " + login.getDisplayName());
						} catch (Exception ex) {
							ex.printStackTrace();
						}
						
						login.setDateLastMktEmailRcvd();
						count++;
					}
					HTMLFunctions.Para(str, "Finished.  " + count + " sent.");
					login.close();

				} else {
					HTMLFunctions.Para(str, "No command.");
				}

				body_html.append(MainLayout.GetHTML(this, "Send Emails", str));
			}
		}
	}

}
