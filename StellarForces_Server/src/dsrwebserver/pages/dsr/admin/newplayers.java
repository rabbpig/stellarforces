package dsrwebserver.pages.dsr.admin;

import java.sql.ResultSet;

import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.LoginsTable;

public class newplayers extends AbstractHTMLPage {

	public newplayers() {
		super();
	}

	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		if (this.session.isLoggedIn()) {
			if (this.current_login.isAdmin()) {
				str.append("<table class=\"stats\">");
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCellHeading(str, "Name");
				HTMLFunctions.AddCellHeading(str, "Email");
				HTMLFunctions.AddCellHeading(str, "Created");
				HTMLFunctions.EndRow(str);

				String sql = "select LoginID, Login, DateCreated from Logins where DATEDIFF(curdate(), DateCreated) < 14 and TotalTurns = 0 and coalesce(Disabled, 0) = 0"; // DATEDIFF(curdate(), DateCreated) > 4 and 
				sql = sql + " AND LoginID NOT IN (SELECT UserID FROM ForumPostings)";
				sql = sql + " AND LoginID NOT IN (SELECT Player1ID FROM GameRequests)";
				sql = sql + " ORDER BY DateCreated";
				ResultSet rs = dbs.getResultSet(sql);
				while (rs.next()) {
					HTMLFunctions.StartRow(str);
					HTMLFunctions.AddCell(str, LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginID"), true));
					HTMLFunctions.AddCell(str, rs.getString("Login"));
					HTMLFunctions.AddCell(str, Dates.FormatDate(rs.getTimestamp("DateCreated"), Dates.UKDATE_FORMAT));
					HTMLFunctions.EndRow(str);
				}

				HTMLFunctions.EndTable(str);

				this.body_html.append(MainLayout.GetHTML(this, "New Players", str));		
			} else {
				this.redirectTo_Using303("/");
			}
		} else {
			this.redirectTo_Using303("/");
		}
	}


}
