package dsrwebserver.pages.dsr;

import ssmith.html.HTMLFunctions;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.GameRequestsTable;
import dsrwebserver.tables.GamesTable;

/**
 * This is the page that starts a game.
 *
 */
public final class JoinGame extends AbstractHTMLPage {

	public JoinGame() {
		super();
	}

	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();
		if (this.session.isLoggedIn()) {
			int reqid = this.headers.getGetOrPostValueAsInt("reqid");
			int player2side = this.headers.getGetOrPostValueAsInt("player2side");
			GameRequestsTable requests = new GameRequestsTable(dbs);
			try {
			if (requests.doesRowExist(reqid) == false) {
				HTMLFunctions.Para(str, "Sorry, that request does not seem to exist any more.");
				this.body_html.append(str);
				return; // Abandon!
			}
			HTMLFunctions.Para(str, "Please wait...");
			requests.selectRow(reqid);
			GamesTable game = new GamesTable(dbs);

			try {
				requests.acceptRequest(current_login, game, player2side);
				this.redirectTo_UsingRefresh("/dsr/BuyArmour.cls?gid=" + game.getID(), 0);
			} catch (RuntimeException ex) {
				HTMLFunctions.Para(str, ex.getMessage());

			}
			} finally {
				requests.close();
			}
		}
		this.body_html.append(MainLayout.GetHTML(this, "Join Game", str));		
	}

}
