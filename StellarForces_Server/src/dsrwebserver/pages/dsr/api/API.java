package dsrwebserver.pages.dsr.api;

import java.io.IOException;

import ssmith.lang.Functions;
import dsrwebserver.pages.AbstractPage;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.SessionsTable;

/*

/stellarforces.com/API/get/map/id - return specific map
/stellarforces.com/API/get/item/id - return item details
/stellarforces.com/API/post/message/ - this could send a message to a use, using the post data for things such as sender id, reciver id, subject and body
/stellarforces.com/API/post/move/ - could submit a new move, post data containing all the specifics
/stellarforces.com/API/post/equip/ - purchase and equip a soldier, making use of html5 to provide a fluid and interactive equipping screen.

 */
public class API extends AbstractPage {

	private StringBuffer response = new StringBuffer();
	//private String request;

	public API() { //String req) {
		super();

		//request = req;
	}


	@Override
	public void process() throws Exception {
		String req = super.headers.line1.toLowerCase(); 
		SessionsTable session = new SessionsTable(dbs);
		LoginsTable login_tbl = new LoginsTable(dbs);

		if (req.indexOf("/get/token/") > 0) {
			// Create token
			String login = super.headers.getGetOrPostValueAsString("login");
			String pwd = super.headers.getGetOrPostValueAsString("pwd");

			if (login_tbl.selectUser(login, pwd)) {
				String token = "" + System.currentTimeMillis() + Functions.rnd(1000, 9999);

				session.selectSessionByCookie(token);
				session.setLoggedIn(login_tbl.getID(), true);

				response.append("response({status:200,data:{token:" + token + "}});");
			} else {
				response.append("Invalid login/pwd");
				this.setHTTPCode(401);
				Functions.delay(3000); // Prevent multiple attempts too quickly
			}
		} else {
			// Validate user
			String login = super.headers.getGetOrPostValueAsString("login");
			String token = super.headers.getGetOrPostValueAsString("token");

			if (session.selectSessionByCookie_NoAdd(token)) {
				if (session.getLoginID() > 0 && session.isLoggedIn()) {
					login_tbl.selectRow(session.getLoginID());
					if (login_tbl.getLogin().equalsIgnoreCase(login) || login_tbl.getDisplayName().equalsIgnoreCase(login)) {
						int loginid = session.getLoginID();
						// Success!
						if (req.indexOf("/get/messages") > 0) {
							response.append(MessagesAPI.GetUsersMessages(dbs, loginid));
						} else if (req.indexOf("/get/message/") > 0) {
							int id = this.headers.getGetOrPostValueAsInt("id");
							response.append(MessagesAPI.GetMessage(dbs, loginid, id));
						} else if (req.indexOf("/post/message/") > 0) {
							MessagesAPI.PostMessage(response, dbs, loginid, this.headers);
							if (response.length() > 0) {
								this.setHTTPCode(400); // Bad request
							}
						}
					} else {
						response.append("Invalid security token");
						this.setHTTPCode(401); // Forbidden
						Functions.delay(3000); // Prevent multiple attempts too quickly
					}
				} else {
					response.append("Invalid security token");
					this.setHTTPCode(401);// Forbidden
					Functions.delay(3000); // Prevent multiple attempts too quickly
				}
			} else {
				response.append("Invalid security token");
				this.setHTTPCode(401);// Forbidden
				Functions.delay(3000); // Prevent multiple attempts too quickly
			}
		}
		login_tbl.close();
		
		this.content_length = response.length();
	}


	@Override
	protected void writeContent() throws IOException {
		this.writeString(response.toString());
	}


}
