package dsrwebserver.pages.dsr.api;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.html.HTMLFunctions;
import dsrwebserver.HTTPHeaders;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MessagesTable;

public class MessagesAPI {
	
	public static String GetUsersMessages(MySQLConnection dbs, int loginid) throws SQLException {
		StringBuffer str = new StringBuffer();
		
		String sql = "SELECT * FROM Messages WHERE (ToID = " + loginid + " OR FromID = " + loginid + ") ";
		sql = sql + " ORDER BY DateCreated DESC LIMIT 50";
		ResultSet rs = dbs.getResultSet(sql);
		str.append("response({status:200, data:{messages:[\n");
		while (rs.next()) {
			AppendMessage(str, dbs, rs, false);
		}
		str.delete(str.length()-3, str.length()-1); // Remove last comma
		str.append("]}})\n");

		
		return str.toString();
	}
	
	
	public static String GetMessage(MySQLConnection dbs, int loginid, int msgid) throws SQLException {
		StringBuffer str = new StringBuffer();
		
		String sql = "SELECT * FROM Messages WHERE MessageID = " + msgid + " AND (ToID = " + loginid + " OR FromID = " + loginid + ")";
		ResultSet rs = dbs.getResultSet(sql);
		str.append("response( {status:200, data:{\n");
		while (rs.next()) {
			AppendMessage(str, dbs, rs, true);
		}
		str.delete(str.length()-3, str.length()-1); // Remove last comma
		str.append("}})\n");

		
		return str.toString();
	}
	
	
	private static void AppendMessage(StringBuffer str, MySQLConnection dbs, ResultSet rs, boolean inc_body) throws SQLException {
		str.append("{\n");
		AppendField(str, "messageid", rs.getInt("MessageID"));
		AppendField(str, "read", rs.getInt("MsgRead"));
		AppendField(str, "fromid", rs.getInt("FromID"));
		AppendField(str, "fromName", LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("FromID"), false));
		AppendField(str, "toid", rs.getInt("ToID"));
		AppendField(str, "toName", LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("ToID"), false));
		AppendField(str, "subject", HTMLFunctions.s2HTML(rs.getString("Subject")));
		if (inc_body) {
			AppendField(str, "body", HTMLFunctions.s2HTML(rs.getString("Message")).replaceAll("\n", "<br />"));
		} else {
			str.delete(str.length()-3, str.length()-1); // Remove last comma
			//str.append("\n");
		}
		str.append("}, \n");
		
	}
	
	public static void AppendField(StringBuffer str, String field, String val) {
		str.append("\"" + field + "\": \"" + val + "\", \n");
	}


	public static void AppendField(StringBuffer str, String field, int val) {
		str.append("\"" + field + "\": \"" + val + "\", \n");
	}


	/**
	 * Returns a blank string on success.
	 */
	public static void PostMessage(StringBuffer str, MySQLConnection dbs, int loginid, HTTPHeaders headers) throws SQLException {
		int toid = headers.getGetOrPostValueAsInt("toid");
		String subject = headers.getGetOrPostValueAsString("subject");
		String body = headers.getGetOrPostValueAsString("body");
		if (toid <= 0) {
			str.append("Invalid toid");
			return;
		}
		if (subject.length() == 0) {
			str.append("No subject");
			return;
		}
		if (body.length() == 0) {
			str.append("No body");
			return;
		}
		//MessagesTable msg_tbl = new MessagesTable(dbs);
		MessagesTable.SendMsg(dbs, loginid, toid, subject, body);
	}
	

}
