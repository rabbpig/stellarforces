package dsrwebserver.pages.dsr;

import ssmith.html.HTMLFuncs_old;
import ssmith.html.HTMLFunctions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.LoginsTable;

public class forgottenpassword extends AbstractHTMLPage {

	public forgottenpassword() {
		super();
	}

	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		String email = this.headers.getPostValueAsString("email");
		if (email.length() > 0) {
			LoginsTable login = new LoginsTable(dbs);
			if (login.selectUserByEmailAddress(email)) {
				if (login.isDisabled() == false) {
					login.sendEmail("Password Reminder", "This is an automated email.  Your password for Stellar Forces is '" + login.getPassword() + "'.");
					HTMLFunctions.Para(str, "An email has been sent.");
				} else {
					HTMLFunctions.Para(str, "Sorry, that login has been disabled due to lack of use and prevent spam attacks.  Please re-register with a new login or email " + DSRWebServer.EMAIL_ADDRESS + " to re-enable it.");
					//DSRWebServer.SendEmailToAdmin("Disabled login's password requested", "User " + login.getDisplayName() + " (" + email + ") has forgotten their password.");
				}
			} else {
				HTMLFunctions.Para(str, "Sorry, that email address cannot be found.");
			}
			login.close();
		} else {
			str.append("<center>");
			HTMLFunctions.StartForm(str, "form1", "", "post");
			//HTMLFunctions.HiddenValue(str, "loginredir", correct_referrer.replace("=", "%3D"));
			HTMLFunctions.StartTable(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "If you have forgotten your password, please enter your email address here and it will be emailed to you.");
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, HTMLFuncs_old.TextBox("email", "", 50));
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, HTMLFuncs_old.SubmitButton("Send Password"));
			HTMLFunctions.EndRow(str);

			HTMLFunctions.EndTable(str);
			HTMLFunctions.EndForm(str);
			str.append("</center>");
		}

		this.body_html.append(MainLayout.GetHTML(this, "Forgotten Password", str));		
	}

}
