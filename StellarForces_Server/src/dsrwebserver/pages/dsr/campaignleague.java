package dsrwebserver.pages.dsr;



public class campaignleague { //extends AbstractHTMLPage { /*implements Comparator<campaignleague.Entry> {
/*
	public static StringBuffer str_units, str_players;
	private static Interval update_forum_interval = new Interval(1000 * 60 * 10, true);

	public campaignleague() {
		super();
	}


	@Override
	public void process() throws Exception {
		/*StringBuffer str = new StringBuffer();

		if (update_forum_interval.hitInterval() || str_players == null || str_units == null) {
			str_players = new StringBuffer();
			appendTopPlayers(str_players);
			
			str_units = new StringBuffer();
			HTMLFunctions.Heading(str_units, 3, "Top Campaign Units");
			appendTopUnits(str_units);
		}
		HTMLFunctions.Heading(str, 3, "Top Players");
		str.append(str_players);
		str.append(str_units);
		this.body_html.append(MainLayout.GetHTML(this, "Campaign League Table", str));		*/
	}
/*

	private void appendTopUnits(StringBuffer str) throws SQLException {
		ResultSet rs = dbs.getResultSet("SELECT CampUnitID FROM CampUnits WHERE RankValue > 0 ORDER BY RankValue DESC LIMIT 10");
		HTMLFunctions.StartTable(str_units, "stats", "100%", 1, "", 5);
		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCellHeading(str, "Pos");
		HTMLFunctions.AddCellHeading(str, "Name");
		HTMLFunctions.AddCellHeading(str, "Commander");
		HTMLFunctions.AddCellHeading(str, "Missions");
		HTMLFunctions.AddCellHeading(str, "Victories");
		HTMLFunctions.AddCellHeading(str, "Kills");
		HTMLFunctions.AddCellHeading(str, "Friendly Kills");
		HTMLFunctions.AddCellHeading(str, "Rank Points");
		HTMLFunctions.EndRow(str);

		int i = 1;
		CampUnitsTable campunit = new CampUnitsTable(dbs);
		while (rs.next()) {
			campunit.selectRow(rs.getInt("CampUnitID"));
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "" + i);
			HTMLFunctions.AddCell(str, campunit.getName());
			HTMLFunctions.AddCell(str, LoginsTable.GetDisplayName_Enc(dbs, campunit.getOwnerID(), true));
			HTMLFunctions.AddCell(str, "" + campunit.getTotalMissions());
			HTMLFunctions.AddCell(str, "" + campunit.getTotalVictories());
			HTMLFunctions.AddCell(str, "" + campunit.getKills());
			HTMLFunctions.AddCell(str, "" + campunit.getFriendlyKills());
			HTMLFunctions.AddCell(str, "" + campunit.getRankValue());
			HTMLFunctions.EndRow(str);
			i++;
		}
		HTMLFunctions.EndTable(str);
	}
	

	public void appendTopPlayers(StringBuffer str) throws SQLException {
		ResultSet rs = dbs.getResultSet("SELECT * FROM Logins");// AND LoginID <> " + AppletMain.ADMIN_LOGIN_ID);
		ArrayList<Entry> lines = new ArrayList<Entry>();
		while (rs.next()) {
			Entry e = new Entry();
			e.name = LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginID"), true);
			e.gross_points = dbs.getScalarAsInt("SELECT SUM(RankValue) FROM CampUnits WHERE OwnerID = " + rs.getInt("LoginID"));
			e.killed_own = dbs.getScalarAsInt("SELECT KilledOwnUnitsCount FROM Logins WHERE LoginID = " + rs.getInt("LoginID"));
			e.inact_points = dbs.getScalarAsInt("SELECT CampInactivityPoints FROM Logins WHERE LoginID = " + rs.getInt("LoginID"));
			
			e.net_points = e.gross_points;
			e.net_points -= (e.killed_own * 3);
			e.net_points -= e.inact_points;
			if (e.net_points > 0) {
				lines.add(e);
			}
		}

		if (lines.size() > 0) {
			Collections.sort(lines, this);
			this.showTable(str, lines);
		} else {
			HTMLFunctions.Para(str, "No players have started the campaign mode yet.");
		}
	}


	private void showTable(StringBuffer str, ArrayList<Entry> lines) {
		HTMLFunctions.StartTable(str_players, "stats", "", 1, "", 5);
		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCellHeading(str, "Pos");
		HTMLFunctions.AddCellHeading(str, "Name");
		HTMLFunctions.AddCellHeading(str, "Collateral Dam.");
		HTMLFunctions.AddCellHeading(str, "Inactivity" + helpwindow.GetLink(helpwindow.HC_CAMP_INACTIVITY));
		HTMLFunctions.AddCellHeading(str, "Gross Pts" + helpwindow.GetLink(helpwindow.HC_CAMP_GROSS_POINTS));
		HTMLFunctions.AddCellHeading(str, "Net Pts" + helpwindow.GetLink(helpwindow.HC_CAMP_NET_POINTS));
		HTMLFunctions.EndRow(str);

		for (int i=0 ; i<lines.size() ; i++) {
			Entry e = (Entry)lines.get(i);
			String name = e.name;
			if (i == 0) {
				name = "<b>" + name + "</b>";
			}
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "" + (i+1));
			HTMLFunctions.AddCell(str, name);
			HTMLFunctions.AddCell(str, "" + e.killed_own);
			HTMLFunctions.AddCell(str, "" + e.inact_points);
			HTMLFunctions.AddCell(str, "" + e.gross_points);
			HTMLFunctions.AddCell(str, "" + e.net_points);
			HTMLFunctions.EndRow(str);
		}
		HTMLFunctions.EndTable(str);


	}


	public class Entry {

		public String name;
		public int gross_points, net_points, killed_own, inact_points;

	}


	//@Override
	public int compare(campaignleague.Entry arg0, campaignleague.Entry arg1) {
		Entry e1 = (Entry)arg0;
		Entry e2 = (Entry)arg1;
		
		if (e1.net_points > e2.net_points) {
			return -1;
		} else if (e1.net_points < e2.net_points) {
			return 1;
		} else {
			return 0;
		}
	}

*/
//}

