package dsrwebserver.pages.dsr;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import ssmith.util.Interval;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.AbstractTable;
import dsrwebserver.tables.CampUnitsTable;
import dsrwebserver.tables.FactionsTable;
import dsrwebserver.tables.GameRequestsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.StarmapLogTable;
import dsrwebserver.tables.StarmapSquaresTable;

public class starmap extends AbstractHTMLPage {

	private static final int DAYS = 2;

	private Interval check_undefended_int = new Interval(Dates.HOUR, true);

	public starmap() {
		super();

		this.addHTMLToHead("<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>");
		this.addHTMLToHead("<script>");
		this.addHTMLToHead("$(document).ready(function(){");
		this.addHTMLToHead("	$(\"#div2\").fadeIn(\"slow\");");
		this.addHTMLToHead("});");
		this.addHTMLToHead("</script>");

	}


	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();
		if (this.session.isLoggedIn()) {
			try {
				CampUnitsTable.EnsureEnoughCampUnits(dbs, this.current_login);
				if (check_undefended_int.hitInterval()) {
					// Check for unaccepted starmap game requests
					ResultSet rs = dbs.getResultSet("SELECT GameRequestID, DATEDIFF(CurDate(), DateCreated) AS Diff FROM GameRequests WHERE CampGame = 1 AND COALESCE(Accepted, 0) = 0 AND AttackingFactionID > 0");
					GameRequestsTable reqs = new GameRequestsTable(dbs);
					while (rs.next()) {
						reqs.selectRow(rs.getInt("GameRequestID"));
						if (rs.getInt("Diff") > DSRWebServer.STARMAP_DEFEND_DAYS) {
							// Sector lost
							int old_facid = StarmapSquaresTable.GetFactionID(dbs, reqs.getStarmapX(), reqs.getStarmapY());
							int new_facid = reqs.getAttackingFactionID();

							FactionsTable.EmailFactionMembers(dbs, old_facid, "Sector Lost!", "Your sector " + reqs.getStarmapX() + "-" + reqs.getStarmapY() + " has been overtaken by the " + FactionsTable.GetName(dbs, reqs.getAttackingFactionID()) + " as it was undefended.", -1);
							FactionsTable.EmailFactionMembers(dbs, new_facid, "Sector Annexed!", "Your faction has taken over Sector " + reqs.getStarmapX() + "-" + reqs.getStarmapY() + " as it was undefended.", -1);

							reqs.delete(rs.getInt("GameRequestID"));

							StarmapSquaresTable.SetFactionID(dbs, reqs.getStarmapX(), reqs.getStarmapY(), new_facid);
							int winning_player_id = reqs.getPlayerIDByNum(1);
							StarmapLogTable.AddRec(dbs, LoginsTable.GetDisplayName(dbs, winning_player_id) + " (" + FactionsTable.GetName(dbs, LoginsTable.GetFactionID(dbs, winning_player_id)) + ") has overtaken sector " + reqs.getStarmapX() + "-" + reqs.getStarmapY() + " as it was undefended.", false);
						}
					}
					reqs.close();
					rs.close();
				}
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}


			int num_spare_units = CampUnitsTable.GetNumCampaignUnits(dbs, this.current_login.getID(), true);

			String next = this.headers.getGetValueAsString("next");
			int map_x = this.headers.getGetValueAsInt("x");
			int map_y = this.headers.getGetValueAsInt("y");
			int reqid = this.headers.getGetValueAsInt("reqid");
			long time = this.headers.getGetValueAsLong("t");
			boolean can_player_attack = canPlayerAttack(dbs, this.current_login);
			if (time + Dates.MINUTE > System.currentTimeMillis()) { // Avoid multiple requests
				if (next.equalsIgnoreCase("attack")) {
					if (num_spare_units > 0) {
						// Check it is adjacent
						boolean adjacent = false;
						int our_side = this.current_login.getFactionID();
						for (int y=map_y-1 ; y<=map_y+1 ; y++) {
							for (int x=map_x-1 ; x<=map_x+1 ; x++) {
								if (x == map_x || y == map_y) {
									int sq_side = StarmapSquaresTable.GetFactionID(dbs, x, y);
									if (our_side == sq_side) {
										adjacent = true;
										break;
									}
								}
							}
						}
						if (adjacent) {
							if (reqid <= 0) { // Is there already a game request?
								if (can_player_attack) { // Can't attack if already playing
									// Is there already a request?
									if (GameRequestsTable.IsUnderAttack(dbs, map_x, map_y) <= 0) { 
										// Create game request
										GameRequestsTable.CreateRequest(dbs, 2, this.current_login.getID(), StarmapSquaresTable.GetMissionID(dbs, map_x, map_y), 1, GameRequestsTable.GT_NORMAL, 0, true, true, 0, "", 0, this.current_login.getFactionID(), map_x, map_y, -1);
										// Send email to faction owners.
										int facid = StarmapSquaresTable.GetFactionID(dbs, map_x, map_y);
										FactionsTable.EmailFactionMembers(dbs, facid, "Sector Under Attack", "One of the sectors controlled by your faction is under attack.  If it is not defended within " + DSRWebServer.STARMAP_DEFEND_DAYS + " days it will be lost!\n\nIf you no longer wish to receive these emails and would like to be removed from your faction, just reply to this email saying so.", -1);
										//HTMLFunctions.Heading(str, 2, "You have chosen to attack sector " + map_x + " / " + map_y + "!");
										StarmapLogTable.AddRec(dbs, this.current_login.getDisplayName() + " (" + FactionsTable.GetName(dbs, this.current_login.getFactionID()) + ") has chosen to attack Sector " + map_x + "-" + map_y + " (" + FactionsTable.GetName(dbs, facid) + ").", false);
										can_player_attack = false;
									} else {
										// There's already an open request for that sector - do nothing.
									}
								}
							} else { // Accept request
								GameRequestsTable req = new GameRequestsTable(dbs);
								try {
									if (req.doesRowExist(reqid)) {
										req.selectRow(reqid);
										AbstractMission mission = AbstractMission.Factory(req.getMission());
										// Does this player have enough units?
										if (num_spare_units >= mission.getMinUnitsForSide(2)) {
											// Does the defender have enough units?
											if (CampUnitsTable.GetNumCampaignUnits(dbs, req.getPlayerIDByNum(1), true) >= mission.getMinUnitsForSide(1)) {
												StarmapLogTable.AddRec(dbs, this.current_login.getDisplayName() + " (" + FactionsTable.GetName(dbs, this.current_login.getFactionID()) + ") has chosen to defend Sector " + map_x + "-" + map_y + ".", false);
												this.redirectTo_Using303("/dsr/JoinGame.cls?reqid=" + reqid + "&player2side=2");
												return;
											} else {
												StarmapLogTable.AddRec(dbs, "The attack on sector " + map_x + "-" + map_y + " has been cancelled as " + LoginsTable.GetDisplayName(dbs, req.getPlayerIDByNum(1)) + " did not have enough spare units.", false);
												req.delete(reqid);
												HTMLFunctions.Heading(str, 2, "<span class=\"warning\">Your opponent does not have enough units.  Their attack has been cancelled!</span>");
											}
										} else {
											HTMLFunctions.Heading(str, 2, "<span class=\"warning\">You do not have enough units!</span>");
										}
									} else {
										HTMLFunctions.Heading(str, 2, "<span class=\"warning\">Sorry, that sector is already under attack</span>");
									}
								} finally {
									req.close();
								}
							}
						} else {
							HTMLFunctions.Heading(str, 2, "<span class=\"warning\">You must own an adjacent square!</span>");
						}
					} else {
						HTMLFunctions.Heading(str, 2, "<span class=\"warning\">You don't have any spare units!</span>");
					}
				} else if (next.equalsIgnoreCase("cancel")) {
					GameRequestsTable.RemoveRequest(dbs, this.current_login.getID(), reqid);
					StarmapLogTable.AddRec(dbs, this.current_login.getDisplayName() + " (" + FactionsTable.GetName(dbs, this.current_login.getFactionID()) + ") has aborted their attack.", false);
					this.redirectTo_Using303("?");
				}
			}

			HTMLFunctions.Para(str, "<div id=\"testx\" style=\"display:none;\">The objective in the campaign is to control all four corners of the galaxy!</div>");

			/*if (HasPlayerLostWithinTime(dbs, this.current_login)) {
				HTMLFunctions.Heading(str, 2, "You have lost your last campaign game so you cannot attack for " + DAYS + " days since your defeat.");
			} else if (this.current_login.doesPlayerHaveTooManyFactionRequestOpen()) {
				HTMLFunctions.Heading(str, 2, "You have chosen to attack another sectors.  You can't attack any more.");
			} else */
			if (current_login.getFactionID() <= 0) {
				HTMLFunctions.Heading(str, 2, "Please join a faction to take part in the campaign!");
			} else {
				HTMLFunctions.Heading(str, 2, "Choose the sector you would like to attack or defend!");
				HTMLFunctions.Para(str, "You must choose somewhere adjacent to a square that you already control.");
			}

			// List sectors under attack
			int c = StarmapSquaresTable.GetNumFactionsUnderAttack(dbs, this.current_login.getFactionID());
			if (c > 0) {
				HTMLFunctions.Heading(str, 3, "You have " + c + " sectors under attack!");
			}
			if (num_spare_units > 0) {
				HTMLFunctions.Heading(str, 3, "You have " + num_spare_units + " units available in your squad.");
			} else {
				HTMLFunctions.Heading(str, 3, "You cannot attack or defend as you don't have any units available in your squad!");
			}

			str.append("Key: ");
			ResultSet rs = dbs.getResultSet("SELECT * FROM Factions");
			while (rs.next()) {
				str.append("<span style=\"background: #" + rs.getString("Colour") + "\"><a href=\"factions.cls?fid=" + rs.getInt("FactionID") + "\">" + rs.getString("Name") + "</a>");
				if (rs.getInt("FactionID") == this.current_login.getFactionID()) {
					str.append(" (yours)");
				}
				str.append("</span> - ");
			}
			str.delete(str.length()-2, str.length());

			HTMLFunctions.StartTable(str);

			/*ResultSet rs_mapsquares = dbs.getResultSet("SELECT * FROM StarmapSquares ORDER BY MapY, MapX");
			int last_y = -1;
			while (rs_mapsquares.next()) {
				if (last_y == -1) {
					last_y = rs_mapsquares.getInt("MapY");
					str.append("<tr>");
				} else if (last_y != rs_mapsquares.getInt("MapY")) {
					last_y = rs_mapsquares.getInt("MapY");
					str.append("</tr><tr>");
				}*/
			int size = StarmapSquaresTable.SIZE;
			for (int y=0 ; y<size ; y++) {
				str.append("<tr>");
				for (int x=0 ; x<size ; x++) {
					//str.append("<td>");
					ResultSet rs_mapsquares = dbs.getResultSet("SELECT * FROM StarmapSquares WHERE MapX = " + x + " AND MapY = " + y);
					if (rs_mapsquares.next()) {
						int attack_faction_id = 0;
						int attack_player_id = 0;
						String attack_string = "";
						int gamereq = GameRequestsTable.IsUnderAttack(dbs, rs_mapsquares.getInt("MapX"), rs_mapsquares.getInt("MapY"));
						if (gamereq > 0) { // Open game
							attack_player_id = GameRequestsTable.GetAttackingPlayerID(dbs, rs_mapsquares.getInt("MapX"), rs_mapsquares.getInt("MapY"));
							attack_faction_id = LoginsTable.GetFactionID(dbs, attack_player_id);
							attack_string = LoginsTable.GetDisplayName_Enc(dbs, attack_player_id, false) + " (" + FactionsTable.GetName(dbs, attack_faction_id) + ")";
						}

						str.append("\n<td width=\"130px\" id=\"ssquare\" style=\"background-color: #" + FactionsTable.GetColour(dbs, rs_mapsquares.getInt("FactionID")) + ";\" >");
						//str.append("<img src=\"" + DSRWebServer.IMAGES_HOST + "/images/saturn.png\" align=\"right\" width=\"50\" />");
						this.showFactionLogo(str, rs_mapsquares.getInt("FactionID"));
						str.append("[" + rs_mapsquares.getInt("MapX") + "-" + rs_mapsquares.getInt("MapY") + "]<br />"); 
						str.append(AbstractMission.GetMissionNameFromType(rs_mapsquares.getInt("MissionID"), true, true) + "<br />");
						str.append("<b>");
						int gid = GamesTable.GetBattleInProgress(dbs, rs_mapsquares.getInt("MapX"), rs_mapsquares.getInt("MapY"));
						if (gamereq > 0) { // Open game
							int days_left = DSRWebServer.STARMAP_DEFEND_DAYS - GameRequestsTable.GetDaysLeft(dbs, rs_mapsquares.getInt("MapX"), rs_mapsquares.getInt("MapY"));
							if (rs_mapsquares.getInt("FactionID") == this.current_login.getFactionID()) { // Is it owned by us?
								str.append("UNDER ATTACK - by " + attack_string + "! (" + days_left + " days left)  Order: <a href=\"?next=attack&x=" + rs_mapsquares.getInt("MapX") + "&y=" + rs_mapsquares.getInt("MapY") + "&reqid=" + gamereq + "&t=" + System.currentTimeMillis() + "\">DEFEND!</a>");
							} else {
								str.append("UNDER ATTACK - by " + attack_string + "! (" + days_left + " days left)");
								// Add option to cancel attack
								if (attack_player_id == this.current_login.getID()) {
									str.append(" [<a href=\"?next=cancel&reqid=" + gamereq + "&t=" + System.currentTimeMillis() + "\"><b>Cancel</b></a>]");
								}
							}
							this.showFactionLogo(str, attack_faction_id);
						} else if (this.current_login.getFactionID() != rs_mapsquares.getInt("FactionID") && gid <= 0) { // Not our sector and no current battle
							// Add option to attack it
							if (can_player_attack) {
								str.append("Order: <a href=\"?next=attack&x=" + rs_mapsquares.getInt("MapX") + "&y=" + rs_mapsquares.getInt("MapY") + "&t=" + System.currentTimeMillis() + "\">ATTACK!</a>");
							} else {
								//str.append("Order: none");
							}
						} else {
							// Show nothing
						}
						if (gid > 0) {
							GamesTable game = new GamesTable(dbs);
							game.selectRow(gid);
							int p1 = game.getLoginIDFromSide(1);
							int p2 = game.getLoginIDFromSide(2);
							str.append("BATTLE IN PROGRESS between " + LoginsTable.GetDisplayName_Enc(dbs, p1, true) + " and " + LoginsTable.GetDisplayName_Enc(dbs, p2, true));
							this.showFactionLogo(str, LoginsTable.GetFactionID(dbs, game.getPlayerID(1)));
							game.close();
						}
						str.append("</b>");
					} else {
						str.append("<td>");

					}
					str.append("</td>"); // </div>
				}
				str.append("</tr>");
			}

			HTMLFunctions.EndTable(str);

			HTMLFunctions.Para(str, "<a href=\"/dsr/factionhelp.cls\">Faction Help</a>");

			// List starmap log
			HTMLFunctions.StartUnorderedList(str);
			rs = dbs.getResultSet("SELECT * FROM StarmapLog ORDER BY DateCreated DESC LIMIT 40");
			while (rs.next()) {
				HTMLFunctions.AddListEntry(str, Dates.FormatDate(rs.getTimestamp("DateCreated"), "dd MMM yyyy") + ": " + rs.getString("Entry"));
			}
			HTMLFunctions.EndUnorderedList(str);
		} else {
			HTMLFunctions.Para(str, "Sorry, you need to be logged in to see the Galaxy Map.");
		}

		this.body_html.append(MainLayout.GetHTML(this, "Galaxy Map", str));		

	}


	public static boolean canPlayerAttack(MySQLConnection dbs, LoginsTable login) throws SQLException {
		/*if (HasPlayerLostWithinTime(dbs, login)) {
			return false;
			/*} else if (login.doesPlayerHaveTooManyFactionRequestOpen()) {
			return false;*/
		//s} else 
		if (login.getFactionID() <= 0) {
			return false;
		}
		return true;
	}


	private static boolean HasPlayerLostWithinTime(MySQLConnection dbs, LoginsTable login) throws SQLException {
		/*String sql = "SELECT Count(*) FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED + " AND CampGame = 1 AND " + AbstractTable.GetLosingPlayerSubQuery(login.getID());
		sql = sql + " AND DateDiff(CurDate(), DateFinished) <= " + DAYS;
		if (dbs.getScalarAsInt(sql) > 0) {
			return true;
		}
		return false;*/
		String sql = "SELECT * FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED + " AND CampGame = 1 AND " + AbstractTable.GetPlayerSubQuery(login.getID());
		sql = sql + " AND WinType <> " + GamesTable.WIN_DRAW;
		sql = sql + " AND DateDiff(CurDate(), DateFinished) <= " + DAYS + " ORDER BY DateFinished DESC LIMIT 1";
		ResultSet rs = dbs.getResultSet(sql);
		if (rs.next()) {
			if (rs.getInt("WinningSide") > 0) {
				if (rs.getInt("Player" + rs.getInt("WinningSide") + "ID") != login.getID()) {
					return true;
				}
			}
		}
		return false;
	}


	private void showFactionLogo(StringBuffer str, int factionid) throws SQLException {
		str.append("<img src=\"" + DSRWebServer.IMAGES_HOST + "/images/factions/" + FactionsTable.GetImageFilename(dbs, factionid) + "\" width=40 align=\"right\" />");

	}

}
