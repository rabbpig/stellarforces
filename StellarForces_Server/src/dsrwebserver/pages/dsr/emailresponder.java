package dsrwebserver.pages.dsr;

import java.util.HashMap;

import ssmith.html.HTMLFunctions;

import dsrwebserver.URLRequest;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.LoginsTable;

/**
 * This is currently unused.
 *
 */
public class emailresponder extends AbstractHTMLPage {

	public emailresponder() {
		super();
	}

	public void process() throws Exception {
		this.setTitle("Email Response");

		
		String code = this.headers.getPostValueAsString("c"); // Base 64 encoded
		HashMap<String, StringBuffer> hash = URLRequest.DecodeParams(code);
		String cmd = hash.get("cmd").toString();
		int id = Integer.parseInt(hash.get("id").toString());

		StringBuffer str = new StringBuffer();
		
		if (cmd.equalsIgnoreCase("unsub")) {
			LoginsTable login = new LoginsTable(dbs);
			login.selectRow(id);
			login.setOptedOutOfEmails(true);
			login.close();
			
			HTMLFunctions.Heading(str, 2, "Email Unsubscribe");
			HTMLFunctions.Para(str, "You have been succesfully removed from the email list.  Apologies for any inconvenience.");
		} else {
			HTMLFunctions.Para(str, "Unknown command: " + cmd);
		}


		body_html.append(MainLayout.GetHTML(this, "Register Here", str));
	}

}
