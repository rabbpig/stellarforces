package dsrwebserver.pages.dsr;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import ssmith.util.Interval;
import ssmith.util.LeagueTable;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.LoginsTable;

public class otherleagues extends AbstractHTMLPage {

	private static final int BEGINNER_CUTOFF_GAMES = 200;

	private static StringBuffer str_leagues;
	private static Interval update_interval = new Interval(Dates.HOUR*4, false);

	public otherleagues() {
		super();
	}


	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		if (leaguetable.calculating) {
			HTMLFunctions.Para(str, "Please wait, the league is currently being calculated.");
		} else {
			if (str_leagues == null || update_interval.hitInterval()) {
				calcLeagues();
			}
		}

		str.append(str_leagues);

		this.body_html.append(MainLayout.GetHTML(this, "Other Leagues", str));		
	}


	private void calcLeagues() throws SQLException {
		str_leagues = new StringBuffer();

		HTMLFunctions.Heading(str_leagues, 3, "Most Prolific Players Ever");

		LeagueTable<Integer> lt = new LeagueTable<Integer>();

		ResultSet rs = dbs.getResultSet("SELECT LoginID FROM Logins WHERE COALESCE(Admin, 0) = 0");
		LoginsTable login = new LoginsTable(dbs);
		while (rs.next()) {
			DSRWebServer.p("Calc'ing other league for " + rs.getInt("LoginID"));
			login.selectRow(rs.getInt("LoginID"));
			lt.addItem(rs.getInt("LoginID"), login.getTotalGamesFinished());
		}
		login.close();
		rs.close();

		HTMLFunctions.StartTable(str_leagues, "stats", "", 1, "", 5);

		HTMLFunctions.StartRow(str_leagues);
		HTMLFunctions.AddCellHeading(str_leagues, "Pos");
		HTMLFunctions.AddCellHeading(str_leagues, "Name");
		HTMLFunctions.AddCellHeading(str_leagues, "Total Games");
		HTMLFunctions.EndRow(str_leagues);

		for (int i=0 ; i<5 ; i++) {
			int loginid = lt.getObjAtPos(i);
			login.selectRow(loginid);

			HTMLFunctions.StartRow(str_leagues);
			HTMLFunctions.AddCell(str_leagues, ""+(i+1));
			HTMLFunctions.AddCell(str_leagues, login.getDisplayName_Enc(true));
			HTMLFunctions.AddCell(str_leagues, ""+lt.getPointsAtPos(i));
			HTMLFunctions.EndRow(str_leagues);

		}

		HTMLFunctions.EndTable(str_leagues);

		//------------------------------------------------------------------

		HTMLFunctions.Heading(str_leagues, 3, "Highest Ever League Points");

		HTMLFunctions.StartTable(str_leagues, "stats", "", 1, "", 5);

		HTMLFunctions.StartRow(str_leagues);
		HTMLFunctions.AddCellHeading(str_leagues, "Pos");
		HTMLFunctions.AddCellHeading(str_leagues, "Name");
		HTMLFunctions.AddCellHeading(str_leagues, "Highest Points");
		HTMLFunctions.EndRow(str_leagues);

		rs = dbs.getResultSet("SELECT LoginID, HighestELOPoints FROM Logins WHERE COALESCE(Admin, 0) = 0 ORDER BY HighestELOPoints DESC LIMIT 5");
		int i=1;
		while (rs.next()) {
			login.selectRow(rs.getInt("LoginID"));

			HTMLFunctions.StartRow(str_leagues);
			HTMLFunctions.AddCell(str_leagues, ""+i);
			HTMLFunctions.AddCell(str_leagues, login.getDisplayName_Enc(true));
			HTMLFunctions.AddCell(str_leagues, ""+rs.getInt("HighestELOPoints"));
			HTMLFunctions.EndRow(str_leagues);

			i++;
		}
		rs.close();

		HTMLFunctions.EndTable(str_leagues);

		//------------------------------------------------------------------

		HTMLFunctions.Heading(str_leagues, 3, "Beginners League (less than " + BEGINNER_CUTOFF_GAMES + " games)");

		HTMLFunctions.StartTable(str_leagues, "stats", "", 1, "", 5);

		HTMLFunctions.StartRow(str_leagues);
		HTMLFunctions.AddCellHeading(str_leagues, "Pos");
		HTMLFunctions.AddCellHeading(str_leagues, "Name");
		HTMLFunctions.AddCellHeading(str_leagues, "Points");
		HTMLFunctions.EndRow(str_leagues);

		rs = dbs.getResultSet("SELECT LoginID, ELOPoints FROM Logins WHERE COALESCE(Admin, 0) = 0 ORDER BY ELOPoints DESC");
		i=1;
		while (rs.next()) {
			login.selectRow(rs.getInt("LoginID"));

			int games = login.getTotalGamesFinished();
			if (games < BEGINNER_CUTOFF_GAMES) {

				HTMLFunctions.StartRow(str_leagues);
				HTMLFunctions.AddCell(str_leagues, ""+i);
				HTMLFunctions.AddCell(str_leagues, login.getDisplayName_Enc(true));
				HTMLFunctions.AddCell(str_leagues, ""+rs.getInt("ELOPoints"));
				HTMLFunctions.EndRow(str_leagues);

				i++;
				if (i > 10) {
					break;
				}
			}
		}
		rs.close();

		HTMLFunctions.EndTable(str_leagues);

		//------------------------------------------------------------------

		HTMLFunctions.Heading(str_leagues, 3, "Premier League (more than " + BEGINNER_CUTOFF_GAMES + " games)");

		HTMLFunctions.StartTable(str_leagues, "stats", "", 1, "", 5);

		HTMLFunctions.StartRow(str_leagues);
		HTMLFunctions.AddCellHeading(str_leagues, "Pos");
		HTMLFunctions.AddCellHeading(str_leagues, "Name");
		HTMLFunctions.AddCellHeading(str_leagues, "Points");
		HTMLFunctions.EndRow(str_leagues);

		rs = dbs.getResultSet("SELECT LoginID, ELOPoints FROM Logins WHERE COALESCE(Admin, 0) = 0 ORDER BY ELOPoints DESC");
		i = 1;
		while (rs.next()) {
			login.selectRow(rs.getInt("LoginID"));

			int games = login.getTotalGamesFinished();
			if (games >= BEGINNER_CUTOFF_GAMES) {

				HTMLFunctions.StartRow(str_leagues);
				HTMLFunctions.AddCell(str_leagues, ""+i);
				HTMLFunctions.AddCell(str_leagues, login.getDisplayName_Enc(true));
				HTMLFunctions.AddCell(str_leagues, ""+rs.getInt("ELOPoints"));
				HTMLFunctions.EndRow(str_leagues);

				i++;
				if (i > 10) {
					break;
				}
			}
		}
		rs.close();

		HTMLFunctions.EndTable(str_leagues);


		//------------------------------------------------------------------

		int TURN_THRESH = 200;
		
		HTMLFunctions.Heading(str_leagues, 3, "Fastest Players (Average days to take a turn, only players with at least " + TURN_THRESH + " turns taken)");

		HTMLFunctions.StartTable(str_leagues, "stats", "", 1, "", 5);

		HTMLFunctions.StartRow(str_leagues);
		HTMLFunctions.AddCellHeading(str_leagues, "Pos");
		HTMLFunctions.AddCellHeading(str_leagues, "Name");
		HTMLFunctions.AddCellHeading(str_leagues, "Avg. Days Per Turn");
		HTMLFunctions.EndRow(str_leagues);

		rs = dbs.getResultSet("SELECT LoginID, TotalDaysTakingTurns/TotalTurns AS Tot FROM Logins WHERE COALESCE(Admin, 0) = 0 AND TotalTurns > " + TURN_THRESH + " ORDER BY Tot");
		i = 1;
		while (rs.next()) {
			login.selectRow(rs.getInt("LoginID"));

				HTMLFunctions.StartRow(str_leagues);
				HTMLFunctions.AddCell(str_leagues, ""+i);
				HTMLFunctions.AddCell(str_leagues, login.getDisplayName_Enc(true));
				HTMLFunctions.AddCell(str_leagues, ""+rs.getFloat("Tot"));
				HTMLFunctions.EndRow(str_leagues);

				i++;
				if (i > 10) {
					break;
				}
		}
		rs.close();
		HTMLFunctions.EndTable(str_leagues);

		//------------------------------------------------------------------


	}


}
