package dsrwebserver.pages.dsr;

import java.io.IOException;

import ssmith.html.HTMLFunctions;
import ssmith.image.ImageFunctions;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;

/**
 * This is for getting a map of the mission, NOT for any particular game.
 *
 */
public class viewmissionmap extends AbstractHTMLPage {

	public viewmissionmap() {
		super();
	}


	@Override
	public void process() throws Exception {
		String full_req = super.disk_cache.convertPath(super.conn.headers.request.getFullURL());
		StringBuffer str = new StringBuffer();
		if (super.disk_cache.contains(full_req) == false) {
			int missionid = this.headers.getGetOrPostValueAsInt("missionid");
			if (AbstractMission.IsValidMission(missionid)) {
				AbstractMission mission = AbstractMission.Factory(missionid);

				String title = "Map for mission '" + mission.mission_name + "'";

				// Draw map image
				str.append("<img align=left hspace=10 src=\"MapImageForMission.cls?missionid=" + missionid + "\" alt=\"Strategic scanner for current game\" ><br />");

				// Draw players table at top
				HTMLFunctions.StartTable(str, "stats", "", 0, "", 5);
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCellHeading(str, "Side");
				HTMLFunctions.AddCellHeading(str, "Name");
				HTMLFunctions.AddCellHeading(str, "Colour");
				HTMLFunctions.AddCellHeading(str, "Max Units in Campaign");
				HTMLFunctions.EndRow(str);

				for (int s=1 ; s<=mission.getNumOfSides() ; s++){
					HTMLFunctions.StartRow(str);
					HTMLFunctions.AddCell(str, "" + s);
					HTMLFunctions.AddCell(str, mission.getSideDescription(s));
					HTMLFunctions.AddCell(str, "<font color=#" + ImageFunctions.GetRGB(MapImageAbstract.GetColourForSide(s)) + ">This Colour</font>");
					try {
						HTMLFunctions.AddCell(str, ""+mission.getNumDeploySquaresForSide(dbs, s));
					} catch (IOException ex) {
						ex.printStackTrace();
						HTMLFunctions.AddCell(str, "enough");
					}
					HTMLFunctions.EndRow(str);
				}
				HTMLFunctions.EndTable(str);

				super.disk_cache.put(full_req, str.toString());
			} else {
				str.append("That is not a valid mission.");
			}
		} else {
			str.append(super.disk_cache.get(full_req));
		}
		this.body_html.append(MainLayout.GetHTML(this, "Mission Map", str));
	}

}
