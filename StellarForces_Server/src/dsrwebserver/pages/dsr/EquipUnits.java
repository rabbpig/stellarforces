package dsrwebserver.pages.dsr;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.html.HTMLFunctions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.FactionsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public final class EquipUnits extends AbstractHTMLPage {

	public EquipUnits() {
		super();
	}


	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();
		if (this.session.isLoggedIn()) {
			int gameid = this.headers.getGetOrPostValueAsInt("gid");
			GamesTable game = new GamesTable(dbs);
			try {
			game.selectRow(gameid);
			// Check we're a player in this game
			if (game.isPlayerInGame(this.current_login.getLoginID())) {
				if (game.getGameStatus() == GamesTable.GS_CREATED_DEPLOYMENT) {
					int our_side = GamesTable.GetSideFromPlayerID(dbs, gameid, this.current_login.getLoginID());
					if (game.hasSideEquipped(our_side) == false) {
						String next = this.headers.getGetOrPostValueAsString("next");

						AbstractMission mission = AbstractMission.Factory(game.getMissionID());
						int tot_creds = mission.getCreditsForSide(our_side);
						tot_creds = (tot_creds * game.getPcentCredit()) / 100;
						int camp_creds = 0;
						if (game.isCampGame()) {
							//camp_creds = Math.min(this.current_login.getCampCredits(), tot_creds); // Only allow 200% credits
							camp_creds = this.current_login.getCampCredits();
							tot_creds = tot_creds + camp_creds;
						}
						int creds_remaining = tot_creds - EquipmentTable.GetValueOfArmourAndEquipment(dbs, gameid, our_side, true);

						// Check we are able to buy anything!
						if (next.equalsIgnoreCase("finish") || dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side) == 0) {
							game.setSideHasEquipped(our_side);
							if (game.isCampGame()) {
								this.current_login.setCampCredits(creds_remaining);
							}
							UnitsTable.UseFirstItem(dbs, game.getID(), our_side);
							this.redirectTo_Using303("MyGames.cls");
							return;
						}


						if (next.equalsIgnoreCase("buy")) {
							EquipmentTypesTable equip_types = new EquipmentTypesTable(dbs);
							ResultSet rs_units = dbs.getResultSet("SELECT * FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side + " ORDER BY OrderBy");
							// Loop through units
							while (rs_units.next()) {
								int eid = this.headers.getGetOrPostValueAsInt("eid_" + rs_units.getInt("UnitID"));
								if (eid > 0) {
									equip_types.selectRow(eid);
									if (mission.isItemTypeAllowed(equip_types.getMajorType(), our_side)) {
										if (mission.isItemAllowed(equip_types.getCode(), our_side)) {
											int cost = equip_types.getCost();
											// Check they can afford it
											if (cost <= creds_remaining) {
												buyForUnit(dbs, gameid, rs_units.getInt("UnitID"), equip_types);
												// Update creds remaining
												creds_remaining = tot_creds - EquipmentTable.GetValueOfArmourAndEquipment(dbs, gameid, our_side, true);
											}
										}
									}
								}
							}
						} else if (next.equalsIgnoreCase("remove")) {
							// Check we own the unit of this item!
							int eid = this.headers.getGetOrPostValueAsInt("eid");
							//int unitid = this.headers.getPostValueAsInt("unitid");
							EquipmentTable eq = new EquipmentTable(dbs);
							if (eq.selectRow(eid, false)) {
								int owner_unitid = eq.getUnitID();
								UnitsTable unit = new UnitsTable(dbs);
								unit.selectRow(owner_unitid);
								if (unit.getGameID() == game.getID() && unit.getSide() == our_side) {
									eq.delete();
								}
								unit.close();
							}
							eq.close();
						}
						// Recalc creds in case we bought or sold something
						creds_remaining = tot_creds - EquipmentTable.GetValueOfArmourAndEquipment(dbs, gameid, our_side, true);

						//HTMLFunctions.Heading(str, 3, "<b>Mission:</b> " + mission.mission_name + "  / <b>Your Side:</b> " + mission.getSideDescription(our_side));
						HTMLFunctions.Heading(str, 3, "<b>Mission:</b> <a href=\"/dsr/missiondescriptions.cls?type=" + game.getMissionID() + "&camp=" + (game.isCampGame() ? "1" : "0") + "\">" + mission.mission_name + "</a>");
						HTMLFunctions.Heading(str, 3, "<b>Your Side:</b> " + mission.getSideDescription(our_side) + " (<a href=\"/dsr/viewmap.cls?gid=" + game.getID() + "\">View Units and Map</a>)");

						HTMLFunctions.Para(str, "Here you can buy equipment for your units.  To do this, select an item for each unit from the dropdown boxes, and then click on 'Buy'.  The cost of each item is shown in brackets.");
						HTMLFunctions.Para(str, "Once you have finished buying equipment for your units, click on 'Finished Buying Equipment' at the very bottom of the page.");

						if (game.isCampGame()) {
							HTMLFunctions.Para(str, "As this is a campaign game, any creds you don't spend will be carried forward to the next game.");
						}

						if (game.isCampGame()) {
							if (FactionsTable.IsBestFaction(dbs, this.current_login.getFactionID())) {
								HTMLFunctions.Para(str, "You are in the top faction and have access to better equipment!");
							}
						}

						int num_units = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side);

						//HTMLFunctions.Heading(str, 2, "Creds remaining: " + creds_remaining + " (approx. " + (creds_remaining/num_units) + " per unit, including " + this.current_login.getCampCredits() + " campaign credits)");
						str.append("<a name=\"main\"></a>");
						String cred_text = "Creds remaining: " + creds_remaining + " (approx. " + (creds_remaining/num_units) + " per unit";
						if (game.isCampGame()) {
							cred_text = cred_text + ", including " + camp_creds + " campaign credits";
						}
						cred_text = cred_text + ")";
						HTMLFunctions.Heading(str, 2, cred_text);

						// Form to add item
						// Check there is anything to buy
						if (dbs.getScalarAsInt("SELECT Count(*) FROM EquipmentTypes WHERE CanBuy = 1 AND Cost <= " + creds_remaining) > 0) {
							HTMLFunctions.StartForm(str, "Form1", "#main", "POST");
						} else {
							HTMLFunctions.Para(str, "You do not have enough creds to buy anything with.");
						}

						HTMLFunctions.StartTable(str, "stats", "", 1, "", 0);
						HTMLFunctions.StartRow(str);
						HTMLFunctions.AddCellHeading(str, "Unit");
						HTMLFunctions.AddCellHeading(str, "Equipment");
						HTMLFunctions.AddCellHeading(str, "Total<br />Weight");
						HTMLFunctions.AddCellHeading(str, "APs");
						HTMLFunctions.AddCellHeading(str, "Health");
						HTMLFunctions.AddCellHeading(str, "Shot Skill");
						HTMLFunctions.AddCellHeading(str, "CC Skill");
						HTMLFunctions.AddCellHeading(str, "Strength");
						if (game.isAdvancedMode()) {
							HTMLFunctions.AddCellHeading(str, "Energy");
							HTMLFunctions.AddCellHeading(str, "Morale");
						}
						HTMLFunctions.AddCellHeading(str, "Buy");
						HTMLFunctions.EndRow(str);

						int max_aps = dbs.getScalarAsInt("SELECT Max(MaxAPs)FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side);
						int max_health = dbs.getScalarAsInt("SELECT Max(MaxHealth)FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side);
						int max_shotskill = dbs.getScalarAsInt("SELECT Max(ShotSkill)FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side);
						int max_ccskill = dbs.getScalarAsInt("SELECT Max(CombatSkill)FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side);
						int max_strength = dbs.getScalarAsInt("SELECT Max(Strength)FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side);

						EquipmentTable equipment = new EquipmentTable(dbs);

						ResultSet rs_units = dbs.getResultSet("SELECT * FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side + " ORDER BY OrderBy");
						while (rs_units.next()) {
							HTMLFunctions.StartRow(str);
							HTMLFunctions.AddCell(str, rs_units.getString("Name"));

							ResultSet rs_equip = dbs.getResultSet("SELECT EquipmentID FROM Equipment WHERE UnitID = " + rs_units.getInt("UnitID") + " ORDER BY DateCreated");
							str.append("<td>");
							boolean any = false;
							while (rs_equip.next()) {
								try {
									if (equipment.doesRowExist(rs_equip.getInt("EquipmentID"))) { // In case it's been deleted since we read the rec
										if (equipment.selectRow(rs_equip.getInt("EquipmentID"), true)) {
											any = true;
											str.append("<a href=\"equipmentdetails.cls?eid=" + equipment.getEquipmentTypeID() + "\">" + equipment.getName(false) + "</a> <span class=\"little\">(" + equipment.getCost() + "cr");
											if (equipment.getName(false).equalsIgnoreCase("stardrive") == false) {
												str.append(" <a href=\"?next=remove&gid=" + gameid + "&eid=" + equipment.getID() + "\">remove</a>");
											}
											str.append(")</span>, ");
										}
									}
								} catch (Exception ex) {
									DSRWebServer.HandleError(ex);
								}
							}
							if (!any) {
								str.append("None");
							} else {
								str.delete(str.length()-2, str.length());
							}
							str.append("</td>");

							HTMLFunctions.AddCell(str, EquipmentTable.GetWeightOfArmourAndEquipment(dbs, rs_units.getInt("UnitID")) + "");
							UnitsTable.AddStatCell(str, max_aps, rs_units.getInt("MaxAPs"));
							UnitsTable.AddStatCell(str, max_health, rs_units.getInt("MaxHealth"));
							UnitsTable.AddStatCell(str, max_shotskill, rs_units.getInt("ShotSkill"));
							UnitsTable.AddStatCell(str, max_ccskill, rs_units.getInt("CombatSkill"));
							UnitsTable.AddStatCell(str, max_strength, rs_units.getInt("Strength"));
							if (game.isAdvancedMode()) {
								HTMLFunctions.AddCell(str, rs_units.getInt("MaxEnergy") + "");
								HTMLFunctions.AddCell(str, rs_units.getInt("MaxMorale") + "");
							}
							HTMLFunctions.StartCell(str);
							appendEquipmentCombo(str, "eid_" + rs_units.getInt("UnitID"), game, mission, our_side, creds_remaining);
							HTMLFunctions.EndCell(str);

							HTMLFunctions.EndRow(str);
						}
						equipment.close();
						HTMLFunctions.EndTable(str);
						str.append("<br /><br />");
						HTMLFunctions.HiddenValue(str, "next", "buy");
						HTMLFunctions.SubmitButton(str, "Buy");
						HTMLFunctions.EndForm(str);

						HTMLFunctions.Para(str, "<a href=\"BuyArmour.cls?gid=" + game.getID() + "\">Return to Buy Armour Page</a> | <a href=\"equipmentdetails.cls\">Weapons & Equipment Details</a>");

						// Form to finish
						HTMLFunctions.StartForm(str, "Form2", "", "POST");
						HTMLFunctions.HiddenValue(str, "next", "finish");
						String extra = "";
							if (this.headers.isMobile()) {
								extra = "You should now exit out this browser and\nreturn to the Android app to deploy your units.";
							} else {
								extra = "You should now run the Android app to deploy your units.";
							}
						HTMLFunctions.SubmitButtonWithConf(str, "Form2", "Finished Buying Equipment", "Have you definitely finished equipping your units?  You can\\'t go back afterwards!", extra);
						HTMLFunctions.EndForm(str);

					} else {
						str.append("You have already equipped.");
					}
				} else {
					str.append("The game is not at the Equip Units stage.");
				}
			} else {
				str.append("That is not your game.");
			}
			} finally {
				game.close();
			}
		}
		this.body_html.append(MainLayout.GetHTML(this, "Equip Units", str));
	}


	private void appendEquipmentCombo(StringBuffer str, String name, GamesTable game, AbstractMission mission, int our_side, int creds_remaining) throws SQLException {
		str.append("<select class=\"combobox\" name=\"" + name + "\">");
		str.append("<option value=\"\">Nothing</option>");
		int max = dbs.getScalarAsInt("SELECT MAX(MajorTypeID) FROM EquipmentTypes WHERE CanBuy = 1 AND Cost <= " + creds_remaining);
		for (byte i=1 ; i<=max ; i++) {
			if (mission.isItemTypeAllowed(i, our_side)) {
				String sql = "SELECT Count(*) FROM EquipmentTypes WHERE MajorTypeID = " + i + " AND CanBuy = 1 AND Cost <= " + creds_remaining;
				/*if (game.isCampGame()) {
					if (FactionsTable.IsBestFaction(dbs, this.current_login.getFactionID()) == false) {
						sql = sql + " AND COALESCE(RestrictedToWinners, 0) = 0";
					}
				}*/
				if (dbs.getScalarAsInt(sql) > 0) {
					str.append("<optgroup label=\"" + EquipmentTypesTable.GetMajorTypeFromID(i) + "\">");
					String eq_sql = "SELECT EquipmentTypeID, CONCAT(Name, ' (', Cost, ')'), Code FROM EquipmentTypes WHERE MajorTypeID = " + i + " AND CanBuy = 1 AND Cost <= " + creds_remaining;
					/*if (game.isCampGame()) {
						if (FactionsTable.IsBestFaction(dbs, this.current_login.getFactionID()) == false) {
							eq_sql = eq_sql + " AND COALESCE(RestrictedToWinners, 0) = 0";
						}
					}*/
					eq_sql = eq_sql + " ORDER BY Cost";
					ResultSet rs = dbs.getResultSet(eq_sql);
					while (rs.next()) {
						if (mission.isItemAllowed(rs.getString("Code"), our_side)) {
							str.append("<option value=\"" + rs.getInt(1) + "\"");
							str.append(">" + HTMLFunctions.s2HTML((String)rs.getObject(2)) + "</option>");
						}
					}
					str.append("</optgroup>");
				}
			}
		}
		str.append("</select>");

	}


	public static void buyForUnit(MySQLConnection dbs, int gameid, int unitid, EquipmentTypesTable equip_types) throws SQLException {
		/*int ammo_cap = 0;
		if (ammo != null) {
			ammo_cap = ammo.getAmmoCapacity();
		}*/
		EquipmentTable.CreateEquipment(dbs, gameid, unitid, equip_types.getID(), equip_types.getAmmoCapacity());
		// Buy ammo?
		/*if (ammo != null) {
			EquipmentTable.CreateEquipment(dbs, gameid, unitid, ammo.getID(), ammo.getAmmoCapacity());
		}*/
	}
}
