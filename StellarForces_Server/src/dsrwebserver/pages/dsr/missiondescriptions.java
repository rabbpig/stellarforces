package dsrwebserver.pages.dsr;

import java.sql.ResultSet;

import ssmith.html.HTMLFunctions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MissionStatsTable;

public final class missiondescriptions extends AbstractHTMLPage {

	public missiondescriptions() {
		super();
	}

	@Override
	public void process() throws Exception {
		String full_req = super.disk_cache.convertPath(super.conn.headers.request.getFullURL());
		StringBuffer str = new StringBuffer();
		try {
			if (super.disk_cache.contains(full_req) == false) {
				int type = this.headers.getGetOrPostValueAsInt("type");
				int camp = this.headers.getGetOrPostValueAsInt("camp");


				if (type <= 0) {
					HTMLFunctions.Para(str, "This is an overview of the available missions.");
				}

				str.append("<p>");

				if (type > 0) {
					AbstractMission mission = AbstractMission.Factory(type);
					HTMLFunctions.Heading(str, 1, mission.getMissionName(false, camp == 1));
					/*String camp = AbstractMission.GetCampaignName(mission.getCampaign());
				if (camp.length() > 0) {
					HTMLFunctions.Heading(str, 2, "Part of " + camp);
				}*/
					if (mission.getMapFilename().length() > 0) {
						HTMLFunctions.Para(str, "<a href=\"viewmissionmap.cls?missionid=" + mission.getMissionID() + "\">View Map</a>");
					}
					str.append(mission.getMissionOverview(camp == 1));
					HTMLFunctions.Heading(str, 3, "Settings:");
					HTMLFunctions.StartUnorderedList(str);
					/*switch (mission.canBePlayedOnAndroid()) {
					case AbstractMission.NOT_ON_ANDROID:
						HTMLFunctions.AddListEntry(str, "Cannot be played on Android" + helpwindow.GetLink(helpwindow.HC_ANDROID));
						break;
					case AbstractMission.ONLY_ONE_CLIENT:
						HTMLFunctions.AddListEntry(str, "Can be played on either Android" + helpwindow.GetLink(helpwindow.HC_ANDROID) + " or PC, but only one throughout the whole game.");
						break;
					case AbstractMission.ANY_CLIENT:
						HTMLFunctions.AddListEntry(str, "The Android" + helpwindow.GetLink(helpwindow.HC_ANDROID) + " and PC clients can be used interchangably in the same game.");
						break;
					default:
						throw new RuntimeException("Unknown type: " + mission.canBePlayedOnAndroid());
					}*/
					HTMLFunctions.AddListEntry(str, "Games Required to Play" + helpwindow.GetLink(helpwindow.HC_GAMES_REQ) + ": " + mission.getGamesReqToPlay());
					HTMLFunctions.AddListEntry(str, "Number of Players: " + mission.getNumOfSides());
					for (byte i=1 ; i<=mission.getNumOfSides() ; i++) {
						HTMLFunctions.AddListEntry(str, "Units on side " + i + ": " + mission.getUnitsForSide(i));
					}
					for (byte i=1 ; i<=mission.getNumOfSides() ; i++) {
						HTMLFunctions.AddListEntry(str, "Creds for side " + i + ": " + mission.getCreditsForSide(i));
					}
					HTMLFunctions.AddListEntry(str, "Max Turns: " + mission.getMaxTurns());
					switch (mission.getWallsDestroyableType()) {
					case AbstractMission.INDESTRUCTABLE_WALLS:
						HTMLFunctions.AddListEntry(str, "Walls are <b>indestructible</b>." + helpwindow.GetLink(helpwindow.HC_WALLS));
						break;
					case AbstractMission.STRONG_WALLS:
						HTMLFunctions.AddListEntry(str, "Walls are <b>strong</b> but destructible." + helpwindow.GetLink(helpwindow.HC_WALLS));
						break;
					case AbstractMission.WEAK_WALLS:
						HTMLFunctions.AddListEntry(str, "Walls are <b>weak</b>." + helpwindow.GetLink(helpwindow.HC_WALLS));
						break;
					default:
						throw new RuntimeException("Unknown wall type: " + mission.getWallsDestroyableType());
					}
					/*if (mission.getMinClientVersion() > SharedStatics.MIN_CLIENT_VERSION) {
						HTMLFunctions.AddListEntry(str, "Minimum version of client required: " + mission.getMinClientVersion());
					}*/

					for (int i=1 ; i<=mission.getNumOfSides() ; i++) {
						if (mission.getVPsPerComputer(i) > 0) {
							HTMLFunctions.AddListEntry(str, "Side " + i + " gets " + mission.getVPsPerComputer(i) + " VPs per computer destroyed.");
						}
					}
					int winner_at_end = mission.getWinnerAtEnd();
					if (winner_at_end > 0) {
						HTMLFunctions.AddListEntry(str, "If the number of turns expires, the " + mission.getSideDescription(winner_at_end) + " (side " + winner_at_end + ") wins.");
					} else if (mission.highestVPsWinAtEnd()) {
						HTMLFunctions.AddListEntry(str, "If the number of turns expires, the side with the most VPs wins.");
					} else {
						HTMLFunctions.AddListEntry(str, "If the number of turns expires, the game is a draw.");
					}
					if (mission.isAlwaysAdvanced()) {
						HTMLFunctions.AddListEntry(str, "Mission is always played in Advanced Mode" + helpwindow.GetLink(helpwindow.HC_ADVANCED_MODE) + ".");
					}
					if (mission.aliensImpregnate()) {
						HTMLFunctions.AddListEntry(str, "Aliens can impregnate" + helpwindow.GetLink(helpwindow.HC_IMPREGNATE) + ".");
					}
					for (int i=1 ; i<=mission.getNumOfSides() ; i++) {
						int uc = mission.getUnderCoveragentsForSide(i);
						if (uc > 0) {
							HTMLFunctions.AddListEntry(str, "Side " + i + " has " + uc + " undercover agents." + helpwindow.GetLink(helpwindow.HC_UNDERCOVER));
						}
					}
					for (int i=1 ; i<=mission.getNumOfSides() ; i++) {
						int uc = mission.doesSideHaveAI(i);
						if (uc > 0) {
							HTMLFunctions.AddListEntry(str, "An AI is available for side " + i);
						}
					}

					// Show the best player at this mission
					int best = MissionStatsTable.GetBestPlayerForMission(dbs, type);
					if (this.session.isLoggedIn() && best == this.current_login.getID()) {
						HTMLFunctions.AddListEntry(str, "Best Player: YOU! (" + MissionStatsTable.GetTotalVictoriesForPlayerAndMission(dbs, type, best) + " victories)");
					} else if (best > 0){
						HTMLFunctions.AddListEntry(str, "Best Player: " + LoginsTable.GetDisplayName_Enc(dbs, best, true) + " (" + MissionStatsTable.GetTotalVictoriesForPlayerAndMission(dbs, type, best) + " victories)"); // Next bit errors if player not logged in:-- you have " + MissionStatsTable.GetTotalVictoriesForPlayerAndMission(dbs, type, this.current_login.getID()) + ")");
					} else {
						HTMLFunctions.AddListEntry(str, "Best Player: No-one");

					}

					for (byte side=1 ; side<=mission.getNumOfSides() ; side++) {
						// Show banned equipment groups
						StringBuffer banned = new StringBuffer(); 
						for (byte i=1 ; i<=EquipmentTypesTable.ET_MAX_TYPES ; i++) {
							try {
								//int id = 
								//if (EquipmentTypesTable.CanBuy(dbs, i) != 0) {
								String name = EquipmentTypesTable.GetMajorTypeFromID(i);
								if (name.length() > 0) {
									if (mission.isItemTypeAllowed(i, side) == false) {
										banned.append(name + ", ");
									}
								}
								//}
							} catch (RuntimeException ex) {
								// Do nothing - invalid equipment type
							}
						}
						if (banned.length() > 0) {
							banned.delete(banned.length()-2, banned.length());
							HTMLFunctions.AddListEntry(str, "'" + banned.toString() + "' are banned for side " + side);
						}
						// Show banned items
						banned = new StringBuffer(); 
						String sql = "SELECT Code, Name FROM EquipmentTypes";
						ResultSet rs = DSRWebServer.dbs.getResultSet(sql);
						while (rs.next()) {
							if (mission.isItemAllowed(rs.getString("Code"), side) == false) {
								banned.append(rs.getString("Name") + ", ");
							}
						}
						if (banned.length() > 0) {
							banned.delete(banned.length()-2, banned.length());
							HTMLFunctions.AddListEntry(str, "'" + banned.toString() + "' banned for side " + side);
						}
					}

					HTMLFunctions.EndUnorderedList(str);

					str.append("<hr />");
				}

				GetMissionLinks(str);

				//HTMLFunctions.Para(str, "<i>Missions marked with a * can only be played with the Android client.  If you would like to contribute some exciting mission background story, please feel free and email it to <a href=\"mailto:" + DSRWebServer.EMAIL_ADDRESS + "\">" + DSRWebServer.EMAIL_ADDRESS + "</a>.</i>");

				super.disk_cache.put(full_req, str.toString());
			} else {
				str.append(super.disk_cache.get(full_req));
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
		}

		this.body_html.append(MainLayout.GetHTML(this, "Mission Descriptions", str));		
	}


	public static void GetMissionLinks(StringBuffer str) {
		//HTMLFunctions.Heading(str, 3, "Mission Details");
		HTMLFunctions.Para(str, "Select a mission for details and information:");
		HTMLFunctions.StartUnorderedList(str);
		//if (DSRWebServer.IsSF()) {
		for (int j=0 ; j<AbstractMission.SF_MISSIONS.length ; j++) {
			int i = AbstractMission.SF_MISSIONS[j];
			if (AbstractMission.IsValidMission(i)) {
				AbstractMission mission = AbstractMission.Factory(i);
				HTMLFunctions.AddListEntry(str, mission.getMissionName(true, false) + " - " + mission.getShortDesc());
			}
		}
		/*} else {
			for (int j=0 ; j<AbstractMission.AE_MISSIONS.length ; j++) {
				int i = AbstractMission.AE_MISSIONS[j];
				if (AbstractMission.IsValidMission(i)) {
					AbstractMission mission = AbstractMission.Factory(i);
					HTMLFunctions.AddListEntry(str, mission.getMissionName(true, false));
				}
			}
		}*/
		HTMLFunctions.EndUnorderedList(str);
	}


}
