package dsrwebserver.pages.dsr;


public class leaguepointsgraph { /*extends AbstractHTMLPage { Todo - don't store in web cache!

	//private static final String FILENAME = "graph_cache/elopoints_";
	private static final long DURATION = Dates.HOUR;

	//private static Interval update_interval = new Interval(Dates.HOUR, false);

	public leaguepointsgraph() {
		super();
	}


	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		int loginid = this.headers.getGetValueAsInt("loginid");

		if (loginid > 0) {
			File f = new File("./webroot/" + getPNGFilename(loginid));
			if (f.exists() == false || f.lastModified() < (System.currentTimeMillis() - DURATION)) {
				// Create the graph!
				//ResultSet rs_games = dbs.getResultSet("SELECT * FROM Games WHERE GameType <> " + GameRequestsTable.GT_PRACTISE  + " AND GameStatus = " + GamesTable.GS_FINISHED + " AND " +GamesTable.GetPlayerSubQuery(loginid) + " ORDER BY DateFinished"); 			String sql = "SELECT GameID FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED + " AND " + AbstractTable.GetPlayerSubQuery(this.current_login.getLoginID());

				StringBuffer csv = new StringBuffer();
				csv.append("Games\n");
				csv.append("Points\n");

				String sql = "SELECT GameID FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED + " AND " + AbstractTable.GetPlayerSubQuery(loginid);
				sql = sql + " ORDER BY DateFinished";// LIMIT 50";
				ResultSet rs = dbs.getResultSet(sql);

				GamesTable game = new GamesTable(dbs);
				int num = 1;
				while (rs.next()) {
					game.selectRow(rs.getInt("GameID"));
					int our_side = game.getSideFromPlayerID(loginid);
					int points = game.getELORunningTotalForSide(our_side);
					if (points > 0) {
						csv.append(num + "     " + points + "\n");
					}
					num++;
				}
				rs.close();
				TextFile.QuickWrite("./webroot/" + getCSVFilename(loginid), csv.toString(), true);

				Process p = Runtime.getRuntime().exec("./webroot/graph_cache/script.sh ./webroot/graph_cache/elopoints_" + loginid + ".csv ./webroot/graph_cache/elopoints_" + loginid + ".png " + loginid);
				p.waitFor();
				int result = p.exitValue();
			}

			HTMLFunctions.Heading(str, 2, "League Points for " + LoginsTable.GetDisplayName_Enc(dbs, loginid, true) + " Over Time");
			str.append("<img src=\"/graph_cache/elopoints_" + loginid + ".png\" />");

			HTMLFunctions.Para(str, "Thanks to Astro for the graph code.");
		} else {
			HTMLFunctions.Para(str, "Invalid player ID.");
		}
		this.body_html.append(MainLayout.GetHTML(this, "Players Points Graph", str));		

	}


	private String getCSVFilename(int loginid) {
		return FILENAME + loginid + ".csv";
	}

	private String getPNGFilename(int loginid) {
		return FILENAME + loginid + ".png";
	}
*/
}
