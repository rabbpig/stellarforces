package dsrwebserver.pages.dsr;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.html.HTMLFunctions;
import ssmith.util.MyList;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.AbstractTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;

public final class MyGames extends AbstractHTMLPage {

	private static final String BOLD_START = "<b>";
	private static final String BOLD_END = "</b>";
	private static final String WAITING_ACTIONS_REPLACE = "#WAITING_ACTIONS_REPLACE#";

	private int action_count;
	private boolean any_games = false;

	public MyGames() {
		super();
	}


	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		if (this.session.isLoggedIn()) {
			HTMLFunctions.Para(str, "This page shows the details of all your current games.  Look under the <b>Next Action</b> column to see what your next action is for each game.  This will tell you whether you are waiting for your opponent or you need to do something yourself if your opponent is waiting for you.");

			HTMLFunctions.Para(str, WAITING_ACTIONS_REPLACE);

			action_count = 0;
			any_games = false;

			HTMLFunctions.StartTable(str, "stats", "", 1, "", 5);
			HTMLFunctions.StartRow(str);
			if (super.conn.headers.isMobile() == false) {
				HTMLFunctions.AddCellHeading(str, "Game");
			}
			//HTMLFunctions.AddCellHeading(str, "Client");
			HTMLFunctions.AddCellHeading(str, "Mission");
			//HTMLFunctions.AddCellHeading(str, "Your Side");
			HTMLFunctions.AddCellHeading(str, "Opponent");
			//HTMLFunctions.AddCellHeading(str, "Settings");
			HTMLFunctions.AddCellHeading(str, "Turn (Max)");
			HTMLFunctions.AddCellHeading(str, "Details");
			HTMLFunctions.AddCellHeading(str, "Next Action");
			HTMLFunctions.AddCellHeading(str, "Days Waiting");
			HTMLFunctions.EndRow(str);

			listGames(str);

			HTMLFunctions.EndTable(str);

			int pos = str.indexOf(WAITING_ACTIONS_REPLACE);
			if (action_count > 0) {
				str.replace(pos, pos+WAITING_ACTIONS_REPLACE.length(), "<h1><i>You have " + action_count + " actions to perform!</i></h1>");
			} else {
				str.replace(pos, pos+WAITING_ACTIONS_REPLACE.length(), "");
			}

			if (any_games == false) {
				HTMLFunctions.Para(str, "You don't currently have any games.  Why not <a href=\"/dsr/GameRequests.cls\">request or join a game</a>?");
			}

			HTMLFunctions.Para(str, "<a href=\"/dsr/finishedgames.cls\">View Your Finished Games</a>");

			this.body_html.append(MainLayout.GetHTML(this, "My Games", str));		
		} else {
			this.redirectTo_Using303("/");
		}
	}


	private void listGames(StringBuffer str) throws SQLException {
		String sql = "SELECT GameID, GameStatus FROM Games WHERE GameStatus <> " + GamesTable.GS_FINISHED + " AND " + AbstractTable.GetPlayerSubQuery(this.current_login.getLoginID());// (";
		//sql = sql + " AND COALESCE(CampGame, 0) = " + SQLFuncs.b201(camp);
		sql = sql + " ORDER BY DateCreated DESC";
		ResultSet rs = dbs.getResultSet(sql);

		GamesTable game = new GamesTable(dbs);
		MyList<Integer> winners = null;
		while (rs.next()) {
			game.selectRow(rs.getInt("GameID"));
			any_games = true;

			AbstractMission mission = AbstractMission.Factory(game.getMissionID());
			int our_side = game.getSideFromPlayerID(this.current_login.getLoginID());

			HTMLFunctions.StartRow(str);
			if (super.conn.headers.isMobile() == false) {
				HTMLFunctions.AddCell(str, "" + rs.getInt("GameID"));
			}
			
			/*String client = "";
			switch (mission.canBePlayedOnAndroid()) {
			case AbstractMission.NOT_ON_ANDROID:
				client = "PC";
				break;
			case AbstractMission.ONLY_ONE_CLIENT:
				client = rs.getInt("IsAndroid") == 0 ? "PC":"Android";
				break;
			case AbstractMission.ANY_CLIENT:
				//HTMLFunctions.AddCell(str, "PC or Android");
				client = "PC or Android";
				break;
			default:
				throw new RuntimeException("Unknown type: " + mission.canBePlayedOnAndroid());
			}*/
			
			HTMLFunctions.AddCell(str, mission.getMissionName(true, game.isCampGame()) + (game.isCampGame() ? " (campaign)":"") + "");
			//HTMLFunctions.AddCell(str, mission.getSideName(dbs, game, our_side) + " (side " + our_side + ")");

			String opponents = game.getOpponentsNamesByLoginID(this.current_login.getID(), true);
			HTMLFunctions.AddCell(str, opponents);
			//HTMLFunctions.AddCell(str, game.getSettings());
			HTMLFunctions.AddCell(str, "<nobr>" + game.getTurnNo() + " (" + mission.getMaxTurns() + ")</nobr>");

			// Details
			String s = "<td valign=\"top\"";
			if (game.getProposeConcedeLoginID() > 0) {
				s = s + " style=\"background-color: red;\"";
			}
			s = s + "><a href=\"viewmap.cls?gid=" + game.getID() + "\">Full Details &amp; Map</a>";
			if (game.getProposeConcedeLoginID() > 0) {
				s = s + " | Proposed Mutual Concede!";
			}
			s = s + "</td>";
			str.append(s);

			boolean waiting = false;
			if (rs.getInt("GameStatus") == GamesTable.GS_CREATED_DEPLOYMENT) {
				if (game.hasSideEquipped(game.getSideFromPlayerID(this.current_login.getLoginID())) == false) {
					HTMLFunctions.AddCell(str, 1, BOLD_START + "<a href=\"BuyArmour.cls?gid=" + game.getID() + "\">EQUIP UNITS</a>" + BOLD_END, 0, "", "", "background-red");
					action_count++;
				} else if (game.hasSideDeployed(game.getSideFromPlayerID(this.current_login.getLoginID())) == false) {
					HTMLFunctions.AddCell(str, 1, BOLD_START + "DEPLOY UNITS! (Run client)" + BOLD_END, 0, "", "", "background-red");
					action_count++;
				} else {
					int waiting_for_side = game.getWaitingForSide();
					if (waiting_for_side > 0) {
						if (game.isSideAI(waiting_for_side)) {
							HTMLFunctions.AddCell(str, 1, BOLD_START + "WAITING FOR AI! (Run client)" + BOLD_END, 0, "", "", "background-red");
							action_count++;
						} else {
							int waiting_for_loginid = game.getLoginIDOfWaitingForPlayer();
							if (waiting_for_loginid > 0) {
								HTMLFunctions.AddCell(str, "Waiting for " + LoginsTable.GetDisplayName_Enc(dbs, waiting_for_loginid, true) + " to deploy");
							} else {
								HTMLFunctions.AddCell(str, "ERROR! (2)");
							}

							waiting = true;
						} 
					} else {
						HTMLFunctions.AddCell(str, "Waiting for opponent to join");
					}
				}
			} else if (rs.getInt("GameStatus") == GamesTable.GS_STARTED) {
				int waiting_for_loginid = game.getLoginIDOfWaitingForPlayer();
				int waiting_for_side = game.getSideFromPlayerID(waiting_for_loginid);
				if (waiting_for_loginid == this.current_login.getLoginID()) {
					HTMLFunctions.AddCell(str, 1, BOLD_START + "TAKE YOUR TURN! (Run the client)" + BOLD_END, 0, "", "", "background-red");
					action_count++;
				} else {
					String text = "";
					if (mission.isSnafu()) {
						text = "Waiting for an opponent to take turn";
					} else {
						if (game.hasSideEquipped(waiting_for_side) == false) {
							text = "Waiting for " + LoginsTable.GetDisplayName_Enc(dbs, waiting_for_loginid, true) + " to equip";
						} else if (game.hasSideDeployed(waiting_for_side) == false) {
							text = "Waiting for " + LoginsTable.GetDisplayName_Enc(dbs, waiting_for_loginid, true) + " to deploy";
						} else {
							text = "Waiting for " + LoginsTable.GetDisplayName_Enc(dbs, waiting_for_loginid, true) + " to take turn";
						}
					}
					HTMLFunctions.AddCell(str, text);
					waiting = true;
				}
			} else if (rs.getInt("GameStatus") == GamesTable.GS_FINISHED) {
				if (game.getWinType() == GamesTable.WIN_DRAW) {
					HTMLFunctions.AddCell(str, "Draw");
				} else if (game.getWinType() == GamesTable.WIN_CANCELLED) {
					HTMLFunctions.AddCell(str, "Game cancelled");
				} else if (game.getWinType() == GamesTable.WIN_DRAW_MUTUAL_CONCEDE) {
					HTMLFunctions.AddCell(str, "Mutual Concede");
				} else if (game.getWinningSides(winners).contains(our_side)) {
					HTMLFunctions.AddCell(str, "WON!");
				} else {
					HTMLFunctions.AddCell(str, "LOST");
				}
			}

			// Days waiting
			if (game.getNumOfSides() > 1 && rs.getInt("GameStatus") != GamesTable.GS_FINISHED && game.haveAllPlayersJoined()) {
				StringBuffer days = new StringBuffer();
				int days_since_turn_started = game.getDaysSinceTurnStarted();
				days.append("<nobr>" + days_since_turn_started);

				/*if (waiting) {
					int days_since_reminder_sent = game.getDaysSinceLastReminder();
					if (days_since_reminder_sent >= sendreminder.DAYS_BETWEEN_REMINDERS || (game.getTurnNo() <= 1 && days_since_reminder_sent >= (sendreminder.DAYS_BETWEEN_REMINDERS/2))) {
						days.append(" (<a href=\"sendreminder.cls?gid=" + rs.getInt("GameID") + "\">send reminder</a>)");
					}
				}*/
				days.append("</nobr>");
				int cancel_days = game.getCancelDays();
				if (days_since_turn_started >= cancel_days && waiting) {
					String text = "FORCE CANCEL";
					if (game.getNumOfSides() > 2) {
						text = "FORCE REMOVAL";
					}
					days.append("<br />(<a href=\"cancelgame.cls?gid=" + rs.getInt("GameID") + "\">" + text + "</a>)");
				}
				// Add holiday text
				int waiting_for_login_id = game.getLoginIDOfWaitingForPlayer();
				String hol_text = LoginsTable.GetOnHolidayText(dbs, waiting_for_login_id);
				if (hol_text.length() > 0) {
					hol_text = " - (" + LoginsTable.GetDisplayName_Enc(DSRWebServer.dbs, waiting_for_login_id, false) + ") " + HTMLFunctions.s2HTML(hol_text) + "";
				}
				HTMLFunctions.AddCell(str, days.toString() + " " + hol_text);
			} else {
				HTMLFunctions.AddCell(str, "");
			}

			HTMLFunctions.EndRow(str);

		}
		game.close();
	}
}
