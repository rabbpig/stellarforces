package dsrwebserver.pages.dsr;


public final class register { /*extends AbstractHTMLPage { 

	private String warning = "";

	public register() {
		super();
	}

	public void process() throws Exception {
		this.setTitle("Register");

		String next = this.headers.getPostValueAsString("next");
		String pwd1 = this.headers.getPostValueAsString("pwd1");
		String pwd2 = this.headers.getPostValueAsString("pwd2");
		String display_name = this.headers.getPostValueAsString("displayname");
		String email = this.headers.getPostValueAsString("email");
		int user_answer = this.headers.getPostValueAsInt("user_answer");
		String ans = this.headers.getPostValueAsString("ans");
		int correct_answer = 0;
		if (ans.length() > 2) {
			correct_answer = Functions.ParseInt(ans.substring(2, ans.length()-2), false);
		}

		if (next.equalsIgnoreCase("save")) {
			if (user_answer == correct_answer) {
				if (email.length() > 0) {
					if (pwd1.equalsIgnoreCase(pwd2)) {
						warning = current_login.createLogin(pwd1, display_name, email);
						if (warning.length() == 0) {
							current_login.sendEmail("Welcome!", "Welcome to " + DSRWebServer.TITLE + "!\n\nThe first thing to do is visit the website at " + DSRWebServer.WEBSITE_URL + " to join in any games or start your own.  You can play as many concurrent games as you can handle!  If you haven't already, get the Android client from " + DSRWebServer.ANDROID_WEB_URL_FREE + ".\n\nIf you have any questions or queries, no matter how trivial, just reply to this or ask on the forums.  We're all more than ready to help new players.  If you like the game, please tell others about it.  If you don't, please tell us.\n\nHope you have fun, and good luck!");
							//MessagesTable.SendMsg(dbs, DSRWebServer.ADMIN_LOGIN_ID, this.current_login.getID(), "Welcome to " + AppletMain.TITLE, "Welcome to " + AppletMain.TITLE + ".  Good luck in your games, and if you have any problems just reply to this message!");
							session.setLoggedIn(current_login.getLoginID(), true);
							this.redirectToMainPage("warning=" + java.net.URLEncoder.encode("Thank you for registering!  You are now logged in.  Please check to see if you receive the confirmation email.", "UTF-8"));
							return;
						}
					} else {
						warning = "Sorry, your passwords do not match.";
					}
				} else {
					warning = "Please enter an email address.";
				}
			} else {
				warning = "You need to brush up on your maths!";
			}
		}

		StringBuffer str = new StringBuffer();

		//HTMLFunctions.Heading(str, 1, "Register");
		if (this.session.isLoggedIn() == false) {
			HTMLFunctions.Para(str, "Please enter the following details to start right now!:-");

			str.append("<center>");
			str.append(GetRegisterBox(warning, email, pwd1, pwd2, display_name));
			str.append("</center>");
			str.append("<hr />");
			HTMLFunctions.Para(str, "Once you click above to register, you will be automatically logged into the website.  You should also receive a welcoming email.  <b>If you do not receive this, you may not have entered your email address correctly, and you will not receive any game notifications!</b>");
		} else {
			HTMLFunctions.Para(str, "You are already logged in!");
		}

		body_html.append(MainLayout.GetHTML(this, "Register Here", str));
	}


	public static StringBuffer GetRegisterBox(String warning, String email, String pwd1, String pwd2, String display_name) {
		StringBuffer str = new StringBuffer();
		HTMLFunctions.StartForm(str, "form1", "", "post");
		HTMLFunctions.HiddenValue(str, "next", "save");
		//HTMLFunctions.HiddenValue(str, "edit", edit_existing ? "true" : "false");
		str.append("<hr />");
		HTMLFunctions.StartTable(str);
		if (warning.length() > 0) {
			HTMLFunctions.StartRow(str);
			HTMLFunctions.StartCell(str);
			HTMLFunctions.Para(str, "warning", warning);
			HTMLFunctions.EndCell(str);
			HTMLFunctions.EndRow(str);
		}
		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, "Email Address: (this will never be shared or displayed to anyone)");
		HTMLFunctions.EndRow(str);
		HTMLFunctions.StartRow(str);
		HTMLFunctions.StartCell(str);
		HTMLFunctions.TextBox(str, "email", email, false, 99, 40);
		HTMLFunctions.EndCell(str);
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, "Password:");
		HTMLFunctions.EndRow(str);
		HTMLFunctions.StartRow(str);
		HTMLFunctions.StartCell(str);
		HTMLFunctions.PasswordBox(str, "pwd1", pwd1, 49);
		HTMLFunctions.EndCell(str);
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, "Confirm Password:");
		HTMLFunctions.EndRow(str);
		HTMLFunctions.StartRow(str);
		HTMLFunctions.StartCell(str);
		HTMLFunctions.PasswordBox(str, "pwd2", pwd2, 49);
		HTMLFunctions.EndCell(str);
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, "Display Name:");
		HTMLFunctions.EndRow(str);
		HTMLFunctions.StartRow(str);
		HTMLFunctions.StartCell(str);
		HTMLFunctions.TextBox(str, "displayname", display_name, false, 99, 40);
		HTMLFunctions.EndCell(str);
		HTMLFunctions.EndRow(str);

		int sum[] = new int[3];
		int tot = 0;
		for (int i=0 ; i<sum.length ; i++) {
			sum[i] = Functions.rnd(0, 9);
			tot += sum[i];
		}
		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, "Maths Test (to check you are human):  What is " + sum[0] + "+" + sum[1] + "+" + sum[2] + "?");
		HTMLFunctions.EndRow(str);
		HTMLFunctions.StartRow(str);
		HTMLFunctions.StartCell(str);
		HTMLFunctions.TextBox(str, "user_answer", "", false, 10, 5);
		HTMLFunctions.EndCell(str);
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, HTMLFuncs_old.SubmitButton("I want to register!!"));
		HTMLFunctions.EndRow(str);

		HTMLFunctions.EndTable(str);

		HTMLFunctions.HiddenValue(str, "ans", "12" + tot + "4x");
		HTMLFunctions.EndForm(str);

		return str;
	}
*/
}
