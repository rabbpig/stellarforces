package dsrwebserver.pages.dsr;

import ssmith.html.HTMLFunctions;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;

public class controls extends AbstractHTMLPage {

	public controls() {
		super();
	}

	@Override
	public void process() throws Exception {
		String full_req = super.disk_cache.convertPath(super.conn.headers.request.getFullURL());
		StringBuffer str = new StringBuffer();
		if (super.disk_cache.contains(full_req) == false) {
			String[][] keys = new String[16][4];

			// Camera
			keys[1][0] = "<b>W</b><br />Camera Forwards";
			keys[0][1] = "<b>A</b><br />Camera Left";
			keys[1][1] = "<b>S</b><br />Camera Backwards";
			keys[2][1] = "<b>D</b><br />Camera Right";
			keys[0][0] = "<b>Q</b><br />Camera Down";
			keys[2][0] = "<b>E</b><br />Camera Up";

			// Misc
			keys[7][2] = "<b>N</b><br />Next Unit";
			keys[8][0] = "<b>P</b><br />Prev Unit";
			keys[5][1] = "<b>G</b><br />Toggle Grid";
			keys[6][1] = "<b>H</b><br />Toggle Help";
			keys[9][1] = "<b>L</b><br />Toggle Lights";
			keys[8][2] = "<b>M</b><br />Toggle Music";
			keys[2][3] = "<b>C</b><br />Toggle Camera Lock";
			keys[8][1] = "<b>K</b><br />Show LOS";
			keys[0][3] = "<b>Z</b><br />Toggle Scanner";

			// Arrow keys
			keys[10][2] = "<b>Up</b><br />Cam Look Up";
			keys[9][3] = "<b>Left</b><br />Cam Look Left";
			keys[10][3] = "<b>Down</b><br />Cam Look Down";
			keys[11][3] = "<b>Right</b><br />Cam Look Right";

			// Move Unit
			keys[13][0] = "<b>7</b><br />Turn Unit Left";
			keys[14][0] = "<b>8</b><br />Move Unit Forwards";
			keys[15][0] = "<b>9</b><br />Turn Unit Right";
			keys[13][1] = "<b>4</b><br />Slide Unit Left";
			keys[15][1] = "<b>6</b><br />Slide Unit Right";
			keys[14][2] = "<b>2</b><br />Move Unit Backwards";

			HTMLFunctions.Para(str, "These are the controls for the Windows & Linux clients.  The Android client is entirely controlled with icons.");

			HTMLFunctions.Heading(str, 3, "Mouse Controls");

			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "Press the left mouse button to select icons");
			HTMLFunctions.AddListEntry(str, "To rotate the view, hold the left mouse button down while moving the mouse (when a unit is not selected).");
			HTMLFunctions.AddListEntry(str, "To query what a piece of equipment is on the floor, right click on it.");
			HTMLFunctions.EndUnorderedList(str);

			//HTMLFunctions.Para(str, "Although the game can be controlled entirely with the mouse, the following keyboard shortcust can be used:-");

			HTMLFunctions.Heading(str, 3, "Keyboard Shortcuts");

			HTMLFunctions.StartTable(str, "", "", 0, "", 0);
			for (int y=0 ; y<keys[0].length ; y++) {
				HTMLFunctions.StartRow(str);
				for (int x=0 ; x<keys.length ; x++) {
					if (keys[x][y] != null) {
						HTMLFunctions.AddCell(str, "<div style=\"border: solid 2px #060; \">" + keys[x][y] + "</div>");
					} else {
						HTMLFunctions.AddCell(str, "");
					}
				}			
				HTMLFunctions.EndRow(str);
			}
			HTMLFunctions.EndTable(str);

			HTMLFunctions.Para(str, "The controls are fully redefinable.  See the wiki for more details.");

			super.disk_cache.put(full_req, str.toString());
		} else {
			str.append(super.disk_cache.get(full_req));
		}

		this.body_html.append(MainLayout.GetHTML(this, "Controls", str));		
	}

}
