package dsrwebserver.pages.dsr;

import java.sql.ResultSet;

import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.PlayerAwardsTable;

public class playerawards extends AbstractHTMLPage {

	public playerawards() {
		super();
	}

	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();
		if (this.session.isLoggedIn()) {
			if (PlayerAwardsTable.CanPlayerMakeAward(dbs, current_login)) {
				HTMLFunctions.Heading(str, 3, "You can award a player award!");
			} else {
				//HTMLFunctions.Heading(str, 3, "Sorry, you cannot award a player award yet!");  Don't have since it redirects here immed after sending an award.
			}

			HTMLFunctions.Para(str, "Players who have won a mission within the last " + DSRWebServer.DAYS_BETWEEN_AWARDS + " days and have not already sent an award within that time are able to send their own award to another player.");
			HTMLFunctions.Para(str, "To send an award to another player, visit that player's page.  If you can award an award, enter the name of the award (e.g. 'Luckiest Shot Award') in the box and then click on 'Send Award'.");
			
			str.append("<hr />");
			
			HTMLFunctions.Para(str, "You have received the following awards:-");

			HTMLFunctions.StartTable(str, "stats", "", 1, "", 5);
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCellHeading(str, "From");
			HTMLFunctions.AddCellHeading(str, "Award Name");
			HTMLFunctions.AddCellHeading(str, "Date Send");
			HTMLFunctions.EndRow(str);

			String sql = "SELECT * FROM PlayerAwards WHERE LoginIDTo = " + this.current_login.getID() + " ORDER BY DateCreated DESC";
			ResultSet rs = dbs.getResultSet(sql);
			while (rs.next()) {
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginIDFrom"), true));
				HTMLFunctions.AddCell(str, HTMLFunctions.s2HTML(rs.getString("Name")));
				HTMLFunctions.AddCell(str, Dates.FormatDate(rs.getTimestamp("DateCreated"), "dd MMM yyyy"));
				HTMLFunctions.EndRow(str);
			}
			rs.close();

			HTMLFunctions.EndTable(str);

			str.append("<hr />");
			
			HTMLFunctions.Para(str, "You have sent the following awards:-");

			HTMLFunctions.StartTable(str, "stats", "", 1, "", 5);
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCellHeading(str, "To");
			HTMLFunctions.AddCellHeading(str, "Award Name");
			HTMLFunctions.AddCellHeading(str, "Date Send");
			HTMLFunctions.EndRow(str);

			sql = "SELECT * FROM PlayerAwards WHERE LoginIDFrom = " + this.current_login.getID() + " ORDER BY DateCreated DESC";
			rs = dbs.getResultSet(sql);
			while (rs.next()) {
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginIDTo"), true));
				HTMLFunctions.AddCell(str, HTMLFunctions.s2HTML(rs.getString("Name")));
				HTMLFunctions.AddCell(str, Dates.FormatDate(rs.getTimestamp("DateCreated"), "dd MMM yyyy"));
				HTMLFunctions.EndRow(str);
			}


			HTMLFunctions.EndTable(str);

		}
		this.body_html.append(MainLayout.GetHTML(this, "Player Awards", str));		
	}

}
