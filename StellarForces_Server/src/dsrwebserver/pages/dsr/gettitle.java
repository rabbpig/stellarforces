package dsrwebserver.pages.dsr;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import ssmith.awt.FontLoader;
import ssmith.image.CustomPNG;
import dsr.AppletMain;
import dsr.comms.AbstractCommFuncs;
import dsrwebserver.DSRWebServer;

public class gettitle extends AbstractDSRPage {

	private static final int MAX_CACHE_SIZE = 30;
	private static final int UNDERLINE_SPACING = 5;

	private static Font base_font = FontLoader.GetFont(AppletMain.DATA_DIR + "XENOTRON.TTF");
	private static Hashtable<String, CustomPNG> cache = new Hashtable<String, CustomPNG>();
	private byte png_data[];
	private static Color main_col, shade_col; 

	public gettitle() {
		super();

		while (cache.size() > MAX_CACHE_SIZE) {
			Enumeration<String> it = cache.keys();
			String key = it.nextElement();
			cache.remove(key);
		}

		if (main_col == null) {
				main_col = new Color(250, 200, 53);
				shade_col = new Color(150, 100, 25);
		}
	}


	@Override
	public void process() throws Exception {
		this.content_type = "image/png";

		String title = AbstractCommFuncs.URLDecodeString(this.headers.getGetValueAsString("t"));
		int height = this.headers.getGetValueAsInt("h");
		boolean underline = this.headers.getGetValueAsInt("ul") == 1;
		if (height == 0) {
			height = 20;
		}
		Font font = base_font.deriveFont(Font.PLAIN, height);

		CustomPNG png = cache.get(title);
		if (png == null) {
			png = new CustomPNG(10, 10);
			Graphics g = png.getGraphics();
			g.setFont(font);
			FontMetrics fm = g.getFontMetrics();
			int w = fm.stringWidth(title);
			int max_w = 1100;
			while (w > max_w) {
				height = height - 2;
				font = base_font.deriveFont(Font.PLAIN, height);
				g.setFont(font);
				fm = g.getFontMetrics();
				w = fm.stringWidth(title);
			}
			int h = fm.getHeight();
			g = null;
			png = null;

			// Recreate now we know the size
			w = w + 5;
			h = h + UNDERLINE_SPACING;
			try {
				png = new CustomPNG(w, h);
			} catch (java.lang.IllegalArgumentException ex) {
				png = new CustomPNG(w, h+2);
				DSRWebServer.SendEmailToAdmin("Error", "Error with title: '" + title + "' height:" + height);
			}
			g = png.getGraphics();
			g.setFont(font);

			// Helpful rectangle
			//g.setColor(Color.red);
			//g.drawRect(0, 0, w-1, h-1);

			g.setColor(main_col);//new Color(250, 200, 53));
			g.drawString(title, 5, h-UNDERLINE_SPACING);

			// Draw underline?
			if (underline) {
				int thickness = 2;//height/6;
				g.fillRect(0, h-thickness, w, thickness);  // height-2
			}

			g.setColor(shade_col);//new Color(150, 100, 25));
			g.setClip(0, h/2, w, height);
			g.drawString(title, 5, h-UNDERLINE_SPACING);

			cache.put(title, png);
		}
		png_data = png.generateDataAsPNG();
		this.addHTTPHeader("Content-Length: " + png_data.length);

	}


	protected void writeContent() throws IOException {
		if (png_data != null) {
			DataOutputStream dos = conn.getDataOutputStream();
			dos.write(png_data);
			dos.flush();
		}
	}



}
