package dsrwebserver.pages.dsr;

import ssmith.html.HTMLFunctions;
import ssmith.io.Base64;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.LoginsTable;

public class validateemail extends AbstractHTMLPage {

	public validateemail() {
		super();
	}


	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		String email_enc = this.headers.getGetValueAsString("email");
		String email = Base64.decodeString(email_enc);

		if (email.length() > 0) {
			LoginsTable login = new LoginsTable(dbs);
			if (login.selectUserByEmailAddress(email)) {
				login.setEmailAddressValidated();
				this.redirectTo_UsingRefresh("/", 5);

				HTMLFunctions.Heading(str, 2, "Please wait...");
				HTMLFunctions.Para(str, "Thank you, your email address has been validated.  You will now be redirected to the <a href=\"/\">main page</a>.");
			} else {
				HTMLFunctions.Para(str, "Sorry, that email address could not be found.");
			}
			login.close();
		} else {
			HTMLFunctions.Para(str, "Sorry, that email address could not be found.");
		}
		this.body_html.append(MainLayout.GetHTML(this, "Validate Email Address", str));		

	}

}
