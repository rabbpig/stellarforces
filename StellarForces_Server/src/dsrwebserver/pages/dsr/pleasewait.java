package dsrwebserver.pages.dsr;

import ssmith.html.HTMLFunctions;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.GamesTable;

public class pleasewait extends AbstractHTMLPage {

	public pleasewait() {
		super();
		send_google_analytics = false;
	}

	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		int gameid = this.headers.getGetValueAsInt("gid");
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);
		if (game.getMapDataID() <= 0) {
			this.redirectTo_UsingRefresh("", 5);

			//HTMLFunctions.Heading(str, 2, "Please wait...");
			HTMLFunctions.Para(str, "The game is being created.  This should take less than a minute.  	You will be automatically redirected when this process is complete.");
			HTMLFunctions.Para(str, "A new subforum is also being created for this game, and both players are automatically subscribed to it.  You can access the specific subforum from a link in the <b>Game Details</b> page.");
			HTMLFunctions.Para(str, "If this page does not refresh after a few minutes, try <a href=\"BuyArmour.cls?gid=" + gameid + "\">this link</a>.");
		} else {
			this.redirectTo_UsingRefresh("BuyArmour.cls?gid=" + gameid, 0);
		}
		game.close();
		this.body_html.append(MainLayout.GetHTML(this, "Please wait...", str));		

	}

}
