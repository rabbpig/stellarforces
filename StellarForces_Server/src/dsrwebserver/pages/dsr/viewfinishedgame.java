package dsrwebserver.pages.dsr;

import java.sql.ResultSet;

import ssmith.html.HTMLFunctions;
import ssmith.image.ImageFunctions;
import ssmith.lang.Dates;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.GameRequestsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.UnitsTable;

public class viewfinishedgame extends AbstractHTMLPage {

	public viewfinishedgame() {
		super();
	}

	@Override
	public void process() throws Exception {
		String full_req = super.disk_cache.convertPath(super.conn.headers.request.getFullURL());
		if (full_req.length() > 256) {
			return;
		}
		StringBuffer str = new StringBuffer();
		this.setTitle("Finished Game");
		int gid = this.headers.getGetValueAsInt("gid");
		String cmd = this.headers.getGetValueAsString("cmd");
		if (cmd.equalsIgnoreCase("makedraw")) {
			dbs.runSQLUpdate("UPDATE Games SET WinningSide = -1, WinningSide2 = -1, WinType = " + GamesTable.WIN_DRAW +" WHERE GameID = " + gid);
			this.redirectTo_Using303("?gid=" + gid);
			return;
		}
		if (super.disk_cache.contains(full_req) == false) {
			int turn_no = this.headers.getGetValueAsInt("turn_no");
			int turn_side = this.headers.getGetValueAsInt("turn_side");

			GamesTable game = new GamesTable(dbs);
			try {
				if (game.doesRowExist(gid)) {
					game.selectRow(gid);
					if (game.getGameStatus() == GamesTable.GS_FINISHED) {
						if (turn_no > 0) {
							this.setTitle(this.getTitle() + ": Turn " + turn_no);
						}

						AbstractMission mission = AbstractMission.Factory(game.getMissionID());
						String opponents = game.getOpponentsNamesBySide(-1, false);
						HTMLFunctions.Heading(str, 2, opponents + " in " + mission.mission_name);

						/*int max = Math.max(UnitHistoryTable.GetMaxTurnNo(dbs, gid), 1);
					if (max > 0) {
						str.append("<p>Select turn number to view game at that stage. ");
						// Loop through available data
						ResultSet rs = dbs.getResultSet("SELECT DISTINCT TurnNo FROM UnitHistory WHERE GameID = " + gid + " ORDER BY TurnNo");//AND EndOfTurn = 1 
						while (rs.next()) {
							if (rs.getInt("TurnNo") == turn_no) {
								str.append("<span class=\"large\">");
							}
							str.append(rs.getInt("TurnNo") + " ");
							for (int i=1 ; i<= game.getNumOfSides() ; i++) {
								char c = (char)(i+96);
								str.append("<a href=\"?gid=" + gid + "&turn_no=" + rs.getInt("TurnNo") + "&turn_side=" + i + "\" rel=\"nofollow\">" + c + "</a> ");
							}
							if (rs.getInt("TurnNo") == turn_no) {
								str.append("</span>");
							}
						}
						str.append("<a href=\"?gid=" + gid + "\" rel=\"nofollow\" >End</a></p>");
					}*/

						/*int our_side = -1;
					try {
						if (this.session.isLoggedIn()) {
							if (game.isPlayerInGame(current_login.getLoginID())) {
								our_side = game.getSideFromPlayerID(current_login.getLoginID());
							}
						}
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex);
					}*/

						str.append("<br clear=\"all\" />");

						str.append("<img align=left hspace=10 src=\"MapImageForFinishedGame.cls?gid=" + gid + "&turn_no=" + turn_no + "&turn_side=" + turn_side + "\" alt=\"Strategic scanner for current game\" >");

						// Draw units table at top
						HTMLFunctions.StartTable(str, "stats", "", 0, "", 5);
						HTMLFunctions.StartRow(str);
						HTMLFunctions.AddCellHeading(str, "Side");
						HTMLFunctions.AddCellHeading(str, "Name");
						HTMLFunctions.AddCellHeading(str, "Units");
						HTMLFunctions.AddCellHeading(str, "VPs");
						//HTMLFunctions.AddCellHeading(str, "Colour");
						if (mission.isSnafu()) {
							HTMLFunctions.AddCellHeading(str, "Objective");
						}
						HTMLFunctions.AddCellHeading(str, "League");
						HTMLFunctions.EndRow(str);

						for (int s=1 ; s<=game.getNumOfSides() ; s++){
							int units = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + gid + " AND Status  = " + UnitsTable.ST_DEPLOYED + " AND Side = " + s);

							HTMLFunctions.StartRow(str);
							HTMLFunctions.AddCell(str, "" + s);
							HTMLFunctions.AddCell(str, LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(s), true) + " (<font color=#" + ImageFunctions.GetRGB(MapImageAbstract.GetColourForSide(s)) + ">" + mission.getSideName(dbs, game, s) + "/" + mission.getSideDescription(s) + "</font>)");
							HTMLFunctions.AddCell(str, "" + units);
							HTMLFunctions.AddCell(str, "" + game.getVPsForSide(s));
							//HTMLFunctions.AddCell(str, "<font color=#" + ImageFunctions.GetRGB(MapImageAbstract.GetColourForSide(s)) + ">This Colour</font>");
							try {
								if (mission.isSnafu()) {
									if (game.getMissionID() == AbstractMission.SNAFU_MOONBASE_ASSAULT || game.getMissionID() == AbstractMission.SNAFU_SABOTAGE) {
										if (game.getSnafuData(s) != 0) {
											HTMLFunctions.AddCell(str, "Saboteur!");
										} else {
											HTMLFunctions.AddCell(str, "Defender");
										}
									} else if (game.getMissionID() == AbstractMission.SNAFU_THE_ASSASSINS) {
										if (game.getSnafuData(s) != 0) {
											HTMLFunctions.AddCell(str, "Assassins (kill side " + game.getSnafuData(s) + "!");
										} else {
											HTMLFunctions.AddCell(str, "Defender");
										}
									} else if (game.getMissionID() == AbstractMission.SNAFU_THE_ASSASSINS_2) {
										HTMLFunctions.AddCell(str, "Kill side " + game.getSnafuData(s) + "!");
									}
								}
							} catch (Exception ex) {
								DSRWebServer.HandleError(ex, true);
							}
							HTMLFunctions.AddCell(str, "" + game.getELOPointsForSide(s));
							HTMLFunctions.EndRow(str);
						}
						HTMLFunctions.EndTable(str);

						if (game.getForumID() > 0) {
							str.append("<h2><a href=\"/dsr/forums/ForumPostingsPage.cls?topic=" + game.getForumID() + "\">Game Chat</a></h2>");
						}
						str.append("<h2><a href=\"/dsr/playbackapplet/playbackpage.cls?gid=" + game.getID() + "\" rel=\"nofollow\">Watch Playback</a></h2>");

						HTMLFunctions.Heading(str, 3, "Game Settings");
						HTMLFunctions.StartUnorderedList(str);
						HTMLFunctions.AddListEntry(str, "The mission is <a href=\"missiondescriptions.cls?type=" + mission.getType() + "&camp=" + (game.isCampGame() ? "1" : "0") + "\">" + mission.mission_name + "</a>");
						/*if (our_side > 0) {
						HTMLFunctions.AddListEntry(str, mission.getMission1Liner(our_side));
					}*/
						if (game.getGameType() == GameRequestsTable.GT_PRACTISE) {
							HTMLFunctions.AddListEntry(str, "This is a practise game");
						}
						if (game.isCampGame()) {
							HTMLFunctions.AddListEntry(str, "This is a campaign mission.");
						}
						if (game.isAdvancedMode()) {
							HTMLFunctions.AddListEntry(str, "This game is Advanced Mode" + helpwindow.GetLink(helpwindow.HC_ADVANCED_MODE));
						}
						if (game.getPcentCredit() != 0 && game.getPcentCredit() != 100) {
							HTMLFunctions.AddListEntry(str, "Both sides have " + game.getPcentCredit() + "% credits.");
						}
						switch (mission.getWallsDestroyableType()) {
						case AbstractMission.INDESTRUCTABLE_WALLS:
							HTMLFunctions.AddListEntry(str, "Walls are <b>indestructible</b>." + helpwindow.GetLink(helpwindow.HC_WALLS));
							break;
						case AbstractMission.STRONG_WALLS:
							HTMLFunctions.AddListEntry(str, "Walls are <b>strong</b> but destructible." + helpwindow.GetLink(helpwindow.HC_WALLS));
							break;
						case AbstractMission.WEAK_WALLS:
							HTMLFunctions.AddListEntry(str, "Walls are <b>weak</b>." + helpwindow.GetLink(helpwindow.HC_WALLS));
							break;
						default:
							throw new RuntimeException("Unknown wall type: " + mission.getWallsDestroyableType());
						}

						try {
							// 2v2?
							/*if (our_side > 0) {
							MyList<Integer> sides = mission.getSidesForSide(our_side);
							if (sides.size() > 1) {
								sides.removeInt(our_side);
								HTMLFunctions.AddListEntry(str, "You are on the same side as player " + sides.toCSVString() + "." + helpwindow.GetLink(helpwindow.HC_2V2));
							}
						}*/
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						HTMLFunctions.EndUnorderedList(str);

						HTMLFunctions.Heading(str, 3, "Key");
						HTMLFunctions.StartUnorderedList(str);
						/*if (game.getGameStatus() == GamesTable.GS_CREATED) {
					HTMLFunctions.AddListEntry(str, "<font color=\"#" + GetRGB(MapImageForCurrentGame.OUR_DEPLOY_AREA) + "\">Your deployment area</font>");
					HTMLFunctions.AddListEntry(str, "<font color=\"#" + GetRGB(MapImageForCurrentGame.OPP_DEPLOY_AREA) + "\">Opponent's deployment area</font>");
				}*/
						HTMLFunctions.AddListEntry(str, "<font color=\"#" + ImageFunctions.GetRGB(MapImageForCurrentGame.OUR_COMPUTER_COL) + "\">Your computer</font>");
						HTMLFunctions.AddListEntry(str, "<font color=\"#" + ImageFunctions.GetRGB(MapImageForCurrentGame.OUR_DESTROYED_COMPUTER_COL) + "\">Your Destroyed computer</font>");
						HTMLFunctions.AddListEntry(str, "<font color=\"#" + ImageFunctions.GetRGB(MapImageForCurrentGame.OPP_COMPUTER_COL) + "\">Opponent's computer</font>");
						HTMLFunctions.AddListEntry(str, "<font color=\"#" + ImageFunctions.GetRGB(MapImageForCurrentGame.OPP_DESTROYED_COMPUTER_COL) + "\">Opponent's destroyed computer</font>");
						HTMLFunctions.AddListEntry(str, "<font color=\"#" + ImageFunctions.GetRGB(MapImageForCurrentGame.ESCAPE_HATCH_COL) + "\">Escape Hatch</font>");
						HTMLFunctions.AddListEntry(str, "<font color=\"#" + ImageFunctions.GetRGB(MapImageForCurrentGame.FLAG_COL) + "\">F.L.A.G.</font>");
						HTMLFunctions.EndUnorderedList(str);

						str.append("<br clear=\"all\" />");

						//HTMLFunctions.StartTable(str, "", "0", 0, "", 5);
						HTMLFunctions.StartTable(str, "stats", "", 1, "", 5);

						HTMLFunctions.StartRow(str);
						HTMLFunctions.AddCellHeading(str, "Name");
						HTMLFunctions.AddCellHeading(str, "Status");
							HTMLFunctions.AddCellHeading(str, "Armour");
							HTMLFunctions.AddCellHeading(str, "Current Item");
						HTMLFunctions.AddCellHeading(str, "Max APs");
						HTMLFunctions.AddCellHeading(str, "Current APs");
						HTMLFunctions.AddCellHeading(str, "Health");
						//HTMLFunctions.AddCellHeading(str, "Energy");
						HTMLFunctions.AddCellHeading(str, "Shot Skill");
						HTMLFunctions.AddCellHeading(str, "CC Skill");
						HTMLFunctions.AddCellHeading(str, "Strength");
						HTMLFunctions.AddCellHeading(str, "Burden");
						//HTMLFunctions.AddCellHeading(str, "Opp Fire?");
						HTMLFunctions.EndRow(str);

						UnitsTable unit = new UnitsTable(dbs);

						String sql = "SELECT * FROM Units WHERE Status IN(" + UnitsTable.ST_DEPLOYED + ", " + UnitsTable.ST_DEAD + ", " + UnitsTable.ST_ESCAPED + ") AND GameID = " + gid + " ORDER BY Side, OrderBy";
						ResultSet rs = dbs.getResultSet(sql);
						int last_side = 0;
						EquipmentTable equipment = new EquipmentTable(dbs);
						while (rs.next()) {
							unit.selectRow(rs.getInt("UnitID"));
							if (last_side != rs.getInt("Side")) {
								last_side = rs.getInt("Side");
								HTMLFunctions.StartRow(str);
								HTMLFunctions.AddCell(str, 13, "<h3>Side " + last_side + " (" + LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(last_side), true) + ")</h3>");
								HTMLFunctions.EndRow(str);
							}
							HTMLFunctions.StartRow(str);

							HTMLFunctions.AddCell(str, unit.getOrderBy() + ": " + unit.getName());
							String status = unit.getStatusText();
							if (unit.getStatus() == UnitsTable.ST_DEAD) {
								if (unit.getKilledBySide() > 0) {
									status = status + "<br /><i>(Killed by " + LoginsTable.GetDisplayName_Enc(dbs,  game.getLoginIDFromSide(unit.getKilledBySide()), true) + ")</i>";
								}
							}
							String eq_name = "";
							try {
								if (unit.getCurrentEquipmentID() > 0) {
									equipment.selectRow(unit.getCurrentEquipmentID());
									eq_name = equipment.getName(true);
								}
							} catch (Exception ex) {
								DSRWebServer.HandleError(ex);
							}
							HTMLFunctions.AddCell(str, status);
							HTMLFunctions.AddCell(str, unit.getArmourName());
							HTMLFunctions.AddCell(str, eq_name);
							HTMLFunctions.AddCell(str, "" + unit.getMaxAPs());
							if (unit.getStatus() != UnitsTable.ST_DEAD) {
								HTMLFunctions.AddCell(str, "" + unit.getCurrentAPs() + " (" + unit.getOppFireAPs() + ")");
							} else {
								HTMLFunctions.AddCell(str, "");
							}
							HTMLFunctions.AddCell(str, "" + unit.getHealth() + " (" + unit.getMaxHealth() + ")");
							//HTMLFunctions.AddCell(str, "" + unit.getEnergy());
							HTMLFunctions.AddCell(str, "" + unit.getShotSkill());
							HTMLFunctions.AddCell(str, "" + unit.getCombatSkill());
							HTMLFunctions.AddCell(str, "" + unit.getStrength());
							HTMLFunctions.AddCell(str, "" + unit.getBurden());

							HTMLFunctions.EndRow(str);

							HTMLFunctions.StartRow(str);
							HTMLFunctions.StartCell(str, "", "", 13, "");

							// Show equipment
							ResultSet rs_equip = dbs.getResultSet("SELECT EquipmentID, Ammo FROM Equipment WHERE UnitID = " + unit.getID());
							boolean any = false;
							while (rs_equip.next()) {
								if (any == false) {
									str.append("<span class=\"little\">Equipment: </span>");
									any = true;
								}
								equipment.selectRow(rs_equip.getInt("EquipmentID"), true);
								str.append("<span class=\"little\"><a href=\"equipmentdetails.cls?eid=" + equipment.getEquipmentTypeID() + "\">" + equipment.getName(true) + "</a>,</span> ");
							}
							if (!any) {
								//str.append("None");
							} else {
								str.delete(str.length()-9, str.length());
							}
							HTMLFunctions.EndCell(str);
							HTMLFunctions.EndRow(str);
							//num++;
						}
						unit.close();
						rs.close();
						equipment.close();
						HTMLFunctions.EndTable(str);

						HTMLFunctions.Heading(str, 2, "Game Log");
						HTMLFunctions.Para(str, "<b>Note that now the mission has finished, log entries from <i>all</i> player's logs are shown, which means there will be some duplication.</b>");

						rs = dbs.getResultSet("SELECT * FROM GameLog WHERE GameID = " + gid + " ORDER BY EventTime, DateCreated");
						while (rs.next()) {
							String dt = Dates.FormatDate(rs.getTimestamp("DateCreated"), Dates.UKDATE_FORMAT2_WITH_TIME);
							String turn = "", side = "";
							if (rs.getInt("TurnNo") > 0) {
								turn = " Turn " + rs.getInt("TurnNo");
							}

							if (rs.getInt("LoginID") > 0 && game.isPlayerInGame(rs.getInt("LoginID"))) { // We need to check the player is in the game otherwise the next line will error
								side = ", Side " + GamesTable.GetSideFromPlayerID(DSRWebServer.dbs, game.getID(), rs.getInt("LoginID")); 
							}
							str.append(dt + "(" + turn + side + "): " + rs.getString("Entry") + "<br />");
						}

						HTMLFunctions.Para(str, "<b>Note that now the mission has finished, log entries from <i>all</i> player's logs are shown, which means there will be some duplication.</b>");

					} else {
						this.redirectTo_Using303("/");
						return;
					}
				}
			} finally {
				game.close();

			}
			super.disk_cache.put(full_req, str.toString());
		} else {
			str.append(super.disk_cache.get(full_req));
		}
		/*if (this.session.isLoggedIn()) { // Notice we have this at the end so it doesn't get cached
			if (this.current_login.isAdmin()) {
				str.append("<a href=\"?cmd=makedraw&gid=" + gid + "\">Make Draw</a>");
			}
		}*/

		this.body_html.append(MainLayout.GetHTML(this, this.getTitle(), str));		

	}

}
