package dsrwebserver.pages.dsr;

import java.sql.SQLException;

import ssmith.lang.Functions;
import dsrwebserver.ClientConnection;
import dsrwebserver.DSRWebServer;
import dsrwebserver.HTTPHeaders;
import dsrwebserver.pages.AbstractPage;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.SessionsTable;

public abstract class AbstractDSRPage extends AbstractPage {

	public static final String SESSION_COOKIE = "dsrid";

	public LoginsTable current_login;
	public SessionsTable session;

	public AbstractDSRPage() {
		super();
	}


	public void init(ClientConnection c, HTTPHeaders head) throws SQLException {
		super.init(c, head);

		session = new SessionsTable(DSRWebServer.dbs);
		session.selectSessionByCookie(this.getSessionID());

		String android_login = this.headers.getGetValueAsString("android_login").trim();
		String android_pwd = this.headers.getGetValueAsString("android_pwd").trim();

		current_login = new LoginsTable(dbs);
		boolean is_mob = head.isMobile();
		//DSRWebServer.p("Is mobile:" + is_mob);
		if (is_mob == false || android_login.length() == 0) {
			if (session.getLoginID() > 0) {
				current_login.selectRow(session.getLoginID());
				if (session.isLoggedIn()) {
					if (current_login.isDisabled() == false) {
						current_login.getLastLoginDate();
						//cli.add(current_login.getDisplayName_Enc(true));
						/*if (is_mob) {
							if (current_login.isRowSelected()) {
								current_login.setHasAndroid(1);
							}
						}*/
					} else {
						session.setLoggedIn(session.getLoginID(), false);
					}
				}
			}
		} else {
			// Select user for Android
			boolean found = current_login.selectUser(android_login, android_pwd);
			if (found) {
				session.setLoggedIn(current_login.getID(), true);
			} else {
				//DSRWebServer.SendEmailToAdmin("Android login failed", android_login + "/" + android_pwd);
				//throw new RuntimeException("Android Login failed!: '" + android_login + "'/'" + android_pwd + "'");
			}
		}

	}


	public String getSessionID() {
		String session_id = "";
		while (session_id.length() == 0) {
			if (headers.doesCookieExist(SESSION_COOKIE) == false) {
				// Create and add new cookie
				session_id = "" + System.currentTimeMillis() + Functions.rnd(1000, 9999);
			} else {
				// Get the cookie
				session_id = (String)headers.getCookie(SESSION_COOKIE);
			}
		}
		addCookie(SESSION_COOKIE, session_id); // Resend the cookie so it increases the expiry date
		return session_id;
	}


}
