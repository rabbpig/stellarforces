package dsrwebserver.pages.dsr;

import java.awt.Point;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import ssmith.lang.Functions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.GameRequestsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.RequestedPlayersTable;
import dsrwebserver.tables.UnitsTable;
import dsrwebserver.tables.UploadedMapsTable;

public final class GameRequests extends AbstractHTMLPage {

	private int missionid;

	public GameRequests() {
		super();

		super.addHTMLToHead("<script type=\"text/javascript\" src=\"/javascripts/misc.js\"></script>");
	}


	@Override
	public void process() throws Exception {
		String title = "Games Lobby";
		StringBuffer str = new StringBuffer();

		if (this.session.isLoggedIn()) {
			boolean campaign = this.headers.getGetOrPostValueAsInt("campaign") == 1;
			String next = this.headers.getGetOrPostValueAsString("next");
			String opp_name = this.headers.getGetOrPostValueAsString("opp_name");
			int p1side = this.headers.getGetOrPostValueAsInt("p1side");
			int uploadedmapid = this.headers.getGetOrPostValueAsInt("uploadedmapid");
			int start = this.headers.getGetOrPostValueAsInt("start"); // Actually generate the game
			int pcent_creds = this.headers.getGetOrPostValueAsInt("pcent_creds");
			if (pcent_creds == 0) {
				pcent_creds = 100;
			}
			int pcent_opp_pts_rng = this.headers.getGetOrPostValueAsInt("pcent_opp_pts_rng");
			String comment = this.headers.getGetOrPostValueAsString("comment");
			AbstractMission selected_mission = null;
			String msg = "";
			if (next.equalsIgnoreCase("newrequest")) {
				missionid = this.headers.getGetOrPostValueAsInt("mission");
				int opp_type = this.headers.getGetOrPostValueAsInt("opp_type");
				boolean practise = this.headers.getGetOrPostValueAsInt("practise") == 1;
				boolean advanced = this.headers.getGetOrPostValueAsInt("advanced") == 1;
				if (missionid > 0) {
					selected_mission = AbstractMission.Factory(missionid); // Select it for
					if (start > 0 && p1side > 0) { // Have we chosen a side and want to generate the game
						String ret = CreateGame(dbs, current_login, p1side, selected_mission, practise, opp_type, advanced, campaign, pcent_creds, opp_name, pcent_opp_pts_rng, comment, -1, uploadedmapid);
						if (Functions.IsNumeric(ret)) { // Was it successful?
							if (campaign == false) {
								int gameid = Functions.ParseInt(ret);
								this.redirectTo_UsingRefresh("/dsr/BuyArmour.cls?gid=" + gameid, 0); // Show the please wait
								return;
							} else {
								// Do nothing yet until all sides have been selected
							}
						} else {
							msg = ret;
						}
					} else {
						//HTMLFunctions.Para(str, "<span class=\"warning\">Sorry, that mission cannot be played on Android.</span>");
					}
				}
			} else if (next.equalsIgnoreCase("remove")) {
				try {
					int reqid = this.headers.getGetValueAsInt("reqid");
					GameRequestsTable req = new GameRequestsTable(dbs);
					try {
						if (req.doesRowExist(reqid)) {
							req.selectRow(reqid);
							if (req.getPlayerIDByNum(1) == this.current_login.getLoginID()) { // Are they the owner
								// Is there a game that needs deleting?
								int gameid = GameRequestsTable.GetGameID(dbs, reqid);
								if (gameid > 0) {
									//GamesTable.DeleteGame(dbs, gameid);
									GamesTable game = new GamesTable(dbs);
									game.selectRow(gameid);
									game.cancelGame();
									game.close();
								}
								GameRequestsTable.RemoveRequest(dbs, this.current_login.getLoginID(), reqid);
								this.redirectTo_Using303("?");
								return;
							}
						}
					} finally {
						req.close();
					}
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
			}

			if (campaign) {
				title = title + " - Campaign Mode";
			}

			if (msg.length() > 0) {
				HTMLFunctions.Para(str, "<span class=\"warning\">" + msg + "</span>");
			}

			/*if (campaign && this.current_login.isPlayerPlayingInCampaign()) {
				HTMLFunctions.Para(str, "<span class=\"warning\">Sorry, you can only play one campaign mission at a time!</span>");
			} else */if (campaign && this.current_login.canPlayInCampaign() == 0) {
				HTMLFunctions.Para(str, "<span class=\"warning\">Sorry, you must complete at least two normal missions to start a campaign mission.</span>");
			} else {
				HTMLFunctions.Para(str, "Here you create a new game and wait for another player to join, or join a game created by another player.  There is no limit on the number of games that you can play, and feel free to accept any request from any player.");

				HTMLFunctions.Heading(str, 2, "I want to accept a game request from another player...");

				HTMLFunctions.Para(str, "<i>Don't forget that if you only have the free version of the Android client, you can only play The Assassins and Moonbase Assault missions.</i>");

				HTMLFunctions.StartTable(str, "stats", "", 1, "", 5);
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCellHeading(str, "Date");
				HTMLFunctions.AddCellHeading(str, "Mission");
				if (campaign == false) {
					HTMLFunctions.AddCellHeading(str, "Map");
					if (headers.isMobile() == false) {
						HTMLFunctions.AddCellHeading(str, "Creator");
					}
					//HTMLFunctions.AddCellHeading(str, "PC or Android" + helpwindow.GetLink(helpwindow.HC_ANDROID));
					HTMLFunctions.AddCellHeading(str, "Settings");
					HTMLFunctions.AddCellHeading(str, "Waiting For");
				}
				HTMLFunctions.AddCellHeading(str, "Your Side Choice");
				if (campaign == false) {
					HTMLFunctions.AddCellHeading(str, "Credits" + helpwindow.GetLink(helpwindow.HC_PCENT_CREDIT_ADJ));
					//HTMLFunctions.AddCellHeading(str, "% Points Range" + helpwindow.GetLink(helpwindow.HC_PCENT_OPP_PTS_RANGE));
					if (headers.isMobile() == false) {
						HTMLFunctions.AddCellHeading(str, "Points for Win/Lose");
						HTMLFunctions.AddCellHeading(str, "Comment");
					}
				}
				HTMLFunctions.AddCellHeading(str, "Accept");
				HTMLFunctions.EndRow(str);
				boolean any = false;

				int our_points = this.current_login.getELOPoints();
				int games_played = this.current_login.getGamesExperience();

				LoginsTable req_owner = new LoginsTable(dbs);

				GameRequestsTable reqs = new GameRequestsTable(dbs);
				String sql = "SELECT * FROM GameRequests WHERE COALESCE(Accepted, 0) = 0 AND COALESCE(CampGame, 0) = " + SQLFuncs.b201(campaign) + " ORDER BY DateCreated";
				ResultSet rs_gamereqs = dbs.getResultSet(sql);
				while (rs_gamereqs.next()) {
					if (reqs.doesRowExist(rs_gamereqs.getInt("GameRequestID"))) { // In case it has been deleted between selecting the rows and reading them
						reqs.selectRow(rs_gamereqs.getInt("GameRequestID"));

						int pid = rs_gamereqs.getInt("Player1ID");
						req_owner.selectRow(pid);

						AbstractMission this_mission = AbstractMission.Factory(rs_gamereqs.getInt("Mission")); 

						// If it's not our request...
						if (rs_gamereqs.getInt("Player1ID") != this.current_login.getLoginID()) {
							// Don't show locked requests not for us
							if (reqs.isSpaceAvailable() == false && reqs.isPlayerInGame(this.current_login.getID()) == false) {
								continue;
							}

							// Don't show if we're experienced and the game is set for n00bs
							if (reqs.getOpponentType() == GameRequestsTable.OT_NEWBIES && this.current_login.getGamesExperience() > 0) {
								continue;
							}

							// Don't show if we're experienced and the game is set for intermediates
							if (reqs.getOpponentType() == GameRequestsTable.OT_INTERMEDIATE && this.current_login.getGamesExperience() > 70) {
								continue;
							}

							if (reqs.getOpponentType() == GameRequestsTable.OT_NON_INTERMEDIATE && this.current_login.getELOPoints() < 20) {
								continue;
							}

							// Don't show if we have actions and are not experienced enough - NO SINCE THEY MAY HAVE A PRACTISE MISSION!
							/*if (GamesTable.DoesPlayerHaveActions(dbs, this.current_login.getID()) && this.current_login.getGamesExperience() <= 2) {
								continue;
							}*/

							// Don't show if camp mission and req owner is already playing one
							/*if (campaign && reqs.isCampaignGame()) {
								if (req_owner.isPlayerPlayingInCampaign()) {
									continue;
								}
							}*/

							// Don't show faction games
							if (campaign && reqs.isCampaignGame()) {
								//if (this.current_login.getFactionID() <= 0 && reqs.isFactionGame()) {
								continue;
								//}
							}

							// Don't show if we have an opponent points range set
							if (reqs.getOpponentPointsRange() > 0) {
								Point p = req_owner.getMinAndMaxOppRange(reqs.getOpponentPointsRange());
								int min = p.x;
								int max = p.y;
								if (our_points < min || our_points > max) {
									continue;
								}
							}

							// Don't show if campaign and we played them in our last campaign
							/*if (campaign) {
								try {
									sql = "SELECT Player1ID, Player2ID FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED;
									sql = sql + " AND " + AbstractTable.GetPlayerSubQuery(this.current_login.getLoginID());
									sql = sql + " AND CampGame = 1";
									sql = sql + " ORDER BY DateFinished DESC LIMIT 1";
									ResultSet rs_last_game = dbs.getResultSet(sql);
									if (rs_last_game.next()) {
										if (req_owner.getID() == rs_last_game.getInt("Player1ID") || req_owner.getID() == rs_last_game.getInt("Player2ID")) {
											continue;
										}
									}
								} catch (Exception ex) {
									DSRWebServer.HandleError(ex);
								}
							}*/

							// Don't show the game if we're not experienced enough
							if (games_played < this_mission.getGamesReqToPlay() && reqs.isPlayerInGame(this.current_login.getID()) == false && DSRWebServer.DEBUG == false) {
								continue;
							}
						}

						any = true;

						HTMLFunctions.StartForm(str, "Form" + rs_gamereqs.getInt("GameRequestID"), "JoinGame.cls", "POST");
						HTMLFunctions.HiddenValue(str, "reqid", rs_gamereqs.getInt("GameRequestID"));

						HTMLFunctions.StartRow(str);
						HTMLFunctions.AddCell(str, Dates.FormatDate(rs_gamereqs.getTimestamp("DateCreated"), "dd MMM yyyy"));
						HTMLFunctions.AddCell(str, "<nobr><a href=\"missiondescriptions.cls?type=" + this_mission.getType() + "&camp=" + (campaign ? "1" : "0") + "\">" + this_mission.mission_name + "</a></nobr>");

						if (reqs.getUploadedMapID() > 0) {
							HTMLFunctions.AddCell(str, UploadedMapsTable.GetName(dbs, reqs.getUploadedMapID()));
						} else {
							HTMLFunctions.AddCell(str, "Standard");
						}

						//if (this.current_login.isAdmin()) {
						//if (reqs.isPlayerInGame(this.current_login.getID()) || rs_gamereqs.getInt("Player1ID") != this.current_login.getLoginID()) {
						if (!campaign) {
							// Show creator's name IF NOT CAMPAIGN
							if (headers.isMobile() == false) {
								String name = LoginsTable.GetDisplayName_Enc(dbs, rs_gamereqs.getInt("Player1ID"), true);
								/*if (req_owner.getFactionID() > 0) {
								name = name + " (" + FactionsTable.GetName(dbs, req_owner.getFactionID()) + ")";
							}*/
								HTMLFunctions.AddCell(str, name);
							}

							// Settings
							StringBuffer settings = new StringBuffer();
							if (rs_gamereqs.getInt("GameType") == GameRequestsTable.GT_PRACTISE) {
								settings.append("Practise, ");
							}
							if (rs_gamereqs.getInt("CanHearEnemies") == 1) {
								settings.append("Advanced Mode, ");
							}
							if (settings.length() > 0) {
								settings.delete(settings.length()-2, settings.length());
							} else {
								settings.append("Standard");
							}
							HTMLFunctions.AddCell(str, settings.toString());

							// Waiting for
							if (reqs.isPlayerInGame(this.current_login.getID()) && reqs.hasPlayerSelectedASide(this.current_login.getID()) == false) {
								str.append("<td style=\"background-color: #ff0000;\">YOU!</td>");
							} else {
								HTMLFunctions.AddCell(str, reqs.getOpponents(this.current_login.getID()));
							}
						}

						// Side choice
						HTMLFunctions.StartCell(str);
						//if (rs_gamereqs.getInt("Player1ID") != this.current_login.getLoginID()) { // Did we create it?
						if (reqs.hasPlayerSelectedASide(this.current_login.getID()) == false) {
							getSideSelectorComboBox(str, rs_gamereqs, this_mission);
						} else {
							try {
								int side = reqs.getSideFromPlayerID(this.current_login.getID());
								str.append(this_mission.getSideDescription(side)); // reqs.getPlayersSideChoice(side)));
								//str.append("Side " + side + " chosen"); // todo - fix above
							} catch (Exception ex) {
								ex.printStackTrace();
								str.append("Side ? chosen");
							}
						}
						HTMLFunctions.EndCell(str);

						if (campaign == false) {
							// Credits
							HTMLFunctions.AddCell(str, reqs.getPcentCredit() + "%");

							// Points range
							/*if (rs_gamereqs.getInt("Player1ID") == this.current_login.getLoginID()) {
								if (reqs.getOpponentPointsRange() == 0) {
									HTMLFunctions.AddCell(str, "");
								} else {
									HTMLFunctions.AddCell(str, reqs.getOpponentPointsRange() + "%");
								}
							} else {
								HTMLFunctions.AddCell(str, "");
							}*/

							if (headers.isMobile() == false) {
								// Points for win
								if (rs_gamereqs.getInt("Player1ID") == this.current_login.getLoginID()) { // Our game
									HTMLFunctions.AddCell(str, "");
								} else {
									HTMLFunctions.AddCell(str, this.current_login.getPointsForWinAgainst(req_owner) + "/" + this.current_login.getPointsForLoseAgainst(req_owner));
								}
								// Comment
								HTMLFunctions.AddCell(str, HTMLFunctions.s2HTML(rs_gamereqs.getString("Comment")));
							}
						}


						// Accept
						HTMLFunctions.StartCell(str);
						if (rs_gamereqs.getInt("Player1ID") == this.current_login.getLoginID()) { // Did we create it?
							str.append("<a href=\"?campaign=" + SQLFuncs.b201(campaign) + "&next=remove&campaign=" + SQLFuncs.b201(campaign) + "&reqid=" + rs_gamereqs.getInt("GameRequestID") + "\">Remove</a>");
						} else if (reqs.isPlayerInGame(this.current_login.getLoginID()) && reqs.hasPlayerSelectedASide(this.current_login.getLoginID())) {
							str.append("<nobr>Accepted - Waiting for more players</nobr>");
						} else {
							HTMLFunctions.HiddenValue(str, "next", "joingame");
							HTMLFunctions.SubmitButton(str, "Accept Request");
						}
						HTMLFunctions.EndCell(str);
						HTMLFunctions.EndRow(str);

						HTMLFunctions.EndForm(str);
					}
				}
				reqs.close();
				rs_gamereqs.close();

				HTMLFunctions.EndRow(str);
				HTMLFunctions.EndTable(str);

				if (!any) {
					HTMLFunctions.Para(str, "<b>* Sorry, either all the current games have players or you need to complete your actions before you can join another.<br />Instead, please use the form below to create another game for another player to join. *</b>");
				}
				// Add form for creating new request
				if (campaign == false) {
					str.append("<hr />");

					//HTMLFunctions.Heading(str, 3, "Create a New Game:-");
					HTMLFunctions.Heading(str, 2, "I want to create a new game for another player to join...");
					str.append("<a name=\"new_game\"></a>");

					HTMLFunctions.StartForm(str, "FormX", "#new_game", "POST");
					HTMLFunctions.HiddenValue(str, "next", "newrequest");
					HTMLFunctions.HiddenValue(str, "campaign", SQLFuncs.b201(campaign));

					HTMLFunctions.StartTable(str, "", "", 0, "", 0);
					HTMLFunctions.StartRow(str);
					HTMLFunctions.AddCell(str, "Select Mission:");
					HTMLFunctions.StartCell(str);
					getMissionSelectorCombo(str, campaign, games_played);//, this.current_login.hasAndroid());
					//str.append(" (Missions marked with a * can only be played with the Android client)");
					if (selected_mission != null) {
						str.append("&nbsp;" + selected_mission.getShortDesc() + " (<a href=\"/dsr/missiondescriptions.cls?type=" + selected_mission.getMissionID() + "\" target=\"_blank\">more...</a>)");
					}
					HTMLFunctions.EndCell(str);
					HTMLFunctions.EndRow(str);

					if (selected_mission != null) {
						HTMLFunctions.StartRow(str);
						HTMLFunctions.AddCell(str, "My Chosen Side:");
						HTMLFunctions.StartCell(str);
						getNewGameSideSelectorComboBox(str, selected_mission, p1side);
						HTMLFunctions.EndCell(str);
						HTMLFunctions.EndRow(str);

						HTMLFunctions.StartRow(str);
						HTMLFunctions.AddCell(str, "Optional Alternative Map");
						HTMLFunctions.StartCell(str);
						getAltMapSelectorComboBox(str);
						HTMLFunctions.EndCell(str);
						HTMLFunctions.EndRow(str);

						if (!campaign) {
							HTMLFunctions.StartRow(str);
							HTMLFunctions.AddCell(str, "Practise" + helpwindow.GetLink(helpwindow.HC_PRACTISE) + "?:");
							HTMLFunctions.StartCell(str);
							boolean practise = false;//mission.getMissionName(false).endsWith("*") || mission.isCampaignMission();
							HTMLFunctions.CheckBox(str, "practise", "1", practise?true:false);
							//str.append("(Practise games are not included in league table calculations)");
							HTMLFunctions.EndCell(str);
							HTMLFunctions.EndRow(str);
						} else {
							HTMLFunctions.HiddenValue(str, "practise", "0");
						}

						if (!campaign) {
							HTMLFunctions.StartRow(str);
							HTMLFunctions.AddCell(str, "Advanced Mode " + helpwindow.GetLink(helpwindow.HC_ADVANCED_MODE) + ":");
							HTMLFunctions.StartCell(str);
							if (campaign == false && selected_mission.isSnafu() == false && selected_mission.isAlwaysAdvanced() == false) {
								HTMLFunctions.CheckBox(str, "advanced", "1", false);
								//str.append("(Players will be informed of enemy movement and shots within noise range even if they can't see them)");
							} else {
								HTMLFunctions.HiddenValue(str, "advanced", "1");
								str.append("Mission is always Advanced");
							}
							HTMLFunctions.EndCell(str);
							HTMLFunctions.EndRow(str);

							HTMLFunctions.StartRow(str);
							HTMLFunctions.AddCell(str, "<nobr>Optional Opponent(s)" + helpwindow.GetLink(helpwindow.HC_ENTER_OPPONENTS_NAME) + "</nobr>");
							HTMLFunctions.StartCell(str);
							HTMLFunctions.TextBox(str, "opp_name", opp_name, false, 400, 50);
							str.append("(Enter name or email, seperate with a <b>comma</b> if entering multiple)");
							HTMLFunctions.EndCell(str);
							HTMLFunctions.EndRow(str);

						} else {
							HTMLFunctions.HiddenValue(str, "advanced", "1");
							//HTMLFunctions.HiddenValue(str, "android", "0");
						}

						if (!campaign) {
							HTMLFunctions.EndTable(str);
							str.append("<h4 style=\"\"><span class=\"editprofilelink\" onclick=\"javascript:hideshow('advanced_options', 120);\"><a href=\"#new_game\">Click here for Advanced Options</a></span></h4>");
							str.append("<div id=\"advanced_options\" style=\"visibility: hidden; height: 0px;\" >");
							str.append("<fieldset><legend>Advanced Options</legend>");
							HTMLFunctions.StartTable(str, "", "", 0, "", 0);


							HTMLFunctions.StartRow(str);
							HTMLFunctions.AddCell(str, "<nobr>% Credit Adjustment" + helpwindow.GetLink(helpwindow.HC_PCENT_CREDIT_ADJ) + "</nobr>");
							HTMLFunctions.StartCell(str);
							String keys[] = {"50", "100", "150", "200"};
							HTMLFunctions.ComboBox(str, "pcent_creds", "", keys, keys, pcent_creds, false);
							str.append("% (This will adjust the standard credits for both sides)");
							HTMLFunctions.EndCell(str);
							HTMLFunctions.EndRow(str);

							HTMLFunctions.StartRow(str);
							HTMLFunctions.AddCell(str, "<nobr>Opponent Points Range" + helpwindow.GetLink(helpwindow.HC_PCENT_OPP_PTS_RANGE) + "</nobr>");
							HTMLFunctions.StartCell(str);
							String keys2[] = {"0", "300", "200", "100", "75", "50", "25"};
							String vals2[] = new String[keys2.length];
							vals2[0] = "All";
							for (int i=1 ; i<keys2.length ; i++) {
								Point p = this.current_login.getMinAndMaxOppRange(Integer.parseInt(keys2[i]));
								vals2[i] = keys2[i] + "% (" + p.x + "-" + p.y + ")";
							}
							//p = this.current_login.getMinAndMaxOppRange(25);
							//vals2[2] = "25% (" + p.x + "-" + p.y + ")";
							HTMLFunctions.ComboBox(str, "pcent_opp_pts_rng", "", keys2, vals2, pcent_opp_pts_rng, false);
							str.append(" (Only opponents with league points within the range will see this request)");
							HTMLFunctions.EndCell(str);
							HTMLFunctions.EndRow(str);

							HTMLFunctions.StartRow(str);
							HTMLFunctions.AddCell(str, "<nobr>Optional Comment</nobr>");
							HTMLFunctions.StartCell(str);
							HTMLFunctions.TextBox(str, "comment", comment, false, 256, 50);
							//str.append("(Enter name or email, seperate with a <b>comma</b>)");
							HTMLFunctions.EndCell(str);
							HTMLFunctions.EndRow(str);

							HTMLFunctions.EndTable(str);

							str.append("</fieldset>");

							str.append("</div>");

						}

						HTMLFunctions.StartTable(str, "", "", 0, "", 0);

						if (!campaign) { // && this.current_login.isAdmin()) {
							String[] values = {"Any", "New Players Only", "Intermediate", ">20 League Points"};
							HTMLFunctions.StartRow(str);
							HTMLFunctions.AddCell(str, "Opponent Type:");
							HTMLFunctions.StartCell(str);
							HTMLFunctions.ComboBox(str, "opp_type", values, 0);
							HTMLFunctions.EndCell(str);
							HTMLFunctions.EndRow(str);
						}

						HTMLFunctions.StartRow(str);
						HTMLFunctions.AddCell(str, "");
						HTMLFunctions.StartCell(str);
						HTMLFunctions.HiddenValue(str, "start", 0);
						str.append("<input type=\"submit\" value=\"Add Request\" onClick=\"FormX.elements['start'].value=1; return true;\" />");
						HTMLFunctions.EndCell(str);
						HTMLFunctions.EndRow(str);

						HTMLFunctions.StartRow(str);
						HTMLFunctions.AddCell(str, "");
						HTMLFunctions.AddCell(str, 2, "You will be sent an email when someone accepts your game request.");
						HTMLFunctions.EndRow(str);
					}

					HTMLFunctions.EndTable(str);

					HTMLFunctions.EndForm(str);
				}

				if (campaign) {
					title = "Campaign Lobby";
				}
			}
		}

		this.body_html.append(MainLayout.GetHTML(this, title, str));		

	}


	private void getSideSelectorComboBox(StringBuffer str, ResultSet rs, AbstractMission mission) throws SQLException {
		ArrayList<String> keys = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();

		for (int side=1 ; side<=mission.getNumOfSides() ; side++) {
			boolean side_selected = false;			
			for (int player=1 ; player<=mission.getNumOfSides() ; player++) {
				if (rs.getInt("Player" + player + "ID") > 0) { // Is this player in the game
					if (rs.getInt("Player" + player + "SideChoice") == side) {
						side_selected = true;
					}
				}
			}
			if (side_selected == false) {
				keys.add("" + side);
				values.add("Side " + side + ": " + mission.getSideDescription(side));
			}

		}
		HTMLFunctions.ComboBox(str, "player2side", "", keys.toArray(), values.toArray(), 0, false);
	}


	private void getMissionSelectorCombo(StringBuffer str, boolean show_camp, int games_played) throws SQLException {
			str.append("<select name=\"mission\" class=\"combobox\" onChange=\"FormX.submit();\">");
			str.append("<option value=\"0\">[Please select]</option>");

			for (int c=2 ; c<=4 ; c++) { // Group by num players
				str.append("<optgroup label=\"" + c + " Player Missions\">");
				for (int j=0 ; j<AbstractMission.SF_MISSIONS.length ; j++) {
					int i = AbstractMission.SF_MISSIONS[j];
					/*if (i == 91) {
						int dfgdfg = 456456;
					}*/
					try {
						if (AbstractMission.IsValidMission(i)) {
							AbstractMission mission = AbstractMission.Factory(i);
							if (mission.getNumOfSides() == c) {//.getCampaign() == c) {
								// Don't show in selector if we're not experienced enough
								if (games_played < mission.getGamesReqToPlay()) {
									continue;
								}

								if (show_camp) {
									if (mission.isCampaignMission() == false) {
										continue;
									}
									if (mission.getNumOfSides() != 2) { // Can't handle any others
										continue;
									}
								}
								String name = AbstractMission.GetMissionNameFromType(i, false, false);
								str.append("<option value=\"" + i + "\"");
								if (missionid == i) {
									str.append(" selected=\"selected\" ");
								}
								str.append(">" + name + "</option>");
							}
						}
					} catch (RuntimeException ex) {
						ex.printStackTrace();
					}
				}
				str.append("</optgroup>");
			}
			str.append("</select>");
	}


	private void getNewGameSideSelectorComboBox(StringBuffer str, AbstractMission mission, int def) throws SQLException {
		String k[], v[];
		if (mission != null) {
			k = new String[mission.getNumOfSides()+1];
			v = new String[mission.getNumOfSides()+1];
			k[0] = "";
			v[0] = "(Please select)";
			for (int side=1 ; side <= mission.getNumOfSides() ; side++) {
				k[side] = "" + side;
				v[side] = "Side " + side + ": " + mission.getSideDescription(side);
			}
		} else {
			k = new String[0];
			v = new String[0];
		}

		HTMLFunctions.ComboBox(str, "p1side", "combobox", k, v, def, false);
	}


	private void getAltMapSelectorComboBox(StringBuffer str) throws SQLException {
		ResultSet rs = dbs.getResultSet("SELECT -1, '[Use standard map]' FROM UploadedMaps LIMIT 1 UNION SELECT UploadedMapID, Name FROM UploadedMaps");
		HTMLFunctions.ComboBox(str, "uploadedmapid", "", rs, -1, false);
		rs.close();
	}


	private static void SendEmailToNewPlayer(LoginsTable current_login, LoginsTable new_player) throws SQLException {
		new_player.sendEmail("You have been asked to join a game!", "A new game of " + DSRWebServer.TITLE + " has been created by player " + current_login.getDisplayName() + " (" + current_login.getEmail() + ") and they would like you to be their opponent.\n\nIf you would like to join the game, please log into the website at " + DSRWebServer.WEBSITE_URL + " with the following details:-\n\nLogin: " + new_player.getDisplayName() + "\nPassword: " + new_player.getPassword() + "\n\nWe hope to see you there soon!\n\n(If you would not like to join the game and do not wish to receive any more emails, simply reply to this message.)");
	}


	/**
	 * Returns gameid if successful, otherwise a message.
	 * I know I need to change this!
	 * @throws SQLException 
	 * 
	 */
	public static String CreateGame(MySQLConnection dbs, LoginsTable current_login, int p1side, AbstractMission selected_mission, boolean practise, int opp_type, boolean advanced, boolean campaign, int pcent_creds, String opp_name, int pcent_opp_pts_rng, String comment, int ai_side, int uploadedmapid) throws SQLException, IOException {
		String msg = "";
		ArrayList<Integer> opponent_ids = new ArrayList<Integer>();
		boolean data_ok = true;
		if (opp_name.length() > 0) { // Have we chosen an opponent
			String opp_names[] = opp_name.split(",");
			LoginsTable login = new LoginsTable(dbs);
			for (int o=0 ; o<opp_names.length ; o++) {
				opp_names[o] = opp_names[o].trim();
				if (opp_names[o].length() > 0) {
					int opp_id = 0;
					if (opp_names[o].indexOf("@") >= 0) {
						if (RequestedPlayersTable.HasPlayerDeclined(dbs, opp_names[o])) {
							data_ok = false;
							msg = msg + "Opponent '" + HTMLFunctions.s2HTML(opp_names[o]) + "' has previously declined to play.  ";
						}
					} else if (login.selectUserByDisplayName(opp_names[o])) {
						opp_id = login.getID();
						if (opp_id == current_login.getID()) {
							data_ok = false;
							msg = msg + "You cannot select yourself as an opponent.  ";
						} else if (login.isDisabled()) {
							data_ok = false;
							msg = msg + "The selected login has been disabled.  ";
						} else if (opp_id == DSRWebServer.AI_LOGIN_ID) {
							data_ok = false;
							msg = msg + "Please select AI using the AI options.  ";
						}
					} else {
						data_ok = false;
						msg = msg + "Opponent '" + HTMLFunctions.s2HTML(opp_names[o]) + "' not found.  ";
					}
					if (data_ok) {
						if (opp_names[o].indexOf("@") >= 0) { // New player?
							if (login.selectUserByEmailAddress(opp_names[o]) == false) { // Is it not an existing player?
								String displayname = opp_names[o].substring(0,  opp_names[o].indexOf("@")).replaceAll("@", "");
								String res = login.createLogin(System.currentTimeMillis() + "", displayname, opp_names[o], false);
								if (res.length() > 0) { // error
									data_ok = false;
									msg = msg + res + "  ";
									break;
								}
							}
							opp_id = login.getLoginID();
							RequestedPlayersTable.AddRec(dbs, opp_names[o], opp_id, current_login.getID());
						}
						opponent_ids.add(opp_id);
						if (opponent_ids.size() > selected_mission.getNumOfSides()-1) {
							data_ok = false;
							msg = msg + "You have selected too many opponents.  ";
						}

					}
				}
			}
			login.close();
		}
		if (data_ok) {
			int reqid = GameRequestsTable.CreateRequest(dbs, selected_mission.getNumOfSides(), current_login.getLoginID(), selected_mission.getMissionID(), p1side, practise ? GameRequestsTable.GT_PRACTISE : GameRequestsTable.GT_NORMAL, opp_type, advanced, campaign, pcent_creds, comment, pcent_opp_pts_rng, -1, -1, -1, uploadedmapid);

			GameRequestsTable requests = new GameRequestsTable(dbs);
			try {
				requests.selectRow(reqid);

				if (opponent_ids.size() > 0) {
					LoginsTable opp_login = new LoginsTable(dbs);
					for (int o=0 ; o<opponent_ids.size() ; o++) {
						requests.setOpponent(opponent_ids.get(o), o+2);
						opp_login.selectRow(opponent_ids.get(o));
						if (opp_login.getLastLoginDate() == null && RequestedPlayersTable.IsNewPlayer(dbs, opp_login.getID())) {
							SendEmailToNewPlayer(current_login, opp_login);
						} else {
							opp_login.sendMsg(current_login.getLoginID(), "New Game Request", "Hi " + LoginsTable.GetDisplayName(dbs, opp_login.getID()) + ",\n\nThis is an automated message.  A new game of Stellar Forces has been created by " + current_login.getDisplayName() + " with you as the opponent.  Please run the Android client to accept it!");
						}
					}
				}

				if (requests.isCampaignGame() == false) {
					GamesTable game = new GamesTable(dbs);
					int gameid = game.createGame(requests.getNumSides(), 0, 0, 0, 0, requests.getMission(), requests.getType(), requests.isAdvancedMode() || selected_mission.isSnafu(), requests.isCampaignGame(), requests.getPcentCredit(), -1, -1, requests.getUploadedMapID());
					game.setPlayerID(p1side, current_login.getID());

					try {
						// Create undercover agents if req
						for (int s=1 ; s<=game.getNumOfSides() ; s++) {
							if (selected_mission.getUnderCoveragentsForSide(s) > 0) {
								game.setUndercoverAgents(s, selected_mission.getUnderCoveragentsForSide(s));
							}
						}
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex);
					}

					game.refreshData();
					requests.setGameID(gameid);

					// We need to create units for all sides since some missions mess about with the units when the map is created.
					selected_mission.createUnits(dbs, game);
					int mapid = selected_mission.createMapStart(dbs, gameid);
					game.setMapDataID(mapid);

					if (game.isAdvancedMode()) {
						try {
							// Add skills
							for (int s=1 ; s<=game.getNumOfSides() ; s++) {
								UnitsTable unit = new UnitsTable(dbs);
								if (unit.selectRandomUnitForSideWithoutSkill(gameid, s)) {
									unit.setSkillID(UnitsTable.SK_MEDIC);
								}

								if (unit.selectRandomUnitForSideWithoutSkill(gameid, s)) {
									unit.setSkillID(UnitsTable.SK_STEALTH);
								}
								unit.close();
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
					}

					if (ai_side > 0) {
						//game.setPlayerID(ai_side, DSRWebServer.ai_login_id);
						game.setAISide(ai_side);
						LoginsTable ai_login = new LoginsTable(dbs);
						ai_login.selectRow(DSRWebServer.AI_LOGIN_ID);
						requests.acceptRequest(ai_login, game, ai_side);
						ai_login.close();
					}
					return ""+gameid;
				} else {
					// Do nothing yet until all sides have been selected
				}
			} finally {
				requests.close();
			}
		}
		return msg;
	}

}
