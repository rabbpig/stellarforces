package dsrwebserver.pages.dsr;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.html.HTMLFunctions;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;

public class sendreminder extends AbstractHTMLPage {

	public static final int DAYS_BETWEEN_REMINDERS = 4;

	public sendreminder() {
		super();
	}

	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();
		if (this.session.isLoggedIn()) {
			int gid = this.headers.getGetValueAsInt("gid");
			GamesTable game = new GamesTable(dbs);
			game.selectRow(gid);
			if (game.isPlayerInGame(this.current_login.getLoginID())) {
				int days = game.getDaysSinceLastReminder();
				if (days >= sendreminder.DAYS_BETWEEN_REMINDERS || (game.getTurnNo() <= 1 && days>= (sendreminder.DAYS_BETWEEN_REMINDERS/2))) {
					SendReminder(dbs, game);
					HTMLFunctions.Para(str, "A reminder has been sent to your opponent.  Don't forget that you can play as many games as you want, so feel free to start another.");
				} else {
					HTMLFunctions.Para(str, "Please wait at least " + DAYS_BETWEEN_REMINDERS + " days to send a reminder.  The last reminder was sent " + days + " days ago.  Don't forget that you can play as many games as you want, so feel free to start another.");
				}
				HTMLFunctions.Para(str, "[<a href=\"MyGames.cls\">Back</a>]");
				/*} else {
				HTMLFunctions.Para(str, "You are not waiting for your opponent, they are waiting for you!");*/
			}
		}

		this.body_html.append(MainLayout.GetHTML(this, "Send Reminder", str));		

	}


	public static void SendReminder(MySQLConnection dbs, GamesTable game) throws SQLException {
		LoginsTable login = new LoginsTable(dbs);
		int lid = game.getLoginIDOfWaitingForPlayer();
		if (lid > 0) {
			login.selectRow(lid);
			if (login.getOnHolidayText().length() == 0) {
				String to_do = "take your turn";
				if (game.getGameStatus() == GamesTable.GS_CREATED_DEPLOYMENT) {
					to_do = "equip your units";
				}
				//String camp = "";
				/*try {
					if (login.isPlayerPlayingInCampaign()) {
						camp = "Note that you are playing in a campaign mission.  You may need to visit the website to select your squad in order to start the game.";
					}
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}*/
				login.sendEmail("Polite Reminder", "This is an automated email reminder to " + to_do + " in one of your games (including game " + game.getID() + ": " + AbstractMission.GetMissionNameFromType(game.getMissionID(), false, false) + ").  Your opponent is waiting! :) \n\nIf you wish to concede the game or if you are having any problems, just reply to this email with details.  Please don't mark this email as spam!");

				game.updateDaysSinceLastReminder();
				login.updateDateLastReminder();
			}
		}
		login.close();
	}

}
