package dsrwebserver.pages.dsr;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.AbstractTable;
import dsrwebserver.tables.CampUnitsTable;
import dsrwebserver.tables.FactionsTable;
import dsrwebserver.tables.GameRequestsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MessagesTable;
import dsrwebserver.tables.MissionStatsTable;
import dsrwebserver.tables.PlayerTrophiesTable;
import dsrwebserver.tables.StarmapSquaresTable;

/**
 * This class does more than just check for trophies.  It is designed to do all regular automated tasks.
 *
 */
public class checkfortrophies extends AbstractHTMLPage {

	private static final int AUTO_CANCEL_DAYS = 28;

	private static Calendar last_run_time;


	public checkfortrophies() {
		super();

	}


	@Override
	public void process() throws Exception {
		if (last_run_time == null) {
			last_run_time = Calendar.getInstance();
		} else {
			Calendar c = Calendar.getInstance();
			c.add(Calendar.HOUR_OF_DAY, -1);
			if (c.before(last_run_time)) {
				return;
			}
		}

		last_run_time = Calendar.getInstance();

		// Cleardown tables
		try {
			DSRWebServer.dbs.runSQLDelete("DELETE FROM Emails where DATEDIFF(now(), DateCreated) > 7 AND Sent = 1");
			DSRWebServer.dbs.runSQLDelete("OPTIMIZE TABLE Emails");
			DSRWebServer.dbs.runSQLDelete("DELETE FROM Sessions where DATEDIFF(now(), DateCreated) > 14");
			DSRWebServer.dbs.runSQLDelete("OPTIMIZE TABLE Sessions");
			DSRWebServer.dbs.runSQLDelete("DELETE FROM WebsiteEvents where DATEDIFF(now(), DateCreated) > 14");
			DSRWebServer.dbs.runSQLDelete("OPTIMIZE TABLE WebsiteEvents");
				DSRWebServer.dbs.runSQLDelete("DELETE FROM StarmapLog where DATEDIFF(now(), DateCreated) > 14");
				DSRWebServer.dbs.runSQLDelete("OPTIMIZE TABLE StarmapLog");
				DSRWebServer.dbs.runSQLDelete("DELETE FROM CampaignLog where DATEDIFF(now(), DateCreated) > 14");
				DSRWebServer.dbs.runSQLDelete("OPTIMIZE TABLE CampaignLog");
			DSRWebServer.dbs.runSQLDelete("DELETE FROM LoginLog where DATEDIFF(now(), DateCreated) > 14");
			DSRWebServer.dbs.runSQLDelete("OPTIMIZE TABLE LoginLog");
			//DSRWebServer.dbs.runSQLDelete("DELETE FROM GameRequests WHERE DATEDIFF(now(), DateCreated) > 28"); // Delete old game requests

		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

		StringBuffer str = new StringBuffer();
		LoginsTable login = new LoginsTable(dbs);
		ResultSet rs = null;

		// Check trophies
		rs = dbs.getResultSet("SELECT LoginID FROM Logins WHERE DATEDIFF(Now(), LastLoginDate) < 30");//LastLoginDate >= '2011-11-01'"); here

		while (rs.next()) {
			login.selectRow(rs.getInt("LoginID"));
			if (PlayerTrophiesTable.DoesPlayerHaveTrophy(dbs, rs.getInt("LoginID"), PlayerTrophiesTable.LONG_SERVICE_TROPHY) == false) {
				int c = login.getGamesExperience();
				if (c >= 100) {
					PlayerTrophiesTable.AddTrophy(dbs, rs.getInt("LoginID"), PlayerTrophiesTable.LONG_SERVICE_TROPHY);
					MessagesTable.SendMsg(dbs, -1, login.getID(), "You have won a trophy!", "Congratulations " + login.getDisplayName() + "!  You have won the " + PlayerTrophiesTable.GetTrophyName(PlayerTrophiesTable.LONG_SERVICE_TROPHY) + " for completing 100 missions.");
					break; // only create one trophy at a time!
				}
			}
			if (PlayerTrophiesTable.DoesPlayerHaveTrophy(dbs, rs.getInt("LoginID"), PlayerTrophiesTable.LASER_SQUAD_TROPHY) == false) {
				if (hasPlayerWonMissionAsSide(rs.getInt("LoginID"), AbstractMission.THE_ASSASSINS, 1)) {
					if (hasPlayerWonMissionAsSide(rs.getInt("LoginID"), AbstractMission.THE_ASSASSINS, 2)) {
						if (hasPlayerWonMissionAsSide(rs.getInt("LoginID"), AbstractMission.MOONBASE_ASSAULT, 1)) {
							if (hasPlayerWonMissionAsSide(rs.getInt("LoginID"), AbstractMission.MOONBASE_ASSAULT, 2)) {
								if (hasPlayerWonMissionAsSide(rs.getInt("LoginID"), AbstractMission.RESCUE_FROM_THE_MINES, 1)) {
									if (hasPlayerWonMissionAsSide(rs.getInt("LoginID"), AbstractMission.RESCUE_FROM_THE_MINES, 2)) {
										PlayerTrophiesTable.AddTrophy(dbs, rs.getInt("LoginID"), PlayerTrophiesTable.LASER_SQUAD_TROPHY);
										MessagesTable.SendMsg(dbs, -1, login.getID(), "You have won a trophy!", "Congratulations " + login.getDisplayName() + "!  You have won the " + PlayerTrophiesTable.GetTrophyName(PlayerTrophiesTable.LASER_SQUAD_TROPHY) + " for winning all 3 Laser Squad missions in Advanced Mode as both sides.");
										break; // only create one trophy at a time!
									}
								}
							}
						}
					}
				}
			}
			if (PlayerTrophiesTable.DoesPlayerHaveTrophy(dbs, rs.getInt("LoginID"), PlayerTrophiesTable.FIRST_BLOOD_TROPHY) == false) {
				if (login.getTotalVictories(LoginsTable.GameType.ONLY_NON_PRACTISE) > 0) {
					PlayerTrophiesTable.AddTrophy(dbs, rs.getInt("LoginID"), PlayerTrophiesTable.FIRST_BLOOD_TROPHY);
					MessagesTable.SendMsg(dbs, -1, login.getID(), "You have won a trophy!", "Congratulations " + login.getDisplayName() + "!  You have won the " + PlayerTrophiesTable.GetTrophyName(PlayerTrophiesTable.FIRST_BLOOD_TROPHY) + " for winning your first mission.");
					break; // only create one trophy at a time!
				}
			}
			if (PlayerTrophiesTable.DoesPlayerHaveTrophy(dbs, rs.getInt("LoginID"), PlayerTrophiesTable.WINNERS_TROPHY) == false) {
				if (login.getTotalVictories(LoginsTable.GameType.ONLY_NON_PRACTISE) >= 50) {
					PlayerTrophiesTable.AddTrophy(dbs, rs.getInt("LoginID"), PlayerTrophiesTable.WINNERS_TROPHY);
					MessagesTable.SendMsg(dbs, -1, login.getID(), "You have won a trophy!", "Congratulations " + login.getDisplayName() + "!  You have won the " + PlayerTrophiesTable.GetTrophyName(PlayerTrophiesTable.WINNERS_TROPHY) + " for winning 50 missions.");
					break; // only create one trophy at a time!
				}
			}
			if (PlayerTrophiesTable.DoesPlayerHaveTrophy(dbs, rs.getInt("LoginID"), PlayerTrophiesTable.LOSERS_TROPHY) == false) {
				int c = login.getTotalDefeats(LoginsTable.GameType.ONLY_NON_PRACTISE);
				if (c >= 40) {
					PlayerTrophiesTable.AddTrophy(dbs, rs.getInt("LoginID"), PlayerTrophiesTable.LOSERS_TROPHY);
					MessagesTable.SendMsg(dbs, -1, login.getID(), "You have won a trophy!", "Congratulations " + login.getDisplayName() + "!  You have won the " + PlayerTrophiesTable.GetTrophyName(PlayerTrophiesTable.LOSERS_TROPHY) + " for losing 40 games!");
					break; // only create one trophy at a time!
				}
			}
		}
		rs.close();

		// Check for best player of each mission
		// Do this before updating each login's faction score!
		MissionStatsTable.ClearStats(dbs);
		rs = dbs.getResultSet("SELECT LoginID FROM Logins WHERE DATEDIFF(Now(), LastLoginDate) < 30");
		while (rs.next()) {
			login.selectRow(rs.getInt("LoginID"));
			if (login.getTotalGamesFinished() > 0) {
				for (int missionid=1 ; missionid<AbstractMission.SF_MISSIONS.length ; missionid++) {
					//DSRWebServer.p("Checking stats for mission " + missionid + " / login " + login.getID());
					int max_v = login.getTotalVictoriesByMission(false, leaguetable.DAYS, missionid);
					MissionStatsTable.SetPlayer(dbs, missionid, login.getID(), max_v);
				}
			}
		}
		rs.close();

		// Check for unaccepted starmap game requests
		//if (DSRWebServer.CAMP_ON) {
		rs = dbs.getResultSet("SELECT GameRequestID, DATEDIFF(CurDate(), DateCreated) AS Diff FROM GameRequests WHERE CampGame = 1 AND COALESCE(Accepted, 0) = 0 AND AttackingFactionID > 0");
		GameRequestsTable reqs = new GameRequestsTable(dbs);
		while (rs.next()) {
			reqs.selectRow(rs.getInt("GameRequestID"));
			if (rs.getInt("Diff") > DSRWebServer.STARMAP_DEFEND_DAYS-1) { // rs.getInt("GameRequestID")
				// Send a warning
				int old_facid = StarmapSquaresTable.GetFactionID(dbs, reqs.getStarmapX(), reqs.getStarmapY());
				//todo - re-add FactionsTable.EmailFactionMembers(dbs, old_facid, "Sector Warning!", "Sector " + reqs.getStarmapX() + "-" + reqs.getStarmapY() + " will be lost to the " + FactionsTable.GetName(dbs, reqs.getAttackingFactionID()) + " unless it is defended soon!", -1);
			}
		}
		reqs.close();
		rs.close();
		//}

		// Update faction points
		ResultSet rs_factions = dbs.getResultSet("SELECT * FROM Factions");
		while (rs_factions.next()) {
			int sectors = dbs.getScalarAsInt("SELECT Count(*) FROM StarmapSquares WHERE FactionID = " + rs_factions.getInt("FactionID"));
			FactionsTable.SetSectors(dbs, rs_factions.getInt("FactionID"), sectors);
		}
		rs_factions.close();

		// Cleardown unaccepted game requests
		GamesTable games = new GamesTable(dbs);
		String sql = "SELECT * FROM GameRequests WHERE Accepted IS NULL AND DATEDIFF(now(), DateCreated) > 28";
		rs = dbs.getResultSet(sql);
		while (rs.next()) {
			reqs.selectRow(rs.getInt("GameRequestID"));
			if (games.doesRowExist(reqs.getGameID())) {
				games.selectRow(reqs.getGameID());
				if (games.getGameStatus() == GamesTable.GS_FINISHED) { // If game finished, remove the request! 
					reqs.delete(rs.getInt("GameRequestID"));
				} else if (reqs.isWaitingForSpecificPlayerToJoin()) { // Waiting too long for specific player to join, so remove
					games.cancelGame();
					reqs.delete(rs.getInt("GameRequestID"));
				}
			}
		}
		games.close();
		rs.close();


		// Send reminders / cancel games
		sql = "SELECT * FROM Games WHERE GameStatus <> " + GamesTable.GS_FINISHED + " ORDER BY DateCreated";
		ResultSet rs_games = dbs.getResultSet(sql);
		GamesTable game = new GamesTable(dbs);
		while (rs_games.next()) {
			game.selectRow(rs_games.getInt("GameID"));
			int days = game.getDaysSinceTurnStarted();
			if (game.getMissionID() != AbstractMission.SF_PRACTISE_MISSION && game.getMissionID() != AbstractMission.SF_PRACTISE_MISSION_WITH_AI) {
				// Note that games may not be practise, but may have an AI opponent!
				if (days >= 2 && days < 10) {
					int lid = game.getWaitingForLoginID();
					if (lid > 0) {
						login.selectRow(lid);
						// Check they haven't had a reminder for at least 2 days
						if (login.getDaysSinceLastReminder() >= 3) {
							try {
								// Check it's not against AI
								int opp_id = game.getOppositeSidesLoginIDs(lid)[1];
								if (opp_id == DSRWebServer.AI_LOGIN_ID) {
									continue;
								}
							} catch (Exception ex) {
								ex.printStackTrace();
							}
							sendreminder.SendReminder(dbs, game);
						}
					}
				} else if (days > AUTO_CANCEL_DAYS) {
					if (game.getGameStatus() <= GamesTable.GS_STARTED) {
						if (game.haveAllPlayersJoined()) {
							try {
								int loginid = game.getLoginIDOfWaitingForPlayer();
								if (loginid > 0) {// && loginid != 230) { // Give PCasaca benefit of doubt!
									try {
										login.selectRow(loginid);
										if (login.getOnHolidayText().length() == 0) {
											cancelgame.ForceConcedeGame(dbs, game, loginid);
											//DSRWebServer.SendEmailToAdmin("Game force cancelled", "Game " + rs_games.getInt("GameID") + " force cancelled.");
										}
									} catch (Exception ex) {
										DSRWebServer.HandleError(ex);
									}
								}
							} catch (Exception ex) {
								DSRWebServer.HandleError(ex, "Game " + game.getID(), false);
							}
						}
					}
				}
			} else {
				if (days >= 3) {
					game.setWinType(GamesTable.WIN_DRAW_MUTUAL_CONCEDE, -1);
				}
			}
		}

		// Refresh camp units - 1 per day
		rs = dbs.getResultSet("SELECT LoginID FROM Logins WHERE FactionID > 0");
		while (rs.next()) {
			login.selectRow(rs.getInt("LoginID"));
			int days_since_last = login.getDaysSinceLastLastCampUnitReplenishment(); 
			if (days_since_last > 0) {
				int max_units = CampUnitsTable.GetMaxCampUnits(dbs, login);
				CampUnitsTable camp_unit = new CampUnitsTable(dbs);
				for (int d = 0 ; d< days_since_last ; d++) { // Give one unit per day since last
					if (CampUnitsTable.GetNumCampaignUnits(dbs, login.getID(), false) < max_units) {
						camp_unit.createUnit(login.getID());
						login.setDateLastCampUnitReplenishment();
					} else {
						break;
					}
				}
				camp_unit.close();
			}
		}
		rs.close();

		// Check for open games
		try {
			if (DSRWebServer.SS_LOGIN_ID > 0) {
				if (dbs.getScalarAsInt("SELECT COUNT(*) FROM GameRequests WHERE Mission = 2 AND Player1ID = " + DSRWebServer.SS_LOGIN_ID + " AND Player2ID IS NULL") == 0) {
					AbstractMission mission = AbstractMission.Factory(AbstractMission.MOONBASE_ASSAULT);
					login.selectRow(DSRWebServer.SS_LOGIN_ID);
					GameRequests.CreateGame(dbs, login, 2, mission, true, GameRequestsTable.OT_NEWBIES, false, false, 0, "", 0, "", -1, -1);
				}
			}
			/*if (DSRWebServer.JEZ_LOGIN_ID > 0) {
				if (dbs.getScalarAsInt("SELECT COUNT(*) FROM GameRequests WHERE Mission = 1 AND Player1ID = " + DSRWebServer.JEZ_LOGIN_ID + " AND Player2ID IS NULL") == 0) {
					AbstractMission mission = AbstractMission.Factory(AbstractMission.THE_ASSASSINS);
					login.selectRow(DSRWebServer.JEZ_LOGIN_ID);
					GameRequests.CreateGame(dbs, login, 2, mission, true, GameRequestsTable.OT_NEWBIES, false, false, 0, "", 0, "", -1);
				}
				if (dbs.getScalarAsInt("SELECT COUNT(*) FROM GameRequests WHERE Mission = 2 AND Player1ID = " + DSRWebServer.JEZ_LOGIN_ID + " AND Player2ID IS NULL") == 0) {
					AbstractMission mission = AbstractMission.Factory(AbstractMission.MOONBASE_ASSAULT);
					login.selectRow(DSRWebServer.JEZ_LOGIN_ID);
					GameRequests.CreateGame(dbs, login, 2, mission, true, GameRequestsTable.OT_NEWBIES, false, false, 0, "", 0, "", -1);
				}
			}*/
			//if (DSRWebServer.TROLLY_LOGIN_ID > 0) {
			if (dbs.getScalarAsInt("SELECT COUNT(*) FROM GameRequests WHERE Mission = 2 AND Player1ID = " + DSRWebServer.SS_LOGIN_ID + " AND Player2ID IS NULL") == 0) {
				AbstractMission mission = AbstractMission.Factory(AbstractMission.MOONBASE_ASSAULT);
				login.selectRow(DSRWebServer.SS_LOGIN_ID);
				GameRequests.CreateGame(dbs, login, 2, mission, true, GameRequestsTable.OT_NEWBIES, false, false, 0, "", 0, "", -1, -1);
			}
			if (dbs.getScalarAsInt("SELECT COUNT(*) FROM GameRequests WHERE Mission = 1 AND Player1ID = " + DSRWebServer.SS_LOGIN_ID + " AND Player2ID IS NULL") == 0) {
				AbstractMission mission = AbstractMission.Factory(AbstractMission.THE_ASSASSINS);
				login.selectRow(DSRWebServer.SS_LOGIN_ID);
				GameRequests.CreateGame(dbs, login, 2, mission, true, GameRequestsTable.OT_NEWBIES, false, false, 0, "", 0, "", -1, -1);
			}
			//}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

		// Remove players from factions who haven't logged in for a while
		dbs.runSQLUpdate("UPDATE Logins SET FactionID = NULL WHERE FactionID > 0 AND DATEDIFF(NOW(), LastLoginDate) > " + FactionsTable.EXPIRY_DAYS);

		// Remove players from factions who haven't played a campaign game for a while
		/*try { //TODO - RE-ADD THIS BUT DON'T REMOVE THEM STRAIGHT AWAY
			rs = dbs.getResultSet("SELECT LoginID FROM Logins WHERE FactionID > 0");
			while (rs.next()) {
				login.selectRow(rs.getInt("LoginID"));
				if (FactionsTable.GetNumSectors(dbs, login.getFactionID()) > 0) { // Only remove if they have sectors to prevent rats leaving a sinking ship
					sql = "SELECT DATEDIFF(NOW(), DateFinished) AS X FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED;
					sql = sql + " AND " + AbstractTable.GetPlayerSubQuery(login.getID());
					sql = sql + " AND CampGame = 1";
					sql = sql + " ORDER BY DateFinished DESC LIMIT 1";
					ResultSet rs_last_game = dbs.getResultSet(sql);
					if (rs_last_game.next()) {
						//if (login.getDisplayName().equalsIgnoreCase("free palestine.") || login.getDisplayName().toLowerCase().startsWith("digi")) {
						//	DSRWebServer.SendEmailToAdmin("Faction removal", login.getDisplayName() + "=" + rs_last_game.getInt("X"));
						//}
						if (rs_last_game.getInt("X") > FactionsTable.EXPIRY_DAYS) {
							login.setFactionID(-1);
							//HTMLFunctions.Para(str, "Removing " + login.getDisplayName());
							DSRWebServer.p("Removing " + login.getDisplayName());
							DSRWebServer.SendEmailToAdmin("Player removed from faction", login.getDisplayName());
						}
					} else {
						login.setFactionID(-1);
						DSRWebServer.SendEmailToAdmin("Player removed from faction", login.getDisplayName());
					}
				}
			}
			rs.close();
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}*/


		this.body_html.append(MainLayout.GetHTML(this, "", str));

	}


	private boolean hasPlayerWonMissionAsSide(int loginid, int mission, int side) throws SQLException {
		StringBuffer sql = new StringBuffer("SELECT Count(*) FROM Games WHERE Mission = " + mission + " AND CanHearEnemies = 1 AND ");
		sql.append("Player" + side + "ID = " + loginid + " AND WinningSide = " + side);// + ") OR ");
		return dbs.getScalarAsInt(sql.toString()) > 0;
	}

}
