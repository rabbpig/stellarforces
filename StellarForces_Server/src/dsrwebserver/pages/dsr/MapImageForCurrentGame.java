package dsrwebserver.pages.dsr;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.image.CustomJPEG;
import ssmith.util.MyList;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitsTable;
import dsrwebserver.tables.VisibleEnemiesTable;

public final class MapImageForCurrentGame extends MapImageAbstract {

	private Color col_wall = Color.green.darker();
	private Color col_smoke = Color.gray;
	private Color col_nerve_gas = Color.green;

	public MapImageForCurrentGame() {
		super();

		addHTTPHeader("Cache-Control: no-cache");
		addHTTPHeader("Pragma: no-cache");
	}


	public void process() throws Exception {
		if (this.session.isLoggedIn() == false) {
			// See if we're using the android client
			String login = this.headers.getGetValueAsString("android_login");
			String pwd = this.headers.getGetValueAsString("android_pwd");

			if (this.current_login.selectUser(login, pwd) == false) {
				return;
			}
		}

		this.content_type = "image/jpeg";

		int gameid = conn.headers.getGetValueAsInt("gid");
		int key = conn.headers.getGetValueAsInt("key");

		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);
		if (game.isPlayerInGame(this.current_login.getLoginID())) {
			AbstractMission mission = AbstractMission.Factory(game.getMissionID());

			int our_side = game.getSideFromPlayerID(current_login.getLoginID());
			mid = MapDataTable.GetMapIDFromGameID(dbs, game.getID());
			Dimension d = MapDataTable.GetMapSize(dbs, mid);
			int smallest_side = Math.min(d.width, d.height) ;
			sq_size = SIZE/smallest_side;

			int img_width = sq_size * d.width;
			int img_height = sq_size * d.height;

			CustomJPEG jpg = new CustomJPEG(img_width, img_height);
			Graphics g = jpg.getGraphics();
			g.setColor(Color.black);
			g.fillRect(0, 0, img_width-1, img_height-1);

			if (mission.thin_thick_walls == AbstractMapModel.BLOCK_WALLS) {
				drawWallEdges(g);
			}

			// Draw floor
			String sql = "SELECT * FROM MapDataSquares WHERE MapDataID = " + mid + " And SquareType = " + MapDataTable.MT_FLOOR;
			ResultSet rs = dbs.getResultSet(sql);
			while (rs.next()) {
				super.setFloorColour(g, rs.getInt("FloorTex"), mission);

				// Show deployment area
				if (game.getGameStatus() < GamesTable.GS_STARTED)  {
					if (rs.getInt("DeploymentSquareSide") > 0) {
						g.setColor(GetColourForSide(rs.getInt("DeploymentSquareSide")));
					}
				}
				if (rs.getInt("EscapeHatch") > 0) {//== our_side) {
					g.setColor(ESCAPE_HATCH_COL);
				}
				g.fillRect(rs.getInt("MapX") * sq_size, rs.getInt("MapY") * sq_size, sq_size, sq_size);

				// Draw doors
				if (rs.getInt("DoorType") > 0) {
					g.setColor(Color.green);
					if (rs.getInt("DoorType") == MapDataTable.DOOR_EW) {
						g.fillRect((rs.getInt("MapX") * sq_size), (rs.getInt("MapY") * sq_size) + (sq_size/3), sq_size, sq_size/3);
					} else if (rs.getInt("DoorType") == MapDataTable.DOOR_NS) {
						g.fillRect((rs.getInt("MapX") * sq_size) + (sq_size/3), rs.getInt("MapY") * sq_size, sq_size/3, sq_size);
					} else {
						throw new RuntimeException("Unknown door type: " + rs.getInt("DoorType"));
					}
					// Blacken if closed  -NO as it gives away position!
					/*if (rs.getInt("DoorOpen") == 1) {
							g.setColor(Color.black);
							int INSET = 3;
							if (rs.getInt("DoorType") == MapDataTable.DOOR_EW) {
								g.fillRect((rs.getInt("MapX") * sq_size)+INSET, (rs.getInt("MapY") * sq_size) + (sq_size/3), sq_size-(INSET*2), sq_size/3);
							} else if (rs.getInt("DoorType") == MapDataTable.DOOR_NS) {
								g.fillRect((rs.getInt("MapX") * sq_size) + (sq_size/3), (rs.getInt("MapY") * sq_size)+INSET, sq_size/3, sq_size- (INSET*2));
							} else {
								throw new RuntimeException("Unknown door type: " + rs.getInt("DoorType"));
							}
						}*/
				}
			}

			// Draw walls
			if (mission.thin_thick_walls == AbstractMapModel.SLIM_WALLS) {
				drawWalls(g);
			}				

			if (mission.isSnafu() == false) {
				this.drawComputers(g, our_side);
			}

			drawFlagOrEggs(g, game);

			drawExplosions(g, game, game.getTurnNo());

			if (mission.canBuildAndDismantle() == 0) { // Too many corpses in these missions!
				if (game.isAdvancedMode()) {// && game.getNumOfSides() > 2) {
					drawDeadUnitsFromEquipment(g, game, our_side, mission); // Only draw seen corpses
					drawDeadUnitsFromCorpses(g, game, our_side, mission, true); // Draw our corpses WITH NUMBERS!
				} else {
					drawDeadUnitsFromCorpses(g, game, our_side, mission, false);
				}
			}
			drawHeardUnits(g, game, our_side, mission);
			drawEnemyUnits(g, game, our_side, mission);
			drawOurUnits(g, game, our_side, mission);

			if (key != 0) {
				// Draw key
				for (int s=1 ; s<=game.getNumOfSides() ; s++) {
					g.setColor(GetColourForSide(s));
					g.drawString(game.getNameFromSide(s), 10, 10 + (s*20));
				}
			}
			
			data = jpg.generateDataAsJPEG();

			this.content_length = data.length;
		} else {
			this.redirectTo_Using303("/FileNotFoundPage.cls");
		}
	}



	private void drawWalls(Graphics g) throws SQLException {
		// Draw the floor first
		String sql = "SELECT MapX, MapY FROM MapDataSquares WHERE MapDataID = " + mid + " And SquareType = " + MapDataTable.MT_WALL;
		ResultSet rs = dbs.getResultSet(sql);
		g.setColor(col_wall);
		while (rs.next()) {
			g.fillRect((rs.getInt("MapX") * sq_size), (rs.getInt("MapY") * sq_size), sq_size, sq_size);

		}
		rs.close();

		// Draw smoke
		sql = "SELECT MapX, MapY, SmokeType FROM MapDataSquares WHERE MapDataID = " + mid + " And SmokeType > 0";
		rs = dbs.getResultSet(sql);
		while (rs.next()) {
			if (rs.getInt("SmokeType")  == EquipmentTypesTable.ET_SMOKE_GRENADE) {
				g.setColor(col_smoke);
			} else {
				g.setColor(col_nerve_gas);
			}
			g.drawRect((rs.getInt("MapX") * sq_size), (rs.getInt("MapY") * sq_size), sq_size-1, sq_size-1);

		}
		rs.close();
	}


	private void drawOurUnits(Graphics g, GamesTable game, int our_side, AbstractMission mission) throws SQLException {
		// Select our units then visible enemy units
		MyList<Integer> ml_sides = mission.getSidesForSide(our_side);
		String sql = "SELECT Units.UnitID, Units.Side, Units.Status, Units.MapX, Units.MapY, Units.OrderBy, " + game.getPhaseNo() + " AS PhaseNo, Units.Angle, " + VisibleEnemiesTable.VT_SEEN + " AS VisibleType FROM Units ";
		sql = sql + " WHERE GameID = " + game.getID() + " AND Status = " + UnitsTable.ST_DEPLOYED;
		sql = sql + " AND Side IN (" + ml_sides.toCSVString() + ")";

		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) {
			g.setColor(GetColourForSide(rs.getInt("Side")));
			g.fillOval(rs.getInt("MapX") * sq_size, rs.getInt("MapY") * sq_size, sq_size, sq_size);

			// Draw direction
			g.setColor(Color.yellow);
			int sx = rs.getInt("MapX") * sq_size + (sq_size/2);
			int sy = rs.getInt("MapY") * sq_size + (sq_size/2);
			g.drawLine(sx, sy, sx+GetXOffSetFromAngle(rs.getInt("Angle"), sq_size), sy+GetYOffSetFromAngle(rs.getInt("Angle"), sq_size));

			// Draw black border to numbers
			g.setColor(Color.black);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 2, (rs.getInt("MapY") * sq_size) + 12);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 4, (rs.getInt("MapY") * sq_size) + 12);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 3, (rs.getInt("MapY") * sq_size) + 11);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 3, (rs.getInt("MapY") * sq_size) + 13);

			// Draw number
			g.setColor(Color.white);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 3, (rs.getInt("MapY") * sq_size) + 12);
		}

	}


	private void drawEnemyUnits(Graphics g, GamesTable game, int our_side, AbstractMission mission) throws SQLException {
		// Select our units then visible enemy units
		String sql = " SELECT Units.UnitID, Units.Side, Units.Status, VisibleEnemies.MapX, VisibleEnemies.MapY, Units.OrderBy, VisibleEnemies.PhaseNo, VisibleEnemies.Angle, VisibleEnemies.VisibleType FROM VisibleEnemies ";
		sql = sql + " INNER JOIN Units ON Units.UnitID = VisibleEnemies.UnitID ";
		sql = sql + " WHERE VisibleEnemies.GameID = " + game.getID();
		sql = sql + " AND Units.Status = " + UnitsTable.ST_DEPLOYED;
		sql = sql + " AND VisibleEnemies.PhaseNo >= " + (game.getPhaseNo()-game.getNumOfSides());
		sql = sql + " AND VisibleEnemies.SeenBySide IN (" + mission.getSidesForSide(our_side).toCSVString() + ")";
		sql = sql + " AND VisibleEnemies.VisibleType = " + VisibleEnemiesTable.VT_SEEN;
		sql = sql + " AND Units.Side NOT IN (" + mission.getSidesForSide(our_side).toCSVString() + ")";
		sql = sql + " ORDER BY Side, OrderBy";

		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) {
			g.setColor(GetColourForSide(rs.getInt("Side")));
			g.fillOval(rs.getInt("MapX") * sq_size, rs.getInt("MapY") * sq_size, sq_size, sq_size);

			// Draw direction
			g.setColor(Color.yellow);
			int sx = rs.getInt("MapX") * sq_size + (sq_size/2);
			int sy = rs.getInt("MapY") * sq_size + (sq_size/2);
			g.drawLine(sx, sy, sx+GetXOffSetFromAngle(rs.getInt("Angle"), sq_size), sy+GetYOffSetFromAngle(rs.getInt("Angle"), sq_size));

			// Draw black border to numbers
			g.setColor(Color.black);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 2, (rs.getInt("MapY") * sq_size) + 12);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 4, (rs.getInt("MapY") * sq_size) + 12);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 3, (rs.getInt("MapY") * sq_size) + 11);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 3, (rs.getInt("MapY") * sq_size) + 13);

			// Draw number
			g.setColor(Color.white);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 3, (rs.getInt("MapY") * sq_size) + 12);
		}

	}


	private void drawDeadUnitsFromCorpses(Graphics g, GamesTable game, int our_side, AbstractMission mission, boolean only_our_side) throws SQLException {
		String sql = "SELECT Units.UnitID, Units.Side, Units.Status, Units.MapX, Units.MapY, Units.OrderBy, " + game.getPhaseNo() + " AS PhaseNo, Units.Angle, " + VisibleEnemiesTable.VT_SEEN + " AS VisibleType FROM Units ";
		sql = sql + " WHERE GameID = " + game.getID() + " AND Status = " + UnitsTable.ST_DEAD;
		if (only_our_side) {
			sql = sql + " AND Side IN (" + mission.getSidesForSide(our_side).toCSVString() + ")";
		}
		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) {
			g.setColor(GetColourForSide(rs.getInt("Side")));
			g.drawOval(rs.getInt("MapX") * sq_size, rs.getInt("MapY") * sq_size, sq_size, sq_size);

			// Draw black border to numbers
			g.setColor(Color.black);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 2, (rs.getInt("MapY") * sq_size) + 12);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 4, (rs.getInt("MapY") * sq_size) + 12);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 3, (rs.getInt("MapY") * sq_size) + 11);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 3, (rs.getInt("MapY") * sq_size) + 13);

			// Draw number
			g.setColor(Color.white);
			g.drawString("" + rs.getInt("OrderBy"), (rs.getInt("MapX") * sq_size) + 3, (rs.getInt("MapY") * sq_size) + 12);
		}
	}


	private void drawDeadUnitsFromEquipment(Graphics g, GamesTable game, int our_side, AbstractMission mission) throws SQLException {
		String sql = "SELECT MapX, MapY, MajorTypeID FROM Equipment ";
		sql = sql + " INNER JOIN EquipmentTypes ON EquipmentTypes.EquipmentTypeID = Equipment.EquipmentTypeID ";
		sql = sql + " WHERE GameID = " + game.getID() + " AND SeenBySide" + our_side + " > 0";
		sql = sql + " AND EquipmentTypes.MajorTypeID IN (14, 15, 16, 17, 18, 19)";

		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) {
			g.setColor(Color.white);
			g.drawOval(rs.getInt("MapX") * sq_size, rs.getInt("MapY") * sq_size, sq_size, sq_size);
		}
	}


	private void drawHeardUnits(Graphics g, GamesTable game, int our_side, AbstractMission mission) throws SQLException {
		String sql = "SELECT Units.UnitID, Units.Side, Units.Status, VisibleEnemies.MapX, VisibleEnemies.MapY, Units.OrderBy, VisibleEnemies.PhaseNo, VisibleEnemies.Angle, VisibleEnemies.VisibleType FROM VisibleEnemies ";
		sql = sql + " INNER JOIN Units ON Units.UnitID = VisibleEnemies.UnitID ";
		sql = sql + " WHERE VisibleEnemies.GameID = " + game.getID();
		sql = sql + " AND VisibleEnemies.PhaseNo >= " + (game.getPhaseNo()-game.getNumOfSides());
		sql = sql + " AND VisibleEnemies.SeenBySide IN (" + mission.getSidesForSide(our_side).toCSVString() + ")";
		sql = sql + " AND VisibleEnemies.VisibleType <> " + VisibleEnemiesTable.VT_SEEN; // Only heard!
		sql = sql + " AND Units.Side NOT IN (" + mission.getSidesForSide(our_side).toCSVString() + ")";
		sql = sql + " ORDER BY Status DESC, Side, OrderBy";

		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) {
			if (rs.getInt("VisibleType") == VisibleEnemiesTable.VT_HEARD_MOVING) {
				g.setColor(Color.gray); // Only heard so only 
			} else {
				g.setColor(Color.white); // Heard shooting 
			}
			g.fillOval(rs.getInt("MapX") * sq_size, rs.getInt("MapY") * sq_size, sq_size, sq_size);

			// Draw direction
			g.setColor(Color.yellow);
			int sx = rs.getInt("MapX") * sq_size + (sq_size/2);
			int sy = rs.getInt("MapY") * sq_size + (sq_size/2);
			g.drawLine(sx, sy, sx+GetXOffSetFromAngle(rs.getInt("Angle"), sq_size), sy+GetYOffSetFromAngle(rs.getInt("Angle"), sq_size));

		}

	}


}
