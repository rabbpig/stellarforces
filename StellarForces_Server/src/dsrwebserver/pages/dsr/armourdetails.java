package dsrwebserver.pages.dsr;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.html.HTMLFunctions;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.ArmourTypesTable;

public final class armourdetails extends AbstractHTMLPage {

	public armourdetails() {
		super();
	}

	@Override
	public void process() throws Exception {
			String full_req = super.disk_cache.convertPath(super.conn.headers.request.getFullURL());
			StringBuffer str = new StringBuffer();
			if (super.disk_cache.contains(full_req) == false) {
				HTMLFunctions.Para(str, "Armour worn by units reduces the amount of damage the receive from shots and explosions.  The table below gives details; the amount that damage is reduced by is determined by the type of armour and also the angle at which the attack occured.  Armour is most protective if the unit is facing the attack.");

				GetArmourStatsTable(dbs, str);

				super.disk_cache.put(full_req, str.toString());
			} else {
				str.append(super.disk_cache.get(full_req));
			}
			body_html.append(MainLayout.GetHTML(this, "Armour Data Sheet", str));
	}


	public static void GetArmourStatsTable(MySQLConnection dbs, StringBuffer str) throws SQLException {
		HTMLFunctions.StartTable(str, "stats", "", 0, "", 5);
		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCellHeading(str, "hed", "Armour<br />Name");
		HTMLFunctions.AddCellHeading(str, "hed", "Front<br />Protec");
		HTMLFunctions.AddCellHeading(str, "hed", "Side<br />Protec");
		HTMLFunctions.AddCellHeading(str, "hed", "Rear<br />Protec");
		HTMLFunctions.AddCellHeading(str, "hed", "Weight");
		//HTMLFunctions.AddCellHeading(str, "hed", "Restricted" + helpwindow.GetLink(helpwindow.HC_RESTRICTED));
		HTMLFunctions.AddCellHeading(str, "hed", "Cost");
		HTMLFunctions.EndRow(str);

		ResultSet rs = dbs.getResultSet("SELECT ArmourTypeID FROM ArmourTypes");// ORDER BY OrderBy");
		while (rs.next()) {
			showArmour(dbs, str, rs.getInt("ArmourTypeID"));
		}
		HTMLFunctions.EndTable(str);

	}

	private static void showArmour(MySQLConnection dbs, StringBuffer str, int id) throws SQLException {
		HTMLFunctions.StartRow(str);

		HTMLFunctions.AddCell(str, ArmourTypesTable.GetName(dbs, id));
		HTMLFunctions.AddCell(str, "" + ArmourTypesTable.GetProtection(dbs, id));
		HTMLFunctions.AddCell(str, "" + ArmourTypesTable.GetProtection(dbs, id) * .5f);
		HTMLFunctions.AddCell(str, "" + ArmourTypesTable.GetProtection(dbs, id) * .25f);
		HTMLFunctions.AddCell(str, "" + ArmourTypesTable.GetWeight(dbs, id));
		//HTMLFunctions.AddCell(str, ArmourTypesTable.IsRestricted(dbs, id) ? "Yes" : "");
		HTMLFunctions.AddCell(str, "" + ArmourTypesTable.GetCost(dbs, id));

		HTMLFunctions.EndRow(str);

	}



}


