package dsrwebserver.pages.dsr;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.html.HTMLFunctions;
import dsr.AppletMain;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.EquipmentTypesTable;

public final class equipmentdetails extends AbstractHTMLPage {

	public equipmentdetails() {
		super();
	}

	@Override
	public void process() throws Exception {
		String full_req = super.disk_cache.convertPath(super.conn.headers.request.getFullURL());
		StringBuffer str = new StringBuffer();
		if (super.disk_cache.contains(full_req) == false) {
			int eid = this.headers.getGetValueAsInt("eid");

			EquipmentTypesTable equipment = new EquipmentTypesTable(dbs);
			String sql_app = "";
			if (eid > 0) {
				sql_app = " AND EquipmentTypeID = " + eid + " ";
			}
			ResultSet rs_mt = dbs.getResultSet("SELECT Distinct MajorTypeID FROM EquipmentTypes WHERE ShowStats = 1 ORDER BY MajorTypeID");
			while (rs_mt.next()) {
				ResultSet rs = dbs.getResultSet("SELECT EquipmentTypeID, ShowStats FROM EquipmentTypes WHERE MajorTypeID = " + rs_mt.getInt("MajorTypeID") + sql_app + " AND CanBuy = 1 ORDER BY Cost DESC");
				HTMLFunctions.StartTable(str, "stats", "", 1, "", 5);
				boolean shown_heading = false;
				while (rs.next()) {
					if (shown_heading == false) {
						shown_heading = true;
						HTMLFunctions.Heading(str, 2, EquipmentTypesTable.GetMajorTypeFromID((byte)rs_mt.getInt("MajorTypeID")));
						this.addHeading(str, (byte)rs_mt.getInt("MajorTypeID"));
						if (rs_mt.getInt("MajorTypeID") == EquipmentTypesTable.ET_GUN) {
							HTMLFunctions.Para(str, "Note that non-laser guns lose 1pt of damage for every two squares <i>after</i> the first " + AppletMain.MIN_DAMAGE_REDUCTION_RANGE + " squares.");
						}
					}
					equipment.selectRow(rs.getInt("EquipmentTypeID"));
					this.showEquipment(str, equipment);
				}
				HTMLFunctions.EndTable(str);
			}
			super.disk_cache.put(full_req, str.toString());
		} else {
			str.append(super.disk_cache.get(full_req));
		}


		body_html.append(MainLayout.GetHTML(this, "Weapons & Equipment Data Sheet", str));

	}


	private void addHeading(StringBuffer str, byte type) {
		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCellHeading(str, "Name");
		switch (type) {
		case EquipmentTypesTable.ET_GUN:
			HTMLFunctions.AddCellHeading(str, "hed", "Aimed Shot Acc");
			HTMLFunctions.AddCellHeading(str, "hed", "Aimed Shot APs");
			HTMLFunctions.AddCellHeading(str, "Snap Shot Acc");
			HTMLFunctions.AddCellHeading(str, "Snap Shot APs");
			HTMLFunctions.AddCellHeading(str, "Auto Shot Acc");
			HTMLFunctions.AddCellHeading(str, "Auto Shot APs");
			HTMLFunctions.AddCellHeading(str, "hed", "Shot Damage");
			HTMLFunctions.AddCellHeading(str, "hed", "Explosion Radius");
			HTMLFunctions.AddCellHeading(str, "hed", "Explosion Damage");
			HTMLFunctions.AddCellHeading(str, "hed", "Reload APs");
			HTMLFunctions.AddCellHeading(str, "hed", "Ammo Type");
			HTMLFunctions.AddCellHeading(str, "hed", "Ammo Capacity");
			HTMLFunctions.AddCellHeading(str, "hed", "Noise Range");
			break;
		case EquipmentTypesTable.ET_CC_WEAPON:
			HTMLFunctions.AddCellHeading(str, "hed", "Accuracy");
			HTMLFunctions.AddCellHeading(str, "hed", "Damage");
			break;
		case EquipmentTypesTable.ET_GRENADE:
			HTMLFunctions.AddCellHeading(str, "hed", "Explosion Radius");
			HTMLFunctions.AddCellHeading(str, "hed", "Explosion Damage");
			break;
		case EquipmentTypesTable.ET_MEDIKIT:
			HTMLFunctions.AddCellHeading(str, "hed", "Health Redeemed");
			break;
		case EquipmentTypesTable.ET_AMMO_CLIP:
			//HTMLFunctions.AddCellHeading(str, "hed", "Health Redeemed");
			break;
		case EquipmentTypesTable.ET_PORTA_PORTER_TRIGGER:
			//HTMLFunctions.AddCellHeading(str, "hed", "Health Redeemed");
			break;
		case EquipmentTypesTable.ET_PORTA_PORTER_LANDER:
			//HTMLFunctions.AddCellHeading(str, "hed", "Health Redeemed");
			break;
		case EquipmentTypesTable.ET_DEATH_GRENADE:
			HTMLFunctions.AddCellHeading(str, "hed", "Explosion Radius");
			HTMLFunctions.AddCellHeading(str, "hed", "Explosion Damage");
			break;
		case EquipmentTypesTable.ET_ADRENALIN_SHOT:
			//HTMLFunctions.AddCellHeading(str, "hed", "Health Redeemed");
			break;
		case EquipmentTypesTable.ET_SMOKE_GRENADE:
			HTMLFunctions.AddCellHeading(str, "hed", "Explosion Radius");
			break;
		case EquipmentTypesTable.ET_NERVE_GAS:
			HTMLFunctions.AddCellHeading(str, "hed", "Explosion Radius");
			HTMLFunctions.AddCellHeading(str, "hed", "Harm");
			break;
		case EquipmentTypesTable.ET_SCANNER:
			break;
		case EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE:
			HTMLFunctions.AddCellHeading(str, "hed", "Explosion Radius");
			break;
		default:
			throw new RuntimeException("Unknown equipment type: " + type);
		}
		HTMLFunctions.AddCellHeading(str, "hed", "Weight");
		HTMLFunctions.AddCellHeading(str, "hed", "Description");
		//HTMLFunctions.AddCellHeading(str, "hed", "Min Client Version");
		//HTMLFunctions.AddCellHeading(str, "hed", "Restricted" + helpwindow.GetLink(helpwindow.HC_RESTRICTED));
		HTMLFunctions.AddCellHeading(str, "hed", "Cost");
		HTMLFunctions.EndRow(str);

	}

	private void showEquipment(StringBuffer str, EquipmentTypesTable equipment) throws SQLException {
		//HTMLFunctions.Heading(str, 2, equipment.getName());

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, equipment.getName());

		switch (equipment.getMajorType()) {
		case EquipmentTypesTable.ET_GUN:
			HTMLFunctions.AddCell(str, "" + equipment.getAimedShotAcc() + "%");
			HTMLFunctions.AddCell(str, "" + equipment.getAimedShotAps());
			HTMLFunctions.AddCell(str, "" + equipment.getSnapShotAcc() + "%");
			HTMLFunctions.AddCell(str, "" + equipment.getSnapShotAps());
			HTMLFunctions.AddCell(str, "" + equipment.getAutoShotAcc() + "%");
			HTMLFunctions.AddCell(str, "" + equipment.getAutoShotAps());
			HTMLFunctions.AddCell(str, "" + equipment.getShotDamage());
			HTMLFunctions.AddCell(str, "" + equipment.getExplosionRadius());
			HTMLFunctions.AddCell(str, "" + equipment.getExplosionDamage());
			HTMLFunctions.AddCell(str, "" + equipment.getReloadAPs());

			if (equipment.getAmmoTypeID() > 0) {
				EquipmentTypesTable ammo_type = new EquipmentTypesTable(dbs);
				ammo_type.selectAmmoByAmmoType(equipment.getAmmoTypeID());
				HTMLFunctions.AddCell(str, "" + ammo_type.getName());
				ammo_type.close();
			} else {
				HTMLFunctions.AddCell(str, "None");
			}
			HTMLFunctions.AddCell(str, "" + equipment.getAmmoCapacity());
			HTMLFunctions.AddCell(str, "" + equipment.getNoiseRange());
			break;
		case EquipmentTypesTable.ET_CC_WEAPON:
			HTMLFunctions.AddCell(str, "" + equipment.getCCAccuracy());
			HTMLFunctions.AddCell(str, "" + equipment.getCCDamage());
			break;
		case EquipmentTypesTable.ET_GRENADE:
			HTMLFunctions.AddCell(str, "" + equipment.getExplosionRadius());
			HTMLFunctions.AddCell(str, "" + equipment.getExplosionDamage());
			break;
		case EquipmentTypesTable.ET_MEDIKIT:
			HTMLFunctions.AddCell(str, "" + equipment.getShotDamage());
			break;
		case EquipmentTypesTable.ET_AMMO_CLIP:
			break;
		case EquipmentTypesTable.ET_PORTA_PORTER_TRIGGER:
			break;
		case EquipmentTypesTable.ET_PORTA_PORTER_LANDER:
			break;
		case EquipmentTypesTable.ET_DEATH_GRENADE:
			HTMLFunctions.AddCell(str, "" + equipment.getExplosionRadius());
			HTMLFunctions.AddCell(str, "" + equipment.getExplosionDamage());
			break;
		case EquipmentTypesTable.ET_ADRENALIN_SHOT:
			break;
		case EquipmentTypesTable.ET_SMOKE_GRENADE:
			HTMLFunctions.AddCell(str, "" + equipment.getExplosionRadius());
			break;
		case EquipmentTypesTable.ET_NERVE_GAS:
			HTMLFunctions.AddCell(str, "" + equipment.getExplosionRadius());
			HTMLFunctions.AddCell(str, "Unit APs remaining");
			break;
		case EquipmentTypesTable.ET_SCANNER:
			break;
		case EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE:
			HTMLFunctions.AddCell(str, "" + equipment.getExplosionRadius());
			break;
		default:
			throw new RuntimeException("Unknown equipment type: " + equipment.getMajorType());
		}

		HTMLFunctions.AddCell(str, "" + equipment.getWeight());
		HTMLFunctions.AddCell(str, "" + equipment.getDesc());
		//HTMLFunctions.AddCell(str, "" + equipment.getMinClientVersion());
		//HTMLFunctions.AddCell(str, "<b>" + (equipment.isRestricted()? "Yes" : "") + "</b>");
		HTMLFunctions.AddCell(str, "<b>" + equipment.getCost() + "</b>");

		HTMLFunctions.EndRow(str);

	}

}


