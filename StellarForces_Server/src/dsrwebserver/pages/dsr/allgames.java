package dsrwebserver.pages.dsr;

import java.sql.ResultSet;

import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MapDataTable;

public class allgames extends AbstractHTMLPage {

	public allgames() {
		super();
	}

	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		String title = "";

		//boolean current = this.headers.getGetValueAsString("finished").equalsIgnoreCase("true") == false;
		if (this.session.isLoggedIn()) {
			if (this.current_login.isAdmin()) {
				int del = this.headers.getGetValueAsInt("del");
				if (del > 0) {
					dbs.runSQLDelete("DELETE FROM Games WHERE GameID = " + del);
					DSRWebServer.SendEmailToAdmin("Game " + del + " Deleted", "Game " + del + " Deleted");
					
				}
				int cancel = this.headers.getGetValueAsInt("cancel");
				if (cancel > 0) {
					GamesTable games = new GamesTable(dbs);
					games.selectRow(cancel);
					games.cancelGame();
					games.close();
				}
				/*int takeover = this.headers.getGetValueAsInt("takeover");
				if (takeover > 0) {
					dbs.runSQLUpdate("UPDATE Games SET Player1ID = " + DSRWebServer.ADMIN_LOGIN_ID + " WHERE Player1ID = " + takeover + " AND Sides > 1 AND GameStatus < " + GamesTable.GS_FINISHED);
					dbs.runSQLUpdate("UPDATE Games SET Player2ID = " + DSRWebServer.ADMIN_LOGIN_ID + " WHERE Player2ID = " + takeover + " AND Sides > 1 AND GameStatus < " + GamesTable.GS_FINISHED);
				}*/
				int backtoequip = this.headers.getGetValueAsInt("backtoequip");
				if (backtoequip > 0) {
					dbs.runSQLUpdate("UPDATE Games SET Player1Equipped = NULL, Player2Equipped = NULL, Player3Equipped = NULL, Player4Equipped = NULL, GameStatus = " + GamesTable.GS_CREATED_DEPLOYMENT +" WHERE GameID = " + backtoequip);
				}
			}
		}

		String sql = "";
		//if (current) {
		title = "All Current Games";
		HTMLFunctions.Para(str, "All games currently being played:");//  <a href=\"?finished=true\">Show completed games</a>.");
		sql = "SELECT * FROM Games WHERE GameStatus <> " + GamesTable.GS_FINISHED + " ORDER BY DateCreated";
		/*} else {
			title = "All Completed Games";
			HTMLFunctions.Para(str, "All completed games.  <a href=\"?\">Show current games</a>.");
			sql = "SELECT * FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED + " ORDER BY DateCreated";
		}*/

		ResultSet rs = dbs.getResultSet(sql);
		GamesTable game = new GamesTable(dbs);
		HTMLFunctions.StartTable(str, "stats", "", 1, "", 5);
		HTMLFunctions.StartRow(str);

		HTMLFunctions.AddCellHeading(str, "Number");
		HTMLFunctions.AddCellHeading(str, "Date Started");
		HTMLFunctions.AddCellHeading(str, "Mission");
		HTMLFunctions.AddCellHeading(str, "Players");
		//if (current) {
		HTMLFunctions.AddCellHeading(str, "Turn (Max)");
		HTMLFunctions.AddCellHeading(str, "Waiting On");
		/*} else {
			HTMLFunctions.AddCellHeading(str, "Winner");
			HTMLFunctions.AddCellHeading(str, "Duration");
		}*/
		if (this.session.isLoggedIn()) {
			if (this.current_login.isAdmin()) {
				HTMLFunctions.AddCellHeading(str, "Days Waiting");
				HTMLFunctions.AddCellHeading(str, "Game ID");
				HTMLFunctions.AddCellHeading(str, "Options");
			}
		}
		HTMLFunctions.EndRow(str);

		int num=0;
		while (rs.next()) {
			num++;
			game.selectRow(rs.getInt("GameID"));

			if (AbstractMission.IsValidMission(game.getMissionID()) || game.getMissionID() == AbstractMission.SF_PRACTISE_MISSION || game.getMissionID() == AbstractMission.SF_PRACTISE_MISSION_WITH_AI) {
				AbstractMission mission = AbstractMission.Factory(game.getMissionID());

				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, num + "(" + rs.getInt("GameID") + ")");
				HTMLFunctions.AddCell(str, Dates.FormatDate(rs.getTimestamp("DateCreated"), Dates.UKDATE_FORMAT));
				HTMLFunctions.AddCell(str, "<nobr>" + mission.getMissionName(true, game.isCampGame()) + (game.isCampGame() ? " (campaign)":"") + "</nobr>");
				if (game.haveAllPlayersJoined()) {
					HTMLFunctions.AddCell(str, game.getOpponentsNamesBySide(-1, true));
				} else {
					HTMLFunctions.AddCell(str, "Game not started yet");
				}
				HTMLFunctions.AddCell(str, "<nobr>" + game.getTurnNo() + " (" + mission.getMaxTurns() + ")</nobr>");

				int waiting_for_side = game.getWaitingForSide();
				if (waiting_for_side > 0) {
					int loginid = game.getLoginIDOfWaitingForPlayer();
					if (loginid > 0) {
						String waiting_for_name = LoginsTable.GetDisplayName_Enc(dbs, loginid, true);
						/*if (this.session.isLoggedIn() && this.current_login.isAdmin()) {
							/*if (game.getGameStatus() == GamesTable.GS_STARTED) {
								int days_since_turn_started = game.getDaysSinceTurnStarted();
								if (days_since_turn_started >= 12) {
									tmp = tmp + " [<a href=\"?takeover=" + loginid + "\" rel=\"nofollow\">Takeover</a>]";
								}
							}
						}*/
						HTMLFunctions.AddCell(str, waiting_for_name);
					} else {
						HTMLFunctions.AddCell(str, "ERROR!");
					}
				} else {
					HTMLFunctions.AddCell(str, "Waiting for Players");
				}

				if (this.session.isLoggedIn()) {
					if (this.current_login.isAdmin()) {
						StringBuffer days = new StringBuffer();
						int days_since_turn_started = game.getDaysSinceTurnStarted();
						days.append(days_since_turn_started);

						//int days_since_reminder_sent = game.getDaysSinceLastReminder();
						/*if (days_since_reminder_sent >= sendreminder.DAYS_BETWEEN_REMINDERS) {
							days.append(" (<a href=\"sendreminder.cls?gid=" + rs.getInt("GameID") + "\" rel=\"nofollow\">send reminder</a>)");
						}*/
						HTMLFunctions.AddCell(str, days.toString());
						HTMLFunctions.AddCell(str, "<a href=\"/dsr/admin/gameadmin.cls?gid=" + game.getID() + "\">" + game.getID() + "</a> (<a href=\"?cancel=" + game.getID() + "\" rel=\"nofollow\">Cancel</a> | <a href=\"?del=" + game.getID() + "\" rel=\"nofollow\">Delete</a>)");
						String notes = "<a href=\"?backtoequip=" + game.getID() + "\" rel=\"nofollow\">* Back to Equip</a>";
						if (MapDataTable.IsFullyCreated(dbs, game.getMapDataID()) == false) { // Don't show if map not created yet as there will be no units for the player to equip
							notes = notes + "  * Map Not Created!";
						}
						HTMLFunctions.AddCell(str, notes);
					}
				}
				HTMLFunctions.EndRow(str);
			}
		}
		HTMLFunctions.EndTable(str);
		game.close();
		this.body_html.append(MainLayout.GetHTML(this, title, str));		

	}

}