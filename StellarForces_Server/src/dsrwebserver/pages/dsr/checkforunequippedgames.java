package dsrwebserver.pages.dsr;

import java.sql.ResultSet;

import dsrwebserver.DSRWebServer;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public class checkforunequippedgames extends AbstractHTMLPage {

	public checkforunequippedgames() {
		super();

	}


	@Override
	public void process() throws Exception {
		// Check for unequipped games
		try {
			StringBuffer str = new StringBuffer();
			GamesTable game = new GamesTable(dbs);
			String sql = "SELECT GameID FROM Games WHERE GameStatus = " + GamesTable.GS_STARTED;
			ResultSet rs_games = dbs.getResultSet(sql);
			while (rs_games.next()) {
				game.selectRow(rs_games.getInt("GameID"));
				if (game.getNumOfSides() > 1 && game.getTurnNo() <= 2) {
					for (int s=1 ; s<=game.getNumOfSides() ; s++) {
						// Are they human?
						sql = "SELECT ModelType FROM Units WHERE GameID = " + game.getID() + " AND Side = " + s + " AND Status = " + UnitsTable.ST_DEPLOYED;
						ResultSet rs = dbs.getResultSet(sql);
						if (rs.next()) {
							int type = rs.getInt("ModelType");
							if (type == UnitsTable.MT_MALE_SIDE_1 || type == UnitsTable.MT_MALE_SIDE_2 ||type == UnitsTable.MT_MALE_SIDE_3 || type == UnitsTable.MT_MALE_SIDE_4 || type == UnitsTable.MT_SCIENTIST) {
								sql = "SELECT Count(*) FROM Equipment INNER JOIN Units ON Units.UnitID = Equipment.UnitID";
								sql = sql + " WHERE Equipment.GameID = " + game.getID() + " AND Units.GameID = " + game.getID() + " AND Units.Side = " + s;
								if (dbs.getScalarAsInt(sql) == 0) {
									str.append("Game " + game.getID() + " side " + s + " has no equipment.\n");
								}
							}
						}
					}
				}
				game.close();
			}
			if (str.length() > 0) {
				DSRWebServer.SendEmailToAdmin("No equipment", str.toString());
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

	}
	
}
