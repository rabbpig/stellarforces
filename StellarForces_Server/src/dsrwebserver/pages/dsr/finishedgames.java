package dsrwebserver.pages.dsr;

import java.sql.ResultSet;

import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import ssmith.util.MyList;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.AbstractTable;
import dsrwebserver.tables.GameRequestsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;

public class finishedgames extends AbstractHTMLPage {

	private static final int RESULTS_PER_PAGE = 50;

	private static final String REMATCH_SAME_SIDES = "rematch_same";
	private static final String REMATCH_OPP_SIDES = "rematch_op";

	public finishedgames() {
		super();
	}

	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		if (this.session.isLoggedIn()) {
			String next = this.headers.getGetValueAsString("next");
			if (next.equalsIgnoreCase(REMATCH_SAME_SIDES) || next.equalsIgnoreCase(REMATCH_OPP_SIDES)) {
				int gameid = this.headers.getGetValueAsInt("gameid");
				GamesTable game = new GamesTable(dbs);
				game.selectRow(gameid);
				int our_side = game.getSideFromPlayerID(this.current_login.getID());
				int opp_side = game.getOppositeSidesForSide(our_side)[0];
				if (next.equalsIgnoreCase(REMATCH_SAME_SIDES)) {
					GameRequests.CreateGame(dbs, this.current_login, our_side, AbstractMission.Factory(game.getMissionID()), game.isPractise(), GameRequestsTable.OT_ANYONE, game.isAdvancedMode(), false, game.getPcentCredit(), game.getOpponentsNamesBySide(our_side, false), 0, "Rematch!", -1, game.getUploadedMapID());
				} else if (next.equalsIgnoreCase(REMATCH_OPP_SIDES)) {
					GameRequests.CreateGame(dbs, this.current_login, opp_side, AbstractMission.Factory(game.getMissionID()), game.isPractise(), GameRequestsTable.OT_ANYONE, game.isAdvancedMode(), false, game.getPcentCredit(), game.getOpponentsNamesBySide(our_side, false), 0, "Rematch!", -1, game.getUploadedMapID());
				}
				this.redirectTo_Using303("MyGames.cls");
				game.close();
				return;
			}

			int opponent = headers.getGetValueAsInt("opponent");

			if (opponent <= 0) {
				HTMLFunctions.Para(str, "This page shows the details of all your finished games.  The most recent are at the top.");
			} else {
				HTMLFunctions.Para(str, "This page shows the details of all your finished games that also included player " + LoginsTable.GetDisplayName(dbs, opponent) + ".  The most recent are at the top.");
			}

			int from = headers.getGetValueAsInt("from");
			if (from < 0) {
				from = 0;
			}
			int to = from + RESULTS_PER_PAGE;

			showNextPrev(str, from, opponent);

			String sql = "SELECT GameID FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED + " AND " + AbstractTable.GetPlayerSubQuery(this.current_login.getLoginID());
			if (opponent > 0) {
				sql = sql + " AND " + AbstractTable.GetPlayerSubQuery(opponent);
			}
			sql = sql + " ORDER BY DateFinished DESC";// LIMIT 50";
			ResultSet rs = dbs.getResultSet(sql);

			GamesTable game = new GamesTable(dbs);
			boolean any = false;
			HTMLFunctions.StartTable(str, "stats", "", 1, "", 5);
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCellHeading(str, "Game");
			HTMLFunctions.AddCellHeading(str, "Mission");
			HTMLFunctions.AddCellHeading(str, "Type");
			HTMLFunctions.AddCellHeading(str, "Your Side");
			HTMLFunctions.AddCellHeading(str, "Opponent");
			//HTMLFunctions.AddCellHeading(str, "Your VPs");
			//HTMLFunctions.AddCellHeading(str, "Opp VPs");
			HTMLFunctions.AddCellHeading(str, "Details");
			HTMLFunctions.AddCellHeading(str, "Won/Lost");
			HTMLFunctions.AddCellHeading(str, "Win Type");
			HTMLFunctions.AddCellHeading(str, "Points");
			HTMLFunctions.AddCellHeading(str, "Running Tot");
			HTMLFunctions.AddCellHeading(str, "Duration");
			HTMLFunctions.AddCellHeading(str, "Rematch?");
			HTMLFunctions.EndRow(str);

			MyList<Integer> winners = null;
			int num = 0;
			while (rs.next()) {
				num++;
				if (num < from) {
					continue;
				} else if (num > to) {
					break;
				}
				game.selectRow(rs.getInt("GameID"));
				try {
					if (AbstractMission.IsValidMission(game.getMissionID())) {
						any = true;

						AbstractMission mission = AbstractMission.Factory(game.getMissionID());
						int our_side = game.getSideFromPlayerID(this.current_login.getLoginID());

						HTMLFunctions.StartRow(str);
						HTMLFunctions.AddCell(str, "" + rs.getInt("GameID"));
						HTMLFunctions.AddCell(str, mission.getMissionName(true, game.isCampGame()));

						HTMLFunctions.AddCell(str, game.getSettings());
						HTMLFunctions.AddCell(str, mission.getSideName(dbs, game, our_side) + " (side " + our_side + ")");

						HTMLFunctions.AddCell(str, game.getOpponentsNamesBySide(our_side, true));
						//HTMLFunctions.AddCell(str, "" + game.getVPsBySide(our_side));
						//HTMLFunctions.AddCell(str, "" + game.getVPs(GamesTable.GetOppositeSide(our_side)));
						HTMLFunctions.AddCell(str, "<a href=\"viewfinishedgame.cls?gid=" + game.getID() + "\">Details</a>");
						if (game.getWinType() == GamesTable.WIN_DRAW || game.getWinType() == GamesTable.WIN_DRAW_MUTUAL_CONCEDE) {
							HTMLFunctions.AddCell(str, "Draw");
						} else if (game.getWinType() == GamesTable.WIN_CANCELLED) {
							HTMLFunctions.AddCell(str, "Cancelled");
						} else if (game.getWinningSides(winners).contains(our_side)) {
							HTMLFunctions.AddCell(str, "WON!");//<br /><nobr>(" + game.getPointsForWin(our_side) + " pts)");
						} else {
							HTMLFunctions.AddCell(str, "LOST");
						}
						HTMLFunctions.AddCell(str, game.getWinTypeAsText());

						//Points
						if (game.isPractise() == false) {
							HTMLFunctions.AddCell(str, game.getELOPointsForSide(our_side));
							HTMLFunctions.AddCell(str, game.getELORunningTotalForSide(our_side));
						} else {
							HTMLFunctions.AddCell(str, "n/a");
							HTMLFunctions.AddCell(str, "n/a");
						}

						// Duration
						HTMLFunctions.AddCell(str, Dates.FormatDate(game.getDateStarted(), Dates.UKDATE_FORMAT) + " - " + Dates.FormatDate(game.getDateFinished(), Dates.UKDATE_FORMAT));

						if (game.getNumOfSides() == 2) {
							HTMLFunctions.AddCell(str, "<a href=\"?next=" + REMATCH_SAME_SIDES + "&gameid=" + game.getID() + "\">Same sides</a><br /><a href=\"?next=" + REMATCH_OPP_SIDES + "&gameid=" + game.getID() + "\">Opposite sides</a>");
						} else {
							HTMLFunctions.AddCell(str, "");

						}
						HTMLFunctions.EndRow(str);
					}
				} catch (RuntimeException ex) {
					DSRWebServer.HandleError(ex);
				}
			}
			HTMLFunctions.EndTable(str);

			showNextPrev(str, from, opponent);

			if (any == false) {
				HTMLFunctions.Para(str, "You don't currently have any finished games.");
			}

		}
		this.body_html.append(MainLayout.GetHTML(this, "Your Finished Games", str));		
	}


	private void showNextPrev(StringBuffer str, int from, int opponent) {
		str.append("<p><a href=\"?from=" + (from-RESULTS_PER_PAGE) + "&opponent=" + opponent + "\">Prev</a> | <a href=\"?from=" + (from+RESULTS_PER_PAGE) + "&opponent=" + opponent + "\">Next</a></p>");
	}

}
