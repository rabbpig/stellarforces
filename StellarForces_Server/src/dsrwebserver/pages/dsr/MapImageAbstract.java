package dsrwebserver.pages.dsr;

import java.awt.Color;
import java.awt.Graphics;
import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import dsr.TextureStateCache;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public abstract class MapImageAbstract extends AbstractDSRPage {

	public static final Color ESCAPE_HATCH_COL = Color.white;
	public static final Color OUR_COMPUTER_COL = new Color(0x6de1b7);
	public static final Color OPP_COMPUTER_COL = new Color(0x899a64);
	public static final Color OUR_DESTROYED_COMPUTER_COL = OUR_COMPUTER_COL.darker().darker();
	public static final Color OPP_DESTROYED_COMPUTER_COL = OPP_COMPUTER_COL.darker().darker();
	public static final Color FLAG_COL = Color.orange.darker();
	public static Color GREEN_FLOOR = new Color(0, 150, 0);
	private static final Color P4_COLOUR = new Color(255, 145, 145);
	public static Color SMOKE = new Color(150, 150, 150);
	public static Color NERVE_GAS = new Color(50, 250, 50);
	public static Color FIRE = new Color(200, 30, 30);

	protected static int SIZE = 850;
	protected static final int BORDER_SIZE = 3;

	//protected CustomJPEG jpg;
	protected byte[] data;
	protected int sq_size;
	protected int mid;


	public MapImageAbstract() {
		super();
		
	}


	protected void writeContent() throws IOException {
		if (data != null) {
			DataOutputStream dos = conn.getDataOutputStream();
			dos.write(data); //jpg.generateDataAsJPEG());
			dos.flush();
		}
	}


	public static Color GetColourForSide(int side) { 
		switch (side) {
		case 1:
			return Color.yellow;
		case 2:
			return Color.red;
		case 3:
			return Color.magenta;
		case 4:
			return Color.blue;
		default:
			return Color.yellow;
		}		
	}


	public static Color GetColourForSide_OLD(int side, int our_side, int num_sides) {
		if (side == our_side) {
			return Color.yellow; // Always make us yellow
		} else {
			if (num_sides == 2 && our_side > 0) {
				return Color.red; // Always make opponent red if 2-player game
			} else {
				switch (side) {
				case 1:
					return Color.red;
				case 2:
					return Color.magenta;
				case 3:
					return Color.blue;
				case 4:
					return P4_COLOUR;//Color.cyan;
				default:
					return Color.yellow;
				}		
			}
		}
	}


	public static int GetXOffSetFromAngle(int ang, int sq_size) {
		switch (ang) {
		case 90:
			return 0;
		case 135:
			return -sq_size;
		case 180:
			return -sq_size;
		case 225:
			return -sq_size;
		case 270:
			return 0;
		case 315:
			return sq_size;
		case 0:
			return sq_size;
		case 45:
			return sq_size;
		default:
			System.err.println("Unknown angle: " + ang);
			return 0;
		}
	}


	public static int GetYOffSetFromAngle(int ang, int sq_size) {
		switch (ang) {
		case 90:
			return sq_size;
		case 135:
			return sq_size;
		case 180:
			return 0;
		case 225:
			return -sq_size;
		case 270:
			return -sq_size;
		case 315:
			return -sq_size;
		case 0:
			return 0;
		case 45:
			return sq_size;
		default:
			System.err.println("Unknown angle: " + ang);
			return 0;
		}
	}


	protected void drawWallEdges(Graphics g) throws SQLException {
		// Draw the floor first
		String sql = "SELECT * FROM MapDataSquares WHERE MapDataID = " + mid + " And SquareType = " + MapDataTable.MT_FLOOR;
		ResultSet rs = dbs.getResultSet(sql);
		g.setColor(Color.green.darker());
		while (rs.next()) {
			g.fillRect((rs.getInt("MapX") * sq_size)-BORDER_SIZE, (rs.getInt("MapY") * sq_size)-BORDER_SIZE, sq_size+BORDER_SIZE*2, sq_size+BORDER_SIZE*2);

		}
		rs.close();
	}


	protected void drawExplosions(Graphics g, GamesTable game, int turn_no) throws SQLException {
		for (int i=0 ; i<=1 ; i++) {
			String sql = "SELECT * FROM UnitHistory WHERE EventType = " + UnitHistoryTable.UH_EXPLOSION + " AND GameID = " + game.getID() + " AND TurnNo = " + (turn_no-i);
			ResultSet rs = dbs.getResultSet(sql);

			g.setColor(Color.red);
			if (i == 1) {
				g.setColor(Color.red.darker());
			}
			while (rs.next()) {
				int rad = rs.getInt("Radius") * sq_size*2;
				g.drawOval((rs.getInt("MapX") * sq_size) - (rad/2) + (sq_size/2), (rs.getInt("MapY") * sq_size) - (rad/2) + (sq_size/2), rad, rad);
			}
		}
	}


	protected void drawComputers(Graphics g, int our_side) throws SQLException {
		// Draw computers
		String sql = "SELECT * FROM MapDataSquares WHERE MapDataID = " + mid + " And SquareType = " + MapDataTable.MT_COMPUTER;
		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) {
			Color c = null;
			if (rs.getInt("OwnerSide") == our_side) {
				if (rs.getInt("Destroyed") != 1) {
					c = OUR_COMPUTER_COL; 
				} else {
					c = OUR_DESTROYED_COMPUTER_COL; 
				}
			} else {
				if (rs.getInt("Destroyed") != 1) {
					c = OPP_COMPUTER_COL;
				} else {
					c = OPP_DESTROYED_COMPUTER_COL; 
				}
			}
			g.setColor(c);
			g.fillRect((rs.getInt("MapX") * sq_size)+2, (rs.getInt("MapY") * sq_size)+2, sq_size-4, sq_size-4);

		}
	}


	protected void setFloorColour(Graphics g, int tex, AbstractMission mission) {
		try {
			switch (tex) {
			case TextureStateCache.TEX_WATER:
				g.setColor(Color.LIGHT_GRAY);
				break;

			default:
				//g.setColor(Color.pink);
				//DSRWebServer.p("Unknown floor tex: " + tex);

				if (mission.thin_thick_walls == AbstractMapModel.BLOCK_WALLS) {
					g.setColor(GREEN_FLOOR);
				} else {
					g.setColor(Color.black);
				}

				//throw new RuntimeException("Unknown floor tex: " + rs.getInt("FloorTex"));  NO! As it sends a million emails
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}		
	}


	protected void drawFlagOrEggs(Graphics g, GamesTable game) throws SQLException {
		String sql = "SELECT MapX, MapY, UnitID FROM Equipment WHERE GameID = " + game.getID() + " AND Coalesce(Destroyed, 0) = 0 AND (EquipmentTypeID = " + EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, "FLAG") + " OR EquipmentTypeID = " + EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, "EGG") + ")";
		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) {
			int x = rs.getInt("MapX");
			int y = rs.getInt("MapY");
			// Is it being carried by a unit?
			if (rs.getInt("UnitID") > 0) {
				UnitsTable unit = new UnitsTable(dbs);
				unit.selectRow(rs.getInt("UnitID"));
				x = unit.getMapX();
				y = unit.getMapY();
				unit.close();
			}
			g.setColor(FLAG_COL);
			int sz = 5;
			g.fillOval((x * sq_size)-sz, (y * sq_size)-sz, sq_size+sz+sz, sq_size+sz+sz);
		}
	}


}
