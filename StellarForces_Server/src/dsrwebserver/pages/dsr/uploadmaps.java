package dsrwebserver.pages.dsr;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.ResultSet;

import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import dsrwebserver.DSRWebServer;
import dsrwebserver.UploadFileData;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.UploadedMapsTable;

public class uploadmaps extends AbstractHTMLPage {
	
	public uploadmaps() {
		super();
	}


	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		if (this.session.isLoggedIn()) {
			if (this.headers.uploaded_files == null || this.headers.uploaded_files.isEmpty()) {
				HTMLFunctions.Para(str, "Here you can upload maps created with the Stellar Forces Map Editor (available <a href=\"/files/SFMapEditor.tar.gz\">here</a>).  You can then use these maps as an alternative to the standard map when creating a game.");
				HTMLFunctions.Para(str, "Please note that there is no 'map validation'.  For example, it doesn't check if there are any computers as required for the Moonbase Assault mission, or enough deployment squares for all sides.");
				str.append("<form method=\"post\" enctype=\"multipart/form-data\">");
				str.append("Name: <input type=\"input\" name=\"name\" ><br />Description:");
				HTMLFunctions.TextArea(str, "desc", 10, 60, "", false);
				str.append("File: <input type=\"file\" name=\"mapfile\" >");
				str.append("<input type=\"submit\" value=\"Upload\">");
				str.append("</form> ");
				
				// List uploaded maps
				ResultSet rs = dbs.getResultSet("SELECT * FROM UploadedMaps ORDER BY DateCreated DESC");
				//HTMLFunctions.StartTable(str);
				HTMLFunctions.StartTable(str, "stats", "", 1, "", 5);
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCellHeading(str, "Name");
				HTMLFunctions.AddCellHeading(str, "User");
				HTMLFunctions.AddCellHeading(str, "Description");
				HTMLFunctions.AddCellHeading(str, "Date Created");
				HTMLFunctions.EndRow(str);
				while (rs.next()) {
					HTMLFunctions.StartRow(str);
					HTMLFunctions.AddCell(str, HTMLFunctions.s2HTML(rs.getString("Name")));
					HTMLFunctions.AddCell(str, LoginsTable.GetDisplayName(dbs, rs.getInt("LoginID")));
					HTMLFunctions.AddCell(str, HTMLFunctions.s2HTML(rs.getString("Description")));
					HTMLFunctions.AddCell(str, Dates.FormatDate(rs.getTimestamp("DateCreated"), Dates.UKDATE_FORMAT));
					HTMLFunctions.EndRow(str);
				}
				HTMLFunctions.EndTable(str);
				rs.close();
			} else {
				UploadFileData file = this.headers.uploaded_files.get("mapfile");
				// todo - delete original file if named the same
				String real_filename = this.current_login.getID() + "_" + System.currentTimeMillis() + ".csv";
				UploadedMapsTable.AddRec(dbs, this.current_login.getID(), this.headers.getPostValueAsString("name"),  this.headers.getPostValueAsString("desc"), file.getOriginalFilename(), real_filename, -1);
				// Copy the actual file
				Path from = FileSystems.getDefault().getPath(file.getNewFilenameAndPath());
				new File(DSRWebServer.UPLOADED_MAPS_DIR).mkdirs();
				Path to = FileSystems.getDefault().getPath(DSRWebServer.UPLOADED_MAPS_DIR + real_filename);
				Files.move(from, to, StandardCopyOption.REPLACE_EXISTING);
				// todo - validate map file?
				HTMLFunctions.Para(str, "Map file successfully uploaded.  Thank you for using the Stellar Forces Map Uploader.Please note that currently, only games created on the website can use uploaded maps.");
				HTMLFunctions.Para(str, "<a href=\"?\">Upload another</a>");
			}
		} else {
			HTMLFunctions.Para(str, "Sorry, you need to be logged in to upload maps.");
		}

		this.body_html.append(MainLayout.GetHTML(this, "Map Upload", str));		

	}

}
