package dsrwebserver.pages.dsr;

import dsrwebserver.pages.AbstractHTMLPage;


public final class screenshots extends AbstractHTMLPage {

	public screenshots() {
		super();
	}

	@Override
	public void process() throws Exception {
		this.redirectToMainPage("");
		/*StringBuffer str = new StringBuffer();

		HTMLFunctions.Para(str, "These are screenshots from the " + DSRWebServer.TITLE + " PC client.  Note that these pictures are taken from various version of the client, including older versions which weren't as good as it is now.");

		//HTMLFunctions.Heading(str, 3, "Click on the pictures for a bigger version.  More screenshots can be found <a href=\"" + DSRWebServer.IMAGES_HOST + "/images/screenshots/older\">here</a>.");

		File files[] = new File("./webroot/images/screenshots").listFiles();
		if (files != null) {
			int col_num = 1;
			str.append("<center>");
			HTMLFunctions.StartTable(str);
			HTMLFunctions.StartRow(str);
			for (int i=0 ; i<files.length ; i++) {
				if (files[i].getName().toLowerCase().endsWith(".jpg") || files[i].getName().toLowerCase().endsWith(".gif") || files[i].getName().toLowerCase().endsWith(".png")) {
					HTMLFunctions.StartCell(str);
					str.append("<a href=\"" + DSRWebServer.IMAGES_HOST + "/images/screenshots/" + files[i].getName() + "\"><img src=\"" + DSRWebServer.IMAGES_HOST + "/images/screenshots/" + files[i].getName() + "\" width=\"270\" alt=\"" + files[i].getName() + "\" title=\"" + files[i].getName() + "\"/></a>");
					HTMLFunctions.EndCell(str);

					if (col_num >= 3) {
						col_num = 1;
						HTMLFunctions.EndRow(str);
						HTMLFunctions.StartRow(str);
					} else {
						col_num++;
					}
				}
			}
			if (col_num  == 1) {
				HTMLFunctions.EndRow(str);
			}
			HTMLFunctions.EndTable(str);
			str.append("</center>");
		}


		this.body_html.append(MainLayout.GetHTML(this, "Screenshots", str));*/		
	}

}
