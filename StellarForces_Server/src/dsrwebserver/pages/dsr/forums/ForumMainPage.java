package dsrwebserver.pages.dsr.forums;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.LoginsTable;

public class ForumMainPage extends AbstractHTMLPage {

	public ForumMainPage() {
		super();

	}

	public void process() throws Exception {
		/*if (DSRWebServer.IsSF()) {
			this.redirectTo_Using303(DSRWebServer.NEW_FORUM_LINK + "/viewforum.php?f=1");
			return;
		} else {*/		
			StringBuffer str = new StringBuffer();

			if (this.session.isLoggedIn()) {
				HTMLFunctions.Para(str, "<a href=\"ForumRepliesPage.cls\">Replies to your postings</a>");
			}
			ForumSearchResults.GetSearchBox(str, "");

			appendListOfCats(str);
			str.append("<br />");
			appendMostRecentComments(str);

			this.body_html.append(MainLayout.GetHTML(this, "Forums", str));
		//}
	}


	private void appendListOfCats(StringBuffer str) throws SQLException {
		str.append("<div class=\"structural\">");
		HTMLFunctions.Heading(str, 3, "Categories:- ");
		ResultSet rs = dbs.getResultSet("SELECT * FROM ForumTopics WHERE ParentForumTopicID IS NULL ORDER BY Ordering");
		HTMLFunctions.StartTable(str);
		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCellHeading(str, "Forum");
		HTMLFunctions.AddCellHeading(str, "Total Posts");
		HTMLFunctions.AddCellHeading(str, "Last Post");
		HTMLFunctions.AddCellHeading(str, "Posted By");
		HTMLFunctions.EndRow(str);
		while (rs.next()) {
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "<a class=\"large\" href=\"ForumTopicsPage.cls?topic=" + rs.getInt("ForumTopicID") + "\">" + HTMLFunctions.s2HTML(rs.getString("Name")) + "</a><br /><div class=\"little\">" + HTMLFunctions.s2HTML(rs.getString("Description")) + "</div>");
			int tot = dbs.getScalarAsInt("SELECT Sum(TotalPostings) FROM ForumTopics WHERE ParentForumTopicID = " + rs.getInt("ForumTopicID"));
			HTMLFunctions.AddCell(str, "<div class=\"large\">" + tot + "</div>");
			if (rs.getDate("DateOfLastPost") != null) {
				HTMLFunctions.AddCell(str, "<div class=\"large\">" + GetTimeAgo(rs.getTimestamp("DateOfLastPost").getTime()) + "</div>");
				if (rs.getInt("LastPostLoginID") > 0) {
					HTMLFunctions.AddCell(str, "<div class=\"large\">" + LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LastPostLoginID"), true) + " " + "</div>");
				} else {
					HTMLFunctions.AddCell(str, "[not set]");
				}
			}
			HTMLFunctions.EndRow(str);
		}
		HTMLFunctions.EndTable(str);
		str.append("</div>");
	}


	private void appendMostRecentComments(StringBuffer str) throws SQLException {
		str.append("<div class=\"structural\">");

		HTMLFunctions.Heading(str, 3, "Most Recent Postings:-");
		String sql = "SELECT Forum2.Name AS PName, ForumPostings.ForumPostingID, ForumPostings.Body, ForumPostings.DateCreated, ForumTopics.Name, ForumTopics.ForumTopicID, Logins.LoginID FROM ForumPostings ";
		sql = sql + " INNER JOIN ForumTopics ON ForumTopics.ForumTopicID = ForumPostings.ForumTopicID ";
		sql = sql + " INNER JOIN ForumTopics Forum2 ON ForumTopics.ParentForumTopicID = Forum2.ForumTopicID ";
		sql = sql + " LEFT JOIN Logins ON Logins.LoginID = ForumPostings.UserID ";
		sql = sql + " ORDER BY DateCreated DESC LIMIT 15";
		ResultSet rs = dbs.getResultSet(sql);
		HTMLFunctions.StartUnorderedList(str);
		while (rs.next()) {
			str.append("<li>");
			str.append("<h4><a href=\"ForumPostingsPage.cls?topic=" + rs.getInt("ForumTopicID") + "\">" + HTMLFunctions.s2HTML(rs.getString("Name")) + "</a></h4>");
			str.append(ForumPostingsPage.GetEncodedMessage(rs.getString("Body")) + "<br />");
			String name = "Anonymous";
			if (rs.getInt("LoginID") >0) {//this.session.isLoggedIn()) {
				name = 	LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginID"), true);
			}
			//str.append("<div class=\"little\">Posted by " + name + " at " + Dates.FormatDate(rs.getTimestamp("DateCreated"), Dates.UKDATE_FORMAT2_WITH_TIME) + " to " + rs.getString("PName") + " | <a href=\"ForumPostingsPage.cls?topic=" + rs.getInt("ForumTopicID") + "#pid" + rs.getInt("ForumPostingID") + "\">Go to comment >></a></div>");
			str.append("<div class=\"little\">Posted by " + name + " " + GetTimeAgo(rs.getTimestamp("DateCreated").getTime()) + " to " + rs.getString("PName") + " | <a href=\"ForumPostingsPage.cls?topic=" + rs.getInt("ForumTopicID") + "#pid" + rs.getInt("ForumPostingID") + "\">Go to comment >></a></div>");
			str.append("</li><br />");
		}
		HTMLFunctions.EndUnorderedList(str);

		str.append("</div>");

	}


	public static String GetTimeAgo(Date d) {
		long ms = d.getTime();
		return GetTimeAgo(ms);
	}


	public static String GetTimeAgo(long ms) {
		if (ms < 0) {
			ms = ms * -1;
		}
		long diff = System.currentTimeMillis() - ms;
		if (diff < Dates.MINUTE) { //< 1 min
			return (diff/1000) + " seconds ago";
		} else if (diff < Dates.HOUR) { // < 1hr
			return (diff/Dates.MINUTE) + " minutes ago";
		} else if (diff < Dates.DAY) {
			return (diff/Dates.HOUR) + " hours ago";
		} else if (diff < Dates.YEAR) {
			return (diff/Dates.DAY) + " days ago";
		} else {
			return (diff/Dates.YEAR) + " years ago";
		}
	}

}
