package dsrwebserver.pages.dsr.forums;

import java.sql.ResultSet;

import ssmith.dbs.SQLFuncs;
import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;

public class ForumSearchResults extends AbstractHTMLPage {
	
	private int MAX_RESULTS = 100;

	public ForumSearchResults() {
		super();

	}

	public void process() throws Exception {
		StringBuffer str = new StringBuffer();
		
		String query = this.headers.getGetValueAsString("q");
		String title = "Search Results";
		
		GetSearchBox(str, query);
		
		if (query.length() > 0) {
			int count = 0;
			title = title + " for '" + HTMLFunctions.HTMLEncode(query) + "'";
			HTMLFunctions.StartUnorderedList(str);

			// Forum titles
			String sql = "SELECT * FROM ForumTopics WHERE Name LIKE " + SQLFuncs.s2sql("%" + query + "%") + " ORDER BY DateCreated DESC LIMIT " + MAX_RESULTS;
			ResultSet rs = dbs.getResultSet(sql);
			while (rs.next()) {
				count++;
				if (count >= MAX_RESULTS) {
					break;
				}
				HTMLFunctions.AddListEntry(str, "<a href=\"ForumPostingsPage.cls?topic=" + rs.getInt("ForumTopicID") + "\">" + HTMLFunctions.HTMLEncode(rs.getString("Name")) + "</a> <i>(" + Dates.FormatDate(rs.getTimestamp("DateCreated"), "dd MMM yyyy HH:mm") + ")</i>");
			}
			rs.close();
			
			// Forum postings
			sql = "SELECT ForumPostings.ForumTopicID, ForumPostings.ForumPostingID, ForumTopics.Name, ForumPostings.Body, ForumPostings.DateCreated FROM ForumPostings ";
			sql = sql + " INNER JOIN ForumTopics ON ForumTopics.ForumTopicID = ForumPostings.ForumTopicID ";
			sql = sql + " WHERE Body LIKE " + SQLFuncs.s2sql("%" + query + "%") + " ORDER BY DateCreated DESC LIMIT " + MAX_RESULTS;
			rs = dbs.getResultSet(sql);
			while (rs.next()) {
				count++;
				if (count >= MAX_RESULTS) {
					break;
				}
				HTMLFunctions.AddListEntry(str, "<a href=\"ForumPostingsPage.cls?topic=" + rs.getInt("ForumTopicID") + "#pid" + rs.getInt("ForumPostingID") + "\">" + HTMLFunctions.HTMLEncode(rs.getString("Name")) + "</a>: " + HTMLFunctions.HTMLEncode(rs.getString("Body")) + " <i>(" + Dates.FormatDate(rs.getTimestamp("DateCreated"), "dd MMM yyyy HH:mm") + ")</i>");
			}
			
			HTMLFunctions.EndUnorderedList(str);
			
			HTMLFunctions.Para(str, count + " results found.");
			
		}

		this.body_html.append(MainLayout.GetHTML(this, title, str));
	}
	
	
	public static void GetSearchBox(StringBuffer str, String query) {
		HTMLFunctions.StartForm(str, "", "/dsr/forums/ForumSearchResults.cls", "get");
		HTMLFunctions.TextBox(str, "q", query, false, 50, 40);
		HTMLFunctions.SubmitButton(str, "Search Forums");
		HTMLFunctions.EndForm(str);
		
	}
	
}
