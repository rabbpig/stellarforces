package  dsrwebserver.pages.dsr.forums;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.html.HTMLFunctions;
import dsr.SharedStatics;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.ForumPostingsTable;
import dsrwebserver.tables.ForumSubscriptionsTable;
import dsrwebserver.tables.ForumTopicsTable;
import dsrwebserver.tables.LoginsTable;

public class ForumPostingsPage extends AbstractHTMLPage {

	public static final String DEF_TEXT = "[Enter comment here]";
	public static final Pattern p_find_links = Pattern.compile("(https?://)[a-zA-Z0-9\\./\\?=\\&;#_+%\\-:]*", Pattern.CASE_INSENSITIVE);

	public ForumPostingsPage() {
		super();
	}

	public void process() throws Exception {
		String comment = headers.getGetOrPostValueAsString("comment").trim();
		String next = headers.getGetOrPostValueAsString("next");
		int replytoid = headers.getGetOrPostValueAsInt("replyto");
		int topicid = headers.getGetOrPostValueAsInt("topic");
		int del = headers.getGetOrPostValueAsInt("del");

		ForumPostingsTable postings = new ForumPostingsTable(dbs);

		// Delete?
		if (del > 0) {
			if (this.session.isLoggedIn()) {
				if (this.current_login.isAdmin()) {
					postings.delete(del);
				}
			}
		}


		String forum_name = "Game Chat";
		try {
			ForumTopicsTable ftt = new ForumTopicsTable(dbs);
			if (ftt.selectByID(topicid)) {
				forum_name = ftt.getName();
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}
		if (comment.length() > 0 && (DSRWebServer.ALLOW_ANON_POSTING || this.session.isLoggedIn())) { // Posting a comment!
			if (comment.equalsIgnoreCase(DEF_TEXT) == false) {
				// Only allow URLs if played a game
				try {
					if (this.current_login.getTotalGamesFinished() <= 0) {
						int count = comment.split("http://").length;
						if (count > 1) {
							this.body_html.append("Linking is not allowed until you have completed at least one game.");
							DSRWebServer.SendEmailToAdmin("Spam Attempt", "User: " + this.current_login.getID() + ", Code: 1\n\n" + comment);
							return;
						}
						// Check for dupes
						count = dbs.getScalarAsInt("SELECT Count(*) FROM ForumPostings WHERE UserID = " + this.current_login.getID() + " AND Body = " + SQLFuncs.s2sql(comment));
						if (count > 1) {
							this.body_html.append("Duplicate posts are not allowed until you have completed at least one game.");
							DSRWebServer.SendEmailToAdmin("Spam Attempt", "User: " + this.current_login.getID() + ", Code: 2\n\n" + comment);
							return;
						}
						// Check for time
						count = dbs.getScalarAsInt("SELECT TIME_TO_SEC(timediff(now(), datecreated)) FROM ForumPostings order by datecreated desc limit 1");
						if (count < 60*4) {
							this.body_html.append("Until you have completed a game, there is a limit on how frequently you can post.  Please wait a few minutes before posting again.");
							DSRWebServer.SendEmailToAdmin("Spam Attempt", "User: " + this.current_login.getID() + ", Code: 3\n\n" + comment);
							return;
						}
					}

				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}

				String from_ip = super.conn.sck.getInetAddress().getHostAddress();
				int our_loginid = -1;
				if (this.session.isLoggedIn()) {
					our_loginid = current_login.getID(); 
					// Subscribe us to this forum
					ForumSubscriptionsTable.AddSub(dbs, our_loginid, topicid);
				}
				int replied_to_login_id = -1;
				int posting_id = -1;
				if (replytoid == 0) {
					posting_id = postings.addPosting(our_loginid, topicid, -1, comment, from_ip);
				} else {
					posting_id = postings.addPosting(our_loginid, topicid, replytoid, comment, from_ip);
				}

				String link = "/dsr/forums/ForumPostingsPage.cls?topic=" + topicid + "#pid" + posting_id; 
				// Email subscribers
				LoginsTable subbers = new LoginsTable(dbs);
				try {
					ArrayList<Integer> subs = ForumSubscriptionsTable.GetAllSub(dbs, topicid);
					try {
						for (int i=0 ; i<subs.size() ; i++) {
							int id = subs.get(i).intValue();
							if (id != replied_to_login_id && id != our_loginid) { // Don't send if we've already sent one
								if (subbers.doesRowExist(id)) {
									subbers.selectRow(id);
									subbers.sendEmail("New Posting to forum '" + forum_name + "'", "You are receiving this email because you are subscribed to the forum " + forum_name + " which has a new comment from " + LoginsTable.GetDisplayName(dbs, this.current_login.getID()) + ".  The comment is as follows:\n\n--\n\"" + comment + "\"\n\n--\n\nYou can go straight to the comment by following this link:-\n\n" + DSRWebServer.WEBSITE_URL + link);
								}
							}
						}
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex, true);
					}
				} finally {
					subbers.close();
				}
				try {
					if (GetTopLevelTopicID(dbs, topicid) != DSRWebServer.GAMES_FORUM_ID) {
						DSRWebServer.SendEmailToAdmin("New Posting to " + DSRWebServer.TITLE + " forum " + forum_name, "New Posting text:\n\n\"" + comment + "\"\n\nYou can go straight to the comment by following this link:-\n\n" + DSRWebServer.WEBSITE_URL + link);
					}
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}

				MainLayout.update_forum_interval.fireInterval(); // Update front page
				super.redirectTo_Using303(link);
				return;
			}
		}
		if (next.equalsIgnoreCase("sub")) {
			if (this.session.isLoggedIn()) {
				ForumSubscriptionsTable.AddSub(dbs, this.current_login.getID(), topicid);
			}
		} else if (next.equalsIgnoreCase("unsub")) {
			if (this.session.isLoggedIn()) {
				ForumSubscriptionsTable.RemoveSub(dbs, this.current_login.getID(), topicid);
			}
		}

		StringBuffer str = new StringBuffer();

		ForumTopicsTable topics = new ForumTopicsTable(dbs);
		if (topics.selectByID(topicid)) {
			super.addHTMLToHead("<script type=\"text/javascript\">");
			super.addHTMLToHead("function onf(id) {");
			super.addHTMLToHead("	var elem = document.getElementById(id);");
			super.addHTMLToHead("	if (elem.value.substring(0,'" + DEF_TEXT + "'.length)  == '" + DEF_TEXT + "') {");
			super.addHTMLToHead("		elem.value = '';");
			super.addHTMLToHead("	}");
			super.addHTMLToHead("}");
			super.addHTMLToHead("function checkLength(id) {");
			super.addHTMLToHead("	var elem = document.getElementById(id);");
			super.addHTMLToHead("	if (elem.value.length > 4000) {");
			super.addHTMLToHead("		elem.value = elem.value.substring(0, 4000);");
			super.addHTMLToHead("	}");
			super.addHTMLToHead("}");
			super.addHTMLToHead("</script>");

			ForumSearchResults.GetSearchBox(str, "");

			this.setTitle(topics.getName());
			this.addHTMLToHead("<script type=\"text/javascript\" src=\"/javascripts/ajax.js\"></script>");
			this.addHTMLToHead("<script type=\"text/javascript\" src=\"/javascripts/forums.js\"></script>");

			str.append("<p><i>You are here: " + GetBreadcrumbs(dbs, topicid, topics.getName()) + "</i></p>");

			if (this.session.isLoggedIn()) {
				//HTMLFunctions.Para(content, "<strong>How to start:</strong>  You can either click on one of the current topics, or start your own by entering the title in the box at the bottom of the list.");
				//HTMLFunctions.Para(str, "You can either post a comment in the topic by entering some text where it says \"[Enter comment here]\", or reply to someone else's comment by clicking on the 'reply' link.");
				if (ForumSubscriptionsTable.IsSubbed(dbs, this.current_login.getID(), topicid)) {
					HTMLFunctions.Para(str, "You are subscribed to this topic.  [<a href=\"?topic=" + topicid + "&next=unsub\">unsubscribe</a>]"); //&ftid=" + ftid + "
				} else {
					HTMLFunctions.Para(str, "You are not currently subscribed to this topic.  [<a href=\"?topic=" + topicid + "&next=sub\">subscribe</a>]"); //&ftid=" + ftid + "
				}
				/*}

			if (this.session.isLoggedIn()) {*/
				//HTMLFunctions.Para(content, "<strong>How to start:</strong>  You can either click on one of the current topics, or start your own by entering the title in the box at the bottom of the list.");
				//HTMLFunctions.Para(str, "You can either post a comment in the topic by entering some text where it says \"[Enter comment here]\", or reply to someone else's comment by clicking on the 'reply' link.");
			}

			/*if (DSRWebServer.ALLOW_ANON_POSTING || this.session.isLoggedIn()) {
				//content.append("" + GetCommentPostingBox(topicid, 0) + "" + HTMLFunctions.CR);
				GetCommentPostingBox(str, topicid, 0);
			}*/

			//str.append("<br />Postings already made:" + HTMLFunctions.CR);

			//content.append(showTopLevelMessages(topicid));
			showTopLevelMessages(str, topicid);

			if (DSRWebServer.ALLOW_ANON_POSTING || this.session.isLoggedIn()) {
				//content.append("" + GetCommentPostingBox(topicid, 0) + "" + HTMLFunctions.CR);
				GetCommentPostingBox(str, topicid, 0);
			}

		} else {
			HTMLFunctions.Para(str, "Sorry, that topic could not be found.");
			HTMLFunctions.Para(str, "<a href=\"ForumMainPage.cls\">Back</a>.");
		}
		this.body_html.append(MainLayout.GetHTML(this, forum_name, str));
	}


	private void showTopLevelMessages(StringBuffer str, int topicid) throws SQLException {
		//str.append("<div class=\"structural\">");
		if (dbs.getScalarAsInt("SELECT Count(*) FROM ForumPostings WHERE ForumTopicID = " + topicid + " AND ReplyToPostingID IS NULL") > 0) {
			HTMLFunctions.StartUnorderedList(str);
			String sql = "SELECT ForumPostings.ForumPostingID, ForumPostings.Body, ForumPostings.ForumTopicID, ForumPostings.DateCreated, Logins.LoginID FROM ForumPostings ";
			sql = sql + " LEFT JOIN Logins ON Logins.LoginID = ForumPostings.UserID ";
			sql = sql + " WHERE ForumTopicID = " + topicid;
			//sql = sql + " AND ReplyToPostingID IS NULL";
			sql = sql + " ORDER By ForumPostings.DateCreated ";//DESC";
			ResultSet rs = dbs.getResultSet(sql);
			while (rs.next()) {
				appendMessage(str, rs);
				//str.append("<br />");
				//this.showReplyMessages(str, rs.getInt("ForumPostingID"));
			}
			HTMLFunctions.EndUnorderedList(str);
		} else {
			str.append("[There are currently no postings on this thread]" + HTMLFunctions.CR);
		}
		//str.append("</div>");
	}


	private void showReplyMessages(StringBuffer str, int postingid) throws SQLException {
		if (dbs.getScalarAsInt("SELECT Count(*) FROM ForumPostings WHERE ReplyToPostingID = " + postingid) > 0) {
			String sql = "SELECT ForumPostings.ForumPostingID, ForumPostings.Body, ForumPostings.ForumTopicID, Logins.LoginID, ForumPostings.DateCreated FROM ForumPostings ";
			sql = sql + " LEFT JOIN Logins ON Logins.LoginID = ForumPostings.UserID ";
			sql = sql + " WHERE ReplyToPostingID = " + postingid + " ORDER By ForumPostings.DateCreated";
			ResultSet rs = dbs.getResultSet(sql);
			str.append("<li>");
			HTMLFunctions.StartUnorderedList(str);
			while (rs.next()) {
				appendMessage(str, rs);
				this.showReplyMessages(str, rs.getInt("ForumPostingID"));
			}
			HTMLFunctions.EndUnorderedList(str);
			str.append("</li>");
		}
	}


	private void appendMessage(StringBuffer str, ResultSet rs_postings) throws SQLException {
		String body_enc = GetEncodedMessage(rs_postings.getString("Body"));
		String reply_link = "";
		if (this.session.isLoggedIn() || DSRWebServer.ALLOW_ANON_POSTING) {
			//reply_link = "<span id=\"id" + rs_postings.getInt("ForumPostingID") + "\">[ <a href=\"javascript:reply(" + rs_postings.getInt("ForumTopicID") + "," + rs_postings.getInt("ForumPostingID") + ");\">Reply</a> ] [<a href=\"ForumPostingsPage.cls?topic=" + rs_postings.getInt("ForumTopicID") + "#pid" + rs_postings.getInt("ForumPostingID") + "\">permalink</a>]";
			reply_link = "<span id=\"id" + rs_postings.getInt("ForumPostingID") + "\">[<a href=\"ForumPostingsPage.cls?topic=" + rs_postings.getInt("ForumTopicID") + "#pid" + rs_postings.getInt("ForumPostingID") + "\">permalink</a>]";
			if (this.session.isLoggedIn()) {
				if (this.current_login.isAdmin()) {
					reply_link = reply_link + "[ <a href=\"?del=" + rs_postings.getInt("ForumPostingID") + "\">Delete</a> ]";
				}
			}
			reply_link = reply_link + "</span>"; 
		} else {
			reply_link = " [Login to reply]"; 
		}
		String name = "Anonymous";
		if (rs_postings.getInt("LoginID") > 0) {//(this.session.isLoggedIn()) {
			name = 	LoginsTable.GetDisplayName_Enc(dbs, rs_postings.getInt("LoginID"), true);
		}
		//str.append("<li><a name=\"pid" + rs_postings.getInt("ForumPostingID") + "\">" + body_enc + "</a> <div class=\"little\">Posted by " + name + " at " + Dates.FormatDate(rs_postings.getTimestamp("DateCreated"), Dates.UKDATE_FORMAT2_WITH_TIME) + " " + reply_link + "</div></li>" + HTMLFunctions.CR);
		str.append("<li><a name=\"pid" + rs_postings.getInt("ForumPostingID") + "\">" + body_enc + "</a> <div class=\"little\">Posted by " + name + " " + ForumMainPage.GetTimeAgo(rs_postings.getTimestamp("DateCreated").getTime()) + " " + reply_link + "</div></li>" + HTMLFunctions.CR);
		str.append("<br /></li>" + HTMLFunctions.CR);
	}


	public static String GetEncodedMessage(String body) {
		String s = HTMLFunctions.s2HTML(body).replaceAll("\\r\\n", "<br />"); // Must encode BEFORE we add HTML tags!
		int start_pos = 0;
		Matcher m = p_find_links.matcher(s);
		while (m.find()) {
			String url = s.substring(m.start()+start_pos, m.end()+start_pos);
			s = s.substring(0, m.start()+start_pos) + "<a href=\"" + url + "\">" + url + "</a>" + s.substring(m.end()+start_pos, s.length());
			start_pos = start_pos + m.start() + 15 + (url.length()*2);//(m.end()*2) - m.start();
			m = p_find_links.matcher(s.substring(start_pos));
		}
		return s;
	}


	public static void GetCommentPostingBox(StringBuffer str, int topicid, int replyto) {
		str.append("<form method=\"post\" action=\"\">" + HTMLFunctions.CR);
		str.append("<input type=\"hidden\" name=\"topic\" value=\"" + topicid + "\" />" + HTMLFunctions.CR);
		str.append("<input type=\"hidden\" name=\"replyto\" value=\"" + replyto + "\" />" + HTMLFunctions.CR);
		String id = "r" + replyto;
		if (replyto == 0) {
			id = "t" + topicid;
		}
		str.append("<textarea id=\"" + id + "\" onmousedown=\"onf('" + id + "');\" onkeyup=\"checkLength('" + id + "');\" name=\"comment\" cols=\"75\" rows=\"5\" title=\"Message\">" + DEF_TEXT + "</textarea><br />");
		String title = "Post Comment";
		if (replyto > 0) {
			title = "Post Reply";
		}
		str.append("<input class=\"submit\" type=\"submit\" value=\"" + title + "\" />" + HTMLFunctions.CR);
		str.append("</form>" + HTMLFunctions.CR);
	}


	public static int GetTopLevelTopicID(MySQLConnection dbs, int topicid) throws SQLException {
		ResultSet rs = null;
		while (topicid > 0) {
			String sql = "SELECT COALESCE(ParentForumTopicID, 0), ForumTopicID FROM ForumTopics WHERE ForumTopicID = " + topicid;
			rs = dbs.getResultSet(sql);
			rs.next();
			topicid = rs.getInt(1);
		}
		return rs.getInt("ForumTopicID");
	}


	private static String GetBreadcrumbs(MySQLConnection dbs, int topicid, String name) throws SQLException {
		ResultSet rs = null;
		StringBuffer str = new StringBuffer();
		str.append("<a href=\"ForumMainPage.cls\">" + SharedStatics.TITLE + "</a>");
		if (topicid > 0) {
			String sql = "SELECT * FROM ForumTopics WHERE ForumTopicID = " + topicid;
			rs = dbs.getResultSet(sql);
			rs.next();
			sql = "SELECT * FROM ForumTopics WHERE ForumTopicID = " + rs.getInt("ParentForumTopicID");
			rs = dbs.getResultSet(sql);
			if (rs.next()) {
				str.append(" &gt; <a href=\"ForumTopicsPage.cls?topic=" + rs.getInt(1) + "\">" + rs.getString("Name") + "</a>");
			}
		}
		str.append(" &gt; <a href=\"ForumPostingsPage.cls?topic=" + topicid + "\">" + name + "</a>");
		return str.toString();
	}

}
