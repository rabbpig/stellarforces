package dsrwebserver.pages.dsr.forums;

import java.util.Random;

import dsrwebserver.pages.AbstractHTMLPage;


import ssmith.lang.Functions;

public class PleaseWait extends AbstractHTMLPage {

	public PleaseWait() {
		super();
		//this.send_in_chunks = true;
		this.send_google_analytics = false;
	}

	public void process() throws Exception {
		this.setTitle("Please wait...");
		super.sendPage();
		
		Random r = new Random();
		while (true) {
			byte b[] = new byte[200];
			r.nextBytes(b);
			String s = new String(b);
			sendHTMLChunk(s);
			super.conn.sck.getOutputStream().flush();
			Functions.delay(1000);
		}
	}

}
