package dsrwebserver.pages.dsr.forums;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.LoginsTable;

/**
 * THIS IS NO LONGER USED!!
 *
 */
public class ForumRepliesPage extends AbstractHTMLPage {

	public void process() throws Exception {
		if (this.session.isLoggedIn()) {
			//this.current_login.setHasForumReply(false);

			StringBuffer str = new StringBuffer();
			HTMLFunctions.Heading(str, 2, "Replies to Your Postings");

			String sql = "SELECT ForumPostings.ForumPostingID, ForumPostings.Body, ForumPostings.ForumTopicID, Logins.LoginID, ForumPostings.DateCreated FROM ForumPostings ";
			sql = sql + " LEFT JOIN Logins ON Logins.LoginID = ForumPostings.UserID ";
			sql = sql + " WHERE ReplyToPostingID IN (";
			sql = sql + " SELECT ForumPostingID FROM ForumPostings WHERE ForumPostings.UserID = " + this.current_login.getID();
			sql = sql + ") ORDER By ForumPostings.DateCreated DESC";
			boolean any = false;
			ResultSet rs = dbs.getResultSet(sql);
			HTMLFunctions.StartUnorderedList(str);
			while (rs.next()) {
				any = true;
				str.append("<p>");
				appendMessage(str, rs);
				str.append("</p>");
			}
			if (!any) {
				str.append("<li>No one seems to have replied to your posts yet.  You need to say something more controversial.</li>");
			}
			HTMLFunctions.EndUnorderedList(str);

			this.body_html.append(MainLayout.GetHTML(this, "Forum Replies", str));
		}
	}

	private void appendMessage(StringBuffer str, ResultSet rs_postings) throws SQLException {
		//StringBuffer str = new StringBuffer();
		String name = "Anonymous";
		if (rs_postings.getInt("LoginID") > 0) {
			name = LoginsTable.GetDisplayName_Enc(dbs, rs_postings.getInt("LoginID"), true);
		}
		str.append("<li>" + ForumPostingsPage.GetEncodedMessage(rs_postings.getString("Body")) + " <div class=\"little\">Posted by " + name + " at " + Dates.FormatDate(rs_postings.getTimestamp("DateCreated"), Dates.UKDATE_FORMAT2_WITH_TIME) + " [<a href=\"ForumPostingsPage.cls?topic=" + rs_postings.getInt("ForumTopicID") + "#pid" + rs_postings.getInt("ForumPostingID") + "\">Context</a>]</div></li>" + HTMLFunctions.CR);
		//str.append("<li>" + ForumTopicsPage.GetEncodedMessage(rs_postings.getString("Body")) + " <div class=\"little\">Posted by " + LoginsTable.GetDisplayName_Enc(dbs, rs_postings.getInt("LoginID"), true) + " at " + Dates.FormatDate(rs_postings.getTimestamp("DateCreated"), Dates.UKDATE_FORMAT2_WITH_TIME) + " <a href=\"ForumPostingsPage.cls?topic=" + ForumPostingsPage.GetTopLevelTopicID(dbs, rs_postings.getInt("ForumTopicID")) + "#pid" + rs_postings.getInt("ForumPostingID") + "\">[Context]</a></div></li>" + HTMLFunctions.CR);
		str.append("</li>" + HTMLFunctions.CR);
		//return str;
	}

}
