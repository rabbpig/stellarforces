package dsrwebserver.pages.dsr.forums;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.html.HTMLFunctions;
import ssmith.util.Interval;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.ForumSubscriptionsTable;
import dsrwebserver.tables.ForumTopicsTable;
import dsrwebserver.tables.LoginsTable;

public class ForumTopicsPage extends AbstractHTMLPage {
	
	private static Interval update_tots_interval = new Interval(1000*60*10, true);

	public ForumTopicsPage() {
		super();
	}

	public void process() throws Exception {
		String next = headers.getGetOrPostValueAsString("next");
		String topicname = headers.getGetOrPostValueAsString("topicname");
		int topicid = headers.getGetOrPostValueAsInt("topic");

		if (next.equalsIgnoreCase("sub")) {
			if (this.session.isLoggedIn()) {
				ForumSubscriptionsTable.AddSub(dbs, this.current_login.getID(), headers.getGetValueAsInt("ftid"));
			}
		} else if (next.equalsIgnoreCase("unsub")) {
			if (this.session.isLoggedIn()) {
				ForumSubscriptionsTable.RemoveSub(dbs, this.current_login.getID(), headers.getGetValueAsInt("ftid"));
			}
		}
		if (topicname.length() > 0) { // New topic
			ForumTopicsTable topics = new ForumTopicsTable(dbs);
			int loginid = -1;
			if (this.session.isLoggedIn()) {
				loginid = this.current_login.getID();
			}
			int id = topics.addTopic(topicid, topicname, loginid);
			super.redirectTo_Using303("ForumPostingsPage.cls?topic=" + id);
			return;
		}
		
		StringBuffer str = new StringBuffer();

		ForumTopicsTable topics = new ForumTopicsTable(dbs);
		if (topics.selectByID(topicid)) {
			this.addHTMLToHead("<script type=\"text/javascript\" src=\"/javascripts/ajax.js\"></script>" + HTMLFunctions.CR);
			this.addHTMLToHead("<script type=\"text/javascript\" src=\"/javascripts/forums.js\"></script>" + HTMLFunctions.CR);

			str.append("<p><i>You are here: " + GetBreadcrumbs(dbs, topicid, topics.getName()) + "</i></p>");

			ForumSearchResults.GetSearchBox(str, "");

			getListOfTopics(str, topics, topicid);

			str.append("<br />");

			appendMostRecentComments(str, topics, topicid);

		} else {
			HTMLFunctions.Para(str, "Sorry, that topic could not be found.");
			HTMLFunctions.Para(str, "<a href=\"ForumMainPage.cls\">Back</a>.");
		}
		this.body_html.append(MainLayout.GetHTML(this, "Forum Topics", str));
	}


	private void getListOfTopics(StringBuffer str, ForumTopicsTable topics, int parenttopicid) throws SQLException {
		str.append("<div class=\"structural\">");
		HTMLFunctions.Heading(str, 2, HTMLFunctions.s2HTML(topics.getName()));
		HTMLFunctions.Heading(str, 5, "Current topics under discussion:-");
		if (dbs.getScalarAsInt("SELECT Count(*) FROM ForumTopics WHERE ParentForumTopicID = " + parenttopicid) > 0) {
			String sql = "SELECT * FROM ForumTopics WHERE ParentForumTopicID = " + parenttopicid + " ORDER By COALESCE(Sticky, 0) DESC, DateOfLastPost DESC";
			ResultSet rs = dbs.getResultSet(sql);
			HTMLFunctions.StartTable(str);
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCellHeading(str, "Thread Name");
			HTMLFunctions.AddCellHeading(str, "Total Posts");
			HTMLFunctions.AddCellHeading(str, "Last Post");
			HTMLFunctions.AddCellHeading(str, "Posted By");
			HTMLFunctions.AddCellHeading(str, "Subscribed?");
			HTMLFunctions.EndRow(str);
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, getNewTopicForm(parenttopicid).toString());
			HTMLFunctions.EndRow(str);
			while (rs.next()) {
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, "<div class=\"large\"><a href=\"ForumPostingsPage.cls?topic=" + rs.getInt("ForumTopicID") + "\">" + HTMLFunctions.s2HTML(rs.getString("Name")) + "</a></div>");
				
				if (update_tots_interval.hitInterval()) {
					int tot = dbs.getScalarAsInt("SELECT Count(*) FROM ForumPostings WHERE ForumTopicID = " + rs.getInt("ForumTopicID"));
					dbs.RunIdentityInsert("UPDATE ForumTopics SET TotalPostings = " + tot + " WHERE ForumTopicID = " + rs.getInt("ForumTopicID"));
				}
				HTMLFunctions.AddCell(str, "<div class=\"large\">" + rs.getInt("TotalPostings") + "</div>");
				
				if (rs.getDate("DateOfLastPost") != null) {
					HTMLFunctions.AddCell(str, "<div class=\"large\">" + ForumMainPage.GetTimeAgo(rs.getTimestamp("DateOfLastPost").getTime()) + "</div>");
				} else {
					HTMLFunctions.AddCell(str, "");
				}
				if (rs.getInt("LastPostLoginID") > 0) {
					HTMLFunctions.AddCell(str, "<div class=\"large\">" + LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LastPostLoginID"), true) + " " + "</div>");
				} else {
					HTMLFunctions.AddCell(str, "[not set]");
				}
				if (this.session.isLoggedIn()) {
					if (ForumSubscriptionsTable.IsSubbed(dbs, this.current_login.getID(), rs.getInt("ForumTopicID"))) {
						HTMLFunctions.AddCell(str, "Yes [<a href=\"?topic=" + parenttopicid + "&next=unsub&ftid=" + rs.getInt("ForumTopicID") + "\">unsubscribe</a>]");
					} else {
						HTMLFunctions.AddCell(str, "No [<a href=\"?topic=" + parenttopicid + "&next=sub&ftid=" + rs.getInt("ForumTopicID") + "\">subscribe</a>]");
					}
				}
				HTMLFunctions.EndRow(str);
			}
			HTMLFunctions.EndTable(str);
		} else {
			if (this.session.isLoggedIn()) {
				HTMLFunctions.StartTable(str);
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, getNewTopicForm(parenttopicid).toString());
				HTMLFunctions.EndRow(str);
				HTMLFunctions.EndTable(str);
			}
		}
		str.append("</div>");
		//return str;
	}


	private void appendMostRecentComments(StringBuffer str, ForumTopicsTable topics, int parenttopicid) throws SQLException {
		str.append("<div class=\"structural\">");

		HTMLFunctions.Heading(str, 3, "Most Recent Postings under " + HTMLFunctions.s2HTML(topics.getName()));
		String sql = "SELECT ForumPostings.ForumPostingID, ForumPostings.Body, ForumPostings.DateCreated, ForumTopics.Name, ForumTopics.ForumTopicID, Logins.LoginID FROM ForumPostings ";
		sql = sql + " INNER JOIN ForumTopics ON ForumTopics.ForumTopicID = ForumPostings.ForumTopicID ";
		sql = sql + " LEFT JOIN Logins ON Logins.LoginID = ForumPostings.UserID ";
		sql = sql + " WHERE ForumTopics.ParentForumTopicID = " + parenttopicid;
		sql = sql + " ORDER BY DateCreated DESC LIMIT 15";
		ResultSet rs = dbs.getResultSet(sql);
		HTMLFunctions.StartUnorderedList(str);
		while (rs.next()) {
			str.append("<li>");
			str.append("<h4><a href=\"ForumPostingsPage.cls?topic=" + rs.getInt("ForumTopicID") + "\">" + HTMLFunctions.s2HTML(rs.getString("Name")) + "</a></h4>");
			str.append(ForumPostingsPage.GetEncodedMessage(rs.getString("Body")) + "<br />");
			String name = "Anonymous";
			if (rs.getInt("LoginID") >0) {//this.session.isLoggedIn()) {
				name = 	LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginID"), true);
			}
			//str.append("<div class=\"little\">Posted by " + name + " at " + Dates.FormatDate(rs.getTimestamp("DateCreated"), Dates.UKDATE_FORMAT2_WITH_TIME) + " | <a href=\"ForumPostingsPage.cls?topic=" + rs.getInt("ForumTopicID") + "#pid" + rs.getInt("ForumPostingID") + "\">Go to comment >></a></div>");
			str.append("<div class=\"little\">Posted by " + name + " " + ForumMainPage.GetTimeAgo(rs.getTimestamp("DateCreated").getTime()) + " | <a href=\"ForumPostingsPage.cls?topic=" + rs.getInt("ForumTopicID") + "#pid" + rs.getInt("ForumPostingID") + "\">Go to comment >></a></div>");
			str.append("</li><br />");
		}
		HTMLFunctions.EndUnorderedList(str);

		str.append("</div>");
	}


	private StringBuffer getNewTopicForm(int topicid) {
		StringBuffer str = new StringBuffer();
		str.append("<form method=\"post\" action=\"\">");
		str.append("<input type=\"hidden\" name=\"topic\" value=\"" + topicid + "\" />");
		str.append("<input maxlength=\"500\" name=\"topicname\" size=\"40\" title=\"Topic Name\" value=\"\" /><input class=\"submit\" type=\"submit\" value=\"Start New Thread\" />");
		str.append("</form>");
		return str;
	}


	private static String GetBreadcrumbs(MySQLConnection dbs, int topicid, String name) throws SQLException {
		StringBuffer str = new StringBuffer();
		str.append("<a href=\"ForumMainPage.cls\">" + DSRWebServer.TITLE + "</a>");
		str.append(" &gt; <a href=\"ForumTopicsPage.cls?topic=" + topicid + "\">" + name + "</a>");
		return str.toString();//rs.getInt("FCTopicID");
	}

}
