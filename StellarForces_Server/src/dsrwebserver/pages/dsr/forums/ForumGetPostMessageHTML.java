package  dsrwebserver.pages.dsr.forums;

import dsrwebserver.pages.AbstractHTMLPage;

public class ForumGetPostMessageHTML extends AbstractHTMLPage {
	
	public ForumGetPostMessageHTML() {
		super();
		
		this.add_default_html = false;
	}

	public void process() throws Exception {
		int topicid = headers.getGetOrPostValueAsInt("topic");
		int replytoid = headers.getGetOrPostValueAsInt("replyto");
		
		//this.body_html.append(ForumPostingsPage.GetCommentPostingBox(topicid, replytoid));
		ForumPostingsPage.GetCommentPostingBox(this.body_html, topicid, replytoid);
	}

}
