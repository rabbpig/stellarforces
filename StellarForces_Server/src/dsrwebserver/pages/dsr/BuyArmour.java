package dsrwebserver.pages.dsr;

import java.sql.ResultSet;

import ssmith.html.HTMLFunctions;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.ArmourTypesTable;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.FactionsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public final class BuyArmour extends AbstractHTMLPage {

	public BuyArmour() {
		super();
	}

	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();
		if (this.session.isLoggedIn()) {
			int gameid = this.headers.getGetOrPostValueAsInt("gid");
			GamesTable game = new GamesTable(dbs);
			try {
			game.selectRow(gameid);
			if (game.isPlayerInGame(this.current_login.getLoginID())) {
				if (game.getGameStatus() == GamesTable.GS_CREATED_DEPLOYMENT) { 
					int our_side = GamesTable.GetSideFromPlayerID(dbs, gameid, this.current_login.getLoginID());

					if (game.isCampGame() && game.hasSideChosenUnits(our_side) == false) {
						//this.redirectTo_Using303("campaign.cls?select_units_mode=1&gameid=" + gameid);
						HTMLFunctions.Para(str, "Please return to the Android app to select your squad.");
						this.body_html.append(MainLayout.GetHTML(this, "Select Squad", str));		
						return;
					} else if (game.hasSideEquipped(our_side) == false) {
						// Check we are able to buy anything!
						if (dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side) == 0) {
							this.redirectTo_Using303("EquipUnits.cls?gid=" + game.getID());
							return;
						}

						AbstractMission m = AbstractMission.Factory(game.getMissionID());
						int tot_creds = m.getCreditsForSide(our_side);
						tot_creds = (tot_creds * game.getPcentCredit()) / 100;
						int camp_creds = 0;
						if (game.isCampGame()) {
							//camp_creds = Math.min(this.current_login.getCampCredits(), tot_creds); // Only allow 200% credits for camp games
							camp_creds = this.current_login.getCampCredits();
							tot_creds = tot_creds + camp_creds;
						}
						int creds_remaining = tot_creds - EquipmentTable.GetValueOfArmourAndEquipment(dbs, gameid, our_side, false);

						String next = this.headers.getGetOrPostValueAsString("next");
						if (next.equalsIgnoreCase("buy")) {
							// Check the cost
							ResultSet rs_units = dbs.getResultSet("SELECT * FROM Units WHERE CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side + " ORDER BY OrderBy");
							int cost = 0;
							while (rs_units.next()) {
								int id = this.headers.getPostValueAsInt("armour_" + rs_units.getInt("UnitID"));
								if (id > 0) {
									cost += ArmourTypesTable.GetCost(dbs, id);
								}
							}
							rs_units.close();
							if (cost <= creds_remaining) {
								UnitsTable unit = new UnitsTable(dbs);
								rs_units = dbs.getResultSet("SELECT UnitID FROM Units WHERE CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side + " ORDER BY OrderBy");
								while (rs_units.next()) {
									unit.selectRow(rs_units.getInt("UnitID"));
									int id = this.headers.getPostValueAsInt("armour_" + rs_units.getInt("UnitID"));
									unit.setArmourType(id);
								}
								unit.close();
								this.redirectTo_Using303("EquipUnits.cls?gid=" + game.getID());
								return;

							} else {
								HTMLFunctions.Para(str, "<b>You do not have enough credits to buy that much!</b>");
							}						
						}

						// Update creds in case we didn't buy any armour
						creds_remaining = tot_creds - EquipmentTable.GetValueOfArmourAndEquipment(dbs, gameid, our_side, true);

						AbstractMission mission = AbstractMission.Factory(game.getMissionID());
						HTMLFunctions.Heading(str, 3, "<b>Mission:</b> <a href=\"/dsr/missiondescriptions.cls?type=" + game.getMissionID() + "&camp=" + (game.isCampGame() ? "1" : "0") + "\">" + mission.mission_name + "</a>");
						HTMLFunctions.Heading(str, 3, "<b>Your Side:</b> " + mission.getSideDescription(our_side) + " (<a href=\"/dsr/viewmap.cls?gid=" + game.getID() + "\">View Units and Map</a>)");

						//HTMLFunctions.Heading(str, 2, "Buy Armour");

						HTMLFunctions.Para(str, "To buy armour, select the type of armour you want for each unit and then click on the Buy Armour button.  The cost of each type of armour is shown in brackets.");

						HTMLFunctions.Para(str, "Once you have purchased your armour, you will be taken to the next page to buy weapons &amp; equipment.  Don't worry, you can return here if you wish.");

						if (game.isCampGame()) {
							HTMLFunctions.Para(str, "As this is a campaign game, any creds you don't spend will be carried forward to the next game.");
						}
						
						if (game.isCampGame()) {
							if (FactionsTable.IsBestFaction(dbs, this.current_login.getFactionID())) {
								HTMLFunctions.Para(str, "You are in the top faction and have access to better equipment!");
							}
						}
						
						str.append("<div style=\"float: right\">");
						armourdetails.GetArmourStatsTable(dbs, str);
						str.append("</div>");

						//str.append("<br clear=\"all\" />");

						// Form to add item
						HTMLFunctions.StartForm(str, "Form1", "", "POST");

						HTMLFunctions.StartTable(str, "stats", "", 1, "", 0);
						HTMLFunctions.StartRow(str);
						HTMLFunctions.AddCellHeading(str, "Unit Name");
						HTMLFunctions.AddCellHeading(str, "Armour");
						HTMLFunctions.AddCellHeading(str, "Total<br />Weight");
						HTMLFunctions.AddCellHeading(str, "APs");
						HTMLFunctions.AddCellHeading(str, "Health");
						HTMLFunctions.AddCellHeading(str, "Shot Skill");
						HTMLFunctions.AddCellHeading(str, "CC Skill");
						HTMLFunctions.AddCellHeading(str, "Strength");
						if (game.isAdvancedMode()) {
							HTMLFunctions.AddCellHeading(str, "Energy");
							HTMLFunctions.AddCellHeading(str, "Morale");
						}
						HTMLFunctions.EndRow(str);

						ResultSet rs_units = dbs.getResultSet("SELECT * FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side + " ORDER BY OrderBy");
						String sql = "SELECT 0, '(none)' FROM ArmourTypes LIMIT 1 UNION SELECT ArmourTypeID, CONCAT(Name, ' (', Cost, ')') FROM ArmourTypes ";
						sql = sql + " WHERE 1=1 ";
						if (mission.getMaxProtection(our_side) > 0) {
							sql = sql + " AND Protection <= " + mission.getMaxProtection(our_side);
						}
						/*try {
							if (game.isCampGame()) {
								if (FactionsTable.IsBestFaction(dbs, this.current_login.getFactionID()) == false) {
									sql = sql + " AND COALESCE(RestrictedToWinners, 0) = 0";
								}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}*/

						int max_aps = dbs.getScalarAsInt("SELECT Max(MaxAPs)FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side);
						int max_health = dbs.getScalarAsInt("SELECT Max(MaxHealth)FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side);
						int max_shotskill = dbs.getScalarAsInt("SELECT Max(ShotSkill)FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side);
						int max_ccskill = dbs.getScalarAsInt("SELECT Max(CombatSkill)FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side);
						int max_strength = dbs.getScalarAsInt("SELECT Max(Strength)FROM Units WHERE CanDeploy = 1 AND CanUseEquipment = 1 AND CanEquip > 0 AND GameID = " + gameid + " AND Side = " + our_side);

						int num_units = 0;
						while (rs_units.next()) {
							num_units++;
							HTMLFunctions.StartRow(str);
							HTMLFunctions.AddCell(str, "<b>" + rs_units.getString("Name") + "</b>");
							HTMLFunctions.StartCell(str);
							HTMLFunctions.ComboBox(str, "armour_" + rs_units.getInt("UnitID"), "combobox", dbs.getResultSet(sql), dbs.getScalarAsInt("SELECT ArmourTypeID FROM Units WHERE UnitID = " + rs_units.getInt("UnitID")), false);
							HTMLFunctions.EndCell(str);
							HTMLFunctions.AddCell(str, EquipmentTable.GetWeightOfArmourAndEquipment(dbs, rs_units.getInt("UnitID")) + "");

							/*HTMLFunctions.AddCell(str, rs_units.getInt("MaxAPs") + "");
							HTMLFunctions.AddCell(str, rs_units.getInt("MaxHealth") + "");
							HTMLFunctions.AddCell(str, rs_units.getInt("ShotSkill") + "");
							HTMLFunctions.AddCell(str, rs_units.getInt("CombatSkill") + "");
							HTMLFunctions.AddCell(str, rs_units.getInt("Strength") + "");*/
							UnitsTable.AddStatCell(str, max_aps, rs_units.getInt("MaxAPs"));
							UnitsTable.AddStatCell(str, max_health, rs_units.getInt("MaxHealth"));
							UnitsTable.AddStatCell(str, max_shotskill, rs_units.getInt("ShotSkill"));
							UnitsTable.AddStatCell(str, max_ccskill, rs_units.getInt("CombatSkill"));
							UnitsTable.AddStatCell(str, max_strength, rs_units.getInt("Strength"));
							if (game.isAdvancedMode()) {
								HTMLFunctions.AddCell(str, rs_units.getInt("MaxEnergy") + "");
								HTMLFunctions.AddCell(str, rs_units.getInt("MaxMorale") + "");
							}

							HTMLFunctions.EndRow(str);
						}

						HTMLFunctions.EndTable(str);

						String cred_text = "Creds remaining: " + creds_remaining + " (approx. " + (creds_remaining/num_units) + " per unit";
						if (game.isCampGame()) {
							cred_text = cred_text + ", including " + camp_creds + " campaign credits";
						}
						cred_text = cred_text + ")";
						HTMLFunctions.Heading(str, 2, cred_text);

						HTMLFunctions.HiddenValue(str, "next", "buy");
						HTMLFunctions.SubmitButton(str, "Buy Armour");
						HTMLFunctions.EndForm(str);

						HTMLFunctions.Para(str, "<sup><a href=\"EquipUnits.cls?gid=" + game.getID() + "\">Don't buy armour and proceed straight to Buy Equipment</a></sup>");

						//str.append("<p></p>");

					} else {
						str.append("You have already equipped.");
					}
				} else {
					str.append("The game is not at the 'Equip Units' stage.");
				}
			} else {
				str.append("That is not your game.");
			}
			} finally {
				game.close();

			}
		}
		this.body_html.append(MainLayout.GetHTML(this, "Buy Armour", str));		
	}

}
