package dsrwebserver.pages.dsr;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import ssmith.dbs.MySQLConnection;
import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import ssmith.util.Interval;
import ssmith.util.MyList;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.FactionsTable;
import dsrwebserver.tables.GameRequestsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;

public class leaguetable extends AbstractHTMLPage {

	public static volatile boolean calculating = false;

	private static int PETERMOCK_ID = -1;

	public static final int DAYS = 730;
	private static final int C = 20;
	private Calendar last_calc_date;

	private static StringBuffer str_league;
	public static StringBuffer str_top10;
	private static Interval update_forum_interval = new Interval(Dates.HOUR*6, true);
	public static String top_player = "";

	public leaguetable() {
		super();
	}


	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		HTMLFunctions.StartUnorderedList(str);

		HTMLFunctions.AddListEntry(str, "Only non-practise games from the last " + DAYS + " days are used for points calculation.");
		HTMLFunctions.AddListEntry(str, "This game uses a version of the ELO rating system for points calculation.");
		//HTMLFunctions.AddListEntry(str, "The league table is calculated using every game ever played.");
		HTMLFunctions.EndUnorderedList(str);

		//HTMLFunctions.Heading(str, 3, DSRWebServer.TITLE + " League Table");

		if (calculating) {
			HTMLFunctions.Para(str, "Calculating.  Please try again soon.");
		} else {
			if (update_forum_interval.hitInterval() || str_league == null) {
				str_league = new StringBuffer();
				topPlayers(str_league);
			}
			str.append(str_league);
		}

		this.body_html.append(MainLayout.GetHTML(this, "League Table", str));		
	}


	public static String GetQual() {
		return "GameStatus = " + GamesTable.GS_FINISHED; // AND COALESCE(GameType, 0) = " + GameRequestsTable.GS_NORMAL + " DATEDIFF(NOW(), DateTurnStarted) < " + leaguetable.DAYS + " AND 
	}


	private void topPlayers(StringBuffer str) throws SQLException {
		try {
			calculating = true; 

			dbs.runSQLUpdate("UPDATE Logins SET ELOPoints = 0");

			PETERMOCK_ID = LoginsTable.GetIDFromDisplayName(dbs, "petermock");

			ResultSet rs_games = dbs.getResultSet("SELECT GameID FROM Games WHERE GameType <> " + GameRequestsTable.GT_PRACTISE  + " AND GameStatus = " + GamesTable.GS_FINISHED + " AND COALESCE(CampGame, 0) = 0 AND DATEDIFF(NOW(), DateFinished) < " + DAYS + " ORDER BY DateFinished");
			GamesTable game = new GamesTable(dbs);
			while (rs_games.next()) {
				/*if (rs_games.getInt("GameID") == 6383) {
					int dfgdfg = 456456;
				}*/
				game.selectRow(rs_games.getInt("GameID"));
				UpdateGame(dbs, game);
			}

			dbs.runSQLUpdate("UPDATE Logins SET ELOPoints = 0 WHERE LoginID = " + DSRWebServer.AI_LOGIN_ID);
			//dbs.runSQLUpdate("UPDATE Logins SET ELOPoints = ELOPoints - 577 WHERE DisplayName = 'petermock'");

			last_calc_date = Calendar.getInstance();

			this.showTable(str);
		} finally {
			calculating = false;
		}
	}


	public static void UpdateGame(MySQLConnection dbs, GamesTable game) throws SQLException {
		if (game.isPractise()) {
			return;
		}
		if (game.getNumOfSides() < 2) {
			return;
		}
		if (game.getNumOfSides() == 2) {
			if (game.isSideAI(1) || game.isSideAI(2)) {
				return;
			}
		}
		for (int s=1 ; s<=4 ; s++) {
			game.setELOPointsForSide(s, 0);
			game.setELORunningTotalForSide(s, 0);
		}
		if (game.getGameStatus() != GamesTable.GS_FINISHED) {
			return;
		}
		if (game.getWinType() == GamesTable.WIN_CANCELLED) {
			return; // Do nothing
		}
		if (game.getWinType() == GamesTable.WIN_DRAW_MUTUAL_CONCEDE) {
			return; // Do nothing
		}
		if (game.getWinType() == GamesTable.WIN_OPPONENT_CONCEDED) {
			if (game.getTurnNo() <= 1) {
				return; // Do nothing
			}
		}
		if (game.getWinningSides(null).size() > 0) {
			int winners_total_points = 0;
			int losers_total_points = 0;
			MyList<Integer> winners_sides = game.getWinningSides(null);
			for (int side=1 ; side<=game.getNumOfSides() ; side++) {
				if (winners_sides.contains(side)) {
					winners_total_points += dbs.getScalarAsInt("SELECT ELOPoints FROM Logins WHERE LoginID = " + game.getLoginIDFromSide(side));
				} else {
					losers_total_points += dbs.getScalarAsInt("SELECT ELOPoints FROM Logins WHERE LoginID = " + game.getLoginIDFromSide(side));
				}
			}

			// Don't lose so many points!
			winners_total_points = winners_total_points / winners_sides.size();
			losers_total_points = losers_total_points / (game.getNumOfSides() - winners_sides.size());

			for (int side=1 ; side<=game.getNumOfSides() ; side++) {
				int loginid = game.getLoginIDFromSide(side);
				int inc = 0;
				if (winners_sides.contains(side)) {
					inc = GetPointsForWinAgainst(winners_total_points, losers_total_points);
				} else {
					inc = GetPointsForLoseAgainst(losers_total_points, winners_total_points);
					/*if (game.getNumOfSides() > 2 && winners_sides.size() == 1) { // DOn't lose so many points
						inc = inc / (game.getNumOfSides()-1);
					}*/
				}
				if (loginid != PETERMOCK_ID || game.getID() >= 27535) {
					LoginsTable.IncELOPoints(dbs, loginid, inc);
				}
				game.setELOPointsForSide(side, inc);
				game.setELORunningTotalForSide(side, LoginsTable.GetELOPoints(dbs, loginid));
			}
			//}
		} else {
			// Draw?
			if (game.getWinType() == GamesTable.WIN_DRAW) {
				// Get the average number of points
				for (int s=1 ; s<=game.getNumOfSides() ; s++) {
					int tot = 0;
					for (int opp_side=1 ; opp_side<=game.getNumOfSides() ; opp_side++) {
						if (opp_side != s) {
							tot += LoginsTable.GetELOPoints(dbs, game.getLoginIDFromSide(opp_side));
						}
					}
					int avg = tot / (game.getNumOfSides()-1);

					int loginid = game.getLoginIDFromSide(s);
					int pts = GetPointsForDrawAgainst(LoginsTable.GetELOPoints(dbs, loginid), avg);
					if (loginid != PETERMOCK_ID || game.getID() >= 27535) {
						LoginsTable.IncELOPoints(dbs, loginid, pts);
					}
					game.setELOPointsForSide(s, pts);
					game.setELORunningTotalForSide(s, LoginsTable.GetELOPoints(dbs, loginid));
				}
			}
		}
	}


	private void showTable(StringBuffer str) throws SQLException {
		str_top10 = new StringBuffer();

		// Show top player
		//HTMLFunctions.Heading(str_league, 2, LeagueTableTable.GetTopPlayerName(dbs) + " is the current champion!");
		//top_player = e.name_no_link;
		//HTMLFunctions.Heading(str_league, 2, e.name_wlink + " is the current champion!");

		HTMLFunctions.Heading(str, 3, "Last calculated on " + Dates.FormatCalendar(this.last_calc_date, "dd MMM yyyy HH:mm"));


		HTMLFunctions.StartTable(str_league, "stats", "", 1, "", 5);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCellHeading(str, "Pos");
		HTMLFunctions.AddCellHeading(str, "Name");
		//HTMLFunctions.AddCellHeading(str, "Played");
		//HTMLFunctions.AddCellHeading(str, "Won");
		//HTMLFunctions.AddCellHeading(str, "Drawn");
		//HTMLFunctions.AddCellHeading(str, "Lost");
		HTMLFunctions.AddCellHeading(str, "Points");
		HTMLFunctions.AddCellHeading(str, "Faction");
		HTMLFunctions.EndRow(str);

		ResultSet rs = dbs.getResultSet("SELECT * FROM Logins WHERE TotalTurns > 0 AND ELOPoints <> 0 AND DATEDIFF(NOW(), LastLoginDate) < " + FactionsTable.EXPIRY_DAYS + " ORDER BY ELOPoints DESC LIMIT 100");
		int i=1;
		while (rs.next()) {
			//if (LoginsTable.IsAdmin(dbs, rs.getInt("LoginID")) == false) {
			//if (rs.getInt("LoginID") != DSRWebServer.JEZ_LOGIN_ID) { // Jez doesn't want to be on there
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, i);
			String name = LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginID"), true);
			HTMLFunctions.AddCell(str, name);
			HTMLFunctions.AddCell(str, rs.getInt("ELOPoints"));
			if (rs.getInt("FactionID") > 0) {
				HTMLFunctions.AddCell(str, "<a href=\"factions.cls?fid=" + rs.getInt("FactionID") + "\">" + FactionsTable.GetName(DSRWebServer.dbs, LoginsTable.GetFactionID(dbs, rs.getInt("LoginID"))) + "</a>");
			} else {
				HTMLFunctions.AddCell(str, "None");
			}
			HTMLFunctions.EndRow(str);

			if (i == 10) { // On 11th player, append it to another stringbuffer for showing on front page
				str_top10.append(str);
				HTMLFunctions.EndTable(str_top10);
			}
			i++;
			//}
		}
		HTMLFunctions.EndTable(str);

	}


	public static float GetExp(int score) {
		if (score < 0) {
			return 0.25f;
		} else {
			float ret =  0.5f + (score/500f);
			float MAX = 4f;
			if (ret > MAX) {
				return MAX;
			} else {
				return ret;
			}
		}
	}


	public static int GetPointsForWinAgainst(int our_pts, int their_pts) throws SQLException {
		int diff = their_pts - our_pts;

		int inc = (int)(C * GetExp(diff));

		return inc;

	}


	public static int GetPointsForLoseAgainst(int our_pts, int their_pts) throws SQLException {
		int diff = our_pts - their_pts;
		int inc = (int)(C * GetExp(diff));
		return -inc;///2;
	}


	public static int GetPointsForDrawAgainst(int our_pts,int their_pts) throws SQLException {
		int diff = their_pts - our_pts;
		int inc1 = (int)(C * GetExp(diff));
		int pts = inc1/3;
		return pts;
	}


}

