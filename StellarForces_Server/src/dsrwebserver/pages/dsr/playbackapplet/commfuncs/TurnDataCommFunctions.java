package dsrwebserver.pages.dsr.playbackapplet.commfuncs;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.io.TextFile;
import ssmith.util.MyList;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.appletcomm.PlaybackComms;
import dsrwebserver.pages.dsr.playbackapplet.PlaybackTurnData;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitHistoryTable;

public class TurnDataCommFunctions {

	// Called by the applet to request the data
	public static String GetTurnDataRequest(int game_id, int login_id, int turn) {
		return "cmd=" + PlaybackComms.GET_PLAYBACK_TURN_DATA + "&gid=" + game_id + "&lid=" + login_id + "&turn="+turn;
	}

	// --------------------------------------------------------------------

	// Called by the Server to send the data
	public static String GetTurnDataResponse(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException {
		int gameid = Integer.parseInt(hashmap.get("gid"));
		int loginid = Integer.parseInt(hashmap.get("lid"));
		int turn_no = Integer.parseInt(hashmap.get("turn"));

		GamesTable game = new GamesTable(dbs);
		try {
			game.selectRow(gameid);
			int side = 0;
			String filename = DSRWebServer.PLAYBACK_CACHE + "/turndata_" + gameid + "_" + turn_no + ".txt";
			if (game.getGameStatus() == GamesTable.GS_STARTED) {
				if (game.isPlayerInGame(loginid)) {
					side = game.getSideFromPlayerID(loginid);
				} else {
					return null; // Don't allow a non-logged in player to watch an unfinished game!
				}
			} else if (game.getGameStatus() == GamesTable.GS_FINISHED) {
				// Notice we only read from the cache if the game has finished!
				if (new File(filename).exists()) {
					try {
						return TextFile.ReadAll(filename, "\n", false);
					} catch (Exception e) {
						DSRWebServer.HandleError(e);
					}
				}
			}

			StringBuffer str = new StringBuffer();
			str.append(gameid + "|" + turn_no + "|");

			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM UnitHistory ");
			sql.append(" WHERE GameID = " + gameid + " AND TurnNo = " + turn_no);
			if (game.getGameStatus() == GamesTable.GS_STARTED) { // Game not finished yet!
				AbstractMission mission = AbstractMission.Factory(game.getMissionID());
				try {
					sql.append(" AND (");
					MyList<Integer> list = mission.getSidesForSide(side);
					for (Integer i : list) {
						sql.append(" SeenBySide" + i + " = 1 OR ");
					}
					sql.delete(sql.length()-3, sql.length());
					sql.append(")");
				} catch (Exception ex) {
					ex.printStackTrace();
					//sql = sql + " AND SeenBySide" + side + " = 1";
				}
				// Don't show computers if snafu
				if (mission.isSnafu()) {
					sql.append(" AND (EventType != " + UnitHistoryTable.UH_COMPUTER_DESTROYED + ")");
				}
			}

			//sql = sql + " ORDER BY DateCreated";
			sql.append(" ORDER BY TurnNo, EventTime, DateCreated");
			ResultSet rs = dbs.getResultSet(sql.toString());
			while (rs.next()) {
				str.append(rs.getInt("UnitID") + "|" + rs.getInt("Status") + "|");
				str.append(rs.getInt("MapX") + "|" + rs.getInt("MapY") + "|");
				str.append(rs.getInt("Angle") + "|" + rs.getInt("EventType") + "|");
				str.append(rs.getInt("Radius") + "|");
			}

			if (game.getGameStatus() == GamesTable.GS_FINISHED) {
				try {
					TextFile.QuickWrite(filename, str.toString(), false);
				} catch (Exception e) {
					DSRWebServer.HandleError(e);
				}
			}
			return str.toString();
		} finally {
			game.close();

		}
	}

	// --------------------------------------------------------------------

	// Called by the applet to decode the data
	public static ArrayList<PlaybackTurnData> DecodeResponse(String response) {
		//System.out.println(response);
		String data[] = response.split("\\|");
		//System.out.println(data);
		ArrayList<PlaybackTurnData> al_data = new ArrayList<PlaybackTurnData>();

		if (data.length > 2) {
			int cell = 2;
			while (true) {
				PlaybackTurnData turn_data = new PlaybackTurnData();
				turn_data.unit_id = Integer.parseInt(data[cell]);
				turn_data.status = Integer.parseInt(data[cell+1]);
				turn_data.mapx = Byte.parseByte(data[cell+2]);
				if (turn_data.mapx < 0) { // Handle small bug
					turn_data.mapx = 0;
				}
				turn_data.mapy = Byte.parseByte(data[cell+3]);
				if (turn_data.mapy < 0) { // Handle small bug
					turn_data.mapy = 0;
				}
				turn_data.angle = Integer.parseInt(data[cell+4]);
				turn_data.event_type = Integer.parseInt(data[cell+5]);
				turn_data.rad = Integer.parseInt(data[cell+6]);

				cell += 7;

				al_data.add(turn_data);

				if (cell >= data.length-5) {
					break;
				}
			}
		}

		return al_data;

	}

}
