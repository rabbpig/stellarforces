package dsrwebserver.pages.dsr.playbackapplet.commfuncs;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.io.TextFile;
import ssmith.lang.Functions;
import dsr.data.UnitData;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.ServerMapSquare;
import dsrwebserver.pages.appletcomm.PlaybackComms;
import dsrwebserver.pages.dsr.playbackapplet.PlayBackGameData;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public class GameDataCommFunctions {

	// Called by the applet to request the data
	public static String GetGameDataRequest(int game_id) {
		return "cmd=" + PlaybackComms.GET_PLAYBACK_GAME_DATA + "&gid=" + game_id;
	}

	// --------------------------------------------------------------------


	// Called by the Server to send the data
	public static String GetGameDataResponse(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException {
		StringBuffer str = new StringBuffer();

		int gid = Integer.parseInt(hashmap.get("gid"));
		GamesTable game = new GamesTable(dbs);
		try {
		String filename = DSRWebServer.PLAYBACK_CACHE + "/gamedata_" + gid + ".txt";
		if (game.doesRowExist(gid)) {
			if (game.getGameStatus() == GamesTable.GS_FINISHED) {
				if (new File(filename).exists()) {
					try {
						return TextFile.ReadAll(filename, "\n", false);
					} catch (Exception e) {
						DSRWebServer.HandleError(e);
					}
				}
			}
			game.selectRow(gid);
			if (game.getGameStatus() == GamesTable.GS_FINISHED) { // Only view finished games for now
				// No probs
			} else if (game.getGameStatus() == GamesTable.GS_STARTED) { // Only view finished games for now
				// No probs
			} else {
				DSRWebServer.p("Game " + gid + " not started yet!");
				game.close();
				return null;
			}
			int max_turns = dbs.getScalarAsInt("SELECT MAX(TurnNo) FROM UnitHistory WHERE GameID = " + gid);

			str.append(max_turns + "|");
			str.append(dbs.getScalarAsInt("SELECT Count(*) FROM UnitHistory WHERE GameID = " + gid) + "|");
			str.append(game.getNumOfSides() + "|");
			str.append(dbs.getScalarAsInt("SELECT Width FROM MapData WHERE MapDataID = " + game.getMapDataID()) + "|");
			str.append(dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + gid) + "|");

			// Get Units
			ResultSet rs = dbs.getResultSet("SELECT * FROM Units WHERE GameID = " + gid + " ORDER BY Side, OrderBy");
			while (rs.next()) {
				str.append(rs.getInt("UnitID") + "|" + rs.getInt("Side") + "|" + rs.getInt("OrderBy") + "|" + rs.getInt("MapX") + "|" + rs.getInt("MapY") + "|" + rs.getInt("Angle") + "|");
			}

			str.append("start_of_map1|");
			// Map data
			int c = dbs.getScalarAsInt("SELECT Count(*) FROM MapDataSquares WHERE MapDataID = " + game.getMapDataID() + " AND (SquareType IN (" + MapDataTable.MT_WALL + ", " + MapDataTable.MT_COMPUTER + ", " + MapDataTable.MT_NOTHING + ") OR DoorType > 0)");
			str.append(c + "|");
			String sql = "SELECT MapX, MapY, SquareType, DoorType FROM MapDataSquares WHERE MapDataID = " + game.getMapDataID() + " AND (SquareType IN (" + MapDataTable.MT_WALL + ", " + MapDataTable.MT_COMPUTER + ", " + MapDataTable.MT_NOTHING + ") OR DoorType > 0)";
			ResultSet rs_map = dbs.getResultSet(sql); 
			while (rs_map.next()) {
				int x = rs_map.getInt("MapX");
				int y = rs_map.getInt("MapY");
				str.append(x + "|" + y + "|" + rs_map.getInt("SquareType") + "|" + rs_map.getInt("DoorType") + "|");
			}

			// Destroyed map squares
			str.append("start_of_map2|");
			c = dbs.getScalarAsInt("SELECT Count(*) FROM UnitHistory WHERE EventType = " + UnitHistoryTable.UH_WALL_DESTROYED + " AND GameID = " + game.getID() + " AND OriginalSquareType = " + MapDataTable.MT_WALL);
			str.append(c + "|");
			rs = dbs.getResultSet("SELECT * FROM UnitHistory WHERE EventType = " + UnitHistoryTable.UH_WALL_DESTROYED + " AND GameID = " + game.getID() + " AND OriginalSquareType = " + MapDataTable.MT_WALL);
			while (rs.next()) {
				int x = rs.getInt("MapX");
				int y = rs.getInt("MapY");
				str.append(x + "|" + y + "|");
			}
			// Player names
			for (int s=1 ; s<=game.getNumOfSides() ; s++) {
				int loginid = game.getLoginIDFromSide(s);
				if (loginid > 0) {
					str.append(LoginsTable.GetDisplayName(dbs, loginid) + "|");
				}
			}
			for (int s=game.getNumOfSides()+1 ; s<=4 ; s++) {
				str.append("|");
			}

			if (game.getGameStatus() == GamesTable.GS_FINISHED) {
				try {
					TextFile.QuickWrite(filename, str.toString(), false);
				} catch (Exception e) {
					DSRWebServer.HandleError(e);
				}
			}
		} else {
			DSRWebServer.p("Game " + gid + " does not exist!");
		}
		} finally {
			game.close();
		}
		return str.toString();
	}


	// --------------------------------------------------------------------

	// Called by the applet to decode the data
	public static PlayBackGameData DecodeResponse(String response) {
		if (response.length() > 0) {
			PlayBackGameData game_data = new PlayBackGameData();
			String data[] = response.split("\\|");
			game_data.max_turns = Functions.ParseInt(data[0]);
			game_data.max_events = Functions.ParseInt(data[1]);
			game_data.total_sides = Functions.ParseInt(data[2]);
			game_data.map_width = Functions.ParseInt(data[3]);
			game_data.units = new UnitData[Functions.ParseInt(data[4])];
			int cell = 5;

			for (int i=0 ; i<game_data.units.length ; i++) {
				game_data.units[i] = new UnitData(Functions.ParseInt(data[cell+2]));
				game_data.units[i].setStatus(UnitsTable.ST_AWAITING_DEPLOYMENT);
				game_data.units[i].unitid = Functions.ParseInt(data[cell]);
				game_data.units[i].setSide(Functions.ParseByte(data[cell+1]));
				game_data.units[i].order_by = Functions.ParseByte(data[cell+2]);
				game_data.units[i].map_x = Functions.ParseByte(data[cell+3]);
				game_data.units[i].map_z = Functions.ParseByte(data[cell+4]);
				game_data.units[i].angle= Functions.ParseInt(data[cell+5]);
				cell += 6;
			}

			game_data.map = new ServerMapSquare[game_data.map_width][game_data.map_width];
			for (byte y=0 ; y<game_data.map_width ; y++) {
				for (byte x=0 ; x<game_data.map_width ; x++) {
					game_data.map[x][y] = new ServerMapSquare(MapDataTable.MT_FLOOR, x, y, (byte)0, false); // Default to floor
				}			
			}

			// Walls
			int squares = Functions.ParseInt(data[cell+1]);
			cell+=2;
			for (int i=0 ; i<squares ; i++) {
				try {
					int x = Functions.ParseInt(data[cell]);
					int y = Functions.ParseInt(data[cell+1]);
					game_data.map[x][y].major_type = Functions.ParseByte(data[cell+2]);
					game_data.map[x][y].door_type = Functions.ParseByte(data[cell+3]);
					cell += 4;
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

			// Destroyed walls
			squares = Functions.ParseInt(data[cell+1]);
			cell+=2;
			for (int i=0 ; i<squares ; i++) {
				game_data.map[Functions.ParseInt(data[cell])][Functions.ParseInt(data[cell+1])].major_type = MapDataTable.MT_WALL;
				cell += 2;
			}

			return game_data;
		} else {
			return null;
		}
	}

}
