package dsrwebserver.pages.dsr.playbackapplet;

import java.applet.Applet;

import dsr.comms.WGet4Client;
import dsrwebserver.pages.dsr.playbackapplet.commfuncs.GameDataCommFunctions;

public class GetGameDataThread extends Thread {
	
	public PlayBackGameData game_data;
	private int game_id;
	private Applet app;
	
	public GetGameDataThread(Applet _app, int gid) {
		super("UpdateDataThread");
		this.setDaemon(true);
		
		app = _app;
		game_id = gid;
		
		start();
	}
	
	public void run() {
		try {
			WGet4Client wg = new WGet4Client(app, GameDataCommFunctions.GetGameDataRequest(game_id), true);
			String response = wg.getResponse();
			game_data = GameDataCommFunctions.DecodeResponse(response);
			
		} catch (Exception ex) {
			PlaybackApplet.HandleError(ex);
		}
	}

}
