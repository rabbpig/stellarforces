package dsrwebserver.pages.dsr.playbackapplet;

import java.awt.BasicStroke;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import dsr.data.UnitData;
import dsrwebserver.pages.dsr.MapImageAbstract;
import dsrwebserver.pages.dsr.playbackapplet.eventgraphics.AbstractEventGraphic;
import dsrwebserver.pages.dsr.playbackapplet.eventgraphics.ExplosionGraphic;
import dsrwebserver.pages.dsr.playbackapplet.eventgraphics.ShootingGraphic;
import dsrwebserver.pages.dsr.playbackapplet.eventgraphics.ThrowingGraphic;
import dsrwebserver.pages.dsr.playbackapplet.eventgraphics.UnitKilledGraphic;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

import ssmith.applet.AppletFramework;
import ssmith.lang.Functions;
import ssmith.util.Interval;

public class PlaybackApplet extends AppletFramework {

	public static final int ARENA_WIDTH = 650, ARENA_HEIGHT=650;
	private static final long LOOP_DELAY = 35;
	private static final long EVENT_DELAY = 100;
	public static Font font_normal = new Font("Impact", Font.PLAIN, 18);
	private static final BasicStroke stroke = new BasicStroke(1);

	// Stages
	private static final int GETTING_GAME_DATA = 0;
	private static final int READY = 1;

	private static final long serialVersionUID = 1L;

	private Point mouse_pos = new Point();
	private int stage;
	private int game_id = -1, login_id = 2;
	private PlayBackGameData game_data = null;
	private GetEventDataThread event_data_thread = null;
	private int event_data_pos;
	private Interval next_event_int = new Interval(EVENT_DELAY, false);
	private static String error;
	private PlaybackIcon icon_pause, icon_restart, icon_ffwd, icon_super_ffwd, icon_rew, icon_slowmo, icon_play;
	private int sq_size = -1;
	private boolean paused = false, slowmo = false;
	private GetGameDataThread game_data_thread;
	private int ffwd_count = 0, rewind_count = 0;
	private ArrayList<AbstractEventGraphic> graphics;
	private boolean run_to_end = false;


	public PlaybackApplet() {
		super(ARENA_WIDTH, ARENA_HEIGHT);

	}


	public void init() {
		super.init();

		game_id = 5574; //Default
		login_id = 3; // default
		try {
			String s_gid = this.getParameter("gid");
			String s_lid = this.getParameter("lid");
			game_id = Integer.parseInt(s_gid);
			login_id = Integer.parseInt(s_lid);
			System.out.println("GID:" + game_id);
			System.out.println("LID:" + login_id);
		} catch (Exception e) {
			e.printStackTrace();
			//PlaybackApplet.HandleError(e);
		}

		t.start();
	}


	private void startFromBeginning() {
		stage = GETTING_GAME_DATA;
		event_data_pos = 0;
		graphics = new ArrayList<AbstractEventGraphic>();
		game_data_thread = new GetGameDataThread(this, game_id);
	}


	public void mouseMoved(MouseEvent evt) {
		mouse_pos.x = evt.getX();
		mouse_pos.y = evt.getY();
	}


	public void run() {
		startFromBeginning();

		try {

			while (!stop_now) {
				long start = System.currentTimeMillis();

				// Check for input
				while (this.input_msgs.size() > 0) {
					Object o = this.input_msgs.get(0);
					if (o instanceof MouseEvent) {
						MouseEvent evt = (MouseEvent)o;
						if (evt.getID() == MouseEvent.MOUSE_RELEASED) {
							if (this.icon_pause.contains(evt.getX(), evt.getY())) {
								paused = true;
								this.run_to_end = false;
							} else if (this.icon_play.contains(evt.getX(), evt.getY())) {
								paused = false;
								this.rewind_count = 0;
								this.ffwd_count = 0;
								this.run_to_end = false;
								this.setSlowMo(false);
							} else if (this.icon_restart.contains(evt.getX(), evt.getY())) {
								this.startFromBeginning();
								this.paused = false;
								this.run_to_end = false;
							} else if (this.icon_ffwd.contains(evt.getX(), evt.getY())) {
								this.ffwd_count = 20;
								this.rewind_count = 0;
								this.paused = false;
								this.setSlowMo(false);
								this.run_to_end = false;
							} else if (this.icon_super_ffwd.contains(evt.getX(), evt.getY())) {
								this.ffwd_count = 20;
								this.rewind_count = 0;
								this.paused = false;
								this.setSlowMo(false);
								this.run_to_end = true;
							} else if (this.icon_rew.contains(evt.getX(), evt.getY())) {
								this.rewind_count = 20;
								this.ffwd_count = 0;
								this.paused = false;
								this.setSlowMo(false);
								this.run_to_end = false;
							} else if (this.icon_slowmo.contains(evt.getX(), evt.getY())) {
								this.paused = false;
								this.setSlowMo(!this.slowmo);
								this.run_to_end = false;
							}
						}
					} else if (o instanceof KeyEvent) {
						/*KeyEvent evt = (KeyEvent)o;
						if (evt.getKeyCode() == KeyEvent.VK_R || evt.getKeyCode() == KeyEvent.VK_S) {
						}*/
					} else {
						System.err.println("Unknown event type: " + o.toString());
					}

					if (this.input_msgs.size() > 0) {
						this.input_msgs.remove(0);
					}
				}

				// See if gamedata thread has finished
				if (stage == GETTING_GAME_DATA) {
					if (game_data_thread != null && game_data_thread.isAlive() == false) {
						this.game_data = game_data_thread.game_data;
						game_data_thread = null;
						if (game_data != null) {
							sq_size = ARENA_WIDTH / game_data.map_width;
							stage = READY;
							event_data_thread = new GetEventDataThread(this, game_id, login_id, game_data.max_turns);
							icon_restart = new PlaybackIcon(5, ARENA_HEIGHT - 35, "Restart");
							icon_rew = new PlaybackIcon(65, ARENA_HEIGHT - 35, "Rewind");
							icon_pause = new PlaybackIcon(125, ARENA_HEIGHT - 35, "Pause");
							icon_play = new PlaybackIcon(185, ARENA_HEIGHT - 35, "Play");
							icon_slowmo = new PlaybackIcon(245, ARENA_HEIGHT - 35, "Slow-Mo");
							icon_ffwd = new PlaybackIcon(305, ARENA_HEIGHT - 35, "FFwd");
							icon_super_ffwd = new PlaybackIcon(365, ARENA_HEIGHT - 35, "To End");
						} else {
							// No such game
						}
					}
				} else {
					if (!paused) {
						if (this.event_data_thread != null) {
							if (next_event_int.hitInterval() || ffwd_count > 0 || rewind_count > 0) {
								if (rewind_count > 0) {
									rewind_count--;
								} 
								while (event_data_pos < this.event_data_thread.al_event_data.size()) { // Have we got any more turn data to get?
									PlaybackTurnData turn_data = this.event_data_thread.al_event_data.get(event_data_pos);
									// process this piece of turn data
									boolean did_something_happen = processEvent(turn_data);
									if (did_something_happen) { // Update graphics
										break; // Something happened so drop out so we loop around the main loop again
									} else {
										this.nextEvent();
									}
								}
								if (event_data_pos >= this.event_data_thread.al_event_data.size()) {
									this.run_to_end = false;
								}
								this.updateGraphics();
								this.nextEvent();
							}
						}
					}
				}

				this.repaint();

				if (this.run_to_end == false) {
					long wait = LOOP_DELAY - System.currentTimeMillis() + start;
					Functions.delay(wait);
					/*} else {
					Functions.delay(1);
					Thread.yield();*/
				}
			}
		} catch (Exception e) {
			PlaybackApplet.HandleError(e);
		}
	}


	private void updateGraphics() {
		for (int i=0 ; i<this.graphics.size() ; i++) {
			AbstractEventGraphic current_event_graphic = this.graphics.get(i);
			current_event_graphic.timer--;
			if (current_event_graphic.timer <= 0) {
				this.graphics.remove(i);
				i--;
			}
		}
	}


	private void nextEvent() {
		if (rewind_count > 0 && event_data_pos > 0) {
			this.event_data_pos--;
		} else if (event_data_pos < this.event_data_thread.al_event_data.size()) {
			this.event_data_pos++;
		}

	}


	private void setSlowMo(boolean b) {
		this.slowmo = b;
		if (slowmo) {
			next_event_int.setInterval(EVENT_DELAY*5, true);
		} else {
			next_event_int.setInterval(EVENT_DELAY, true);
		}

	}


	/**
	 * Returns true if we pause after it.
	 */
	private boolean processEvent(PlaybackTurnData turn_data) {
		try {
			UnitData unit = null;
			switch (turn_data.event_type) {
			case UnitHistoryTable.UH_UNIT_MOVEMENT:
				unit = UnitData.GetUnitDataFromID(game_data.units, turn_data.unit_id);
				unit.map_x = turn_data.mapx;
				unit.map_z = turn_data.mapy;
				unit.angle = turn_data.angle;
				//if (unit.getStatus() == UnitsTable.ST_AWAITING_DEPLOYMENT) {
					unit.setStatus(UnitsTable.ST_DEPLOYED); // Just in case, and needed for Reanimator
				//}
				return true;
			case UnitHistoryTable.UH_EXPLOSION:
				this.graphics.add(new ExplosionGraphic(turn_data.mapx*sq_size, turn_data.mapy*sq_size, turn_data.rad*sq_size));
				return true;
			case UnitHistoryTable.UH_WALL_DESTROYED:
				game_data.map[turn_data.mapx][turn_data.mapy].destroyed = 1;
				return false;
			case UnitHistoryTable.UH_UNIT_KILLED:
				unit = UnitData.GetUnitDataFromID(game_data.units, turn_data.unit_id);
				unit.map_x = turn_data.mapx;
				unit.map_z = turn_data.mapy;
				unit.angle = turn_data.angle;
				if (rewind_count <= 1) {
					unit.setStatus(UnitsTable.ST_DEAD);
					this.graphics.add(new UnitKilledGraphic(turn_data.mapx*sq_size, turn_data.mapy*sq_size, sq_size));
				} else { // Going backwards to make alive
					unit.setStatus(UnitsTable.ST_DEPLOYED);
				}
				return true;
			case UnitHistoryTable.UH_SHOT_FIRED:
				unit = UnitData.GetUnitDataFromID(this.game_data.units, turn_data.unit_id);
				// Update the unit
				unit.map_x = turn_data.mapx;
				unit.map_z = turn_data.mapy;
				int ang = turn_data.angle;
				if (ang == 0) {
					ang = unit.angle;
				}
				int length = turn_data.rad;
				if (length == 0) {
					length = 2; // Default
				}
				length = length * sq_size;
				this.graphics.add(new ShootingGraphic(turn_data.mapx*sq_size+(sq_size/2), turn_data.mapy*sq_size+(sq_size/2), ang, length));
				return true;
			case UnitHistoryTable.UH_ITEM_THROWN:
				unit = UnitData.GetUnitDataFromID(this.game_data.units, turn_data.unit_id);
				// Update the unit
				unit.map_x = turn_data.mapx;
				unit.map_z = turn_data.mapy;
				ang = turn_data.angle;
				if (ang == 0) {
					ang = unit.angle;
				}
				length = turn_data.rad;
				if (length == 0) {
					length = 2; // Default
				}
				if (length < 60) { // Since the Android client was sending length in Pixels
					length = length * sq_size;
				}
				this.graphics.add(new ThrowingGraphic(turn_data.mapx*sq_size+(sq_size/2), turn_data.mapy*sq_size+(sq_size/2), ang, length));
				return true;
			case UnitHistoryTable.UH_UNIT_DEPLOYED:
				unit = UnitData.GetUnitDataFromID(game_data.units, turn_data.unit_id);
				unit.map_x = turn_data.mapx;
				unit.map_z = turn_data.mapy;
				unit.angle = turn_data.angle;
				unit.setStatus(UnitsTable.ST_DEPLOYED);
				return true;
			case UnitHistoryTable.UH_UNIT_ESCAPED:
				unit = UnitData.GetUnitDataFromID(game_data.units, turn_data.unit_id);
				if (rewind_count <= 1) {
					unit.setStatus(UnitsTable.ST_ESCAPED);
				} else {// Going backwards
					unit.map_x = turn_data.mapx;
					unit.map_z = turn_data.mapy;
					unit.angle = turn_data.angle;
					unit.setStatus(UnitsTable.ST_DEPLOYED);
				}
				return true;
				/*case UnitHistoryTable.UH_SEEN_CLOSE_COMBAT:
			return false;
		case UnitHistoryTable.UH_SEEN_ITEM_THROWN:
			return false;*/
			case UnitHistoryTable.UH_COMPUTER_DESTROYED:
				if (rewind_count <= 1) {
					game_data.map[turn_data.mapx][turn_data.mapy].destroyed = 1;
				} else {// Going backwards
					game_data.map[turn_data.mapx][turn_data.mapy].destroyed = 0;
				}
				return true;
			case UnitHistoryTable.UH_FLAG_LOCATION:
				return false;
			case UnitHistoryTable.UH_DOOR_OPENED:
				return false;
			case UnitHistoryTable.UH_DOOR_CLOSED:
				return false;
			case UnitHistoryTable.UH_GRENADE_PRIMED:
				return false;
			case UnitHistoryTable.UH_BLOB_SPLIT:
				return false;
			case UnitHistoryTable.UH_SMOKE_CREATED:
				game_data.map[turn_data.mapx][turn_data.mapy].smoke_type = EquipmentTypesTable.ET_SMOKE_GRENADE;
				return false;
			case UnitHistoryTable.UH_SMOKE_REMOVED:
				game_data.map[turn_data.mapx][turn_data.mapy].smoke_type = -1;
				return false;
			case UnitHistoryTable.UH_NERVE_GAS_CREATED:
				game_data.map[turn_data.mapx][turn_data.mapy].smoke_type = EquipmentTypesTable.ET_NERVE_GAS;
				return false;
			case UnitHistoryTable.UH_NERVE_GAS_REMOVED:
				game_data.map[turn_data.mapx][turn_data.mapy].smoke_type = -1;
				return false;
			case UnitHistoryTable.UH_FIRE_CREATED:
				game_data.map[turn_data.mapx][turn_data.mapy].smoke_type = EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE;
				return false;
			case UnitHistoryTable.UH_FIRE_REMOVED:
				game_data.map[turn_data.mapx][turn_data.mapy].smoke_type = -1;
				return false;
			case UnitHistoryTable.UH_MAPSQUARE_CHANGED:
				game_data.map[turn_data.mapx][turn_data.mapy].major_type = turn_data.rad;
				return true;
			default:
				//throw new RuntimeException("Unknown event type:" + turn_data.event_type);
				System.err.println("Unknown event type: " + turn_data.event_type);
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}

	}


	public void update(Graphics g2) {
		try {
			Graphics g = this.img_back.getGraphics();

			Graphics2D g2d = (Graphics2D)g;

			g.setColor(Color.black);
			g.fillRect(0, 0, ARENA_WIDTH, ARENA_HEIGHT);

			if (stage == GETTING_GAME_DATA) {
				g.setColor(Color.white);
				g.drawString("Please wait - getting game data...", 10, 40);
			} else {
				if (this.run_to_end == false) {
					this.drawMap(g);
					this.drawUnits(g);
					for (int i=0 ; i<this.graphics.size() ; i++) {
						AbstractEventGraphic current_event_graphic = this.graphics.get(i);
						current_event_graphic.paint(g);
					}
				}

				// Draw text
				g.setColor(Color.white);
				if (event_data_thread.al_event_data.size() < this.game_data.max_events) {
					g.drawString("Events downloaded: " + event_data_thread.al_event_data.size() + "/" + this.game_data.max_events, 10, 40);
				}
				if (this.paused) {
					g.drawString("PAUSED", 10, 60);
				} else if (event_data_pos >= this.event_data_thread.al_event_data.size() && this.event_data_thread.al_event_data.size() > 0) { // Have we reached the end?
					g.drawString("THE END", 10, 60);
					/*} else if (slowmo) {
					g.drawString("Slow-Mo", 10, 60);*/
				} else {
					g.drawString("Now showing event " + event_data_pos + "/" + this.event_data_thread.al_event_data.size(), 10, 60);
				}
			}

			g.setColor(Color.white);
			g.drawString("Game " + game_id + " selected.", 10, 20);
			if (error != null) {
				g.drawString(error, 10, 80);
			}

			// Draw icons
			g2d.setStroke(stroke);
			if (icon_pause != null) {
				icon_pause.paint(g);
			}
			if (icon_play != null) {
				icon_play.paint(g);
			}
			if (icon_restart != null) {
				icon_restart.paint(g);
			}
			if (icon_ffwd != null) {
				icon_ffwd.paint(g);
			}
			if (icon_super_ffwd != null) {
				icon_super_ffwd.paint(g);
			}
			if (icon_slowmo != null) {
				icon_slowmo.paint(g);
			}
			if (icon_rew != null) {
				icon_rew.paint(g);
			}

			g2.drawImage(this.img_back, 0, 0, this);
		} catch (Exception e) {
			PlaybackApplet.HandleError(e);
		}
	}


	private void drawMap(Graphics g) {
		int size = game_data.map.length; 

		for (int y=0 ; y<size ; y++) {
			for (int x=0 ; x<size ; x++) {
				if (game_data.map[x][y].smoke_type == EquipmentTypesTable.ET_SMOKE_GRENADE) {
					g.setColor(MapImageAbstract.SMOKE);
					g.drawRect(x * sq_size, y * sq_size, sq_size-1, sq_size-1);
				} else if (game_data.map[x][y].smoke_type == EquipmentTypesTable.ET_NERVE_GAS) {
					g.setColor(MapImageAbstract.NERVE_GAS);
					g.drawRect(x * sq_size, y * sq_size, sq_size-1, sq_size-1);
				} else if (game_data.map[x][y].smoke_type == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
					g.setColor(MapImageAbstract.FIRE);
					g.drawRect(x * sq_size, y * sq_size, sq_size-1, sq_size-1);
				}
				if (game_data.map[x][y].major_type == MapDataTable.MT_WALL) {
					if (game_data.map[x][y].destroyed == 0) {
						g.setColor(MapImageAbstract.GREEN_FLOOR);
						g.fillRect((x * sq_size), (y * sq_size), sq_size-1, sq_size-1);
					}
				} else if (game_data.map[x][y].major_type == MapDataTable.MT_NOTHING) {
					g.setColor(Color.DARK_GRAY);
					g.fillRect((x * sq_size), (y * sq_size), sq_size, sq_size);
				} else if (game_data.map[x][y].major_type == MapDataTable.MT_COMPUTER) {
					if (game_data.map[x][y].destroyed == 0) {
						g.setColor(MapImageAbstract.OPP_COMPUTER_COL);
					} else {
						g.setColor(MapImageAbstract.OPP_DESTROYED_COMPUTER_COL);
					}
					g.fillRect((x * sq_size), (y * sq_size), sq_size-1, sq_size-1);
				} else if (game_data.map[x][y].door_type > 0) {
					g.setColor(Color.DARK_GRAY);
					g.fillRect((x * sq_size), (y * sq_size), sq_size-1, sq_size-1);
				}
			}
		}
	}


	private void drawUnits(Graphics g) {
		// Draw units
		for (int i=0 ; i<this.game_data.units.length ; i++) {
			UnitData unit = this.game_data.units[i];
			g.setColor(MapImageAbstract.GetColourForSide(unit.getSide()));
			if (unit.getStatus() == UnitsTable.ST_DEAD) {
				g.setColor(g.getColor().darker());
				g.drawOval(unit.map_x * sq_size, unit.map_z * sq_size, sq_size, sq_size);
			} else if (unit.getStatus() == UnitsTable.ST_DEPLOYED) {
				g.fillOval(unit.map_x * sq_size, unit.map_z * sq_size, sq_size, sq_size);
				// Draw direction
				g.setColor(Color.gray);
				int sx = unit.map_x * sq_size + (sq_size/2);
				int sy = unit.map_z * sq_size + (sq_size/2);
				g.drawLine(sx, sy, sx+MapImageAbstract.GetXOffSetFromAngle(unit.angle, sq_size), sy+MapImageAbstract.GetYOffSetFromAngle(unit.angle, sq_size));
			} else {
				continue; // Don't draw if escaped
			}

			// Draw black border to numbers
			g.setColor(Color.black);
			g.drawString("" + unit.order_by, (unit.map_x * sq_size) + 2, (unit.map_z * sq_size) + 12);
			g.drawString("" + unit.order_by, (unit.map_x * sq_size) + 4, (unit.map_z * sq_size) + 12);
			g.drawString("" + unit.order_by, (unit.map_x * sq_size) + 3, (unit.map_z * sq_size) + 11);
			g.drawString("" + unit.order_by, (unit.map_x * sq_size) + 3, (unit.map_z * sq_size) + 13);

			g.setColor(Color.white);
			g.drawString("" + unit.order_by, (unit.map_x * sq_size) + 3, (unit.map_z * sq_size) + 12);
		}
	}


	public static void HandleError(Exception ex) {
		ex.printStackTrace();
		error = ex.toString();
	}


}
