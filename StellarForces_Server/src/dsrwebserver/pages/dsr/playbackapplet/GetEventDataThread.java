package dsrwebserver.pages.dsr.playbackapplet;

import java.applet.Applet;
import java.util.ArrayList;

import ssmith.lang.Functions;
import dsr.comms.WGet4Client;
import dsrwebserver.pages.dsr.playbackapplet.commfuncs.TurnDataCommFunctions;

public class GetEventDataThread extends Thread {

	public ArrayList<PlaybackTurnData> al_event_data = new ArrayList<PlaybackTurnData>();
	private int max_turns, game_id, login_id, turn_no = 0;
	private Applet app;

	public GetEventDataThread(Applet _app, int gid, int lid, int _max_turns) {
		super("GetEventDataThread");

		app = _app;
		game_id = gid;
		login_id = lid;
		max_turns = _max_turns;

		this.setDaemon(true);
		start();
	}


	public void run() {
		try {
			while (turn_no <= max_turns) {
				WGet4Client wg = new WGet4Client(app, TurnDataCommFunctions.GetTurnDataRequest(game_id, login_id, turn_no), true);
				String response = wg.getResponse();
				ArrayList<PlaybackTurnData> turn_data = TurnDataCommFunctions.DecodeResponse(response);
				al_event_data.addAll(turn_data);

				Functions.delay(2000);
				turn_no++; // Get turn 0 first just in case
			}
		} catch (Exception ex) {
			PlaybackApplet.HandleError(ex);
		}
	}

}
