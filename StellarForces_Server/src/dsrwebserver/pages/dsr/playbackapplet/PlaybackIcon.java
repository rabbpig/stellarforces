package dsrwebserver.pages.dsr.playbackapplet;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class PlaybackIcon extends Rectangle {
	
	private static final long serialVersionUID = 1L;

	private static final int DEF_HEIGHT = 20;
	private static final int DEF_WIDTH = 50;
	
	private String text;
	
	public PlaybackIcon(int x, int y, String _text) {
		super(x, y, DEF_WIDTH, DEF_HEIGHT);
		
		text = _text;
	}
	
	
	public void paint(Graphics g) {
		/*if (this.width <= 0) {
			FontMetrics fm = g.getFontMetrics();
			this.width = fm.stringWidth(text) + 10;
			
		}*/
		g.setColor(Color.white);
		g.fillRoundRect(x, y, width, height, 5, 5);
		g.setColor(Color.black);
		g.drawString(text, x+5, y+14);
	}

}
