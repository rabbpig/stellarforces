package dsrwebserver.pages.dsr.playbackapplet.eventgraphics;

import java.awt.Color;
import java.awt.Graphics;

public class ThrowingGraphic extends AbstractEventGraphic {
	
	private static final long serialVersionUID = 1L;
	
	private int x_off, y_off;
	
	public ThrowingGraphic(int x, int y, int ang, int len) {
		super(x, y, 20);
		
		x_off = (int)(Math.cos(Math.toRadians(ang)) * len);
		y_off = (int)(Math.sin(Math.toRadians(ang)) * len);
		//y_off = MapImageAbstract.GetYOffSetFromAngle(_ang, len);
	}

	
	@Override
	public void paint(Graphics g) {
		g.setColor(Color.cyan);
		g.drawLine(x, y, x+x_off, y+y_off);
	}

}
