package dsrwebserver.pages.dsr.playbackapplet.eventgraphics;

import java.awt.Color;
import java.awt.Graphics;

public class ExplosionGraphic extends AbstractEventGraphic {
	
	private static final long serialVersionUID = 1L;
	
	private int rad;
	
	public ExplosionGraphic(int x, int y, int _rad) {
		super(x, y, 10);
		
		rad = _rad;
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.red);
		g.fillOval(x-rad, y-rad, rad*2, rad*2);
	}

}
