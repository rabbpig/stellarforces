package dsrwebserver.pages.dsr.playbackapplet.eventgraphics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class ShootingGraphic extends AbstractEventGraphic {
	
	private static final BasicStroke stroke = new BasicStroke(3);
	
	private static final long serialVersionUID = 1L;
	
	private int x_off, y_off;
	
	public ShootingGraphic(int x, int y, int ang, int len) {
		super(x, y, 20);
		
		x_off = (int)(Math.cos(Math.toRadians(ang)) * len);
		y_off = (int)(Math.sin(Math.toRadians(ang)) * len);
		//y_off = MapImageAbstract.GetYOffSetFromAngle(_ang, len);
	}

	@Override
	public void paint(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		g2.setStroke(stroke);
		g2.setColor(Color.yellow);
		g2.drawLine(x, y, x+x_off, y+y_off);
	}

}
