package dsrwebserver.pages.dsr.playbackapplet.eventgraphics;

import java.awt.Graphics;
import java.awt.Point;

public abstract class AbstractEventGraphic extends Point {
	
	private static final long serialVersionUID = 1L;
	
	public int timer;
	
	public AbstractEventGraphic(int x, int y, int tmr) {
		super(x, y);
		timer = tmr;
	}

	public abstract void paint(Graphics g);
}
