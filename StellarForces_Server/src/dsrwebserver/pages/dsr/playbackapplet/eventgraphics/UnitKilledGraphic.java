package dsrwebserver.pages.dsr.playbackapplet.eventgraphics;

import java.awt.Color;
import java.awt.Graphics;

public class UnitKilledGraphic extends AbstractEventGraphic {
	
	private static final long serialVersionUID = 1L;
	
	private int sz;
	
	public UnitKilledGraphic(int x, int y, int _sz) {
		super(x, y, 10);
		
		sz = _sz;
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(Color.magenta);
		g.fillRect(x, y, sz, sz);
	}

}
