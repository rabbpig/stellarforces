package dsrwebserver.pages.dsr.playbackapplet;

import ssmith.html.HTMLFunctions;
import ssmith.image.ImageFunctions;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.pages.dsr.MapImageAbstract;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;

public class playbackpage extends AbstractHTMLPage {

	public playbackpage() {
		super();
	}

	
	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		int gid =  this.headers.getGetValueAsInt("gid");

		GamesTable game = new GamesTable(dbs);
		try {
		if (game.doesRowExist(gid)) {
			game.selectRow(gid);
			
			if (game.getGameStatus() == GamesTable.GS_STARTED) {
				if (this.session.isLoggedIn() == false) {
					return;
				}
			} else if (game.getGameStatus() == GamesTable.GS_FINISHED) {
				// Do nothing
			} else {
				return;
			}

			AbstractMission mission = AbstractMission.Factory(game.getMissionID());
			String opponents = game.getOpponentsNamesBySide(-1, false);
			HTMLFunctions.Heading(str, 2, opponents + " in " + mission.mission_name);

			// Draw players table at top
			HTMLFunctions.StartTable(str, "stats", "", 0, "", 5);
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCellHeading(str, "Side");
			HTMLFunctions.AddCellHeading(str, "Player");
			HTMLFunctions.AddCellHeading(str, "Side");
			HTMLFunctions.AddCellHeading(str, "Colour");
			HTMLFunctions.EndRow(str);

			for (int s=1 ; s<=game.getNumOfSides() ; s++){
				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, "" + s);
				HTMLFunctions.AddCell(str, LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(s), true));
				HTMLFunctions.AddCell(str, mission.getSideDescription(s));
				HTMLFunctions.AddCell(str, "<font color=#" + ImageFunctions.GetRGB(MapImageAbstract.GetColourForSide(s)) + ">This Colour</font>");
				HTMLFunctions.EndRow(str);
			}
			HTMLFunctions.EndTable(str);

			if (game.getForumID() > 0) {
				str.append("<h3><a href=\"/dsr/forums/ForumPostingsPage.cls?topic=" + game.getForumID() + "\">Game Chat</a></h3>");
				//str.append("<p class=\"little2\">The game chat forum is created automatically for each game, and all players are subscribed to it and will receive an email notification whenever someone writes something.  It is ideal for communication during and after your game.</p>");
			}
			if (game.getGameStatus() == GamesTable.GS_FINISHED) {
				str.append("<h3><a href=\"/dsr/viewfinishedgame.cls?gid=" + game.getID() + "\">Game Details</a></h3>");
			} else {
				str.append("<h3><a href=\"/dsr/viewmap.cls?gid=" + game.getID() + "\">Game Details</a></h3>");
			}

			str.append("<br />");
			str.append("<center>");
			str.append("<applet codebase=\"/\" code=\"dsrwebserver.pages.dsr.playbackapplet.PlaybackApplet.class\" archive=\"libs/jme/jME_2.0.jar\" width=\"" + PlaybackApplet.ARENA_WIDTH + "\" height=\"" + PlaybackApplet.ARENA_HEIGHT + "\">");
			str.append("<param name=\"gid\" value=\"" + gid + "\">");
			if (this.session.isLoggedIn()) {
				str.append("<param name=\"lid\" value=\"" + this.current_login.getID() + "\">");
			} else {
				str.append("<param name=\"lid\" value=\"0\">");
			}
			str.append("<noapplet>Sorry, your browser does not have Java installed.</noapplet>");
			str.append("</applet>");
			str.append("</center>");


			this.body_html.append(MainLayout.GetHTML(this, "Mission Playback", str));
		}
		} finally {
			game.close();

		}

	}

}
