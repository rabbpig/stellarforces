package dsrwebserver.pages.dsr;

import ssmith.html.HTMLFunctions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.pages.AbstractHTMLPage;

public class helpwindow extends AbstractHTMLPage {

	// Help codes
	public static final int HC_GAME_OPTIONS = 1;
	//public static final int HC_SELECTION_ORDER = 2;
	public static final int HC_PRACTISE = 3;
	public static final int HC_ADVANCED_MODE = 4;
	public static final int HC_ENTER_OPPONENTS_NAME = 5;
	public static final int HC_SELECT_OPP_FIRE = 6;
	public static final int HC_SNAFU_MISSION = 7;
	public static final int HC_ARCH_NEMESIS = 8;
	public static final int HC_GAMES_REQ = 9;
	public static final int HC_IMPREGNATE = 10;
	public static final int HC_UNDERCOVER = 11;
	public static final int HC_MORALE = 12;
	public static final int HC_TROPHIES = 13;
	public static final int HC_WALLS = 14;
	public static final int HC_PCENT_CREDIT_ADJ = 15;
	public static final int HC_PCENT_OPP_PTS_RANGE = 16;
	public static final int HC_2V2 = 17;
	public static final int HC_BLOB_UNITS = 18;
	public static final int HC_CAMP_INACTIVITY = 19;
	public static final int HC_CAMP_GROSS_POINTS = 20;
	public static final int HC_CAMP_NET_POINTS = 21;
	public static final int HC_ANDROID = 22;
	public static final int HC_RESTRICTED = 23;

	public helpwindow() {
		super();
	}

	@Override
	public void process() throws Exception {
		int help_code = this.headers.getGetValueAsInt("hc");

		StringBuffer str = new StringBuffer();

		switch (help_code) {
		case HC_GAME_OPTIONS:
			HTMLFunctions.Heading(str, 2, "Game Options");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "Games can only be conceded by a side that has lost at least half of their units.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_PRACTISE:
			HTMLFunctions.Heading(str, 2, "Practise Games");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "Practise games are not included in league table calculations.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_ADVANCED_MODE:
			HTMLFunctions.Heading(str, 2, "Advanced Mode");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "Players are informed of enemy movement and shots fired if one of their units is within the noise range, even if they can't see them.");
			HTMLFunctions.AddListEntry(str, "Equipment and corpses are only shown if a unit can actually see it.");
			//HTMLFunctions.AddListEntry(str, "Eggs and Flags are not shown on the strategic scanner.");
			HTMLFunctions.AddListEntry(str, "Dead bodies are not shown on the strategic scanner.");
			HTMLFunctions.AddListEntry(str, "Players are only informed of game events (e.g. the death of a unit) if one of their units saw it happen.");
			HTMLFunctions.AddListEntry(str, "That stats 'energy' and 'morale' play a part in the game.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_ENTER_OPPONENTS_NAME:
			HTMLFunctions.Heading(str, 2, "Enter Opponent's Name");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "If you enter an opponent's name here, they will be the only player able to accept the game.");
			HTMLFunctions.AddListEntry(str, "You can enter multiple names by seperating them with a comma.");
			HTMLFunctions.AddListEntry(str, "You can enter a person's email address, and if they are not already a player they will be automatically sent an email with instructions on how to join.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_SELECT_OPP_FIRE:
			HTMLFunctions.Heading(str, 2, "Select Opportunity Fire");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "This set's whether your units will shoot at the enemy during the enemy's turn.");
			HTMLFunctions.AddListEntry(str, "You must set this before your opponent starts the client.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_SNAFU_MISSION:
			HTMLFunctions.Heading(str, 2, "SNAFU Missions");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "A SNAFU mission is one where the actual enemy must be discovered out of all the sides playing.");
			HTMLFunctions.AddListEntry(str, "SNAFU missions are always played in Advanced Mode.");
			HTMLFunctions.AddListEntry(str, "It is indicated on the details page which side you are and what your objective is.");
			HTMLFunctions.AddListEntry(str, "To prevent the early identification of the enemy, no VP's are shown until the end, and the number of enemy units remaining is not revealed.");
			HTMLFunctions.AddListEntry(str, "Opportunity fire must be selected (by side) for your units to shoot at the enemy or attack them.  This is changed on the details page.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_ARCH_NEMESIS:
			HTMLFunctions.Heading(str, 2, "Arch-Nemesis");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "This is the player who you have lost most games to.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_GAMES_REQ:
			HTMLFunctions.Heading(str, 2, "Games Required");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "This is the number of games you must have completed to be able to select this mission.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_IMPREGNATE:
			HTMLFunctions.Heading(str, 2, "Aliens Impregnate");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "If a mission allows aliens to impregnate, and an alien kills a human, a new alien will be created on the next turn, under the control of the alien player.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_UNDERCOVER:
			HTMLFunctions.Heading(str, 2, "Undercover Agents");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "An undercover agent is a randomly selected unit on an opposing side who you can turn to your side.");
			HTMLFunctions.AddListEntry(str, "The option to turn the agent is a Game Option on the Strategic Scanner page.");
			HTMLFunctions.AddListEntry(str, "You cannot turn the agent until at least turn " + DSRWebServer.EARLIEST_ACTIVATION_TURN + ".");
			HTMLFunctions.AddListEntry(str, "You are recommended to activate the agent just prior to your own turn.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_MORALE:
			HTMLFunctions.Heading(str, 2, "Morale");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "A unit's morale goes up and down depending on certain events.");
			HTMLFunctions.AddListEntry(str, "If a unit's morale reaches 0, the unit will panick, drop their current item, and have half their APs for that turn.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_TROPHIES:
			HTMLFunctions.Heading(str, 2, "Trophies");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "Full details on trophies are in the <a href=\"http://wiki.stellarforces.com/doku.php?id=gameplay:trophies\">wiki</a>.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_WALLS:
			HTMLFunctions.Heading(str, 2, "Walls");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "There are 3 kinds of wall:-");
			HTMLFunctions.AddListEntry(str, "<b>Indestructable</b>: These cannot be destroyed in anyway (except for events in the Meltdown mission).");
			HTMLFunctions.AddListEntry(str, "<b>Strong</b>: Grenades will only destroy the first layer of these walls.  Any units behind the first layer of walls will only receive 50% of the usual damage.");
			HTMLFunctions.AddListEntry(str, "<b>Weak</b>: Grenades will destroy all the walls in the radius, and all units wlll receive the usual damage.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_PCENT_CREDIT_ADJ:
			HTMLFunctions.Heading(str, 2, "% Credit Adjustment");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "This option will adjust the standard credits for both sides on the mission by the percentage amount.");
			HTMLFunctions.AddListEntry(str, "For example, if a side in a mission usually has 200 credits, and 150% is selected, they will have 300 credits in this game.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_PCENT_OPP_PTS_RANGE:
			HTMLFunctions.Heading(str, 2, "% Opponent Points Range");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "This option causes only players within a certain range of points (from the league table) of your own will be able to see your game request.");
			HTMLFunctions.AddListEntry(str, "For example, if you select 50% and you have 50 points in the league table, only players who have 25 - 75 points will be able to see your game request.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_2V2:
			HTMLFunctions.Heading(str, 2, "2 Vs 2 Mission");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "In this mission, multiple players are on the same side.");
			HTMLFunctions.AddListEntry(str, "Players on the same side can see their comrade's units, and they also share VPs.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_BLOB_UNITS:
			HTMLFunctions.Heading(str, 2, "Blob Units");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "Instead of showing the number of blob units (which is meaningless), the total health of all the blob units is shown.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_CAMP_INACTIVITY:
			HTMLFunctions.Heading(str, 2, "Campaign Inactivity");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "Every day a player doesn't have a campaign request open nor is playing a campaign game, they will incur an Inactivity Point which is subtracted from their Campaign Points.");
			HTMLFunctions.AddListEntry(str, "Inactivity Points can never be more than a player's Campaign Points.");
			HTMLFunctions.AddListEntry(str, "Inactivity Points are reduced by one every time a player ends a turn in a campaign game.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_CAMP_GROSS_POINTS:
			HTMLFunctions.Heading(str, 2, "Gross Points");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "This is the player's total points for campaign games before modifiers.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_CAMP_NET_POINTS:
			HTMLFunctions.Heading(str, 2, "Net Points");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "This is the player's total points for campaign games after modifiers.");
			HTMLFunctions.AddListEntry(str, "Modifiers are Inactivity Points and Collateral Damage.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_ANDROID:
			HTMLFunctions.Heading(str, 2, "Android");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "There are two version of the Stellar Forces client: PC version and Android version.");
			//HTMLFunctions.AddListEntry(str, "Only certain missions can be played on the Android client, although that list is increasing.");
			//HTMLFunctions.AddListEntry(str, "Some missions can be played on either client, and different clients can be used in the same game.");
			//HTMLFunctions.AddListEntry(str, "Some missions can only be played on either PC or Android throughout the whole game; it is not possible to take one turn on a PC and another on Android.");
			HTMLFunctions.AddListEntry(str, "The free Android client can be downloaded <a href=\"" + DSRWebServer.ANDROID_WEB_URL_FREE + "\">here</a>.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		case HC_RESTRICTED:
			HTMLFunctions.Heading(str, 2, "Restricted Items");
			HTMLFunctions.StartUnorderedList(str);
			HTMLFunctions.AddListEntry(str, "In campaign games, restricted items are only available for purchase to the faction with the most sectors.");
			HTMLFunctions.EndUnorderedList(str);
			break;
		default:
			//throw new RuntimeException("Sorry, no help is available on topic " + help_code);
			//str.append("Sorry, no help is available on that topic.");
			HTMLFunctions.Heading(str, 2, "Invalid Option");
			HTMLFunctions.Para(str, "Sorry, no help is available on topic " + help_code);
		}

		this.body_html.append(str);
	}


	public static String GetLink(int code) {
		return "<sup>[<a href=\"#\"><span onclick=\"javascript: window.open('/dsr/helpwindow.cls?hc=" + code + "','Help','status=0, height=300, width=300, resizable=0');\">?</span></a>]</sup>";
	}

}


