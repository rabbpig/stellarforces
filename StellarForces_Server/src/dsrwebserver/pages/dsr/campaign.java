package dsrwebserver.pages.dsr;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.html.HTMLFunctions;
import ssmith.io.TextFile;
import ssmith.lang.Functions;
import ssmith.util.MyList;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.CampUnitsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.UnitsTable;

public final class campaign extends AbstractHTMLPage {


	public campaign() {
		super();
	}


	@Override
	public void process() throws Exception {
		StringBuffer str = new StringBuffer();

		if (this.session.isLoggedIn()) {
			/*int units_selected = this.headers.getGetOrPostValueAsInt("units_selected");
			int gameid = this.headers.getGetOrPostValueAsInt("gameid");
			String s_ids = super.headers.getPostValueAsString("campunitid");
			boolean select_units_mode = this.headers.getGetValueAsInt("select_units_mode") == 1 && gameid > 0;
			MyList<Integer> ids = new MyList<Integer>();
			int min_units = 1, max_units = DSRWebServer.MAX_CAMP_UNITS;
			int recommend_units = -1;

			GamesTable game = new GamesTable(dbs);
			AbstractMission mission = null;
			int tot_creds = -1;
			int unit_cost = -1;
			int side = -1;

			if (gameid > 0) {
				game.selectRow(gameid);
				mission = AbstractMission.Factory(game.getMissionID());
				side = game.getSideFromPlayerID(this.current_login.getID());
				recommend_units = mission.getUnitsForSide(side);
				// Get min units if required
				min_units = mission.getMinUnitsForSide(side);
				// Get unit cost
				tot_creds = mission.getCreditsForSide(side);
				int base_creds = tot_creds;
				tot_creds = (tot_creds * game.getPcentCredit()) / 100;
				if (game.isCampGame()) {
					//tot_creds = tot_creds + Math.min(this.current_login.getCampCredits(), tot_creds);
					tot_creds = tot_creds + this.current_login.getCampCredits();
				}
				unit_cost = (base_creds / recommend_units) / 2;

				if (select_units_mode) {
					// Get min and max
					if (game.isPlayerInGame(this.current_login.getID())) {
						//MapData data = MapLoader.Import(dbs, -1, mission.getMapFilename(), false);
						max_units = mission.getNumDeploySquaresForSide(dbs, side);
						if (max_units > DSRWebServer.MAX_CAMP_UNITS) {
							max_units = DSRWebServer.MAX_CAMP_UNITS;
						}

						if (units_selected == 1) {
							ids = MyList.CreateFromCSVInts(s_ids);
							//if (game.isPlayerInGame(this.current_login.getID())) {
							if (game.hasSideChosenUnits(game.getSideFromPlayerID(this.current_login.getID())) == false) {
								boolean cont = true;
								// Check they have selected enough
								if (ids.size() < min_units) {
									cont = false;
									HTMLFunctions.Heading(str, 3, "You must select at least " + min_units + " units for this mission.");
								} else if (ids.size() > max_units) {
									cont = false;
									HTMLFunctions.Heading(str, 3, "You cannot select more than " + max_units + " units for this mission.");
								} else {
									// Check creds
									int bought_units = ids.size() - recommend_units;
									if (bought_units > 0) {
										int cost = bought_units * unit_cost;
										if (cost > tot_creds) {
											cont = false;
											HTMLFunctions.Heading(str, 3, "You cannot afford that many units.");
										} else {
											// Reduce camp creds
											if (cost > 0) {
												//this.current_login.incCampCredits(-cost);  NO, do it later in createUnits();
											}
										}
									}
								}
								if (cont) {
									CreateUnits(dbs, this.current_login, game, side, ids);
									this.redirectTo_Using303("BuyArmour.cls?gid=" + gameid);
									return;
								}
							} else {
								this.redirectTo_Using303("BuyArmour.cls?gid=" + gameid);
								return;
							}
						}
					}
				}
			}
			game.close();*/
			
			if (this.current_login.canPlayInCampaign() == 0) {
				HTMLFunctions.Heading(str, 2, "You need to complete at least " + DSRWebServer.MIN_CAMP_GAMES + " games to join the campaign.");
			}

			CampUnitsTable.EnsureEnoughCampUnits(dbs, this.current_login);

			CampUnitsTable.RecalcRanks(dbs, false); // Just in case.

			/*if (select_units_mode) {
				HTMLFunctions.Heading(str, 3, "<b>Mission:</b> <a href=\"/dsr/missiondescriptions.cls?type=" + game.getMissionID() + "&camp=" + (game.isCampGame() ? "1" : "0") + "\">" + mission.mission_name + "</a>");
				HTMLFunctions.Heading(str, 3, "<b>Your Side:</b> " + mission.getSideDescription(side) + " / Side " + side + " (<a href=\"/dsr/viewmap.cls?gid=" + game.getID() + "\">View Units and Map</a>)");

				HTMLFunctions.Para(str, "Please select your squad for this mission!  You must select between " + min_units + " and " + max_units + " units.  The standard version of this mission has " + recommend_units + " units on your side.");
				//HTMLFunctions.Para(str, "<b>This is a new feature in Stellar Forces.  If you notice any problems then please report them ASAP.  Thanks!</b>");

				HTMLFunctions.Para(str, "You have " + tot_creds + " creds available.  Any units after the first " + recommend_units + " will cost you " + unit_cost + " creds each.");
			} else {*/
				HTMLFunctions.Heading(str, 3, "Campaign Credits: " + this.current_login.getCampCredits());
				HTMLFunctions.Para(str, "This is your list of campaign units:-");//  The units at the top of the list are selected first for the next campaign mission.");
			/*}

			if (select_units_mode) {
				HTMLFunctions.StartForm(str, "form1", "", "post");
			}*/

			//HTMLFunctions.Heading(str, 3, "Your Squad");
			HTMLFunctions.StartTable(str, "stats", "100%", 1, "", 5);
			HTMLFunctions.StartRow(str);
			/*if (select_units_mode) {
				HTMLFunctions.AddCellHeading(str, "Select");//ion Order");
			}*/
			HTMLFunctions.AddCellHeading(str, "Name");
			HTMLFunctions.AddCellHeading(str, "Status");
			//HTMLFunctions.AddCellHeading(str, "Wages");
			HTMLFunctions.AddCellHeading(str, "Missions");
			HTMLFunctions.AddCellHeading(str, "Victories");
			HTMLFunctions.AddCellHeading(str, "Kills");
			HTMLFunctions.AddCellHeading(str, "Friendly Kills");
			HTMLFunctions.AddCellHeading(str, "Rank Points");
			HTMLFunctions.AddCellHeading(str, "APs");
			HTMLFunctions.AddCellHeading(str, "Shot Skill");
			HTMLFunctions.AddCellHeading(str, "Combat Skill");
			HTMLFunctions.AddCellHeading(str, "Strength");
			HTMLFunctions.AddCellHeading(str, "Health");
			HTMLFunctions.AddCellHeading(str, "Energy");
			HTMLFunctions.AddCellHeading(str, "Morale");
			HTMLFunctions.EndRow(str);

			ResultSet rs = dbs.getResultSet("SELECT CampUnitID FROM CampUnits WHERE OwnerID = " + this.current_login.getID() + " ORDER BY OrderBy");
			CampUnitsTable campunit = new CampUnitsTable(dbs);
			while (rs.next()) {
				HTMLFunctions.StartRow(str);
				campunit.selectRow(rs.getInt("CampUnitID"));
				//HTMLFunctions.AddCell(str, "<nobr>" + campunit.getOrderBy() + " <a href=\"?cmd=up&uid=" + rs.getInt("CampUnitID") + "\">Up/Fwd</a>|<a href=\"?cmd=dn&uid=" + rs.getInt("CampUnitID") + "\">Down/Back</a></nobr>");
				/*if (select_units_mode) {
					//HTMLFunctions.CheckBox(str, "unit_" + rs.getInt("CampUnitID"), "1", false);
					// don't allow selection of unit already deployed!
					HTMLFunctions.StartCell(str);
					if (campunit.getUnitID() <= 0) {
						HTMLFunctions.CheckBox(str, "campunitid", ""+rs.getInt("CampUnitID"), ids.contains(rs.getInt("CampUnitID")));
					}
					HTMLFunctions.EndCell(str);
				}*/
				HTMLFunctions.AddCell(str, campunit.getName());
				HTMLFunctions.AddCell(str, campunit.getStatusText());
				//HTMLFunctions.AddCell(str, "" + GetWages(campunit));
				HTMLFunctions.AddCell(str, "" + campunit.getTotalMissions());
				HTMLFunctions.AddCell(str, "" + campunit.getTotalVictories());
				HTMLFunctions.AddCell(str, "" + campunit.getKills());
				HTMLFunctions.AddCell(str, "" + campunit.getFriendlyKills());
				HTMLFunctions.AddCell(str, "" + campunit.getRankValue());

				HTMLFunctions.AddCell(str, "" + campunit.getMaxAPs());
				HTMLFunctions.AddCell(str, "" + campunit.getShotSkill());
				HTMLFunctions.AddCell(str, "" + campunit.getCombatSkill());
				HTMLFunctions.AddCell(str, "" + campunit.getStrength());
				HTMLFunctions.AddCell(str, "" + campunit.getMaxHealth());
				HTMLFunctions.AddCell(str, "" + campunit.getMaxEnergy());
				HTMLFunctions.AddCell(str, "" + campunit.getMaxMorale());

				HTMLFunctions.EndRow(str);
			}
			HTMLFunctions.EndTable(str);
			campunit.close();

			/*if (select_units_mode) {
				str.append("<br />");
				HTMLFunctions.HiddenValue(str, "select_units_mode", 1);
				HTMLFunctions.HiddenValue(str, "units_selected", "1");
				HTMLFunctions.HiddenValue(str, "gameid", gameid);
				HTMLFunctions.SubmitButton(str, "Let's Move Out!");
				HTMLFunctions.EndForm(str);
			} else {*/
				HTMLFunctions.Para(str, "Your squad is automatically replenished when you are not fighting in any campaign missions.  You can have a maximum of " + CampUnitsTable.GetMaxCampUnits(dbs, this.current_login) + " units.");
				HTMLFunctions.Para(str, "The calculation for determining the number of units in your squad is \"(#players in the biggest faction\" * " + DSRWebServer.MIN_CAMP_UNITS + ") / \"#players in your faction\"");
			//}
		}
		this.body_html.append(MainLayout.GetHTML(this, "Campaign Squad", str));		
	}


	public static void CreateUnits(MySQLConnection dbs, LoginsTable login, GamesTable game, int side, MyList<Integer> ids) throws SQLException, FileNotFoundException, IOException {
		int mission_type = game.getMissionID();

		int order = 1, model_type = 0, vps_if_killed = 0, vps_if_escape = 0, can_deploy = 0;

		// Get mission settings - find the first record - 
		TextFile tf = new TextFile();
		tf.openFile(DSRWebServer.SERVER_DATA_DIR + "units.csv", TextFile.READ);
		tf.readLine(); // Skip header
		boolean any_found = false;
		while (tf.isEOF() == false) {
			String s = tf.readLine();
			if (s.length() > 0) {
				if (s.startsWith("#") == false) {
					String data[] = s.split("\t");
					if (data.length > 0) {
						if (data[0].equalsIgnoreCase("" + mission_type)) {
							any_found = true;
							int file_side = Functions.ParseInt(data[4]);
							if (file_side == side) {
								order = Functions.ParseInt(data[1]);
								model_type = Functions.ParseInt(data[2]);
								vps_if_killed = Functions.ParseInt(data[7]);
								vps_if_escape = Functions.ParseInt(data[8]);
								boolean can_rename = Functions.ParseInt(data[13]) == 1;
								can_deploy = Functions.ParseInt(data[14]);
								if (can_rename) { // only these are valid
									break;
								}
							}
						}
					}
				}
			}
		}

		if (any_found) {
			// Create unit recs
			CampUnitsTable camp_units = new CampUnitsTable(dbs);
			UnitsTable units = new UnitsTable(dbs);

			// loop through ids
			for (Integer id : ids) {
				camp_units.selectRow(id);
				int unitid = units.createUnit(game.getID(), order, model_type, camp_units.getName(), side, camp_units.getMaxAPs(), camp_units.getMaxHealth(), vps_if_killed, vps_if_escape, camp_units.getMaxEnergy(), camp_units.getShotSkill(), camp_units.getCombatSkill(), camp_units.getStrength(), can_deploy, camp_units.getMaxMorale(), 0, 1);
				camp_units.setUnitID(unitid);
				order++;
			}
			camp_units.close();
			units.close();

			try {
				// Update creds
				AbstractMission mission = AbstractMission.Factory(game.getMissionID());
				int recommend_units = mission.getUnitsForSide(side);

				// Get unit cost
				int base_creds = mission.getCreditsForSide(side);
				int unit_cost = (base_creds / recommend_units) / 2;

				int bought_units = ids.size() - recommend_units;
				int cost = bought_units * unit_cost;
				if (cost > 0) {
					login.incCampCredits(-cost);
				}
			} catch (Exception ex)			 {
				DSRWebServer.HandleError(ex);
			}
		}

		game.setSideHasChosenUnits(side);


	}

}
