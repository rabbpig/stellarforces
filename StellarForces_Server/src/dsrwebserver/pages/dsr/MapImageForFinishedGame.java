package dsrwebserver.pages.dsr;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.image.CustomJPEG;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public class MapImageForFinishedGame extends MapImageAbstract {

	public MapImageForFinishedGame() {
		super();
	}

	public void process() throws Exception {
		this.content_type = "image/jpeg";

		int gameid = conn.headers.getGetValueAsInt("gid");
		int turn_no = conn.headers.getGetValueAsInt("turn_no");
		int turn_side = conn.headers.getGetValueAsInt("turn_side");

		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);
		if (game.getGameStatus() == GamesTable.GS_FINISHED) {
			if (turn_no == 0) { // Then we selected "end"
				turn_no = game.getTurnNo();
			}

			AbstractMission mission = AbstractMission.Factory(game.getMissionID());

			int our_side = -1;
			if (this.session.isLoggedIn()) {
				if (game.isPlayerInGame(current_login.getLoginID())) {
					our_side = game.getSideFromPlayerID(current_login.getLoginID());
				}
			}
			mid = MapDataTable.GetMapIDFromGameID(dbs, game.getID());
			Dimension d = MapDataTable.GetMapSize(dbs, mid);
			int smallest_side = Math.min(d.width, d.height) ;
			sq_size = SIZE/smallest_side;

			int img_width = sq_size * d.width;
			int img_height = sq_size * d.height;

			CustomJPEG jpg = new CustomJPEG(img_width, img_height);
			Graphics g = jpg.getGraphics();
			g.setColor(Color.black);
			g.fillRect(0, 0, img_width-1, img_height-1);

			if (mission.thin_thick_walls == AbstractMapModel.BLOCK_WALLS) {
				drawWallEdges(g);
			}

			// Draw floor
			String sql = "SELECT * FROM MapDataSquares WHERE MapDataID = " + mid + " And SquareType = " + MapDataTable.MT_FLOOR;
			ResultSet rs = dbs.getResultSet(sql);
			while (rs.next()) {
				super.setFloorColour(g, rs.getInt("FloorTex"), mission);

				if (rs.getInt("EscapeHatch") > 0) {//== our_side) {
					g.setColor(ESCAPE_HATCH_COL);
				}
				g.fillRect(rs.getInt("MapX") * sq_size, rs.getInt("MapY") * sq_size, sq_size, sq_size);

				// Draw doors
				if (rs.getInt("DoorType") > 0) {
					g.setColor(Color.green); //.darker()
					if (rs.getInt("DoorType") == MapDataTable.DOOR_EW) {
						g.fillRect((rs.getInt("MapX") * sq_size), (rs.getInt("MapY") * sq_size) + (sq_size/3), sq_size, sq_size/3);
					} else if (rs.getInt("DoorType") == MapDataTable.DOOR_NS) {
						g.fillRect((rs.getInt("MapX") * sq_size) + (sq_size/3), rs.getInt("MapY") * sq_size, sq_size/3, sq_size);
					} else {
						throw new RuntimeException("Unknown door type: " + rs.getInt("DoorType"));
					}
				}
			}

			// Draw walls
			if (mission.thin_thick_walls == AbstractMapModel.SLIM_WALLS) {
				drawWalls(g, gameid, turn_no, turn_side);
			}				

			this.drawComputers(g, our_side);

			if (turn_no == game.getTurnNo()) { // Last turn no
				drawFlagOrEggs(g, game);
			}

			drawExplosions(g, game, turn_no);

			drawUnits(g, game, mission, our_side, turn_no, turn_side);

			data = jpg.generateDataAsJPEG();
			this.content_length = data.length;
		}
	}


	private void drawWalls(Graphics g, int gameid, int turn_no, int turn_side) throws SQLException {
		String sql = "SELECT * FROM MapDataSquares WHERE MapDataID = " + mid + " And SquareType = " + MapDataTable.MT_WALL;
		ResultSet rs = dbs.getResultSet(sql);
		g.setColor(Color.green.darker());
		while (rs.next()) {
			g.fillRect((rs.getInt("MapX") * sq_size), (rs.getInt("MapY") * sq_size), sq_size, sq_size);

		}
		rs.close();

		// Draw any walls that were destroyed in a later turn
		if (turn_no > 0 && turn_side > 0) {
			sql = "SELECT * FROM UnitHistory WHERE EventType = " + UnitHistoryTable.UH_WALL_DESTROYED + " AND GameID = " + gameid + " AND (TurnNo > " + turn_no + " OR (TurnNo = " + turn_no + " AND TurnSide >= " + turn_side + "))";
			rs = dbs.getResultSet(sql);
			g.setColor(Color.green.darker().darker());
			while (rs.next()) {
				if (rs.getInt("OriginalSquareType") ==  MapDataTable.MT_WALL) {
					g.fillRect((rs.getInt("MapX") * sq_size), (rs.getInt("MapY") * sq_size), sq_size, sq_size);
				} else if (rs.getInt("OriginalSquareType") ==  MapDataTable.MT_FLOOR && rs.getInt("OriginalDoorType") > 0) {
					if (rs.getInt("OriginalDoorType") == MapDataTable.DOOR_EW) {
						g.fillRect((rs.getInt("MapX") * sq_size), (rs.getInt("MapY") * sq_size) + (sq_size/3), sq_size, sq_size/3);
					} else if (rs.getInt("OriginalDoorType") ==  MapDataTable.DOOR_NS) {
						g.fillRect((rs.getInt("MapX") * sq_size) + (sq_size/3), rs.getInt("MapY") * sq_size, sq_size/3, sq_size);
					}
				}

			}
			rs.close();
		}
	}


	private void drawUnits(Graphics g, GamesTable game, AbstractMission mission, int our_side, int turn_no, int turn_side) throws SQLException {
		// Add units
		String sql1 = "SELECT * FROM Units WHERE GameID = " + game.getID() + " ORDER BY Side, OrderBy";
		ResultSet rs_units = dbs.getResultSet(sql1);
		while (rs_units.next()) {
			//DSRWebServer.p("Drawing unit side " + rs_units.getInt("Side") + " / " + rs_units.getInt("OrderBy"));
			// Get the last position for the selected turn
			/*String sql = "SELECT * FROM UnitHistory";
			sql = sql + " WHERE GameID = " + game.getID();
			sql = sql + " AND UnitID = " + rs_units.getInt("UnitID");
			if (turn_no > 0 && turn_no < game.getTurnNo()) {
				sql = sql + " AND TurnNo <= " + turn_no;
				//sql = sql + " AND TurnSide = " + turn_side;
			}
			sql = sql + " AND EventType IN (" + UnitHistoryTable.UH_UNIT_MOVEMENT + ", " + UnitHistoryTable.UH_UNIT_KILLED + ", " + UnitHistoryTable.UH_UNIT_DEPLOYED + ")";
			sql = sql + " ORDER BY TurnNo DESC, TurnSide DESC, EventTime DESC, DateCreated DESC";
			sql = sql + " LIMIT 1";

			// If no units are shown, it's probably because a players has died
			ResultSet rs_hist = dbs.getResultSet(sql);
			if (rs_hist.next()) {*/
				g.setColor(GetColourForSide(rs_units.getInt("Side")));
				//if (rs_units.getInt("EventType") == UnitHistoryTable.UH_UNIT_KILLED) {
				if (rs_units.getInt("Status") == UnitsTable.ST_DEAD) {
					g.drawOval(rs_units.getInt("MapX") * sq_size, rs_units.getInt("MapY") * sq_size, sq_size, sq_size);
				} else {
					g.fillOval(rs_units.getInt("MapX") * sq_size, rs_units.getInt("MapY") * sq_size, sq_size, sq_size);
					// Draw direction
					g.setColor(Color.yellow);
					int sx = rs_units.getInt("MapX") * sq_size + (sq_size/2);
					int sy = rs_units.getInt("MapY") * sq_size + (sq_size/2);
					g.drawLine(sx, sy, sx+GetXOffSetFromAngle(rs_units.getInt("Angle"), sq_size), sy+GetYOffSetFromAngle(rs_units.getInt("Angle"), sq_size));

				}

				// Draw black border to numbers
				g.setColor(Color.black);
				g.drawString("" + rs_units.getInt("OrderBy"), (rs_units.getInt("MapX") * sq_size) + 2, (rs_units.getInt("MapY") * sq_size) + 12);
				g.drawString("" + rs_units.getInt("OrderBy"), (rs_units.getInt("MapX") * sq_size) + 4, (rs_units.getInt("MapY") * sq_size) + 12);
				g.drawString("" + rs_units.getInt("OrderBy"), (rs_units.getInt("MapX") * sq_size) + 3, (rs_units.getInt("MapY") * sq_size) + 11);
				g.drawString("" + rs_units.getInt("OrderBy"), (rs_units.getInt("MapX") * sq_size) + 3, (rs_units.getInt("MapY") * sq_size) + 13);

				g.setColor(Color.white);
				int x = (rs_units.getInt("MapX") * sq_size) + 3;
				int y = (rs_units.getInt("MapY") * sq_size) + 12;
				//DSRWebServer.p("Drawing at " + x + ", " + y);
				g.drawString("" + rs_units.getInt("OrderBy"), x, y);
			//}

		}
	}


}
