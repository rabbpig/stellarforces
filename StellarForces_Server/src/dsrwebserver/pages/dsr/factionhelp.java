package dsrwebserver.pages.dsr;

import java.sql.ResultSet;

import ssmith.html.HTMLFunctions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.FactionsTable;

public class factionhelp extends AbstractHTMLPage {

	public factionhelp() {
		super();
	}

	@Override
	public void process() throws Exception {
		this.setTitle("About " + DSRWebServer.TITLE);

		StringBuffer str = new StringBuffer();

		if (this.session.isLoggedIn()) {
			ResultSet rs = dbs.getResultSet("SELECT * FROM Factions");
			while (rs.next()) {
				str.append("<img src=\"" + DSRWebServer.IMAGES_HOST + "/images/factions/" + FactionsTable.GetImageFilename(dbs, rs.getInt("FactionID")) + "\" width=150 hspace=20 />");
			}

			HTMLFunctions.Heading(str, 2, "There is a battle raging in the galaxy between various factions.");  

			HTMLFunctions.Para(str, "The galaxy is divided into sectors, and each sector is controlled by a particular faction.  You can view the galaxy <a href=\"starmap.cls\">here</a>.");

			HTMLFunctions.Para(str, "If you are a member of a faction, you can choose to attack an adjacent sector or defend a sector that is under attack.  You can only choose sectors that are adjacent to a sector that you already control.  You can only choose to attack or defend one sector at a time.");

			HTMLFunctions.Para(str, "If a faction attacks a sector, the players in the defending faction will be notified by email and have " + DSRWebServer.STARMAP_DEFEND_DAYS + " days to select to defend it, otherwise it is automatically lost.  Once a player chooses to defend the sector, this starts a mission.");

			HTMLFunctions.Para(str, "Certain equipment is only available to players in the faction with the most sectors.");

			HTMLFunctions.Para(str, "Please feel free to make any suggestions or comments about the new Faction system (which was designed by Deadlime).  Bear in mind that it is still in 'beta' though :).");

			str.append(factions.GetFactionStuff(dbs, this.current_login.getFactionID()));
		}
		this.body_html.append(MainLayout.GetHTML(this, "Faction Help", str));		

	}


}
