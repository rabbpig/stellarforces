package dsrwebserver.pages.dsr;

import ssmith.html.HTMLFunctions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;

public class about extends AbstractHTMLPage {

	public about() {
		super();
	}

	
	@Override
	public void process() throws Exception {
		//String full_req = AbstractHTMLPage.disk_cache.convertPath(super.conn.headers.request.getFullURL());
		StringBuffer str = new StringBuffer();
		//if (AbstractHTMLPage.disk_cache.contains(full_req) == false) {
			this.setTitle("About " + DSRWebServer.TITLE);

			str.append("<img align=\"left\" hspace=15 vspace=15 height=\"190\" src=\"" + DSRWebServer.IMAGES_HOST + "/images/android_screenshots/android1.jpg\" alt=\"Screenshot\" />");

			//MainPage.AppendAbout(str);
			
			HTMLFunctions.Para(str, "Hi and welcome to Stellar Forces!");
			HTMLFunctions.Para(str, "This is a turn-based squad-level strategy game.  All the code and pretty much all the graphics were created by myself (Steve Smith).  I decided to write it when I saw that there wasn't (at the time) and multi-player turn-based strategy games similar to X-Com or Laser Squad.  There used to be a PC client, but it's not played entirely on Android, and the website is used to store information like the league table and mission details.");
			HTMLFunctions.Para(str, "I'm always open to new suggestions for the game, so if you can think of anything good, contact me and if makes sense and is easy to implement, I'll probably do it.");
			
			this.appendFeatures(str);

			//AbstractHTMLPage.disk_cache.put(full_req, str.toString());
		/*} else {
			str.append(AbstractHTMLPage.disk_cache.get(full_req));
		}*/
		this.body_html.append(MainLayout.GetHTML(this, "About SF", str));		

	}


	private void appendFeatures(StringBuffer str) {
		str.append("<br clear=\"all\" />");
		HTMLFunctions.Heading(str, 2, "Special Features!");

		HTMLFunctions.StartUnorderedList(str);
		HTMLFunctions.AddListEntry(str, "Take your turn when <b>you</b> want.");
		HTMLFunctions.AddListEntry(str, "Over 90 different missions.");
		HTMLFunctions.AddListEntry(str, "Watch the complete playback of all your games.");
		//HTMLFunctions.AddListEntry(str, "Clients for Windows, Linux and Android.");
		HTMLFunctions.AddListEntry(str, "A wide range of weapons and equipment, from guns to explosives and beyond.");
		HTMLFunctions.AddListEntry(str, "Play against 1, 2 or 3 mostly human opponents!");
		//HTMLFunctions.AddListEntry(str, "Translations in English, Espanol, Portugues, Francais and Deutsch (PC client only).");
		HTMLFunctions.AddListEntry(str, "A helpful and welcoming community that will answer any questions without condescension.");
		HTMLFunctions.AddListEntry(str, "No in-app purchases or adverts.  No further payment is required to unlock any feature.");
		HTMLFunctions.AddListEntry(str, "A practise option to avoid embarassingly low league positions.");
		HTMLFunctions.AddListEntry(str, "A development team always open to new suggestions, no matter how wacky and far-fetched.");
		HTMLFunctions.AddListEntry(str, "Involving game mechanics, like overhearing enemies, opportunity fire, and line-of-sight.");
		//HTMLFunctions.AddListEntry(str, "Doesn't mess about with your registry!");
		HTMLFunctions.AddListEntry(str, "Exclusively <b>not</b> on iTunes.");
		HTMLFunctions.AddListEntry(str, "<b>Doesn't</b> try to link to your Facebook/Twitter/LinkedIn account!");
		HTMLFunctions.EndUnorderedList(str);

	}

}
