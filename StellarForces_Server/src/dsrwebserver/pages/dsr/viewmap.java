package dsrwebserver.pages.dsr;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.html.HTMLFunctions;
import ssmith.image.ImageFunctions;
import ssmith.lang.Functions;
import ssmith.util.MyList;
import dsr.comms.AbstractCommFuncs;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.missions.SnafuTheAssassins;
import dsrwebserver.missions.SnafuTheAssassins2;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.GameLogTable;
import dsrwebserver.tables.GameRequestsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.OppFireSelectionTable;
import dsrwebserver.tables.UnitsTable;
import dsrwebserver.tables.UploadedMapsTable;

public final class viewmap extends AbstractHTMLPage {

	private AbstractMission mission;

	public viewmap() {
		super();

	}


	@Override
	public void process() throws Exception {
		if (this.session.isLoggedIn()) {
			StringBuffer str = new StringBuffer();

			String title = "Strategic Scanner";
			String concede = this.headers.getPostValueAsString("concede");
			int gid = this.headers.getGetOrPostValueAsInt("gid");
			String cmd = this.headers.getGetOrPostValueAsString("cmd");
			int side = this.headers.getGetOrPostValueAsInt("side");

			/*if (this.current_login.isAdmin()) {
				try {
					int kill = this.headers.getGetOrPostValueAsInt("kill");
					if (kill > 0) {
						UnitsTable unit = new UnitsTable(dbs);
						unit.selectRow(kill);
						unit.setStatus(UnitsTable.ST_DEAD);
						HTMLFunctions.Para(str, "Unit " + unit.getName() + " killed.");
					}
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
			}*/

			GamesTable game = new GamesTable(dbs);
			try {
				if (game.doesRowExist(gid)) {
					game.selectRow(gid);

					if (game.getGameStatus() == GamesTable.GS_FINISHED) {
						super.redirectTo_Using303("viewfinishedgame.cls?" + this.headers.getQueryString());
						return;
					}
					mission = AbstractMission.Factory(game.getMissionID());
					if (game.isPlayerInGame(current_login.getLoginID())) {
						int our_side = game.getSideFromPlayerID(current_login.getLoginID());
						if (concede.length() > 0) {
							if (concede.equalsIgnoreCase("activate")) { // Activate undercover agent
								if (game.getNumUndercoverUnits(our_side) > 0 && (game.getTurnNo() >= DSRWebServer.EARLIEST_ACTIVATION_TURN && game.getTurnSide() == our_side)) {
									String result = this.activateUndercoverAgent(game, our_side);
									HTMLFunctions.Para(str, result);
								}
							} else if (concede.equalsIgnoreCase("cancel")) { // Cancel mutual concede
								if (game.getNumOfSides() == 2) {
									if (game.getProposeConcedeLoginID() == this.current_login.getID()) {
										game.setProposeMutualConcedeLoginID(0);
										HTMLFunctions.Para(str, "Your proposition for mutually conceding the game has been cancelled.");
									}
								}
							} else if (concede.equalsIgnoreCase("accept")) {  // Accept mutual concede
								if (game.getNumOfSides() == 2) {
									if (game.getProposeConcedeLoginID() > 0 && game.getProposeConcedeLoginID() != this.current_login.getID()) {
										game.setWinType(GamesTable.WIN_DRAW_MUTUAL_CONCEDE, -1);
										HTMLFunctions.Para(str, "That game has been mutually conceded.  Want to <a href=\"/dsr/GameRequests.cls\">start another game?</a>");
										GameLogTable.AddRec(dbs, game, -1, -1, "This game has been mutually conceded.", true, System.currentTimeMillis());
									}
								}
							} else if (concede.equalsIgnoreCase("propose")) { // Propose mutual concede
								if (game.getNumOfSides() == 2) {
									if (game.getProposeConcedeLoginID() == 0) {
										game.setProposeMutualConcedeLoginID(this.current_login.getID());
										HTMLFunctions.Para(str, "You have proposed mutually conceding the game.");
									}
								}
							} else if (concede.equalsIgnoreCase("concede")) {
								if (game.getProposeConcedeLoginID() == 0) {
									if (canConcede(gid, our_side)) {
										//game.concedeGame(game.getOpponentsLoginIDFor2PlayerGame(this.current_login.getID()));
										//HTMLFunctions.Para(str, "That game has been conceded.  Want to <a href=\"/dsr/GameRequests.cls\">start another game?</a>");
										//GameLogTable.AddRec(dbs, game.getID(), -1, game.getTurnNo(), "This game has been conceded by " + LoginsTable.GetDisplayName_Enc(dbs, this.current_login.getID(), false) + ".");
										this.redirectTo_Using303("/dsr/cancelgame.cls?gid=" + game.getID() + "&lid=1");// + this.current_login.getID());
										return;
									} else {
										HTMLFunctions.Para(str, "You cannot concede yet.  You need to have lost at least half of your units.");
									}
								}
							} else {
								//HTMLFunctions.PgetMissionTypeara(str, "Unknown option.");
							}
							HTMLFunctions.Para(str, "[<a href=\"?gid=" + gid + "\">Back</a>]");
						} else {
							//AbstractMission mission = AbstractMission.Factory(game.getMissionID());
							if (cmd.length() > 0) {
								if (mission.isSnafu()) {
									try {
										String shooter = "unknown";
										String shootee = "unknown";
										try {
											shooter = LoginsTable.GetDisplayName(dbs, game.getLoginIDFromSide(our_side));
											shootee = LoginsTable.GetDisplayName(dbs, game.getLoginIDFromSide(side));
										} catch (SQLException ex) {
											DSRWebServer.HandleError(ex, "GameID=" + game.getID(), true);
										}
										if (cmd.equalsIgnoreCase("ofy")) {
											OppFireSelectionTable.AddRec(dbs, game.getID(), this.current_login.getID(), side, true);
											HTMLFunctions.Para(str, "Your units units will now fire upon units owned by " + shootee);
											String s = shooter + " will now fire upon units owned by " + shootee;
											GameLogTable.AddRec(dbs, game, this.current_login.getID(), -1, s, false, System.currentTimeMillis());
											//HTMLFunctions.Para(str, "<i>If you are currently running the client, please restart it for the new option to take effect.</i>");
										} else if (cmd.equalsIgnoreCase("ofn")) {
											OppFireSelectionTable.AddRec(dbs, game.getID(), this.current_login.getID(), side, false);
											//HTMLFunctions.Para(str, "<i>If you are currently running the client, please restart it for the new option to take effect.</i>");
											HTMLFunctions.Para(str, "Your units units will now NOT fire upon units owned by " + shootee);
											String s = shooter + " will now NOT fire upon units owned by " + shootee;
											GameLogTable.AddRec(dbs, game, this.current_login.getID(), -1, s, false, System.currentTimeMillis());
										}
									} catch (Exception ex) {
										DSRWebServer.HandleError(ex);
									}
								}
							}

							String opponents = game.getOpponentsNamesBySide(-1, false);
							HTMLFunctions.Heading(str, 3, opponents + " in '" + mission.mission_name + "'");

							if (mission.getMaxTurns() > 0) {
								HTMLFunctions.Heading(str, 4, "Turn " + game.getTurnNo() + " (" + (mission.getMaxTurns() - game.getTurnNo()) + " turns remaining)");
							} else {
								HTMLFunctions.Heading(str, 4, "Turn " + game.getTurnNo());
							}

								str.append("<br clear=\"all\" />");

							// Draw map image
							if (MapDataTable.IsFullyCreated(dbs, MapDataTable.GetMapIDFromGameID(dbs, gid))) {
								str.append("<img align=left hspace=10 src=\"MapImageForCurrentGame.cls?gid=" + gid + "\" alt=\"Strategic scanner for current game\" ><br />");
							} else {
								HTMLFunctions.Heading(str, 3, "Map still being generated...");
							}

							if (game.getProposeConcedeLoginID() > 0) {
								if (game.getProposeConcedeLoginID() == this.current_login.getID()) {
									HTMLFunctions.Para(str, "<b>You have proposed to mutually concede this game.</b>");
								} else {
									HTMLFunctions.Para(str, "<b>Your opponent has proposed to mutually concede this game.</b>");
								}
							}

							try {
								if (game.getMissionID() == AbstractMission.SNAFU_MOONBASE_ASSAULT || game.getMissionID() == AbstractMission.SNAFU_SABOTAGE) {
									String sql = "SELECT Count(*) FROM MapDataSquares WHERE MapDataID = " + game.getMapDataID() + " AND SquareType = " + MapDataTable.MT_COMPUTER + " AND Destroyed = 1";
									HTMLFunctions.Heading(str, 3, "Computers Destroyed: " + dbs.getScalarAsInt(sql));
								} else if (game.getMissionID() == AbstractMission.SNAFU_THE_ASSASSINS) {
									if (our_side == SnafuTheAssassins.GetAssassinsSide(game)) {
										HTMLFunctions.Heading(str, 3, "Targets Remaining: " + SnafuTheAssassins.GetTargetsRemaining(dbs, game));
									}
								} else if (game.getMissionID() == AbstractMission.SNAFU_THE_ASSASSINS_2) {
									HTMLFunctions.Heading(str, 3, "Targets Remaining: " + SnafuTheAssassins2.GetTargetsRemaining(dbs, game, our_side));
								}
								for (int s=1 ; s<=game.getNumOfSides() ; s++){
									if (game.getNumUndercoverUnits(s) > 0) {
										HTMLFunctions.Heading(str, 3, "Side " + s + " has " + game.getNumUndercoverUnits(s) + " Unactivated Undercover Agents" + helpwindow.GetLink(helpwindow.HC_UNDERCOVER));
									}
								}
							} catch (Exception ex) {
								DSRWebServer.HandleError(ex);
							}

							// Draw players table at top
							HTMLFunctions.StartTable(str, "stats", "", 0, "", 5);
							HTMLFunctions.StartRow(str);
							HTMLFunctions.AddCellHeading(str, "Side");
							HTMLFunctions.AddCellHeading(str, "Name");
								HTMLFunctions.AddCellHeading(str, "Units");
							HTMLFunctions.AddCellHeading(str, "VPs");
							HTMLFunctions.AddCellHeading(str, "Colour");
							//HTMLFunctions.AddCellHeading(str, "Win Pts");
							if (mission.isSnafu()) {
								HTMLFunctions.AddCellHeading(str, "Opp Fire " + helpwindow.GetLink(helpwindow.HC_SELECT_OPP_FIRE));
							}
							HTMLFunctions.EndRow(str);

							for (int s=1 ; s<=game.getNumOfSides() ; s++){
								String units = ""  + dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + gid + " AND Status  = " + UnitsTable.ST_DEPLOYED + " AND Side = " + s);

								HTMLFunctions.StartRow(str);
								HTMLFunctions.AddCell(str, "" + s);
								// Num units
								HTMLFunctions.AddCell(str, LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(s), true) + " (" + mission.getSideName(dbs, game, s) + ")");
								//if (mission.isSnafu() == false || s == game.getSideFromPlayerID(this.current_login.getID())) {
								if (game.isAdvancedMode() == false || mission.getSidesForSide(our_side).contains(s)) {
									try {
										// Check there are some units
										if (dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + s) > 0) {
											// See if it's a blob side
											int model_type = dbs.getScalarAsInt("SELECT ModelType FROM Units WHERE GameID = " + game.getID() + " AND Side = " + s);
											if (model_type == UnitsTable.MT_BLOB) {
												units = dbs.getScalarAsInt("SELECT SUM(Health) FROM Units WHERE GameID = " + gid + " AND Status  = " + UnitsTable.ST_DEPLOYED + " AND Side = " + s) + "h" + helpwindow.GetLink(helpwindow.HC_BLOB_UNITS);
											}
										}
									} catch (Exception ex) {
										DSRWebServer.HandleError(ex);
									}
									HTMLFunctions.AddCell(str, units);
								} else {
									HTMLFunctions.AddCell(str, "***");
								}
								// VPs
								if (mission.isSnafu() == false) {
									HTMLFunctions.AddCell(str, "" + game.getVPsForSide(s));
								} else {
									HTMLFunctions.AddCell(str, "***"); // Don't show VPs otherwise they will know if they are attacking the right people
								}
								// Color
								HTMLFunctions.AddCell(str, "<font color=#" + ImageFunctions.GetRGB(MapImageAbstract.GetColourForSide(s)) + ">This Colour</font>");

								// Points for win
								/*if (game.haveAllPlayersJoined()) {
								HTMLFunctions.AddCell(str, "" + game.getPointsForWin(s));
							} else {
								HTMLFunctions.AddCell(str, "tba");
							}*/

								// Opp fire
								if (mission.isSnafu()) {
									if (s != our_side) {
										if (OppFireSelectionTable.IsOppFire(dbs, game.getID(), this.current_login.getID(), s)) {
											HTMLFunctions.AddCell(str, "YES <sup>[<a href=\"?gid=" + game.getID() + "&cmd=ofn&side=" + s + "\" rel=\"nofollow\" >no</a>]</sup>");
										} else {
											HTMLFunctions.AddCell(str, "NO <sup>[<a href=\"?gid=" + game.getID() + "&cmd=ofy&side=" + s + "\" rel=\"nofollow\" >yes</a>]</sup>");
										}
									} else {
										HTMLFunctions.AddCell(str, "n/a");
									}
								}

								HTMLFunctions.EndRow(str);
							}
							HTMLFunctions.EndTable(str);

							HTMLFunctions.Heading(str, 3, "Game Settings");
							HTMLFunctions.StartUnorderedList(str);
							if (mission.isSnafu()) {
								HTMLFunctions.AddListEntry(str, "This is a SNAFU mission! " + helpwindow.GetLink(helpwindow.HC_SNAFU_MISSION));
								HTMLFunctions.AddListEntry(str, "<b>" + mission.getSnafuMissionText(game, our_side) + "</b>");
							}
							HTMLFunctions.AddListEntry(str, "The mission is <a href=\"missiondescriptions.cls?type=" + mission.getType() + "&camp=" + (game.isCampGame() ? "1" : "0") + "\">" + mission.mission_name + "</a>");
							HTMLFunctions.AddListEntry(str, mission.getMission1Liner(our_side));
							if (game.isCampGame()) {
								HTMLFunctions.AddListEntry(str, "This is a campaign game");
							}
							if (game.getUploadedMapID() > 0) {
								HTMLFunctions.AddListEntry(str, "This uses custom map " + UploadedMapsTable.GetName(dbs, game.getUploadedMapID()));
							}

							if (game.getGameType() == GameRequestsTable.GT_PRACTISE) {
								HTMLFunctions.AddListEntry(str, "This is a practise game");
							}
							if (game.isAdvancedMode()) {
								HTMLFunctions.AddListEntry(str, "This game is Advanced Mode" + helpwindow.GetLink(helpwindow.HC_ADVANCED_MODE));
							}
							if (game.getPcentCredit() != 0 && game.getPcentCredit() != 100) {
								HTMLFunctions.AddListEntry(str, "Both sides have " + game.getPcentCredit() + "% credits.");
							}
							switch (mission.getWallsDestroyableType()) {
							case AbstractMission.INDESTRUCTABLE_WALLS:
								HTMLFunctions.AddListEntry(str, "Walls are <b>indestructible</b>." + helpwindow.GetLink(helpwindow.HC_WALLS));
								break;
							case AbstractMission.STRONG_WALLS:
								HTMLFunctions.AddListEntry(str, "Walls are <b>strong</b> but destructible." + helpwindow.GetLink(helpwindow.HC_WALLS));
								break;
							case AbstractMission.WEAK_WALLS:
								HTMLFunctions.AddListEntry(str, "Walls are <b>weak</b>." + helpwindow.GetLink(helpwindow.HC_WALLS));
								break;
							default:
								throw new RuntimeException("Unknown wall type: " + mission.getWallsDestroyableType());
							}

							/*switch (mission.canBePlayedOnAndroid()) {
						case AbstractMission.NOT_ON_ANDROID:
							HTMLFunctions.AddListEntry(str, "PC Client Only");
							break;
						case AbstractMission.ONLY_ONE_CLIENT:
							HTMLFunctions.AddListEntry(str, game.isAndroid() == 0 ? "PC Only":"<span style=\"color: #ff0000;\">Android Client Only</span>");
							break;
						case AbstractMission.ANY_CLIENT:
							HTMLFunctions.AddListEntry(str, "PC or Android Client");
							break;
						default:
							throw new RuntimeException("Unknown type: " + mission.canBePlayedOnAndroid());
						}*/

							try {
								// 2v2?
								MyList<Integer> sides = mission.getSidesForSide(our_side);
								if (sides.size() > 1) {
									sides.removeInt(our_side);
									HTMLFunctions.AddListEntry(str, "You are on the same side as player " + sides.toCSVString() + "." + helpwindow.GetLink(helpwindow.HC_2V2));
								}
							} catch (Exception ex) {
								DSRWebServer.HandleError(ex);
							}
							HTMLFunctions.EndUnorderedList(str);

							if (game.getForumID() > 0) {
								str.append("<hr /><h3><a href=\"/dsr/forums/ForumPostingsPage.cls?topic=" + game.getForumID() + "\">Game Chat</a></h3>");
								str.append("<p class=\"little2\">The game chat forum is created automatically for each game, and all players are subscribed to it and will receive an email notification whenever someone writes something.  It is ideal for communication during and after your game.</p>");
							}

							//mission = AbstractMission.Factory(game.getMissionID());
							MyList<Integer> sides = mission.getSidesForSide(our_side);
							sides.remove((Integer)our_side);
							if (sides.size() > 0) {
								int comrade = game.getLoginIDFromSide(sides.get(0));
								if (comrade > 0) { // Have they joined the game yet?
									String link = "About <a href=\"" + DSRWebServer.WEBSITE_URL + "/dsr/viewmap.cls?gid=" + game.getID() + "\">Game " + game.getID() + ":" + mission.mission_name + "</a>, \n\n";
									str.append("<hr /><h3><a href=\"/dsr/playerspublicpage.cls?loginid=" + comrade + "&amp;subject=" + AbstractCommFuncs.URLEncodeString("Game " + game.getID() + ":" + mission.mission_name) + "&msg=" + AbstractCommFuncs.URLEncodeString(link) + "\">Message Comrade</a></h3>");
									str.append("<p class=\"little2\">This link will take you to a blank message for you to send a message to your comrade.</p>");
								}
							}

							if (game.getID() > 800 && game.getGameStatus() == GamesTable.GS_STARTED) {
								str.append("<hr /><h3><a href=\"/dsr/playbackapplet/playbackpage.cls?gid=" + game.getID() + "\">Intermediate Playback</a></h3>");
								str.append("<p class=\"little2\">Watch the movement of all visible units in the game up to the current point.</p>");
							}

							str.append("<hr />");

							if (game.getGameStatus() == GamesTable.GS_STARTED) {
								StringBuffer options = new StringBuffer();
								if (game.getNumUndercoverUnits(our_side) > 0 && (game.getTurnNo() >= DSRWebServer.EARLIEST_ACTIVATION_TURN && game.getTurnSide() == our_side)) {
									options.append("<option value=\"activate\">Activate Agent</option>");
								}
								if (game.getProposeConcedeLoginID() > 0) {
									if (game.getProposeConcedeLoginID() == this.current_login.getID()) {
										options.append("<option value=\"cancel\">Cancel Mutual Concede Proposal</option>");
									} else { //if (game.isPlayerInGame()) {
										options.append("<option value=\"accept\">Accept Mutual Concede Proposal</option>");
									}
								} else {
									if (game.getNumOfSides() == 2) {
										options.append("<option value=\"propose\">Propose Mutual Concede</option>");
									}
									if (game.isPractise() || canConcede(gid, our_side)) { 
										options.append("<option value=\"concede\">Concede Game</option>");
									}
								}

								if (options.length() > 0) {
									HTMLFunctions.Heading(str, 3, "Game Options " + helpwindow.GetLink(helpwindow.HC_GAME_OPTIONS) + "");
									HTMLFunctions.StartForm(str, "form1", "", "POST");
									str.append("<select name=\"concede\" class=\"combobox\" >");
									str.append("<option value=\"\">[Please Choose]</option>");
									str.append(options);
									str.append("</select>");
									str.append("&nbsp;&nbsp;");
									HTMLFunctions.HiddenValue(str, "gid", gid);
									HTMLFunctions.SubmitButtonWithConf(str, "Form", "Select", "Are you sure?", "");
									HTMLFunctions.EndForm(str);

									str.append("<hr />");
								}
							}

								HTMLFunctions.Heading(str, 3, "Map Key");
								HTMLFunctions.StartUnorderedList(str);
								HTMLFunctions.AddListEntry(str, "<font color=\"#" + ImageFunctions.GetRGB(MapImageForCurrentGame.OUR_COMPUTER_COL) + "\">Your computer</font>");
								HTMLFunctions.AddListEntry(str, "<font color=\"#" + ImageFunctions.GetRGB(MapImageForCurrentGame.OUR_DESTROYED_COMPUTER_COL) + "\">Your Destroyed computer</font>");
								HTMLFunctions.AddListEntry(str, "<font color=\"#" + ImageFunctions.GetRGB(MapImageForCurrentGame.OPP_COMPUTER_COL) + "\">Opponent's computer</font>");
								HTMLFunctions.AddListEntry(str, "<font color=\"#" + ImageFunctions.GetRGB(MapImageForCurrentGame.OPP_DESTROYED_COMPUTER_COL) + "\">Opponent's destroyed computer</font>");
								HTMLFunctions.AddListEntry(str, "<font color=\"#" + ImageFunctions.GetRGB(MapImageForCurrentGame.ESCAPE_HATCH_COL) + "\">Escape Hatch</font>");
								HTMLFunctions.AddListEntry(str, "<font color=\"#" + ImageFunctions.GetRGB(MapImageForCurrentGame.FLAG_COL) + "\">F.L.A.G. or Eggs</font>");
								HTMLFunctions.EndUnorderedList(str);

							HTMLFunctions.Para(str, "<br clear=\"all\"><i>Only visible enemy units are shown until the game has finished.  Any enemy units seen during your opponent's turn are shown as grey circles at each point they were seen and the arrow shows the direction they were facing.  Hollow circles are dead bodies.  If an enemy unit is heard shooting, this is shown as a white circle.</i>");

							str.append("<br clear=\"all\" />");

							// Add units
							int max_shotskill = dbs.getScalarAsInt("SELECT Max(ShotSkill)FROM Units WHERE GameID = " + gid + " AND Side = " + our_side);
							//int max_shotdamage = dbs.getScalarAsInt("SELECT Max(ShotDamage)FROM Units WHERE GameID = " + gid + " AND Side = " + our_side);
							int max_ccskill = dbs.getScalarAsInt("SELECT Max(CombatSkill)FROM Units WHERE GameID = " + gid + " AND Side = " + our_side);
							int max_strength = dbs.getScalarAsInt("SELECT Max(Strength)FROM Units WHERE GameID = " + gid + " AND Side = " + our_side);

							HTMLFunctions.StartTable(str, "stats", "", 1, "", 5);

							HTMLFunctions.StartRow(str);
							HTMLFunctions.AddCellHeading(str, "Name");
							HTMLFunctions.AddCellHeading(str, "Status");
								HTMLFunctions.AddCellHeading(str, "Armour");
								HTMLFunctions.AddCellHeading(str, "Current Item");
							HTMLFunctions.AddCellHeading(str, "APs (Max)");
							HTMLFunctions.AddCellHeading(str, "Health");
							HTMLFunctions.AddCellHeading(str, "Shot Skill");
							HTMLFunctions.AddCellHeading(str, "CC Skill");
							HTMLFunctions.AddCellHeading(str, "Strength");
								HTMLFunctions.AddCellHeading(str, "Burden");
							HTMLFunctions.AddCellHeading(str, "Opp Fire?");
							if (game.isAdvancedMode()) {
								HTMLFunctions.AddCellHeading(str, "Energy (Max)");
								HTMLFunctions.AddCellHeading(str, "Morale (Max)" + helpwindow.GetLink(helpwindow.HC_MORALE));
							}
							HTMLFunctions.EndRow(str);

							UnitsTable unit = new UnitsTable(dbs);
							MyList<Integer> ml_sides = mission.getSidesForSide(our_side);
							String sql = "SELECT * FROM Units WHERE GameID = " + gid + " AND Side IN (" + ml_sides.toCSVString() + ") AND Status <> " + UnitsTable.ST_NO_LONGER_EXISTS + " ORDER BY Side, OrderBy"; 
							ResultSet rs = dbs.getResultSet(sql);
							//int num = 1;
							int last_side = -1;
							EquipmentTable equipment = new EquipmentTable(dbs);
							while (rs.next()) {
								unit.selectRow(rs.getInt("UnitID"));

								if (game.getNumOfSides() > 2) {
									if (last_side != rs.getInt("Side")) {
										last_side = rs.getInt("Side");
										HTMLFunctions.StartRow(str);
										HTMLFunctions.AddCell(str, 13, "<h3>Side " + last_side + " (" + LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(last_side), true) + ")</h3>");
										HTMLFunctions.EndRow(str);
									}
								}

								String eq_name = "";
								if (unit.getCurrentEquipmentID() > 0) {
									try {
										equipment.selectRow(unit.getCurrentEquipmentID());
										eq_name = equipment.getName(true);
									} catch (Exception ex) {
										ex.printStackTrace();
									}
								}

								HTMLFunctions.StartRow(str);

								HTMLFunctions.AddCell(str, unit.getOrderBy() + ": " + unit.getName());
								HTMLFunctions.AddCell(str, unit.getStatusText());
									HTMLFunctions.AddCell(str, unit.getArmourName());
									HTMLFunctions.AddCell(str, eq_name);
								if (unit.getStatus() != UnitsTable.ST_DEAD) {
									HTMLFunctions.AddCell(str, "" + unit.getCurrentAPs() + " (" + unit.getMaxAPs() + ")");
								} else {
									HTMLFunctions.AddCell(str, "");
								}
								HTMLFunctions.AddCell(str, "" + unit.getHealth() + " (" + unit.getMaxHealth() + ")");
								UnitsTable.AddStatCell(str, max_shotskill, unit.getShotSkill());
								UnitsTable.AddStatCell(str, max_ccskill, unit.getCombatSkill());
								UnitsTable.AddStatCell(str, max_strength, unit.getStrength());
									HTMLFunctions.AddCell(str, "" + unit.getBurden());
								if (unit.getStatus() == UnitsTable.ST_DEPLOYED) {
									HTMLFunctions.AddCell(str, "" + Functions.b2YesNo(unit.isOppFire()));
								} else {
									HTMLFunctions.AddCell(str, "n/a");
								}
								if (game.isAdvancedMode()) {
									if (unit.getCurrentEnergy() <= 0 && unit.getStatus() == UnitsTable.ST_DEPLOYED) {
										str.append("<td style=\"background-color: #ff0000;\">" + unit.getCurrentEnergy() + " (" + unit.getMaxEnergy() + ")" + "</td>");
									} else {
										HTMLFunctions.AddCell(str, unit.getCurrentEnergy() + " (" + unit.getMaxEnergy() + ")");
									}
									if (unit.getCurrentMorale() <= UnitsTable.SCARED_LEVEL && unit.getStatus() == UnitsTable.ST_DEPLOYED) {
										str.append("<td style=\"background-color: #ff0000;\">" + unit.getCurrentMorale() + " (" + unit.getMaxMorale() + ")" + "</td>");
									} else {
										HTMLFunctions.AddCell(str, unit.getCurrentMorale() + " (" + unit.getMaxMorale() + ")");
									}
								}

								HTMLFunctions.EndRow(str);

								HTMLFunctions.StartRow(str);
								HTMLFunctions.StartCell(str, "", "", 13, "");
								// Show equipment
								ResultSet rs_equip = dbs.getResultSet("SELECT EquipmentID, Ammo FROM Equipment WHERE UnitID = " + unit.getID());
								boolean any = false;
								while (rs_equip.next()) {
									if (any == false) {
										str.append("<span class=\"little\">Equipment: </span>");
										any = true;
									}
									equipment.selectRow(rs_equip.getInt("EquipmentID"), true);
									str.append("<span class=\"little\"><a href=\"equipmentdetails.cls?eid=" + equipment.getEquipmentTypeID() + "\">" + equipment.getName(true) + "</a>,</span> ");
								}
								if (!any) {
									//str.append("None");
								} else {
									str.delete(str.length()-9, str.length());
								}
								HTMLFunctions.EndCell(str);
								HTMLFunctions.EndRow(str);
							}
							unit.close();
							rs.close();
							equipment.close();
							HTMLFunctions.EndTable(str);

							HTMLFunctions.Heading(str, 2, "Game Log");

							if (mission.getMissionID() == AbstractMission.SNAFU_MOONBASE_ASSAULT || mission.getMissionID() == AbstractMission.SNAFU_SABOTAGE) {
								HTMLFunctions.Para(str, "<b>Note that since this is a SNAFU mission, to preserve the anonymity of the saboteurs, it is NOT shown in the logs when a computer is destroyed.</b>");
							}
							HTMLFunctions.Heading(str, 3, "Game Log");

							str.append(GameLogTable.GetLogEntries(dbs, game, -1, this.current_login.getLoginID(), true, mission.isSnafu() == false));
						}
					} else {
						HTMLFunctions.Para(str, "You are not playing in the selected game.");
					}
				} else {
					HTMLFunctions.Para(str, "Game not found.");
				}
				this.body_html.append(MainLayout.GetHTML(this, title, str));		
			} finally {
				game.close();

			}
		} else {
			this.redirectToMainPage("");
		}
	}


	private boolean canConcede(int gid, int our_side) throws SQLException {
		// Only concede if lost half of our units
		int tot_units = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + gid + " AND Side = " + our_side);
		int remaining_units = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + gid + " AND Side = " + our_side + " AND Status = " + UnitsTable.ST_DEPLOYED);
		return (remaining_units*2 <= tot_units);

	}


	private String activateUndercoverAgent(GamesTable game, int our_side) throws SQLException {
		// Select random unit
		if (game.getNumUndercoverUnits(our_side) > 0 && (game.getTurnNo() >= DSRWebServer.EARLIEST_ACTIVATION_TURN)) {
			// Choose random side to take agent from
			int opp_sides[] = game.getOpponentSidesForSide(our_side, mission);
			int old_side = opp_sides[Functions.rnd(0, opp_sides.length-1)];
			String sql = "SELECT * FROM Units WHERE GameID = " + game.getID() + " AND Side = " + old_side + " ORDER BY RAND() LIMIT 1";
			ResultSet rs = dbs.getResultSet(sql);
			if (rs.next()) {
				UnitsTable unit = new UnitsTable(dbs);
				try {
					unit.selectRow(rs.getInt("UnitID"));
					if (unit.getStatus() == UnitsTable.ST_DEPLOYED) {
						unit.setSide(our_side);
						try {
							// Set the morale to be the average of the new side
							int new_morale = dbs.getScalarAsInt("SELECT AVG(CurrentMorale) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + our_side);
							unit.setCurrentMorale(new_morale);
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						game.reduceUndercoverAgents(our_side);
						String s = "Undercover agent " + rs.getString("Name") + " has been activated!"; 
						GameLogTable.AddRec(dbs, game, this.current_login.getID(), -1, s, true, System.currentTimeMillis());
						return s + "  If you are currently running the client, please restart it.";
					} else {
						return "I'm afraid undercover agent " + rs.getString("Name") + " is dead.";
					}
				} finally {
					unit.close();
				}
			}
		}

		return "No units available!";
	}

}
