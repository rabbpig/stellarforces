package dsrwebserver.pages.dsr;

import ssmith.html.HTMLFunctions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.pages.AbstractHTMLPage;

public class faq extends AbstractHTMLPage {

	public faq() {
		super();
	}

	
	@Override
	public void process() throws Exception {
		String full_req = AbstractHTMLPage.disk_cache.convertPath(super.conn.headers.request.getFullURL());
		StringBuffer str = new StringBuffer();
		if (AbstractHTMLPage.disk_cache.contains(full_req) == false) {
			this.setTitle(DSRWebServer.TITLE + " FAQ");

			str.append("<br clear=\"all\" />");
			appendEntry(str, "What is the difference between the Free and Full versions?", "The only difference is in the missions that can be played.  The free version is restricted to 2 missions, but the full version can play any mission.  <b>Neither version has any ads or in-app purchases.</b>");
			appendEntry(str, "What games can I join", "You can join absolutely any game from the list.  If a player wants to play against a specific opponent, they choose that opponent when creating a game, and you would not see it on the list.  So feel free to join <b>any</b> game.");
			appendEntry(str, "What can I do if my opponent has stopped playing?", "If you've sent a message to your opponent and they have not replied or wish to abandon, send an email to the usual address and a new player will be assigned.  Feel free to start another game as well.  There is no limit on the number you can play.");
			appendEntry(str, "Is there an AI to play against?", "Yes.  If you start a practise mission, you will play against an AI instead of a human.  This is good for practising, but not as much fun as playing against a real person.");
			appendEntry(str, "Is the AI a bit stupid?", "Yes.  It's only designed to give new players someone to fight against in order to practise.  You are recommended to play against another player for maximum fun.");
			appendEntry(str, "Why is the damage caused different every time?", "All damage, after adjustments for distance, strength etc., is randomized from 50%-150%.");
			appendEntry(str, "My stats on the front page aren't correct!", "These are cached and only updated once a day.");
			appendEntry(str, "Why are my units stats a bit rubbish?", "Unit stats (with the exception of campaign games) are randomly generated.");
			appendEntry(str, "Why do some events not get mention in the log?", "If the event was seen be you (i.e. it took place while you were taking your turn) it doesn't bother logging it since you'll already know about it.  The log is primarily for events that took place during your opponent's turn, or events of great importance, like telling you that you lost.");
			appendEntry(str, "Why can't I play [insert mission]?", "The more complex missions require that you play a certain number of games before you can join them to ensure you are experienced enough.  The mission page will tell you how many game you need to have played.");
			appendEntry(str, "How can I end a game I don't want to play any more?", "In the Game Details page for the particular game, there are two options for ending them game: either concede (which means you lose) or mutual concede (which needs to be accepted by your opponent but means the game is a draw).");
			appendEntry(str, "Can I play the game without Android?", "You could try using the Bluestacks App-Player (http://www.bluestacks.com/app-player.html).  Other similar options may be available.");
			appendEntry(str, "How do I join a faction?", "Once you have completed a sufficient number of games (currently 4) a link will appear on the website.  You will need the full version to play in the campaign.");
			appendEntry(str, "Why can't my units escape?", "If your unit is standing on an escape hatch, you need to select Escape from the menu");
			//appendEntry(str, "", "");

			AbstractHTMLPage.disk_cache.put(full_req, str.toString());
		} else {
			str.append(AbstractHTMLPage.disk_cache.get(full_req));
		}
		this.body_html.append(MainLayout.GetHTML(this, "FAQ", str));		

	}


	private void appendEntry(StringBuffer str, String q, String a) {
		HTMLFunctions.Heading(str, 3, q);
		HTMLFunctions.Para(str, a);


	}

}
