package dsrwebserver.pages;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import dsrwebserver.ClientConnection;
import dsrwebserver.DSRWebServer;
import dsrwebserver.HTTPHeaders;

public final class FilePage extends AbstractPage {

	private File file;
	private boolean slow_down = false;

	public FilePage() {
		super();
	}


	public void init(ClientConnection conn, HTTPHeaders head) throws SQLException {
		super.init(conn, head);
		this.content_type = DSRWebServer.ctypes.getContentTypeFromExtention(headers.request.getMainPartOfURL());
	}


	public void process() throws IOException, SQLException {
		String filename = this.headers.request.getMainPartOfURL();

		if (filename.toLowerCase().endsWith("zip") || filename.toLowerCase().endsWith(".gz")) {
			slow_down = true;
		}

		String full_path = null;
		if (filename.endsWith(".class") && filename.indexOf("client_update") < 0) { // Java applet class
			full_path = "bin" + this.headers.request.getMainPartOfURL().replaceAll("%20", " ");
		} else if (filename.endsWith(".jar")) { // Java applet class
			full_path = "." + URLDecoder.decode(this.headers.request.getMainPartOfURL(), "UTF-8");
		} else {
			full_path = "webroot" + URLDecoder.decode(this.headers.request.getMainPartOfURL(), "UTF-8");//.replaceAll("%20", " ");
		}
		file = new File(full_path);
		if (file.isDirectory()) {
			// Is there a file with a default extention?
			File f2 = new File(full_path + "index.html");
			if (f2.exists()) {
				file = f2;
			} else {
				this.setRedirectInCodeTo("DirectoryListingPage" + ClientConnection.DEF_EXT + "?url=" + this.headers.request.getMainPartOfURL());
				return;
			}
		}

		if (file.canRead()) {
			SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
			String s_date = DATE_FORMAT.format(file.lastModified());
			addHTTPHeader("Last-Modified: " + s_date);
			this.content_length = file.length();
			//this.addHTTPHeader("Content-Length: " + file.length());
		} else {
			//System.err.println("Cannot read " + full_path);
			//super.redirectTo("FileNotFound" + ClientConnection.DEF_EXT);
			throw new FileNotFoundException(full_path);
		}
	}


	public void writeContent() throws IOException {
		try {
			DataOutputStream dos = conn.getDataOutputStream();
			byte data[] = new byte[4096*8];
			FileInputStream fis = new FileInputStream(file);
			int len = fis.read(data);
			while (len > 0) {
				dos.write(data, 0, len);
				len = fis.read(data);
				if (slow_down) {
					Thread.yield();
				}
			}
			fis.close();
			dos.flush();
			//dos.close();
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


}
