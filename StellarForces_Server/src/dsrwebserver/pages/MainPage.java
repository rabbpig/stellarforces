package dsrwebserver.pages;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;

import ssmith.dbs.MySQLConnection;
import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import ssmith.util.Interval;
import ssmith.util.MemCache;
import ssmith.util.MyList;
import dsr.SharedStatics;
import dsrwebserver.DSRWebServer;
import dsrwebserver.components.MainLayout;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.dsr.helpwindow;
import dsrwebserver.pages.dsr.leaguetable;
import dsrwebserver.pages.dsr.playerspublicpage;
import dsrwebserver.pages.dsr.forums.ForumMainPage;
import dsrwebserver.tables.FactionsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.PlayerAwardsTable;
import dsrwebserver.tables.PlayerTrophiesTable;
import dsrwebserver.tables.StarmapSquaresTable;

public final class MainPage extends AbstractHTMLPage {

	// Memcache keys
	private static final String MINI_MAP = "Minimap";

	private static final int TITLE_HEIGHT = 35;

	private static StringBuffer str_recent_victories, str_trophies, str_player_awards;
	private static Hashtable<Integer, StringBuffer> str_player_stats = new Hashtable<Integer, StringBuffer>();
	public static Interval update_data_int = new Interval(Dates.HOUR, true);
	public static Interval update_player_stats_int = new Interval(Dates.DAY, false);

	private static MemCache mcache = new MemCache(Dates.HOUR);

	public MainPage() {
		super();

		this.addHTMLToHead("<meta name=\"title\" content=\"Stellar Forces\" />"); 
		this.addHTMLToHead("<meta name=\"description\" content=\"Stellar Forces is a multiplayer turn-based squad strategy game.  Equip your units for the mission you have chosen and then battle it out against another player.\" />"); 
		this.addHTMLToHead("<meta name=\"keywords\" content=\"Stellar Forces, squad, strategy, turn-based strategy, laser squad, laser squad nemesis, lsn, free, tbs, gollop\" />"); 
	}


	@Override
	public void process() throws Exception {
		if (this.conn.headers.getHost().equalsIgnoreCase("stellarforces.com")) {
			this.redirectTo_Using303("http://www.stellarforces.com");
			return;
		}
		this.setTitle("Turn based Strategy for Android");

		this.refreshData();

		StringBuffer str = new StringBuffer();

		String msg = this.headers.getGetValueAsString("warning");
		if (msg.length() > 0) {
			HTMLFunctions.Heading(str, 3, "<i>" + msg + "</i>");
		}

		if (this.session.isLoggedIn()) {
			if (this.current_login.getTotalCurrentGames(LoginsTable.GameType.ALL) == 0) { 
				HTMLFunctions.Heading(str, 2, "What next?  Download the <a href=\"https://market.android.com/details?id=com.scs.stellarforces.main.lite\">Android client</a>, and start or join a game <a href=\"/dsr/GameRequests.cls\">here</a>.");
			}
			if (this.current_login.isAdmin()) {
				int maps = dbs.getScalarAsInt("SELECT Count(*) FROM MapData WHERE COALESCE(FullyCreated, 0) = 0");
				int emails = dbs.getScalarAsInt("SELECT Count(*) FROM Emails WHERE COALESCE(Sent, 0) = 0");
				int games = dbs.getScalarAsInt("SELECT Count(*) FROM Games WHERE GameStatus = " + GamesTable.GS_STARTED + " AND TurnNo > 1");
				HTMLFunctions.Para(str, "Emails: " + emails + " / Maps:" + maps + " / Games:" + games + " / Max Conns:" + DSRWebServer.server.highest_conns);
			}
		}

		str.append("<table><tr><td valign=\"top\">");

		if (this.headers.isMobile() == false) {
			this.appendWebsiteLog(str);
			this.appendScreenshots(str);
		}

		str.append("</td><td valign=\"top\">");

		boolean show_about = true;
		if (this.session.isLoggedIn()) {
			this.appendPlayersStats(str);
			if (this.current_login.getTotalGamesFinished() > 2) {
				show_about = false;
			}
		}
		if (show_about) {
			AppendAbout(str);
		}
		AppendTeamTactics(str);

		//appendChat(str);

		this.appendLeagueTables(str);

		if (str_recent_victories != null) {
			str.append(str_recent_victories);
		}

		if (this.session.isLoggedIn()) {
			if (this.current_login.getFactionID() > 0) {
				AppendMiniStarmap(dbs, str);
			}
		}

		if (str_player_awards != null) {
			str.append(str_player_awards);
		}

		//this.appendAdvert(str);

		if (str_trophies != null) {
			str.append(str_trophies);
		}

		HTMLFunctions.Para(str, "If you like this game, why not try...");
		str.append("<a href=\"https://play.google.com/store/apps/details?id=com.scs.sorcerers.main.lite\"><img src=\"" + DSRWebServer.IMAGES_HOST + "/images/sorcerers.png\" alt=\"Sorcerers\"/></a>");
		str.append("<a href=\"https://play.google.com/store/apps/details?id=com.scs.nanowars.main\"><img src=\"/images/nanowars.png\" alt=\"Nanowars\"/></a>");

		str.append("</td></tr></table>");

		this.body_html.append(MainLayout.GetHTML(this, "", str));
	}


	/*private void appendAdvert(StringBuffer str) {
		str.append("<script type=\"text/javascript\"><!--\r\n");
		str.append("google_ad_client = \"pub-4051325490886119\";\r\n");
		// 468x60 SF
		str.append("google_ad_slot = \"3128952796\";\r\n");
		str.append("google_ad_width = 468;\r\n"); 
		str.append("google_ad_height = 60;\r\n");

		str.append("</script>\r\n");
		str.append("<script type=\"text/javascript\"\r\n");
		str.append("src=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">\r\n");
		str.append("--></script>\r\n");
	}*/


	private void appendChat(StringBuffer str) {
		str.append("<br /><img src=\"/dsr/gettitle.cls?t=IRC%20Chat&h=" + TITLE_HEIGHT + "\" border=\"0\" alt=\"IRC Chat\" /><br />");
		str.append("<iframe width=\"550\" height=\"350\" scrolling=\"no\" src=\"http://widget.mibbit.com/?settings=03afc03ee4dec4929052793c424604ee&server=irc.Mibbit.Net&channel=%23StellarForces_test\"></iframe>");
	}

	
	private void appendLeagueTables(StringBuffer str) {
		try {
			if (leaguetable.str_top10 != null) {
				str.append("<br /><a href=\"/dsr/leaguetable.cls\"><img src=\"/dsr/gettitle.cls?t=Top%20Players&h=" + TITLE_HEIGHT + "\" border=\"0\" alt=\"Top players\" /></a><br />");
				str.append(leaguetable.str_top10);
			}
		} catch (Exception ex) {
			// Cached League has no records yet!
			DSRWebServer.HandleError(ex, true);
		}
	}


	private void refreshData() throws SQLException {
		if (update_data_int.hitInterval() || str_recent_victories == null) {
			try {
				// Player awards
				str_player_awards = new StringBuffer();
				str_player_awards.append("<br /><br /><a href=\"/dsr/playerawards.cls\"><img src=\"/dsr/gettitle.cls?t=Latest%20Player%20Awards&h=30&ul=1\" border=\"0\" alt=\"Latest Player Awards\" /></a><br /><br />");
				HTMLFunctions.StartTable(str_player_awards, "stats", "", 1, "", 5);
				HTMLFunctions.StartRow(str_player_awards);
				HTMLFunctions.AddCellHeading(str_player_awards, "To");
				HTMLFunctions.AddCellHeading(str_player_awards, "Award Name");
				HTMLFunctions.AddCellHeading(str_player_awards, "From");
				HTMLFunctions.EndRow(str_player_awards);

				String sql = "SELECT * FROM PlayerAwards ORDER BY DateCreated DESC LIMIT 5";
				ResultSet rs = dbs.getResultSet(sql);
				while (rs.next()) {
					HTMLFunctions.StartRow(str_player_awards);
					HTMLFunctions.AddCell(str_player_awards, LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginIDTo"), true));
					HTMLFunctions.AddCell(str_player_awards, "Award for " + HTMLFunctions.s2HTML(rs.getString("Name")));
					HTMLFunctions.AddCell(str_player_awards, LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginIDFrom"), true));
					HTMLFunctions.EndRow(str_player_awards);
				}
				HTMLFunctions.EndTable(str_player_awards);


				// Recent victories
				str_recent_victories = new StringBuffer();
				str_recent_victories.append("<br /><br /><img src=\"/dsr/gettitle.cls?t=Recent%20Victories&h=" + TITLE_HEIGHT + "\" alt=\"Recent Victories\" /><br />");

				sql = "SELECT GameID FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI + " AND WinType <> " + GamesTable.WIN_CANCELLED + " AND WinType <> " + GamesTable.WIN_OPPONENT_CONCEDED + " ORDER BY DateFinished DESC LIMIT 5";
				rs = dbs.getResultSet(sql);
				GamesTable game = new GamesTable(dbs);
				AbstractMission mission;
				MyList<Integer> winning_logins = null;
				MyList<Integer> winning_sides = null;
				MyList<Integer> losing_logins = null;
				while (rs.next()) {
					game.selectRow(rs.getInt("GameID"));
					mission = AbstractMission.Factory(game.getMissionID());
					str_recent_victories.append("<p>" + Dates.FormatDate(game.getDateFinished(), Dates.UKDATE_FORMAT2_WITH_TIME) + ": ");
					if (game.getWinType() == GamesTable.WIN_DRAW || game.getWinType() == GamesTable.WIN_DRAW_MUTUAL_CONCEDE) {
						String names = game.getOpponentsNamesBySide(-1, true);
						str_recent_victories.append(names + " have drawn in " + AbstractMission.GetMissionNameFromType(game.getMissionID(), true, game.isCampGame()));
						if (game.getWinType() == GamesTable.WIN_DRAW_MUTUAL_CONCEDE) {
							str_recent_victories.append(" as the players mutually conceded");
						}
						//					} else if (game.getWinType() == GamesTable.WIN_CANCELLED) {
						// Do nothing
					} else {
						winning_logins = game.getWinningLoginIDs(winning_logins);
						winning_sides = game.getWinningSides(winning_sides);
						String winners = LoginsTable.GetDisplayNames_Enc(dbs, winning_logins, true);
						winners = winners  + " (" + mission.getSideDescription(winning_sides.get(0)) + ")";

						losing_logins = game.getLosingLoginIDs(losing_logins);
						String losers = LoginsTable.GetDisplayNames_Enc(dbs, losing_logins, true);
						if (game.getNumOfSides() == 2) {
							losers = losers + " (" + mission.getSideDescription(game.getOppositeSidesForSide(winning_sides.get(0))[0]) + ")";
						}
						str_recent_victories.append(winners + " has defeated " + losers + " in " + AbstractMission.GetMissionNameFromType(game.getMissionID(), true, game.isCampGame()));
						if (game.isPractise()) {
							str_recent_victories.append(" (practise)");
						}
						if (game.isCampGame()) {
							str_recent_victories.append(" (campaign)");
						}
						/*if (game.getWinType() == GamesTable.WIN_OPPONENT_CONCEDED) {
							str_recent_victories.append(" as their opponent conceded");
						}*/
					}
					str_recent_victories.append(" (<a href=\"/dsr/playbackapplet/playbackpage.cls?gid=" + game.getID() + "\" rel=\"nofollow\">Watch Playback</a>)");
					str_recent_victories.append("</p>");
				}
				game.close();
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}

			// Trophies
			try {
				str_trophies = new StringBuffer();
				//HTMLFunctions.Heading(str_trophies, 1, "<a href=\"http://wiki.stellarforces.com/doku.php?id=gameplay:trophies\">Trophy Winners</a>");
				str_trophies.append("<br /><br /><img src=\"/dsr/gettitle.cls?t=Trophy%20Winners&h=" + TITLE_HEIGHT + "\" alt=\"Trophy Winners\" /><br />");

				String sql = "SELECT * FROM PlayerTrophies ORDER BY DateCreated DESC LIMIT 2";
				ResultSet rs = dbs.getResultSet(sql);
				//HTMLFunctions.StartUnorderedList(str_trophies);
				if (rs.next()) {
					//HTMLFunctions.AddListEntry(str_trophies, LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginID"), true) + " has won the " + PlayerTrophiesTable.GetTrophyName(rs.getInt("Trophy")));
					HTMLFunctions.Heading(str_trophies, 2, LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginID"), true) + " has won the " + PlayerTrophiesTable.GetTrophyName(rs.getInt("Trophy")) + "!");
				}
				if (rs.next()) {
					//HTMLFunctions.AddListEntry(str_trophies, LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginID"), true) + " has won the " + PlayerTrophiesTable.GetTrophyName(rs.getInt("Trophy")));
					HTMLFunctions.Heading(str_trophies, 3, "Previous winner: " + LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginID"), true) + " won the " + PlayerTrophiesTable.GetTrophyName(rs.getInt("Trophy")));
				}
				//HTMLFunctions.EndUnorderedList(str_trophies);
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}

		}
	}


	private void appendWebsiteLog(StringBuffer str) throws SQLException {
		str.append("<div class=\"structural\" style=\"width:155px; float:left; \">");
		str.append("<div class=\"little\">");
		//HTMLFunctions.StartUnorderedList(str);
		ResultSet rs = dbs.getResultSet("SELECT * FROM WebsiteEvents ORDER BY DateCreated DESC LIMIT 10");
		while (rs.next()) {
			String ago = ForumMainPage.GetTimeAgo(rs.getTimestamp("DateCreated").getTime());
			//HTMLFunctions.AddListEntry(str, Dates.FormatDate(rs.getTimestamp("DateCreated"), "HH:mm") + ": " + rs.getString("Entry"));
			//str.append(Dates.FormatDate(rs.getTimestamp("DateCreated"), "HH:mm") + ": " + rs.getString("Entry") + "<br />");
			str.append(ago + ": " + rs.getString("Entry") + "<br />");
		}
		//HTMLFunctions.EndUnorderedList(str);
		str.append("</div>");
		str.append("</div>");
	}


	private void appendScreenshots(StringBuffer str) {
		int WIDTH = 145;

		str.append("<div class=\"structural\" style=\"width:155px; float:left; \">");
		//str.append("Android Screenshots<br />");
		str.append("<a href=\"" + DSRWebServer.IMAGES_HOST + "/images/android_screenshots/android1.jpg\"><img hspace=5 vspace=5 width=\"" + WIDTH + "\" src=\"" + DSRWebServer.IMAGES_HOST + "/images/android_screenshots/android1.jpg\" alt=\"Screenshot\" border=\"0\" /></a>");
		str.append("<a href=\"" + DSRWebServer.IMAGES_HOST + "/images/android_screenshots/android2.jpg\"><img hspace=5 vspace=5 width=\"" + WIDTH + "\" src=\"" + DSRWebServer.IMAGES_HOST + "/images/android_screenshots/android2.jpg\" alt=\"Screenshot\" border=\"0\" /></a>");
		str.append("<a href=\"" + DSRWebServer.IMAGES_HOST + "/images/android_screenshots/android3.jpg\"><img hspace=5 vspace=5 width=\"" + WIDTH + "\" src=\"" + DSRWebServer.IMAGES_HOST + "/images/android_screenshots/android3.jpg\" alt=\"Screenshot\" border=\"0\" /></a>");
		str.append("</div>");

		//str.append("<img hspace=5 vspace=5 width=\"" + (WIDTH*1.2f) + "\" src=\"/images/suclassic.gif\" alt=\"SU Classic\" border=\"0\" />");
		//str.append("<img hspace=5 vspace=5 width=\"" + (WIDTH*1.2f) + "\" src=\"/images/best_seller.png\" alt=\"Best Seller\" border=\"0\" />");

		/*str.append("<div class=\"structural\" style=\"width:155px; float:left; \">");
		str.append("PC Screenshots<br />");
		str.append("<a href=\"/dsr/screenshots.cls\"><img hspace=5 vspace=5 width=\"" + WIDTH + "\" src=\"" + DSRWebServer.IMAGES_HOST + "/images/screenshots/sf_ss37.png\" alt=\"Screenshot\" /></a>");
		str.append("<a href=\"/dsr/screenshots.cls\"><img hspace=5 vspace=5 width=\"" + WIDTH + "\" src=\"" + DSRWebServer.IMAGES_HOST + "/images/screenshots/sf_ss31.png\" alt=\"Screenshot\" /></a>");
		str.append("<a href=\"/dsr/screenshots.cls\"><img hspace=5 vspace=5 width=\"" + WIDTH + "\" src=\"" + DSRWebServer.IMAGES_HOST + "/images/screenshots/sf_ss38.png\" alt=\"Screenshot\" /></a>");
		str.append("</div>");*/

	}


	public static void AppendAbout(StringBuffer str) {
		//str.append("<iframe align=\"right\" width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/aY7vIkqHIbQ\" frameborder=\"0\" allowfullscreen></iframe>");

		HTMLFunctions.Para(str, "<font size=\"+1\"><b>" + SharedStatics.TITLE + " is a turn-based multi-player squad strategy game.</b></font>");
		HTMLFunctions.Para(str, "<b>Take control of an elite squad</b> and defeat opponents in one of 80+ <a href=\"/dsr/missiondescriptions.cls\">missions</a>.  Choose what to equip them with, where to deploy them, and then use whatever strategy and tactics you want to ensure victory.");
		HTMLFunctions.Para(str, "Once you have registered using the <a href=\"https://play.google.com/store/apps/details?id=com.scs.stellarforces.main.lite\">Android App</a>, you can play as many games as you want.  This game has no adverts and no in-app purchases.");
		HTMLFunctions.Para(str, "<b>This game is played in a casual way</b>" +
				".  Once you have taken your turn, your opponent is notified by e-mail that it is now their turn.  However, they may not take it straight away.  Typically, expect to take a turn and then wait about a day to take your next turn.  Of course, you can play as many concurrent games as you can handle if you get bored in the meantime!");
	}


	private static void AppendTeamTactics(StringBuffer str) {
		str.append("<a href=\"http://www.stellarforces.com/dsr/forums/ForumPostingsPage.cls?topic=17270\"><img src=\"/images/teamtactics/team_tactics_logo.png\" border=0/></a>");
		HTMLFunctions.Para(str, "<h3><a href=\"http://teamtactics.penultimateapps.com/\">Team Tactics</a>, a new realtime version of Stellar Forces.  Play it now for free.</h3>");
	}


	private static void AppendMiniStarmap(MySQLConnection dbs, StringBuffer str2) throws SQLException {
		if (mcache.containsKey(MINI_MAP) == false) {
			StringBuffer str = new StringBuffer();

			str.append("<br /><a href=\"/dsr/starmap.cls\"><img src=\"/dsr/gettitle.cls?t=Galaxy%20Map&h=30&ul=1\" border=\"0\" alt=\"Galaxy Map\" /></a><br /><br />");

			str.append("<table><tr><td width=\"50%\">");

			str.append("<table width=\"100%\">");
			int size = StarmapSquaresTable.SIZE;
			for (int y=0 ; y<size ; y++) {
				str.append("<tr>");
				for (int x=0 ; x<size ; x++) {
					ResultSet rs_mapsquares = dbs.getResultSet("SELECT * FROM StarmapSquares WHERE MapX = " + x + " AND MapY = " + y);
					if (rs_mapsquares.next()) {
						str.append("<td style=\"background: #" + FactionsTable.GetColour(dbs, rs_mapsquares.getInt("FactionID")) + ";\" >&nbsp;");
						//str.append("[" + rs_mapsquares.getInt("MapX") + "," + rs_mapsquares.getInt("MapY") + "] "); 
					} else {
						str.append("<td>");
					}
					str.append("</td>");
				}
				str.append("</tr>");
			}
			HTMLFunctions.EndTable(str);

			str.append("</td><td>");

			HTMLFunctions.StartUnorderedList(str);
			ResultSet rs = dbs.getResultSet("SELECT * FROM StarmapLog ORDER BY DateCreated DESC LIMIT 4");
			while (rs.next()) {
				HTMLFunctions.AddListEntry(str, Dates.FormatDate(rs.getTimestamp("DateCreated"), "dd MMM yyyy") + ": " + rs.getString("Entry"));
			}
			HTMLFunctions.EndUnorderedList(str);

			str.append("</td></tr></table>");

			str.append("<br />");

			rs = dbs.getResultSet("SELECT * FROM Factions");
			while (rs.next()) {
				str.append("<span style=\"background: #" + rs.getString("Colour") + "\">" + rs.getString("Name") + "</span> - ");
			}
			str.delete(str.length()-2, str.length());
			mcache.put(MINI_MAP, str);
		}
		str2.append(mcache.get(MINI_MAP));

	}


	private void appendPlayersStats(StringBuffer body) throws SQLException {
		if (update_player_stats_int.hitInterval()) {
			str_player_stats.clear();
		}
		if (str_player_stats.get(this.current_login.getID()) != null) {
			body.append(str_player_stats.get(this.current_login.getID()));
		} else {
			//update_player_stats_int.fireInterval(); // So we don't do it again straight away

			StringBuffer str = new StringBuffer();

			//HTMLFunctions.Heading(str, 2, "Your Stats");
			str.append("<br /><img src=\"/dsr/gettitle.cls?t=Your%20Stats&h=30&ul=0\" border=\"0\" alt=\"Your Stats\" /><br />");

			HTMLFunctions.StartTable(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "<nobr>Games Played: </nobr>");
			HTMLFunctions.AddCell(str, "" + this.current_login.getTotalGamesFinished());
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "<nobr>League Points: </nobr>");
			HTMLFunctions.AddCell(str, "" + this.current_login.getELOPoints());
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Current Games: ");
			HTMLFunctions.AddCell(str, "" + this.current_login.getTotalCurrentGames(LoginsTable.GameType.ALL));
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Victories: ");
			HTMLFunctions.AddCell(str, "" + this.current_login.getTotalVictories(LoginsTable.GameType.ALL));
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Draws: ");
			HTMLFunctions.AddCell(str, "" + this.current_login.getTotalDraws(LoginsTable.GameType.ALL));
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Defeats: ");
			HTMLFunctions.AddCell(str, "" + this.current_login.getTotalDefeats(LoginsTable.GameType.ALL));
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Total Concedes: ");
			HTMLFunctions.AddCell(str, "" + this.current_login.getTotalConcedes());
			HTMLFunctions.EndRow(str);

			// Trophies
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Trophies:" + helpwindow.GetLink(helpwindow.HC_TROPHIES));
			if (PlayerTrophiesTable.DoesPlayerHaveAnyTrophy(dbs, this.current_login.getID())) {
				str.append("<td>");
				boolean any = false;
				for (int i=1 ; i<= PlayerTrophiesTable.MAX_TROPHIES ; i++) {
					if (PlayerTrophiesTable.DoesPlayerHaveTrophy(dbs, this.current_login.getID(), i)) {
						any = true;
						str.append(PlayerTrophiesTable.GetTrophyName(i) + ", ");
						//str.append("<img width=\"150\" src=\"" + PlayerTrophiesTable.GetTrophyImageURL(i) + "\" alt=\"" + PlayerTrophiesTable.GetTrophyName(i) + "\" /><br>");
					}
				}
				if (any) {
					str.delete(str.length()-2, str.length());
				}
				str.append("</td>");
			} else {
				HTMLFunctions.AddCell(str, "None yet!");
			}
			HTMLFunctions.EndRow(str);

			// Awards
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Awards:");
			if (PlayerAwardsTable.DoesPlayerHaveAward(dbs, this.current_login.getID())) {
				str.append("<td>");
				String sql = "SELECT * FROM PlayerAwards WHERE LoginIDTo = " + this.current_login.getID() + " ORDER BY DateCreated DESC";
				ResultSet rs = dbs.getResultSet(sql);
				while (rs.next()) {
					str.append(HTMLFunctions.s2HTML(rs.getString("Name")) + " (from " + LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("LoginIDFrom"), true) + "), ");
				}
				str.delete(str.length()-2, str.length());
				str.append("</td>");
			} else {
				HTMLFunctions.AddCell(str, "None yet!");
			}
			HTMLFunctions.EndRow(str);

			// Arch nemesis
			try {
				String sql = "select PID, count(*) as Tot from (";
				for (int i=1 ; i<=4 ; i++) {
					sql = sql + " select Player1ID as PID from Games WHERE (Player1ID = " + this.current_login.getID() + " or Player2ID = " + this.current_login.getID() + " or player3ID = " + this.current_login.getID() + " or player4id = " + this.current_login.getID() + ") and WinningSide <> " + i;
					if (i < 4) {
						sql = sql + " UNION ALL";
					}
				}
				sql = sql + ") X where PID <> " + this.current_login.getID() + " and PID <> 0 and PID IS NOT NULL";
				sql = sql + " group by pid order by Tot desc";
				ResultSet rs = dbs.getResultSet(sql);
				if (rs.next()) {
					HTMLFunctions.StartRow(str);
					HTMLFunctions.AddCell(str, "Arch Nemesis: ");
					HTMLFunctions.AddCell(str, LoginsTable.GetDisplayName_Enc(dbs, rs.getInt(1), true));
					HTMLFunctions.EndRow(str);
				}

			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "<nobr>Total <a href=\"" + DSRWebServer.NEW_FORUM_LINK  +"\">Forum</a> Posts: </nobr>");
			int tot = this.current_login.getTotalForumPosts();
			try {
				MySQLConnection phpdbs = DSRWebServer.getPHPDbs();
				if (phpdbs != null) {
					tot += phpdbs.getScalarAsInt("SELECT Count(*) FROM phpbb_posts WHERE poster_id = (SELECT user_id FROM phpbb_users WHERE username = '" + this.current_login.getDisplayName_Enc(false) + "')");
				}
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}
			HTMLFunctions.AddCell(str, "" + tot);
			HTMLFunctions.EndRow(str);

			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, "Favourite Mission: ");
			try {
				HTMLFunctions.AddCell(str, playerspublicpage.GetMostPopularMission(dbs, this.current_login.getID()));
			} catch (Exception ex) {
				//DSRWebServer.HandleError(ex, true);
			}
			HTMLFunctions.EndRow(str);

				HTMLFunctions.StartRow(str);
				HTMLFunctions.AddCell(str, "Missions Not Played: ");
				try {
					HTMLFunctions.AddCell(str, this.current_login.getUnplayedMissions(5));
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex, true);
				}
				HTMLFunctions.EndRow(str);
			HTMLFunctions.EndTable(str);

			str_player_stats.put(this.current_login.getID(), str);

			body.append(str);
		}
	}

}	

