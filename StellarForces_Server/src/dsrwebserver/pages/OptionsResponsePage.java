package dsrwebserver.pages;

import java.io.IOException;

public final class OptionsResponsePage extends AbstractPage {

	public void process() throws Exception {
		super.addHTTPHeader("Public: OPTIONS, GET, HEAD, POST");
        //super.addHTTPHeader("Content-Length: 0");
		this.content_length = 0;
	}

	protected void writeContent() throws IOException {
		// Do nothing
	}
	
}
