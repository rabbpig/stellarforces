package dsrwebserver.pages;

import ssmith.html.HTMLFunctions;

public final class TempUnavailable extends AbstractHTMLPage {
	
	public TempUnavailable() {
		super();
		this.setHTTPCode(503);
		send_google_analytics = false;
	}

	public void process() throws Exception {
		this.setTitle("Temporarily Unavailable");
		HTMLFunctions.Heading(this.body_html, 1, "Temporarily Unavailable");
		HTMLFunctions.Para(this.body_html, "Sorry, this page is temporarily unavailable.  Please try again later.");
		this.AddStdFooter(this.body_html);
	}

}
