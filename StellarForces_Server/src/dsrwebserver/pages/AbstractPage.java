package dsrwebserver.pages;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import ssmith.dbs.MySQLConnection;
import ssmith.util.KeyValuePair;
import dsrwebserver.ClientConnection;
import dsrwebserver.DSRWebServer;
import dsrwebserver.HTTPHeaders;

public abstract class AbstractPage {

	protected static final SimpleDateFormat COOKIE_DATE_FORMAT = new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss z");

	protected HTTPHeaders headers;
	private ArrayList<String> http_headers_to_add = new ArrayList<String>();
	protected String content_type = "text/html; charset=UTF-8"; // default, gets changed if required.
	private int http_code = 200; // Default
	public ClientConnection conn;
	private String redirect_to = "";
	private ArrayList<KeyValuePair> cookies_to_add = new ArrayList<KeyValuePair>();
	private boolean header_sent = false;
	public MySQLConnection dbs;
	protected long content_length = -1; // Set this to send the length

	public void init(ClientConnection c, HTTPHeaders head) throws SQLException {
		conn = c;
		headers = head;
		dbs = DSRWebServer.dbs;
	}

	
	public void setHTTPCode(int i) {
		this.http_code = i;
	}

	
	public int getHTTPErrorCode() {
		return this.http_code;
	}
	

	protected void setRedirectInCodeTo(String to) {
		this.redirect_to = to;
	}

	
	public String getRedirectTo() {
		return this.redirect_to;
	}

	
	public void sendPage() throws Exception {
		if (header_sent == false) {
			this.writeHeader();
			header_sent = true;
		}
		if (this.headers.request.isHeadRequest() == false && this.headers.request.isOptionsRequest() == false) {
			this.writeContent();
		}
	}

	
	public abstract void process() throws Exception;

	
	protected abstract void writeContent() throws Exception;

	
	protected void writeHeader() throws IOException {
		Calendar c1 = Calendar.getInstance();
		String s_date = COOKIE_DATE_FORMAT.format(c1.getTime());
		String line1 = "HTTP/1.1 " + http_code + (http_code == 200 ? " OK" : "");
		if (DSRWebServer.VERBOSE) {
			DSRWebServer.p("Responding with '" + line1 + "'");
		}
		this.writeString(line1);
		this.writeString("Date: " + s_date + " GMT"); // Format: Mon, 23 May 2005 22:38:34 GMT
		this.writeString("Server: " + DSRWebServer.WEBSERVER_NAME);
		this.writeString("Content-Type: " + content_type);
		if (this.content_length >= 0) {
			this.writeString("Content-Length: " + this.content_length); 
		}
		if (conn.headers.isKeepAlive() == false) {
			this.writeString("Connection: close");
		}

		if (this.http_headers_to_add.size() > 0) {
			for(int i=0 ; i<this.http_headers_to_add.size() ; i++) {
				String s = (String)this.http_headers_to_add.get(i);
				this.writeString(s);
			}
		}

		if (cookies_to_add.size() > 0) {
			c1.add(Calendar.DAY_OF_YEAR, 7);
			String s = COOKIE_DATE_FORMAT.format(c1.getTime());
			for (int i=0 ; i<cookies_to_add.size() ; i++) {
				KeyValuePair kvp = (KeyValuePair)cookies_to_add.get(i);
				// ; path=/
				this.writeString("Set-Cookie: " + kvp.key + "=" + kvp.value + "; Expires=" + s + "; httponly;"); 
			}
		}
		this.writeString("");

	}

	
	protected void writeString(String s) throws IOException { // changed this!
		conn.getPrintWriter().println(s);
	}

	
	protected void redirectToMainPage(String get) throws SQLException {
		this.redirectTo_Using303("/MainPage.cls?" + get);
	}

	
	protected void redirectTo_Using303(String url) throws SQLException {
		if (url.length() == 0) {
			url = "?"; // Otherwise IE says "page cannot be displayed"
		}
		this.setHTTPCode(303);
		this.addHTTPHeader("Location: " + url);
		//this.addHeader("<meta http-equiv=\"refresh\" content=\"0; URL=\"MainPage\" + Server.DEF_EXTENTION\">");  BAD PRACTISE
	}

	
	protected void addHTTPHeader(String html) {
		this.http_headers_to_add.add(html);
	}

	
	protected void addCookie(String name, String val) {
		KeyValuePair kvp = new KeyValuePair(name, val);
		cookies_to_add.add(kvp);
	}
	
	
	protected void flush() throws IOException {
		this.conn.getDataOutputStream().flush();
	}

	
	protected void close() throws IOException {
		this.conn.close();
	}
	
	
	public String getURL() {
		return headers.request.getFullURL();
	}

}
