package dsrwebserver;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ssmith.lang.Functions;
import ssmith.net.NetworkMultiServer2;
import ssmith.net.NetworkMultiServerConn2Text;
import dsrwebserver.pages.AbstractPage;
import dsrwebserver.pages.ErrorPage;
import dsrwebserver.pages.FileNotFoundPage;
import dsrwebserver.pages.FilePage;
import dsrwebserver.pages.MainPage;
import dsrwebserver.pages.OptionsResponsePage;
import dsrwebserver.pages.dsr.api.API;
import dsrwebserver.pages.dsr.forums.ForumMainPage;

public final class ClientConnection extends NetworkMultiServerConn2Text {

	public static final String DEF_EXT = ".cls";
	private static final int TIMEOUT = 60 * 1000;

	public HTTPHeaders headers;

	public ClientConnection(NetworkMultiServer2 svr, Socket sck) throws IOException, SQLException {
		super(svr, sck);
		
		sck.setSoTimeout(TIMEOUT);
		
		start();
	}


	public void run() {
		try {
			while (sck.isConnected()) {
				try {
					// Read clients data
					long stop_waiting_at = System.currentTimeMillis() + TIMEOUT; // Does FF keep using the same connection all the time?
					while (this.getDataInputStream().available() <= 0) {
						Functions.delay(100);
						if (System.currentTimeMillis() > stop_waiting_at) {
							throw new ClientException("Client timed-out");
						}
					}

					//long start_time = System.currentTimeMillis();

					//headers = new HTTPHeaders(this.getBufferedReader());
					headers = new HTTPHeaders(this.getDataInputStream());
					
					//headers.getUserAgent();

					// Log it
					//String client = sck.getInetAddress().getHostAddress();
					/*if (client.indexOf("127.0.0.1") < 0 && client.indexOf("localhost") < 0) {
						WebRequestsTable.addRequest(DSRWebServer.dbs, headers.getHost(), client, headers.request.getFullURL(), headers.getReferer(), headers.getUserAgent());
					}*/

					sck.setKeepAlive(headers.isKeepAlive());

					AbstractPage page = null;
					try {
						boolean page_completed = false;
						while (!page_completed) {
							String url = headers.request.getMainPartOfURL();
							// Which page do they want?
							if (headers.request.isOptionsRequest()) {
								page = new OptionsResponsePage();
							} else if (url.equalsIgnoreCase("/")) { // Root
								page = getRootPage();
							} else if (url.endsWith("/viewforum.php")) { // PHP redirect page
								page = new ForumMainPage();
							} else if (url.endsWith(DEF_EXT)) { // Processable page
								page = this.getPageFromName(url);
							} else if (url.startsWith("/API/")) { // Processable page
								page = new API();
							} else {
								page = new FilePage(); // File page
							}

							// Process the page
							page.init(this, headers);
							page.process(); // This needs to be seperate to sending it so that we can check for errors before sending anything, and so we know how long its going to be.
							if (page.getRedirectTo().length() > 0) {
								headers.blank("get", page.getRedirectTo()); // Stop logout getting caught in a loop
							} else {
								page_completed = true;
							}
						}
					} catch (FileNotFoundException ex) {
						//ex.printStackTrace();
						page = new FileNotFoundPage();//ex.getMessage());
						try {
							page.init(this, headers);
							page.process();
						} catch (Exception ex2) {
							DSRWebServer.HandleError(ex2, true);
						}
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex, true);
						page = new ErrorPage(500, "", ex);
						try {
							page.init(this, headers);
							page.process();
						} catch (Exception ex2) {
							DSRWebServer.HandleError(ex2, true);
						}
					}

					page.sendPage();
					sck.getOutputStream().flush();

					if (headers.isKeepAlive() == false) {
						break;
					}
				} catch (IOException e) {
					e.printStackTrace();
					/*if (e instanceof ClientIOException == false || e instanceof SocketTimeoutException) { // Don't send us the error!
						DSRWebServer.HandleError(e, false);
					}*/
					try {
						sck.getOutputStream().flush();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			DSRWebServer.HandleError(e, true);
		} finally {
			this.close();
		}
	}


	private AbstractPage getRootPage() {
		return new MainPage();
	}


	private String getPackageName() {
		return "";
	}


	private AbstractPage getPageFromName(String name) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		name = name.replaceAll("//", "/");
		if (name.startsWith("/") == false) {
			name = "/" + name; // We need to "/" to convert into a "."?
		}
		try {
			String pkg_name = this.getPackageName();
			String c = "dsrwebserver.pages" + pkg_name + name.replace("/", ".").replace(ClientConnection.DEF_EXT, "");
			//System.out.println("class: " + c);
			Class pageclass = Class.forName(c);
			return (AbstractPage) pageclass.newInstance();
		} catch (java.lang.ClassCastException e) {
			//return new FileNotFoundPage();//name);
			return getRootPage();
		} catch (java.lang.ClassNotFoundException e) {
			//return new FileNotFoundPage();//name);
			return getRootPage();
			// Try and find it!
			/*String dir = "./bin/" + (GameOnWebServer.cfgfile.get("page_class_prefix").toString() + pkg_name).replace(".", "/");
			String file_to_find = name.replace(ClientConnection.DEF_EXT, "") + ".class";
			String found = findFileInDir(file_to_find, dir);
			if (found != null) {
				return getPageFromName(found.replace(".class", ".cls"));
			}
			return new FileNotFoundPage(name);*/
		} catch (java.lang.NoClassDefFoundError e) {
			// This doesn't work in Linux!
			Pattern p = Pattern.compile("wrong name.*\\)", Pattern.CASE_INSENSITIVE);
			Matcher match = p.matcher(e.getMessage());
			if (match.find()) {
				String s = e.getMessage().substring(match.start() + 12, match.end() - 1);
				/*while (s.indexOf("/") > 0) {
					s = s.substring(s.indexOf("/") + 1, s.length());
				}
				//System.out.println("Extract class /" + s + ClientConnection.DEF_EXT);
				return getPageFromName("/" + s + ClientConnection.DEF_EXT);*/
				Class pageclass = Class.forName(s.replace("/", "."));
				return (AbstractPage) pageclass.newInstance();
			} else {
				return new FileNotFoundPage();//name);
			}
		}

	}

	
/*	private AbstractPage runGroovy() throws CompilationFailedException, IOException, InstantiationException, IllegalAccessException, ResourceException, ScriptException {
		String[] roots = new String[] { ".", "/my/groovy/script/path" };
		GroovyScriptEngine gse = new GroovyScriptEngine(roots);
		Binding binding = new Binding();
		binding.setVariable("input", "world");
		
		AbstractHTMLPage
		
		gse.run("HelloWorld.groovy", binding);
		System.out.println(binding.getVariable("output"));
	}
	
	
	private void runGroovy_Works() throws CompilationFailedException, IOException, InstantiationException, IllegalAccessException {
		ClassLoader parent = getClass().getClassLoader();
		GroovyClassLoader loader = new GroovyClassLoader(parent);
		Class groovyClass = loader.parseClass(new File("HelloWorld.groovy"));

		// let's call some method on an instance
		GroovyObject groovyObject = (GroovyObject) groovyClass.newInstance();
		Object[] args = {};
		groovyObject.invokeMethod("run", args);

	}
*/
/*	private static File result;
	private String findFileInDir(String file_to_find, String dir) {
		File f = new File(dir);
		File file[] = f.listFiles();
		for (int i=0 ; i<file.length ; i++) {
			if (file[i].isDirectory()) {
				this.findFileInDir(file_to_find, file[i].getAbsolutePath());
				if (result != null) {
					return result.getAbsolutePath();
				}
			} else {
					if (file[i].toString().toLowerCase().equalsIgnoreCase(GameOnWebServer.cfgfile.get("webroot").toString() + file_to_find)) {
						result = file[i];
						return result.getAbsolutePath();
					}
				}
		}
		return null;
	}
*/
}
