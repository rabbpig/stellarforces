package dsrwebserver.maps;

import ssmith.lang.Functions;
import dsr.TextureStateCache;
import dsrwebserver.tables.MapDataTable;

/**
 * This is for a Alien Encounter mission.
 *
 */
public class ContainmentMapGen extends MapData { 

	private static final int LAYERS = 3;
	private static final int SIZE = 40;

	public ContainmentMapGen() {
		super(SIZE);

		// clear the Map
		for (byte y=0 ; y<this.getMapHeight() ; y++) {
			for (byte x=0 ; x<this.getMapWidth() ; x++) {
				map[x][y] = new ServerMapSquare(MapDataTable.MT_FLOOR, x, y, (byte)0, false);
				map[x][y].texture_code = TextureStateCache.TEX_ALIEN_PURPLE;
			}
		}


		int sx = SIZE/2;
		int size = 0;
		boolean doors_ew = false;
		for (int r=1 ; r<=LAYERS ; r++) {
			sx -= 4;
			size += 8;
			int x = sx;
			int y = sx;
			int w = size;
			int h = size;

			byte deploy = 0;
			if (r == 1) {
				deploy = 2;
			}
			this.createRoomByTopLeft(x, y, w, h, deploy, doors_ew);
			doors_ew = !doors_ew;
		}

		// Sprinkle individual walls
		for (int r=1 ; r<=40 ; r++) {
			byte x = Functions.rndByte(4, SIZE-5);
			byte y = Functions.rndByte(4, SIZE-5);
			AbstractMapSquare ms = map[x][y];
			if (ms.door_type == 0) { // Don't block doors
				ms.major_type = MapDataTable.MT_WALL;
				ms.texture_code = TextureStateCache.TEX_CELLS3;
			}
		}		

		createSide1DeploySquares();

		createEscapeHatches();

	}


	private void createRoomByTopLeft(int x, int y, int w, int h, byte deploy_side, boolean doors_ew) {
		for (int y2=y ; y2<=y+h ; y2++) {
			for (int x2=x ; x2<=x+w ; x2++) {
				AbstractMapSquare ms = map[x2][y2];
				if (y2 == y || y2 == y+h || x2 == x || x2 == x+w) { // Is it the wall?
					ms.major_type = MapDataTable.MT_WALL;
					ms.door_type = 0;
					ms.texture_code = TextureStateCache.TEX_CELLS3;
					// Create door?
					if (x2 == (x+x+w)/2 && doors_ew) {
						ms.major_type = MapDataTable.MT_FLOOR;
						ms.door_type = MapDataTable.DOOR_EW;
						ms.texture_code = TextureStateCache.TEX_ALIEN_GREEN;
					} else if (y2 == (y+y+h)/2 && !doors_ew) {
						ms.major_type = MapDataTable.MT_FLOOR;
						ms.door_type = MapDataTable.DOOR_NS;
						ms.texture_code = TextureStateCache.TEX_ALIEN_GREEN;
					}
				} else { // Floor
					if (deploy_side > 0) {
						ms.major_type = MapDataTable.MT_FLOOR;
						ms.texture_code = TextureStateCache.TEX_ALIEN_GREEN;
						ms.deploy_sq_side = deploy_side;
					}
				}
			}
		}

	}


	private void createSide1DeploySquares() {
		for (byte y2=0 ; y2<SIZE ; y2++) {
			for (byte x2=0 ; x2<SIZE ; x2++) {
				if (y2 == 1 || y2 == SIZE-2 || x2 == 1 || x2 == SIZE-2) {
					AbstractMapSquare ms = map[x2][y2];
					ms.deploy_sq_side = 1;
				}
			}
		}

	}


	private void createEscapeHatches() {
		for (byte y2=0 ; y2<SIZE ; y2++) {
			for (byte x2=0 ; x2<SIZE ; x2++) {
				if (y2 == 0 || y2 == SIZE-1 || x2 == 0 || x2 == SIZE-1) {
					AbstractMapSquare ms = map[x2][y2];
					ms.escape_hatch_side = 2;
					ms.texture_code = TextureStateCache.TEX_ESCAPE_HATCH;
				}
			}
		}

	}




}
