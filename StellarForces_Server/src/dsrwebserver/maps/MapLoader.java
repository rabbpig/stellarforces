package dsrwebserver.maps;

import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.io.TextFile;
import ssmith.lang.Functions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UploadedMapsTable;

public class MapLoader {

	private MapLoader() {
	}

	public static MapData Import(MySQLConnection dbs, int gameid, String filename, boolean create_data) throws IOException, SQLException {
		int uploadedmapid = -1;
		if (gameid > 0) { // It's -1 if we're viewing the mission map
			GamesTable game = new GamesTable(dbs);
			game.selectRow(gameid);
			uploadedmapid = game.getUploadedMapID();
			game.close();
		}

		TextFile tf = new TextFile();
		MapData map = null;
		String path = null;
		try {
			if (uploadedmapid > 0) {
				path = DSRWebServer.UPLOADED_MAPS_DIR + UploadedMapsTable.GetActualFilename(dbs, uploadedmapid);
				//System.out.println("Loading map " + path);
				//tf.openFile(path, TextFile.READ);
			} else {
				//tf.openFile(DSRWebServer.SERVER_DATA_DIR + "new_maps/" + filename, TextFile.READ);
				path = DSRWebServer.SERVER_DATA_DIR + "new_maps/" + filename;
			}
			if (new File(path).canRead() == false) {
				throw new IOException("Unable to load " + path);
			}
			tf.openFile(path, TextFile.READ);
			String line[] = tf.readLine().split(",");
			int size = Functions.ParseInt(line[0]);
			map = new MapData(size);
			map.random_deploy_squares.clear();
			byte z=0;
			while (tf.isEOF() == false) { // Loop through each line of the file
				line = tf.readLine().replaceAll("\"", "").split(",");
				if (z<size) {
					for (byte x=0 ; x<size ; x++) { // Loop through each section of the line
						ServerMapSquare sq = map.getSq(x, z);
						String data[] = null;
						try {
							data = line[x].split("\\|");
						} catch (java.lang.ArrayIndexOutOfBoundsException ex2) {
							ex2.printStackTrace();
						}
						for (int i=0 ; i<data.length ; i++) { // Loop through each bit of data in the cell
							if (data[i].length() > 0) {
								String subdata[] = data[i].split("\\:");
								if (subdata[0].equalsIgnoreCase("NOTHING")) {
									sq.major_type = MapDataTable.MT_NOTHING;
								} else if (subdata[0].equalsIgnoreCase("FLOOR")) {
									sq.major_type = MapDataTable.MT_FLOOR;
									sq.texture_code = Short.parseShort(subdata[1]);
								} else if (subdata[0].equalsIgnoreCase("WALL")) {
									sq.major_type = MapDataTable.MT_WALL;
									sq.texture_code = Short.parseShort(subdata[1]);
								} else if (subdata[0].equalsIgnoreCase("COMP")) {
									sq.major_type = MapDataTable.MT_COMPUTER;
									sq.texture_code = Short.parseShort(subdata[1]);
								} else if (subdata[0].equalsIgnoreCase("DOOR")) {
									sq.door_type = Byte.parseByte(subdata[1]);
									sq.major_type = MapDataTable.MT_FLOOR; // Override the default just in case
								} else if (subdata[0].equalsIgnoreCase("OWNER")) {
									sq.owner_side = Byte.parseByte(subdata[1]);
								} else if (subdata[0].equalsIgnoreCase("DEPLOY")) {
									byte side = Byte.parseByte(subdata[1]);
									sq.deploy_sq_side = side;
									map.num_deploy_squares[side]++;
								} else if (subdata[0].equalsIgnoreCase("ESCAPE")) {
									sq.escape_hatch_side = Byte.parseByte(subdata[1]);
								} else if (subdata[0].equalsIgnoreCase("RND_DEPLOY")) {
									map.random_deploy_squares.add(new Point(x, z));
								} else if (subdata[0].equalsIgnoreCase("BARREL")) {
									// Do nothing at the mo
								} else if (subdata[0].equalsIgnoreCase("SCENERY")) {
									sq.scenery_code = Short.parseShort(subdata[1]);
									if (subdata.length >= 3) {
										sq.scenery_direction = Byte.parseByte(subdata[2]);
									}
									if (create_data) {
										checkForEquipmentScenery(dbs, gameid, map, sq);
									}
								} else if (subdata[0].equalsIgnoreCase("RAISED_FLOOR")) {
									sq.raised_texture_code = Short.parseShort(subdata[1]);
								} else {
									throw new RuntimeException("Unknown code: " + subdata[0]);
								}
							}
						}
					}
				}
				z++;
			}
		} finally {
			tf.close();
		}

		return map;
	}


	private static void checkForEquipmentScenery(MySQLConnection dbs, int gameid, MapData map, ServerMapSquare sq) throws SQLException {
		if (sq.scenery_code == MapDataTable.BARREL1) {
			AbstractMission.AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_GASCANNISTER, sq.x, sq.y, -1, -1);
		} else if (sq.scenery_code == MapDataTable.BARREL2) {
			//AbstractMission.AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_GASCANNISTER, sq.x, sq.y);
		}
	}
}

