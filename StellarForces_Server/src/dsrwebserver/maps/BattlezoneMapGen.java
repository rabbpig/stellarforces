package dsrwebserver.maps;

import ssmith.lang.Functions;
import dsr.TextureStateCache;
import dsrwebserver.tables.MapDataTable;

/**
 * This is for a Alien Encounter mission.
 *
 */
public class BattlezoneMapGen extends MapData { 

	private static final int LAYERS = 3;
	private static final int SIZE = 40;

	public BattlezoneMapGen() {
		super(SIZE);

		// clear the Map
		for (byte y=0 ; y<this.getMapHeight() ; y++) {
			for (byte x=0 ; x<this.getMapWidth() ; x++) {
				map[x][y] = new ServerMapSquare(MapDataTable.MT_FLOOR, x, y, (byte)0, false);
				map[x][y].texture_code = TextureStateCache.TEX_ALIEN_PURPLE;
			}
		}


		// Sprinkle big walls
		for (int r=1 ; r<=20 ; r++) {
			byte x = Functions.rndByte(4, SIZE-8);
			byte y = Functions.rndByte(1, SIZE-5);
			/*AbstractMapSquare ms = map[x][y];
			ms.major_type = MapDataTable.MT_WALL;
			ms.texture_code = TextureStateCache.TEX_CELLS3;*/
			createRoomByTopLeft(x, y);
		}		

		// Sprinkle little walls
		for (int r=1 ; r<=20 ; r++) {
			byte x = Functions.rndByte(4, SIZE-5);
			byte y = Functions.rndByte(1, SIZE-2);
			AbstractMapSquare ms = map[x][y];
			ms.major_type = MapDataTable.MT_WALL;
			ms.door_type = 0;
			ms.texture_code = TextureStateCache.TEX_CELLS3;
		}		

		createDeploySquares();


	}


	private void createRoomByTopLeft(int x, int y) {
		int h=3, w=3;
		for (int y2=y ; y2<=y+h ; y2++) {
			for (int x2=x ; x2<=x+w ; x2++) {
				AbstractMapSquare ms = map[x2][y2];
				ms.major_type = MapDataTable.MT_WALL;
				ms.door_type = 0;
				ms.texture_code = TextureStateCache.TEX_CELLS3;
			}
		}

	}


	private void createDeploySquares() {
		for (byte y2=0 ; y2<SIZE ; y2++) {
			AbstractMapSquare ms = map[0][y2];
			ms.deploy_sq_side = 1;
			ms = map[SIZE-1][y2];
			ms.deploy_sq_side = 2;
		}

	}


}
