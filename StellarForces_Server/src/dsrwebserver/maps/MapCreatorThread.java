package dsrwebserver.maps;

import ssmith.dbs.MySQLConnection;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.MapDataTable;

public class MapCreatorThread extends Thread {
	
	private MySQLConnection dbs;
	private int gameid;
	private AbstractMission mission;

	public MapCreatorThread(MySQLConnection _dbs, AbstractMission _mission, int _gameid) {
		super("MapCreatorThread");
		
		this.setDaemon(false);
		this.setPriority(Thread.MIN_PRIORITY+1);
		
		dbs =_dbs;
		mission =_mission;
		gameid = _gameid;
		
		start();
	}

	
	public void run() {
		try {
			mission.createMap(dbs, gameid);
			//DSRWebServer.p("Finished creating map.");
			MapDataTable.SetFullyCreated(dbs, MapDataTable.GetMapIDFromGameID(dbs, gameid));
			mission.mapCreated(dbs, gameid);
		} catch (Exception e) {
			DSRWebServer.HandleError(e, true);
		}
	}

}
