package dsrwebserver.maps;

public final class ServerMapSquare extends AbstractMapSquare {
	
	public ServerMapSquare(int major_type, int x, int z, int destroyed, boolean door_open) {
		super(major_type, x, z, destroyed, door_open);
	}

}
