package dsrwebserver.maps;

import java.awt.Point;
import java.util.ArrayList;

import ssmith.astar.AStar;
import ssmith.lang.Functions;
import dsr.TextureStateCache;
import dsrwebserver.tables.MapDataTable;

/**
 * This is for a Alien Encounter mission.
 *
 */
public class KillTheQueenMapGen extends MapData { 

	private static final int MAX_ROOM_SIZE = 8;
	private static final int SIZE = 40;

	private ArrayList<Point> centres = new ArrayList<Point>();

	public KillTheQueenMapGen(int ROOMS) {
		super(SIZE);

		while (true) {
			// clear the Map
			for (int y=0 ; y<this.getMapHeight() ; y++) {
				for (int x=0 ; x<this.getMapWidth() ; x++) {
					map[x][y] = new ServerMapSquare(MapDataTable.MT_FLOOR, x, y, (byte)0, false);
					map[x][y].texture_code = TextureStateCache.TEX_ALIEN_PURPLE;
				}
			}


			for (int r=1 ; r<=ROOMS ; r++) {
				int x = Functions.rndByte(SIZE/6, SIZE/2);
				int y = Functions.rndByte(SIZE/6, SIZE/2);
				int w = Functions.rndByte(4, MAX_ROOM_SIZE);
				int h = Functions.rndByte(4, MAX_ROOM_SIZE);

				// Set along boundaries.
				x = (x/2);
				x = (x * 2);
				y = (y/2);
				y = (y * 2);
				w = (w/2);
				w = (w * 2);
				h = (h/2);
				h = (h * 2);

				centres.add(new Point(x+1, y+1));
				
				this.createRoomByTopLeft(x, y, w, h, (byte)2);
			}

			// remove doors not attached to any walls
			for (byte y=0 ; y<this.getMapHeight() ; y++) {
				for (byte x=0 ; x<this.getMapWidth() ; x++) {
					AbstractMapSquare ms = map[x][y];
					if (ms.door_type > 0) {
						AbstractMapSquare ms_u = map[x][y-1];
						AbstractMapSquare ms_d = map[x][y+1];
						AbstractMapSquare ms_l = map[x-1][y];
						AbstractMapSquare ms_r = map[x+1][y];
						int tot_walls = 0;
						//int tot_floors = 0;
						if (ms_u.major_type == MapDataTable.MT_WALL) {
							tot_walls++;
						}
						if (ms_d.major_type == MapDataTable.MT_WALL) {
							tot_walls++;
						}
						if (ms_l.major_type == MapDataTable.MT_WALL) {
							tot_walls++;
						}
						if (ms_r.major_type == MapDataTable.MT_WALL) {
							tot_walls++;
						}
						if (tot_walls <= 1) {
							ms.door_type = 0;
							ms.major_type = MapDataTable.MT_FLOOR;
							ms.texture_code = TextureStateCache.TEX_ALIEN_PURPLE;
						}
					}
				}
			}

			// Sprinkle individual walls
			for (int r=1 ; r<=ROOMS * 8 ; r++) {
				byte x = Functions.rndByte(4, SIZE-5);
				byte y = Functions.rndByte(4, SIZE-5);
				if (x < SIZE/4 || x > SIZE*.66f) {
					if (y < SIZE/4 || y > SIZE*.66f) {
						AbstractMapSquare ms = map[x][y];
						ms.major_type = MapDataTable.MT_WALL;
						ms.texture_code = TextureStateCache.TEX_CELLS3;
						ms.deploy_sq_side = -1;
					}
				}
			}		

			// Check all rooms are connected
			boolean success = true;
			for (int s=0 ; s<centres.size() ; s++) {
				if (areRoomsConnected(centres, s) == false) {
					success = false;
					break;
				}
			}
			if (success) {
				break;
			}
		}
		createSide1DeploySquares();

	}


	private void createRoomByTopLeft(int x, int y, int w, int h, byte deploy_side) {
		boolean door_ew = Functions.rnd(1, 2) == 1;
		for (int y2=y ; y2<=y+h ; y2++) {
			for (int x2=x ; x2<=x+w ; x2++) {
				AbstractMapSquare ms = map[x2][y2];
				if (y2 == y || y2 == y+h || x2 == x || x2 == x+w) { // Is it the wall?
					// Create door?
					if (x2 == (x+x+w)/2 && door_ew == false) {
						ms.major_type = MapDataTable.MT_FLOOR;
						ms.door_type = MapDataTable.DOOR_EW;
						ms.texture_code = TextureStateCache.TEX_ALIEN_GREEN;
					} else if (y2 == (y+y+h)/2 && door_ew) {
						ms.major_type = MapDataTable.MT_FLOOR;
						ms.door_type = MapDataTable.DOOR_NS;
						ms.texture_code = TextureStateCache.TEX_ALIEN_GREEN;
					} else {
						ms.major_type = MapDataTable.MT_WALL;
						ms.door_type = 0;
						ms.texture_code = TextureStateCache.TEX_CELLS3;
						ms.deploy_sq_side = -1;
					}
				} else { // Floor
					ms.major_type = MapDataTable.MT_FLOOR;
					ms.texture_code = TextureStateCache.TEX_ALIEN_GREEN;
					if (deploy_side >= 0) {
						ms.deploy_sq_side = deploy_side;
					}
				}
			}
		}

	}


	private void createSide1DeploySquares() {
		for (int y2=0 ; y2<SIZE ; y2++) {
			for (int x2=0 ; x2<SIZE ; x2++) {
				AbstractMapSquare ms = map[x2][y2];
				if (y2 == 1 || y2 == SIZE-2 || x2 == 1 || x2 == SIZE-2) {
					ms.deploy_sq_side = 1;
				}
			}
		}

	}


	private boolean areRoomsConnected(ArrayList<Point> centres, int e) {
		AStar astar = new AStar(this);
		Point start = new Point(0, 0);
		Point end = centres.get(e);
		astar.findPath(start.x, start.y, end.x, end.y, false);
		if (astar.wasSuccessful()) {
			return true;
		} else {
			return false;
		}

	}




}
