package dsrwebserver.maps;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import dsrwebserver.tables.MapDataTable;

import ssmith.lang.Functions;

public class SegmentMapGen extends MapData {

	private static final int SEGMENT_SIZE = 12;
	private int max_seg;

	public SegmentMapGen(int seg_size, int num_players) throws IOException, SQLException {
		super(seg_size * SEGMENT_SIZE);

		max_seg = seg_size;

		super.clearMap();

		for (int y=0 ; y<seg_size ; y++) {
			for (int x=0 ; x<seg_size ; x++) {
				loadSegment(x, y);
			}
		}

	}


	private void loadSegment(int segx, int segy) throws IOException, SQLException {
		boolean use_deploy = false;
		if (segx == 0 && segy == 0) {
			use_deploy = true;
		} else if (segx == 0 && segy == max_seg-1) {
			use_deploy = true;
		} else if (segx == max_seg-1 && segy == 0) {
			use_deploy = true;
		} else if (segx == max_seg-1 && segy == max_seg-1) {
			use_deploy = true;
		}

		String filename = null;
		if (use_deploy == false) {
			File segments[] = new File("./webroot/serverdata/map_segments/").listFiles();
			while (filename == null) {
				File f = segments[Functions.rnd(0, segments.length-1)];//.getName();
				if (f.getName().startsWith(".") == false) {
					if (f.getName().startsWith("garden") == false && 
							f.getName().startsWith("field") == false && 
							f.getName().startsWith("swimming_pool") == false && 
							f.getName().startsWith("pillars") == false && 
							f.getName().startsWith("tarmac") == false) {
						filename = f.getName();
					}
				}
			}
		} else {
			filename = "deploy.csv";
		}
		MapData md = MapLoader.Import(null, -1, "../map_segments/" + filename, false);

		int ox = segx*SEGMENT_SIZE;
		int oy = segy*SEGMENT_SIZE;

		for (int y=0 ; y<SEGMENT_SIZE ; y++) {
			for (int x=0 ; x<SEGMENT_SIZE ; x++) {
				int nx = x+ox;
				int ny = y+oy;
				super.map[nx][ny] = md.map[x][y];
				super.map[nx][ny].x = nx;
				super.map[nx][ny].y = ny;
				// Add deploy squares
				if (super.map[nx][ny].major_type == MapDataTable.MT_FLOOR) {
					if (segx == 0 && segy == 0) {
						super.map[x+ox][y+oy].deploy_sq_side = 1;
					} else if (segx == 0 && segy == max_seg-1) {
						super.map[x+ox][y+oy].deploy_sq_side = 2;
					} else if (segx == max_seg-1 && segy == 0) {
						super.map[x+ox][y+oy].deploy_sq_side = 3;
					} else if (segx == max_seg-1 && segy == max_seg-1) {
						super.map[x+ox][y+oy].deploy_sq_side = 4;
					}
				}
			}
		}

	}


}
