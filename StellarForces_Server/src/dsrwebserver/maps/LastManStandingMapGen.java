package dsrwebserver.maps;

import java.awt.Point;
import java.util.ArrayList;

import ssmith.astar.AStar;
import ssmith.lang.Functions;
import ssmith.util.Interval;
import dsr.TextureStateCache;
import dsrwebserver.DSRWebServer;

public final class LastManStandingMapGen extends MapData { 

	private static final int MAX_ROOM_SIZE = 5;
	public static final int DOOR_CHANCE = 5;

	private ArrayList<Point> centres = new ArrayList<Point>();

	public LastManStandingMapGen(int size, int rooms, int num_players, boolean extra_door) {
		super(size);

		int attempts = 0;
		restart: while (true) {
			Interval int_check = new Interval(10 * 1000, false); // Give us time to create the map
			attempts++;
			if (attempts > 100) {
				DSRWebServer.SendEmailToAdmin("Map Gen Timed out", "The map gen has timed out after " + attempts + " attempts.");
				throw new RuntimeException("Map generation timed out.");
			}

			super.clearMap();
			centres.clear();

			for (byte r=1 ; r<=rooms ; r++) { // Create rooms
				byte x = Functions.rndByte(1, size-MAX_ROOM_SIZE-2);
				byte y = Functions.rndByte(1, size-MAX_ROOM_SIZE-2);
				byte w = Functions.rndByte(2, MAX_ROOM_SIZE);
				byte h = Functions.rndByte(2, MAX_ROOM_SIZE);
				byte deploy_area = -1;

				if (r >= 1 && r<=num_players) { // Start room
					Point p = getRoomLoc(r, size);
					x = (byte)p.x; // Need to store these so we connect the rooms.
					y = (byte)p.y;
					deploy_area = r;
				} else if (r == num_players+1) { // Middle room
					x = (byte)((size/2) - (w/2));
					y = (byte)((size/2) - (h/2));
				} else {
					// Loop until we find an empty area
					while (super.isThereARoomAt(x, y, w, h) == true) {
						x = Functions.rndByte(1, size-MAX_ROOM_SIZE-2);
						y = Functions.rndByte(1, size-MAX_ROOM_SIZE-2);
						w = Functions.rndByte(2, MAX_ROOM_SIZE);
						h = Functions.rndByte(2, MAX_ROOM_SIZE);

						// Check if we've run out of time
						if (int_check.hitInterval()) {
							this.showMap();
							continue restart;
						}
					}
				}
				this.createRoomByTopLeft(x, y, w, h, TextureStateCache.TEX_INTERIOR1, deploy_area, false);
				// Store the rooms for checking later
				centres.add(new Point(x+(w/2), y+(h/2)));
			}

			// Connect rooms
			doors.clear();
			for (int i=0 ; i<centres.size() ; i++) {
				Point start = centres.get(i);
				Point end = centres.get(Functions.rnd(0, centres.size()-1));
				while (start == end) {
					end = centres.get(Functions.rnd(0, centres.size()-1));
				}
				this.addCorridorAndDoors(start.x, start.y, end.x, end.y, extra_door);
			}

			// Check deployment rooms are connected
			boolean success = true;
			for (int s=0 ; s<num_players ; s++) {
				if (areRoomsConnected(centres, s, s+1) == false) {
					super.showMap();
					success = false;
					break;
				}
			}

			if (success) {
				addDoors();
				break;
			}
			DSRWebServer.p("Recreating map.");

		}

	}


	private Point getRoomLoc(int side, int size) {
		switch (side) {
		case 1:
			return new Point(1, 1);
		case 2:
			return new Point(size-MAX_ROOM_SIZE-2, size-MAX_ROOM_SIZE-2);
		case 3:
			return new Point(1, size-MAX_ROOM_SIZE-2);
		case 4:
			return new Point(size-MAX_ROOM_SIZE-2, 1);
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	private boolean areRoomsConnected(ArrayList<Point> centres, int s, int e) {
		AStar astar = new AStar(this);
		Point start = centres.get(s);
		Point end = centres.get(e);
		astar.findPath(start.x, start.y, end.x, end.y, false);
		if (astar.wasSuccessful()) {
			return true;
		} else {
			return false;
		}

	}
	
	
	public static void main(String args[]) {
		LastManStandingMapGen lms = new LastManStandingMapGen(30, 10, 4, true) ;
		lms.showMap();
	}

}
