package dsrwebserver.components;

import java.sql.SQLException;

import ssmith.html.HTMLFuncs_old;
import ssmith.html.HTMLFunctions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.pages.dsr.AbstractDSRPage;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MessagesTable;
import dsrwebserver.tables.StarmapSquaresTable;

public class MobileLayout {

	public static String GetHTML(AbstractHTMLPage page, String title, StringBuffer html) throws SQLException {
		if (title.length() > 0) {
			page.setTitle(title);
		}

		StringBuffer str = new StringBuffer();
		//str.append("<center><table style=\"width: 50%; text-align: center;  margin-left: auto; margin-right: auto;\">"); // Make mobile site thinner
		//str.append("<table border=0 width=50%><tr><td>"); // Make mobile site thinner

		// Logo
		/*str.append("<a href=\"/\">");
		HTMLFunctions.Image(str, DSRWebServer.IMAGES_HOST + "/images/sf_logo2.png", "", "Stellar Forces", 250, 70);
		str.append("</a>"); */
		
		// Main options, e.g. "start new game"
		//str.append("<div style=\"float: right;\">");
		if (page.session.isLoggedIn() == false || page.current_login.hasRow() == false) {
			//if (page instanceof register == false) {
				//str.append("<h2><a href=\"/dsr/register.cls\">Register here to play (free)</a></h2>");
				//str.append("<a href=\"/dsr/register.cls\"><img src=\"/dsr/gettitle.cls?t=Register%20here%20to%20play&h=30&ul=1\" border=0 alt=\"Register here to play\" /></a><br />");
				//if (page.session.isLoggedIn() == false) {
				GetLoginBox(str, page);
			//}

		} else {
			//str.append("<p>Logged in as: <a href=\"/dsr/mysettings.cls\">" + page.current_login.getDisplayName_Enc(false) + "</a></p>");
			if (GamesTable.DoesPlayerHaveActions(page.dbs, page.current_login.getID())) {
				str.append("<span class=\"warning\"><a href=\"/dsr/MyGames.cls\">YOU HAVE ACTIONS!</a></span>");
			} else {//if (GamesTable.DoesPlayerHaveGames(page.dbs, page.current_login.getID()) == false) {
				str.append("<a href=\"/dsr/GameRequests.cls\"><span class=\"warning\">START ANOTHER GAME</span></a>");
			}
			if (MessagesTable.GetNumUnreadMessages(page.dbs, page.current_login.getID()) > 0) {
				str.append(" | <a href=\"/dsr/viewmessages.cls\"><span class=\"warning\"><img src=\"" + DSRWebServer.IMAGES_HOST + "/images/mail.gif\" height=\"20\" border=\"0\" alt=\"Mail\" /></span></a>");
			}

			if (page.current_login.getFactionID() <= 0 && page.current_login.canPlayInCampaign() == 1) {
				str.append(" | <a href=\"/dsr/factions.cls?add=1\">JOIN A FACTION!</a>");
			} else if (page.current_login.getFactionID() > 0) {
				int sua = StarmapSquaresTable.GetNumFactionsUnderAttack(page.dbs, page.current_login.getFactionID());
				if (sua > 0) {
					str.append(" | <a href=\"/dsr/starmap.cls\">" + sua + " SECTORS UNDER ATTACK</a>");
				}
			}
		}

		if (title.length() > 0) {
			//try {
				//str.append("<p><img src=\"/dsr/gettitle.cls?t=" + URLEncoder.encode(title, "UTF-8") + "&h=40\" border=0 /></p>");
				str.append("<h1>" + HTMLFunctions.s2HTML(title) + "</h1>");
			/*} catch (UnsupportedEncodingException ex) {
				DSRWebServer.HandleError(ex);
			}*/
		}

		/*if (page instanceof MainPage) {
			str.append("<div style=\"float: left; width: 75%\">");
		}*/
		str.append(html);
		/*if (page instanceof MainPage) {
			str.append("</div>");
		}*/

		str.append("<br clear=\"all\" />");

		if (page.session.isLoggedIn()) {
		str.append("<p>Logged in as: <a href=\"/dsr/mysettings.cls\">" + page.current_login.getDisplayName_Enc(false) + "</a></p>");
		if (GamesTable.DoesPlayerHaveActions(page.dbs, page.current_login.getID())) {
			str.append("<span class=\"warning\"><a href=\"/dsr/MyGames.cls\">YOU HAVE ACTIONS!</a></span>");
		} else {//if (GamesTable.DoesPlayerHaveGames(page.dbs, page.current_login.getID()) == false) {
			str.append("<a href=\"/dsr/GameRequests.cls\"><span class=\"warning\">START ANOTHER GAME</span></a>");
		}
		if (MessagesTable.GetNumUnreadMessages(page.dbs, page.current_login.getID()) > 0) {
			str.append(" | <a href=\"/dsr/viewmessages.cls\"><span class=\"warning\"><img src=\"" + DSRWebServer.IMAGES_HOST + "/images/mail.gif\" height=\"20\" border=\"0\" alt=\"Mail\" /></span></a>");
		}
		if (page.current_login.getFactionID() <= 0 && page.current_login.canPlayInCampaign() == 1) {
			str.append(" | <a href=\"/dsr/factions.cls?add=1\">JOIN A FACTION!</a>");
		}/* else if (page.current_login.getFactionID() > 0) {
			int sua = StarmapSquaresTable.GetNumFactionsUnderAttack(page.dbs, page.current_login.getFactionID());
			if (sua > 0) {
				str.append(" | <a href=\"/dsr/starmap.cls\">" + sua + " SECTORS UNDER ATTACK</a>");
			}
		}*/
		}

		str.append("<div style=\"position: bottom;\">");

		AppendMenuBar(page, str);

		str.append("</div>");

		//str.append("</td</tr></table>"); // main div

		return str.toString();
	}


	private static void AppendMenuBar(AbstractHTMLPage page, StringBuffer str) throws SQLException {
		str.append("<div class=\"mainmenu\">");
		str.append("<ul id=\"sddm\">");

		if (page.session.isLoggedIn()) {
			str.append("<li>Games");
			str.append("<a href=\"/dsr/MyGames.cls\">Current Games</a>");
			str.append("<a href=\"/dsr/GameRequests.cls\">Start New Game</a>");
			str.append("<a href=\"/dsr/finishedgames.cls\">Finished Games</a>");
			/*if (page.current_login.getFactionID() > 0) {
				str.append("<a href=\"/dsr/starmap.cls\">Galaxy Map</a>");
			}*/
			if (page.current_login.isAdmin()) {
				str.append("<a href=\"/dsr/allgames.cls\">All Current Games</a>");
			}
			str.append("</li>");
		}

		if (page.session.isLoggedIn()) {
			str.append("<li>My Pages");
			if (page.current_login.getFactionID() > 0) {
				str.append("<a href=\"/dsr/factions.cls\">Faction</a>");
				str.append("<a href=\"/dsr/campaign.cls\">Campaign Squad</a>");
			}
			str.append("<a href=\"/dsr/viewmessages.cls\">Messages</a>");
			str.append("<a href=\"/dsr/playerawards.cls\">Player Awards</a>");
			str.append("<a href=\"/dsr/mysettings.cls\">Settings</a>");
			str.append("</li>");
		}

		/*str.append("<li>Help");
		str.append("<a href=\"/dsr/about.cls\">About SF</a>");
		if (page.session.isLoggedIn()) {
			str.append("<a href=\"/dsr/factionhelp.cls\">Faction Help</a>");
		}
		//str.append("<a href=\"" + DSRWebServer.WIKI_LINK + "/doku.php?id=gameplay:quickstart\">Quickstart Help</a>");
		str.append("<a href=\"" + DSRWebServer.WIKI_LINK + "\">Instructions</a>");
		str.append("<a href=\"" + DSRWebServer.FAQ_LINK + "\">FAQ</a>");
		str.append("</li>");*/

		str.append("<li>Community");
		str.append("<a href=\"" + DSRWebServer.NEW_FORUM_LINK + "\">Forums</a>");
		str.append("<a href=\"/dsr/leaguetable.cls\">League Table</a>");
		str.append("<a href=\"/dsr/otherleagues.cls\">Other Leagues</a>");
		str.append("<a href=\"/dsr/recentposts.cls\">Most Recent Forum Posts</a>");
		//str.append("<a href=\"http://www.facebook.com/#!/group.php?gid=123390784381347\">Facebook Page</a>");
		//str.append("<a href=\"http://www.reddit.com/r/StellarForces/\">StellarForces Reddit</a>");
		str.append("</li>");

		str.append("<li>Stellarpedia");
		str.append("<a href=\"/dsr/webtutorial.cls\">Online Tutorial</a>");
		str.append("<a href=\"/dsr/about.cls\">About</a>");
		str.append("<a href=\"/dsr/faq.cls\"><nobr>FAQ</nobr></a>");
		str.append("<a href=\"/dsr/missiondescriptions.cls\">The Missions</a>");
		str.append("<a href=\"/dsr/equipmentdetails.cls\">Weapons &amp; Equipment</a>");
		str.append("<a href=\"/dsr/armourdetails.cls\">Armour</a>");
		if (page.session.isLoggedIn()) {
			str.append("<a href=\"/dsr/miscstatspage.cls\">Game Stats</a>");
		}
		str.append("<a href=\"/dsr/datefeatureadded.cls\">Date Feature Added</a>");
		//str.append("<a href=\"/dsr/credits.cls\">Credits</a>");
		str.append("</li>");


		str.append("</ul><div style=\"clear:both\"></div>");
		str.append("</div>");

	}


	protected static void GetLoginBox(StringBuffer str, AbstractDSRPage page) {
		//str.append("<div class=\"structural\" style=\"width: 20%; float:right; \">");
		//str.append("<center>");
		HTMLFunctions.StartForm(str, "form1", "/dsr/LoginPage.cls", "post");
		HTMLFunctions.StartTable(str);

		HTMLFunctions.StartRow(str);
		//HTMLFunctions.AddCell(str, "<b>Login here</b>");
		HTMLFunctions.AddCell(str, "<img src=\"/dsr/gettitle.cls?t=Login%20Here&h=20\" border=0 alt=\"Login Here\" />");
		HTMLFunctions.EndRow(str);

		String warning = page.conn.headers.getGetValueAsString("warning");
		if (warning.length() > 0) {
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, warning);
			HTMLFunctions.EndRow(str);
		}

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, "Login/Email Address:");
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, HTMLFuncs_old.TextBox("login", "", 50));
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, "Password: ");
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, HTMLFuncs_old.PasswordBox("pwd"));
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, HTMLFuncs_old.SubmitButton("Login"));
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, "<div class=\"little\"><a href=\"/dsr/forgottenpassword.cls\">Forgotten your password?</a></div>");
		HTMLFunctions.EndRow(str);

		/*HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, "<h3><a href=\"/dsr/register.cls\">Register here</a></h3>");
		HTMLFunctions.EndRow(str);
*/
		HTMLFunctions.EndTable(str);
		HTMLFunctions.EndForm(str);

		//str.append("</center></div>");
	}



}


