package dsrwebserver.components;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import ssmith.html.HTMLFuncs_old;
import ssmith.html.HTMLFunctions;
import ssmith.util.Interval;
import dsr.comms.AbstractCommFuncs;
import dsrwebserver.DSRWebServer;
import dsrwebserver.pages.AbstractHTMLPage;
import dsrwebserver.pages.MainPage;
import dsrwebserver.pages.dsr.AbstractDSRPage;
import dsrwebserver.pages.dsr.forums.ForumMainPage;
import dsrwebserver.pages.dsr.forums.ForumPostingsPage;
import dsrwebserver.tables.CampUnitsTable;
import dsrwebserver.tables.ChatTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MessagesTable;

/**
 * There is a seperate layout file for mobiles.
 *
 */
public class MainLayout {

	public static final int RANDOM_TYPE = 0;
	public static final int PHP_POSTS = 1;
	public static final int GAME_CHAT = 2;

	private static StringBuffer str_forum;
	public static Interval update_forum_interval = new Interval(1000 * 60 * 10, true);
	private static int year;

	static {
		Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
	}

	protected MainLayout() {

	}


	public static String GetHTML(AbstractHTMLPage page, String title, StringBuffer html) throws SQLException {
		if (page.conn.headers.isMobile()) { // || DSRWebServer.DEBUG) {
			return MobileLayout.GetHTML(page, title, html);
		}

		if (title.length() > 0) {
			page.setTitle(title);
		}

		StringBuffer str = new StringBuffer();

		if (page.session.isLoggedIn()) {
			if (page.current_login.isAdmin()) {
				String cmd = page.conn.headers.getGetValueAsString("cmd");
				if (cmd.equalsIgnoreCase("recalcranks")) {
					CampUnitsTable.RecalcRanks(page.dbs, true);
					HTMLFunctions.Para(str, "Ranks recalculated.");
				} else if (cmd.equalsIgnoreCase("testemail")) {
					DSRWebServer.SendEmailToAdmin("Test email", "This is a test");
					/*} else if (cmd.equalsIgnoreCase("recalcvps")) {
					ResultSet rs = page.dbs.getResultSet("SELECT GameID FROM Games");
					GamesTable game = new GamesTable(page.dbs);
					while (rs.next()) {
						game.selectRow(rs.getInt("GameID"));
						game.recalcVPs();
						//HTMLFunctions.Para(str, "Done game " + game.getID());
						DSRWebServer.p("Done game " + game.getID());
					}
					HTMLFunctions.Para(str, "VPs recalculated.");*/
				}
			}
		}


		/*if (page instanceof MainPage) { // Only bring sids in on front page
			str.append("<div class=\"overall\">");
		}*/

		if (page instanceof MainPage) {
			//str.append("<img src=\"/images/out-now.png\" alt=\"Out Now!\" style=\"position: absolute; left: 0px; top: 0px; z-index: -1;\"/>");
		}		

		// Logo
		str.append("<a href=\"/\">");
		HTMLFunctions.Image(str, DSRWebServer.IMAGES_HOST + "/images/sf_logo2.png", "", "Stellar Forces", 550, 85);
		//HTMLFunctions.Image(str, "/images/sf_pink.png", "", "Stellar Forces", -1, -1);
		//str.append("<center><img src=\"" + DSRWebServer.IMAGES_HOST + "/images/sf_logo2.png\" alt=\"Stellar Forces\" title=\"Stellar Forces\" width=\"900\" height=\"120\" border=\"0\" /></center>");
		str.append("</a>");

		// Main options, e.g. "start new game"
		str.append("<div style=\"float: right;\">");

		if (page.session.isLoggedIn() == false) {
			//str.append("<img hspace=5 vspace=5 width=\"100\" align=\"right\" src=\"/images/best_seller.png\" alt=\"Best Seller\" border=\"0\" />");

			//str.append("<h2><a href=\"/dsr/register.cls\">Register here to play (free)</a></h2>");
			//str.append("<a href=\"/dsr/register.cls\"><img src=\"/dsr/gettitle.cls?t=Register%20here%20to%20play!&h=30&ul=1\" border=0 alt=\"Register here to play\" /></a><br />");
		} else {
			str.append("<p style=\"text-align: right;\">Logged in as: <a href=\"/dsr/mysettings.cls\">" + page.current_login.getDisplayName_Enc(false) + "</a></p>");
			if (GamesTable.DoesPlayerHaveActions(page.dbs, page.current_login.getID())) {
				str.append("<span class=\"warning\"><a href=\"/dsr/MyGames.cls\">YOU HAVE ACTIONS!</a></span>");
			} else {//if (GamesTable.DoesPlayerHaveGames(page.dbs, page.current_login.getID()) == false) {
				str.append("<a href=\"/dsr/GameRequests.cls\"><span class=\"warning\">START ANOTHER GAME</span></a>");
			}
			int msgs = MessagesTable.GetNumUnreadMessages(page.dbs, page.current_login.getID()); 
			if (msgs > 0) {
				str.append(" | <a href=\"/dsr/viewmessages.cls\"><span class=\"warning\"><img src=\"" + DSRWebServer.IMAGES_HOST + "/images/mail.gif\" height=\"20\" border=\"0\" alt=\"Mail\" /> " + (msgs>1 ? "[" + msgs + "]" : "") + "</span></a>");
			}
			//str.append(" | <a href=\"http://www.stellarforces.com/dsr/forums/ForumPostingsPage.cls?topic=13584\">JOIN KNOCKOUT II!</a>");
			if (page.current_login.getFactionID() <= 0 && page.current_login.canPlayInCampaign() == 1) {
				str.append(" | <a href=\"/dsr/factions.cls?add=1\">JOIN A FACTION</a>");
			}/* else if (page.current_login.getFactionID() > 0) {
				int sua = StarmapSquaresTable.GetNumFactionsUnderAttack(page.dbs, page.current_login.getFactionID());
				if (sua > 0) {
					str.append(" | <a href=\"/dsr/starmap.cls\">" + sua + " SECTORS UNDER ATTACK</a>");
				} else if (starmap.canPlayerAttack(page.dbs, page.current_login)) {
					str.append(" | <a href=\"/dsr/starmap.cls\">INVADE A SECTOR</a>");
				}
			}

			if (PlayerAwardsTable.CanPlayerMakeAward(page.dbs, page.current_login)) {
				str.append(" | <a href=\"/dsr/playerawards.cls\">AWARD AN AWARD</a>");
			}*/
			//str.append(" | ");
		}
		//str.append("<a href=\"http://forums.stellarforces.com/viewtopic.php?f=8&t=328\">GET THE FULL VERSION FOR FREE</a>");
		str.append("</div>");

		AppendMenuBar(page, str);

		if (page.session.isLoggedIn()) {
			if (page.current_login.getGamesExperience() < 5) {
				//str.append("<p class=\"little\">If you are a new player and are finding the website or game confusing, <a href=\"mailto:" + AppletMain.COMMENTS_EMAIL_ADDRESS + "\">please let me know</a>.  Much appreciated!</p>");
				str.append("<p class=\"little\">New players!  Feel free to get stuck in and play/join any games you want.  If you have any questions about the website or game, no matter how small, please don't hesitate to <a href=\"" + DSRWebServer.NEW_FORUM_LINK + "\">ask them on the forums</a>.</p>");
			}
		}

		if (title.length() > 0) {
			str.append("<p><img src=\"/dsr/gettitle.cls?t=" + AbstractCommFuncs.URLEncodeString(title) + "&h=40\" border=0 /></p>");
		}

		if (page instanceof MainPage) {
			str.append("<div style=\"float: left; width: 75%\">");
		}
		str.append("\n" + html + "\n");
		if (page instanceof MainPage) {
			str.append("</div>");
		}

		if (page.session.isLoggedIn() == false) {
			GetStandardLoginBox(str, page);
		}

		if (page instanceof MainPage) {
			if (page.session.isLoggedIn()) {
				String chat = page.conn.headers.getPostValueAsString("chat");
				if (chat.length() > 0) {
					ChatTable.AddPosting(page.dbs, page.current_login.getID(), chat);
					DSRWebServer.SendEmailToAdmin("Chat posted", "Msg:" + chat);
				}
			}

			// Show chat
			/*str.append("<div class=\"structural\" style=\"width: 20%; float:right; \">");

			if (page.session.isLoggedIn()) {
				str.append("<p><div class=\"little2\">");
				HTMLFunctions.StartForm(str, "form1", "", "post");
				HTMLFunctions.TextBox(str, "chat", "", false, 100);
				HTMLFunctions.SubmitButton(str, "Shout");
				HTMLFunctions.EndForm(str);
				str.append("</p></div>");
			}

			String sql = "SELECT * FROM Chat ORDER BY DateCreated DESC LIMIT 10";
			str.append("<p><div class=\"little2\">");
			ResultSet rs = page.dbs.getResultSet(sql);
			while (rs.next()) {
				str.append(LoginsTable.GetDisplayName_Enc(page.dbs, rs.getInt("UserID"), true) + ": " + HTMLFunctions.s2HTML(rs.getString("Body")) + "<br />");
			}
			str.append("</p></div>");

			str.append("</div>");*/

			// Show android stuff
			/*str.append("<div class=\"structural\" style=\"width: 20%; float:right; \"><center>");
			str.append("<a href=\"" + DSRWebServer.ANDROID_WEB_URL_FREE + "\"><img src=\"" + DSRWebServer.IMAGES_HOST + "/images/android.jpg\" width=80% alt=\"Play SF on Android\" /></a>");
			//str.append("<a href=\"https://market.android.com/details?id=com.scs.stellarforces.main.full\"><img src=\"" + DSRWebServer.IMAGES_HOST + "/images/qr_sf_market.png\" width=80% alt=\"Android QR link\" /></a>");
			str.append("<a href=\"" + DSRWebServer.ANDROID_WEB_URL_FREE + "\"><img src=\"" + DSRWebServer.IMAGES_HOST + "/images/android-logo.jpg\" width=80% alt=\"Play SF on Android\" /></a></center>");
			str.append("<a href=\"" + DSRWebServer.ANDROID_WEB_URL_FULL + "\">There's also a paid-for version as well!</a></center>");
			str.append("</div>");*/

			GetRecentPosts(str, page, page.conn.headers.getGetValueAsInt("posts_type"));

		}

		str.append("<br clear=\"all\" />");
		str.append("<div style=\"position: bottom;\">");

		str.append("<div align=\"right\">");
		if (page.session.isLoggedIn()) {
			str.append("<p><a href=\"/dsr/logout.cls?logout=true\">Logout</a></p>");
		}

		str.append("</div>");

		str.append("<hr />");
		str.append("<address>&copy;" + year + " Penultimate Apps.  All rights reserved.</address>");
		str.append("<p class=\"little\">" + DSRWebServer.SUPPORT_TEXT + "  Note that emails may be read by the NSA/Tempora for security purposes.  Hello to Jason Isaacs!</p>");
		//long diff = System.currentTimeMillis() - page.start_time;
		//str.append("<p class=\"little\"> Page took " + diff + "ms to process</p>");

		//str.append("<a name=\"fb_share\" type=\"button_count\" share_url=\"http://www.stellarforces.com/\" href=\"http://www.facebook.com/sharer.php\">Share</a><script src=\"http://static.ak.fbcdn.net/connect.php/js/FB.Share\" type=\"text/javascript\"></script>");
		/*if (page.session.isLoggedIn()) {
			if (page.current_login.isAdmin()) {
				if (last_client_req != null) {
					str.append("<p class=\"little\">Last request: " + Dates.FormatDate(last_client_req, Dates.UKDATE_FORMAT2_WITH_TIME) + "</p>");
				}
			}
		}*/
		str.append("</div>");

		if (page instanceof MainPage) { // Only bring sides in on front page
			str.append("<br clear=all /><spanxx style=\"color: #000000; font-size=1\">");
			str.append(" | <a href=\"https://play.google.com/store/apps/details?id=com.scs.ninja.main.full\">Ninja!</a>");
			str.append(" | <a href=\"https://play.google.com/store/apps/details?id=com.scs.sorcerers.main.full\">Sorcerers</a>, a Chaos remake");
			str.append(" | <a href=\"https://play.google.com/store/apps/details?id=com.scs.uktrafficalerts.main.full\">UK Traffic Alerts</a>");
			//str.append(" | <a href=\"https://play.google.com/store/apps/details?id=com.scs.chromakey.main.full\">Chromakey Photo Edit</a>");
			str.append(" | <a href=\"https://play.google.com/store/apps/details?id=com.scs.speechbrowser\">Speech Web Browser</a>");
			str.append(" | <a href=\"https://play.google.com/store/apps/details?id=com.scs.speedwarning.main.full\">Speed Warning</a>");
			str.append(" | <a href=\"https://play.google.com/store/apps/details?id=com.scs.worldcrafter.main.lite\">Worldcrafter</a>");
			str.append(" | <a href=\"https://play.google.com/store/apps/details?id=com.scs.androidsoccer.main.lite\">Android Soccer</a>");
			str.append(" | <a href=\"https://play.google.com/store/apps/details?id=com.scs.mathattack.main.full\">Math Monsters</a>");
			str.append(" | <a href=\"https://play.google.com/store/apps/details?id=com.scs.alienanagrams.main.full\">Alien Anagrams</a>");
			str.append(" | <a href=\"https://play.google.com/store/apps/details?id=com.scs.spookyspelling.main.full\">Spooky Spelling</a>");
			str.append(" | <a href=\"https://play.google.com/store/apps/details?id=com.scs.trafficalertsiq.main.full\">Intelligent Traffic Alerts</a>");
			str.append(" | <a href=\"https://play.google.com/store/apps/details?id=com.scs.assaultsquad.main.full\">Assault Squad</a>");
			str.append(" | <a href=\"https://play.google.com/store/apps/details?id=com.scs.moonbasedefence.main.full\">Outpost Omega</a>");
			str.append("</span><span style=\"color: #000000\">laser squad, lazersquad, lazer squad, ufo:ai, x-com, enemy unknown, laser squad nemesis</span>");
			str.append("</div>");

			str.append("<script type=\"text/javascript\">\n");
			str.append("document.getElementById('login').focus();\n");
			str.append("</script>");

		}
		return str.toString();
	}


	private static void AppendMenuBar(AbstractHTMLPage page, StringBuffer str) throws SQLException {
		str.append("<div class=\"mainmenu\">");
		str.append("<ul id=\"sddm\">");

		if (page.session.isLoggedIn()) {
			str.append("<li><a href=\"#\" onmouseover=\"mopen('m1')\" onmouseout=\"mclosetime()\">Games</a> <div id=\"m1\" onmouseover=\"mcancelclosetime()\" onmouseout=\"mclosetime()\">");
			str.append("<a href=\"/dsr/MyGames.cls\">My Current Games</a>");
			str.append("<a href=\"/dsr/GameRequests.cls\">Start New Game</a>");
			str.append("<a href=\"/dsr/finishedgames.cls\">My Finished Games</a>");
			str.append("<a href=\"/dsr/starmap.cls\">Galaxy Map</a>");
			if (page.current_login.isAdmin()) {
				str.append("<a href=\"/dsr/allgames.cls\">All Current Games</a>");
			}
			str.append("</div></li>");
		}

		if (page.session.isLoggedIn()) {
			str.append("<li><a href=\"#\" onmouseover=\"mopen('m2')\" onmouseout=\"mclosetime()\">My Pages</a> <div id=\"m2\" onmouseover=\"mcancelclosetime()\" onmouseout=\"mclosetime()\">");
			if (page.current_login.getFactionID() > 0) {
				str.append("<a href=\"/dsr/factions.cls\">Faction</a>");
				str.append("<a href=\"/dsr/campaign.cls\">Campaign Squad</a>");
			}
			str.append("<a href=\"/dsr/playerawards.cls\">Player Awards</a>");
			str.append("<a href=\"/dsr/viewmessages.cls\">Messages</a>");
			str.append("<a href=\"/dsr/mysettings.cls\">Settings</a>");
			str.append("<a href=\"/dsr/playerspublicpage.cls?loginid=" + page.current_login.getID() + "\">My Stats</a>");
			str.append("<a href=\"/dsr/uploadmaps.cls\">Upload a Map</a>");
			str.append("</div></li>");
		}

		str.append("<li><a href=\"#\" onmouseover=\"mopen('m7')\" onmouseout=\"mclosetime()\"><nobr>Download</nobr></a> <div id=\"m7\" onmouseover=\"mcancelclosetime()\" onmouseout=\"mclosetime()\">");
		str.append("<a href=\"" + DSRWebServer.ANDROID_WEB_URL_FREE + "\">Get Free Android Client</a>");
		str.append("<a href=\"" + DSRWebServer.ANDROID_WEB_URL_FULL + "\">Get Full Android Client</a>");
		str.append("<a href=\"http://www.bluestacks.com/app-player.html\">Bluestacks App Player (For playing on Windows/Mac)</a>");
		//str.append("<a href=\"http://code.google.com/p/stellarforces/downloads/list\">Download PC Client</a>");
		str.append("</div></li>");

		/*str.append("<li><a href=\"#\" onmouseover=\"mopen('m4')\" onmouseout=\"mclosetime()\">Help</a> <div id=\"m4\" onmouseover=\"mcancelclosetime()\" onmouseout=\"mclosetime()\">");
		str.append("<a href=\"" + DSRWebServer.FAQ_LINK + "\">FAQ</a>");
		str.append("<a href=\"/dsr/about.cls\">About SF</a>");
		/*str.append("<a href=\"" + DSRWebServer.WIKI_LINK + "/doku.php?id=gameplay:quickstart\">Quickstart Help (PC client)</a>");
		str.append("<a href=\"/dsr/controls.cls\">Controls (PC client)</a>");
		str.append("<a href=\"" + DSRWebServer.WIKI_LINK + "\">Manual (PC client)</a>");*/
		/*if (page.session.isLoggedIn()) {
			str.append("<a href=\"/dsr/startpractisemission.cls\">Start Practise Mission</a>");
		}*/
		//str.append("</div></li>");

		str.append("<li><a href=\"#\" onmouseover=\"mopen('m3')\" onmouseout=\"mclosetime()\">Community</a> <div id=\"m3\" onmouseover=\"mcancelclosetime()\" onmouseout=\"mclosetime()\">");
		str.append("<a href=\"" + DSRWebServer.NEW_FORUM_LINK + "\">Forums</a>"); // /viewforum.php?f=1
		str.append("<a href=\"/dsr/leaguetable.cls\">League Table</a>");
		str.append("<a href=\"/dsr/otherleagues.cls\">Other Leagues</a>");
		if (page.session.isLoggedIn()) {
			str.append("<a href=\"/dsr/factionhelp.cls\">Faction Help</a>");
		}
		//str.append("<a href=\"http://stellarforces.blogspot.com/\">Blog</a>");
		str.append("<a href=\"/dsr/recentposts.cls\"><nobr>Most Recent Forum Posts</nobr></a>");
		//str.append("<a href=\"http://www.facebook.com/#!/group.php?gid=123390784381347\">Facebook</a>");
		str.append("<a href=\"http://www.facebook.com/StellarForces\">Facebook</a>");
		str.append("<a href=\"http://www.reddit.com/r/StellarForces/\">StellarForces Reddit</a>");
		str.append("</div></li>");

		/*str.append("<li><a href=\"#\" onmouseover=\"mopen('m5')\" onmouseout=\"mclosetime()\"><nobr>Media</nobr></a> <div id=\"m5\" onmouseover=\"mcancelclosetime()\" onmouseout=\"mclosetime()\">");
		str.append("<a href=\"/dsr/screenshots.cls\">Screenshots</a>");
		//str.append("<a href=\"/dsr/videopage.cls\">Videos</a>");
		str.append("</div></li>");
		 */
		str.append("<li><a href=\"#\" onmouseover=\"mopen('m6')\" onmouseout=\"mclosetime()\">Stellarpedia</a> <div id=\"m6\" onmouseover=\"mcancelclosetime()\" onmouseout=\"mclosetime()\">");
		str.append("<a href=\"/dsr/webtutorial.cls\">Online Tutorial</a>");
		str.append("<a href=\"/dsr/about.cls\">About</a>");
		str.append("<a href=\"/dsr/faq.cls\"><nobr>FAQ</nobr></a>");
		str.append("<a href=\"/dsr/missiondescriptions.cls\"><nobr>The Missions</nobr></a>");
		str.append("<a href=\"/dsr/equipmentdetails.cls\"><nobr>Weapons &amp; Equipment</nobr></a>");
		str.append("<a href=\"/dsr/armourdetails.cls\"><nobr>Armour</nobr></a>");
		if (page.session.isLoggedIn()) {
			str.append("<a href=\"/dsr/miscstatspage.cls\"><nobr>Game Stats</nobr></a>");
		}
		//str.append("<a href=\"/dsr/datefeatureadded.cls\">Date Feature Added</a>");
		str.append("<a href=\"/dsr/credits.cls\">Credits</a>");
		str.append("</div></li>");


		if (page.session.isLoggedIn()) {
			if (page.current_login.isAdmin()) {
				str.append("<li><a href=\"#\" onmouseover=\"mopen('m8')\" onmouseout=\"mclosetime()\">Admin</a> <div id=\"m8\" onmouseover=\"mcancelclosetime()\" onmouseout=\"mclosetime()\">");
				str.append("<a href=\"/dsr/admin/newplayers.cls\"><nobr>New Players</nobr></a>");
				str.append("<a href=\"/dsr/admin/showerrors.cls\"><nobr>Show Errors</nobr></a>");
				str.append("<a href=\"/dsr/admin/createdata.cls\"><nobr>Import Data</nobr></a>");
				str.append("<a href=\"/dsr/admin/updatephpbb.cls\"><nobr>Update PHP</nobr></a>");
				str.append("<a href=\"?cmd=testemail\"><nobr>Send Test Email</nobr></a>");
				//str.append("<a href=\"?cmd=recalcranks\"><nobr>Recalc Ranks</nobr></a>");
				//str.append("<a href=\"?cmd=recalcvps\"><nobr>Recalc VPs</nobr></a>");
				str.append("</div></li>");
			}
		}

		str.append("</ul><div style=\"clear:both\"></div>");

		//str.append("<br />");
		appendTwitter(str);

		str.append("</div>");

	}


	private static void GetStandardLoginBox(StringBuffer str, AbstractDSRPage page) {
		str.append("<div class=\"structural\" style=\"width: 20%; float:right; \">");
		str.append("<center>");
		//str.append("<img hspace=5 vspace=5 src=\"/images/smash_hit.png\" alt=\"Smash Hit App!\" border=\"0\" />");
		HTMLFunctions.StartForm(str, "form1", "/dsr/LoginPage.cls", "post");
		HTMLFunctions.HiddenValue(str, "url", page.getURL());
		HTMLFunctions.StartTable(str);

		HTMLFunctions.StartRow(str);
		//HTMLFunctions.AddCell(str, "<b>Login here</b>");
		HTMLFunctions.AddCell(str, "<img src=\"/dsr/gettitle.cls?t=Login%20Here&h=20\" border=0 alt=\"Login Here\" />");
		HTMLFunctions.EndRow(str);

		String warning = page.conn.headers.getGetValueAsString("warning");
		if (warning.length() > 0) {
			HTMLFunctions.StartRow(str);
			HTMLFunctions.AddCell(str, warning);
			HTMLFunctions.EndRow(str);
		}

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, "Email Address:");
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, HTMLFuncs_old.TextBox("login", "", 50));
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, "Password: ");
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, HTMLFuncs_old.PasswordBox("pwd"));
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, HTMLFuncs_old.SubmitButton("Login"));
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		HTMLFunctions.AddCell(str, "<div class=\"little\"><a href=\"/dsr/forgottenpassword.cls\">Forgotten your password?</a></div>");
		HTMLFunctions.EndRow(str);

		HTMLFunctions.StartRow(str);
		//HTMLFunctions.AddCell(str, "<h3><a href=\"/dsr/register.cls\">Register here</a></h3>");
		HTMLFunctions.AddCell(str, "<h3>Register using the Android app</h3>");
		HTMLFunctions.EndRow(str);

		HTMLFunctions.EndTable(str);
		HTMLFunctions.EndForm(str);
		str.append("</center></div>");
	}


	protected static void GetRecentPosts(StringBuffer str2, AbstractDSRPage page, int type) throws SQLException {
		int MAX_POSTS = 15;
		if (update_forum_interval.hitInterval() || str_forum == null || type > 0) {
			try {
				ArrayList<Post> posts = new ArrayList<Post>();

				/*if (type == RANDOM_TYPE) {
					if (Functions.rnd(1, 4) > 3) {
						type = GAME_CHAT;
					} else {
						type = PHP_POSTS;
					}
				}
				if (type == PHP_POSTS) {
					// get PHPBB posts
					MySQLConnection phpdbs = DSRWebServer.getPHPDbs();
					if (phpdbs != null) {
						String sql = "SELECT t.topic_id, t.topic_title, t.topic_last_post_id, t.forum_id, p.post_id, p.poster_id, p.post_time, p.post_text, u.user_id, u.username";  
						sql = sql + " FROM  phpbb_posts p";
						sql = sql + " left join phpbb_topics t on t.topic_id = p.topic_id";
						sql = sql + " left join phpbb_users u  on p.poster_id = u.user_id";  
						sql = sql + " WHERE t.forum_id != 4 ";
						sql = sql + " ORDER BY p.post_time DESC";  
						sql = sql + " LIMIT " + MAX_POSTS;
						ResultSet rs = phpdbs.getResultSet(sql);
						//String last_title = "";
						while (rs.next()) {
							//if (rs.getString("topic_title").equalsIgnoreCase(last_title) == false) {  // Only show first post under each topic
							//last_title = rs.getString("topic_title");
							Post p = new Post();
							p.link = DSRWebServer.NEW_FORUM_LINK + "/viewtopic.php?f=" + rs.getInt("forum_id") + "&t=" + rs.getInt("topic_id") + "&p=" + rs.getInt("post_id") + "#p" + rs.getInt("post_id");
							p.title = rs.getString("topic_title");
							p.text = rs.getString("post_text").replaceAll("<!--", "").replaceAll("-->", "");
							p.posted_by = rs.getString("username");
							p.ms = rs.getLong("post_time") * 1000;
							posts.add(p);
							if (posts.size() > 6) {
								break;
							}
						}
						rs.close();
					}
				} else { // Game chat*/
				// Get my posts
				String sql = "SELECT ForumPostings.ForumPostingID, ForumPostings.Body, ForumPostings.DateCreated, ForumTopics.Name, ForumTopics.ForumTopicID, Logins.LoginID FROM ForumPostings ";
				sql = sql + " INNER JOIN ForumTopics ON ForumTopics.ForumTopicID = ForumPostings.ForumTopicID ";
				sql = sql + " LEFT JOIN Logins ON Logins.LoginID = ForumPostings.UserID ";
				//sql = sql + " WHERE ForumTopics.ParentForumTopicID <> " + DSRWebServer.GAMES_FORUM_ID;
				sql = sql + " ORDER BY DateCreated DESC LIMIT " + MAX_POSTS;
				ResultSet rs = page.dbs.getResultSet(sql);
				while (rs.next()) {
					Post p = new Post();
					p.link = "/dsr/forums/ForumPostingsPage.cls?topic=" + rs.getInt("ForumTopicID") + "#pid" + rs.getInt("ForumPostingID");
					p.title = rs.getString("Name");
					p.text = ForumPostingsPage.GetEncodedMessage(rs.getString("Body"));
					p.posted_by = LoginsTable.GetDisplayName_Enc(page.dbs, rs.getInt("LoginID"), true);
					p.ms = rs.getTimestamp("DateCreated").getTime();
					posts.add(p);
				}
				rs.close();
				//}
				str_forum = new StringBuffer();
				str_forum.append("<div class=\"structural\" style=\"width: 20%; float:right; \">");

				HTMLFunctions.Heading(str_forum, 2, "<a href=\"" + DSRWebServer.NEW_FORUM_LINK + "\">SF Forums</a>"); ///viewforum.php?f=1 
				//HTMLFunctions.Heading(str_forum, 4, "<a href=\"?posts_type=1\">Forum Posts</a> | <a href=\"?posts_type=2\">Game Chat</a>"); 

				int max = Math.min(MAX_POSTS, posts.size());
				for (int i=0 ; i<max ; i++) {
					Post p = posts.get(i);
					str_forum.append("<p><div class=\"little2\">");
					str_forum.append("<h3><a href=\"" + p.link + "\" rel=\"nofollow\" >" + p.title + "</a></h3>");
					String text = p.text;
					if (text.length() > 800) {
						text = text.substring(0, 799) + "[...]";
					}
					str_forum.append(text + "<br />");
					str_forum.append("</div><div class=\"little\">Posted by " + p.posted_by + " " + ForumMainPage.GetTimeAgo(p.ms));
					//+ " " + ForumMainPage.GetTimeAgo(rs.getTimestamp("post_time").getTime()));
					// + " to <a href=\"/dsr/forums/ForumPostingsPage.cls?topic=" + rs.getInt("ForumTopicID") + "#pid" + rs.getInt("ForumPostingID") + "\">" + rs.getString("Name") + "</a>");
					str_forum.append("</div>");
					str_forum.append("</p><hr />");

				}
				str_forum.append("</div>");
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}
		}
		str2.append(str_forum);

	}


	private static void appendTwitter(StringBuffer str) {
		//str.append("Follow <a href=\"https://twitter.com/stephencsmith\">@stephencsmith</a> for the latest news<br />");
		str.append("<a href=\"https://twitter.com/stephencsmith\" class=\"twitter-follow-button\" data-show-count=\"false\">Follow @stephencsmith</a>");
		str.append("<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=\"//platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");</script>");

	}

}
