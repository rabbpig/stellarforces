package dsrwebserver.components;

import java.util.Comparator;

public class Post implements Comparator<Post> {

	public long ms;
	public String title, text, posted_by, link;

	//@Override
	public int compare(Post arg0, Post arg1) {
		Post e1 = (Post)arg0;
		Post e2 = (Post)arg1;

		if (e1.ms > e2.ms) {
			return -1;
		} else {
			return 1;
		}
	}


}
