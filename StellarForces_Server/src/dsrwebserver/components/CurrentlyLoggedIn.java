package dsrwebserver.components;

import java.util.ArrayList;

public class CurrentlyLoggedIn extends ArrayList<CurrentlyLoggedIn.LoginDetails> {

	private static final long DURATION = 1000*60*2;

	private static final long serialVersionUID = 1L;

	public CurrentlyLoggedIn() {
		super();
	}


	public String getUsers() {
		StringBuffer str = new StringBuffer();

		// Check if any need removing
		for (int i=0 ; i<this.size() ; i++) {
			LoginDetails l = (LoginDetails)this.get(i);
			if (l.remove_time < System.currentTimeMillis()) {
				this.remove(i);
				i--;
			} else {
				str.append(l.name + ", ");
			}
		}

		if (str.length() > 2) {
			str.delete(str.length()-2, str.length());
		}
		return str.toString();
	}


	public boolean remove(String o) {
		LoginDetails li = new LoginDetails(o);
		while (this.contains(li)) {
			this.remove(li);
		}
		return true;
	}


	public boolean add(String o) {
		if (o != null) {
			remove(o);

			LoginDetails li = new LoginDetails(o);
			super.add(li);
			return true;
		} 
		return false;
	}


	//**************************************************************

	class LoginDetails {

		public String name;
		public long remove_time;

		public LoginDetails(String _name) {
			name = _name;
			remove_time = System.currentTimeMillis() + DURATION;
		}

		public boolean equals(Object l2) {
			if (l2 instanceof LoginDetails) {
				LoginDetails l3 = (LoginDetails)l2;
				return l3.name.equalsIgnoreCase(this.name);
			} else {
				throw new RuntimeException("Invalid object type: " + l2);
			}
		}
	}

}

