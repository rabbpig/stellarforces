package dsrwebserver;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import ssmith.io.TextFile;

public final class ContentTypes extends HashMap<String, String> {

	private static final long serialVersionUID = 1737629419260498290L;

	public ContentTypes() throws FileNotFoundException, IOException {
		super();

		TextFile tf = new TextFile();
		tf.openFile("./config/content-types.txt", TextFile.READ);
		while (tf.isEOF() == false) {
			String line = tf.readLine();
			if (line.length() > 0) {
				String csv[] = line.split("=");
				String extentions[] = csv[1].split(" ");
				for (int i=0 ; i<extentions.length ; i++) {
					String ext = extentions[i].toLowerCase();
					String ctype = csv[0];
					//System.out.println("Ext:" + ext + "  Ctype:" + ctype);
					super.put(ext, ctype);
				}
			}
		}
		tf.close();

	}

	public String getContentTypeFromExtention(String url) {
		String s[] = url.split("\\.");
		String ext = s[s.length-1].toLowerCase();
		if (this.containsKey(ext)) {
			return (String)this.get(ext);
		} else {
			DSRWebServer.p("Unknown content type for " + url + ".  Using default (" + (String)this.get("default") + ")");
			return (String)this.get("default");
		}
	}

}
