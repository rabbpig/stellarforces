package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

/**
 * 20/2/14 - Attackers have 250 -> 270 creds.
 *
 */
public class MoonbaseAssault_Campaign extends AbstractMission {

	private static final int MAX_TURNS = 10;

	public MoonbaseAssault_Campaign() {
		super(AbstractMission.MOONBASE_ASSAULT_CAMPAIGN, CP_OMNI_CORP, "Laser Squad", "Marsec Corp", MAX_TURNS, 270, 210, 2, AbstractMission.SHOW_CEILINGS, 7, 0, 0.38f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "moonbase_assault.txt", "moonbase_assault_campaign.txt", IS_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "moonbaseassault_campaign.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Attackers";
		case 2:
			return "The Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "Computers must be defended or destroyed.";
	}


	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must destroy the computers";
		case 2:
			return "You must prevent the computers being destroyed";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	/*public int canBePlayedOnAndroid() {
		return ANY_CLIENT;
	}*/


	@Override
	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			if (side == 2) {
				return code.equalsIgnoreCase(EquipmentTypesTable.CD_NERVE_GAS) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_INCENDIARY_GRENADE) == false; // It's too easy otherwise
			} else {
				return true;
			}
		}
		return false;
	}


}
