package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.util.MyList;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitsTable;

public class LaboratoryAttack2v2 extends AbstractMission {

	private static final int MAX_TURNS = 25;
	private static final String SIDE1_NAME = "Laser Squad 1";
	private static final String SIDE2_NAME = "Marsec Corporation 1";
	private static final String SIDE3_NAME = "Laser Squad 2";
	private static final String SIDE4_NAME = "Marsec Corporation 2";
	private static final int VPS_PER_CPU = 10;

	/**
	 * 30/5/13 - Reduced creds for sides 2 & 4 from 230 to 200
	 */
	public LaboratoryAttack2v2() {
		super(AbstractMission.LAB_ATTACK_2V2, CP_SHADOW_CONSPIRACY, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, 230, 200, 230, 200, 2, AbstractMission.SHOW_CEILINGS, VPS_PER_CPU, 0, VPS_PER_CPU, 0, 1.63f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "laboratory_attack_2v2.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, false, SPECIFIED_WIN_AT_END, "laboratoryattack.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "The base computers and scientists must be protected.  Two players control an attack squad who must kill enough computers and scientists, and the other two players controls the defenders and the scientists.";
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
		case 3:
			return "You must destroy the computers and kill the scientists";
		case 2:
		case 4:
			return "You must kill the assassins";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public MyList<Integer> getSidesForSide(int side) {
		switch (side) {
		case 1:
		case 3:
			return MyList.CreateIntsFromInts(1,3);
		case 2:
		case 4:
			return MyList.CreateIntsFromInts(2,4);
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	/*@Override
	public int canBePlayedOnAndroid() {
		return ANY_CLIENT;
	}*/


	@Override
	public boolean addComradesVPs(int side) {
		return false;
	}


	@Override
	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}


	public boolean hasSpecialVPCalc() {
		// Override if req
		return true;
	}


	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		int vps = 0;
		AbstractMission mission = AbstractMission.Factory(this.getMissionID());

		// Calc from units killed
		String sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + game.getID() + " AND Side IN (" + game.getOpponentSidesForSideAsCSV(side, mission) + ") AND Status = " + UnitsTable.ST_DEAD;
		/*if (this.getNumOfSides() > 2 && mission.doWeCareWhoKilledUnits()) { // True for LMS, false for most 2v2 missions
			sql_vps = sql_vps + " AND COALESCE(KilledBySide, 0) IN (0, " + side + ")";
		}*/
		vps = dbs.getScalarAsInt(sql_vps);

		// Calc from destroyed computers
		int vps_per_cpu = mission.getVPsPerComputer(side);
		if (vps_per_cpu != 0) {
			String sql = "SELECT Count(*) FROM MapDataSquares WHERE MapDataID = " + game.getMapDataID() + " AND SquareType = " + MapDataTable.MT_COMPUTER + " AND Destroyed = 1 ";
			/*if (mission.doWeCareWhoDestroyedComputers()) { // We care who destroyed them
				sql = sql + "AND COALESCE(DestroyedBySide, 0) IN (0, " + mission.getSidesForSide(side).toCSVString() + ")";
			}*/
			/*if (mission.doWeCareWhoOwnsTheComputers()) {
				sql = sql + " AND OwnerSide IN (0, " + getOpponentSidesForSideAsCSV(side, mission) + ")";				
			}*/
			vps += (vps_per_cpu * dbs.getScalarAsInt(sql));
		}

		return vps;
	}



}
