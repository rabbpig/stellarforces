package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

/**
 * 17/6/13 - Inc VPs per CPU from 5 to 6
 * 13/8/15 - Side 1 has 260 creds (was 250), side 2 has 305 creds (was 320)
 */
public class MoonbaseClash extends AbstractMission {
	
	private static final int MAX_TURNS = 30;

	public MoonbaseClash() {
		super(AbstractMission.MOONBASE_CLASH, CP_OMNI_CORP, "Laser Squad", "Marsec Corp", MAX_TURNS, 260, 305, 2, AbstractMission.SHOW_CEILINGS, 6, 0, 1.62f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "moonbase_clash.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_1, SPECIFIED_WIN_AT_END, "moonbaseassault.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Attackers";
		case 2:
			return "The Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public String getShortDesc() {
		return "One player must attack the moonbase and destroy the computers.  The other player must defend the moonbase until the turns run out.  This is the same as the standard Moonbase mission except walls can be destroyed.";
	}

	
	@Override
	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}
	

	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must destroy the computers";
		case 2:
			return "You must prevent the computers being destroyed";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	/*public int canBePlayedOnAndroid() {
		return ANY_CLIENT;
	}*/


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
		return t != EquipmentTypesTable.ET_SCANNER;
		}
		return true;
	}



}
