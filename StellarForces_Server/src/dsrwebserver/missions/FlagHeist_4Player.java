package dsrwebserver.missions;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.maps.MapData;
import dsrwebserver.tables.EquipmentTypesTable;

public class FlagHeist_4Player extends AbstractMission {
	
	private static final int MAX_TURNS = 30;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Renegade Squad";
	private static final String SIDE3_NAME = "Omni Corp";
	private static final String SIDE4_NAME = "Gallactix Corp";

	public FlagHeist_4Player() {
		super(AbstractMission.FLAG_HEIST_4P, CP_SHADOW_CONSPIRACY, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, 320, 320, 320, 320, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.26f, AbstractMission.WEAK_WALLS, AbstractMapModel.SLIM_WALLS, "flag_heist.txt", IS_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, SPECIFIED_WIN_AT_END, "flag_heist.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "Two teams battle it out to escape with the FLAG by taking it to the opposite side of the map.  However, the FLAG is encased in a solid room, so explosives will be required.";
	}

	
	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		MapData map = super.createBasicMap(dbs, gameid);
		
		// Add the flag
		AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_FLAG, -1, -1, -1, -1);
	}

	
	/*public boolean isItemTypeAllowed(int t, int side) {
		return t != EquipmentTypesTable.ET_ADRENALIN_SHOT;
	}*/


	@Override
	public boolean doesMissionInvolveFlag() {
		return true;
	}
	

	@Override
	public String getMission1Liner(int side) {
		return "You must get the flag to the opposite side of the map";
	}



}

