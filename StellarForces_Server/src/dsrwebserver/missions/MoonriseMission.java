package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.OppFireSelectionTable;
import dsrwebserver.tables.UnitsTable;

public class MoonriseMission extends AbstractMission {

	private static final int MAX_TURNS = 25;

	public MoonriseMission() {
		super(AbstractMission.MOONRISE, -1, 3, "Laser Squad", "Marsec Corp", "Globex Inc.", "", MAX_TURNS, 240, 240, 240, -1, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.27f, AbstractMission.WEAK_WALLS, AbstractMapModel.SLIM_WALLS, "moonrise.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, HIGHEST_VPS_WIN_AT_END, "moonrise.csv");
	}


	@Override
	public String getSideDescription(int side) {
		return super.sidename[side];
	}


	@Override
	public String getShortDesc() {
		return "Side 1 must kill side 2, side 2 must kill side 3, and side 3 must kill side 1.";
	}


	@Override
	public String getMission1Liner(int side) {
		int kill_side = side+1;
		if (kill_side > 3) {
			kill_side = 1;
		}
		return "You must kill members of " + super.sidename[kill_side] + ".";
	}


	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		super.createMap(dbs, gameid);

		try {
			// Stop the wrong sides from shooting each other
			GamesTable game = new GamesTable(dbs);
			game.selectRow(gameid);
			for (int s=1 ; s<=3 ; s++) {
				int non_kill_side = s-1;
				if (non_kill_side < 1) {
					non_kill_side = 3;
				}
				OppFireSelectionTable.AddRec(dbs, gameid, game.getLoginIDFromSide(s), non_kill_side, false);
			}
			game.close();
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	@Override
	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		try {
			int kill_side = side+1;
			if (kill_side > 3) {
				kill_side = 1;
			}
			int vps = 0;
			// Calc from units killed
			String sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + kill_side + " AND Status = " + UnitsTable.ST_DEAD;
			//sql_vps = sql_vps + " AND COALESCE(KilledBySide, 0) IN (0, " + side + ")";
			vps = dbs.getScalarAsInt(sql_vps);
			return vps;
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
			return 0;
		}
	}


	@Override
	public boolean hasSpecialVPCalc() {
		return true;
	}


	@Override
	public boolean doWeCareWhoKilledUnits() {
		return false;
	}


}
