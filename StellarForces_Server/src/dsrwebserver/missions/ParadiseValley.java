package dsrwebserver.missions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public class ParadiseValley extends AbstractMission {

	private static final int MAX_TURNS = 30;

	public ParadiseValley() {
		super(AbstractMission.PARADISE_VALLEY, CP_DEL_ME, "The Aliens", "Galactix Corp", MAX_TURNS, 0, 260, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.58f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "paradise_valley.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "paradise.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Alien Attackers";
		case 2:
			return "The Human Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "todo.";
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false;
		}
		return false;
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill the humans";
		case 2:
			return "Get the Stardrive to the right hand side of the map";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		super.createMap(dbs, gameid);

		// Add smoke
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);
		int mid = game.getMapDataID();

		String sql = "SELECT MapDataSquares.MapX, MapDataSquares.MapY FROM MapDataSquares ";
		sql = sql + " LEFT JOIN SpecialMapDataSquares ON SpecialMapDataSquares.MapDataID = MapDataSquares.MapDataID AND SpecialMapDataSquares.MapX = MapDataSquares.MapX AND SpecialMapDataSquares.MapY = MapDataSquares.MapY ";
		sql = sql + " WHERE MapDataSquares.MapDataID = " + mid;
		sql = sql + " AND SpecialMapDataSquares.SceneryCode = 24";
		ResultSet rs = dbs.getResultSet(sql);
		try {
			while (rs.next()) {
				int x = rs.getInt("MapX");
				int y = rs.getInt("MapY");
				dbs.runSQLUpdate("UPDATE MapDataSquares SET SmokeType = " + EquipmentTypesTable.ET_SMOKE_GRENADE + ", SmokeTurnCount = " + (game.getNumOfSides()-1) + " WHERE MapDataID = " + mid + " AND MapX = " + x + " AND MapY = " + y);
				UnitHistoryTable.AddRecord_Smoke(dbs, game, x, y, System.currentTimeMillis(), true);
				dbs.runSQLUpdate("UPDATE SpecialMapDataSquares SET SceneryCode = 0 WHERE MapDataID = " + mid + " AND MapX = " + x + " AND MapY = " + y);
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		} finally {
			game.close();
			rs.close();
		}
	}

	
	@Override
	public void createUnits(MySQLConnection dbs, GamesTable game) throws FileNotFoundException, SQLException, IOException {
		super.createUnits(dbs, game);

		try {
			// Give the stardrive to the first unit in side 2
			int eq_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_STARDRIVE);
			if (dbs.getScalarAsInt("SELECT Count(*) FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + eq_id) == 0) {
				String sql = "SELECT UnitID FROM Units WHERE GameID = " + game.getID() + " AND Side = 2";
				ResultSet rs = dbs.getResultSet(sql);
				if (rs.next()) {
					EquipmentTable.CreateEquipment(dbs, game.getID(), rs.getInt("UnitID"), eq_id, 0);
				}
			}

		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	@Override
	public boolean hasSpecialVPCalc() {
		return true;
	}


	@Override
	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		if (side == 2) { // Humans
			String sql = "SELECT MapX, MapY, UnitID FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_STARDRIVE);
			ResultSet rs = dbs.getResultSet(sql);
			while (rs.next()) { // Loop through all the Stardrives in the game
				int x = rs.getInt("MapX");
				if (rs.getInt("UnitID") > 0) {
					UnitsTable unit = new UnitsTable(dbs);
					unit.selectRow(rs.getInt("UnitID"));
					x = unit.getMapX();
					unit.close();
				}
				if (x >= MapDataTable.GetMapSize(dbs, game.getMapDataID()).width - 1) {
					return 100;
				}
			}
		} else if (side == 1) { // Aliens
			String sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + game.getID() + " AND Side = 1 AND Status = " + UnitsTable.ST_DEAD;
			return dbs.getScalarAsInt(sql_vps);
		}
		return 0;
	}


	@Override
	public void nextPhase(MySQLConnection dbs, GamesTable game) throws SQLException {
		super.nextPhase(dbs, game);

		this.addRandomSmoke(dbs, game.getID(), 10);
	}


	private void addRandomSmoke(MySQLConnection dbs, int gameid, int amt) throws SQLException {
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);

		try {
			for (int i=0 ; i<amt ; i++) {
				int x = Functions.rnd(1, 59);
				int y = Functions.rnd(1, 59);
				while (MapDataTable.GetMapSquareType(dbs, game.getMapDataID(), x, y) != MapDataTable.MT_FLOOR) {
					x = Functions.rnd(1, 59);
					y = Functions.rnd(1, 59);
				}
				dbs.runSQLUpdate("UPDATE MapDataSquares SET SmokeType = " + EquipmentTypesTable.ET_SMOKE_GRENADE + ", SmokeTurnCount = " + (game.getNumOfSides()-1) + " WHERE MapDataID = " + game.getMapDataID() + " AND MapX = " + x + " AND MapY = " + y);
				UnitHistoryTable.AddRecord_Smoke(dbs, game, x, y, System.currentTimeMillis(), true);
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

		game.close();

	}
	
}
