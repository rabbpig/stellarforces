package dsrwebserver.missions;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.maps.MapData;
import dsrwebserver.tables.EquipmentTypesTable;

public class CaptureTheFlagMission extends AbstractMission {
	
	private static final int MAX_TURNS = 30;
	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Renegade Squad";

	public CaptureTheFlagMission() {
		super(AbstractMission.CAPTURE_THE_FLAG, CP_SHADOW_CONSPIRACY, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 320, 320, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 1.26f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "capture_the_flag.txt", "", IS_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "capturetheflag.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "The incredibly valuable F.L.A.G. is located in the middle of the moonbase.  Two teams battle it out in close quarters to be the first to capture it and escape.";
	}


	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		MapData map = super.createBasicMap(dbs, gameid);
		
		// Add the flag
		AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_FLAG, -1, -1, -1, -1);
	}


	@Override
	public boolean doesMissionInvolveFlag() {
		return true;
	}
	

	@Override
	public String getMission1Liner(int side) {
		return "You must get the flag to the opposite side of the map";
	}



}
