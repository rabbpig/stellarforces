package dsrwebserver.missions;

import ssmith.util.MyList;
import dsr.models.map.AbstractMapModel;

public class Escort2v2 extends AbstractMission {

	private static final int MAX_TURNS = 20;

	public Escort2v2() {
		super(AbstractMission.ESCORT_2V2, CP_STERNER_REGNIX, 4, "Laser Squad 1", "Sterner Regnix", "Laser Squad 2", "Globex Megacorp 2", MAX_TURNS, 190, 200, 190, 200, 1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.58f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "escort_2.txt", IS_CAMPAIGN, AbstractMission.GR_NONE, false, SPECIFIED_WIN_AT_END, "escort.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 2 || side == 4;
	}

	
	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Assassins 1";
		case 2:
			return "Sterner Regnix";
		case 3:
			return "The Assassins 2";
		case 4:
			return "Bodyguards";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "Sterner Regnix must make it to his escape pod on the other side of the complex.  One player controls Sterner, another controls his bodyguards, and the other two players each control an assassin squad.";
	}

	
	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
		case 3:
			return "You must kill Sterner Regnix";
		case 2:
		case 4:
			return "Sterner Regnix must escape";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public MyList<Integer> getSidesForSide(int side) {
		switch (side) {
		case 1:
		case 3:
			return MyList.CreateIntsFromInts(1,3);
		case 2:
		case 4:
			return MyList.CreateIntsFromInts(2,4);
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}
	

	@Override
	public boolean addComradesVPs(int side) {
		return false;
	}

}
