package dsrwebserver.missions;

import java.awt.Point;
import java.sql.ResultSet;
import java.util.ArrayList;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.MapData;
import dsrwebserver.maps.MapLoader;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public class Denunciation extends AbstractMission {

	private static final int MAX_TURNS = 25;

	public Denunciation() {
		super(AbstractMission.DENUNCIATION, -1, "Banana Republic", "Rebels", MAX_TURNS, 280, 280, -1, AbstractMission.NO_CEILINGS, 0, 0, 1.46f, AbstractMission.WEAK_WALLS, AbstractMapModel.SLIM_WALLS, "denunciation.txt", "denunciation_campaign.txt", IS_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "denunciation.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "Banana Republic";
		case 2:
			return "The Rebels";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "Two sides must assassinate each other's ambassador who is somewhere in the UN village.  Each player controls a squad and one ambassador.";
	}



	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		MapData map = MapLoader.Import(dbs, gameid, this.map_filename, true);

		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);

		ArrayList<Point> prison_squares = new ArrayList<Point>();
		prison_squares.add(new Point(Functions.rnd(5, 55),42));
		prison_squares.add(new Point(Functions.rnd(5, 55),15));

		// Deploy ambassadors
		ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE Status = " + UnitsTable.ST_AWAITING_DEPLOYMENT + " AND GameID = " + gameid + " AND CanDeploy = 0 ORDER BY Side");
		while (rs.next()) {
			Point p = prison_squares.remove(0);
			//prison_squares.remove(rnd);
			dbs.runSQLUpdate("UPDATE Units SET Status = " + UnitsTable.ST_DEPLOYED + ", MapX = " + p.x + ", MapY = " + p.y + " WHERE UnitID = " + rs.getInt("UnitID"));

			try {
				// Add event
				UnitsTable unit = new UnitsTable(dbs);
				unit.selectRow(rs.getInt("UnitID"));
				int seen_by_side[] = {0, 0, 0, 0, 0};
				seen_by_side[unit.getSide()] = 1; // Only owner side knows about it
				UnitHistoryTable.AddRecord_UnitDeployed(dbs, game, unit, seen_by_side, System.currentTimeMillis());
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}
		}

		super.storeMapDataInSQL(dbs, map);
	}


	@Override
	public String getMission1Liner(int side) {
		return "You must kill your opponent's ambassador";
	}



}

