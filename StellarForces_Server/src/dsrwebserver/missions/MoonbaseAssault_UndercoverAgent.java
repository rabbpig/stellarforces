package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class MoonbaseAssault_UndercoverAgent extends AbstractMission {

	private static final int MAX_TURNS = 30;

	public MoonbaseAssault_UndercoverAgent() {
		super(AbstractMission.MOONBASE_ASSAULT_UNDERCOVER, CP_OMNI_CORP, "Laser Squad", "Marsec Corp", MAX_TURNS, 400, 240, 2, AbstractMission.SHOW_CEILINGS, 5, 0, 1.50f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "moonbase_assault_undercover_agent.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, SPECIFIED_WIN_AT_END, "moonbaseassault.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Attackers";
		case 2:
			return "The Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "One player must attack the moonbase and destroy the computers.  The other player must defend the moonbase until the turns run out.  However, one defender is actually an undercover agent for the attackers, and can be activated at any time.";
	}

	
	@Override
	public int getUnderCoveragentsForSide(int s) {
		switch (s) {
		case 1: return 0;
		case 2: return 2;
		}
		return 0;
	}

	
	@Override
	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}
	

	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must destroy the computers";
		case 2:
			return "You must prevent the computers being destroyed";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			if (side == 2) {
				return code.equalsIgnoreCase(EquipmentTypesTable.CD_INCENDIARY_GRENADE) == false; // It's too easy otherwise
			} else {
				return true;
			}
		}
		return false;
	}

}
