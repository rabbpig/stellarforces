package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class BlobAssassins extends AbstractMission {

	private static final int MAX_TURNS = 20; // LS is 20

	/**
	 * 30/5/13 - Increased all blob's CC from 100 to 150, and strength from 50 to 75, and health from 45 to 60.
	 *  
	 */
	public BlobAssassins() {
		super(AbstractMission.BLOB_ASSASSINS, CP_BLOBS, "The Blobs", "Globex Megacorp", MAX_TURNS, 0, 200, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.70f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "blob_assassins.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, SPECIFIED_WIN_AT_END, "theassassins.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Blobs";
		case 2:
			return "The Fugitives";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}


	public String getShortDesc() {
		return "Sterner Regnix must be killed!  One player controls the blobs who must try and kill him, and the other player controls Sterner and his bodyguards.";
	}

	
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill Sterner Regnix";
		case 2:
			return "You must kill the blobs";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


}
