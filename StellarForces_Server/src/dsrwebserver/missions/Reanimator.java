package dsrwebserver.missions;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GameLogTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

/**
 * 27/8/13 - Zombie's now have 9 units
 * 
 *
 */
public class Reanimator extends AbstractMission {

	private static final int MAX_TURNS = 20;

	public Reanimator() {
		super(AbstractMission.REANIMATOR, -1, "Research Monitors", "Cadavers", MAX_TURNS, 200, 0, 1, AbstractMission.SHOW_CEILINGS, 0, 0, 1.62f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "reanimator.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, SPECIFIED_WIN_AT_END, "reanimator.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "Research Monitors";
		case 2:
			return "Cadavers";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "Research Monitors must survive the cadaver onslaught until help arrives.  One player controls a group of zombies, who's distingtushing feature is the ability to come back to life after being killed.  The other side controls the Research Monitors.";
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must survive until the end";
		case 2:
			return "You must kill the research monitors";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public int getMaxProtection(int side) {
		return 20;
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_SCANNER && t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}


	@Override
	public void nextTurn(MySQLConnection dbs, GamesTable game) throws SQLException {
		// Need to do this here as the PC Client doesn't have ZOMBIE units (which extends this class)
		// Restore dead on side 2 only after side 1 has taken their turn (so they deon't reappear straight away)
		UnitsTable unit = new UnitsTable(dbs);
		ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE GameID = " + game.getID() + " AND Side = 2 AND Status = " + UnitsTable.ST_DEAD);
		while (rs.next()) {
			if (Functions.rnd(1, 3) == 1) {
				unit.selectRow(rs.getInt("UnitID"));
				unit.setHealth(unit.getMaxHealth());
				unit.setStatus(UnitsTable.ST_DEPLOYED);
				unit.setOnFire(false);
				GameLogTable.AddRec(dbs, game, -1, -1, unit.getName() + " has been reanimated.", false, System.currentTimeMillis());

				try {
					// Remove corpse
					String code = EquipmentTypesTable.CD_HUMAN_CORPSE_SIDE + unit.getSide();
					int mtid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, code);
					dbs.runSQLDelete("DELETE FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + mtid + " AND MapX = " + unit.getMapX() + " AND MapY = " + unit.getMapY());
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
			}
		}
		rs.close();
		unit.close();
	}


	@Override
	public boolean canSideWinByKillingAllOpposition(int side) {
		return side == 2;
	}



}
