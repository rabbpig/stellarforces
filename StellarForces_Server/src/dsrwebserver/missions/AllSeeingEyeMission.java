package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class AllSeeingEyeMission extends AbstractMission {

	private static final int MAX_TURNS = 30;

	private static final String SIDE1_NAME = "The Psychic";
	private static final String SIDE2_NAME = "The Cleaners";

	/**
	 * 17/6/13 - Inc side 2 creds from 150 to 180
	 */
	public AllSeeingEyeMission() {
		super(AbstractMission.ALL_SEEING_EYE, -1, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 20, 180, 2, AbstractMission.NO_CEILINGS, 0, 0, 1.69f, AbstractMission.WEAK_WALLS, AbstractMapModel.SLIM_WALLS, "allseeingeye.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, HIGHEST_VPS_WIN_AT_END, "allseeingeye.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 1;
	}

	
	@Override
	public String getShortDesc() {
		return "A psychic must get to the extraction point before the time runs out.  One player controls the poorly-armed psychic, who knows the position of all the enemies at all times.  The other player must try and kill the psychic.";
	}

	
	@Override
	public String getMission1Liner(int side) {
		return "You must kill your opponent's units";
	}

	
	@Override
	public boolean canSideSeeEnemies(int side) {
		return side == 1;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			if (side == 2) {
				return code.equalsIgnoreCase(EquipmentTypesTable.CD_INCENDIARY_GRENADE) == false;
			} else {
				return true;
			}
		}
		return false;
	}


}

