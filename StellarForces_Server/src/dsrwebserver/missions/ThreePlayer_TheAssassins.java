package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.missions.ai.TheAssassinsAI;
import dsrwebserver.tables.GamesTable;

public class ThreePlayer_TheAssassins extends AbstractMission {

	private static final int MAX_TURNS = 20; // LS is 20

	/**
	 * 30/5/13 - Reduced Sterner's creds from 180 to 160
	 *
	 */
	public ThreePlayer_TheAssassins() {
		super(AbstractMission.THREE_PLAYER_ASSASSINS, CP_STERNER_REGNIX, 3, "Laser Squad", "Globex Megacorp", "Rebel Squad", "", MAX_TURNS, 180, 160, 180, 0, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 0.45f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "3-player_assassins.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, SPECIFIED_WIN_AT_END, "3player_theassassins.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Assassins";
		case 2:
			return "The Fugitives";
		case 3:
			return "More Assassins";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "Sterner Regnix must be killed!  Two players each control a squad of the assassins and must race to be the first to kill him.  The other player controls Sterner and his bodyguards.";
	}

	
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
		case 3:
			return "You must kill Sterner Regnix";
		case 2:
			return "You must kill the assassins";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public boolean doWeCareWhoKilledUnits() {
		return true;
	}


	@Override
	public void mapCreated(MySQLConnection dbs, int gameid) throws SQLException {
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);

		if (game.getAIForSide(2) > 0) {
			TheAssassinsAI.CreateAI(dbs, game);
		}
	}


	@Override
	public int doesSideHaveAI(int side) {
		if (side == 2) {
			return 1;
		}
		return 0;
	}
	
}
