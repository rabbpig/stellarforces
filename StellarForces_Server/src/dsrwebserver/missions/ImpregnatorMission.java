package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class ImpregnatorMission extends AbstractMission {
	
	private static final int MAX_TURNS = 20;

	/**
	 * 30/5/13 - Reduced side 2 units by one and reduced side 2 creds from 280 to 250.
	 * 3/6/13 - Now there are 6 aliens.
	 * 15/1/14 - Now there are 6 humans
	 * 
	 */
	public ImpregnatorMission() {
		super(AbstractMission.IMPREGNATOR, CP_DEL_ME, "The Aliens", "Galactix Corp", MAX_TURNS, 0, 250, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.23f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "impregnator.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "impregnator.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Alien Attackers";
		case 2:
			return "The Human Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	public String getShortDesc() {
		return "A base must be defended from a group of aliens.  If the aliens manage to kill a human, then a new alien will be created.";
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}


	@Override
	public boolean canSideSeeEnemies(int side) {
		return side == 1;
	}

	
	@Override
	public boolean aliensImpregnate() {
		return true;
	}

	
	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill the humans";
		case 2:
			return "You must kill the aliens";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


}
