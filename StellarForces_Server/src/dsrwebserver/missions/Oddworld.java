package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;

public class Oddworld extends AbstractMission {

	private static final int MAX_TURNS = 30;

	private static final String SIDE1_NAME = "One Side";
	private static final String SIDE2_NAME = "Another Side";

	public Oddworld() {
		super(AbstractMission.ODDWORLD, -1, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 350, 350, -1, AbstractMission.NO_CEILINGS, 0, 0, 1.69f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "oddworld.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, HIGHEST_VPS_WIN_AT_END, "oddworld.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "Two sides must battle it out.";
	}


	@Override
	public String getMission1Liner(int side) {
		return "You must kill your opponent's units";
	}

	

}
