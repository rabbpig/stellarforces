package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.missions.ai.MoonbaseAssaultAI;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;

public class MoonbaseAssault extends AbstractMission {
	
	private static final int MAX_TURNS = 30;

	public MoonbaseAssault() {
		super(AbstractMission.MOONBASE_ASSAULT, CP_OMNI_CORP, "Laser Squad", "Marsec Corp", MAX_TURNS, 320, 320, 2, AbstractMission.SHOW_CEILINGS, 5, 0, 1.62f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "moonbase_assault.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "moonbaseassault.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Attackers";
		case 2:
			return "The Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	public String getShortDesc() {
		return "One player must attack the moonbase and destroy the computers.  The other player must defend the moonbase until the turns run out.";
	}

	
	@Override
	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}
	

	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must destroy the computers";
		case 2:
			return "You must prevent the computers being destroyed";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
		return t != EquipmentTypesTable.ET_SCANNER;
		}
		return true;
	}


	@Override
	public void mapCreated(MySQLConnection dbs, int gameid) throws SQLException {
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);

		if (game.getAIForSide(2) > 0) { // Has AI been chosen?
			MoonbaseAssaultAI.CreateAI(dbs, game);
		}
	}


	@Override
	public int doesSideHaveAI(int side) {
		if (side == 2) {
			return 1;
		}
		return 0;
	}
	

	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			if (side == 2) {
				return code.equalsIgnoreCase(EquipmentTypesTable.CD_INCENDIARY_GRENADE) == false; // It's too easy otherwise
			} else {
				return true;
			}
		}
		return false;
	}


}
