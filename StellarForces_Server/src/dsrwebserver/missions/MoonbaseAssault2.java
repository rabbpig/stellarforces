package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class MoonbaseAssault2 extends AbstractMission {
	
	private static final int MAX_TURNS = 30;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Marsec Corp";

	public MoonbaseAssault2() {
		super(AbstractMission.MOONBASE_ASSAULT_2, CP_OMNI_CORP, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 480, 480, -1, AbstractMission.SHOW_CEILINGS, 8, 8, 0.38f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "moonbase_assault_2.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, HIGHEST_VPS_WIN_AT_END, "moonbaseassault2.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public String getShortDesc() {
		return "Each player is in control of a moonbase, and they must use their squad to defend it, but also destroy the computers in their opponent's moonbase.";
	}

	
	@Override
	public String getMission1Liner(int side) {
		return "You must destroy your opponents computers";
	}
	
	
	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			//if (side == 2) {
			return code.equalsIgnoreCase(EquipmentTypesTable.CD_INCENDIARY_GRENADE) == false; // It's too easy otherwise
			/*} else {
				return true;
			}*/
		}
		return false;
	}

	
	@Override
	public boolean doWeCareWhoOwnsTheComputers() {
		return true;
	}


}
