package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;

public class Colonization extends ColonizationAbstract {

	private static final int MAX_UNITS = 16;

	public Colonization() {
		super(AbstractMission.COLONIZATION, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, -1, 120, 120, 120, 120, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.74f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "colonization.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, false, SPECIFIED_WIN_AT_END, "colonization.csv", MAX_UNITS);
	}


}
