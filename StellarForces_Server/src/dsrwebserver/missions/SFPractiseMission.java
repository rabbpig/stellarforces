package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;

public class SFPractiseMission extends AbstractMission {

	private static final int MAX_TURNS = 20; // LS is 20

	public SFPractiseMission() {
		super(AbstractMission.SF_PRACTISE_MISSION, CP_HIDDEN, 1, "Laser Squad", "Sterner Regnix", "", "", MAX_TURNS, 220, -1, -1, -1, 1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.72f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "practise_missions.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, false, SPECIFIED_WIN_AT_END, "theassassins.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "Laser Squad";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public String getShortDesc() {
		return "A beginners mission for practise.";
	}


	@Override
	public String getMission1Liner(int side) {
		return "This is the practise mission";
	}


/*	public int canBePlayedOnAndroid() {
		return ANY_CLIENT;
	}
*/

}
