package dsrwebserver.missions;

import java.awt.Point;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public class SFPractiseMissionWithAI extends AbstractMission {

	private static final int MAX_TURNS = 20; // LS is 20

	public SFPractiseMissionWithAI() {
		super(AbstractMission.SF_PRACTISE_MISSION_WITH_AI, 0, 2, "Laser Squad", "Sterner Regnix", "", "", MAX_TURNS, 220, -1, -1, -1, 1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.72f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "practise_missions.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, false, SPECIFIED_WIN_AT_END, "theassassins.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "Laser Squad";
		case 2:
			return "The Fugitives";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "A beginners mission for practise, with AI.";
	}


	@Override
	public String getMission1Liner(int side) {
		return "This is the practise mission with AI";
	}


	/*public int canBePlayedOnAndroid() {
		return ANY_CLIENT;
	}*/


	@Override
	public void mapCreated(MySQLConnection dbs, int gameid) throws SQLException {
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);

		dbs.runSQLUpdate("UPDATE Units SET AIType = 1 WHERE GameID = " + gameid + " AND Side = 2");

		// Equip 
		UnitsTable unit = new UnitsTable(dbs);
		ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE GameID = " + gameid + " AND AIType > 0");
		while (rs.next()) {
			unit.selectRow(rs.getInt("UnitID"));
			unit.setArmourType(2);
			int equipment_type_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SP30);
			int id = EquipmentTable.CreateEquipment(dbs, gameid, rs.getInt("UnitID"), equipment_type_id, 20);
			unit.setCurrentEquipmentID(id);
		}		

		game.setSideHasEquipped(2);

		// Deploy side 2
		ArrayList<Point> sterner_squares = new ArrayList<Point>();
		sterner_squares.add(new Point(11-2, 16-2));
		sterner_squares.add(new Point(23-2, 17-2));
		sterner_squares.add(new Point(34-2, 22-2));
		sterner_squares.add(new Point(38-2, 28-2));
		sterner_squares.add(new Point(23-2, 27-2));
		sterner_squares.add(new Point(29-2, 24-2));
		sterner_squares.add(new Point(25-2, 34-2));

		rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE Status = " + UnitsTable.ST_AWAITING_DEPLOYMENT + " AND GameID = " + gameid + " AND AIType > 0 AND ModelType = " + UnitsTable.MT_SCIENTIST + "");
		while (rs.next()) {
			Point p = sterner_squares.remove(Functions.rnd(0, sterner_squares.size()-1));
			try {
				if (MapDataTable.GetMapSquareType(dbs, game.getMapDataID(), p.x, p.y) == MapDataTable.MT_FLOOR) {
					dbs.runSQLUpdate("UPDATE Units SET Status = " + UnitsTable.ST_DEPLOYED + ", MapX = " + p.x + ", MapY = " + p.y + " WHERE UnitID = " + rs.getInt("UnitID"));
					try {
						// Add event
						int seen_by_side[] = {0, 0, 0, 0, 0};
						seen_by_side[unit.getSide()] = 1; // Only owner side knows about it
						UnitHistoryTable.AddRecord_UnitDeployed(dbs, game, unit, seen_by_side, System.currentTimeMillis());
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex);
					}
				} else {
					throw new RuntimeException("Map " + p.x + "/" + p.y + " is a wall!");
				}
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}
		}
		sterner_squares = null;

		ArrayList<Point> other_squares = new ArrayList<Point>();
		other_squares.add(new Point(23-2,25-2));
		other_squares.add(new Point(22-2,20-2));
		other_squares.add(new Point(18-2,23-2));
		other_squares.add(new Point(38-2,20-2));
		other_squares.add(new Point(18-2,30-2));
		other_squares.add(new Point(26-2,23-2));
		other_squares.add(new Point(15-2,18-2));
		other_squares.add(new Point(34-2,20-2));
		other_squares.add(new Point(14-2,33-2));
		other_squares.add(new Point(24-2,27-2));
		other_squares.add(new Point(37-2,24-2));

		rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE Status = " + UnitsTable.ST_AWAITING_DEPLOYMENT + " AND GameID = " + gameid + " AND AIType > 0 AND ModelType <> " + UnitsTable.MT_SCIENTIST + "");
		while (rs.next()) {
			Point p = other_squares.remove(Functions.rnd(0, other_squares.size()-1));
			try {
				if (MapDataTable.GetMapSquareType(dbs, game.getMapDataID(), p.x, p.y) == MapDataTable.MT_FLOOR) {
					dbs.runSQLUpdate("UPDATE Units SET Status = " + UnitsTable.ST_DEPLOYED + ", MapX = " + p.x + ", MapY = " + p.y + " WHERE UnitID = " + rs.getInt("UnitID"));
					try {
						// Add event
						int seen_by_side[] = {0, 0, 0, 0, 0};
						seen_by_side[unit.getSide()] = 1; // Only owner side knows about it
						UnitHistoryTable.AddRecord_UnitDeployed(dbs, game, unit, seen_by_side, System.currentTimeMillis());
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex);
					}
				} else {
					throw new RuntimeException("Map " + p.x + "/" + p.y + " is a wall!");
				}
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}
		}

		game.setSideHasDeployed(2, System.currentTimeMillis()); // This must be after they have actually been deployed!

		// Give them APs
		dbs.runSQLUpdate("UPDATE Units SET CurrentAPs = MaxAPs WHERE GameID = " + gameid + " AND Side = 2");

	}

}
