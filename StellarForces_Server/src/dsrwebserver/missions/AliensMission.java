package dsrwebserver.missions;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.maps.MapData;
import dsrwebserver.maps.MapLoader;
import dsrwebserver.pages.dsr.EquipUnits;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.UnitsTable;

public class AliensMission extends AbstractMission {
	
	private static final int MAX_TURNS = 30;
	
	private static final String MARINES = "The Colonial Marines"; 
	private static final String ALIENS = "Aliens"; 

	public AliensMission() {
		super(AbstractMission.ALIENS, CP_DEL_ME, MARINES, ALIENS, MAX_TURNS, 350, 0, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.78f, AbstractMission.WEAK_WALLS, AbstractMapModel.BLOCK_WALLS, "aliens.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "aliens.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 1;
	}

	
	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return MARINES;
		case 2:
			return ALIENS;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}
	
	
	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		MapData map = MapLoader.Import(dbs, gameid, this.map_filename, true);

		dbs.runSQLUpdate("UPDATE Units SET Status = " + UnitsTable.ST_DEPLOYED + ", MapX = 42, MapY = 9 WHERE GameID = " + gameid +  " AND Side = 1 AND OrderBy = 1");
		dbs.runSQLUpdate("UPDATE Units SET Status = " + UnitsTable.ST_DEPLOYED + ", MapX = 43, MapY = 9 WHERE GameID = " + gameid +  " AND Side = 1 AND OrderBy = 2");
		dbs.runSQLUpdate("UPDATE Units SET Status = " + UnitsTable.ST_DEPLOYED + ", MapX = 44, MapY = 9 WHERE GameID = " + gameid +  " AND Side = 1 AND OrderBy = 3");
		
		EquipmentTypesTable equip_types = new EquipmentTypesTable(dbs);
		// Gorman
		int unitid = dbs.getScalarAsInt("SELECT UnitID FROM Units WHERE GameID = " + gameid +  " AND Side = 1 AND OrderBy = 1");
		equip_types.selectByCode(EquipmentTypesTable.CD_SP30);
		EquipUnits.buyForUnit(dbs, gameid, unitid, equip_types);
		equip_types.selectByCode(EquipmentTypesTable.CD_PISTOL_CLIP);
		EquipUnits.buyForUnit(dbs, gameid, unitid, equip_types);
		equip_types.selectByCode(EquipmentTypesTable.CD_AP50);
		EquipUnits.buyForUnit(dbs, gameid, unitid, equip_types);
		equip_types.selectByCode(EquipmentTypesTable.CD_AP50);
		EquipUnits.buyForUnit(dbs, gameid, unitid, equip_types);

		// Ripley
		unitid = dbs.getScalarAsInt("SELECT UnitID FROM Units WHERE GameID = " + gameid +  " AND Side = 1 AND OrderBy = 2");
		equip_types.selectByCode(EquipmentTypesTable.CD_MK1);
		EquipUnits.buyForUnit(dbs, gameid, unitid, equip_types);
		equip_types.selectByCode(EquipmentTypesTable.CD_MACHINE_CLIP);
		EquipUnits.buyForUnit(dbs, gameid, unitid, equip_types);
		
		super.storeMapDataInSQL(dbs, map);
	}


	public String getShortDesc() {
		return "The Colonial Marines must escape from LV-426.";
	}

	

	/*@Override
	public boolean aliensImpregnate() {
		return false;
		
	}*/


	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must escape to the APC";
		case 2:
			return "You must kill all the humans";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	

}
