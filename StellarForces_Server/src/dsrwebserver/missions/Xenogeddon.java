package dsrwebserver.missions;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GameLogTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

/**
 * 7/10/13 - Side 1 now has 280 creds up from 250.
 * 17/12/13 - Extra human unit, extra blob, removed zombie
 * 10/6/14 - Extra zombie, extra alien, banned AP100s
 * 22/8/14 - Extra alien
 *
 */
public class Xenogeddon extends AbstractMission {

	private static final int MAX_TURNS = 30;

	public Xenogeddon() {
		super(AbstractMission.XENOGEDDON, -1, 4, "Humans", "Zombies", "Aliens", "Blobs", MAX_TURNS, 280, 0, 0, 0, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.74f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "xenogeddon.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, false, SPECIFIED_WIN_AT_END, "flag_collector.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "Humans";
		case 2:
			return "Zombies";
		case 3:
			return "Aliens";
		case 4:
			return "Blobs";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public void nextTurn(MySQLConnection dbs, GamesTable game) throws SQLException {
		UnitsTable unit = new UnitsTable(dbs);
		ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE GameID = " + game.getID() + " AND Side = 2 AND Status = " + UnitsTable.ST_DEAD);
		while (rs.next()) {
			if (Functions.rnd(1, 3) == 1) {
				unit.selectRow(rs.getInt("UnitID"));
				unit.setHealth(unit.getMaxHealth());
				unit.setStatus(UnitsTable.ST_DEPLOYED);
				unit.setOnFire(false);
				GameLogTable.AddRec(dbs, game, -1, -1, unit.getName() + " has been reanimated.", false, System.currentTimeMillis());

				try {
					// Remove corpse
					String code = EquipmentTypesTable.CD_HUMAN_CORPSE_SIDE + unit.getSide();
					int mtid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, code);
					dbs.runSQLDelete("DELETE FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + mtid + " AND MapX = " + unit.getMapX() + " AND MapY = " + unit.getMapY());
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
			}
		}
		rs.close();
		unit.close();
	}


	@Override
	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		try {
			// Check we have some units left
			String sql = "SELECT COUNT(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED;
			if (dbs.getScalarAsInt(sql) <= 0) {
				return 0;
			}
			// Calc from units killed
			/*String sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + game.getID() + " AND Side <> " + side + " AND Status = " + UnitsTable.ST_DEAD; // NOT IN (" + game.getOpponentSidesForSideAsCSV(side, mission) + ")
			sql_vps = sql_vps + " AND COALESCE(KilledBySide, 0) IN (0, " + side + ")";
			int vps = dbs.getScalarAsInt(sql_vps);*/

			int vps = 0;
			try {
				String sql_vps = "SELECT * FROM Units WHERE GameID = " + game.getID() + " AND Side <> " + side + " AND Status = " + UnitsTable.ST_DEAD + " AND COALESCE(KilledBySide, 0) IN (0, " + side + ")";
				ResultSet rs = dbs.getResultSet(sql_vps);
				while (rs.next()) {
					if (rs.getInt("ModelType") == UnitsTable.MT_BLOB) { // Base VPs in size!
						vps += rs.getInt("Strength") / 4;
					} else {
						vps += rs.getInt("VPsIfKilled");
					}
				}
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}
			return vps;
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
			return 0;
		}
	}


	@Override
	public boolean hasSpecialVPCalc() {
		return true;
	}


	@Override
	public String getShortDesc() {
		return "Four very different lifeforms battle it out: humans, aliens, blobs and zombies.  Each has their own skills and weaknesses.";
	}


	@Override
	public String getMission1Liner(int side) {
		return "You must kill all other lifeforms!";
	}


	@Override
	public boolean doWeCareWhoKilledUnits() {
		return true;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			return code.equalsIgnoreCase(EquipmentTypesTable.CD_DEATHGRENADE) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_AP100) == false;
		}
		return false;
	}



}
