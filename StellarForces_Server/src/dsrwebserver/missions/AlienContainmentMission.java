package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitsTable;

/**
 * 7/10/13 - Side 2 now has 430 creds up from 400.
 * 7/10/13 - Side 1 now has 390 creds down from 400.
 */
public class AlienContainmentMission extends AbstractMission {

	private static final int MAX_TURNS = 18;

	public AlienContainmentMission() {
		super(AbstractMission.ALIEN_CONTAINMENT, CP_DEL_ME, 3, "The Aliens", "The Rebels", "Marsec Corporation", "", MAX_TURNS, 0, 390, 430, -1, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 25, 0, 0.51f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "alien_containment.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, SPECIFIED_WIN_AT_END, "3p_alien_containment.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 1;
	}

	
	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Aliens";
		case 2:
			return "The Rebels";
		case 3:
			return "The Marsec Corporation";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "This is a 3-sided mission.  The rebels objective is to  defend the computers and stop the aliens from escaping; the Marsec Corp. must destroy the computers; and the aliens must escape before the time runs out.";
	}



	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE && t != EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE;
		}
		return true;
	}


	@Override
	public boolean aliensImpregnate() {
		return true;

	}

	
	@Override
	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}


	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must escape";
		case 2:
			return "You must defend the computers and stop the aliens escaping";
		case 3:
			return "You must destroy the computers and stop the aliens escaping";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public boolean hasSpecialVPCalc() {
		return true;
	}


	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		int vps = 0;
		try {
			if (side == 1) {
				// See how many have escaped
				if (dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = 1 AND Status = " + UnitsTable.ST_ESCAPED) > 0) {
					vps = 100;
				}
			} else if (side == 2) {
				// See if no enemies alive or escaped
				if (dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side <> 2 AND Status = " + UnitsTable.ST_DEPLOYED) == 0) {
					// Check no aliens have escaped
					if (dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = 1 AND Status = " + UnitsTable.ST_ESCAPED) == 0) {
						vps = 100;
					}
				}
			} else if (side == 3) {
				// Count destroyed computers
				int vps_per_cpu = this.getVPsPerComputer(side);
				if (vps_per_cpu != 0) {
					String sql = "SELECT Count(*) FROM MapDataSquares WHERE MapDataID = " + game.getMapDataID() + " AND SquareType = " + MapDataTable.MT_COMPUTER + " AND Destroyed = 1";
					vps += (vps_per_cpu * dbs.getScalarAsInt(sql));
				}
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
		return vps;
	}


	/*public int canBePlayedOnAndroid() {
		return ANY_CLIENT;
	}
*/

	public boolean doWeCareWhoKilledUnits() {
		return true;
	}



}
