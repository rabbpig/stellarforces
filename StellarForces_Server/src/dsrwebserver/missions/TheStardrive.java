package dsrwebserver.missions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitsTable;

// todo - remove escape hatches from this mission
public class TheStardrive extends AbstractMission {

	private static final int MAX_TURNS = 30;
	private static final String SIDE1_NAME = "Attackers";
	private static final String SIDE2_NAME = "Defenders";

	/**
	 * 17/6/13 - Reduced side 2 creds from 240 to 210
	 */
	public TheStardrive() {
		super(AbstractMission.THE_STARDRIVE, CP_HIDDEN, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 350, 210, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.26f, AbstractMission.STRONG_WALLS, AbstractMapModel.BLOCK_WALLS, "thestardrive.txt", "", IS_CAMPAIGN, AbstractMission.GR_LEVEL_2, SPECIFIED_WIN_AT_END, "stardrive.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "The attackers must get the Stardrive back to the exit.";
	}


	@Override
	public void createUnits(MySQLConnection dbs, GamesTable game) throws FileNotFoundException, SQLException, IOException {
		super.createUnits(dbs, game);

		try {
			// Give the stardrive to the first unit in side 2
			int eq_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_STARDRIVE);
			if (dbs.getScalarAsInt("SELECT Count(*) FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + eq_id) == 0) {
				String sql = "SELECT UnitID FROM Units WHERE GameID = " + game.getID() + " AND Side = 2";
				ResultSet rs = dbs.getResultSet(sql);
				if (rs.next()) {
					EquipmentTable.CreateEquipment(dbs, game.getID(), rs.getInt("UnitID"), eq_id, 0);
				}
			}

			// Give scanner to side 1
			eq_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SCANNER);
			if (dbs.getScalarAsInt("SELECT Count(*) FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + eq_id) == 0) {
				String sql = "SELECT UnitID FROM Units WHERE GameID = " + game.getID() + " AND Side = 1";
				ResultSet rs = dbs.getResultSet(sql);
				if (rs.next()) {
					EquipmentTable.CreateEquipment(dbs, game.getID(), rs.getInt("UnitID"), eq_id, 0);
				}
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	@Override
	public String getMission1Liner(int side) {
		return "Get the Stardrive back to the start";
	}


	@Override
	public boolean hasSpecialVPCalc() {
		return true;
	}


	@Override
	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		if (side == 1) {
			String sql = "SELECT MapX, MapY, UnitID FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_STARDRIVE);
			ResultSet rs = dbs.getResultSet(sql);
			while (rs.next()) { // Loop through all the Stardrives in the game
				int y = rs.getInt("MapY");
				if (rs.getInt("UnitID") > 0) {
					UnitsTable unit = new UnitsTable(dbs);
					unit.selectRow(rs.getInt("UnitID"));
					y = unit.getMapY();
					unit.close();
				}
				if (side == 1 && y >= MapDataTable.GetMapSize(dbs, game.getMapDataID()).height - 5) {
					return 100;
				}
			}
		} else if (side == 2) {
			String sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + game.getID() + " AND Side = 1 AND Status = " + UnitsTable.ST_DEAD;
			return dbs.getScalarAsInt(sql_vps);
		}
		return 0;
	}


	public void nextTurn(MySQLConnection dbs, GamesTable game) throws SQLException {
		try {
			// Remove excess stardrives
			int eq_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_STARDRIVE);
			ResultSet rs = dbs.getResultSet("SELECT EquipmentID FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + eq_id);
			rs.next();
			while (rs.next()) {
				dbs.runSQLDelete("DELETE FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentID = " + rs.getInt("EquipmentID"));
				DSRWebServer.SendEmailToAdmin("Deleted Stardrive 1", "Game " + game.getID());
			}
			rs.close();

			// Remove excess scanner
			eq_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SCANNER);
			rs = dbs.getResultSet("SELECT EquipmentID FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + eq_id);
			rs.next();
			while (rs.next()) {
				dbs.runSQLDelete("DELETE FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentID = " + rs.getInt("EquipmentID"));
				DSRWebServer.SendEmailToAdmin("Deleted Scanner 1", "Game " + game.getID());
			}
			rs.close();
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}

	
	//  override if req
	public boolean canUnitsEscape(int side) {
		return side == 1;
	}

	

}
