package dsrwebserver.missions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.io.TextFile;
import ssmith.lang.Dates;
import ssmith.lang.Functions;
import ssmith.util.MemCache;
import ssmith.util.MyList;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.MapCreatorThread;
import dsrwebserver.maps.MapData;
import dsrwebserver.maps.MapLoader;
import dsrwebserver.maps.ServerMapSquare;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitNamesTable;
import dsrwebserver.tables.UnitsTable;

public abstract class AbstractMission {

	// Campaigns - todo - delete these
	public static final int CP_HIDDEN = 0;
	public static final int CP_STERNER_REGNIX = 1;
	public static final int CP_OMNI_CORP = 2;
	public static final int CP_SHADOW_CONSPIRACY = 3;
	public static final int CP_REBELLION = 4;
	public static final int CP_SIGMA_7 = 5;
	public static final int CP_AZARIAN_CRUSADE = 6;
	public static final int CP_DEL_ME = 7;
	public static final int CP_BLOBS = 8;
	//public static final int CP_MISC = 9;
	public static final int MAX_CAMPAIGNS = 9;


	// SF Missions - Used in the spreadsheet
	public static final int THE_ASSASSINS = 1; // Kill a certain unit
	public static final int MOONBASE_ASSAULT = 2;
	protected static final int OUTHOUSES = 3;
	protected static final int LAST_MAN_STANDING = 4; // All-out battle
	public static final int MOONBASE_ASSAULT_2v2 = 5;
	protected static final int THE_ALIEN_HORDES = 6;
	public static final int RESCUE_FROM_THE_MINES = 7;
	protected static final int BREAKOUT = 8;
	protected static final int THREE_PLAYER_ESCAPE = 9;
	protected static final int TALE_OF_TWO_BASES = 10;
	protected static final int DEFEND_THE_BASE = 11;
	protected static final int MOONBASE_ASSAULT_2 = 12;
	protected static final int ESCORT = 13;
	public static final int BLOB_ASSASSINS = 14;
	protected static final int FOUR_PLAYER_LAST_MAN_STANDING = 15;
	protected static final int FOUR_PLAYER_ESCAPE = 16;
	protected static final int THREE_PLAYER_LAST_MAN_STANDING = 17;
	protected static final int PRISON_MUTINY = 18;
	protected static final int LABORATORY_ATTACK = 19;
	protected static final int IMPREGNATOR = 20;
	protected static final int THREE_PLAYER_ASSASSINS = 21;
	protected static final int BATTLE_FOR_SIGMA_7 = 22;
	public static final int MELTDOWN = 23;
	protected static final int ALIEN_ESCAPE = 24;
	protected static final int THE_EXTERMINATORS = 25;
	protected static final int CAPTURE_THE_FLAG = 26;
	protected static final int PRISON_RIOT = 27;
	protected static final int CAPTURE_THE_FLAG_4P = 28;
	protected static final int STERNERS_REVENGE = 29;
	protected static final int MOONBASE_ASSAULT_3P = 30;
	protected static final int MOONBASE_ASSAULT_4P = 31;
	protected static final int ALIEN_HORDES_3P = 32;
	protected static final int FLAG_HEIST = 33;
	protected static final int FLAG_HEIST_4P = 34;
	protected static final int LAST_MAN_STANDING_LARGE_4_PLAYER = 35;
	protected static final int HIT_AND_RUN1_BREAKOUT = 36;
	protected static final int HIT_AND_RUN2_ESCORT = 37;
	protected static final int SCORCHED_EARTH = 38;
	protected static final int THE_ASSASSINS_CAMPAIGN = 39;
	protected static final int MOONBASE_ASSAULT_CAMPAIGN = 40;
	protected static final int TALE_OF_TWO_BASES_CAMPAIGN = 41;
	protected static final int ALIEN_CONTAINMENT = 42;
	protected static final int HIT_AND_RUN3_RESCUE_FROM_MINES = 43;
	public static final int SNAFU_MOONBASE_ASSAULT = 44;
	public static final int SF_PRACTISE_MISSION = 45;
	public static final int SNAFU_THE_ASSASSINS = 46;
	protected static final int EGG_HUNT = 47;
	protected static final int EGG_COLLECTOR = 48;
	protected static final int SNIPERS_PARADISE = 49;
	public static final int SNAFU_THE_ASSASSINS_2 = 50;
	public static final int SNAFU_SABOTAGE = 51;
	protected static final int ALIEN = 52;
	protected static final int DENUNCIATION = 53;
	protected static final int MOONBASE_ASSAULT_UNDERCOVER = 54;
	protected static final int KILL_FRENZY_4P = 55;
	protected static final int THE_BIG_PUSH = 56;
	protected static final int ODDWORLD = 57;
	protected static final int DENUNCIATION_2V2 = 58;
	protected static final int ATOTB_2V2 = 59;
	public static final int TERRORS_OF_THE_DEEP = 60;
	public static final int THE_ASSASSINS_2V2 = 61;
	public static final int ALIENS = 62;
	public static final int ESCORT_2V2 = 63;
	public static final int LAB_ATTACK_2V2 = 64;
	public static final int SUPERHUMAN_MOONBASE_ASSAULT = 65;
	public static final int HIDEOUT = 66;
	public static final int THE_STARDRIVE = 67;
	protected static final int CAPTURE_THE_FLAG_4P_CC_ONLY = 68;
	protected static final int ALL_SEEING_EYE = 69;
	protected static final int CRASH_SITE = 70;
	protected static final int THE_RAID = 71;
	protected static final int FLAG_COLLECTOR = 72;
	public static final int ALIEN_PRISON = 73;
	public static final int MELTDOWN_4P = 74;
	protected static final int SUPERHUMAN_DEFEND_THE_BASE = 75;
	public static final int MOONBASE_CLASH = 80;
	public static final int CLONE_WARS = 81;
	public static final int REANIMATOR = 82;
	public static final int REANIMATOR_PCCLIENT = 83;
	public static final int SF_PRACTISE_MISSION_WITH_AI = 84;
	public static final int THE_ASSASSINS_NEW_HOUSE = 85;
	public static final int DESCENT = 86; // Unused
	public static final int XENOGEDDON = 87;
	//public static final int THE_ASSASSINS_MULTI_STOREY = 88;
	public static final int COLONIZATION = 89;
	public static final int THE_ASSASSINS_TUNNELS = 90;
	public static final int BATTLE_ROYALE = 91;
	public static final int THE_FLOOD = 92;
	public static final int COLONIZATION2 = 93;
	public static final int ALIEN_COLONY = 94;
	public static final int UNIVERSE_MISSION = 95;
	public static final int THE_ASSYLUM = 96;
	public static final int COLONY3_COOP = 97;
	protected static final int ALIEN_HORDES_XENOPHOBIA = 98;
	protected static final int THE_HUNTED = 99;
	protected static final int MOONRISE = 100;
	protected static final int BLOB_OUTBREAK = 101;
	protected static final int CABIN_IN_THE_WOODS = 102;
	protected static final int ANGELS_OF_DEATH = 103;
	public static final int REANIMATOR_4P = 104;
	public static final int CYBER_HORDES = 105;
	public static final int REBELSTAR = 106;
	public static final int REBELSTAR_XENOMORPH = 107;
	public static final int REBELSTAR_THEOLDBASE = 108;
	public static final int REBELSTAR_ALIEN_HORDES = 109;
	public static final int PARADISE_VALLEY = 110;
	public static final int PARADISE_VALLEY_HUMANS = 111;
	public static final int COLONIZATION4_PLEASUREDOME = 112;

	// Removed: THREE_PLAYER_LAST_MAN_STANDING, MOONBASE_ASSAULT_4P, SCORCHED_EARTH, DESCENT, FOUR_PLAYER_LAST_MAN_STANDING, LAST_MAN_STANDING_LARGE_4_PLAYER, THREE_PLAYER_ESCAPE, 
	// LMS missions removed cos the player who hides usually wins.
	public static int SF_MISSIONS[] = {THE_ASSASSINS, MOONBASE_ASSAULT, OUTHOUSES, THE_ASSASSINS_CAMPAIGN, THREE_PLAYER_ASSASSINS, THE_ASSASSINS_2V2, STERNERS_REVENGE, HIDEOUT, THE_ASSASSINS_NEW_HOUSE, 
		MOONBASE_CLASH, MOONBASE_ASSAULT_2v2, MOONBASE_ASSAULT_3P, MOONBASE_ASSAULT_CAMPAIGN, MOONBASE_ASSAULT_2, MOONBASE_ASSAULT_UNDERCOVER, SUPERHUMAN_MOONBASE_ASSAULT, 
		RESCUE_FROM_THE_MINES, DENUNCIATION, DENUNCIATION_2V2, THE_EXTERMINATORS, ESCORT, ESCORT_2V2, ANGELS_OF_DEATH, TALE_OF_TWO_BASES, TALE_OF_TWO_BASES_CAMPAIGN, ATOTB_2V2, DEFEND_THE_BASE, SUPERHUMAN_DEFEND_THE_BASE, BREAKOUT, LABORATORY_ATTACK, LAB_ATTACK_2V2, MELTDOWN, MELTDOWN_4P, PRISON_MUTINY, PRISON_RIOT, THE_ASSYLUM, SNIPERS_PARADISE, FOUR_PLAYER_ESCAPE, CAPTURE_THE_FLAG, CAPTURE_THE_FLAG_4P, CAPTURE_THE_FLAG_4P_CC_ONLY, THE_STARDRIVE, FLAG_HEIST, FLAG_HEIST_4P, BATTLE_FOR_SIGMA_7, THE_BIG_PUSH, CABIN_IN_THE_WOODS, LAST_MAN_STANDING, KILL_FRENZY_4P, BATTLE_ROYALE, THE_FLOOD, MOONRISE, CYBER_HORDES, REBELSTAR,
		THE_ALIEN_HORDES, ALIEN_HORDES_3P, IMPREGNATOR, ALIEN_ESCAPE, ALIEN_CONTAINMENT, EGG_HUNT, EGG_COLLECTOR, ALIEN_HORDES_XENOPHOBIA, ALIEN, REBELSTAR_XENOMORPH, REBELSTAR_THEOLDBASE, REBELSTAR_ALIEN_HORDES,
		HIT_AND_RUN1_BREAKOUT, HIT_AND_RUN2_ESCORT, HIT_AND_RUN3_RESCUE_FROM_MINES, SNAFU_MOONBASE_ASSAULT, SNAFU_THE_ASSASSINS, SNAFU_THE_ASSASSINS_2, SNAFU_SABOTAGE, 
		BLOB_ASSASSINS, TERRORS_OF_THE_DEEP, BLOB_OUTBREAK, 
		ODDWORLD, ALL_SEEING_EYE, CRASH_SITE, THE_RAID, FLAG_COLLECTOR, ALIEN_PRISON, CLONE_WARS, REANIMATOR, REANIMATOR_4P, XENOGEDDON, THE_ASSASSINS_TUNNELS, COLONIZATION, COLONIZATION2, ALIEN_COLONY, COLONY3_COOP, COLONIZATION4_PLEASUREDOME, THE_HUNTED, PARADISE_VALLEY, PARADISE_VALLEY_HUMANS};

	// Games required
	protected static final int GR_NONE = 0;
	protected static final int GR_LEVEL_1 = 4;
	protected static final int GR_LEVEL_2 = 8;
	protected static final int GR_MULTI_PLAYER = 10;
	protected static final int GR_SNAFU = 16;

	// Consts
	public static final boolean NO_CEILINGS = false; // i.e. can see space
	public static final boolean SHOW_CEILINGS = true;

	public static final int INDESTRUCTABLE_WALLS = 0;
	public static final int STRONG_WALLS = 1;
	public static final int WEAK_WALLS = 2;

	public static final boolean IS_CAMPAIGN = true;
	public static final boolean IS_NOT_CAMPAIGN = false;

	public static final boolean IS_SNAFU = true;
	public static final boolean IS_NOT_SNAFU = false;

	public static final boolean HIGHEST_VPS_WIN_AT_END = true;
	public static final boolean SPECIFIED_WIN_AT_END = false; // The winner at the end is specified (either a draw or a certain side)

	private static MemCache units_cache = new MemCache(Dates.DAY);
	private static MemCache num_deply_squares_cache = new MemCache(Dates.DAY);

	protected int map_id;
	public String mission_name;
	private byte sides;
	protected String[] sidename;
	protected int max_turns;
	protected int[] sidecreds;
	protected int mission_id;
	private int winner_at_end;
	private int destroyable_walls_type;
	private boolean ceilings;
	private int[] vps_per_computer_side;
	private float min_client_version;
	public int thin_thick_walls;
	protected String overview_filename, camp_overview_filename = "";
	private boolean is_campaign;
	private int games_req_to_play;
	private boolean is_snafu;
	private boolean highest_vps_win_at_end;
	protected String map_filename;


	public static AbstractMission Factory(int type) {
		switch (type) {
		case THE_ASSASSINS:
			return new TheAssassinsMission();
		case THREE_PLAYER_ASSASSINS:
			return new ThreePlayer_TheAssassins();
		case OUTHOUSES:
			return new TheAssassins_Outhouses();
		case MOONBASE_ASSAULT:
			return new MoonbaseAssault();
		case MOONBASE_ASSAULT_3P:
			return new MoonbaseAssault_3Player();
		case MOONBASE_ASSAULT_4P:
			return new MoonbaseAssault_4Player();
		case RESCUE_FROM_THE_MINES:
			return new RescueFromTheMines();
		case LAST_MAN_STANDING:
			return new LastManStandingMission();
		case BREAKOUT:
			return new BreakoutMission();
		case TALE_OF_TWO_BASES:
			return new ATaleOfTwoBasesMission();
		case DEFEND_THE_BASE:
			return new DefendTheBase();
		case THE_ALIEN_HORDES:
			return new TheAlienHordes();
		case MOONBASE_ASSAULT_2:
			return new MoonbaseAssault2();
		case ESCORT:
			return new EscortMission();
		case THREE_PLAYER_ESCAPE:
			return new ThreePlayerEscape();
		case THREE_PLAYER_LAST_MAN_STANDING:
			return new ThreePlayerLastManStanding();
		case FOUR_PLAYER_ESCAPE:
			return new FourPlayerEscapeMission();
		case FOUR_PLAYER_LAST_MAN_STANDING:
			return new FourPlayerLastManStanding();
		case PRISON_MUTINY:
			return new PrisonMutinyMission();
		case LABORATORY_ATTACK:
			return new LaboratoryAttackMission();
		case BATTLE_FOR_SIGMA_7:
			return new BattleForSigma7Mission();
		case MELTDOWN:
			return new MeltdownMission();
		case IMPREGNATOR:
			return new ImpregnatorMission();
		case ALIEN_ESCAPE:
			return new AlienEscapeMission();
		case THE_EXTERMINATORS:
			return new TheExterminatorsMission();
		case CAPTURE_THE_FLAG:
			return new CaptureTheFlagMission();
		case CAPTURE_THE_FLAG_4P:
			return new CaptureTheFlag_4Player();
		case PRISON_RIOT:
			return new PrisonRiotMission();
		case ALIEN_HORDES_3P:
			return new ThreePlayerCoop_AlienHordes();
		case FLAG_HEIST:
			return new FlagHeistMission();
		case FLAG_HEIST_4P:
			return new FlagHeist_4Player();
		case LAST_MAN_STANDING_LARGE_4_PLAYER:
			return new LastManStanding2_LargeMap();
		case HIT_AND_RUN1_BREAKOUT:
			return new HitAndRun1_Breakout();
		case HIT_AND_RUN2_ESCORT:
			return new HitAndRun2_Escort();
		case THE_ASSASSINS_CAMPAIGN:
			return new TheAssassins_Campaign();
		case MOONBASE_ASSAULT_CAMPAIGN:
			return new MoonbaseAssault_Campaign();
		case TALE_OF_TWO_BASES_CAMPAIGN:
			return new ATaleOfTwoBases_Campaign();
		case HIT_AND_RUN3_RESCUE_FROM_MINES:
			return new HitAndRun3_RescueFromTheMines();
		case ALIEN_CONTAINMENT:
			return new AlienContainmentMission();
		case SNAFU_MOONBASE_ASSAULT:
			return new SnafuMoonbaseAssault();
		case SF_PRACTISE_MISSION:
			return new SFPractiseMission();
		case SF_PRACTISE_MISSION_WITH_AI:
			return new SFPractiseMissionWithAI();
		case SNAFU_THE_ASSASSINS:
			return new SnafuTheAssassins();
		case EGG_HUNT:
			return new EggHunt();
		case EGG_COLLECTOR:
			return new EggCollector();
		case SNIPERS_PARADISE:
			return new SnipersParadise();
		case SNAFU_THE_ASSASSINS_2:
			return new SnafuTheAssassins2();
		case SNAFU_SABOTAGE:
			return new SnafuSabotage();
		case ALIEN:
			return new AlienMission();
		case DENUNCIATION:
			return new Denunciation();
		case ALIENS:
			return new AliensMission();
		case MOONBASE_ASSAULT_UNDERCOVER:
			return new MoonbaseAssault_UndercoverAgent();
		case KILL_FRENZY_4P:
			return new KillFrenzy4P();
		case BLOB_ASSASSINS:
			return new BlobAssassins();
		case THE_BIG_PUSH:
			return new TheBigPush();
		case ODDWORLD:
			return new Oddworld();
		case DENUNCIATION_2V2:
			return new Denunciation2v2();
		case ATOTB_2V2:
			return new ATOTB_2v2();
		case TERRORS_OF_THE_DEEP:
			return new TerrorsOfTheDeep();
		case THE_ASSASSINS_2V2:
			return new TheAssassins2v2();
		case SCORCHED_EARTH:
			return new ScorchedEarth();
		case MOONBASE_ASSAULT_2v2:
			return new MoonbaseAssault_2v2();
		case STERNERS_REVENGE:
			return new SternersRevenge();
		case ESCORT_2V2:
			return new Escort2v2();
		case LAB_ATTACK_2V2:
			return new LaboratoryAttack2v2();
		case SUPERHUMAN_MOONBASE_ASSAULT:
			return new Superhuman_MoonbaseAssault();
		case HIDEOUT:
			return new Hideout();
		case THE_STARDRIVE:
			return new TheStardrive();
		case CAPTURE_THE_FLAG_4P_CC_ONLY:
			return new CaptureTheFlag_4P_CC();
		case ALL_SEEING_EYE:
			return new AllSeeingEyeMission();
		case CRASH_SITE:
			return new CrashSite();
		case THE_RAID:
			return new TheRaid();
		case FLAG_COLLECTOR:
			return new FlagCollector();
		case ALIEN_PRISON:
			return new AlienPrison();
		case MELTDOWN_4P:
			return new MeltdownMission_4P();
		case SUPERHUMAN_DEFEND_THE_BASE:
			return new SuperhumanDefendTheBase();
		case MOONBASE_CLASH:
			return new MoonbaseClash();
		case CLONE_WARS:
			return new CloneWars();
		case REANIMATOR:
			return new Reanimator();
		case REANIMATOR_PCCLIENT:
			return new Reanimator_PCClient();
		case THE_ASSASSINS_NEW_HOUSE:
			return new TheAssassinsNewHouse();
		case DESCENT:
			return new DescentMission();
		case XENOGEDDON:
			return new Xenogeddon();
		case COLONIZATION:
			return new Colonization();
		case THE_ASSASSINS_TUNNELS:
			return new TheAssassins_Tunnels();
		case BATTLE_ROYALE:
			return new BattleRoyale();
		case THE_FLOOD:
			return new TheFlood();
		case COLONIZATION2:
			return new Colonization2();
		case ALIEN_COLONY:
			return new AlienColony();
		case UNIVERSE_MISSION:
			return new UniverseMission();
		case THE_ASSYLUM:
			return new TheAssylum();
		case COLONY3_COOP:
			return new Colony3_Coop();
		case ALIEN_HORDES_XENOPHOBIA:
			return new AlienHordes_Xenophobia();
		case THE_HUNTED:
			return new TheHunted();
		case MOONRISE:
			return new MoonriseMission();
		case BLOB_OUTBREAK:
			return new BlobOutbreak();
		case CABIN_IN_THE_WOODS:
			return new CabinInTheWoods();
		case ANGELS_OF_DEATH:
			return new AngelsOfDeath();
		case REANIMATOR_4P:
			return new Reanimator_4P();
		case CYBER_HORDES:
			return new CyberHordes();
		case REBELSTAR:
			return new Rebelstar();
		case REBELSTAR_XENOMORPH:
			return new RebelstarXenomorph();
		case REBELSTAR_THEOLDBASE:
			return new RebelstarTheOldBase();
		case REBELSTAR_ALIEN_HORDES:
			return new RebelstarAlienHordes();
		case PARADISE_VALLEY:
			return new ParadiseValley();
		case PARADISE_VALLEY_HUMANS:
			return new ParadiseValleyHumans();
		case COLONIZATION4_PLEASUREDOME:
			return new Colonization4_PleasureDome();
		default:
			throw new RuntimeException("Unknown Mission Type: " + type);
		}

	}


	// todo - remove campaign_type param
	protected AbstractMission(int _type, int campaign_type, String s1name, String s2name, int _max_turns, int s1creds, int s2creds, int _winner_at_end, boolean _ceilings, int cpu_vps1, int cpu_vps2, float _min_client_version, int _destroyable_walls, int _map_type, String overview_filename, String _camp_overview_filename, boolean is_campaign, int _games_req_to_play, boolean _highest_vps_win_at_end, String map_filename) {
		this(_type, campaign_type, 2, s1name, s2name, "", "", _max_turns, s1creds, s2creds, -1, -1, _winner_at_end, _ceilings, cpu_vps1, cpu_vps2, -1, -1, _min_client_version, _destroyable_walls, _map_type, overview_filename, is_campaign, _games_req_to_play, IS_NOT_SNAFU, _highest_vps_win_at_end, map_filename);

		if (_camp_overview_filename.length() == 0) {
			camp_overview_filename = overview_filename;
		} else {
			camp_overview_filename = _camp_overview_filename;
		}
	}


	protected AbstractMission(int _type, int _campaign_type, int _sides, String s1name, String s2name, String s3name, String s4name, int _max_turns, int s1creds, int s2creds, int s3creds, int s4creds, int _winner_at_end, boolean _ceilings, int cpu_vps1, int cpu_vps2, int cpu_vps3, int cpu_vps4, float _min_client_version, int _destroyable_walls, int _map_type, String _overview_filename, boolean _is_campaign, int _games_req_to_play, boolean _is_snafu, boolean _highest_vps_win_at_end, String _map_filename) {
		mission_id = _type;
		mission_name = GetMissionNameFromType(_type, false, false);
		sides = (byte)_sides;
		sidename = new String[sides+1];
		sidename[1] = s1name;
		if (sides >= 2) {
			sidename[2] = s2name;
		}
		if (sides >= 3) {
			sidename[3] = s3name;
		}
		if (sides >= 4) {
			sidename[4] = s4name;
		}
		sidecreds = new int[sides+1];
		sidecreds[1] = s1creds;
		if (sides >= 2) {
			sidecreds[2] = s2creds;
		}
		if (sides >= 3) {
			sidecreds[3] = s3creds;
		}
		if (sides >= 4) {
			sidecreds[4] = s4creds;
		}
		max_turns = _max_turns;

		winner_at_end = _winner_at_end;
		ceilings = _ceilings;
		vps_per_computer_side = new int[sides+1];
		vps_per_computer_side[1] = cpu_vps1;
		if (sides >= 2) {
			vps_per_computer_side[2] = cpu_vps2;
		}
		if (sides >= 3) {
			vps_per_computer_side[3] = cpu_vps3;
		}
		if (sides >= 4) {
			vps_per_computer_side[4] = cpu_vps4;
		}
		min_client_version = _min_client_version;
		destroyable_walls_type = _destroyable_walls;
		thin_thick_walls = _map_type;
		overview_filename = _overview_filename;
		is_campaign = _is_campaign;
		games_req_to_play = _games_req_to_play;
		is_snafu = _is_snafu;
		highest_vps_win_at_end = _highest_vps_win_at_end;
		map_filename = _map_filename;

	}


	/**
	 * This is called to start the mapgen thread.
	 * 
	 */
	public final int createMapStart(MySQLConnection dbs, int _gameid) throws SQLException {
		map_id = MapDataTable.CreateRec(dbs, _gameid);
		new MapCreatorThread(dbs, this, _gameid);
		return map_id;
	}


	public void createMap(MySQLConnection dbs, int gameid) throws Exception { // Must be public as it gets overridden!
		this.createBasicMap(dbs, gameid);
	}


	/**
	 * This is called by the MapCreatorThread.  Don't call directly!
	 * 
	 */
	public MapData createBasicMap(MySQLConnection dbs, int gameid) throws Exception {
		MapData map = MapLoader.Import(dbs, gameid, this.map_filename, true);
		storeMapDataInSQL(dbs, map);
		return map;
	}



	protected void storeMapDataInSQL(MySQLConnection dbs, MapData map) throws SQLException {
		MapDataTable.SetSize(dbs, map_id, map.getMapWidth(), map.getMapHeight());

		for (byte y=0 ; y<map.getMapHeight() ; y++) {
			for (byte x=0 ; x<map.getMapWidth() ; x++) {
				ServerMapSquare sq = map.getSq(x, y);
				MapDataTable.CreateMapSquare(dbs, map_id, sq); // Slow!
				Thread.yield();
			}
		}
	}


	public abstract String getSideDescription(int side);


	public int getType() {
		return this.mission_id;
	}


	public String getMissionOverview(boolean camp_game) {
		String html = "";
		try {
			if (camp_game == false) {
				html = TextFile.ReadAll(DSRWebServer.SERVER_DATA_DIR + "mission_text/" + overview_filename, "<br />", false);
			} else {
				html = TextFile.ReadAll(DSRWebServer.SERVER_DATA_DIR + "mission_text/" + camp_overview_filename, "<br />", false);
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
		return html;
	}


	public void createUnits(MySQLConnection dbs, GamesTable game) throws SQLException, FileNotFoundException, IOException {
		/*try {
			if (dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + side) >= 2) {
				return; // They already exist!
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}*/
		UnitsTable units = new UnitsTable(dbs);

		boolean any_found = false;

		for (int side = 1; side <= this.getNumOfSides() ; side++) {
			TextFile tf = new TextFile();
			tf.openFile(DSRWebServer.SERVER_DATA_DIR + "units.csv", TextFile.READ);
			tf.readLine(); // Skip header
			while (tf.isEOF() == false) {
				String s = tf.readLine();
				if (s.length() > 0) {
					if (s.startsWith("#") == false) {
						String data[] = s.split("\t");
						if (data.length > 14) {
							if (data[0].equalsIgnoreCase("" + this.mission_id)) {
								any_found = true;
								int file_side = Functions.ParseInt(data[4]);
								if (file_side == side) {
									int loginid = game.getLoginIDFromSide(side);
									String unit_name = data[3].replaceAll("\"", "");
									int order = Functions.ParseInt(data[1]);
									int model_type = Functions.ParseInt(data[2]);
									int aps = Functions.ParseInt(data[5]);
									int health = Functions.ParseInt(data[6]);
									int vps_if_killed = Functions.ParseInt(data[7]);
									int vps_if_escape = Functions.ParseInt(data[8]);
									int energy = Functions.ParseInt(data[9]);
									int shot_skill = Functions.ParseInt(data[10]) + Functions.rnd(-10, 10);
									int combat_skill = Functions.ParseInt(data[11]) + Functions.rnd(-10, 10);
									//int can_use_equipment = Functions.ParseInt(data[12]);
									int strength = Functions.ParseInt(data[12]);
									boolean can_rename = Functions.ParseInt(data[13]) == 1;
									int can_deploy = Functions.ParseInt(data[14]);
									//int silent = Functions.ParseInt(data[15]);
									int morale = Functions.ParseInt(data[15]);
									int can_equip = Functions.ParseInt(data[16]);
									if (game.isCampGame() && can_rename) {
										// Not done here any more!
										/*CampUnitsTable camp_units = new CampUnitsTable(dbs);
										camp_units.selectNextUnit(game.getLoginIDFromSide(side));
										int id = units.createUnit(game.getID(), order, model_type, camp_units.getName(), side, camp_units.getMaxAPs(), camp_units.getMaxHealth(), vps_if_killed, vps_if_escape, camp_units.getMaxEnergy(), camp_units.getShotSkill(), camp_units.getCombatSkill(), camp_units.getStrength(), can_deploy, camp_units.getMaxMorale());
										camp_units.setUnitID(id);
										camp_units.setSelected();*/
									} else {
										if (can_rename) {
											String new_name = UnitNamesTable.GetName(dbs, loginid, Functions.ParseInt(data[1]));
											if (new_name.length() > 0) {
												unit_name = new_name; 
											}
										}
										units.createUnit(game.getID(), order, model_type, unit_name, side, aps + Functions.rnd(-6, 6), health + Functions.rnd(-10, 10), vps_if_killed, vps_if_escape, energy + Functions.rnd(-50, 50), shot_skill, combat_skill, strength + Functions.rnd(-2, 4), can_deploy, morale + Functions.rnd(-10, 10), 0, can_equip);
									}
								}
							}
						}
					}
				}
			}
			tf.close();

		}
		units.close();
		if (!any_found) {
			throw new RuntimeException("No unit data found (game " + game.getID() + ").");
		}
	}


	public void renameUnits(MySQLConnection dbs, GamesTable game, int side) throws SQLException, IOException {
		if (game.isCampGame()) {
			return;
		}
		int loginid = game.getLoginIDFromSide(side);
		//UnitsTable units = new UnitsTable(dbs);

		TextFile tf = new TextFile();
		tf.openFile(DSRWebServer.SERVER_DATA_DIR + "units.csv", TextFile.READ);
		tf.readLine(); // Skip header
		//boolean any_found = false;
		int seq = 0;
		while (tf.isEOF() == false) {
			String s = tf.readLine();
			if (s.length() > 0) {
				if (s.startsWith("#") == false) {
					String data[] = s.split("\t");
					if (data.length > 0) {
						if (data[0].equalsIgnoreCase("" + this.mission_id)) {
							//any_found = true;
							int file_side = Functions.ParseInt(data[4]);
							if (file_side == side) {
								seq++;
								/*String unit_name = data[3].replaceAll("\"", "");
								int order = Functions.ParseInt(data[1]);
								int model_type = Functions.ParseInt(data[2]);
								int aps = Functions.ParseInt(data[5]);
								int health = Functions.ParseInt(data[6]);
								int vps_if_killed = Functions.ParseInt(data[7]);
								int vps_if_escape = Functions.ParseInt(data[8]);
								int energy = Functions.ParseInt(data[9]);
								int shot_skill = Functions.ParseInt(data[10]) + Functions.rnd(-10, 10);
								int combat_skill = Functions.ParseInt(data[11]) + Functions.rnd(-10, 10);
								int strength = Functions.ParseInt(data[12]);
								int can_deploy = Functions.ParseInt(data[14]);
								int morale = Functions.ParseInt(data[15]);
								int shot_damage = -1;*/
								boolean can_rename = Functions.ParseInt(data[13]) == 1;
								if (can_rename) {
									String new_name = UnitNamesTable.GetName(dbs, loginid, Functions.ParseInt(data[1]));
									if (new_name.length() > 0) {
										UnitsTable.RenameUnit(dbs, game.getID(), side, seq, new_name); 
									}
								}
							}
						}
					}
				}
			}
		}
		tf.close();

	}


	public String getSideName(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		if (game.isCampGame()) {
			LoginsTable login = new LoginsTable(dbs);
			try {
				int loginid = game.getLoginIDFromSide(side);
				if (loginid > 0) {
					login.selectRow(loginid);
					String name = login.getCampTeamName();
					if (name != null) {
						if (name.length() > 0) {
							return name;
						}				
					}
				}
			} finally {
				login.close();
			}
		}
		return sidename[side];
	}


	public int getCreditsForSide(int side) {
		return sidecreds[side];
	}


	public static boolean IsValidMission(int i) {
		/*if (i == here) {
			return false;
		}*/
		try {
			GetMissionNameFromType(i, false, false);
			return true;
		} catch (Exception ex) {
			// Do nothing
			ex.printStackTrace();
		}
		return false;
	}


	/**
	 * These are also used for the mission_text filenames
	 * @param type
	 * @return
	 */
	public static String GetMissionNameFromType(int type, boolean link, boolean camp) {
		String start = "", end = "";
		if (link) {
			start = "<a href=\"/dsr/missiondescriptions.cls?type=" + type + "&amp;camp=" + (camp?"1":"0") + "\" rel=\"nofollow\" >";
			end = "</a>";
		}
		// If you change or add to this, you'll need to change
		// the name of the mission text file.
		switch (type) {
		case LAST_MAN_STANDING:
			return start + "Last Man Standing" + end;
		case THE_ASSASSINS:
			return start + "The Assassins" + end;
		case THREE_PLAYER_ASSASSINS:
			return start + "3-Player The Assassins" + end;
		case OUTHOUSES:
			return start + "Outhouses" + end;
		case MOONBASE_ASSAULT:
			return start + "Moonbase Assault" + end;
		case MOONBASE_ASSAULT_3P:
			return start + "Moonbase Assault 3-Player" + end;
		case MOONBASE_ASSAULT_4P:
			return start + "Moonbase Assault 4-Player" + end;
		case RESCUE_FROM_THE_MINES:
			return start + "Rescue from the Mines" + end;
		case BREAKOUT:
			return start + "Breakout" + end;
		case TALE_OF_TWO_BASES:
			return start + "A Tale of Two Moonbases" + end;
		case DEFEND_THE_BASE:
			return start + "Defend the Base" + end;
		case THE_ALIEN_HORDES:
			return start + "The Alien Hordes" + end;
		case MOONBASE_ASSAULT_2:
			return start + "Moonbase Assault 2" + end;
		case ESCORT:
			return start + "Escort" + end;
		case THREE_PLAYER_ESCAPE:
			return start + "3-Player Escape" + end;
		case THREE_PLAYER_LAST_MAN_STANDING:
			return start + "3-Player Last Man Standing" + end;
		case FOUR_PLAYER_ESCAPE:
			return start + "Escape 4-Player " + end;
		case FOUR_PLAYER_LAST_MAN_STANDING:
			return start + "Last Man Standing 4-Player" + end;
		case PRISON_MUTINY:
			return start + "Prison Mutiny" + end;
		case LABORATORY_ATTACK:
			return start + "Laboratory Attack" + end;
		case BATTLE_FOR_SIGMA_7:
			return start + "Battle for Sigma 7" + end;
		case MELTDOWN:
			return start + "Meltdown" + end;
		case IMPREGNATOR:
			return start + "Impregnator" + end;
		case ALIEN_ESCAPE:
			return start + "Alien Escape" + end;
		case THE_EXTERMINATORS:
			return start + "The Exterminators" + end;
		case CAPTURE_THE_FLAG:
			return start + "Capture the F.L.A.G." + end;
		case CAPTURE_THE_FLAG_4P:
			return start + "Capture the F.L.A.G. 4-Player" + end;
		case PRISON_RIOT:
			return start + "Prison Riot" + end;
		case ALIEN_HORDES_3P:
			return start + "Alien Hordes 3-Player" + end;
		case FLAG_HEIST:
			return start + "F.L.A.G. Heist" + end;
		case FLAG_HEIST_4P:
			return start + "F.L.A.G. Heist 4-Player" + end;
		case LAST_MAN_STANDING_LARGE_4_PLAYER:
			return start + "Last Man Standing - CTF Map 4-Player" + end;
		case HIT_AND_RUN1_BREAKOUT:
			return start + "Hit and Run 1 - Breakout" + end;
		case HIT_AND_RUN2_ESCORT:
			return start + "Hit and Run 2 - Escort" + end;
		case THE_ASSASSINS_CAMPAIGN:
			return start + "Assassinate - Campaign Version" + end;
		case MOONBASE_ASSAULT_CAMPAIGN:
			return start + "Moonbase Assault - Campaign Version" + end;
		case TALE_OF_TWO_BASES_CAMPAIGN:
			return start + "A Tale of Two Moonbases - Campaign Version" + end;
		case HIT_AND_RUN3_RESCUE_FROM_MINES:
			return start + "Hit and Run 3 - Rescue from the Mines" + end;
		case ALIEN_CONTAINMENT:
			return start + "Alien Containment" + end;
		case SNAFU_MOONBASE_ASSAULT:
			return start + "SNAFU: Moonbase Assault" + end;
		case SF_PRACTISE_MISSION:
			return start + "Practise Mission" + end;
		case SF_PRACTISE_MISSION_WITH_AI:
			return start + "Practise Mission with AI" + end;
		case SNAFU_THE_ASSASSINS:
			return start + "SNAFU: The Assassins" + end;
		case EGG_HUNT:
			return start + "Egg Hunt" + end;
		case EGG_COLLECTOR:
			return start + "Egg Collector" + end;
		case SNIPERS_PARADISE:
			return start + "Sniper's Paradise" + end;
		case SNAFU_THE_ASSASSINS_2:
			return start + "SNAFU: The Assassins 2" + end;
		case SNAFU_SABOTAGE:
			return start + "SNAFU: Sabotage" + end;
		case ALIEN:
			return start + "Alien" + end;
		case DENUNCIATION:
			return start + "Denunciation" + end;
		case ALIENS:
			return start + "Aliens" + end;
		case MOONBASE_ASSAULT_UNDERCOVER:
			return start + "Moonbase Assault: Undercover Agent" + end;
		case KILL_FRENZY_4P:
			return start + "Kill Frenzy 4-Player" + end;
		case BLOB_ASSASSINS:
			return start + "The Blob Assassins" + end;
		case THE_BIG_PUSH:
			return start + "The Big Push" + end;
		case ODDWORLD:
			return start + "Oddworld" + end;
		case DENUNCIATION_2V2:
			return start + "Denunciation 2v2" + end;
		case ATOTB_2V2:
			return start + "A Tale of Two Bases 2v2" + end;
		case TERRORS_OF_THE_DEEP:
			return start + "Terrors of the Deep" + end;
		case THE_ASSASSINS_2V2:
			return start + "The Assassins 2v2" + end;
		case SCORCHED_EARTH:
			return start + "Scorched Earth" + end;
		case MOONBASE_ASSAULT_2v2:
			return start + "Moonbase Assault 2v2" + end;
		case STERNERS_REVENGE:
			return start + "Sterner's Revenge" + end;
		case ESCORT_2V2:
			return start + "Escort 2v2" + end;
		case LAB_ATTACK_2V2:
			return start + "Laboratory Attack 2v2" + end;
		case SUPERHUMAN_MOONBASE_ASSAULT:
			return start + "Superhuman: Moonbase Assault" + end;
		case HIDEOUT:
			return start + "Hideout" + end;
		case THE_STARDRIVE:
			return start + "The Stardrive" + end;
		case CAPTURE_THE_FLAG_4P_CC_ONLY:
			return start + "Capture the F.L.A.G. 4-Player CC-Only" + end;
		case ALL_SEEING_EYE:
			return start + "All Seeing Eye" + end;
		case CRASH_SITE:
			return start + "Crash Site" + end;
		case THE_RAID:
			return start + "The Raid" + end;
		case FLAG_COLLECTOR:
			return start + "Flag Collector" + end;
		case ALIEN_PRISON:
			return start + "Alien Prison" + end;
		case MELTDOWN_4P:
			return start + "Meltdown 4-Player" + end;
		case SUPERHUMAN_DEFEND_THE_BASE:
			return start + "Superhuman: Defend the Base" + end;
		case MOONBASE_CLASH:
			return start + "Moonbase Clash" + end;
		case CLONE_WARS:
			return start + "Clone Wars" + end;
		case REANIMATOR:
			return start + "Reanimator" + end;
		case REANIMATOR_PCCLIENT:
			return start + "Reanimator" + end;
		case THE_ASSASSINS_NEW_HOUSE:
			return start + "The Assassins - New House" + end;
		case DESCENT:
			return start + "Descent (beta)" + end;
		case XENOGEDDON:
			return start + "Xenogeddon" + end;
		case COLONIZATION:
			return start + "Colonization" + end;
		case THE_ASSASSINS_TUNNELS:
			return start + "The Assassins - Tunnels" + end;
		case BATTLE_ROYALE:
			return start + "Battle Royale" + end;
		case THE_FLOOD:
			return start + "The Flood" + end;
		case COLONIZATION2:
			return start + "Colonization 2" + end;
		case ALIEN_COLONY:
			return start + "Alien Colony*" + end;
		case UNIVERSE_MISSION:
			return start + "Universe Mission" + end;
		case THE_ASSYLUM:
			return start + "The Assylum" + end;
		case COLONY3_COOP:
			return start + "Colony 3: Co-Op" + end;
		case ALIEN_HORDES_XENOPHOBIA:
			return start + "Alien Hordes: Xenophobia" + end;
		case THE_HUNTED:
			return start + "The Hunted" + end;
		case MOONRISE:
			return start + "Moonrise" + end;
		case BLOB_OUTBREAK:
			return start + "Blob Outbreak" + end;
		case CABIN_IN_THE_WOODS:
			return start + "Cabin in the Woods" + end;
		case ANGELS_OF_DEATH:
			return start + "The Angels of Death" + end;
		case REANIMATOR_4P:
			return start + "Reanimator 4-Player" + end;
		case CYBER_HORDES:
			return start + "The Cyber Hordes" + end;
		case REBELSTAR:
			return start + "Rebelstar" + end;
		case REBELSTAR_XENOMORPH:
			return start + "Rebelstar Xenomorph" + end;
		case REBELSTAR_THEOLDBASE:
			return start + "Rebelstar - The Old Base" + end;
		case REBELSTAR_ALIEN_HORDES:
			return start + "Rebelstar Alien Hordes" + end;
		case PARADISE_VALLEY:
			return start + "Paradise Valley (beta)" + end;
		case PARADISE_VALLEY_HUMANS:
			return start + "Paradise Valley Human v Human (beta)" + end;
		case COLONIZATION4_PLEASUREDOME:
			return start + "Colonization 4 - Pleasure Dome (beta)" + end;
		default:
			throw new RuntimeException("Mission " + type + " unknown");
		}
	}


	public String getMissionName(boolean link, boolean camp) {
		return AbstractMission.GetMissionNameFromType(this.mission_id, link, camp);
	}


	public abstract String getShortDesc();


	public int getMaxTurns() {
		return this.max_turns;
	}


	public int getWallsDestroyableType() {
		return this.destroyable_walls_type;
	}


	public int getWinnerAtEnd() {
		return this.winner_at_end;
	}


	public boolean showCeilings() {
		return this.ceilings;
	}


	public boolean isCampaignMission() {
		return this.is_campaign;
	}


	public int getVPsPerComputer(int side) {
		return vps_per_computer_side[side];
	}

	public float getMinClientVersion() {
		return this.min_client_version;
	}


	public byte getNumOfSides() {
		return sides;
	}


	public int getThinThickWallType() {
		return this.thin_thick_walls;
	}


	public boolean isItemTypeAllowed(int t, int side) {
		// Override if reqd
		return true;
	}


	public boolean isItemAllowed(String code, int side) {
		// Override if reqd
		if (code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER)) {
			if (this.mission_id == AbstractMission.ALIEN ||
					this.mission_id == AbstractMission.PRISON_MUTINY ||
					this.mission_id == AbstractMission.THE_ASSYLUM ||
					this.mission_id == AbstractMission.PRISON_RIOT) {				
				return  false;
			}
		}/* else if (code.equalsIgnoreCase(EquipmentTypesTable.CD_FIRE_EXTINGUISHER) || code.equalsIgnoreCase(EquipmentTypesTable.CD_INCENDIARY_GRENADE) || code.equalsIgnoreCase(EquipmentTypesTable.CD_FLAMETHROWER)) {
			if (this.mission_id != AbstractMission.LAST_MAN_STANDING && this.mission_id != AbstractMission.LAST_MAN_STANDING_LARGE_4_PLAYER && this.mission_id != AbstractMission.BATTLE_ROYALE && this.mission_id != AbstractMission.THE_ASSASSINS && this.mission_id != AbstractMission.MOONBASE_ASSAULT) {
				return false;
			}
		}*/
		return true;
	}


	public boolean canSideSeeEnemies(int side) {
		// Override if reqd
		return false;
	}


	public int getMaxProtection(int side) {
		// Override if reqd
		return -1;
	}


	public void nextTurn(MySQLConnection dbs, GamesTable game) throws SQLException {
		// Override if reqd
	}


	public void nextPhase(MySQLConnection dbs, GamesTable game) throws SQLException {
		// Override if reqd
	}


	/*public static void AddEquipmentToMap(MySQLConnection dbs, int gameid, MapData map, String code, int x, int z, int owner_side) throws SQLException {
		AddEquipmentToMap(dbs, gameid, map, code, x, z, 0, owner_side);
	}*/


	public static void AddEquipmentToMap(MySQLConnection dbs, int gameid, MapData map, String code, int x, int z, int ammo, int owner_side) throws SQLException {
		int equiptypeid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, code);
		int id = EquipmentTable.CreateEquipment(dbs, gameid, -1, equiptypeid, ammo);

		if (map != null && (x < 0 || z < 0)) {
			x = (map.getMapWidth()/2)+1;
			z = map.getMapHeight()/2;
		}
		int seen_by_side[] = new int[5];
		if (owner_side >= 0) {
			seen_by_side[owner_side] = 1;
		}
		EquipmentTable.Update(dbs, id, -1, x, z, ammo, 0, 0, 0, 0, seen_by_side, 0);

	}


	public int getGamesReqToPlay() {
		return games_req_to_play;
	}


	public boolean isSnafu() {
		return this.is_snafu;
	}


	/**
	 * Override if req'd.
	 * @return
	 */
	public String getSnafuMissionText(GamesTable game, int side) throws Exception {
		throw new RuntimeException("getSnafuMissionText() not implemented for game " + game.getID() +" mission " +game.getMissionID() + "!");
	}


	/**
	 * Override if req'd.
	 */
	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		throw new RuntimeException("getVPs() not implemented!");
	}


	public int getMissionID() {
		return this.mission_id;
	}


	//  override if req
	public boolean canUnitsEscape(int side) {
		return false;
	}


	public boolean aliensImpregnate() {
		// Override if req
		return false;

	}


	public boolean hasSpecialVPCalc() {
		// Override if req
		return false;
	}


	public boolean highestVPsWinAtEnd() {
		return this.highest_vps_win_at_end;
	}


	public String getMapFilename() {
		return this.map_filename;
	}


	public boolean isAlwaysAdvanced() {
		return false; // Override if req
	}


	public int getUnderCoveragentsForSide(int s) {
		return 0; // Override if req
	}


	public boolean doWeCareWhoDestroyedComputers() {
		return true; // Override if req
	}


	public boolean doWeCareWhoOwnsTheComputers() {
		return false; // Override if req
	}


	public boolean doesMissionInvolveFlag() {
		return false; // Override if req
	}


	public abstract String getMission1Liner(int side);


	/**
	 * Override if req
	 */
	public MyList<Integer> getSidesForSide(int side) {
		return MyList.CreateIntsFromInts(side);
	}


	// Override if required
	public int doesSideHaveAI(int side) {
		// Override if required - return 1 if so
		return 0;
	}


	public int getMinUnitsForSide(int side) throws IOException { // For campaign games.
		if (this.canUnitsEscape(side)) {
			return this.getUnitsForSide(side);
		} else {
			return this.getUnitsForSide(side)/2;
		}
	}


	public int getMaxUnitsForSide(int side) throws IOException { // For campaign games.
		return (int)(this.getUnitsForSide(side) * 1.25f);
	}


	public int getUnitsForSide(int our_side) throws IOException {
		// Is it in the cache?
		String key = this.mission_id + "_" + our_side;
		if (units_cache.containsKey(key)) {
			return Byte.parseByte(units_cache.get(key).toString());
		}

		byte num = 0;
		TextFile tf = new TextFile();
		String filename = DSRWebServer.SERVER_DATA_DIR + "units.csv";
		tf.openFile(filename, TextFile.READ);
		while (tf.isEOF() == false) {
			String s = tf.readLine();
			if (s.length() > 0) {
				if (s.startsWith("#") == false) {
					String data[] = s.split("\t");
					if (data.length > 0) {
						if (data[0].equalsIgnoreCase("" + mission_id)) {
							int side = Functions.ParseInt(data[4]);
							if (side == our_side) {
								num++;
							}
						}
					}
				}
			}
		}
		tf.close();
		units_cache.put(key, ""+num);
		return num;
	}


	public int getNumDeploySquaresForSide(MySQLConnection dbs, int side) throws IOException, SQLException {
		// Is it in the cache?
		try {
			String key = this.mission_id + "_" + side;
			if (num_deply_squares_cache.containsKey(key)) {
				return Integer.parseInt(num_deply_squares_cache.get(key).toString());
			}

			if (this.getMapFilename().endsWith(".csv")) {
				MapData data = MapLoader.Import(dbs, -1, this.getMapFilename(), false);
				int max_units = data.num_deploy_squares[side];
				num_deply_squares_cache.put(key, "" + max_units);
				return max_units;
			} else {
				return 999; // Do nothing since it's a generated map
			}
		} catch (Exception ex) {
			throw new RuntimeException("Error getting getNumDeploySquaresForSide, mission " + mission_id + ", side " + side, ex);
		}
	}


	/**
	 * Override if required.
	 */
	public boolean doWeCareWhoKilledUnits() {
		return false;
	}


	/**
	 * Override if required.
	 */
	public boolean addComradesVPs(int side) {
		return true;
	}


	/**
	 * Override if required.
	 */
	public boolean canSideWinByKillingAllOpposition(int side) {
		return true;
	}


	/**
	 * Override if required.
	 */
	public void mapCreated(MySQLConnection dbs, int gameid) throws SQLException {
		// Do nothing
	}


	/**
	 * Override if required.
	 */
	public int getMinAndroidVersion() {
		return 0;
	}


	/**
	 * Override if required.
	 */
	public int canBuildAndDismantle() {
		return 0;
	}


	/*	protected void giveEquipmentToUnit(MySQLConnection dbs, GamesTable game, String eq_code, int unitid) throws SQLException {
		int eq_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, eq_code);
		EquipmentTable.CreateEquipment(dbs, game.getID(), unitid, eq_id, 0);
	}*/
}

