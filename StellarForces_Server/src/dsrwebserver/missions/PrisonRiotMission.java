package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class PrisonRiotMission extends AbstractMission {

	private static final int MAX_TURNS = 25;
	private static final String SIDE1_NAME = "The Prisoners";
	private static final String SIDE2_NAME = "The Guards";

	public PrisonRiotMission() {
		super(AbstractMission.PRISON_RIOT, CP_REBELLION, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 30, 300, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.07f, AbstractMission.WEAK_WALLS, AbstractMapModel.BLOCK_WALLS, "prison_riot.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, SPECIFIED_WIN_AT_END, "prison_mutiny.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 1;
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "A large group of poorly-armed prisoners must escape from the prison complex.";
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			if (side == 1) { // Prisoners
				return t == EquipmentTypesTable.ET_CC_WEAPON;//.GRENADE && t != EquipmentTypesTable.DEATH_GRENADE;
			} else { // Guards
				return t != EquipmentTypesTable.ET_DEATH_GRENADE && t != EquipmentTypesTable.ET_GRENADE;
			}
		}
		return true;
	}


	@Override
	public int getMaxProtection(int side) {
		return 20;
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must escape from the prison";
		case 2:
			return "You must kill the prisoners";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}



}
