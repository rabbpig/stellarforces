package dsrwebserver.missions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

/**
 * 
 * 13/8/15 - Side 1 has 550 (was 530), Side 2 has 610 (was 630) creds
 * 31/8/15 - now 30 turns (was 25) and 20VPs per cpu (was 10)
 */
public class Rebelstar extends AbstractMission {

	private static final int MAX_TURNS = 30;

	public Rebelstar() {
		super(AbstractMission.REBELSTAR, -1, "Rebel Squad", "Droid Squad", MAX_TURNS, 550, 610, 2, AbstractMission.SHOW_CEILINGS, 20, 0, 1.58f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "rebelstar.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_1, SPECIFIED_WIN_AT_END, "rebelstar.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "Rebel Attackers";
		case 2:
			return "Droid Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "The moonbase must be defended from the droids.";
	}


	@Override
	public void createUnits(MySQLConnection dbs, GamesTable game) throws SQLException, FileNotFoundException, IOException {
		super.createUnits(dbs, game);

		UnitsTable unit = new UnitsTable(dbs);

		// Set up patrol droids
		int sniper_rifle_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SNIPERRIFLE);
		ResultSet rs = dbs.getResultSet("SELECT * FROM Units WHERE GameID = " + game.getID() + " AND Side = 2 AND OrderBy BETWEEN 7 AND 12"); // todo - use model types not numbers
		while (rs.next()) {
			unit.selectRow(rs.getInt("UnitID"));
			unit.setArmourType(2);
			if (EquipmentTable.GetNumItemsOnUnit(dbs, unit.getID()) == 0) {
				EquipmentTable.CreateEquipment(dbs, game.getID(), unit.getID(), sniper_rifle_id, 100);
			}
		}
		rs.close();

		// Set up battle droid
		int blaster_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_BLASTER);
		rs = dbs.getResultSet("SELECT * FROM Units WHERE GameID = " + game.getID() + " AND Side = 2 AND OrderBy BETWEEN 13 AND 14"); // todo - use model types not numbers
		while (rs.next()) {
			unit.selectRow(rs.getInt("UnitID"));
			unit.setArmourType(4);
			if (EquipmentTable.GetNumItemsOnUnit(dbs, unit.getID()) == 0) {
				EquipmentTable.CreateEquipment(dbs, game.getID(), unit.getID(), blaster_id, 100);
			}
		}
		rs.close();

		unit.close();
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			if (side == 1) {
				return true;
			} else {
				return t == EquipmentTypesTable.ET_GUN || t == EquipmentTypesTable.ET_AMMO_CLIP;
			}
		}
		return true;
	}


	/*@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false;
		}
		return false;
	}*/


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must destroy the computers";
		case 2:
			return "You must kill the rebels";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


}
