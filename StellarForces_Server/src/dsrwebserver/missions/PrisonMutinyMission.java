package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class PrisonMutinyMission extends AbstractMission {

	private static final int MAX_TURNS = 25;
	private static final String SIDE1_NAME = "The Prisoners";
	private static final String SIDE2_NAME = "The Guards";

	public PrisonMutinyMission() {
		super(AbstractMission.PRISON_MUTINY, CP_REBELLION, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 400, 170, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.07f, AbstractMission.WEAK_WALLS, AbstractMapModel.BLOCK_WALLS, "prison_mutiny.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_1, SPECIFIED_WIN_AT_END, "prison_mutiny.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 1;
	}

	
	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	public String getShortDesc() {
		return "A small group of well-armed prisoners must escape from the prison complex.";
	}

	
	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must escape from the prison";
		case 2:
			return "You must kill the prisoners";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
		return t != EquipmentTypesTable.ET_GRENADE;
		}
		return true;
	}



}
