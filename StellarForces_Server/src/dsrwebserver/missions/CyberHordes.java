package dsrwebserver.missions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public class CyberHordes extends AbstractMission {

	private static final int MAX_TURNS = 25;

	public CyberHordes() {
		super(AbstractMission.CYBER_HORDES, -1, "Rebel Squad", "Droid Squad", MAX_TURNS, 330, 430, 1, AbstractMission.SHOW_CEILINGS, 0, 20, 1.58f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "cyberhordes.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "cyberhordes.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "Rebel Defenders";
		case 2:
			return "Droid Attackers";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "The moonbase must be defended from the droids.";
	}


	@Override
	public void createUnits(MySQLConnection dbs, GamesTable game) throws SQLException, FileNotFoundException, IOException {
		super.createUnits(dbs, game);
		
		UnitsTable unit = new UnitsTable(dbs);
		
		// Set up patrol droids
		int sniper_rifle_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SNIPERRIFLE);
		ResultSet rs = dbs.getResultSet("SELECT * FROM Units WHERE GameID = " + game.getID() + " AND Side = 2 AND OrderBy BETWEEN 4 AND 6");  // todo - use model types not numbers
		while (rs.next()) {
			unit.selectRow(rs.getInt("UnitID"));
			unit.setArmourType(2);
			if (unit.hasEquipment() == false) {
				EquipmentTable.CreateEquipment(dbs, game.getID(), unit.getID(), sniper_rifle_id, 100);
			}
		}
		rs.close();
		
		// Set up battle droid
		int blaster_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_BLASTER);
		rs = dbs.getResultSet("SELECT * FROM Units WHERE GameID = " + game.getID() + " AND Side = 2 AND OrderBy = 7"); // todo - use model types not numbers
		while (rs.next()) {
			unit.selectRow(rs.getInt("UnitID"));
			unit.setArmourType(4);
			if (unit.hasEquipment() == false) {
				EquipmentTable.CreateEquipment(dbs, game.getID(), unit.getID(), blaster_id, 100);
			}
		}
		rs.close();
		
		unit.close();
	}
	
	
	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			if (side == 1) {
				return true;
			} else {
				return t == EquipmentTypesTable.ET_GUN || t == EquipmentTypesTable.ET_AMMO_CLIP;
			}
		}
		return true;
	}


	/*@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false;
		}
		return false;
	}*/


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill the droids";
		case 2:
			return "You must destroy the computers";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	

}
