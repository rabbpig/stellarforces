package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class Hideout extends AbstractMission {

	private static final int MAX_TURNS = 20;

	public Hideout() {
		super(AbstractMission.HIDEOUT, CP_STERNER_REGNIX, "Laser Squad", "Sterner's Gang", MAX_TURNS, 280, 180, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.63f, AbstractMission.WEAK_WALLS, AbstractMapModel.SLIM_WALLS, "hideout.txt", "", IS_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "theassassins_outhouses.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Assassins";
		case 2:
			return "Sterner's Gang";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "Sterner Regnix and his whole gang must be killed!";
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			if (side == 1) {
				return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_ROCKET_LAUNCHER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_AUTOCANNON) == false;
			} else {
				return true;
			}
		}
		return false;
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill all of Sterner's gang";
		case 2:
			return "You must hide for as long as possible";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


}
