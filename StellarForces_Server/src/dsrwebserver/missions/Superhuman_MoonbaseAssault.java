package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

/**
 * 7/10/13 - Defenders now have only 270 creds down from 320. Added extra superhuman.
 * 23/12/13 - Reduced side 2 creds to 250
 */
public class Superhuman_MoonbaseAssault extends AbstractMission {

	private static final int MAX_TURNS = 24;

	public Superhuman_MoonbaseAssault() {
		super(AbstractMission.SUPERHUMAN_MOONBASE_ASSAULT, CP_OMNI_CORP, "Superhuman", "Marsec Corp", MAX_TURNS, 180, 250, 2, AbstractMission.SHOW_CEILINGS, 5, 0, 1.62f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "superhuman_moonbase_assault.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "moonbaseassault.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Superhuman";
		case 2:
			return "The Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "One player must attack the moonbase and destroy the computers with their small squad of superhumans.  The other player must defend the moonbase until the turns run out.";
	}


	public boolean doWeCarewhoDestroyedComputers() {
		return false;
	}


	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must destroy the computers";
		case 2:
			return "You must prevent the computers being destroyed";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			if (side == 2) {
				return code.equalsIgnoreCase(EquipmentTypesTable.CD_INCENDIARY_GRENADE) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_FLAMETHROWER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_DEATHGRENADE) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_ROCKET_LAUNCHER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_AUTOCANNON) == false; // It's too easy otherwise
			} else {
				return true;
			}
		}
		return false;
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_GRENADE;
		}
		return true;
	}


}
