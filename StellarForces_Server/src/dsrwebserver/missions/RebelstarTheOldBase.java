package dsrwebserver.missions;

import java.sql.ResultSet;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.MapData;
import dsrwebserver.maps.MapLoader;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;

/**
 * 
 * 13/8/15 - Removed 2 aliens (16, was 18)
 */
public class RebelstarTheOldBase extends AbstractMission {

	private static final int MAX_TURNS = 30;

	public RebelstarTheOldBase() {
		super(AbstractMission.REBELSTAR_THEOLDBASE, CP_DEL_ME, "The Marines", "The Aliens", MAX_TURNS, 390, 0, 2, AbstractMission.SHOW_CEILINGS, 20, 0, 1.58f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "rebelstar_theoldbase.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, SPECIFIED_WIN_AT_END, "rebelstar_theoldbase.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Marines";
		case 2:
			return "The Aliens";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "The computers must be destroyed.  Alien lifeforms may be in the base.";
	}

	
	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must destroy the computers";
		case 2:
			return "You must kill the humans";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		MapData map = MapLoader.Import(dbs, gameid, this.map_filename, true);
		super.storeMapDataInSQL(dbs, map);

		// Remove 5 computers
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);
		try {
			ResultSet rs = dbs.getResultSet("SELECT * FROM MapDataSquares WHERE MapDataID = " + game.getMapDataID() + " AND SquareType = " + MapDataTable.MT_COMPUTER + " ORDER BY RAND() LIMIT 5");
			while (rs.next()) {
				dbs.runSQLUpdate("UPDATE MapDataSquares SET SquareType = " + MapDataTable.MT_WALL + ", DoorType = 1, FloorTex = 1 WHERE MapDataSquareID = " + rs.getInt("MapDataSquareID"));
			}
			rs.close();
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
		game.close();
	}

}
