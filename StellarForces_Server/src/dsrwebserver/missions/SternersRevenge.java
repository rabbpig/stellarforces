package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public class SternersRevenge extends AbstractMission {

	private static final int MAX_TURNS = 30;

	/**
	 * 30/5/13 - Reduced Side 1 creds from 240 to 200
	 *
	 */
	public SternersRevenge() {
		super(AbstractMission.STERNERS_REVENGE, -1, "The Army", "Sterner's Gang", MAX_TURNS, 200, 300, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.56f, AbstractMission.STRONG_WALLS, AbstractMapModel.BLOCK_WALLS, "sterners_revenge.txt", "", IS_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "sterners_revenge.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 1;
	}

	
	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Army";
		case 2:
			return "Sterner's Gang";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	public String getShortDesc() {
		return "Sterner Regnix must kill the soldiers!";
	}

	
	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill Sterner's gang or escape";
		case 2:
			return "You must prevent the soldiers from escaping";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		int vps = super.getVPs(dbs, game, side);
		try {
			if (side == 1) { // The army
				// See if all of Sterner's gang has been killed
				if (dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = 2 AND Status = " + UnitsTable.ST_DEPLOYED) == 0) {
					vps = 100;
				}
			} else if (side == 2) {
				// Do nothing
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
		return vps;
	}


}
