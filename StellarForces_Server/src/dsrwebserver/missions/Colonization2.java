package dsrwebserver.missions;

import java.io.IOException;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.maps.MapData;
import dsrwebserver.maps.SegmentMapGen;

public class Colonization2 extends ColonizationAbstract {

	private static final int MAX_UNITS = 16;

	private static final int CREDS = 160;

	public Colonization2() {
		super(AbstractMission.COLONIZATION2, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, -1, CREDS, CREDS, CREDS, CREDS, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.74f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "colonization2.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, false, SPECIFIED_WIN_AT_END, "", MAX_UNITS);
	}

	
	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws SQLException, IOException {
		MapData map = new SegmentMapGen(5, 5);

		super.storeMapDataInSQL(dbs, map);
	}



}
