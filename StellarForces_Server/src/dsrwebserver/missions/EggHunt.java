package dsrwebserver.missions;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.maps.MapData;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

/**
 * 15/7/13 - Reduced side 2 & 3 creds from 300 to 280
 * 10/6/14 - Added two more aliens
 * 
 */
public class EggHunt extends AbstractMission {

	private static final int MAX_TURNS = 25;

	private static final String SIDE1_NAME = "Aliens";
	private static final String SIDE2_NAME = "Laser Squad";
	private static final String SIDE3_NAME = "Colonial Marines";


	public EggHunt() {
		super(AbstractMission.EGG_HUNT, CP_DEL_ME, 3, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, "", MAX_TURNS, 0, 280, 280, -1, 1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.43f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "egg_hunt.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, SPECIFIED_WIN_AT_END, "egg_hunt.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "Two humans sides must battle each other to obtain an alien egg.  However, a third player controls the aliens and must prevent any eggs being taken.";
	}


	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		MapData map = super.createBasicMap(dbs, gameid);

		super.AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_EGG, 27, 28, -1, 1);
		super.AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_EGG, 34, 28, -1, 1);
		super.AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_EGG, 27, 33, -1, 1);
		super.AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_EGG, 34, 33, -1, 1);
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}


	public boolean hasSpecialVPCalc() {
		return true;
	}


	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		int vps = 0;
		String sql = "SELECT UnitID FROM Equipment WHERE GameID = " + game.getID() + " AND UnitID > 0 AND EquipmentTypeID = " + EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_EGG);
		//String sql = "SELECT UnitID, MapX, MapY FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_EGG);
		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) { // Loop through all the eggs in the game
			//int x = rs.getInt("MapX");
			//int y = rs.getInt("MapY");
			//if (rs.getInt("UnitID") > 0) {
			UnitsTable unit = new UnitsTable(dbs);
			unit.selectRow(rs.getInt("UnitID"));
			if (unit.getSide() == side) {
				int x = unit.getMapX();
				int y = unit.getMapY();
				if (x == 3 && y == 3) {
					vps = 100;
				} else if (x == 56 && y == 3) {
					vps = 100;
				} else  if (x == 3 && y == 57) {
					vps = 100;
				} else if (x == 56 && y == 57) {
					vps = 100;
				}
			}
			unit.close();
		}
		return vps;
	}


	/*public int getVPs_OLD(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		int vps = 0;
		String sql = "SELECT MapX, MapY, UnitID FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_EGG);
		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) { // Loop through all the eggs in the game
			int x = rs.getInt("MapX");
			int y = rs.getInt("MapY");
			if (rs.getInt("UnitID") > 0) {
				UnitsTable unit = new UnitsTable(dbs);
				unit.selectRow(rs.getInt("UnitID"));
				x = unit.getMapX();
				y = unit.getMapY();
			}
			if (side == 2) {
				if (x == 2 && y == 2) {
					vps = 100;
				} else if (x == 56 && y == 2) {
					vps = 100;
				}
			} else if (side == 3) {
				if (x == 2 && y == 57) {
					vps = 100;
				} else if (x == 57 && y == 57) {
					vps = 100;
				}
			}
		}
		return vps;
	}*/


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must defend the alien eggs";
		case 2:
		case 3:
			return "You must obtain an egg and escape";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


}
