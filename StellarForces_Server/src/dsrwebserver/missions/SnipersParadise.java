package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;

public class SnipersParadise extends AbstractMission {
	
	private static final int MAX_TURNS = 20;
	private static final int CREDS = 200;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Renegade Squad";

	public SnipersParadise() {
		super(AbstractMission.SNIPERS_PARADISE, CP_SIGMA_7, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, CREDS, CREDS, -1, true, 0, 0, 1.44f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "snipers_paradise.txt", "", IS_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "snipers_paradise.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public String getShortDesc() {
		return "Two teams battle it out to the death across a wide and barren landscape.";
	}
	

	@Override
	public String getMission1Liner(int side) {
		return "You must kill all your opponents units";
	}


/*	public int canBePlayedOnAndroid() {
		return ANY_CLIENT;
	}
*/

}

