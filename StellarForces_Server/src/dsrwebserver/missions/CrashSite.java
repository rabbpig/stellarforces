package dsrwebserver.missions;

import java.sql.ResultSet;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.MapData;
import dsrwebserver.maps.MapLoader;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public class CrashSite extends AbstractMission {

	private static final int MAX_TURNS = 30;

	public CrashSite() {
		super(AbstractMission.CRASH_SITE, -1, "Rescue Squad", "Marsec Corp", MAX_TURNS, 210, 200, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 0.54f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.BLOCK_WALLS, "crash_site.txt", "", IS_CAMPAIGN, AbstractMission.GR_LEVEL_2, HIGHEST_VPS_WIN_AT_END, "crash_site.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 1;
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Rescuers";
		case 2:
			return "The Assassins";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "A lone crash survivor must be rescued from the centre of the map by the rescue squad.  Unfortunately they are surrounded by advancing enemy forces.";
	}


	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		MapData map = MapLoader.Import(dbs, gameid, this.map_filename, true);

		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);

		// Create the Ambassador
		ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE Status = " + UnitsTable.ST_AWAITING_DEPLOYMENT + " AND GameID = " + gameid + " AND CanDeploy = 0 LIMIT 1");
		if (rs.next()) {
			try {
				// Give equipment
				int eq_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SP30);
				int ammo = EquipmentTypesTable.GetAmmoCapacityFromEquipmentType(dbs, eq_id); 
				if (dbs.getScalarAsInt("SELECT Count(*) FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + eq_id) == 0) {
					EquipmentTable.CreateEquipment(dbs, gameid, rs.getInt("UnitID"), eq_id, ammo);

					// Give armour and deploy survivor
					int armour_id = dbs.getScalarAsInt("SELECT ArmourTypeID FROM ArmourTypes WHERE Name = 'Heavy'");
					dbs.runSQLUpdate("UPDATE Units SET Status = " + UnitsTable.ST_DEPLOYED + ", ArmourTypeID = " + armour_id + ", MapX = 29, MapY = 23 WHERE UnitID = " + rs.getInt("UnitID"));

					// Add event
					int seen_by_side[] = {0, 0, 0, 0, 0};
					UnitsTable unit = new UnitsTable(dbs);
					unit.selectRow(rs.getInt("UnitID"));
					seen_by_side[unit.getSide()] = 1; // Only owner side knows about it
					UnitHistoryTable.AddRecord_UnitDeployed(dbs, game, unit, seen_by_side, System.currentTimeMillis());
				}
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}
		}

		super.storeMapDataInSQL(dbs, map);
	}


	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must get the survivor to the exit point";
		case 2:
			return "You must kill the survivor";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			if (side == 2) {
				return t != EquipmentTypesTable.ET_GRENADE;
			}
		}
		return true;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			if (side == 1) {
				return code.equalsIgnoreCase(EquipmentTypesTable.CD_SMOKE_GRENADE) == false;
			} else if (side == 2) {
				return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_ROCKET_LAUNCHER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_AUTOCANNON) == false;
			}
		}
		return false;
	}




}
