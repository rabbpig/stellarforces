package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class TheRaid extends AbstractMission {

	private static final int MAX_TURNS = 25;

	private static final String SIDE1_NAME = "Galactic Police";
	private static final String SIDE2_NAME = "The Zakyua Gang";

	/**
	 * 30/5/13 - Reduced creds for sides 2 from 150 to 130
	 * 17/6/13 - Inc side 1 creds from 175 to 195.
	 * 2/12/13 - Reduce side 2 creds to 110
	 */
	public TheRaid() {
		super(AbstractMission.THE_RAID, CP_HIDDEN, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 195, 110, 2, AbstractMission.NO_CEILINGS, 0, 0, 0.51f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "the_raid.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "the_raid.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "The Galactic Police must raid the hideout of a deadly gang and kill them.";
	}


	public String getMission1Liner(int side) {
		return "You must kill your opponent's units";
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_GRENADE &&  t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_ROCKET_LAUNCHER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_AUTOCANNON) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_NERVE_GAS) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_INCENDIARY_GRENADE) == false;
		}
		return false;
	}


	@Override
	public int getMaxProtection(int side) {
		if (side == 2) {
			return 30;
		} else {
			return -1;
		}
	}



}
