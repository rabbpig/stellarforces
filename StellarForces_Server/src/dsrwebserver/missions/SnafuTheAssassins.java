package dsrwebserver.missions;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public class SnafuTheAssassins extends AbstractMission {

	private static final int MAX_TURNS = 20;
	private static final int VPS_FOR_NON_ENEMY_UNIT = -40;
	private static final int SAB_VPS_FOR_TARGET_UNIT = 20;
	private static final int VPS_FOR_SAB_UNIT = 13;
	private static final int VPS_FOR_LAST_SAB_UNIT = 50;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Marsec Corp";
	private static final String SIDE3_NAME = "Globex Inc.";
	private static final String SIDE4_NAME = "Space Ruffians";

	public SnafuTheAssassins() {
		// Was SPECIFIED_WIN_AT_END, changed to HIGHEST_VPS_WIN_AT_END 22/3/13 since we don't want it to be a draw at the end if no side has 100 VPs.
		super(AbstractMission.SNAFU_THE_ASSASSINS, CP_STERNER_REGNIX, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, 240, 240, 240, 240, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.38f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "snafu_the_assassins.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_SNAFU, IS_SNAFU, HIGHEST_VPS_WIN_AT_END, "snafu_theassassins.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "One randomly chosen player controls the assassins, and must kill all the units on another randonly chosen side.  The other players must prevent this by killing the assassins.  However, only the assassins know that they are the assassins, and the other 3 players must try and work it out.";
	}



	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		super.createMap(dbs, gameid);

		// Create snafu data
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);

		int saboteur = Functions.rnd(1, 4);
		int target_side = saboteur;
		while (target_side == saboteur) {
			target_side = Functions.rnd(1, 4);
		}
		game.setSnafuData(saboteur, target_side); // Which side must the kill?
		game.close();
	}


	public String getSnafuMissionText(GamesTable game, int side) throws Exception {
		if (game.getSnafuData(side) > 0) {
			return "You are the assassins!  You must kill all the units on side " + game.getSnafuData(side) + "!";
		} else {
			return "You must kill the assassins!";
		}
	}


	public static int GetAssassinsSide(GamesTable game) throws SQLException {
		int assassin_side = -1;
		for (int i=1 ; i<=4 ; i++) {
			if (game.getSnafuData(i) > 0) {
				assassin_side = i;
				break;
			}
		}
		return assassin_side;
	}
	
	
	public static int GetTargetsRemaining(MySQLConnection dbs, GamesTable game) throws SQLException {
		String sql = "SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + game.getSnafuData(GetAssassinsSide(game)) + " AND Status = " + UnitsTable.ST_DEPLOYED;
		return dbs.getScalarAsInt(sql);
	}
	
	
	@Override
	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		int vps = 0;
		int assassin_side = GetAssassinsSide(game);
		boolean is_assassins = (game.getSnafuData(side) > 0);

		// Calc from units
		if (is_assassins == false) {
			// VPs for killing wrong side
			String sql_vps = "SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side <> " + assassin_side + " AND Side IN (" + game.getOppositeSidesForSideAsCSV(side) + ") AND Status = " + UnitsTable.ST_DEAD + " AND COALESCE(KilledBySide, 0) IN (0, " + side + ")";
			vps += dbs.getScalarAsInt(sql_vps) * VPS_FOR_NON_ENEMY_UNIT;

			// VPs for killing assassin units
			sql_vps = "SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + assassin_side + " AND Status = " + UnitsTable.ST_DEAD + " AND COALESCE(KilledBySide, 0) IN (0, " + assassin_side + ", " + side + ")";
			vps += dbs.getScalarAsInt(sql_vps) * VPS_FOR_SAB_UNIT;

			try {
				// If they killed the last unit, give extra VPs
				int sabs_remaining = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + assassin_side + " AND Status = " + UnitsTable.ST_DEPLOYED);
				if (sabs_remaining <= 0) {
					ResultSet rs = dbs.getResultSet("SELECT KilledBySide FROM Units WHERE GameID = " + game.getID() + " AND Side = " + assassin_side + " AND Status = " + UnitsTable.ST_DEAD + " AND COALESCE(KilledBySide, 0) > 0 ORDER BY DateKilled DESC");
					if (rs.next()) {
						int last_unit_killer = rs.getInt("KilledBySide");
						if (last_unit_killer == side) {
							vps += (VPS_FOR_LAST_SAB_UNIT-VPS_FOR_SAB_UNIT);
						}
					}
				}
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}

		} else { // Is assassins
			// VPs for killing target units
			String sql_vps = "SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + game.getSnafuData(assassin_side) + " AND Status = " + UnitsTable.ST_DEAD;// + " AND COALESCE(KilledBySide, 0) IN (0, " + side + ")";
			vps += dbs.getScalarAsInt(sql_vps) * SAB_VPS_FOR_TARGET_UNIT;
			if (vps < 100) {
				vps = 0; // Sabo must get 100 VPs!
			}
		}

		return vps;
	}

	
	@Override
	public boolean hasSpecialVPCalc() {
		// Override if req
		return true;
	}

	
	@Override
	public String getMission1Liner(int side) {
		return "An assassin must kill the enemy, whoever they are.";
	}

	
	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
		return t != EquipmentTypesTable.ET_GRENADE && t != EquipmentTypesTable.ET_SMOKE_GRENADE && t != EquipmentTypesTable.ET_NERVE_GAS && t != EquipmentTypesTable.ET_ADRENALIN_SHOT;
		}
		return true;
	}


	@Override
	public boolean doWeCareWhoKilledUnits() {
		return true;
	}

}
