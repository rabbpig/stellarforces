package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

/**
 * 10/6/14 - Reduced Defenders creds by 40 to 280, Added an extra attacker
 * 
 *
 */
public class DefendTheBase extends AbstractMission {

	private static final int MAX_TURNS = 20;

	public DefendTheBase() {
		super(AbstractMission.DEFEND_THE_BASE, CP_SHADOW_CONSPIRACY, "Laser Squad", "Marsec Corp", MAX_TURNS, 320, 280, 2, AbstractMission.SHOW_CEILINGS, 100, 0, 1.58f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "defend_the_base.txt", "defend_the_base_campaign.txt", IS_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "defendthebase.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Attackers";
		case 2:
			return "The Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "A small moonbase on a deserted moon is under attack.  One player must attack the base and destroy the computer.  The other player must defend the base until the end.";
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_GRENADE && t != EquipmentTypesTable.ET_NERVE_GAS;
		}
		return true;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_ROCKET_LAUNCHER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_AUTOCANNON) == false;
		}
		return false;
	}


	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must destroy the computer";
		case 2:
			return "You must prevent the computer being destroyed";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}


}
