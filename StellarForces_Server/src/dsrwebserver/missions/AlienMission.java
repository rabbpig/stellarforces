package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class AlienMission extends AbstractMission {

	private static final int MAX_TURNS = 25;

	public AlienMission() {
		super(AbstractMission.ALIEN, CP_DEL_ME, "Nostromo Crew", "Alien", MAX_TURNS, 45, 1, -1, AbstractMission.NO_CEILINGS, 99, 0, 1.56f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.BLOCK_WALLS, "alien.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_1, SPECIFIED_WIN_AT_END, "alien.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 1;
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Nostromo Crew";
		case 2:
			return "The Alien";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "The humans must destroy the reactor and escape while avoiding the Queen alien.  This mission is meant to be a recreation of the film.";
	}



	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE && t != EquipmentTypesTable.ET_GRENADE;
		}
		return true;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_ROCKET_LAUNCHER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_POWER_SWORD) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_AUTOCANNON) == false;
		}
		return false;
	}


	@Override
	public boolean isAlwaysAdvanced() {
		return true;
	}


	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must initiate self-destruct and escape";
		case 2:
			return "You must kill the humans";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}




}
