package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class TheAssassins_Outhouses extends AbstractMission {

	private static final int MAX_TURNS = 13;

	public TheAssassins_Outhouses() {
		super(AbstractMission.OUTHOUSES, -1, "Laser Squad", "Globex Megacorp", MAX_TURNS, 220, 280, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.63f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "the_assassins_outhouses.txt", "the_assassins_outhouses_campaign.txt", IS_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "theassassins_outhouses.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Assassins";
		case 2:
			return "The Fugitives";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "Sterner Regnix must be killed!  One player controls the assassins, and the other player controls Sterner and his bodyguards.  THis mission uses a map containing lots of small buildings.";
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			if (side == 1) {
				return code.equalsIgnoreCase(EquipmentTypesTable.CD_ROCKET_LAUNCHER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_AUTOCANNON) == false; // It's too easy otherwise
			} else {
				return true;
			}
		}
		return false;
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill Sterner Regnix";
		case 2:
			return "You must kill the assassins";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


}
