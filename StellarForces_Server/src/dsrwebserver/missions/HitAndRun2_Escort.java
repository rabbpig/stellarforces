package dsrwebserver.missions;

import java.io.IOException;

import dsr.models.map.AbstractMapModel;

public class HitAndRun2_Escort extends AbstractMission { 

	private static final int MAX_TURNS = 20;
	private static final String SIDE1_NAME = "Rebel Squad";
	private static final String SIDE2_NAME = "Metallix Corp";

	public HitAndRun2_Escort() {
		super(AbstractMission.HIT_AND_RUN2_ESCORT, CP_STERNER_REGNIX, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 250, 250, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0.45f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "hit_and_run_2_escort.txt", "hit_and_run_2_escort_campaign.txt", IS_CAMPAIGN, AbstractMission.GR_NONE, HIGHEST_VPS_WIN_AT_END, "hitandrun2_escort2.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return true;
	}

	
	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public String getShortDesc() {
		return "Be the side that inflicts the most harm.";
	}

	
	@Override
	public String getMission1Liner(int side) {
		return "You must inflict more harm than your opponent and then escape";
	}


	@Override
	public int getMinUnitsForSide(int side) throws IOException {
		return 1;
	}

	

}
