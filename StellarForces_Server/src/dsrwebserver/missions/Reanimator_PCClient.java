package dsrwebserver.missions;

public class Reanimator_PCClient extends Reanimator {
	
	public Reanimator_PCClient() {
		super();
		
		this.mission_id = AbstractMission.REANIMATOR_PCCLIENT;
		mission_name = GetMissionNameFromType(mission_id, false, false);

	}

}
