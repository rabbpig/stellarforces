package dsrwebserver.missions;

import ssmith.util.MyList;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class MoonbaseAssault_2v2 extends AbstractMission {
	
	private static final int MAX_TURNS = 30;

	private static final String SIDE1_NAME = "Attackers 1";
	private static final String SIDE2_NAME = "Defenders 1";
	private static final String SIDE3_NAME = "Attackers 2";
	private static final String SIDE4_NAME = "Defenders 2";

	public MoonbaseAssault_2v2() {
		// Notice that player 1 gets 3 VPs and player 2 gets 2 VPs for each computer.
		super(AbstractMission.MOONBASE_ASSAULT_2v2, CP_OMNI_CORP, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, 160, 160, 160, 160, 2, AbstractMission.SHOW_CEILINGS, 3, 0, 2, 0, 1.76f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "moonbase_assault.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_1, false, SPECIFIED_WIN_AT_END, "moonbaseassault.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	public String getShortDesc() {
		return "Two players must attack the moonbase and destroy the computers.  The other two players must defend the moonbase until the turns run out.";
	}

	
	@Override
	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}
	

	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
		case 3:
			return "You must destroy the computers";
		case 2:
		case 4:
			return "You must prevent the computers being destroyed";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public MyList<Integer> getSidesForSide(int side) {
		switch (side) {
		case 1:
		case 3:
			return MyList.CreateIntsFromInts(1,3);
		case 2:
		case 4:
			return MyList.CreateIntsFromInts(2,4);
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}
	

	@Override
	public boolean doWeCareWhoKilledUnits() {
		return false;
	}


	@Override
	public boolean addComradesVPs(int side) {
		return side == 1 || side == 3;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			if (side == 2 || side == 4) {
				return code.equalsIgnoreCase(EquipmentTypesTable.CD_INCENDIARY_GRENADE) == false; // It's too easy otherwise
			} else {
				return true;
			}
		}
		return false;
	}

}
