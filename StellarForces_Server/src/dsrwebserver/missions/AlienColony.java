package dsrwebserver.missions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.TextureStateCache;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.MapData;
import dsrwebserver.maps.SegmentMapGen;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GameLogTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public class AlienColony extends AbstractMission {

	protected static final int MAX_UNITS = 16;

	protected static final String SIDE1_NAME = "Laser Squad";
	protected static final String SIDE2_NAME = "Quaziargs";
	protected static final String SIDE3_NAME = "Globex Industries";
	protected static final String SIDE4_NAME = "Fl'hurgs";


	protected AlienColony() {
		super(AbstractMission.ALIEN_COLONY, -1, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, -1, 120, 0, 120, 0, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.74f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "alien_colony.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, false, SPECIFIED_WIN_AT_END, "unused");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public void createUnits(MySQLConnection dbs, GamesTable game) throws FileNotFoundException, SQLException, IOException {
		super.createUnits(dbs, game);

		// Make them all engineers
		try {
			dbs.runSQLUpdate("UPDATE Units SET UnitType = " + UnitsTable.UT_ENGINEER + " WHERE Side IN (1, 3) AND GameID = " + game.getID());
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

		for (int s=1 ; s<= 4 ; s++) {
			if (s == 1 || s == 3) {
				game.setResourcePointsForSide(s, 11+s);
			} else {
				game.setResourcePointsForSide(s, 7+s);
			}
		}
	}


	@Override
	public String getShortDesc() {
		return "Two human and two alien colonies battle it out for supremacy.  Each has their advantages; the humans can build structures and the aliens can lay eggs.  The last surviving side is the winner.";
	}


	@Override
	public String getMission1Liner(int side) {
		return "You must ensure your colony is the only one to survive.";
	}


	@Override
	public boolean doWeCareWhoKilledUnits() {
		return false;
	}


	@Override
	public void nextPhase(MySQLConnection dbs, GamesTable game) throws SQLException {
		try {
			// Delete all empty guns.
			int equiptypeid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SP30);
			dbs.runSQLUpdate("UPDATE Equipment SET Destroyed = 1 WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + equiptypeid + " AND MapX > 0 AND MapY > 0 AND COALESCE(Destroyed, 0) = 0 AND COALESCE(UnitID, 0) <= 0 AND Ammo = 0");

			/*if (game.getTurnNo() % 3 == 0) { // Only every 2 turns
				// Remove all clone corpses
				equiptypeid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SCIENTIST_CORPSE);
				dbs.runSQLUpdate("UPDATE Equipment SET Destroyed = 1 WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + equiptypeid);
			}*/
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

		try {
			int side = game.getTurnSide();

			// Check if queen killed, if so, create new queen
			if (side == 2 || side == 4) {
				try {
					if (dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + side + " AND ModelType = " + UnitsTable.MT_QUEEN_ALIEN + " AND Status = " + UnitsTable.ST_DEPLOYED) == 0) {
						ResultSet rs_aliens = dbs.getResultSet("SELECT * FROM Units WHERE GameID = " + game.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED + " ORDER BY Rand()");
						if (rs_aliens.next()) {
							UnitsTable unit = new UnitsTable(dbs);
							unit.selectRow(rs_aliens.getInt("UnitID"));
							unit.setModelType(UnitsTable.MT_QUEEN_ALIEN);
							unit.close();
						}
						rs_aliens.close();
					}
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
			}

			// check for installations and process accordingly
			ResultSet rs_ms = dbs.getResultSet("SELECT * FROM MapDataSquares WHERE MapDataID = " + game.getMapDataID() + " AND OwnerSide = " + side +" AND FloorTex IN (" + TextureStateCache.TEX_MEDI_BAY + ", " + TextureStateCache.TEX_GUN_VENDING_MACHINE + ", " + TextureStateCache.TEX_GRENADE_VENDING_MACHINE + ", " + TextureStateCache.TEX_CLONE_GENERATOR + ", " + TextureStateCache.TEX_ALIEN_COLONY + ")");
			while (rs_ms.next()) {
				int unitid_in_square = -1;
				try {
					ResultSet rs_unit = dbs.getResultSet("SELECT * FROM Units WHERE GameID = " + game.getID() + " AND MapX = " + rs_ms.getInt("MapX") + " AND MapY = " + rs_ms.getInt("MapY") + " AND Status = " + UnitsTable.ST_DEPLOYED);
					if (rs_unit.next()) {
						unitid_in_square = rs_unit.getInt("UnitID");
					}
					rs_unit.close();
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
				boolean vender_blocked = false;
				try {
					ResultSet rs_eq = dbs.getResultSet("SELECT EquipmentID FROM Equipment WHERE GameID = " + game.getID() + " AND MapX = " + rs_ms.getInt("MapX") + " AND MapY = " + rs_ms.getInt("MapY") + " AND COALESCE(Destroyed, 0) = 0 AND COALESCE(UnitID, 0) <= 0");
					if (rs_eq.next()) {
						vender_blocked = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SP30) == rs_eq.getInt("EquipmentID");
						vender_blocked = vender_blocked || EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_AP50) == rs_eq.getInt("EquipmentID");
					}
					rs_eq.close();
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
				if (rs_ms.getInt("FloorTex") == TextureStateCache.TEX_MEDI_BAY) {
					try {
						if (unitid_in_square > 0) { // Is there a unit there?
							UnitsTable unit = new UnitsTable(dbs);
							unit.selectRow(unitid_in_square);
							if (unit.getHealth() < unit.getMaxHealth()) {
								//if (game.checkAndReduceResourcePointsForSide(rs_ms.getInt("OwnerSide"), 1)) {
								unit.setHealth(unit.getMaxHealth());
								//GameLogTable.AddRec(dbs, game, game.getLoginIDFromSide(unit.getSide()), -1, unit.getName() + " has had their health restored.", false, System.currentTimeMillis());
								//}
							}
							unit.close();
						}
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex);
					}
				}
				if (game.getTurnNo() % 2 == 0) { // Only every 2 turns
					if (rs_ms.getInt("FloorTex") == TextureStateCache.TEX_GUN_VENDING_MACHINE && vender_blocked == false) {
						try {
							if (game.checkAndReduceResourcePointsForSide(rs_ms.getInt("OwnerSide"), 1)) {
								AbstractMission.AddEquipmentToMap(dbs, game.getID(), null, EquipmentTypesTable.CD_SP30, rs_ms.getInt("MapX"), rs_ms.getInt("MapY"), 7, rs_ms.getInt("OwnerSide"));
								//GameLogTable.AddRec(dbs, game, game.getLoginIDFromSide(rs_ms.getInt("OwnerSide")), -1, "A new weapon has been created.", false, System.currentTimeMillis());
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
					} else if (rs_ms.getInt("FloorTex") == TextureStateCache.TEX_GRENADE_VENDING_MACHINE && vender_blocked == false) {
						try {
							if (game.checkAndReduceResourcePointsForSide(rs_ms.getInt("OwnerSide"), 1)) {
								AbstractMission.AddEquipmentToMap(dbs, game.getID(), null, EquipmentTypesTable.CD_AP50, rs_ms.getInt("MapX"), rs_ms.getInt("MapY"), 0, rs_ms.getInt("OwnerSide"));
								//GameLogTable.AddRec(dbs, game, game.getLoginIDFromSide(rs_ms.getInt("OwnerSide")), -1, "A new grenade has been created.", false, System.currentTimeMillis());
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
					} else if (rs_ms.getInt("FloorTex") == TextureStateCache.TEX_CLONE_GENERATOR && unitid_in_square < 0) { // Do clones last, otherwise player could end up with loads of clones and no guns
						try {
							// Check they don't have too many units
							int c = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + rs_ms.getInt("OwnerSide") + " And Status = " + UnitsTable.ST_DEPLOYED);
							if (c < MAX_UNITS) {
								//if (unitid_in_square < 0) { // Check the square is empty of another clone
								if (game.checkAndReduceResourcePointsForSide(rs_ms.getInt("OwnerSide"), 2)) {
									int order = dbs.getScalarAsInt("SELECT MAX(OrderBy) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + rs_ms.getInt("OwnerSide")) + 1;
									UnitsTable unit = new UnitsTable(dbs);
									unit.createUnit(game.getID(), order, UnitsTable.MT_SCIENTIST, "Clone " + order, rs_ms.getInt("OwnerSide"), 64, 36, 0, 0, 250, 35, 50, 10, 0, 40, 0, 0);
									unit.setStatus(UnitsTable.ST_DEPLOYED);
									unit.setMapXYAngle(rs_ms.getInt("MapX"), rs_ms.getInt("MapY"), 90);
									unit.setCurrentAPs(unit.getMaxAPs());
									try {
										// Add event
										int seen_by_side[] = {0, 0, 0, 0, 0};
										seen_by_side[unit.getSide()] = 1; // Only owner side knows about it
										UnitHistoryTable.AddRecord_UnitDeployed(dbs, game, unit, seen_by_side, System.currentTimeMillis());
									} catch (Exception ex) {
										DSRWebServer.HandleError(ex);
									}

									GameLogTable.AddRec(dbs, game, game.getLoginIDFromSide(rs_ms.getInt("OwnerSide")), -1, "A clone has been created.", false, System.currentTimeMillis());
								}
								//}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
					} else if (rs_ms.getInt("FloorTex") == TextureStateCache.TEX_ALIEN_COLONY) { // Do clones last, otherwise player could end up with loads of clones and no guns
						try {
							// Check they don't have too many units
							int c = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + rs_ms.getInt("OwnerSide") + " And Status = " + UnitsTable.ST_DEPLOYED);
							if (c < MAX_UNITS) {
								//if (unitid_in_square < 0) { // Check the square is empty of another clone
								//if (game.checkAndReduceResourcePointsForSide(rs_ms.getInt("OwnerSide"), 2)) {
								int order = dbs.getScalarAsInt("SELECT MAX(OrderBy) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + rs_ms.getInt("OwnerSide")) + 1;
								UnitsTable unit = new UnitsTable(dbs);
								unit.createUnit(game.getID(), order, UnitsTable.MT_ALIEN_TYRANT, "Alien " + order, rs_ms.getInt("OwnerSide"), 58, 45, 0, 0, 450, 0, 100, 60, 0, 999, 0, 0);
								unit.setStatus(UnitsTable.ST_DEPLOYED);
								unit.setMapXYAngle(rs_ms.getInt("MapX"), rs_ms.getInt("MapY"), 90);
								unit.setCurrentAPs(unit.getMaxAPs());
								try {
									// Add event
									int seen_by_side[] = {0, 0, 0, 0, 0};
									seen_by_side[unit.getSide()] = 1; // Only owner side knows about it
									UnitHistoryTable.AddRecord_UnitDeployed(dbs, game, unit, seen_by_side, System.currentTimeMillis());
								} catch (Exception ex) {
									DSRWebServer.HandleError(ex);
								}
								// Remove the egg
								dbs.runSQLUpdate("UPDATE MapDataSquares SET FloorTex = " + TextureStateCache.TEX_ALIEN_PURPLE + " WHERE MapDataSquareID = " + rs_ms.getInt("MapDataSquareID"));

								GameLogTable.AddRec(dbs, game, game.getLoginIDFromSide(rs_ms.getInt("OwnerSide")), -1, "An alien has been hatched.", false, System.currentTimeMillis());
								//}
								//}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
					}
				}
			}
			rs_ms.close();
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	@Override
	public int getMinAndroidVersion() {
		return 99;
	}


	public int canBuildAndDismantle() {
		return 1;
	}


	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws SQLException, IOException {
		MapData map = new SegmentMapGen(4, 4);

		super.storeMapDataInSQL(dbs, map);
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}


}
