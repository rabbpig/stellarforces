package dsrwebserver.missions;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.maps.MapData;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitsTable;

public class AlienPrison extends AbstractMission {

	private static final int MAX_TURNS = 30;
	private static final int CREDS = 320;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Renegade Squad";
	private static final String SIDE3_NAME = "Aliens";

	public AlienPrison() {
		super(AbstractMission.ALIEN_PRISON, -1, 3, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, "", MAX_TURNS, CREDS, CREDS, 0, 0, 3, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.26f, AbstractMission.WEAK_WALLS, AbstractMapModel.SLIM_WALLS, "alien_prison.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, SPECIFIED_WIN_AT_END, "alienprison.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "Two humans sides must battle it out to capture the FLAG; however, it is located in the middle of a sealed prison which is infested with aliens.  Once the prison walls have been breached, all hell will break loose.";
	}


	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		MapData map = super.createBasicMap(dbs, gameid);

		AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_FLAG, -1, -1, -1, 3);
	}


	public boolean doesMissionInvolveFlag() {
		return true;
	}


	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
		case 2:
			return "You must get the flag to the <b>opposite</b> side of the map";
		case 3:
			return "You must kill the humans";
		}
		return "";
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
		return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_ROCKET_LAUNCHER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_AUTOCANNON) == false;
		}
		return false;
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}


	@Override
	public boolean aliensImpregnate() {
		return true;

	}


	@Override
	public boolean hasSpecialVPCalc() {
		return true;
	}


	@Override
	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		int vps = 0;

		// Calc from units killed
		//String sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + game.getID() + " AND Side IN (" + game.getOpponentSidesForSideAsCSV(side, this) + ") AND Status = " + UnitsTable.ST_DEAD;
		//if (this.getNumOfSides() > 2) { // If more than two, check who the killer was - NO, SOMETIMES WE DON'T CARE, E.G. ASSASSINS 2V2
		/*if (this.getNumOfSides() > 2 && mission.doWeCareWhoKilledUnits()) { // True for LMS, false for most 2v2 missions
			sql_vps = sql_vps + " AND COALESCE(KilledBySide, 0) IN (0, " + side + ")";
		}*/
		//vps = dbs.getScalarAsInt(sql_vps);
		// Add units escaped
		//vps += dbs.getScalarAsInt("SELECT SUM(VPsIfEscape) FROM Units WHERE GameID = " + this.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_ESCAPED);

		// Check if flag is at end for capture the flag
		if (side == 1 || side == 2) {
			String sql = "SELECT MapX, MapY, UnitID FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_FLAG);
			ResultSet rs = dbs.getResultSet(sql);
			while (rs.next()) { // Loop through all the flags in the game
				int x = rs.getInt("MapX");
				int y = rs.getInt("MapY");
				if (rs.getInt("UnitID") > 0) {
					UnitsTable unit = new UnitsTable(dbs);
					unit.selectRow(rs.getInt("UnitID"));
					x = unit.getMapX();
					y = unit.getMapY();
					unit.close();
				}
				if (side == 1 && x >= MapDataTable.GetMapSize(dbs, game.getMapDataID()).width-1) {
					vps = 100;
				} else if (side == 2 && x == 0) {
					vps = 100;
				}
			}
			rs.close();
		}
		return vps;
	}




}
