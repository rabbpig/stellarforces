package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.maps.LastManStandingMapGen;
import dsrwebserver.maps.MapData;

public final class LastManStandingMission extends AbstractMission {
	
	private static final int MAX_TURNS = 20;
	private static final int SIZE = 30;
	private static final int CREDS = 170;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Renegade Squad";

	public LastManStandingMission() {
		super(AbstractMission.LAST_MAN_STANDING, CP_AZARIAN_CRUSADE, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, CREDS, CREDS, -1, AbstractMission.NO_CEILINGS, 0, 0, 0, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.BLOCK_WALLS, "last_man_standing.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public String getShortDesc() {
		return "Two teams battle it out on a remote space station.";
	}

	
	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws SQLException {
		MapData map = new LastManStandingMapGen(SIZE, 6, 2, false);
		super.storeMapDataInSQL(dbs, map);
	}
	

	@Override
	public String getMission1Liner(int side) {
		return "You must kill all your opponents units";
	}


	@Override
	public boolean doWeCareWhoKilledUnits() {
		return true;
	}

}
