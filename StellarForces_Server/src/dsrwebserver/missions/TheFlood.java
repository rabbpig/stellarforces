package dsrwebserver.missions;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.TextureStateCache;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.GameLogTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public class TheFlood extends AbstractMission {

	private static final int MAX_TURNS = 20;
	private static final int CREDS = 220;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "The Omni Corp";
	private static final String SIDE3_NAME = "Globex Industries";
	private static final String SIDE4_NAME = "Banana Republic";


	public TheFlood() {
		super(AbstractMission.THE_FLOOD, -1, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, CREDS, CREDS, CREDS, CREDS, -1, false, 0, 0, 0, 0, 1.09f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "the_flood.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, SPECIFIED_WIN_AT_END, "the_flood.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "Four teams battle it out while a flood slowly engulfs the map.";
	}


	@Override
	public String getMission1Liner(int side) {
		return "You must kill more enemies than any other while avoiding the flood.";
	}


	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		try {
			// Check we have some units left
			String sql = "SELECT COUNT(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED;
			if (dbs.getScalarAsInt(sql) <= 0) {
				return 0;
			}
			// Calc from units killed
			String sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + game.getID() + " AND Side <> " + side + " AND Status = " + UnitsTable.ST_DEAD; // NOT IN (" + game.getOpponentSidesForSideAsCSV(side, mission) + ")
			sql_vps = sql_vps + " AND COALESCE(KilledBySide, 0) IN (0, " + side + ")";
			int vps = dbs.getScalarAsInt(sql_vps);

			return vps;
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
			return 0;
		}
	}


	@Override
	public boolean hasSpecialVPCalc() {
		return true;
	}


	@Override
	public boolean doWeCareWhoKilledUnits() {
		return true;
	}


	public void nextPhase(MySQLConnection dbs, GamesTable game) throws SQLException {
		UnitsTable unit = new UnitsTable(dbs);
		// Extend the flood!
		int min = 29-(game.getTurnNo()*2);
		if (min < 2) {
			min = 2;
		}
		int max = 29+(game.getTurnNo()*2);
		if (max > 55) {
			max = 55;
		}
		for (int y=min ; y<=max ; y++) {
			for (int x=min ; x<=max ; x++) {
				try {
					ResultSet rs = dbs.getResultSet("SELECT * FROM MapDataSquares WHERE MapDataID = " + game.getMapDataID() + " AND MapX = " + x + " AND MapY = " + y + " AND FloorTex <> " + TextureStateCache.TEX_WATER);// > 0) { // AND SquareType = " + MapDataTable.MT_FLOOR + " 
					if (rs.next()) {
						// Check for dead units
						ResultSet rs_unit = dbs.getResultSet("SELECT UnitID FROM Units WHERE GameID = " + game.getID() + " AND MapX = " + x + " AND MapY = " + y + " AND Status = " + UnitsTable.ST_DEPLOYED);
						while (rs_unit.next()) {
							unit.selectRow(rs_unit.getInt("UnitID"));
							unit.setStatus(UnitsTable.ST_DEAD);
							try {
								LoginsTable owner = new LoginsTable(dbs);
								owner.selectRow(game.getLoginIDFromSide(unit.getSide()));
								GameLogTable.AddRec(dbs, game, -1, -1, unit.getName() + " (" + owner.getDisplayName() + "'s #" + unit.getOrderBy() + ") has drowned!", true, System.currentTimeMillis());
								owner.close();
								try {
									// Add event
									int seen_by_side[] = {0, 1, 1, 1, 1};
									//seen_by_side[unit.getSide()] = 1; // Only owner side knows about it
									UnitHistoryTable.AddRecord_UnitKilled(dbs, game, unit, seen_by_side, System.currentTimeMillis());
								} catch (Exception ex) {
									DSRWebServer.HandleError(ex);
								}
							} catch (Exception ex) {
								DSRWebServer.HandleError(ex);
							}
						}
						if (rs.getInt("SquareType") != MapDataTable.MT_FLOOR) { // Only destroy non-floor squares randomly
							if (Functions.rnd(1, 8) > 1) {
								continue;
							}
						}

						dbs.runSQLUpdate("UPDATE MapDataSquares SET SquareType = " + MapDataTable.MT_FLOOR + ", FloorTex = " + TextureStateCache.TEX_WATER + " WHERE MapDataID = " + game.getMapDataID() + " AND MapX = " + x + " AND MapY = " + y);
						try {
							// Remove scenery
							dbs.runSQLDelete("DELETE FROM SpecialMapDataSquares WHERE MapDataID = " + game.getMapDataID() + " AND MapX = " + x + " AND MapY = " + y);
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
					}
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
			}
		}
	}


}
