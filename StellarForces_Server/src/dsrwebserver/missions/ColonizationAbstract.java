package dsrwebserver.missions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.TextureStateCache;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GameLogTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public abstract class ColonizationAbstract extends AbstractMission {


	protected static final String SIDE1_NAME = "Laser Squad";
	protected static final String SIDE2_NAME = "The Omni Corp";
	protected static final String SIDE3_NAME = "Globex Industries";
	protected static final String SIDE4_NAME = "Banana Republic";

	private int max_units; 

	protected ColonizationAbstract(int _type, String s1name, String s2name, String s3name, String s4name, int _max_turns, int s1creds, int s2creds, int s3creds, int s4creds, int _winner_at_end, boolean _ceilings, int cpu_vps1, int cpu_vps2, int cpu_vps3, int cpu_vps4, float _min_client_version, int _destroyable_walls, int _map_type, String _overview_filename, boolean _is_campaign, int _games_req_to_play, boolean _is_snafu, boolean _highest_vps_win_at_end, String _map_filename, int _max_units) {
		super(_type, -1, 4, s1name, s2name, s3name, s4name, _max_turns, s1creds, s2creds, s3creds, s4creds, _winner_at_end, _ceilings, cpu_vps1, cpu_vps2, cpu_vps3, cpu_vps4, _min_client_version, _destroyable_walls, _map_type, _overview_filename, _is_campaign, _games_req_to_play, _is_snafu, _highest_vps_win_at_end, _map_filename);

		max_units = _max_units;
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return sidename[1];
		case 2:
			return sidename[2];
		case 3:
			return sidename[3];
		case 4:
			return sidename[4];
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public void createUnits(MySQLConnection dbs, GamesTable game) throws FileNotFoundException, SQLException, IOException {
		super.createUnits(dbs, game);

		// Make them all engineers
		try {
			dbs.runSQLUpdate("UPDATE Units SET UnitType = " + UnitsTable.UT_ENGINEER + " WHERE GameID = " + game.getID());
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

		for (int s=1 ; s<= 4 ; s++) {
			game.setResourcePointsForSide(s, 11+s);
		}
	}


	@Override
	public String getShortDesc() {
		return "Four colonies battle it out in a war of attrition.  They can build structures to create new clones and weapons, but must also defend those structures.  The last colony to survive is the winner.";
	}


	@Override
	public String getMission1Liner(int side) {
		return "You must ensure your colony is the only one to survive.";
	}


	@Override
	public boolean doWeCareWhoKilledUnits() {
		return false;
	}


	@Override
	public void nextPhase(MySQLConnection dbs, GamesTable game) throws SQLException {
		try {
			// Delete all empty guns.
			int equiptypeid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SP30);
			dbs.runSQLUpdate("UPDATE Equipment SET Destroyed = 1 WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + equiptypeid + " AND MapX > 0 AND MapY > 0 AND COALESCE(Destroyed, 0) = 0 AND COALESCE(UnitID, 0) <= 0 AND Ammo = 0");
			equiptypeid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SNIPERRIFLE);
			dbs.runSQLUpdate("UPDATE Equipment SET Destroyed = 1 WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + equiptypeid + " AND MapX > 0 AND MapY > 0 AND COALESCE(Destroyed, 0) = 0 AND COALESCE(UnitID, 0) <= 0 AND Ammo = 0");

			/*if (game.getTurnNo() % 3 == 0) { // Only every 2 turns
				// Remove all clone corpses
				equiptypeid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SCIENTIST_CORPSE);
				dbs.runSQLUpdate("UPDATE Equipment SET Destroyed = 1 WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + equiptypeid);
			}*/
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

		try {
			int side = game.getTurnSide();
			boolean has_excess_rps = game.getResPointsForSide(side) > 40;
			// check for installations and process accordingly
			int new_clones = 0;
			ResultSet rs_ms = dbs.getResultSet("SELECT * FROM MapDataSquares WHERE MapDataID = " + game.getMapDataID() + " AND OwnerSide = " + side +" AND FloorTex IN (" + TextureStateCache.TEX_MEDI_BAY + ", " + TextureStateCache.TEX_GUN_VENDING_MACHINE + ", " + TextureStateCache.TEX_GRENADE_VENDING_MACHINE + ", " + TextureStateCache.TEX_CLONE_GENERATOR + ")");
			while (rs_ms.next()) {
				int unitid_in_square = -1;
				try {
					ResultSet rs_unit = dbs.getResultSet("SELECT * FROM Units WHERE GameID = " + game.getID() + " AND MapX = " + rs_ms.getInt("MapX") + " AND MapY = " + rs_ms.getInt("MapY") + " AND Status = " + UnitsTable.ST_DEPLOYED);
					if (rs_unit.next()) {
						unitid_in_square = rs_unit.getInt("UnitID");
					}
					rs_unit.close();
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
				boolean vender_blocked = false;
				try {
					ResultSet rs_eq = dbs.getResultSet("SELECT EquipmentTypeID FROM Equipment WHERE GameID = " + game.getID() + " AND MapX = " + rs_ms.getInt("MapX") + " AND MapY = " + rs_ms.getInt("MapY") + " AND COALESCE(Destroyed, 0) = 0 AND COALESCE(UnitID, 0) <= 0");
					while (rs_eq.next()) { // There might be more than one item so don't stop after checking the first!
						// Only block if has an AP50 or SP30 in the way, to stop them being churned out.
						vender_blocked = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SP30) == rs_eq.getInt("EquipmentTypeID") 
								|| EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SNIPERRIFLE) == rs_eq.getInt("EquipmentTypeID") 	
								|| EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_AP50) == rs_eq.getInt("EquipmentTypeID")
								|| EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_AP100) == rs_eq.getInt("EquipmentTypeID");
						if (vender_blocked) {
							break;
						}
					}
					rs_eq.close();
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
				if (rs_ms.getInt("FloorTex") == TextureStateCache.TEX_MEDI_BAY) {
					try {
						if (unitid_in_square > 0) { // Is there a unit there?
							UnitsTable unit = new UnitsTable(dbs);
							unit.selectRow(unitid_in_square);
							if (unit.getHealth() < unit.getMaxHealth()) {
								//if (game.checkAndReduceResourcePointsForSide(rs_ms.getInt("OwnerSide"), 1)) {
								unit.setHealth(unit.getMaxHealth());
								//GameLogTable.AddRec(dbs, game, game.getLoginIDFromSide(unit.getSide()), -1, unit.getName() + " has had their health restored.", false, System.currentTimeMillis());
								//}
							}
							unit.close();
						}
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex);
					}
				}
				if (game.getTurnNo() % 2 == 0) { // Only every 2 turns
					if (rs_ms.getInt("FloorTex") == TextureStateCache.TEX_GUN_VENDING_MACHINE && vender_blocked == false) {
						try {
							if (has_excess_rps) {
								if (game.checkAndReduceResourcePointsForSide(side, 3)) {
									AbstractMission.AddEquipmentToMap(dbs, game.getID(), null, EquipmentTypesTable.CD_SNIPERRIFLE, rs_ms.getInt("MapX"), rs_ms.getInt("MapY"), 7, rs_ms.getInt("OwnerSide"));
								}
							} else {
								if (game.checkAndReduceResourcePointsForSide(side, 1)) {
									AbstractMission.AddEquipmentToMap(dbs, game.getID(), null, EquipmentTypesTable.CD_SP30, rs_ms.getInt("MapX"), rs_ms.getInt("MapY"), 7, rs_ms.getInt("OwnerSide"));
								}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
					} else if (rs_ms.getInt("FloorTex") == TextureStateCache.TEX_GRENADE_VENDING_MACHINE && vender_blocked == false) {
						try {
							if (has_excess_rps) {
								if (game.checkAndReduceResourcePointsForSide(side, 3)) {
									AbstractMission.AddEquipmentToMap(dbs, game.getID(), null, EquipmentTypesTable.CD_AP100, rs_ms.getInt("MapX"), rs_ms.getInt("MapY"), 0, rs_ms.getInt("OwnerSide"));
								}
							} else {
								if (game.checkAndReduceResourcePointsForSide(side, 1)) {
									AbstractMission.AddEquipmentToMap(dbs, game.getID(), null, EquipmentTypesTable.CD_AP50, rs_ms.getInt("MapX"), rs_ms.getInt("MapY"), 0, rs_ms.getInt("OwnerSide"));
								}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
					} else if (rs_ms.getInt("FloorTex") == TextureStateCache.TEX_CLONE_GENERATOR && unitid_in_square < 0) { // Do clones last, otherwise player could end up with loads of clones and no guns
						try {
							// Check they don't have too many units
							//int c = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + rs_ms.getInt("OwnerSide") + " And Status = " + UnitsTable.ST_DEPLOYED);
							int c = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side IN (" + this.getSidesForSide(rs_ms.getInt("OwnerSide")).toCSVString() + ") And Status = " + UnitsTable.ST_DEPLOYED);
							if (c < max_units) {
								if (unitid_in_square < 0) { // Check the square is empty of another clone
									if (game.checkAndReduceResourcePointsForSide(side, 2)) {
										int order = dbs.getScalarAsInt("SELECT MAX(OrderBy) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + rs_ms.getInt("OwnerSide")) + 1;
										UnitsTable unit = new UnitsTable(dbs);
										unit.createUnit(game.getID(), order, UnitsTable.MT_SCIENTIST, "Clone " + order, side, 64, 36, 0, 0, 250, 35, 50, 10, 0, 40, 0, 0);
										unit.setStatus(UnitsTable.ST_DEPLOYED);
										unit.setMapXYAngle(rs_ms.getInt("MapX"), rs_ms.getInt("MapY"), 90);
										unit.setCurrentAPs(unit.getMaxAPs());
										try {
											// Add event
											int seen_by_side[] = {0, 0, 0, 0, 0};
											seen_by_side[unit.getSide()] = 1; // Only owner side knows about it
											UnitHistoryTable.AddRecord_UnitDeployed(dbs, game, unit, seen_by_side, System.currentTimeMillis());
										} catch (Exception ex) {
											DSRWebServer.HandleError(ex);
										}
										new_clones++;
									}
								}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
					}
				}
			}
			rs_ms.close();
			if (new_clones > 0) {
				GameLogTable.AddRec(dbs, game, game.getLoginIDFromSide(side), -1, new_clones + " new clone(s) has been created for you", false, System.currentTimeMillis());
			}

		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	@Override
	public int getMinAndroidVersion() {
		return 86;
	}


	public int canBuildAndDismantle() {
		return 1;
	}



}
