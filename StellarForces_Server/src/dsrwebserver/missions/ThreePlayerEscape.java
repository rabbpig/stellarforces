package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public class ThreePlayerEscape extends AbstractMission {

	private static final int MAX_TURNS = 30;
	private static final int CREDS = 170;

	private static final String SIDE1_NAME = "Metallix Corp.";
	private static final String SIDE2_NAME = "Rebel Squad";
	private static final String SIDE3_NAME = "Laser Squad";

	public ThreePlayerEscape() {
		super(AbstractMission.THREE_PLAYER_ESCAPE, CP_AZARIAN_CRUSADE, 3, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, "", MAX_TURNS, CREDS, CREDS, CREDS, -1, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.06f, AbstractMission.WEAK_WALLS, AbstractMapModel.BLOCK_WALLS, "3playerescape.txt", IS_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, HIGHEST_VPS_WIN_AT_END, "3playerescape.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return true;
	}

	
	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "Three teams race to be the first to escape.";
	}

	
	@Override
	public String getMission1Liner(int side) {
		return "You must be the first to escape";
	}

	
	@Override
	public boolean hasSpecialVPCalc() {
		return true;
	}


	@Override
	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		// Add units escaped
		int vps = dbs.getScalarAsInt("SELECT SUM(VPsIfEscape) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_ESCAPED);
		
		// Check they have enough units left to actually win
		int left = dbs.getScalarAsInt("SELECT SUM(VPsIfEscape) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED);

		if (vps + left < 100) {
			vps = 0; // They can't get 100!
		}
		return vps;
	}

}
