package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;

public class BreakoutMission extends AbstractMission {

	private static final int MAX_TURNS = 30;

	public BreakoutMission() {
		super(AbstractMission.BREAKOUT, CP_REBELLION, "Rebel Squad", "Metallix Corp", MAX_TURNS, 350, 350, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 0.45f, AbstractMission.WEAK_WALLS, AbstractMapModel.BLOCK_WALLS, "breakout.txt", "", IS_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "breakout.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 1;
	}

	
	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Prisoners";
		case 2:
			return "The Prison Guards";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public String getShortDesc() {
		return "A group of prisoners must escape from the complex.  There are 4 escape points in the four corners of the map which need covering by the guards.";
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must escape from the prison";
		case 2:
			return "You must kill the prisoners";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


}
