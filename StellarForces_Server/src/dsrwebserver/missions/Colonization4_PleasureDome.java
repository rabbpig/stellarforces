package dsrwebserver.missions;

import java.awt.Dimension;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;

public class Colonization4_PleasureDome extends ColonizationAbstract {

	private static final int MAX_UNITS = 16;
	private static final int CREDS = 160;
	private static final int DEPLOY_SIZE = 10;

	public Colonization4_PleasureDome() {
		super(AbstractMission.COLONIZATION4_PLEASUREDOME, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, -1, CREDS, CREDS, CREDS, CREDS, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.74f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "colonization.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, false, SPECIFIED_WIN_AT_END, "theexterminators.csv", MAX_UNITS);
	}



	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		super.createMap(dbs, gameid);

		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);
		Dimension d = MapDataTable.GetMapSize(dbs, game.getMapDataID());

		// Fix deploy squares
		try {
			for (int y=0 ; y<d.height ; y++) {
				for (int x=0 ; x<d.width ; x++) {
					int deploy = 0;
					if (x<MAX_UNITS && y<MAX_UNITS) {
						deploy = 1;
					} else if (x>d.width-MAX_UNITS && y<MAX_UNITS) {
						deploy = 2;
					} else if (x<MAX_UNITS && y>d.height-MAX_UNITS) {
						deploy = 3;
					} else if (x>d.width-MAX_UNITS && y>d.height-MAX_UNITS) {
						deploy = 4;
					}
					dbs.runSQLUpdate("UPDATE MapDataSquares SET DeploymentSquareSide= " + deploy + " WHERE MapDataID = " + game.getMapDataID() + " AND MapX = " + x + " AND MapY = " + y);
				}
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
		game.close();

	}

}
