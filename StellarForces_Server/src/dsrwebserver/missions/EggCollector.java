package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.maps.MapData;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;

public class EggCollector extends AbstractMission {

	private static final int MAX_TURNS = 30;
	private static final int VPS_PER_EGG = 5;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Colonial Marines";
	private static final String SIDE3_NAME = "Marsec Corp";
	private static final String SIDE4_NAME = "Rebel Squad";

	public EggCollector() {
		super(AbstractMission.EGG_COLLECTOR, CP_DEL_ME, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, 300, 300, 300, 300, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.4f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "egg_collector.txt", IS_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, HIGHEST_VPS_WIN_AT_END, "egg_collector.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "A nest of alien eggs has been found in the middle of the moonbase.  Each side must collect the most alien eggs and take them back to their starting point for examination.  Whoever has the most eggs at the end is the winner.";
	}


	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		MapData map = super.createBasicMap(dbs, gameid);

		// Add eggs 
		for (int y=27 ; y<= 34 ; y++) {
			for (int x=26 ; x<= 35 ; x++) {
				AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_EGG, x, y, -1, -1);
			}
		}
	}


	public boolean hasSpecialVPCalc() {
		return true;
	}


	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		/*int vps = 0;
		int mapid = dbs.getScalarAsInt("SELECT MapDataID FROM Games WHERE GameID = " + game.getID());
		ResultSet rs = dbs.getResultSet("SELECT * FROM MapDataSquares WHERE MapDataID = " + mapid + " AND OwnerSide = " + side);
		while (rs.next()) {
			String sql = "SELECT EquipmentID FROM Equipment ";
			sql = sql + " INNER JOIN EquipmentTypes ON Equipment.EquipmentTypeID = EquipmentTypes.EquipmentTypeID ";
			sql = sql + " WHERE GameID = " + game.getID() + " AND UnitID <= 0 AND MapX = " + rs.getInt("MapX") + " AND MapY = " + rs.getInt("MapY") + " AND MajorTypeID = " + EquipmentTypesTable.ET_EGG + " LIMIT 1";
			ResultSet rs_equip = dbs.getResultSet(sql);
			if (rs_equip.next()) {
				vps += VPS_PER_EGG;
			}
		}
		return vps;*/
		int mapid = dbs.getScalarAsInt("SELECT MapDataID FROM Games WHERE GameID = " + game.getID());
		String sql = "SELECT Count(*) FROM Equipment";  
		sql = sql + " INNER JOIN EquipmentTypes ON Equipment.EquipmentTypeID = EquipmentTypes.EquipmentTypeID";  
		sql = sql + " INNER JOIN MapDataSquares ON MapDataSquares.MapX = Equipment.MapX AND MapDataSquares.MapY = Equipment.MapY";
		sql = sql + " WHERE GameID = " + game.getID() ;
		sql = sql + " and MapDataSquares.MapDataID = " + mapid;
		sql = sql + " and MapDataSquares.OwnerSide = " + side;
		sql = sql + " aND MajorTypeID = " + EquipmentTypesTable.ET_EGG;
		return dbs.getScalarAsInt(sql) * VPS_PER_EGG;
	}


	@Override
	public String getMission1Liner(int side) {
		return "You must collect as many eggs as possible";
	}

}

