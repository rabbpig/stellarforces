package dsrwebserver.missions;

import java.io.IOException;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.util.MyList;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.maps.MapData;
import dsrwebserver.maps.SegmentMapGen;
import dsrwebserver.tables.GamesTable;

public class UniverseMission extends AbstractMission {

	private static final int MAX_TURNS = 20;
	private static final int CREDS = 220;

	private static final String SIDE1_NAME = "Side 1";
	private static final String SIDE2_NAME = "Side 2";
	private static final String SIDE3_NAME = "Side 3";
	private static final String SIDE4_NAME = "Side 4";

	
	public UniverseMission() {
		super(AbstractMission.UNIVERSE_MISSION, -1, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, CREDS, CREDS, CREDS, CREDS, -1, false, 0, 0, 0, 0, 1.09f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "", IS_NOT_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, SPECIFIED_WIN_AT_END, "");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "The default universe mission.";
	}

	
	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws SQLException, IOException {
		MapData map = new SegmentMapGen(4, 4);

		super.storeMapDataInSQL(dbs, map);
	}


	@Override
	public String getMission1Liner(int side) {
		return "You must kill more enemies than any other.";
	}

	
	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		return 0;
	}


	@Override
	public boolean hasSpecialVPCalc() {
		return true;
	}


	@Override
	public boolean doWeCareWhoKilledUnits() {
		return false;
	}


	@Override
	public MyList<Integer> getSidesForSide(int side) {
		switch (side) {
		case 1:
		case 2:
			return MyList.CreateIntsFromInts(1,2);
		case 3:
		case 4:
			return MyList.CreateIntsFromInts(3,4);
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}
	

}
