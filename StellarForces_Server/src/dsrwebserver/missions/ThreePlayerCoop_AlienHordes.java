package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

/**
 * 10/6/14 - Added two more aliens
 * 
 */
public class ThreePlayerCoop_AlienHordes extends AbstractMission {

	private static final int MAX_TURNS = 20;

	public ThreePlayerCoop_AlienHordes() {
		super(AbstractMission.ALIEN_HORDES_3P, CP_DEL_ME, 3, "The Aliens", "Laser Squad", "Galactix Corp", "", MAX_TURNS, 0, 180, 180, -1, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 0.51f, AbstractMission.WEAK_WALLS, AbstractMapModel.SLIM_WALLS, "coop_alien_hordes.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, SPECIFIED_WIN_AT_END, "coop_alienhordes.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Alien Attackers";
		case 2:
			return "The Human Defenders 1";
		case 3:
			return "The Human Defenders 2";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "The moonbase must be defended from a large horde of aliens by two squads of human defenders.";
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill the humans";
		case 2:
		case 3:
			return "You must kill the aliens";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}



}
