package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class AlienEscapeMission extends AbstractMission {
	
	private static final int MAX_TURNS = 20;

	public AlienEscapeMission() {
		super(AbstractMission.ALIEN_ESCAPE, CP_DEL_ME, "Laser Squad", "Aliens", MAX_TURNS, 300, 0, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.23f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "alien_escape.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "alien_escape.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 1;
	}

	
	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Human Escapees";
		case 2:
			return "The Aliens";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}
	
	
	public String getShortDesc() {
		return "The humans must escape from the alien-infested moonbase, avoiding the aliens which will impregnate dead humans.  The aliens must kill all the humans.";
	}

	

	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE && t != EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE;
		}
		return true;
	}


	public boolean aliensImpregnate() {
		return true;
		
	}


	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must escape";
		case 2:
			return "You must kill the humans";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}
	
	

}
