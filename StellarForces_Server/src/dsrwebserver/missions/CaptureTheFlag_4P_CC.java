package dsrwebserver.missions;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.maps.MapData;
import dsrwebserver.tables.EquipmentTypesTable;

public class CaptureTheFlag_4P_CC extends AbstractMission {

	private static final int MAX_TURNS = 30;
	private static final int CREDS = 21*8;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Renegade Squad";
	private static final String SIDE3_NAME = "Omni Corp";
	private static final String SIDE4_NAME = "Gallactix Corp";

	public CaptureTheFlag_4P_CC() {
		super(AbstractMission.CAPTURE_THE_FLAG_4P_CC_ONLY, CP_SHADOW_CONSPIRACY, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, CREDS, CREDS, CREDS, CREDS, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.26f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "capture_the_flag.txt", IS_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, SPECIFIED_WIN_AT_END, "capturetheflag.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "The incredibly valuable F.L.A.G. is located in the middle of the moonbase.  Four teams battle it out in close quarters to be the first to capture it and escape.";
	}


	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		MapData map = super.createBasicMap(dbs, gameid);

		AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_FLAG, -1, -1, -1, -1);
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_GUN && t != EquipmentTypesTable.ET_GRENADE; 
		}
		return true;
	}


	public boolean doesMissionInvolveFlag() {
		return true;
	}


	public String getMission1Liner(int side) {
		return "You must get the flag to the <b>opposite</b> side of the map";
	}



}

