package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public class LastManStanding2_LargeMap extends AbstractMission {
	
	private static final int MAX_TURNS = 30;
	private static final int CREDS = 400;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "The Omni Corp";
	private static final String SIDE3_NAME = "Globex Industries";
	private static final String SIDE4_NAME = "Banana Republic";

	public LastManStanding2_LargeMap() {
		super(AbstractMission.LAST_MAN_STANDING_LARGE_4_PLAYER, CP_AZARIAN_CRUSADE, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, CREDS, CREDS, CREDS, CREDS, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.09f, AbstractMission.WEAK_WALLS, AbstractMapModel.SLIM_WALLS, "4-player_last_man_standing_maze.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_1, IS_NOT_SNAFU, SPECIFIED_WIN_AT_END, "lastmanstanding2.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "Four teams battle it out in a large complex.";
	}


	@Override
	public String getMission1Liner(int side) {
		return "You must kill more enemies than any other.";
	}


	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		try {
			// Check we have some units left
			String sql = "SELECT COUNT(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED;
			if (dbs.getScalarAsInt(sql) <= 0) {
				return 0;
			}
			// Calc from units killed
			String sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + game.getID() + " AND Side <> " + side + " AND Status = " + UnitsTable.ST_DEAD; // NOT IN (" + game.getOpponentSidesForSideAsCSV(side, mission) + ")
			sql_vps = sql_vps + " AND COALESCE(KilledBySide, 0) IN (" + side + ")";
			int vps = dbs.getScalarAsInt(sql_vps);

			return vps;
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
			return 0;
		}
	}


	@Override
	public boolean hasSpecialVPCalc() {
		return true;
	}


	@Override
	public boolean doWeCareWhoKilledUnits() {
		return true;
	}

	

}
