package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

/**
 * 20/1/14: The aliens now have a queen (replacing an existing alien)
 * 7/8/14 - Reduced human creds by 30 to 260
 * 
 *
 */
public class TheAlienHordes extends AbstractMission {

	private static final int MAX_TURNS = 18;

	public TheAlienHordes() {
		super(AbstractMission.THE_ALIEN_HORDES, CP_DEL_ME, "The Aliens", "Galactix Corp", MAX_TURNS, 0, 260, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.58f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "the_alien_hordes.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "thealienhordes.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Alien Attackers";
		case 2:
			return "The Human Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "The moonbase must be defended by the human colonists from a large horde of aliens who are approaching.";
	}

	
	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false;
		}
		return false;
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill the humans";
		case 2:
			return "You must kill the aliens";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	

}
