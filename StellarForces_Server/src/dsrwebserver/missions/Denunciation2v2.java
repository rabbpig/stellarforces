package dsrwebserver.missions;

import java.awt.Point;
import java.sql.ResultSet;
import java.util.ArrayList;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import ssmith.util.MyList;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.MapData;
import dsrwebserver.maps.MapLoader;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public class Denunciation2v2 extends AbstractMission {

	private static final int MAX_TURNS = 25;
	private static final int CREDS = 200;

	private static final String SIDE1_NAME = "Banana Republic A";
	private static final String SIDE2_NAME = "The Rebels A";
	private static final String SIDE3_NAME = "Banana Republic B";
	private static final String SIDE4_NAME = "The Rebels B";

	public Denunciation2v2() {
		super(AbstractMission.DENUNCIATION_2V2, CP_SIGMA_7, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, CREDS, CREDS, CREDS, CREDS, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.72f, AbstractMission.WEAK_WALLS, AbstractMapModel.SLIM_WALLS, "denunciation2vs2.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, false, SPECIFIED_WIN_AT_END, "denunciation2vs2.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "Two sides, each controlled by two players, must assassinate an opponent's ambassador who is somewhere in the UN village.  Each player controls a squad and one ambassador.";
	}



	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		MapData map = MapLoader.Import(dbs, gameid, this.map_filename, true);

		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);

		ArrayList<Point> deploy_squares = new ArrayList<Point>();
		deploy_squares.add(new Point(Functions.rnd(5, 55),34));
		deploy_squares.add(new Point(Functions.rnd(5, 55),23));
		deploy_squares.add(new Point(Functions.rnd(5, 55),34));
		deploy_squares.add(new Point(Functions.rnd(5, 55),23));

		// Deploy ambassadors
		ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE Status = " + UnitsTable.ST_AWAITING_DEPLOYMENT + " AND GameID = " + gameid + " AND CanDeploy = 0 ORDER BY Side");
		while (rs.next()) {
			int rnd = 0;
			Point p = deploy_squares.get(rnd);
			deploy_squares.remove(rnd);
			dbs.runSQLUpdate("UPDATE Units SET Status = " + UnitsTable.ST_DEPLOYED + ", MapX = " + p.x + ", MapY = " + p.y + " WHERE UnitID = " + rs.getInt("UnitID"));
			try {
				// Add event
				UnitsTable unit = new UnitsTable(dbs);
				unit.selectRow(rs.getInt("UnitID"));
				int seen_by_side[] = {0, 0, 0, 0, 0};
				seen_by_side[unit.getSide()] = 1; // Only owner side knows about it
				UnitHistoryTable.AddRecord_UnitDeployed(dbs, game, unit, seen_by_side, System.currentTimeMillis());
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}
		}

		super.storeMapDataInSQL(dbs, map);
	}


	public String getMission1Liner(int side) {
		return "You must kill your opponent's ambassador";
	}

	
	@Override
	public MyList<Integer> getSidesForSide(int side) {
		switch (side) {
		case 1:
		case 3:
			return MyList.CreateIntsFromInts(1,3);
		case 2:
		case 4:
			return MyList.CreateIntsFromInts(2,4);
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}
	
}
