package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class TheAssassinsNewHouse extends AbstractMission {

	private static final int MAX_TURNS = 30;

	public TheAssassinsNewHouse() {
		super(AbstractMission.THE_ASSASSINS_NEW_HOUSE, 0, "Laser Squad", "Globex Megacorp", MAX_TURNS, 330, 200, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.56f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "the_assassins_new_house.txt", "", IS_CAMPAIGN, AbstractMission.GR_LEVEL_1, SPECIFIED_WIN_AT_END, "The_Assasins_New_House_By_Globex-v1.0.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Assassins";
		case 2:
			return "The Fugitives";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "Sterner Regnix must be killed!  One player controls the assassins, and the other player controls Sterner and his bodyguards.";
	}


	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill Sterner Regnix";
		case 2:
			return "You must kill the assassins";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
		return t != EquipmentTypesTable.ET_SCANNER;
		}
		return true;
	}

	
}
