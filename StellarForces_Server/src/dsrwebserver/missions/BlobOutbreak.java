package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class BlobOutbreak extends AbstractMission {

	private static final int MAX_TURNS = 25;
	private static final String SIDE1_NAME = "Blobs";
	private static final String SIDE2_NAME = "Marsec Corporation";

	public BlobOutbreak() {
		super(AbstractMission.BLOB_OUTBREAK, -1, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 0, 350, -1, AbstractMission.SHOW_CEILINGS, 10, 0, 1.63f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "blob_outbreak.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, SPECIFIED_WIN_AT_END, "blob_outbreak.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "There has been an outbreak of blobs at a research laboratory.";
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must destroy the computers and kill the scientists";
		case 2:
			return "You must destroy the blobs";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}



}
