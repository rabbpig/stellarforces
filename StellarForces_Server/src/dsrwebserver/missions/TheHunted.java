package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.util.MyList;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public class TheHunted extends AbstractMission {

	private static final int MAX_TURNS = 30;

	private static final String SIDE1_NAME = "The Hunted 1";
	private static final String SIDE2_NAME = "The Hunted 2";
	private static final String SIDE3_NAME = "The Hunted 3";
	private static final String SIDE4_NAME = "The Hunters";

	public TheHunted() {
		super(AbstractMission.THE_HUNTED, -1, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, 25, 25, 25, 200, -1, true, 0, 0, 0, 0, 1.57f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "thehunted.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, SPECIFIED_WIN_AT_END, "thehunted.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getMission1Liner(int side) {
		return "The Hunted must try and kill the Hunters";
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (side >= 1 && side <= 3) {
			if (super.isItemTypeAllowed(t, side)) {
				return t != EquipmentTypesTable.ET_GUN && t != EquipmentTypesTable.ET_GRENADE; 
			}
		}
		return true;
	}


	@Override
	public String getShortDesc() {
		return "A terminator squad must kill poorly armed civilians.";
	}
	

	@Override
	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		try {
			// Check we have some units left
			/*String sql = "SELECT COUNT(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED;
			if (dbs.getScalarAsInt(sql) <= 0) {
				return 0;
			}*/
			int vps = 0;
			// Calc from units killed
			if (side == 4) { // The hunters
				String sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + game.getID() + " AND Side <> " + side + " AND Status = " + UnitsTable.ST_DEAD;
				sql_vps = sql_vps + " AND COALESCE(KilledBySide, 0) IN (0, " + side + ")";
				vps = dbs.getScalarAsInt(sql_vps);
			} else { // The hunted
				// How many hunters have we killed?
				String sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + game.getID() + " AND Side = 4 AND Status = " + UnitsTable.ST_DEAD;
				sql_vps = sql_vps + " AND COALESCE(KilledBySide, 0) IN (0, " + side + ")";
				vps = dbs.getScalarAsInt(sql_vps);
				
				// How many comrades have we killed?
				sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + game.getID() + " AND Side NOT IN(" + side + ", 4) AND Status = " + UnitsTable.ST_DEAD;
				sql_vps = sql_vps + " AND COALESCE(KilledBySide, 0) IN (" + side + ")";
				vps -= dbs.getScalarAsInt(sql_vps);
			}

			return vps;
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
			return 0;
		}
	}


	@Override
	public boolean hasSpecialVPCalc() {
		return true;
	}


	@Override
	public boolean doWeCareWhoKilledUnits() {
		return true;
	}


	@Override
	public MyList<Integer> getSidesForSide(int side) {
		switch (side) {
		case 1:
		case 2:
		case 3:
			return MyList.CreateIntsFromInts(1,2,3);
		case 4:
			return MyList.CreateIntsFromInts(4);
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}
	

}
