package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;

public class MeltdownMission extends AbstractMission {
	
	private static final int MAX_TURNS = 35;
	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Marsec Corporation";

	public MeltdownMission() {
		super(AbstractMission.MELTDOWN, CP_SHADOW_CONSPIRACY, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 380, 280, 2, AbstractMission.SHOW_CEILINGS, 25, 0, 1.45f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "meltdown.txt", "meltdown_campaign.txt", IS_CAMPAIGN, AbstractMission.GR_LEVEL_2, SPECIFIED_WIN_AT_END, "meltdown.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Saboteurs";
		case 2:
			return "The Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "Highly explosive computers must be defended or destroyed.  One player must destroy the computers and the other player must defend them.  Warning: when the computers explode, they will destroy everything close by!";
	}

	
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must destroy the computers";
		case 2:
			return "You must prevent the computers being destroyed";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}



}
