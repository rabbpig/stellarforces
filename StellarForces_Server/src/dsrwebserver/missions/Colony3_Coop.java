package dsrwebserver.missions;

import java.io.IOException;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.util.MyList;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.MapData;
import dsrwebserver.maps.SegmentMapGen;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public class Colony3_Coop extends ColonizationAbstract {

	private static final int CREDS = 160;

	protected static final String SIDE1_NAME = "Laser Engineers";
	protected static final String SIDE2_NAME = "Laser Clones";
	protected static final String SIDE3_NAME = "Globex Engineers";
	protected static final String SIDE4_NAME = "Globex Clones";

	public Colony3_Coop() {
		super(AbstractMission.COLONY3_COOP, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, -1, CREDS, CREDS, CREDS, CREDS, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.74f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "colony3_coop.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, false, SPECIFIED_WIN_AT_END, "", 20);
	}


	@Override
	public MyList<Integer> getSidesForSide(int side) {
		switch (side) {
		case 1:
		case 2:
			return MyList.CreateIntsFromInts(1,2);
		case 3:
		case 4:
			return MyList.CreateIntsFromInts(3,4);
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "Two colonies battle it out in a war of attrition.  One player on each side controls the engineers who can build structures to create new clones and weapons; the other player controls the clones that are created.  The last colony to survive is the winner.";
	}


	@Override
	public String getMission1Liner(int side) {
		return "You must ensure your colony is the only one to survive.";
	}


	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws SQLException, IOException {
		MapData map = new SegmentMapGen(4, 4);

		super.storeMapDataInSQL(dbs, map);
	}


	@Override
	public void nextPhase(MySQLConnection dbs, GamesTable game) throws SQLException {
		super.nextPhase(dbs, game);

		// Make all clones owned by sides 2 and 4.
		try {
			dbs.runSQLUpdate("UPDATE Units SET Side = 2 WHERE GameID = " + game.getID() + " AND Side = 1 AND ModelType = " + UnitsTable.MT_SCIENTIST);
			// Update Names - todo
			//int order = dbs.getScalarAsInt("SELECT MAX(OrderBy) FROM Units WHERE GameID = " + game.getID() + " AND Side = 2") + 1;
			//dbs.
			dbs.runSQLUpdate("UPDATE Units SET Side = 4 WHERE GameID = " + game.getID() + " AND Side = 3 AND ModelType = " + UnitsTable.MT_SCIENTIST);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


}

