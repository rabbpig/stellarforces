package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.util.MyList;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public class TheAssassins2v2 extends AbstractMission {

	private static final int MAX_TURNS = 20;

	public TheAssassins2v2() {
		super(AbstractMission.THE_ASSASSINS_2V2, CP_STERNER_REGNIX, 4, "Assassins 1", "Sterner Regnix", "Assassins 2", "Bodyguards", MAX_TURNS, 132, 70, 132, 150, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.74f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "the_assassins.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, false, SPECIFIED_WIN_AT_END, "theassassins.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Assassins 1";
		case 2:
			return "Sterner Regnix";
		case 3:
			return "The Assassins 2";
		case 4:
			return "The Bodyguards";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	public String getShortDesc() {
		return "Sterner Regnix must be killed!";
	}

	
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
		case 3:
			return "You must kill Sterner Regnix";
		case 2:
		case 4:
			return "You must kill the assassins";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public MyList<Integer> getSidesForSide(int side) {
		switch (side) {
		case 1:
		case 3:
			return MyList.CreateIntsFromInts(1,3);
		case 2:
		case 4:
			return MyList.CreateIntsFromInts(2,4);
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}
	

	/*public int canBePlayedOnAndroid() {
		return ANY_CLIENT;
	}
	*/
	
	@Override
	public boolean hasSpecialVPCalc() {
		// Override if req
		return true;
	}


	@Override
	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		int vps = 0;
		AbstractMission mission = AbstractMission.Factory(this.getMissionID());

		// Calc from units killed
		String sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + game.getID() + " AND Side IN (" + game.getOpponentSidesForSideAsCSV(side, mission) + ") AND Status = " + UnitsTable.ST_DEAD;
		vps = dbs.getScalarAsInt(sql_vps);

		return vps;
	}



}
