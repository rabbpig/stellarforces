package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

/**
 * 7/10/13 - Side 1 now has 220 creds down from 250.
 * 23/12/13 - Side 1 now has 190 creds.
 *
 */
public class TerrorsOfTheDeep extends AbstractMission {

	private static final int MAX_TURNS = 20;

	public TerrorsOfTheDeep() {
		super(AbstractMission.TERRORS_OF_THE_DEEP, CP_BLOBS, "The Aquanaughts", "The Blobs", MAX_TURNS, 190, 0, 1, AbstractMission.SHOW_CEILINGS, 50, 0, 1.70f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.BLOCK_WALLS, "terrors_of_the_deep.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, SPECIFIED_WIN_AT_END, "terrors_of_the_deep.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Aquanaughts";
		case 2:
			return "The Blobs";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}


	public String getShortDesc() {
		return "Computers must be destroyed or defended.";
	}

	
	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must destroy the computers";
		case 2:
			return "You must kill the Aquanaughts";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	

}
