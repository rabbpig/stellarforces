package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;

public class LaboratoryAttackMission extends AbstractMission {

	private static final int MAX_TURNS = 25;
	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Marsec Corporation";

	public LaboratoryAttackMission() {
		super(AbstractMission.LABORATORY_ATTACK, CP_SHADOW_CONSPIRACY, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 450, 450, 2, AbstractMission.SHOW_CEILINGS, 10, 0, 1.63f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "laboratory_attack.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, SPECIFIED_WIN_AT_END, "laboratoryattack.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "The base computers and scientists must be protected.  One player controls an attack squad who must kill enough computers and scientists, and the other player controls the defenders and the scientists.";
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must destroy the computers and kill the scientists";
		case 2:
			return "You must kill the assassins";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}



}
