package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

public class AlienHordes_Xenophobia extends AbstractMission {

	private static final int MAX_TURNS = 28; // Was 26, 7/8/14

	public AlienHordes_Xenophobia() {
		super(AbstractMission.ALIEN_HORDES_XENOPHOBIA, CP_DEL_ME, "The Aliens", "Galactix Corp", MAX_TURNS, 0, 290, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 1.58f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "alien_hordes_xenophobia.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_1, SPECIFIED_WIN_AT_END, "thealienhordes.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Alien Attackers";
		case 2:
			return "The Human Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "The moonbase must be defended by the human colonists from a large horde of aliens who are approaching.";
	}

	
	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false;
		}
		return false;
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill the humans";
		case 2:
			return "You must kill the aliens";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	

}
