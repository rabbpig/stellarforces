package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;

public class TheAssassins_Campaign extends AbstractMission {

	private static final int MAX_TURNS = 20; // LS is 20

	public TheAssassins_Campaign() {
		super(AbstractMission.THE_ASSASSINS_CAMPAIGN, CP_STERNER_REGNIX, "Laser Squad", "Globex Megacorp", MAX_TURNS, 320, 260, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 0.45f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "the_assassins_campaign.txt", "", IS_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "theassassins_campaign.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Assassins";
		case 2:
			return "The Fugitives";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	public String getShortDesc() {
		return "Sterner Regnix must be killed!  One player controls the assassins, and the other player controls Sterner and his bodyguards.";
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill Sterner Regnix";
		case 2:
			return "You must kill the assassins";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


}
