package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.missions.ai.TheAssassinsAI;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;


/**
 * 7/10/13 - Sterner now has only 185 creds down from 200.
 * 13/8/15 - Side 1 has 230 (was 220), Sterner now has only 180 creds down from 230.
 */
public final class TheAssassinsMission extends AbstractMission {
	
	private static final int MAX_TURNS = 20; // LS is 20

	public TheAssassinsMission() {
		super(AbstractMission.THE_ASSASSINS, CP_STERNER_REGNIX, "Laser Squad", "Globex Megacorp", MAX_TURNS, 230, 180, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.56f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "the_assassins.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "theassassins.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Assassins";
		case 2:
			return "The Fugitives";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "Sterner Regnix must be killed!  One player controls the assassins, and the other player controls Sterner and his bodyguards.";
	}


	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill Sterner Regnix";
		case 2:
			return "You must kill the assassins";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	/*public int canBePlayedOnAndroid() {
		return ANY_CLIENT;
	}

	 */
	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
		return t != EquipmentTypesTable.ET_SCANNER;
		}
		return true;
	}


	@Override
	public void mapCreated(MySQLConnection dbs, int gameid) throws SQLException {
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);

		if (game.getAIForSide(2) > 0) {
			TheAssassinsAI.CreateAI(dbs, game);
		}
	}


	@Override
	public int doesSideHaveAI(int side) {
		if (side == 2) {
			return 1;
		}
		return 0;
	}
	
}
