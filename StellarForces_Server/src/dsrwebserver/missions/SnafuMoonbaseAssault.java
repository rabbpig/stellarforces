package dsrwebserver.missions;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitsTable;

public class SnafuMoonbaseAssault extends AbstractMission {

	private static final int MAX_TURNS = 20;
	
	private static final int VPS_FOR_NON_SAB_UNIT = -40;
	private static final int VPS_FOR_SAB_UNIT = 25;
	private static final int VPS_FOR_LAST_SAB_UNIT = 60;
	private static final int VPS_FOR_CPU = 5;
	private static final int CREDS = 180;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Marsec Corp";
	private static final String SIDE3_NAME = "Globex Inc.";
	private static final String SIDE4_NAME = "Space Ruffians";

	public SnafuMoonbaseAssault() {
		super(AbstractMission.SNAFU_MOONBASE_ASSAULT, CP_OMNI_CORP, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, CREDS, CREDS, CREDS, CREDS, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.43f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "snafu_moonbase_assault.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_SNAFU, IS_SNAFU, SPECIFIED_WIN_AT_END, "snafu_moonbaseassault.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "One randomly chosen player is the saboteur, and must destroy the computers, and the other players must prevent this.  However, only the saboteur knows it is they who must destroy the computers; the other 3 players must try and work it out.";
	}



	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		super.createMap(dbs, gameid);

		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);
		
		// Create snafu data
		int saboteur = Functions.rnd(1, 4);
		game.setSnafuData(saboteur, 1);
		game.close();
	}


	public String getSnafuMissionText(GamesTable game, int side) throws Exception {
		if (game.getSnafuData(side) > 0) {
			return "You are the saboteurs!  You must destroy the computers!";
		} else {
			return "You must defend the computers.  You must only kill units from the saboteur side!";
		}
	}


	public boolean isItemTypeAllowed(int t, int side) {
		return t != EquipmentTypesTable.ET_GRENADE && t != EquipmentTypesTable.ET_SMOKE_GRENADE && t != EquipmentTypesTable.ET_NERVE_GAS && t != EquipmentTypesTable.ET_ADRENALIN_SHOT;
	}


	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		int vps = 0;
		boolean is_sab = (game.getSnafuData(side) > 0);
		int sab_side = -1;
		for (int i=1 ; i<=4 ; i++) {
			if (game.getSnafuData(i) > 0) {
				sab_side = i;
				break;
			}
		}
		// Calc from units
		if (is_sab == false) {
			// VPs for killing wrong side
			String sql_vps = "SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side <> " + sab_side + " AND Side IN (" + game.getOppositeSidesForSideAsCSV(side) + ") AND Status = " + UnitsTable.ST_DEAD + " AND COALESCE(KilledBySide, 0) IN (" + side + ")";
			vps += (dbs.getScalarAsInt(sql_vps) * VPS_FOR_NON_SAB_UNIT);
			
			// VPs for killing sab units
			sql_vps = "SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + sab_side + " AND Status = " + UnitsTable.ST_DEAD + " AND COALESCE(KilledBySide, 0) IN (0, " + side + ")";
			vps += dbs.getScalarAsInt(sql_vps) * VPS_FOR_SAB_UNIT;

			try {
				// If they killed the last unit, give extra VPs
				int sabs_remaining = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + sab_side + " AND Status = " + UnitsTable.ST_DEPLOYED);
				if (sabs_remaining <= 0) {
					ResultSet rs = dbs.getResultSet("SELECT KilledBySide FROM Units WHERE GameID = " + game.getID() + " AND Side = " + sab_side + " AND Status = " + UnitsTable.ST_DEAD + " AND COALESCE(KilledBySide, 0) > 0 ORDER BY DateKilled DESC");
					if (rs.next()) {
						int last_unit_killer = rs.getInt("KilledBySide");
						if (last_unit_killer == side) {
							vps += (VPS_FOR_LAST_SAB_UNIT - VPS_FOR_SAB_UNIT);
						}
					}
				}
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}

		} else { // Is Sab
			// Calc from destroyed computers
			String sql = "SELECT Count(*) FROM MapDataSquares WHERE MapDataID = " + game.getMapDataID() + " AND SquareType = " + MapDataTable.MT_COMPUTER + " AND Destroyed = 1";
			vps += (VPS_FOR_CPU * dbs.getScalarAsInt(sql));
			if (vps < 100) {
				vps = 0; // Sabo must get 100 VPs to win!
			}
		}

		return vps;
	}

	
	@Override
	public boolean hasSpecialVPCalc() {
		return true;
	}

	
	@Override
	public String getMission1Liner(int side) {
		return "A saboteur must destroy the computers.";
	}

	
/*	public int canBePlayedOnAndroid() {
		return ANY_CLIENT;
	}
*/
}
