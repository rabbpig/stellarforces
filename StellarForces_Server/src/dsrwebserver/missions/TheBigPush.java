package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;

public class TheBigPush extends AbstractMission {

	private static final int MAX_TURNS = 20;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Renegade Squad";

	public TheBigPush() {
		super(AbstractMission.THE_BIG_PUSH, CP_SIGMA_7, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 550, 550, -1, AbstractMission.NO_CEILINGS, 0, 0, 1.69f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "thebigpush.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "thebigpush.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "Two sides must battle it out.  One side starts at a disadvantage as they are stuck in the open with no cover.";
	}


	@Override
	public String getMission1Liner(int side) {
		return "Kill all your opponent's units";
	}

	

}
