package dsrwebserver.missions;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.MapData;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GameLogTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public class CabinInTheWoods extends AbstractMission {

	private static final int MAX_TURNS = 25;

	private static final String SIDE1_NAME = "Residents";
	private static final String SIDE2_NAME = "Zombies";

	public CabinInTheWoods() {
		super(AbstractMission.CABIN_IN_THE_WOODS, -1, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 10, 0, 1, AbstractMission.NO_CEILINGS, 0, 0, 1.69f, AbstractMission.WEAK_WALLS, AbstractMapModel.SLIM_WALLS, "cabin_in_the_woods.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "cabin_in_the_woods.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "A horde of zombies have descended on a small town";
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1: return "You must survive the zombie attack until the end";
		case 2: return "Eat lots of resident's brainzzz";
		default: return "Unknown";
		}
	}


	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);
		MapData map = super.createBasicMap(dbs, gameid);

		for (int i=0 ; i<10 ; i++) {
			int x = Functions.rnd(10, 38);
			int y = Functions.rnd(10, 38);
			while (MapDataTable.GetMapSquareType(dbs, game.getMapDataID(), x, y) != MapDataTable.MT_FLOOR) {
				x = Functions.rnd(10, 38);
				y = Functions.rnd(10, 38);
			}
			AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_SP30, x, y, 7, 0);
			AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_PISTOL_CLIP, x, y, 7, 0);
		}
		game.close();
	}


	@Override
	public void nextTurn(MySQLConnection dbs, GamesTable game) throws SQLException {
		// Need to do this here as the PC Client doesn't have ZOMBIE units (which extends this class)
		// Restore dead on side 2 only after side 1 has taken their turn (so they deon't reappear straight away)
		UnitsTable unit = new UnitsTable(dbs);
		ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE GameID = " + game.getID() + " AND Side = 2 AND Status = " + UnitsTable.ST_DEAD);
		while (rs.next()) {
			if (Functions.rnd(1, 3) == 1) {
				unit.selectRow(rs.getInt("UnitID"));
				unit.setHealth(unit.getMaxHealth());
				unit.setStatus(UnitsTable.ST_DEPLOYED);
				unit.setOnFire(false);
				GameLogTable.AddRec(dbs, game, -1, -1, unit.getName() + " has been reanimated.", false, System.currentTimeMillis());

				try {
					// Remove corpse
					String code = EquipmentTypesTable.CD_HUMAN_CORPSE_SIDE + unit.getSide();
					int mtid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, code);
					dbs.runSQLDelete("DELETE FROM Equipment WHERE GameID = " + game.getID() + " AND EquipmentTypeID = " + mtid + " AND MapX = " + unit.getMapX() + " AND MapY = " + unit.getMapY());
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
			}
		}
		rs.close();

		// Create new zombies
		int order = dbs.getScalarAsInt("SELECT COUNT(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = 2")+1;
		if (order < 16) {
			unit.createUnit(game.getID(), order, UnitsTable.MT_ZOMBIE, "Zombie", 2, 38, 30, 0, 0, 250, 1, 70, 30, 0, 400, 0, 0);
			unit.setStatus(UnitsTable.ST_DEPLOYED);
			switch (Functions.rnd(1, 4)) {
			case 1: 
				unit.setMapXYAngle(0, 0, 90);
				break;
			case 2: 
				unit.setMapXYAngle(47, 0, 0);
				break;
			case 3: 
				unit.setMapXYAngle(0, 47, 90);
				break;
			case 4: 
				unit.setMapXYAngle(47, 47, 180);
				break;
			}
			unit.setCurrentAPs(unit.getMaxAPs());
			GameLogTable.AddRec(dbs, game, -1, -1, "A zombie has arrived!", false, System.currentTimeMillis());
			try {
				// Add event
				int seen_by_side[] = {0, 0, 0, 0, 0};
				seen_by_side[unit.getSide()] = 1; // Only owner side knows about it
				UnitHistoryTable.AddRecord_UnitDeployed(dbs, game, unit, seen_by_side, System.currentTimeMillis());
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}
		}
		unit.close();
	}


	@Override
	public boolean canSideWinByKillingAllOpposition(int side) {
		return side == 2;
	}



}

