package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;

/**
 * THIS MISSION IS UNFINISHED (I.E. HARDLY STARTED)
 *
 */
public class DescentMission extends AbstractMission {

	private static final int MAX_TURNS = 20;

	public DescentMission() {
		super(AbstractMission.DESCENT, -1, "Laser Squad", "Globex Megacorp", MAX_TURNS, 320, 350, 1, AbstractMission.SHOW_CEILINGS, 0, 0, 1.58f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "descent.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "descent.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 2;
	}

	
	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Assassins";
		case 2:
			return "The Fugitives Side";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "Sterner Regnix must escape from the complex.";
	}

	
	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill Sterner Regnix";
		case 2:
			return "Sterner Regnix must escape";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
}
