package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

/**
 * 22/8/14 - Reduced side 4 creds from 150 to 110
 * 
 */
public class TheAssylum extends AbstractMission {

	private static final int MAX_TURNS = 25;
	private static final String SIDE1_NAME = "Prisoners 1";
	private static final String SIDE2_NAME = "Prisoners 2";
	private static final String SIDE3_NAME = "Prisoners 3";
	private static final String SIDE4_NAME = "The Guards";

	public TheAssylum() {
		super(AbstractMission.THE_ASSYLUM, CP_REBELLION, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, 10, 10, 10, 110, 4, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.07f, AbstractMission.WEAK_WALLS, AbstractMapModel.BLOCK_WALLS, "the_assylum.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, false, SPECIFIED_WIN_AT_END, "the_assylum.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side != 4;
	}

	
	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public String getShortDesc() {
		return "Groups of poorly-armed prisoners must escape.  3 players each control a group of prisoners, and one player controls the prison guards.  Can the prisoners work together or will they kill each other before they get a chance to escape?";
	}

	
	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
		if (side != 4) { // Prisoners
			return t == EquipmentTypesTable.ET_CC_WEAPON;//.GRENADE && t != EquipmentTypesTable.DEATH_GRENADE;
		} else { // Guards
			return t != EquipmentTypesTable.ET_DEATH_GRENADE && t != EquipmentTypesTable.ET_GRENADE;
		}
		}
		return true;
	}


	@Override
	public int getMaxProtection(int side) {
		return 20;
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
		case 2:
		case 3:
			return "You must escape from the prison";
		case 4:
			return "You must kill the prisoners";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


}

