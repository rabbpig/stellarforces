package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.MapData;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GameLogTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;


/**
 * 22/8/14 - Increased side 1 creds from 250 to 270, removed 1 clone unit at start
 * 
 */
public class CloneWars extends AbstractMission {

	private static final int MAX_TURNS = 30;

	/**
	 * 9/9/13 - Side 1 now only gets 7 VPs per CPU, and clone weapon now SP30.
	 */
	public CloneWars() {
		super(AbstractMission.CLONE_WARS, CP_OMNI_CORP, "Research Monitors", "Clones", MAX_TURNS, 270, 100, 2, AbstractMission.SHOW_CEILINGS, 7, 0, 1.62f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "clone_wars.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, SPECIFIED_WIN_AT_END, "clone_wars.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Attackers";
		case 2:
			return "The Clone Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "One player must attack the moonbase and destroy the computers.  The other player must defend the moonbase with clones.  Every turn, a new clone is created.";
	}


	@Override
	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must destroy the computers";
		case 2:
			return "You must prevent the computers being destroyed";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			// Grenades make it too easy to destroy all the computers quickly
			return t != EquipmentTypesTable.ET_SCANNER && t != EquipmentTypesTable.ET_GRENADE;
		}
		return true;
	}


	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);
		MapData map = super.createBasicMap(dbs, gameid);

		for (int i=0 ; i<30 ; i++) {
			int x = Functions.rnd(29, 33);
			int y = Functions.rnd(30, 36);
			while (MapDataTable.GetMapSquareType(dbs, game.getMapDataID(), x, y) != MapDataTable.MT_FLOOR) {
				x = Functions.rnd(29, 33);
				y = Functions.rnd(30, 36);
			}
			AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_SP30, x, y, 7, 2);
			//AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_MACHINE_CLIP, x, y);
		}
		game.close();
	}


	@Override
	public void nextTurn(MySQLConnection dbs, GamesTable game) throws SQLException {
		// Create new clones
		int order = dbs.getScalarAsInt("SELECT COUNT(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = 2")+1;
		UnitsTable unit = new UnitsTable(dbs);
		unit.createUnit(game.getID(), order, UnitsTable.MT_SCIENTIST, "Clone", 2, 64, 36, 0, 0, 250, 35, 50, 10, 0, 40, 0, 0);
		unit.setStatus(UnitsTable.ST_DEPLOYED);
		unit.setMapXYAngle(31, 32, 90);
		unit.setCurrentAPs(unit.getMaxAPs());
		GameLogTable.AddRec(dbs, game, -1, -1, "A clone has been created.", false, System.currentTimeMillis());
		try {
			// Add event
			int seen_by_side[] = {0, 0, 0, 0, 0};
			seen_by_side[unit.getSide()] = 1; // Only owner side knows about it
			UnitHistoryTable.AddRecord_UnitDeployed(dbs, game, unit, seen_by_side, System.currentTimeMillis());
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	@Override
	public boolean canSideWinByKillingAllOpposition(int side) {
		return side == 2;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			if (side == 2) {
				return code.equalsIgnoreCase(EquipmentTypesTable.CD_NERVE_GAS) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_INCENDIARY_GRENADE) == false; // It's too easy otherwise
			} else {
				return true;
			}
		}
		return false;
	}



}
