package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public class SnafuTheAssassins2 extends AbstractMission {

	private static final int MAX_TURNS = 20;

	private static final int VPS_FOR_NON_ENEMY_UNIT = -40;
	private static final int VPS_FOR_TARGET_UNIT = 20;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Marsec Corp";
	private static final String SIDE3_NAME = "Globex Inc.";
	private static final String SIDE4_NAME = "Space Ruffians";

	public SnafuTheAssassins2() {
		super(AbstractMission.SNAFU_THE_ASSASSINS_2, CP_STERNER_REGNIX, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, 240, 240, 240, 240, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.38f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "snafu_the_assassins2.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_SNAFU, IS_SNAFU, SPECIFIED_WIN_AT_END, "snafu_theassassins2.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "Each player must kill all the units on another randonly chosen side, and no others.  However, no-one knows which side is trying to kill them.";
	}



	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		super.createMap(dbs, gameid);

		// Create snafu data
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);

		boolean selected[] = new boolean[5];
		for (int s=1 ; s<=4 ; s++) {
			int target = -1;
			while (true) {
				target = Functions.rnd(1, 4);
				if (target != s) { // Don't kill ourselves
					if (selected[target] == false) {
						break;
					}
				}
			}
			selected[target] = true;
			game.setSnafuData(s, target); // Which side must they kill?
		}
		game.close();
	}


	public String getSnafuMissionText(GamesTable game, int side) throws Exception {
		return "You must kill all the units on side " + game.getSnafuData(side) + "!";
	}


	public static int GetTargetsRemaining(MySQLConnection dbs, GamesTable game, int our_side) throws SQLException {
		int target_side = game.getSnafuData(our_side);
		String sql = "SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + target_side + " AND Status = " + UnitsTable.ST_DEPLOYED;
		return dbs.getScalarAsInt(sql);
	}


	@Override
	public int getVPs(MySQLConnection dbs, GamesTable game, int our_side) throws SQLException {
		int vps = 0;

		int target_side = game.getSnafuData(our_side);
		int side_trying_to_kill_us = -1;
		for (int i=1 ; i<=4 ; i++) {
			if (game.getSnafuData(i) == our_side) {
				side_trying_to_kill_us = i;
				break;
			}
		}

		// Negative VPs for killing wrong side
		String sql_vps = "SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side <> " + target_side + " AND Side <> " + side_trying_to_kill_us + " AND Status = " + UnitsTable.ST_DEAD + " AND COALESCE(KilledBySide, 0) IN (" + our_side + ")";
		vps += dbs.getScalarAsInt(sql_vps) * VPS_FOR_NON_ENEMY_UNIT;

		// VPs for dead target units, regardless of who killed them
		sql_vps = "SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + target_side + " AND Status = " + UnitsTable.ST_DEAD;
		vps += dbs.getScalarAsInt(sql_vps) * VPS_FOR_TARGET_UNIT;

		return vps;
	}


	@Override
	public boolean hasSpecialVPCalc() {
		// Override if req
		return true;
	}


	@Override
	public String getMission1Liner(int side) {
		return "Each player must kill all the units on a particular side.";
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_GRENADE && t != EquipmentTypesTable.ET_SMOKE_GRENADE && t != EquipmentTypesTable.ET_NERVE_GAS && t != EquipmentTypesTable.ET_ADRENALIN_SHOT;
		}
		return true;
	}


	@Override
	public boolean doWeCareWhoKilledUnits() {
		return true;
	}

}
