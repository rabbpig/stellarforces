package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitsTable;

public class ScorchedEarth extends AbstractMission {

	private static final int MAX_TURNS = 20;

	private static final String SIDE1_NAME = "Destruction Crew";
	private static final String SIDE2_NAME = "Terran Army";

	public ScorchedEarth() {
		super(AbstractMission.SCORCHED_EARTH, -1, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 450, 550, 2, AbstractMission.NO_CEILINGS, 0, 0, 1.69f, AbstractMission.WEAK_WALLS, AbstractMapModel.SLIM_WALLS, "scorched_earth.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_1, SPECIFIED_WIN_AT_END, "scorched_earth.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
		return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_ROCKET_LAUNCHER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_AUTOCANNON) == false;
		}
		return false;
	}


	public String getShortDesc() {
		return "An outpost must be levelled.";
	}


	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "Destroy all the walls in the centre of the map.";
		case 2:
			return "You must kill the destruction crew.";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public boolean hasSpecialVPCalc() {
		return true;
	}


	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		int vps = 0;
		if (side == 1) {
			int mapid = dbs.getScalarAsInt("SELECT MapDataID FROM Games WHERE GameID = " + game.getID());
			int c = dbs.getScalarAsInt("SELECT Count(*) FROM MapDataSquares WHERE MapDataID = " + mapid + " AND SquareType = " + MapDataTable.MT_WALL + " AND COALESCE(Destroyed, 0) = 0 AND MapX BETWEEN 9 AND 40 AND MapY BETWEEN 7 AND 31");
			if (c == 0) {
				vps = 100;
			}
		} else { // Side 2
			AbstractMission mission = AbstractMission.Factory(game.getMissionID());
			String sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + game.getID() + " AND Side IN (" + game.getOpponentSidesForSideAsCSV(side, mission) + ") AND Status = " + UnitsTable.ST_DEAD;
			vps = dbs.getScalarAsInt(sql_vps);
		}
		return vps;
	}


	
}
