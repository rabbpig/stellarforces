package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;

public class ATaleOfTwoBasesMission extends AbstractMission {

	private static final int MAX_TURNS = 30;

	private static final String SIDE1_NAME = "Laser Platoon";
	private static final String SIDE2_NAME = "Star Federation";

	public ATaleOfTwoBasesMission() {
		super(AbstractMission.TALE_OF_TWO_BASES, CP_SHADOW_CONSPIRACY, SIDE1_NAME, SIDE2_NAME, MAX_TURNS, 320, 320, -1, AbstractMission.SHOW_CEILINGS, 100, 100, 0.45f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "a_tale_of_two_bases.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "ataleoftwobases.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "Each player is in control of a moonbase, and they must use their squad to defend it, but also destroy the computer in the middle of their opponent's moonbase.";
	}


	public String getMission1Liner(int side) {
		return "You must destroy your opponent's computer";
	}

	
	/*public int canBePlayedOnAndroid() {
		return ANY_CLIENT;
	}*/


	@Override
	public boolean doWeCareWhoOwnsTheComputers() {
		return true;
	}


	@Override
	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}


}
