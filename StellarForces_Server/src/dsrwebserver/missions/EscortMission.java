package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;

public class EscortMission extends AbstractMission {

	private static final int MAX_TURNS = 20;

	public EscortMission() {
		super(AbstractMission.ESCORT, -1, "Laser Squad", "Globex Megacorp", MAX_TURNS, 320, 350, 1, AbstractMission.SHOW_CEILINGS, 0, 0, 1.58f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "escort.txt", "escort_campaign.txt", IS_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "escort.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 2;
	}

	
	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Assassins";
		case 2:
			return "The Fugitives";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "Sterner Regnix must make it to his escape pod on the other side of the complex.  One player controls Sterner and his gang, the other player controls the assassins who must prevent him from getting to the other side.";
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill Sterner Regnix";
		case 2:
			return "Sterner Regnix must escape";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}



}
