package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;
import dsrwebserver.tables.EquipmentTypesTable;

/**
 * 17/6/13 - Reduced side 2 creds from 320 to 280
 * 23/12/13 - Added extra superhuman, reduced side 2 creds to 250
 * 20/2/14 - Attackers have 250 -> 220 creds.
 */
public class SuperhumanDefendTheBase extends AbstractMission {

	private static final int MAX_TURNS = 20;

	public SuperhumanDefendTheBase() {
		super(AbstractMission.SUPERHUMAN_DEFEND_THE_BASE, CP_SHADOW_CONSPIRACY, "The Superhuman", "Marsec Corp", MAX_TURNS, 160, 220, 2, AbstractMission.SHOW_CEILINGS, 100, 0, 1.58f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "superhuman_defend_the_base.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_NONE, SPECIFIED_WIN_AT_END, "defendthebase.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Superhumans";
		case 2:
			return "The Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "One player must attack the base and destroy the computer with their small squad of Superhumans.  The other player must defend the base.";
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			return t != EquipmentTypesTable.ET_GRENADE && t != EquipmentTypesTable.ET_NERVE_GAS;
		}
		return true;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_DEATHGRENADE) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_ROCKET_LAUNCHER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_AUTOCANNON) == false; // It's too easy otherwise
		}
		return false;
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must destroy the computer";
		case 2:
			return "You must prevent the computer being destroyed";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}



	@Override
	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}


}
