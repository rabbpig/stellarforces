package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;

public class AngelsOfDeath extends AbstractMission {

	private static final int MAX_TURNS = 20;

	public AngelsOfDeath() {
		super(AbstractMission.ANGELS_OF_DEATH, -1, "Angels of Death", "Sterner Regnix", MAX_TURNS, 0, 50, 1, AbstractMission.SHOW_CEILINGS, 0, 0, 1.58f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "angels_of_death.txt", "", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, SPECIFIED_WIN_AT_END, "angels_of_death.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 2;
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "Angels of Death";
		case 2:
			return "The Fugitives";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public String getShortDesc() {
		return "Sterner Regnix must make it to his escape pod on the other side of the complex whilst avoiding the Angels of Death";
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill Sterner Regnix";
		case 2:
			return "Sterner Regnix must escape";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		super.createMap(dbs, gameid);

		this.addRandomSmoke(dbs, gameid, 50);

	}


	@Override
	public void nextPhase(MySQLConnection dbs, GamesTable game) throws SQLException {
		super.nextPhase(dbs, game);

		this.addRandomSmoke(dbs, game.getID(), 15);
	}


	private void addRandomSmoke(MySQLConnection dbs, int gameid, int amt) throws SQLException {
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);

		try {
			for (int i=0 ; i<amt ; i++) {
				int x = Functions.rnd(1, 59);
				int y = Functions.rnd(1, 59);
				while (MapDataTable.GetMapSquareType(dbs, game.getMapDataID(), x, y) != MapDataTable.MT_FLOOR) {
					x = Functions.rnd(1, 59);
					y = Functions.rnd(1, 59);
				}
				dbs.runSQLUpdate("UPDATE MapDataSquares SET SmokeType = " + EquipmentTypesTable.ET_SMOKE_GRENADE + ", SmokeTurnCount = " + (game.getNumOfSides()-1) + " WHERE MapDataID = " + game.getMapDataID() + " AND MapX = " + x + " AND MapY = " + y);
				UnitHistoryTable.AddRecord_Smoke(dbs, game, x, y, System.currentTimeMillis(), true);
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

		game.close();

	}
}
