package dsrwebserver.missions;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.maps.MapData;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;

public class FlagCollector extends AbstractMission {
	
	private static final int MAX_TURNS = 20;
	private static final int VPS_PER_FLAG = 49;
	private static final int CREDS = 180;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "Colonial Marines";
	private static final String SIDE3_NAME = "Marsec Corp";
	private static final String SIDE4_NAME = "Rebel Squad";

	public FlagCollector() {
		super(AbstractMission.FLAG_COLLECTOR, -1, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, CREDS, CREDS, CREDS, CREDS, -1, AbstractMission.SHOW_CEILINGS, 0, 0, 0, 0, 1.4f, AbstractMission.STRONG_WALLS, AbstractMapModel.SLIM_WALLS, "flag_collector.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, HIGHEST_VPS_WIN_AT_END, "flag_collector.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		case 4:
			return SIDE4_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "Each side must collect the most F.L.A.G.s. from the centre of the moonbase and take them back to where they started.  Whoever has the most at the end is the winner.";
	}

	
	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		MapData map = super.createBasicMap(dbs, gameid);

		// Add flags
		AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_FLAG, 17, 17, -1, -1);
		AddEquipmentToMap(dbs, gameid, map, EquipmentTypesTable.CD_FLAG, 18, 18, -1, -1);
	}


	public boolean hasSpecialVPCalc() {
		return true;
	}


	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		int vps = 0;
		int mapid = dbs.getScalarAsInt("SELECT MapDataID FROM Games WHERE GameID = " + game.getID());
		ResultSet rs = dbs.getResultSet("SELECT * FROM MapDataSquares WHERE MapDataID = " + mapid + " AND OwnerSide = " + side);
		while (rs.next()) {
			String sql = "SELECT EquipmentID FROM Equipment ";
			sql = sql + " INNER JOIN EquipmentTypes ON Equipment.EquipmentTypeID = EquipmentTypes.EquipmentTypeID ";
			sql = sql + " WHERE GameID = " + game.getID() + " AND UnitID <= 0 AND MapX = " + rs.getInt("MapX") + " AND MapY = " + rs.getInt("MapY") + " AND MajorTypeID = " + EquipmentTypesTable.ET_FLAG + " LIMIT 1";
			ResultSet rs_equip = dbs.getResultSet(sql);
			if (rs_equip.next()) {
				vps += VPS_PER_FLAG;
			}
		}
		return vps;
	}


	public String getMission1Liner(int side) {
		return "You must collect as many F.L.A.G.s as possible";
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
		return t != EquipmentTypesTable.ET_DEATH_GRENADE;
		}
		return true;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
		return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_ROCKET_LAUNCHER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_AUTOCANNON) == false;
		}
		return false;
	}



}

