package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.missions.ai.MoonbaseAssaultAI;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;

/**
 * 22/8/14 - Reduced side 2 creds from 380 to 340
 * 
 */
public class MoonbaseAssault_3Player extends AbstractMission {
	
	private static final int MAX_TURNS = 25;

	public MoonbaseAssault_3Player() {
		super(AbstractMission.MOONBASE_ASSAULT_3P, CP_OMNI_CORP, 3, "Laser Squad", "Marsec Corp", "Globex Inc.", "", MAX_TURNS, 240, 340, 240, -1, 2, AbstractMission.SHOW_CEILINGS, 10, 0, 10, -1, 1.27f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "moonbase_assault_3p.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, SPECIFIED_WIN_AT_END, "3p_moonbaseassault.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Attackers";
		case 2:
			return "The Defenders";
		case 3:
			return "More Attackers";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public String getShortDesc() {
		return "Computers must be defended or destroyed.";
	}


	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
		case 3:
			return "You must destroy the computers";
		case 2:
			return "You must prevent the computers being destroyed";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public void mapCreated(MySQLConnection dbs, int gameid) throws SQLException {
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);

		if (game.getAIForSide(2) > 0) { // Has AI been chosen?
			MoonbaseAssaultAI.CreateAI(dbs, game);
		}
	}


	@Override
	public int doesSideHaveAI(int side) {
		if (side == 2) {
			return 1;
		}
		return 0;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			if (side == 2) {
				return code.equalsIgnoreCase(EquipmentTypesTable.CD_INCENDIARY_GRENADE) == false; // It's too easy otherwise
			} else {
				return true;
			}
		}
		return false;
	}


}
