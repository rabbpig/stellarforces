package dsrwebserver.missions;

import ssmith.util.MyList;
import dsr.models.map.AbstractMapModel;

/**
 * 7/10/13 - Attackers now have only 175 creds down from 190.
 */
public class MeltdownMission_4P extends AbstractMission {
	
	private static final int MAX_TURNS = 35;
	private static final String SIDE1_NAME = "Laser Squad 1";
	private static final String SIDE2_NAME = "Laser Squad 2";
	private static final String SIDE3_NAME = "Marsec Corporation 1";
	private static final String SIDE4_NAME = "Marsec Corporation 2";

	public MeltdownMission_4P() {
		super(AbstractMission.MELTDOWN_4P, CP_SHADOW_CONSPIRACY, 4, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, SIDE4_NAME, MAX_TURNS, 175, 175, 140, 140, 2, AbstractMission.SHOW_CEILINGS, 25, 25, 0, 0, 1.45f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.SLIM_WALLS, "meltdown.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_LEVEL_2, false, SPECIFIED_WIN_AT_END, "meltdown_4p.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
		case 2:
			return "The Saboteurs";
		case 3:
		case 4:
			return "The Defenders";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "Highly explosive computers must be defended or destroyed.  Two players must destroy the computers and the other two players must defend them.  Warning: when the computers explode, they will destroy everything close by!";
	}

	
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
		case 2:
			return "You must destroy the computers";
		case 3:
		case 4:
			return "You must prevent the computers being destroyed";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public MyList<Integer> getSidesForSide(int side) {
		switch (side) {
		case 1:
		case 2:
			return MyList.CreateIntsFromInts(1,2);
		case 3:
		case 4:
			return MyList.CreateIntsFromInts(3,4);
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}
	

	@Override
	public boolean doWeCareWhoKilledUnits() {
		return false;
	}


	@Override
	public boolean doWeCareWhoDestroyedComputers() {
		return false;
	}


	@Override
	public boolean addComradesVPs(int side) {
		return false;//side == 1 || side == 2;
	}


}
