package dsrwebserver.missions;

import dsr.models.map.AbstractMapModel;

public class TheExterminatorsMission extends AbstractMission {

	private static final int MAX_TURNS = 20; // LS is 20

	/**
	 *  15/7/13 - Reduced side 1 creds from 460 to 420
	 */
	public TheExterminatorsMission() {
		super(AbstractMission.THE_EXTERMINATORS, CP_STERNER_REGNIX, "Laser Squad", "Globex Megacorp", MAX_TURNS, 420, 270, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 1.24f, AbstractMission.WEAK_WALLS, AbstractMapModel.SLIM_WALLS, "the_exterminators.txt", "", IS_CAMPAIGN, AbstractMission.GR_LEVEL_1, SPECIFIED_WIN_AT_END, "theexterminators.csv");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Exterminators";
		case 2:
			return "The Fugitives";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	@Override
	public String getShortDesc() {
		return "Sterner Regnix must be killed!  One player controls the assassins, and the other player controls Sterner and his bodyguards.";
	}

	
	@Override
	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must kill Sterner Regnix";
		case 2:
			return "You must kill the assassins";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


}
