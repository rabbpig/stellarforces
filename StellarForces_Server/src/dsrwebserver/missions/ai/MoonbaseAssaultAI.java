package dsrwebserver.missions.ai;

import java.awt.Point;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public class MoonbaseAssaultAI {

	public static void CreateAI(MySQLConnection dbs, GamesTable game) throws SQLException {
		if (game.getAIForSide(2) > 0) { // Has AI been chosen?
			dbs.runSQLUpdate("UPDATE Units SET AIType = 1 WHERE GameID = " + game.getID() + " AND Side = 2");

			// Equip 
			UnitsTable unit = new UnitsTable(dbs);
			ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE GameID = " + game.getID() + " AND AIType > 0");
			while (rs.next()) {
				unit.selectRow(rs.getInt("UnitID"));
				unit.setArmourType(2);
				int equipment_type_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SP30);
				int id = EquipmentTable.CreateEquipment(dbs, game.getID(), rs.getInt("UnitID"), equipment_type_id, 20);
				unit.setCurrentEquipmentID(id);
			}
			rs.close();

			game.setSideHasEquipped(2);

			// Deploy side 2
			ArrayList<Point> other_squares = new ArrayList<Point>();
			other_squares.add(new Point(13,19));
			other_squares.add(new Point(5,48));
			other_squares.add(new Point(37,16));
			other_squares.add(new Point(17,36));
			other_squares.add(new Point(46,36));
			other_squares.add(new Point(31,27));
			other_squares.add(new Point(49,19));
			other_squares.add(new Point(26,44));
			other_squares.add(new Point(34,44));
			other_squares.add(new Point(34,52));
			other_squares.add(new Point(59,45));
			other_squares.add(new Point(31,19));
			other_squares.add(new Point(34,36));
			other_squares.add(new Point(26,36));
			other_squares.add(new Point(26,50));
			other_squares.add(new Point(38,47));
			other_squares.add(new Point(10,19));
			other_squares.add(new Point(42,36));
			other_squares.add(new Point(21,48));
			other_squares.add(new Point(31,40));
			other_squares.add(new Point(31,34));

			rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE Status = " + UnitsTable.ST_AWAITING_DEPLOYMENT + " AND GameID = " + game.getID() + " AND AIType > 0 AND Side = 2");
			while (rs.next()) {
				Point p = other_squares.remove(Functions.rnd(0, other_squares.size()-1));
				try {
					if (MapDataTable.GetMapSquareType(dbs, game.getMapDataID(), p.x, p.y) == MapDataTable.MT_FLOOR) {
						dbs.runSQLUpdate("UPDATE Units SET Status = " + UnitsTable.ST_DEPLOYED + ", MapX = " + p.x + ", MapY = " + p.y + " WHERE UnitID = " + rs.getInt("UnitID"));
						try {
							// Add event
							int seen_by_side[] = {0, 0, 0, 0, 0};
							seen_by_side[unit.getSide()] = 1; // Only owner side knows about it
							UnitHistoryTable.AddRecord_UnitDeployed(dbs, game, unit, seen_by_side, System.currentTimeMillis());
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
					} else {
						throw new RuntimeException("Map " + p.x + "/" + p.y + " is a wall in MA!");
					}
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
			}

			game.setSideHasDeployed(2, System.currentTimeMillis()); // This must be after they have actually been deployed!

			// Give them APs
			dbs.runSQLUpdate("UPDATE Units SET CurrentAPs = MaxAPs WHERE GameID = " + game.getID() + " AND AIType > 0");
		}

	}


}
