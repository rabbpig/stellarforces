package dsrwebserver.missions;

import java.awt.Point;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.MapData;
import dsrwebserver.maps.MapLoader;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public class RescueFromTheMines extends AbstractMission {

	private static final int MAX_TURNS = 30;

	public RescueFromTheMines() {
		super(AbstractMission.RESCUE_FROM_THE_MINES, CP_SHADOW_CONSPIRACY, "Laser Squad", "Marsec Corp", MAX_TURNS, 320, 260, 2, AbstractMission.SHOW_CEILINGS, 0, 0, 0.54f, AbstractMission.WEAK_WALLS, AbstractMapModel.BLOCK_WALLS, "rescue_from_the_mines.txt", "rescue_from_the_mines_campaign.txt", IS_CAMPAIGN, AbstractMission.GR_LEVEL_1, SPECIFIED_WIN_AT_END, "rescuefromthemines.csv");
	}


	@Override
	public boolean canUnitsEscape(int side) {
		return side == 1;
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return "The Hostages";
		case 2:
			return "The Prison Guards";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	public String getShortDesc() {
		return "3 Hostages must be rescued by one player from the mine complex.  They must remember to take some explosives with them to break open the prison walls.  The other player must prevent them escaping.";
	}


	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws Exception {
		MapData map = MapLoader.Import(dbs, gameid, this.map_filename, true);

		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);

		// Deploy prisoners
		ArrayList<Point> prison_squares = map.random_deploy_squares;
		while (true) {
			ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE Status = " + UnitsTable.ST_AWAITING_DEPLOYMENT + " AND GameID = " + gameid + " AND CanDeploy = 0 LIMIT 1");
			if (rs.next()) {
				int rnd = Functions.rnd(0, prison_squares.size()-1);
				Point p = prison_squares.get(rnd);
				prison_squares.remove(rnd);
				dbs.runSQLUpdate("UPDATE Units SET Status = " + UnitsTable.ST_DEPLOYED + ", MapX = " + p.x + ", MapY = " + p.y + " WHERE UnitID = " + rs.getInt("UnitID"));

				try {
					// Add event
					UnitsTable unit = new UnitsTable(dbs);
					unit.selectRow(rs.getInt("UnitID"));
					int seen_by_side[] = {0, 0, 0, 0, 0};
					seen_by_side[unit.getSide()] = 1; // Only owner side knows about it
					UnitHistoryTable.AddRecord_UnitDeployed(dbs, game, unit, seen_by_side, System.currentTimeMillis());
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
			} else {
				break;
			}
		}

		super.storeMapDataInSQL(dbs, map);
	}


	public String getMission1Liner(int side) {
		switch (side) {
		case 1:
			return "You must rescue the hostages";
		case 2:
			return "You must kill the rescuers";
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	@Override
	public boolean isItemTypeAllowed(int t, int side) {
		if (super.isItemTypeAllowed(t, side)) {
			if (side == 2) {
				return t != EquipmentTypesTable.ET_GRENADE;
			}
		}
		return true;
	}


	@Override
	public boolean isItemAllowed(String code, int side) {
		if (super.isItemAllowed(code, side)) {
			if (side == 2) {
				return code.equalsIgnoreCase(EquipmentTypesTable.CD_BLASTER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_ROCKET_LAUNCHER) == false && code.equalsIgnoreCase(EquipmentTypesTable.CD_AUTOCANNON) == false;
			} else {
				return true;
			}
		}
		return false;
	}


	@Override
	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		int vps = super.getVPs(dbs, game, side);
		try {
			if (side == 1) {
				// See if the enemy has been killed
				if (dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = 2 AND Status = " + UnitsTable.ST_DEPLOYED) == 0) {
					vps = 100;
				}
			} else if (side == 2) {
				// Do nothing
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
		return vps;
	}



}
