package dsrwebserver.missions;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsr.models.map.AbstractMapModel;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.LastManStandingMapGen;
import dsrwebserver.maps.MapData;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public class ThreePlayerLastManStanding extends AbstractMission {
	
	private static final int MAX_TURNS = 25;
	private static final int SIZE = 20;
	private static final int ROOMS = 7;
	private static final int CREDS = 170;

	private static final String SIDE1_NAME = "Laser Squad";
	private static final String SIDE2_NAME = "The Omni Corp.";
	private static final String SIDE3_NAME = "Marsec Corp.";

	public ThreePlayerLastManStanding() {
		super(AbstractMission.THREE_PLAYER_LAST_MAN_STANDING, CP_AZARIAN_CRUSADE, 3, SIDE1_NAME, SIDE2_NAME, SIDE3_NAME, "", MAX_TURNS, CREDS, CREDS, CREDS, -1, -1, false, 0, 0, 0, 0, 1.14f, AbstractMission.INDESTRUCTABLE_WALLS, AbstractMapModel.BLOCK_WALLS, "3-player_last_man_standing.txt", IS_NOT_CAMPAIGN, AbstractMission.GR_MULTI_PLAYER, IS_NOT_SNAFU, SPECIFIED_WIN_AT_END, "");
	}


	@Override
	public String getSideDescription(int side) {
		switch (side) {
		case 1:
			return SIDE1_NAME;
		case 2:
			return SIDE2_NAME;
		case 3:
			return SIDE3_NAME;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}

	
	public String getShortDesc() {
		return "Three teams battle it out on a remote space station.";
	}

	
	@Override
	public void createMap(MySQLConnection dbs, int gameid) throws SQLException {
		MapData map = new LastManStandingMapGen(SIZE, ROOMS, 3, false);
		super.storeMapDataInSQL(dbs, map);
	}

	
	@Override
	public String getMission1Liner(int side) {
		return "You must kill all your opponents units";
	}


	public int getVPs(MySQLConnection dbs, GamesTable game, int side) throws SQLException {
		try {
			// Check we have some units left
			String sql = "SELECT COUNT(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED;
			if (dbs.getScalarAsInt(sql) <= 0) {
				return 0;
			}
			// Calc from units killed
			String sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + game.getID() + " AND Side <> " + side + " AND Status = " + UnitsTable.ST_DEAD; // NOT IN (" + game.getOpponentSidesForSideAsCSV(side, mission) + ")
			sql_vps = sql_vps + " AND COALESCE(KilledBySide, 0) IN (0, " + side + ")";
			int vps = dbs.getScalarAsInt(sql_vps);

			return vps;
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
			return 0;
		}
	}


	@Override
	public boolean hasSpecialVPCalc() {
		return true;
	}


	@Override
	public boolean doWeCareWhoKilledUnits() {
		return true;
	}



}
