package dsrwebserver;

import java.util.Date;

import ssmith.mail.SmtpMail;

public class ShutdownHook extends Thread {

	private String mail_server;
	private String address;

	public ShutdownHook(String _mail_server, String _address) {
		mail_server = _mail_server;
		address = _address;

	}

	public void run() {
		if (mail_server.length() > 0) {
			SmtpMail mail = new SmtpMail(); // Create new instance each time just in case!
			mail.debugMode(false);
			DSRWebServer.p("Sending email sent to " + address);
			try {
				//mail.simpleSend(mail_server, address, address, DSRWebServer.TITLE + " Shutting Down", "");
				DSRWebServer.SendEmailToAdmin("Server Shutting Down", "The " + DSRWebServer.TITLE + " web server has shut down at " + new Date().toString());
				DSRWebServer.p("Email sent to " + address);
			} catch (Exception e) {
				e.printStackTrace();
				DSRWebServer.p("Email NOT sent to " + address);
			}
		}

	}

}
