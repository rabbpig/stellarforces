package dsrwebserver;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.ResultSet;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Dates;
import ssmith.lang.Functions;
import ssmith.mail.SmtpMail;
import dsrwebserver.tables.EmailsTable;

public class SendEmailsThread extends Thread {

	private static final boolean USE_SERVICE = true;

	private String mail_server, mail_service;
	private MySQLConnection dbs;

	public SendEmailsThread(MySQLConnection database, String _mail_server, String _mail_service) {
		super("SendEmailsThread");

		this.setDaemon(true);
		this.setPriority(Thread.NORM_PRIORITY-1);
		dbs = database;
		mail_server =_mail_server;
		mail_service = _mail_service;

		if (USE_SERVICE == false) {
			if (mail_server.length() == 0) {
				DSRWebServer.p("WARNING: Not sending emails!");
			}
		} else {
			if (mail_service.length() == 0) {
				DSRWebServer.p("WARNING: Not sending emails!");
			}
		}
	}

	public void run() {
		//if (mail_server.length() > 0) {
		DSRWebServer.p("Checking for emails...");
		try {
			while (true) {
				ResultSet rs = dbs.getResultSet("SELECT * FROM Emails WHERE COALESCE(Sent, 0) = 0 AND Attempts < 10 ORDER BY DateCreated");
				while (rs.next()) {
					try {
						EmailsTable.IncAttempts(dbs, rs.getInt("EmailID"));
						DSRWebServer.p("Sending email sent to " + rs.getString("ToAddress"));
						if (USE_SERVICE == false) {
							SmtpMail mail = new SmtpMail(); // Create new instance each time just in case!
							mail.debugMode(false);
							mail.simpleSend(mail_server, rs.getString("FromAddress"), rs.getString("ToAddress"), rs.getString("Subject"), rs.getString("Body"));
						} else {
							DSRWebServer.p("Sending email via service to " + rs.getString("ToAddress"));
							URL url = new URL(mail_service + "?cmd=send&from=" + URLEncoder.encode(rs.getString("FromAddress"), "UTF-8") + "&to=" + URLEncoder.encode(rs.getString("ToAddress"), "UTF-8") + "&subject=" + URLEncoder.encode(rs.getString("Subject"), "UTF-8") + "&msg=" + URLEncoder.encode(rs.getString("Body"), "UTF-8"));
							HttpURLConnection conn = (HttpURLConnection)url.openConnection();
							conn.setReadTimeout((int)Dates.MINUTE*5);
							if (conn.getResponseCode() == 200) {
								DSRWebServer.p("Email sent");
							} else {
								throw new IOException("Error sending email - got response " + conn.getResponseCode());
							}
						}
						EmailsTable.MarkAsSent(dbs, rs.getInt("EmailID"));
						DSRWebServer.p("Email sent to " + rs.getString("ToAddress"));
						int c = dbs.getScalarAsInt("SELECT Count(*) FROM Emails WHERE COALESCE(Sent, 0) = 0 ORDER BY DateCreated");
						DSRWebServer.p(c + " emails remaining.");
					} catch (Exception ex) {
						ex.printStackTrace();
						DSRWebServer.p("** Email NOT sent to " + rs.getString("ToAddress") + "**");
						//mail.simpleSend(mail_server, 25, admin_email, admin_email, "Error from AutoEmail", Functions.Exception2String(ex));
						Functions.delay(Dates.MINUTE * 2);
					}
					Functions.delay(Dates.MINUTE);
				}
				rs.close();
				Functions.delay(Dates.MINUTE * 2);
			}

		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
			new SendEmailsThread(dbs, mail_server, mail_service).start(); // Start another!
		}
		//}
	}

}
