/*
 * Copyright (c) 2009-2016 Stephen Carlyle-Smith (stephen.carlylesmith@googlemail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * * Neither the name of 'jMonkeyEngine' nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dsrwebserver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Hashtable;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.io.IOFunctions;
import ssmith.lang.Dates;
import ssmith.lang.Functions;
import dsr.SharedStatics;
import dsr.comms.AbstractCommFuncs;
import dsrwebserver.pages.dsr.admin.createdata;
import dsrwebserver.tables.ArmourTypesTable;
import dsrwebserver.tables.CampUnitsTable;
import dsrwebserver.tables.CampaignLogTable;
import dsrwebserver.tables.ChatTable;
import dsrwebserver.tables.ClientErrorsTable;
import dsrwebserver.tables.ContractsTable;
import dsrwebserver.tables.EmailsTable;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.FactionsTable;
import dsrwebserver.tables.ForumPostingsTable;
import dsrwebserver.tables.ForumSubscriptionsTable;
import dsrwebserver.tables.ForumTopicsTable;
import dsrwebserver.tables.GameLogTable;
import dsrwebserver.tables.GameRequestsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LeagueTableTable;
import dsrwebserver.tables.LoginLogTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.MessagesTable;
import dsrwebserver.tables.MissionStatsTable;
import dsrwebserver.tables.OppFireSelectionTable;
import dsrwebserver.tables.PlayerAwardsTable;
import dsrwebserver.tables.PlayerTrophiesTable;
import dsrwebserver.tables.RequestedPlayersTable;
import dsrwebserver.tables.ReservePlayersTable;
import dsrwebserver.tables.SessionsTable;
import dsrwebserver.tables.StarmapLogTable;
import dsrwebserver.tables.StarmapSquaresTable;
import dsrwebserver.tables.StaticDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitNamesTable;
import dsrwebserver.tables.UnitsTable;
import dsrwebserver.tables.UniverseLogTable;
import dsrwebserver.tables.UniversePlayersEquipmentTable;
import dsrwebserver.tables.UniverseSquaresTable;
import dsrwebserver.tables.UniverseTreatiesTable;
import dsrwebserver.tables.UniverseUnitsTable;
import dsrwebserver.tables.UploadedMapsTable;
import dsrwebserver.tables.VisibleEnemiesTable;
import dsrwebserver.tables.WebsiteEventsTable;

public final class DSRWebServer {

	public static final boolean DEBUG = false;

	public static final int CURR_FULL_APP_VERSION = 125; // todo - update
	public static final int CURR_FREE_APP_VERSION = 122; // todo - update
	public static final boolean VERBOSE = false;
	public static final String IMAGES_HOST = ""; //"http://www.carlylesmith.karoo.net/stellarforces";

	// These settings are changed depending on the game mode
	public static String TITLE;// = "Stellar Forces";
	public static String WEBSERVER_NAME;// = "Stellar Forces Web Server " + AppletMain.VERSION;
	public static String WEBSITE_URL;
	private static String EMAIL_FOOTER;// = "\n\n--\n" + AppletMain.TITLE + " (" + AppletMain.WEBSITE_LINK + ")";
	public static String NEW_FORUM_LINK;// = "http://forums.stellarforces.com"; // DON'T ADD SLASH!
	public static String OLD_FORUM_LINK;// = "/dsr/forums/ForumMainPage.cls";
	public static String WIKI_LINK;// = "http://wiki.stellarforces.com"; // Do NOT add a slash to the end
	public static String INSTRUCTIONS_LINK;// = WIKI_LINK + "/doku.php?id=gameplay:start";
	public static String SUPPORT_TEXT;// = "For help &amp; support, please either <a href=\"mailto:" + AppletMain.SUPPORT_EMAIL_ADDRESS + "\">email</a> or <a href=\"" + DSRWebServer.NEW_FORUM_LINK + "\">visit the forums</a>.";
	public static String ANDROID_WEB_URL_FULL;// = "https://market.android.com/details?id=com.scs.stellarforces.main.full";
	public static String ANDROID_WEB_URL_FREE;// = "https://market.android.com/details?id=com.scs.stellarforces.main.lite";
	public static String ANDROID_MARKET_URL;// = "market://details?id=com.scs.stellarforces.main.full";
	public static final String SERVER_DATA_DIR = "./webroot/serverdata/";
	public static String EMAIL_ADDRESS;// = "StellarForces@stellarforces.com";
	public static String NOREPLY_EMAIL_ADDRESS;// = "StellarForces@stellarforces.com";
	public static final String UPLOADED_MAPS_DIR = SERVER_DATA_DIR + "uploadedmaps/";


	public static final int DAYS_BETWEEN_AWARDS = 21;
	public static final int STARMAP_DEFEND_DAYS = 7;
	public static final int MIN_CAMP_GAMES = 4;
	public static final int MIN_CAMP_UNITS = 15;
	public static final int MAX_CAMP_UNITS = 28;
	public static final int GAMES_FORUM_ID = 3;
	public static final boolean ALLOW_GAME_REQ_DELETE = true;
	public static final boolean ALLOW_ANON_POSTING = false;

	private static final String AI_NAME = "AI";
	private static final String ADMIN_NAME = "Admin";
	public static int SS_LOGIN_ID, ADMIN_LOGIN_ID = -1, AI_LOGIN_ID;  

	public static final String SS_LOGIN = "SteveSmith";
	public static final int EARLIEST_ACTIVATION_TURN = 4;

	public static String WEB_CACHE;// = "./webroot/serverdata/web_cache/";
	public static String PLAYBACK_CACHE;// = "./webroot/serverdata/playback_cache/";
	public static String MAP_IMAGE_CACHE;// = "/serverdata/mapimage_cache/";

	public static final String CRLF = "\r\n";
	private static final int MAX_SOCKETS = 2000;

	public static Hashtable<String, String> cfgfile;
	public static ContentTypes ctypes;
	public static ServerListener server;

	public static volatile MySQLConnection dbs, phpdbs;

	public DSRWebServer(String cfg) throws FileNotFoundException, IOException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
		super();

		PLAYBACK_CACHE = System.getProperty("java.io.tmpdir") + "/sf_playback_cache";
		p("Temp dir = " + PLAYBACK_CACHE);
		File tmp = new File(PLAYBACK_CACHE);
		tmp.mkdirs();
		if (tmp.exists() == false) {
			p("Cannot create temp dir " + PLAYBACK_CACHE);
		}
		WEB_CACHE = System.getProperty("java.io.tmpdir") + "/sf_web_cache";
		p("Temp dir = " + WEB_CACHE);
		tmp = new File(WEB_CACHE);
		tmp.mkdirs();
		if (tmp.exists() == false) {
			p("Cannot create temp dir " + WEB_CACHE);
		}
		MAP_IMAGE_CACHE = System.getProperty("java.io.tmpdir") + "/sf_map_image_cache";
		p("Temp dir = " + MAP_IMAGE_CACHE);
		tmp = new File(MAP_IMAGE_CACHE);
		tmp.mkdirs();
		if (tmp.exists() == false) {
			p("Cannot create temp dir " + MAP_IMAGE_CACHE);
		}

		p("Reading config file " + cfg);
		cfgfile = Functions.ConfigFile("./config/" + cfg, "=");
		ctypes = new ContentTypes();

		TITLE = "Stellar Forces";
		WEBSITE_URL = "http://www.stellarforces.com";
		EMAIL_ADDRESS = "StellarForces@stellarforces.com";
		NOREPLY_EMAIL_ADDRESS = "noreply@stellarforces.com";
		EMAIL_FOOTER = "\n\n--\n" + TITLE + " (" + WEBSITE_URL + ")";
		NEW_FORUM_LINK = "http://www.stellarforces.com/dsr/forums/ForumMainPage.cls";
		OLD_FORUM_LINK = "/dsr/forums/ForumMainPage.cls";
		WIKI_LINK = "http://wiki.stellarforces.com"; // Do NOT add a slash to the end
		INSTRUCTIONS_LINK = WIKI_LINK + "/doku.php?id=gameplay:start";
		SUPPORT_TEXT = "For help &amp; support, please <a href=\"" + DSRWebServer.NEW_FORUM_LINK + "\">visit the forums</a>.";
		ANDROID_WEB_URL_FULL = "https://market.android.com/details?id=com.scs.stellarforces.main.full";
		ANDROID_WEB_URL_FREE = "https://market.android.com/details?id=com.scs.stellarforces.main.lite";
		ANDROID_MARKET_URL = "market://details?id=com.scs.stellarforces.main.full";

		WEBSERVER_NAME = TITLE + " Web Server " + SharedStatics.VERSION;

		AbstractCommFuncs.GetCommFuncs(); // To check it loads
		
		try {
			IOFunctions.DeleteAllFilesInDir(WEB_CACHE);
			IOFunctions.DeleteAllFilesInDir(PLAYBACK_CACHE);
			IOFunctions.DeleteAllFilesInDir(MAP_IMAGE_CACHE);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		int port = Functions.ParseInt(cfgfile.get("port"));//_port;

		System.setProperty("line.separator", CRLF);

		dbs = new MySQLConnection();
		String server_ip = cfgfile.get("sql_server");
		String login = cfgfile.get("sql_login");
		String pwd = cfgfile.get("sql_pwd");
		dbs.connect(server_ip, cfgfile.get("sql_default_dbs"), login, pwd);
		if (dbs.doesDatabaseExist(cfgfile.get("database")) == false) {
			dbs.runSQL("CREATE DATABASE " + cfgfile.get("database"));
		}
		dbs.close();
		dbs.connect(server_ip, cfgfile.get("database"), login, pwd);
		this.createTables();

		SetAdminLogin(dbs);

		// Does equipment need importing
		if (dbs.getScalarAsInt("SELECT Count(*) FROM EquipmentTypes") == 0) {
			createdata.ImportData(dbs);
		}

		// Uncomment to Restart campaign
		//dbs.runSQLUpdate("DELETE FROM StarmapSquares");
		//StarmapSquaresTable.CreateStarmap(dbs, 8);

		// todo - comment this out
		/*GamesTable game = new GamesTable(dbs);
		game.selectRow(34835);
		game.recalcVPs();
		GameDataComms.CheckForWinner(dbs, game);
		 */

		//SQLCommandsTable.activated = true;

		//SendEmailToAdmin("Server started", "The web server has started." + (DEBUG?"IN DEBUG MODE!":""));

		//new SendEmailsThread(dbs, cfgfile.get("mail_server"), cfgfile.get("mail_service")).start();

		server = new ServerListener(port, MAX_SOCKETS);
		server.start();

		p(TITLE + " now listening for clients on port " + port);
		if (DEBUG) {
			p("** WARNING!  DEBUG MODE ON! **");
		}

		new StayConnected();
		new SendEmailsThread(dbs, cfgfile.get("mail_server"), cfgfile.get("mail_service")).start();

		SendEmailToAdmin("Server started", "The web server has started at " + new Date().toString() + (DEBUG ? " IN DEBUG MODE!":""));

		Runtime.getRuntime().addShutdownHook(new ShutdownHook(cfgfile.get("mail_server"), cfgfile.get("admin_email")));


	}


	private void createTables() throws SQLException {
		GameRequestsTable.CreateTable(dbs);
		EquipmentTable.CreateTable(dbs);
		EquipmentTypesTable.CreateTable(dbs);
		GamesTable.CreateTable(dbs);
		LoginsTable.CreateTable(dbs);
		SessionsTable.CreateTable(dbs);
		UnitsTable.CreateTable(dbs);
		MapDataTable.CreateTable(dbs);
		ArmourTypesTable.CreateTable(dbs);
		GameLogTable.CreateTable(dbs);
		ForumTopicsTable.CreateTable(dbs);
		ForumPostingsTable.CreateTable(dbs);
		VisibleEnemiesTable.CreateTable(dbs);
		UnitHistoryTable.CreateTable(dbs);
		MessagesTable.CreateTable(dbs);
		UnitNamesTable.CreateTable(dbs);
		ForumSubscriptionsTable.CreateTable(dbs);
		LoginLogTable.CreateTable(dbs);
		EmailsTable.CreateTable(dbs);
		CampUnitsTable.CreateTable(dbs);
		CampaignLogTable.CreateTable(dbs);
		OppFireSelectionTable.CreateTable(dbs);
		ClientErrorsTable.CreateTable(dbs);
		RequestedPlayersTable.CreateTable(dbs);
		PlayerTrophiesTable.CreateTable(dbs);
		FactionsTable.CreateTable(dbs);
		StaticDataTable.CreateTable(dbs);
		PlayerAwardsTable.CreateTable(dbs);
		MissionStatsTable.CreateTable(dbs);
		StarmapLogTable.CreateTable(dbs);
		UniverseSquaresTable.CreateTable(dbs);
		UniverseTreatiesTable.CreateTable(dbs);
		UniversePlayersEquipmentTable.CreateTable(dbs);
		UniverseUnitsTable.CreateTable(dbs);
		UniverseLogTable.CreateTable(dbs);
		ContractsTable.CreateTable(dbs);
		WebsiteEventsTable.CreateTable(dbs);
		ChatTable.CreateTable(dbs);
		LeagueTableTable.CreateTable(dbs);
		ReservePlayersTable.CreateTable(dbs);
		StarmapSquaresTable.CreateTable(dbs);
		UploadedMapsTable.CreateTable(dbs);

	}


	public static synchronized void HandleError(Exception ex) {
		HandleError(ex, "", true);
	}


	public static synchronized void HandleError(Exception ex, boolean log) {
		HandleError(ex, "", log);
	}


	public static synchronized void HandleError(Exception ex, String extra_details, boolean log) {
		if (ex instanceof ClientException == false) {
			System.err.println("Error at: " + new Date().toString());
			System.err.println("Message: " + ex.getMessage());
			if (extra_details.length() > 0) {
				System.err.println("Type: " + extra_details);
			}
			System.err.println("Type: " + ex.getClass().toString());
			ex.printStackTrace();

			/*if (log) {
				Functions.LogError(ex, ERROR_LOG);
			}*/

			if (ex instanceof java.net.SocketException == false) {
				// Email me the error
				try {
					//if (cfgfile.get("mail_server").length() > 0) {
					SendEmailToAdmin("Server Error on " + DSRWebServer.TITLE, "At: " + new Date().toString() + "\n\n" + ex.toString() + "\n\n" + Functions.Exception2String(ex, "\n"));
					// Send email directly!
					//SmtpMail mail = new SmtpMail(); // Create new instance each time just in case!
					//mail.debugMode(false);
					//mail.simpleSend(cfgfile.get("mail_server"), cfgfile.get("admin_email"), cfgfile.get("admin_email"), "Server Error on " + DSRWebServer.TITLE, "At: " + new Date().toString() + "\n\n" + ex.toString() + "\n\n" + Functions.Exception2String(ex, "\n"));
					//}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}


	public static void SendEmailToAdmin(String subject, String msg) {
		SendEmail(cfgfile.get("admin_email"), "(ADMIN) " + subject, msg);
	}


	public static void SendEmail(String to, String subject, String msg) {
		try {
			if (to.indexOf("@") > 0) {
				try {
					EmailsTable.AddRec(dbs, NOREPLY_EMAIL_ADDRESS, to, TITLE + ": " + subject, msg + EMAIL_FOOTER);
				} catch (Exception ex) {
					// Notice we don't send an email!
					ex.printStackTrace();
				}
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}
	}


	public static void p(Object o) {
		System.out.println(Dates.FormatDate(new Date(), Dates.UKDATE_FORMAT2_WITH_TIME) + ": " + o); 
	}


	/*public static void LogEquipping(int gameid, String s) {
		try {
			TextFile.QuickAppend(EQUIP_LOG + gameid + ".txt", Dates.FormatDate(new Date(), Dates.UKDATE_FORMAT2_WITH_TIME) + ": " + s, true, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/


	public static void LogCampUnits(String s) {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static MySQLConnection getPHPDbs() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		if (phpdbs == null) {
			if (dbs.doesDatabaseExist("phpBB")) {
				MySQLConnection tmp = new MySQLConnection();
				String server = DSRWebServer.cfgfile.get("sql_server");
				String login = DSRWebServer.cfgfile.get("sql_login");
				String pwd = DSRWebServer.cfgfile.get("sql_pwd");
				tmp.connect(server, "phpBB", login, pwd);
				phpdbs = tmp;
			}
		}
		return phpdbs;
	}


	public static void stopServer() {
		ServerListener.StopListening();
	}


	public static void main(String[] args) {
		try {
			if (args.length > 0) {
				new DSRWebServer(args[0]);//, Functions.ParseInt(args[0]));
			} else {
				//p("Usage: DSRWebServer <config file>");
				new DSRWebServer("config_sf_work.txt");
			}
		} catch (Exception e) {
			//HandleError(e, true);
			e.printStackTrace();
		}
	}


	public static void SetAdminLogin(MySQLConnection dbs) throws SQLException {
		// Create AI login
		if (dbs.getScalarAsInt("SELECT Count(*) FROM Logins WHERE DisplayName = " + SQLFuncs.s2sql(AI_NAME)) == 0) {
			LoginsTable login = new LoginsTable(dbs);
			login.createLogin(Functions.rnd(10000000, 99999999)+"", AI_NAME, "ai_"+EMAIL_ADDRESS, false);
			login.close();
		}
		AI_LOGIN_ID = dbs.getScalarAsInt("SELECT LoginID FROM Logins WHERE DisplayName = " + SQLFuncs.s2sql(AI_NAME));
		// Create Admin login
		if (dbs.getScalarAsInt("SELECT Count(*) FROM Logins WHERE DisplayName = " + SQLFuncs.s2sql(ADMIN_NAME)) == 0) {
			LoginsTable login = new LoginsTable(dbs);
			login.createLogin(Functions.rnd(10000000, 99999999)+"", ADMIN_NAME, NOREPLY_EMAIL_ADDRESS, false);
			login.close();
		}
		ADMIN_LOGIN_ID = dbs.getScalarAsInt("SELECT LoginID FROM Logins WHERE DisplayName = " + SQLFuncs.s2sql(ADMIN_NAME));

		if (SS_LOGIN_ID <= 0) {
			ResultSet rs = dbs.getResultSet("SELECT LoginID FROM Logins WHERE DisplayName = 'SteveSmith'");
			if (rs.next()) {
				SS_LOGIN_ID = rs.getInt("LoginID");
			} else {
				DSRWebServer.SendEmailToAdmin("SteveSmith login not found", "");
			}
			rs.close();
		}
	}

}
