package dsrwebserver.exceptions;

import java.io.IOException;

public class ClientIOException extends IOException {
	
	private static final long serialVersionUID = 1L;

	public ClientIOException(String s) {
		super(s);
	}

}
