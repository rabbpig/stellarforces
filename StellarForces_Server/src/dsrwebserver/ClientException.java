package dsrwebserver;

/**
 * This is for errors that shouldn't be emailed.
 *
 */
public final class ClientException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ClientException(String s) {
		super(s);
	}
	
}
