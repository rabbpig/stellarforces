package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

public final class ArmourTypesTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("ArmourTypes") == false) {
			dbs.runSQL("CREATE TABLE ArmourTypes (ArmourTypeID INTEGER AUTO_INCREMENT KEY, Name VARCHAR(64), Protection SMALLINT, Weight SMALLINT, Cost SMALLINT, RestrictedToWinners TINYINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}

	
	public static void AddRec(MySQLConnection dbs, String name, int protec, int weight, int cost, int restricted) throws SQLException {
		if (dbs.getScalarAsInt("SELECT Count(*) FROM ArmourTypes WHERE Name = " + SQLFuncs.s2sql(name)) == 0) {
			dbs.RunIdentityInsert("INSERT INTO ArmourTypes (Name, Protection, Weight, Cost, RestrictedToWinners) VALUES (" + SQLFuncs.s2sql(name) + ", " + protec + ", " + weight + ", " + cost + ", " + restricted + ")");
		} else {
			dbs.runSQLUpdate("UPDATE ArmourTypes SET Protection = " + protec + ", Weight = " + weight + ", Cost = " + cost + ", RestrictedToWinners = " + restricted + " WHERE Name = " + SQLFuncs.s2sql(name));
		}
	}
	
	
	public static int GetCost(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsInt("SELECT Cost FROM ArmourTypes WHERE ArmourTypeID = " + id);
	}
	
	
	public static int GetProtection(MySQLConnection dbs, int id) throws SQLException {
		if (id > 0) {
			return dbs.getScalarAsInt("SELECT Protection FROM ArmourTypes WHERE ArmourTypeID = " + id);
		} else {
			return 0;
		}
	}
	
	
	public static int GetWeight(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsInt("SELECT Weight FROM ArmourTypes WHERE ArmourTypeID = " + id);
	}
	
	
	/*public static boolean IsRestricted(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsInt("SELECT RestrictedToWinners FROM ArmourTypes WHERE ArmourTypeID = " + id) == 1;
	}*/
	
	
	public static String GetName(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsString("SELECT Name FROM ArmourTypes WHERE ArmourTypeID = " + id);
	}
	
}
