package dsrwebserver.tables;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsrwebserver.missions.AbstractMission;

public class StarmapSquaresTable {
	
	public static final int SIZE = 10;

	private static ArrayList<Integer> missions = new ArrayList<Integer>();

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("StarmapSquares") == false) {
			dbs.runSQL("CREATE TABLE StarmapSquares (StarmapSquareID INTEGER AUTO_INCREMENT KEY, MapX SMALLINT, MapY SMALLINT, FactionID INTEGER, MissionID INTEGER, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}

		if (dbs.doesIndexExist("StarmapSquares", "idx_StarmapSquares_MapX_MapY") == false) {
			dbs.runSQL("CREATE INDEX idx_StarmapSquares_MapX_MapY ON StarmapSquares (MapX, MapY)");
		}

		CreateStarmap(dbs);
	}


	public static int GetFactionID(MySQLConnection dbs, int x, int y) throws SQLException {
		return dbs.getScalarAsInt("SELECT FactionID FROM StarmapSquares WHERE MapX = " + x + " AND MapY = " + y + " UNION SELECT 0");
	}


	public static void SetFactionID(MySQLConnection dbs, int x, int y, int facid) throws SQLException {
		dbs.runSQLUpdate("UPDATE StarmapSquares SET FactionID = " + facid + " WHERE MapX = " + x + " AND MapY = " + y);
	}


	public static int GetMissionID(MySQLConnection dbs, int x, int y) throws SQLException {
		return dbs.getScalarAsInt("SELECT MissionID FROM StarmapSquares WHERE MapX = " + x + " AND MapY = " + y);
	}


	public static int GetNumFactionsUnderAttack(MySQLConnection dbs, int facid) throws SQLException {
		int c = 0;
		ResultSet rs = dbs.getResultSet("SELECT * FROM StarmapSquares WHERE FactionID = " + facid);
		while (rs.next()) {
			if (GameRequestsTable.GetAttackingFactionID(dbs, rs.getInt("MapX"), rs.getInt("MapY")) > 0) {
				c++;
			}
		}
		return c;
	}


	private static void CreateStarmap(MySQLConnection dbs) throws SQLException {
		int size = SIZE;
		if (dbs.getScalarAsInt("SELECT Count(*) FROM StarmapSquares") == 0) { // New game!
			dbs.runSQLUpdate("UPDATE Logins SET CampaignCredits = 0, FactionID = NULL");
			dbs.runSQLUpdate("UPDATE Games SET StarmapX = NULL, StarmapY = NULL, CampGame = 0, GameType = " + GameRequestsTable.GT_PRACTISE + " WHERE GameStatus < " + GamesTable.GS_FINISHED);
			//dbs.runSQLUpdate("UPDATE GameRequests SET StarmapX = null, StarmapY = null, CampGame = 0");
			dbs.runSQLUpdate("DELETE FROM GameRequests WHERE CampGame > 0 OR GameID NOT IN (SELECT GameID FROM Games)");
			dbs.runSQLUpdate("DELETE FROM CampUnits");
			dbs.runSQLUpdate("DELETE FROM CampaignLog");
			dbs.runSQLUpdate("DELETE FROM StarmapLog");
			dbs.runSQLUpdate("UPDATE Factions SET Sectors = " + ((size/2)*(size/2)));

			for (int y=0 ; y<size ; y++) {
				for (int x=0 ; x<size ; x++) {
					if (Functions.rnd(1, 5) > 1) { // Don't create map squares for all sectors
						if (dbs.getScalarAsInt("SELECT Count(*) FROM StarmapSquares WHERE MapX = " + x + " AND MapY = " + y) == 0) { // Don't create if it already exists!
							int side = 0;
							// todo - calc faction id/side based on rec ids
							if (x < size/2 && y < size/2) {
								side = 1;
							} else if (x >= size/2 && y < size/2) {
								side = 2;
							} else if (x >= size/2 && y >= size/2) {
								side = 3;
							} else if (x < size/2 && y >= size/2) {
								side = 5; // Not that faction 4 doesn't exist!
							}
							dbs.RunIdentityInsert("INSERT INTO StarmapSquares (MapX, MapY, MissionID, FactionID) VALUES (" + x + ", " + y + ", " + GetRandomStarmapMission() + ", " + side + ")");
						}
					}
				}
			}
		}
	}


	private static int GetRandomStarmapMission() {
		if (missions.size() == 0) {
			for (int j=0 ; j<AbstractMission.SF_MISSIONS.length ; j++) {
				int i = AbstractMission.SF_MISSIONS[j];
				if (AbstractMission.IsValidMission(i)) {
					AbstractMission mission = AbstractMission.Factory(i);
					if (mission.isCampaignMission() && mission.getNumOfSides() == 2) {
						missions.add(AbstractMission.SF_MISSIONS[j]);
					}
				}
			}
		}
		return missions.get(Functions.rnd(0, missions.size()-1));

	}


}

