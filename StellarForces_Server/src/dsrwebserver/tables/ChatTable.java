package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

public class ChatTable {

	private ChatTable() {
		//dbs = sqldbs;
	}

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("Chat") == false) {
			dbs.runSQL("CREATE TABLE Chat (ChatID INTEGER AUTO_INCREMENT KEY, UserID INTEGER, Body VARCHAR(4096), DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}


	public static int AddPosting(MySQLConnection dbs, int loginid, String body) throws SQLException {
		body = body.trim();
		int id = dbs.RunIdentityInsert_Syncd("INSERT INTO Chat (UserID, Body) VALUES (" + loginid + "," + SQLFuncs.s2sql(body) + ")");
		return id;
	}


	public static int GetUserID(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsInt("SELECT UserID FROM Chat WHERE ChatID = " + id);
	}

}
