package dsrwebserver.tables;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import dsrwebserver.DSRWebServer;

public final class UnitsTable extends AbstractTable {

	// Morale
	public static final int SCARED_LEVEL = 5;
	private static final int FRIENDLY_UNIT_KILLED = -6;
	private static final int UNIT_KILLED_ENEMY = 8;
	private static final int COMRADE_KILLED_ENEMY = 4;

	// Model Types - these are used in the Missions.csv!
	public static final byte MT_MALE_SIDE_1 = 1;
	public static final byte MT_MALE_SIDE_2 = 2;
	public static final byte MT_ALIEN_TYRANT = 3;
	public static final byte MT_SCIENTIST = 4;
	public static final byte MT_MALE_SIDE_3 = 5;
	public static final byte MT_MALE_SIDE_4 = 6;
	public static final byte MT_QUEEN_ALIEN = 7;
	public static final byte MT_BLOB = 8;
	public static final byte MT_CRAB = 9;
	public static final byte MT_GHOUL = 10;
	public static final byte MT_GHOUL_QUEEN = 11;
	public static final byte MT_ZOMBIE = 12;
	public static final byte MT_CLONE = 13;
	public static final byte MT_ANGEL = 14;


	// Statuses
	public static final byte ST_AWAITING_DEPLOYMENT = 1;
	public static final byte ST_DEPLOYED = 2;
	public static final byte ST_DEAD = 3;
	public static final byte ST_ESCAPED = 4;
	public static final byte ST_NO_LONGER_EXISTS = 5; // i.e. merged blobs or conceded player
	public static final byte ST_CREATED_MID_MISSION = 6; // i.e. a new alien from impreg


	// Unit types
	public static final int UT_NORMAL = 0;
	public static final int UT_ENGINEER = 1;


	// Skills
	public static final int SK_NONE = 0;
	public static final int SK_MEDIC = 1;
	public static final int SK_STEALTH = 2;
	public static final int SK_ENGINEER = 3;


	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("Units") == false) {
			dbs.runSQL("CREATE TABLE Units (UnitID INTEGER AUTO_INCREMENT KEY, UnitCode VARCHAR(32), GameID INTEGER, OrderBy SMALLINT, ModelType SMALLINT, Name VARCHAR(64), Side TINYINT, Status TINYINT, MapX SMALLINT, MapY SMALLINT, CurrentEquipmentID INTEGER, MaxAPs SMALLINT, CurrentAPs SMALLINT, Health SMALLINT, VPsIfKilled TINYINT, VPsIfEscape TINYINT, ArmourTypeID INTEGER, ShotSkill SMALLINT, CombatSkill SMALLINT, Strength SMALLINT, Burden SMALLINT, Angle SMALLINT, OppFire TINYINT, MaxHealth SMALLINT, OppFireAPsReq SMALLINT, CanDeploy TINYINT, KilledBySide TINYINT, DateKilled DATETIME, CorpseCreated TINYINT, MaxEnergy SMALLINT, CurrentEnergy SMALLINT, MaxMorale SMALLINT, CurrentMorale SMALLINT, Panicked TINYINT, CanUseEquipment TINYINT, Kneeling TINYINT, OppFireType TINYINT, AIType TINYINT, UnitType TINYINT, OnFire TINYINT, CanEquip TINYINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		if (dbs.doesColumnExist("Units", "SkillID") == false) {
			dbs.runSQL("ALTER TABLE Units ADD SkillID SMALLINT");
		}

		// create index on GameID
		if (dbs.doesIndexExist("Units", "idx_Units_GameID_Side") == false) {
			dbs.runSQL("create index idx_Units_GameID_Side ON Units (GameID, Side)");
		}

	}


	public UnitsTable(MySQLConnection dbs) {
		super(dbs, "Units", "UnitID");
	}


	public int createUnit(int gameid, int order, int model_type, String name, int side, int APs, int health, int vps_if_killed, int vps_if_escape, int energy, int shot_skill, int combat_skill, int strength, int can_deploy, int morale, int ai, int can_equip) throws SQLException {
		int s_skill = Math.max(0, shot_skill);
		int can_use_equipment = 1;
		if (model_type == UnitsTable.MT_ALIEN_TYRANT || model_type == UnitsTable.MT_QUEEN_ALIEN || model_type == UnitsTable.MT_BLOB || model_type == UnitsTable.MT_CRAB || model_type == UnitsTable.MT_GHOUL) {
			can_use_equipment = 0;
		}
		// Strength must be >= 7 to be able to pick up the rocket launcher!
		int id = dbs.RunIdentityInsert_Syncd("INSERT INTO Units (UnitCode, GameID, OrderBy, ModelType, Name, Side, CurrentAPs, MaxAPs, MaxHealth, Health, VPsIfKilled, VPsIfEscape, MaxEnergy, CurrentEnergy, ShotSkill, CombatSkill, Strength, Status, CanDeploy, OppFire, MaxMorale, CurrentMorale, CanUseEquipment, AIType, CanEquip) VALUES ("
				+ System.currentTimeMillis() + ", " + gameid + ", " + order + ", " + model_type + ", " + SQLFuncs.s2sql(name) + ", " + side + ", " + APs + ", " + APs + ", " + health + ", " + health + ", " + vps_if_killed + ", " + vps_if_escape + ", " + energy + ", " + energy + ", " + s_skill + ", " + combat_skill + ", " + strength + ", " + ST_AWAITING_DEPLOYMENT + ", " + can_deploy + ", 1, " + (morale+10) + ", " + morale + ", " + can_use_equipment + ", " + ai + ", " + can_equip + ")");
		this.selectRow(id);
		return id;
	}


	public String getName() throws SQLException {
		return rs.getString("Name");
	}


	public int getOrderBy() throws SQLException {
		return rs.getInt("OrderBy");
	}


	public String getArmourName() throws SQLException {
		ResultSet rs_a = dbs.getResultSet("SELECT Name FROM ArmourTypes WHERE ArmourTypeID = " + this.rs.getInt("ArmourTypeID"));
		if (rs_a.next()) {
			return rs_a.getString("Name");
		}
		return "None";
	}


	public int CurrentEquipmentID() throws SQLException {
		return rs.getInt("CurrentEquipmentID");
	}


	public int getCurrentItemNoiseRange() throws SQLException {
		ResultSet rs_a = dbs.getResultSet("SELECT EquipmentTypes.NoiseRange FROM Equipment INNER JOIN EquipmentTypes ON EquipmentTypes.EquipmentTypeID = Equipment.EquipmentTypeID WHERE Equipment.EquipmentID = " + this.rs.getInt("CurrentEquipmentID"));
		if (rs_a.next()) {
			return rs_a.getInt("NoiseRange");
		}
		return 0;
	}


	public String getStatusText() throws SQLException {
		return UnitsTable.GetStatusTextFromID(rs.getInt("Status"));
	}


	public int getStatus() throws SQLException {
		return rs.getInt("Status");
	}


	public int getGameID() throws SQLException {
		return rs.getInt("GameID");
	}


	public int getBurden() throws SQLException {
		return rs.getInt("Burden");
	}


	public int getSide() throws SQLException {
		return rs.getInt("Side");
	}


	public int getSkillID() throws SQLException {
		return rs.getInt("SkillID");
	}


	public int isSilent() throws SQLException {
		if (rs.getInt("ModelType") == UnitsTable.MT_ALIEN_TYRANT || rs.getInt("ModelType") == UnitsTable.MT_QUEEN_ALIEN || rs.getInt("SkillID") == SK_STEALTH) {
			return 1;
		}
		return 0;
	}


	public int getMapX() throws SQLException {
		return rs.getInt("MapX");
	}


	public int getMapY() throws SQLException {
		return rs.getInt("MapY");
	}


	public int getStrength() throws SQLException {
		return rs.getInt("Strength");
	}


	public int getMaxAPs() throws SQLException {
		return rs.getInt("MaxAPs");
	}


	public int getMaxEnergy() throws SQLException {
		return rs.getInt("MaxEnergy");
	}


	public int getMaxMorale() throws SQLException {
		return rs.getInt("MaxMorale");
	}


	public int getCurrentAPs() throws SQLException {
		return rs.getInt("CurrentAPs");
	}


	public int getOppFireAPs() throws SQLException {
		return rs.getInt("OppFireAPsReq");
	}


	public int getHealth() throws SQLException {
		return rs.getInt("Health");
	}


	public int getMaxHealth() throws SQLException {
		return rs.getInt("MaxHealth");
	}


	public int getAngle() throws SQLException {
		return rs.getInt("Angle");
	}


	public int getCurrentEnergy() throws SQLException {
		return rs.getInt("CurrentEnergy");
	}


	public int getCurrentMorale() throws SQLException {
		return rs.getInt("CurrentMorale");
	}


	public int getKilledBySide() throws SQLException {
		return rs.getInt("KilledBySide");
	}


	public int getCombatSkill() throws SQLException {
		return rs.getInt("CombatSkill");
	}


	public int getShotSkill() throws SQLException {
		return rs.getInt("ShotSkill");
	}


	public int getVPsIfEscape() throws SQLException {
		return rs.getInt("VPsIfEscape");
	}


	public int getVPsIfKilled() throws SQLException {
		return rs.getInt("VPsIfKilled");
	}


	public int getModelType() throws SQLException {
		return rs.getInt("ModelType");
	}


	public int getCurrentEquipmentID() throws SQLException {
		return rs.getInt("CurrentEquipmentID");
	}


	public String getUnitCode() throws SQLException {
		return rs.getString("UnitCode");
	}


	public int getCanUseEquipment() throws SQLException {
		return rs.getInt("CanUseEquipment");
	}


	public boolean isOppFire() throws SQLException {
		return rs.getInt("OppFire") == 1;
	}


	public void setArmourType(int id) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET ArmourTypeID = " + id + " WHERE UnitID = " + this.getID());
	}


	public void setSkillID(int id) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET SkillID = " + id + " WHERE UnitID = " + this.getID());
	}


	public void setCurrentEquipmentID(int id) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET CurrentEquipmentID = " + id + " WHERE UnitID = " + this.getID());
	}


	public void setOnFire(boolean b) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET OnFire = " + (b?"1":"0") + " WHERE UnitID = " + this.getID());
	}


	public void setHealth(int h) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET Health = " + h + " WHERE UnitID = " + this.getID());
	}


	public void setCurrentAPs(int h) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET CurrentAPs = " + h + " WHERE UnitID = " + this.getID());
	}


	public void setModelType(int m) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET ModelType = " + m + " WHERE UnitID = " + this.getID());
	}


	public void setSide(int s) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET Side = " + s + " WHERE UnitID = " + this.getID());
	}


	public void setCurrentMorale(int s) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET CurrentMorale = " + s + " WHERE UnitID = " + this.getID());
	}


	public void incCurrentMorale(int amt) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET CurrentMorale = CurrentMorale + " + amt + " WHERE UnitID = " + this.getID());
	}


	public void incCurrentEnergy(int amt) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET CurrentEnergy = CurrentEnergy + " + amt + " WHERE UnitID = " + this.getID());
	}


	public void setStatus(int s) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET Status = " + s + " WHERE UnitID = " + this.getID());
	}


	public void setMapXYAngle(int x, int y, int ang) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET MapX = " + x + ", MapY = " + y + ", Angle = " + ang + " WHERE UnitID = " + this.getID());
	}


	/*public void setCanUseEquipment(boolean b) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET CanUseEquipment = " + SQLFuncs.b201(b) + " WHERE UnitID = " + this.getID());
	}*/


	/*public void setCombatSkill(int i) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET CombatSkill = " + i + " WHERE UnitID = " + this.getID());
	}


	public void setStrength(int i) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET Strength = " + i + " WHERE UnitID = " + this.getID());
	}*/


	/*public void setVPsIfEscape(int vps) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET VPsIfEscape = " + vps + " WHERE UnitID = " + this.getID());
	}*/


	public void setKilled(int killed_by_side, int killedby_unitid, GamesTable game) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET DateKilled = NOW(), KilledBySide = " + killed_by_side + " WHERE UnitID = " + this.getID());
		EquipmentTable.CreateEquipmentFromCorpse(dbs, rs);

		try {
			if (game.isAdvancedMode()) {
				// Adjust morale for the sides
				game.adjustSidesMorale(true, this.getSide(), FRIENDLY_UNIT_KILLED);
				game.adjustSidesMorale(false, this.getSide(), COMRADE_KILLED_ENEMY);
				// Adjust morale for specific unit
				if (killedby_unitid > 0) {
					UnitsTable unit = new UnitsTable(dbs);
					unit.selectRow(killedby_unitid);
					unit.incCurrentMorale(UNIT_KILLED_ENEMY-COMRADE_KILLED_ENEMY);
					//unit.clone();
					unit.close();
				}
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	public void setName(String n) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET Name = " + SQLFuncs.s2sql(n) + " WHERE UnitID = " + this.getID());
	}


	public static String GetStatusTextFromID(int st) {
		switch (st) {
		case ST_AWAITING_DEPLOYMENT:
			return "Awaiting Deployment";
		case ST_DEPLOYED:
			return "Deployed";
		case ST_DEAD:
			return "DEAD";
		case ST_ESCAPED:
			return "ESCAPED";
		case ST_NO_LONGER_EXISTS:
			return "DOESN'T EXIST";
		case ST_CREATED_MID_MISSION:
			return "GESTATING";
		default:
			throw new RuntimeException("Unknown status type: " + st);
		}
	}


	public static void Update(MySQLConnection dbs, int unitid, String unitcode, int x, int y, int angle, int status, int curr_item, int aps, int health, int opp_fire_type, int max_health, int combat_skill, int strength, int armour_id, int shot_skill, int on_fire) throws SQLException {
		dbs.runSQLUpdate("UPDATE Units SET MapX = " + x + ", MapY = " + y + ", Angle = " + angle + ", Status = " + status + ", CurrentEquipmentID = " + curr_item + ", CurrentAPs = " + aps + ", Health = " + health + ", OppFireType = " + opp_fire_type + ", MaxHealth = " + max_health + ", CombatSkill = " + combat_skill + ", Strength = " + strength + ", OnFire = " + on_fire + " WHERE UnitID = " + unitid + " AND UnitCode = " + SQLFuncs.s2sql(unitcode));
		if (armour_id >= 0) { // If it's -1 it means the client is too early to specify this
			dbs.runSQLUpdate("UPDATE Units SET ArmourTypeID = " + armour_id + " WHERE UnitID = " + unitid + " AND UnitCode = " + SQLFuncs.s2sql(unitcode));
		}
	}


	public static void RenameUnit(MySQLConnection dbs, int gameid, int side, int num, String name) throws SQLException {
		try {
			dbs.runSQLUpdate("UPDATE Units SET Name = " + SQLFuncs.s2sql(name) + " WHERE GameID = " + gameid + " AND Side = " + side + " AND OrderBy = " + num);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	public static void UseFirstItem(MySQLConnection dbs, int gameid, int side) throws SQLException {
		String sql = "SELECT * FROM Units WHERE GameID = " + gameid + " AND Side = " + side;
		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) {
			ResultSet rs_equip = dbs.getResultSet("SELECT EquipmentID FROM Equipment WHERE UnitID = " + rs.getInt("UnitID") + " ORDER BY EquipmentID"); // Order by equipid NOT datecreated as datecreated are all probably the same
			if (rs_equip.next()) {
				dbs.runSQLUpdate("UPDATE Units SET CurrentEquipmentID = " + rs_equip.getInt("EquipmentID") + " WHERE UnitID = " + rs.getInt("UnitID"));
			}
		}
	}


	public static void AddStatCell(StringBuffer str, int max, int max_health) {
		int diff = max - max_health;
		//String hex = Integer.toHexString(((255 * max_health)/max));
		String hex = Integer.toHexString(((diff+1) * 20));
		str.append("<td style=\"background-color: #" + hex + "ff" + hex + ";\">" + max_health + "</td>");
	}


	public void dropCurrentItem() throws SQLException { // Used when unit panicks.
		dbs.runSQLUpdate("UPDATE Equipment SET UnitID = 0, MapX = " + this.getMapX() + ", MapY = " + this.getMapY() + " WHERE EquipmentID = " + this.getCurrentEquipmentID());
		this.setCurrentEquipmentID(0);
	}


	public static int GetUnitsRemaining(MySQLConnection dbs, int gameid, int side) throws SQLException {
		return dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + gameid + " AND Side = " + side + " AND Status IN (" + UnitsTable.ST_AWAITING_DEPLOYMENT + ", " + UnitsTable.ST_DEPLOYED + ")");
	}


	public int getAIType() throws SQLException {
		return rs.getInt("AIType");
	}


	public boolean hasEquipment() throws SQLException {
		return dbs.getScalarAsInt("SELECT Count(*) FROM Equipment WHERE UnitID = " + this.getID()) > 0;
	}

	public boolean selectRandomUnitForSideWithoutSkill(int gameid, int side) throws SQLException {
		try {
			int id = dbs.getScalarAsInt("SELECT UnitID FROM Units WHERE GameID = " + gameid
					+ " AND ModelType IN ( " + MT_MALE_SIDE_1 + ", " + MT_MALE_SIDE_2 + ", " + MT_MALE_SIDE_3 + "," + MT_MALE_SIDE_4 +")"
					+ " AND Side = " + side + " AND  COALESCE(SkillID, 0) = 0 ORDER BY Rand()");
			this.selectRow(id);
			return true;
		} catch (SQLException ex) { // todo - improve this, dont rely on error
			ex.printStackTrace();
			return false;
		}
	}


}

