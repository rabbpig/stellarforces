package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;

/**
 * This is for all universe equipment NOT in a game.
 *
 */
public class UniversePlayersEquipmentTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("UniversePlayersEquipment") == false) {
			dbs.runSQL("CREATE TABLE UniversePlayersEquipment (UniversePlayersEquipmentID INTEGER AUTO_INCREMENT KEY, LoginID INTEGER, EquipmentTypeID INTEGER, Qty SMALLINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )"); // Ammo SMALLINT, 
		}
	}
	
	
	public static void CreateData(MySQLConnection dbs, int loginid) throws SQLException {
		int eqid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SP30);
		AddRec(dbs, loginid, eqid, 10);

		eqid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_MK1);
		AddRec(dbs, loginid, eqid, 10);

		eqid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_AUTOCANNON);
		AddRec(dbs, loginid, eqid, 5);

		eqid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_AP50);
		AddRec(dbs, loginid, eqid, 10);

	}
	
	
	public static void AddRec(MySQLConnection dbs, int loginid, int eqid, int qty) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UniversePlayersEquipment (LoginID, EquipmentTypeID, Qty) VALUES (" + loginid + ", " + eqid + ", " + qty + ")");
	}
	
	
	public static void DeductEquipment(MySQLConnection dbs, int loginid, int eqid) throws SQLException {
		dbs.runSQLUpdate("UPDATE UniversePlayersEquipment SET Qty = Qty-1 WHERE LoginID = " + loginid + " AND EquipmentTypeID = " + eqid);
	}
	
}
