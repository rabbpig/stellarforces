package dsrwebserver.tables;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.io.TextFile;
import ssmith.lang.Dates;
import ssmith.lang.Functions;
import ssmith.util.Interval;
import dsrwebserver.DSRWebServer;

public class CampUnitsTable extends AbstractTable {

	private static ArrayList<String> names = null;
	private static String[] RANKS = {"Recruit", "Private", "Corporal", "Sergeant", "Lieutenant", "Captain", "Major", "Colonel", "Brigadier", "General"};

	private static Interval recalc_ranks_interval = new Interval(Dates.HOUR);
	private static Object recalc_lock = new Object();


	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("CampUnits") == false) {
			dbs.runSQL("CREATE TABLE CampUnits (CampUnitID INTEGER AUTO_INCREMENT KEY, CampUnitCode VARCHAR(32), OrderBy SMALLINT, Name VARCHAR(64), MaxAPs SMALLINT, ShotSkill SMALLINT, CombatSkill SMALLINT, Strength SMALLINT, CanUseEquipment TINYINT, MaxHealth SMALLINT, RankValue SMALLINT, Kills SMALLINT, OwnerID INTEGER, UnitID INTEGER, TotalMissions INTEGER, TotalVictories INTEGER, FriendlyKills SMALLINT, MaxEnergy SMALLINT, MaxMorale SMALLINT, PrevRankPos INTEGER, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}


	public CampUnitsTable(MySQLConnection dbs) {
		super(dbs, "CampUnits", "CampUnitID");
	}


	public boolean selectByUnitID(int unitid) throws SQLException {
		String sql = "SELECT CampUnitID FROM CampUnits WHERE UnitID = " + unitid;
		ResultSet rs = dbs.getResultSet(sql);
		if (rs.next()) {
			this.selectRow(rs.getInt("CampUnitID"));
			return true;
		} else {
			return false;
		}
	}


	public int createUnit(int owner_id) throws SQLException {
		int APS = 62;
		int HEALTH = 36;
		int ENERGY = 250;
		int SHOT_SKILL = 35;
		int COMBAT_SKILL = 50;
		// Strength must be >= 7 to be able to pick up the rocket launcher
		int STRENGTH = 9;
		int MORALE = 40;
		String name = CampUnitsTable.GetRandomName();
		int id = dbs.RunIdentityInsert_Syncd("INSERT INTO CampUnits (CampUnitCode, OrderBy, Name, MaxAPs, MaxHealth, MaxEnergy, ShotSkill, CombatSkill, Strength, OwnerID, MaxMorale) VALUES (" + System.currentTimeMillis() + ", 10, " + SQLFuncs.s2sql(name) + ", " + APS + ", " + HEALTH + ", " + ENERGY + ", " + SHOT_SKILL + ", " + COMBAT_SKILL + ", " + STRENGTH + ", " + owner_id + ", " + MORALE + ")");
		CampaignLogTable.AddRec(dbs, owner_id, name + " recruited.", false);
		this.selectRow(id);
		return id;
	}


	public String getName() throws SQLException {
		return CampUnitsTable.GetRank(rs.getInt("RankValue")) + " " + rs.getString("Name");
	}


	public String getStatusText() throws SQLException {
		if (this.getUnitID() > 0) {
			return "On duty";
		} else {
			return "Resting";
		}
	}


	public int getOrderBy() throws SQLException {
		return rs.getInt("OrderBy");
	}


	public int getKills() throws SQLException {
		return rs.getInt("Kills");
	}


	public int getTotalMissions() throws SQLException {
		return rs.getInt("TotalMissions");
	}


	public int getTotalVictories() throws SQLException {
		return rs.getInt("TotalVictories");
	}


	public int getFriendlyKills() throws SQLException {
		return rs.getInt("FriendlyKills");
	}


	public int getRankValue() throws SQLException {
		return rs.getInt("RankValue");
	}


	public int getStrength() throws SQLException {
		return rs.getInt("Strength");// + GetRankPos(this.getRankValue());
	}


	public int getMaxAPs() throws SQLException {
		return rs.getInt("MaxAPs");// + GetRankPos(this.getRankValue());
	}


	public int getMaxHealth() throws SQLException {
		return rs.getInt("MaxHealth");// + GetRankPos(this.getRankValue());
	}


	public int getMaxEnergy() throws SQLException {
		return rs.getInt("MaxEnergy");// + (GetRankPos(this.getRankValue()*5));
	}


	public int getMaxMorale() throws SQLException {
		return rs.getInt("MaxMorale");// + GetRankPos(this.getRankValue());
	}


	public int getCombatSkill() throws SQLException {
		return rs.getInt("CombatSkill");// + GetRankPos(this.getRankValue());
	}


	public int getShotSkill() throws SQLException {
		return rs.getInt("ShotSkill");// + GetRankPos(this.getRankValue());
	}


	private void incStatsRandomly(int rank_val_inc) throws SQLException {
		try {
			for(int i=0 ; i<rank_val_inc*7 ; i++) { // Increase one point per stat.
				int stat = Functions.rnd(1, 7);
				String sql = "UPDATE CampUnits SET ";
				switch (stat) {
				case 1:
					sql = sql + "Strength = Strength + 1";
					break;
				case 2:
					sql = sql + "MaxAPs = MaxAPs + 1";
					break;
				case 3:
					sql = sql + "MaxHealth = MaxHealth + 1";
					break;
				case 4:
					sql = sql + "MaxEnergy = MaxEnergy + 5";
					break;
				case 5:
					sql = sql + "MaxMorale = MaxMorale + 1";
					break;
				case 6:
					sql = sql + "CombatSkill = CombatSkill + 1";
					break;
				case 7:
					sql = sql + "ShotSkill = ShotSkill + 1";
					break;
				default:
					throw new RuntimeException("Unknown stat num: " + stat);
				}
				sql = sql + " WHERE CampUnitID = " + this.getID();
				dbs.runSQLUpdate(sql);
				DSRWebServer.LogCampUnits(sql);
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	public int getOwnerID() throws SQLException {
		return rs.getInt("OwnerID");
	}


	public int getModelType() throws SQLException {
		return rs.getInt("ModelType");
	}


	public int getUnitID() throws SQLException {
		return rs.getInt("UnitID");
	}


	public void hasKilledEnemy() throws SQLException {
		dbs.runSQLUpdate("UPDATE CampUnits SET Kills = COALESCE(Kills, 0) + 1 WHERE CampUnitID = " + this.getID());
		CampaignLogTable.AddRec(dbs, getOwnerID(), getName() + " has killed an enemy unit!", true);
	}


	public void hasKilledFriend() throws SQLException {
		dbs.runSQLUpdate("UPDATE CampUnits SET FriendlyKills = COALESCE(FriendlyKills, 0) + 1 WHERE CampUnitID = " + this.getID());
		CampaignLogTable.AddRec(dbs, getOwnerID(), getName() + " has killed a friendly unit!", true);
	}


	public void incTotalMissions() throws SQLException {
		dbs.runSQLUpdate("UPDATE CampUnits SET TotalMissions = COALESCE(TotalMissions,0) + 1 WHERE CampUnitID = " + this.getID());
		CampaignLogTable.AddRec(dbs, getOwnerID(), getName() + " has survived a mission.", false);
	}


	public void incTotalVictories() throws SQLException {
		dbs.runSQLUpdate("UPDATE CampUnits SET TotalVictories = COALESCE(TotalVictories,0) + 1 WHERE CampUnitID = " + this.getID());
		CampaignLogTable.AddRec(dbs, getOwnerID(), getName() + " was on the winning side.", true);
	}


	/**
	 * This will do it for ALL camp units.
	 * 
	 */
	public static void RecalcRanks(MySQLConnection dbs, boolean force) throws SQLException {
		try {
			if (recalc_ranks_interval.hitInterval() || force) {
				synchronized (recalc_lock) {
					CampUnitsTable camp_unit = new CampUnitsTable(dbs);

					ResultSet rs2 = dbs.getResultSet("SELECT * FROM CampUnits");
					while (rs2.next()) {
						dbs.runSQLUpdate("UPDATE CampUnits SET RankValue = (COALESCE(TotalMissions, 0)) + (COALESCE(TotalVictories, 0) * 5) + (COALESCE(Kills, 0) * 3) - (COALESCE(FriendlyKills, 0) * 3) WHERE CampUnitID = " + rs2.getInt("CampUnitID"));

						camp_unit.selectRow(rs2.getInt("CampUnitID"));

						// Inc stats?
						int rank_pos = GetRankPos(rs2.getInt("RankValue"));
						if (rs2.getInt("PrevRankPos") < rank_pos) {
							//DSRWebServer.p("Increasing stats of unit " + rs2.getInt("CampUnitID"));
							int diff = rank_pos - rs2.getInt("PrevRankPos");
							dbs.runSQLUpdate("UPDATE CampUnits SET PrevRankPos = " + rank_pos + " WHERE CampUnitID = " + rs2.getInt("CampUnitID"));
							camp_unit.incStatsRandomly(diff);

							// Update the units name if applic
							if (camp_unit.getUnitID() > 0) {
								UnitsTable unit = new UnitsTable(dbs);
								unit.selectRow(camp_unit.getUnitID());
								unit.setName(camp_unit.getName());
								unit.close();
							}
						}
					}
					camp_unit.close();
				}
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	public static int GetNumCampaignUnits(MySQLConnection dbs, int loginid, boolean only_spare) throws SQLException {
		String sql = "SELECT Count(*) FROM CampUnits WHERE OwnerID = " + loginid;
		if (only_spare) {
			sql = sql + " AND COALESCE(UnitID, 0) = 0";
		}
		return dbs.getScalarAsInt(sql);

	}


	public static int GetRankPos(int val) {
		if (val <= 0) {
			return 0;
		} else if (val < 9) {
			return 1;
		} else if (val < 18) {
			return 2;
		} else if (val < 36) {
			return 3;
		} else if (val < 72) {
			return 4;
		} else if (val < 150) {
			return 5;
		} else if (val < 300) {
			return 6;
		} else if (val < 600) {
			return 7;
		} else if (val < 1500) {
			return 8;
		} else {
			return 9;
		}
	}


	private static String GetRank(int val) {
		try {
			return RANKS[GetRankPos(val)];
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
			return "Error";
		}
	}


	public static String GetRandomName() {
		if (names == null) {
			ArrayList<String> tmp = new ArrayList<String>();
			TextFile tf = new TextFile();
			try {
				tf.openFile("./webroot/serverdata/names.txt", TextFile.READ);
				while (tf.isEOF() == false) {
					String s = tf.readLine();
					if (s.length() > 0) {
						tmp.add(s);
					}
				}
				tf.close();
			} catch (IOException ex) {
				DSRWebServer.HandleError(ex, true);
				tmp.add("X");
			}
			names = tmp;
		}
		return names.get(Functions.rnd(0, names.size()-1));
	}



	/*public static void FixUnits(MySQLConnection dbs) throws SQLException {
		ResultSet rs = dbs.getResultSet("SELECT * FROM CampUnits WHERE CampUnitID IN (1156, 936)");
		CampUnitsTable camp = new CampUnitsTable(dbs);
		while (rs.next()) {
			camp.selectRow(rs.getInt("CampUnitID"));
			camp.incStatsRandomly(1);
		}
	}*/


	public static void EnsureEnoughCampUnits(MySQLConnection dbs, LoginsTable login) throws SQLException {
		try {
			if (login.getFactionID() > 0) {
				// Check player is in game
				//if (login.isPlayerPlayingInCampaign() == false) { // Only replenish when not fighting!
					// Ensure they have enough units
					int max_units = GetMaxCampUnits(dbs, login);
					CampUnitsTable camp_unit = new CampUnitsTable(dbs);
					while (max_units > CampUnitsTable.GetNumCampaignUnits(dbs, login.getID(), false)) {
						camp_unit.createUnit(login.getID());
					}
					camp_unit.close();
				//}
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	public static int GetMaxCampUnits(MySQLConnection dbs, LoginsTable login) throws SQLException {
		//String DATE_DIFF = "DATEDIFF(curdate(), LastLoginDate) < " + FactionsTable.EXPIRY_DAYS;
		//int players_in_biggest_faction = dbs.getScalarAsInt("select Count(*) from Logins where FactionID > 0 AND " + DATE_DIFF + " GROUP BY FactionID order by Count(*) desc");
		//int players_in_this_faction = dbs.getScalarAsInt("select Count(*) from Logins where FactionID = " + login.getFactionID() + " AND " + DATE_DIFF);
		int players_in_biggest_faction = dbs.getScalarAsInt("select Count(*) from Logins where FactionID > 0 GROUP BY FactionID order by Count(*) desc");
		int players_in_this_faction = dbs.getScalarAsInt("select Count(*) from Logins where FactionID = " + login.getFactionID());
		if (players_in_this_faction == 0) {
			players_in_this_faction = 1; // Prevent div/0 error
		}
		int max_units = (players_in_biggest_faction * DSRWebServer.MIN_CAMP_UNITS) / players_in_this_faction;
		if (max_units > DSRWebServer.MAX_CAMP_UNITS) {
			max_units = DSRWebServer.MAX_CAMP_UNITS;
		}
		return max_units;

	}


	public void setUnitID(int id) throws SQLException {
		dbs.runSQLUpdate("UPDATE CampUnits SET UnitID = " + id + " WHERE CampUnitID = " + this.getID());
	}


	public void deleteIfUnitDead() throws SQLException {
		UnitsTable unit = new UnitsTable(dbs);
		unit.selectRow(this.getUnitID());
		if (unit.getStatus() == UnitsTable.ST_DEAD) {
			this.delete(this.getID());
			CampaignLogTable.AddRec(dbs, this.getOwnerID(), "" + this.getName() + " was killed in the last mission.", false);
		}
		unit.close();
	}


	public static void DetachUnitsAndCampUnitsIfGameFinished(MySQLConnection dbs, int gameid, LoginsTable login) throws SQLException {
		// Check camp units are not allocated to units that don't exist any more.
		// ONLY CALL THIS ONCE CAMP UNIT STATS HAVE BEEN UPDATED!

		try {
			GamesTable game = new GamesTable(dbs);
			if (game.doesRowExist(gameid)) {
				game.selectRow(gameid);
				if (game.getGameStatus() == GamesTable.GS_FINISHED) {
					CampUnitsTable camp_unit = new CampUnitsTable(dbs);
					ResultSet rs = dbs.getResultSet("SELECT CampUnitID FROM CampUnits INNER JOIN Units ON Units.UnitID = CampUnits.UnitID WHERE CampUnits.UnitID > 0 AND CampUnits.OwnerID = " + login.getID() + " AND Units.GameID = " + gameid);
					while (rs.next()) {
						camp_unit.selectRow(rs.getInt("CampUnitID"));
						camp_unit.setUnitID(0);
					}
					camp_unit.close();
				}
			}
			game.close();
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


}

