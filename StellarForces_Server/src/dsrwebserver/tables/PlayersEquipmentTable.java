package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;

/**
 * This is for all universe equipment NOT in a game.
 *
 */
public class PlayersEquipmentTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("PlayersEquipment") == false) {
			dbs.runSQL("CREATE TABLE PlayersEquipment (PlayersEquipmentID INTEGER AUTO_INCREMENT KEY, LoginID INTEGER, EquipmentTypeID INTEGER, Ammo SMALLINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		if (dbs.doesColumnExist("PlayersEquipment", "Qty") == false) {
			dbs.runSQL("ALTER TABLE PlayersEquipment ADD Qty SMALLINT");
		}
	}
	
	
	public static void CreateData(MySQLConnection dbs, int loginid) throws SQLException {
		int eqid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_SP30);
		AddRec(dbs, loginid, eqid, 7, 10);

		eqid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_MK1);
		AddRec(dbs, loginid, eqid, 7, 10);

		eqid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_AUTOCANNON);
		AddRec(dbs, loginid, eqid, 7, 5);

		eqid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_AP50);
		AddRec(dbs, loginid, eqid, 7, 10);

	}
	
	
	public static void AddRec(MySQLConnection dbs, int loginid, int eqid, int ammo, int qty) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO PlayersEquipment (LoginID, EquipmentTypeID, Ammo, Qty) VALUES (" + loginid + ", " + eqid + ", " + ammo + ", " + qty + ")");
	}
	
	
}
