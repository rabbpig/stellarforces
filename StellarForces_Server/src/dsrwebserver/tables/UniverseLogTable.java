package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

public class UniverseLogTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("UniverseLog") == false) {
			dbs.runSQL("CREATE TABLE UniverseLog (UniverseLogID INTEGER AUTO_INCREMENT KEY, LoginID INTEGER, Entry VARCHAR(128), DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}
	
	
	/**
	 * LoginID is the player who should see the log (or 0 for all).
	 */
	public static void AddRec(MySQLConnection dbs, int loginid, String entry, boolean bold) throws SQLException {
		if (bold) {
			entry = "<b>" + entry + "</b>";
		}
		dbs.RunIdentityInsert("INSERT INTO UniverseLog (LoginID, Entry) VALUES (" + loginid + ", " + SQLFuncs.s2sql(entry) + ")");
	}
	
}
