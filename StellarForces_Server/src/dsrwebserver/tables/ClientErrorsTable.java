package dsrwebserver.tables;

import java.sql.SQLException;

import dsrwebserver.DSRWebServer;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

public class ClientErrorsTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		dbs.runSQL("CREATE TABLE IF NOT EXISTS ClientErrors (ClientErrorID INTEGER AUTO_INCREMENT KEY, ErrorText VARCHAR(4096), Fixed TINYINT, Version FLOAT, Login VARCHAR(128), DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
	}
	
	
	public static void AddRec(MySQLConnection dbs, float version, String login, String text) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO ClientErrors (Version, Login, ErrorText) VALUES (" + version + ", " + SQLFuncs.s2sql(login) + ", " + SQLFuncs.s2sql(text) + ")");
		DSRWebServer.SendEmailToAdmin("Client error", version + ":" + text);
	}
	
	
	public static void DeleteRec(MySQLConnection dbs, int id) throws SQLException {
		dbs.runSQLDelete("DELETE FROM ClientErrors WHERE ClientErrorID = " + id);
	}
	
	

}
