package dsrwebserver.tables;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

public class ForumTopicsTable {

	private MySQLConnection dbs;
	private ResultSet rs;

    public ForumTopicsTable(MySQLConnection sqldbs) {
		dbs = sqldbs;
	}

    
	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("ForumTopics") == false) {
			dbs.runSQL("CREATE TABLE ForumTopics (ForumTopicID INTEGER AUTO_INCREMENT KEY, Name VARCHAR(512), DateOfLastPost DATETIME, ParentForumTopicID INTEGER, Description VARCHAR(512), Img VARCHAR(128), Ordering INTEGER, CreatorID INTEGER, LastPostLoginID INTEGER, TotalPostings INTEGER, Sticky TINYINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}
	
	
	public boolean selectByID(int id) throws SQLException {
		rs = dbs.getResultSet("SELECT * FROM ForumTopics WHERE ForumTopicID = " + id);
		return rs.next();
	}
	
	
	public String getImageFilename() throws SQLException {
		return rs.getString("Img");
	}
	
	
	public String getName() throws SQLException {
		return rs.getString("Name");
	}
	
	
	public void addTopic(String name, int userid) throws SQLException {
		if (dbs.getScalarAsInt("SELECT Count(*) FROM ForumTopics WHERE ParentForumTopicID IS NULL AND Name = " + SQLFuncs.s2sql(name)) == 0) {
			dbs.runSQL("INSERT INTO ForumTopics (Name, CreatorID) VALUES (" + SQLFuncs.s2sql(name) + ", " + userid + ")");
		}
	}
	

	public int addTopic(int parentid, String name, int userid) throws SQLException {
		if (dbs.getScalarAsInt("SELECT Count(*) FROM ForumTopics WHERE ParentForumTopicID = " + parentid + " AND Name = " + SQLFuncs.s2sql(name)) == 0) {
			return dbs.RunIdentityInsert_Syncd("INSERT INTO ForumTopics (ParentForumTopicID, Name, CreatorID) VALUES (" + parentid + ", " + SQLFuncs.s2sql(name) + ", " + userid + ")");
		} else {
			return parentid;
		}
	}

	
	public void setName(int id, String name) throws SQLException {
		dbs.runSQLUpdate("UPDATE ForumTopics SET Name = " + SQLFuncs.s2sql(name) + " WHERE ForumTopicID = " + id);
	}
}
