package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;

public class TreatiesTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("Treaties") == false) {
			dbs.runSQL("CREATE TABLE Treaties (TreatyID INTEGER AUTO_INCREMENT KEY, Login1ID INTEGER, Login2ID INTEGER, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}
	
	

}
