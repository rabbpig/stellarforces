package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

/**
 * This is the log for players units in the campaign.  Note that this is different to the StarmapLog!
 *
 */
public class CampaignLogTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("CampaignLog") == false) {
			dbs.runSQL("CREATE TABLE CampaignLog (CampaignLogID INTEGER AUTO_INCREMENT KEY, LoginID INTEGER, Entry VARCHAR(128), DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}
	
	
	/**
	 * LoginID is the player who should see the log (or 0 for all).
	 */
	public static void AddRec(MySQLConnection dbs, int loginid, String entry, boolean bold) throws SQLException {
		if (bold) {
			entry = "<b>" + entry + "</b>";
		}
		dbs.RunIdentityInsert("INSERT INTO CampaignLog (LoginID, Entry) VALUES (" + loginid + ", " + SQLFuncs.s2sql(entry) + ")");
	}
	
}
