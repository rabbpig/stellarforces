package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

public class ForumPostingsTable {

	private MySQLConnection dbs;

	public ForumPostingsTable(MySQLConnection sqldbs) {
		dbs = sqldbs;
	}

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("ForumPostings") == false) {
			dbs.runSQL("CREATE TABLE ForumPostings (ForumPostingID INTEGER AUTO_INCREMENT KEY, ReplyToPostingID INTEGER, UserID INTEGER, Body VARCHAR(4096), ForumTopicID INTEGER, FromIP VARCHAR(16), DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}

		// Index
		if (dbs.doesIndexExist("ForumPostings", "idx_ForumPostings_ForumTopicID") == false) {
			dbs.runSQL("create index idx_ForumPostings_ForumTopicID ON ForumPostings (ForumTopicID)");
		}
	}


	public int addPosting(int loginid, int topicid, int replytoid, String body, String from_ip) throws SQLException {
		if (topicid > 0) {
			body = body.trim();
			String r = (replytoid > 0 ? ""+replytoid : "NULL");
			int id = dbs.RunIdentityInsert_Syncd("INSERT INTO ForumPostings (UserID, ForumTopicID, ReplyToPostingID, Body, FromIP) VALUES (" + loginid + "," + topicid + ", " + r + ", " + SQLFuncs.s2sql(body) + "," + SQLFuncs.s2sql(from_ip) + ")");
			updateForumTopicDOLP(topicid, loginid);

			//DSRWebServer.SendEmail(DSRWebServer.cfgfile.get("admin_email"), "New Posting to " + AppletMain.TITLE + " forums", "New Posting text:\n\n\"" + body + "\"");
			return id;
		} else {
			return -1;
		}
	}


	private void updateForumTopicDOLP(int topicid, int loginid) throws SQLException {
		while (true) {
			dbs.runSQLUpdate("UPDATE ForumTopics SET DateOfLastPost = NOW(), LastPostLoginID = " + loginid + " WHERE ForumTopicID = " + topicid);
			topicid = dbs.getScalarAsInt("SELECT ParentForumTopicID FROM ForumTopics WHERE ForumTopicID = " + topicid);
			if (topicid <= 0) {
				break;
			}
		}
	}


	public void delete(int postid) throws SQLException {
		dbs.runSQLUpdate("DELETE FROM ForumPostings WHERE ForumPostingID = " + postid);
	}


	public static int GetUserID(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsInt("SELECT UserID FROM ForumPostings WHERE ForumPostingID = " + id);
	}

}
