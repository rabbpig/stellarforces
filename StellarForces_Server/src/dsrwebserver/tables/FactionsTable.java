package dsrwebserver.tables;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import ssmith.util.MemCacheArray;
import ssmith.util.MyList;
import dsrwebserver.DSRWebServer;
import dsrwebserver.pages.dsr.factions;

public class FactionsTable extends AbstractTable {

	public static final int EXPIRY_DAYS = 21; // Days before not counted in faction

	private static MemCacheArray mc_fac_cols = new MemCacheArray(5);

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("Factions") == false) {
			dbs.runSQL("CREATE TABLE Factions (FactionID INTEGER AUTO_INCREMENT KEY, Name VARCHAR(128), Sectors INTEGER, Colour CHAR(6), ImageFilename VARCHAR(128), DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
		}
	}


	public FactionsTable(MySQLConnection sqldbs) throws SQLException {
		super(sqldbs, "Factions", "FactionID");
	}


	public static String GetName(MySQLConnection dbs, int id) throws SQLException {
		if (id > 0) {
			return dbs.getScalarAsString("SELECT Name FROM Factions WHERE FactionID = " + id);
		} else {
			return "None";
		}
	}


	public static String GetColour(MySQLConnection dbs, int id) throws SQLException {
		if (mc_fac_cols.containsKey(id)) {
			return (String)mc_fac_cols.get(id);
		}
		String res = "000000"; 
		if (id > 0) {
			res = dbs.getScalarAsString("SELECT Colour FROM Factions WHERE FactionID = " + id);
		}
		mc_fac_cols.put(id, res);
		return res;
	}


	public static String GetImageFilename(MySQLConnection dbs, int id) throws SQLException {
		if (id > 0) {
			return dbs.getScalarAsString("SELECT ImageFilename FROM Factions WHERE FactionID = " + id);
		} else {
			return "";
		}
	}


	public static int GetFactionWithLeastPlayers_UNUSED(MySQLConnection dbs) throws SQLException {
		try {
			return dbs.getScalarAsInt("SELECT FactionID FROM Logins WHERE " + factions.DATE_DIFF + " AND FactionID is not null AND FactionID > 0 GROUP BY FactionID ORDER BY count(*)");
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
			return dbs.getScalarAsInt("SELECT FactionID FROM Factions ORDER BY RAND()");
		}
	}	


	public static int GetFactionWithLessThanPlayers(MySQLConnection dbs, int num) throws SQLException {
		try {
			return dbs.getScalarAsInt("SELECT FactionID FROM Logins WHERE " + factions.DATE_DIFF + " AND FactionID is not null AND FactionID > 0 GROUP BY FactionID HAVING Count(*) <= " + num + " ORDER BY count(*) DESC");
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
			return 0;
		}
	}	


	public static int GetFactionWithLeastLeaguePoints(MySQLConnection dbs) throws SQLException {
		try {
			int lowest_facid = -1;
			int lowest_points = 9999;
			ResultSet rs = dbs.getResultSet("SELECT * FROM Factions");
			while (rs.next()) {
				int pts = dbs.getScalarAsInt("SELECT SUM(ELOPoints) FROM Logins WHERE FactionID = " + rs.getInt("FactionID"));
				if (pts < lowest_points) {
					lowest_points = pts;
					lowest_facid = rs.getInt("FactionID");
				}
			}
			rs.close();
			return lowest_facid;
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
			return dbs.getScalarAsInt("SELECT FactionID FROM Factions ORDER BY RAND()");
		}
	}	


	public static int GetNumSectors(MySQLConnection dbs, int factionid) throws SQLException {
		return dbs.getScalarAsInt("SELECT Sectors FROM Factions WHERE FactionID = " + factionid);
	}	


	public static int GetWorstFaction_UNUSED(MySQLConnection dbs) throws SQLException {
		int min_sectors = dbs.getScalarAsInt("SELECT MIN(Sectors) FROM Factions WHERE Sectors > 0"); // Don't choose factions with no land
		MyList<Integer> fs = MyList.CreateFromCSVInts(dbs.getListAsString("SELECT FactionID FROM Factions WHERE Sectors = " + min_sectors));
		String id = fs.get(Functions.rnd(0, fs.size()-1)).toString();
		return Integer.parseInt(id);
	}


	public static boolean IsBestFaction(MySQLConnection dbs, int facid) throws SQLException {
		// We do this messing about in case two factions are joint top
		int most_sectors = dbs.getScalarAsInt("SELECT MAX(Sectors) FROM Factions");
		return dbs.getScalarAsInt("SELECT Count(*) FROM Factions WHERE FactionID = " + facid + " AND Sectors = " + most_sectors) > 0;
	}


	public static void SetSectors(MySQLConnection dbs, int id, int amt) throws SQLException {
		dbs.runSQLUpdate("UPDATE Factions SET Sectors = " + amt + " WHERE FactionID = " + id);
	}


	public static void EmailFactionMembers(MySQLConnection dbs, int facid, String subject, String msg, int ignore_loginid) throws SQLException {
		//String DATE_DIFF = "DATEDIFF(curdate(), LastLoginDate) < " + FactionsTable.EXPIRY_DAYS;
		ResultSet rs = dbs.getResultSet("SELECT LoginID FROM Logins WHERE FactionID = " + facid); // + " AND " + DATE_DIFF);
		LoginsTable login = new LoginsTable(dbs);
		while (rs.next()) {
			if (ignore_loginid != rs.getInt("LoginID")) {
				login.selectRow(rs.getInt("LoginID"));
				login.sendEmail(subject, msg);
			}
		}
		login.close();

	}
}
