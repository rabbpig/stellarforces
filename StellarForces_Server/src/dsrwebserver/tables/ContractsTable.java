package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;

public class ContractsTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		dbs.runSQL("CREATE TABLE IF NOT EXISTS Contracts (ContractID INTEGER AUTO_INCREMENT KEY, CreatorLoginID INTEGER, AcceptorLoginID INTEGER, Subject VARCHAR(1024), Body VARCHAR(4096), DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
	}
	
	
}
