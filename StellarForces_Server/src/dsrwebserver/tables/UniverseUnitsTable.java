package dsrwebserver.tables;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.lang.Functions;
import dsrwebserver.DSRWebServer;

public class UniverseUnitsTable extends AbstractTable {

	public static final int STARTING_UNITS = 16;

	public UniverseUnitsTable(MySQLConnection dbs) {
		super(dbs, "UniverseUnits", "UniverseUnitID");
	}


	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("UniverseUnits") == false) {
			dbs.runSQL("CREATE TABLE UniverseUnits (UniverseUnitID INTEGER AUTO_INCREMENT KEY, UniverseUnitCode VARCHAR(32), OrderBy SMALLINT, Name VARCHAR(64), MaxAPs SMALLINT, ShotSkill SMALLINT, CombatSkill SMALLINT, Strength SMALLINT, MaxHealth SMALLINT, RankValue SMALLINT, OwnerID INTEGER, UnitID INTEGER, MaxEnergy SMALLINT, MaxMorale SMALLINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		if (dbs.doesColumnExist("UniverseUnits", "GameID") == false) {
			dbs.runSQL("ALTER TABLE UniverseUnits ADD GameID INTEGER");
		}
	}


	public static int CreateUniverseUnit(MySQLConnection dbs, int owner_id) throws SQLException {
		int APS = 62;
		int HEALTH = 36;
		int ENERGY = 250;
		int SHOT_SKILL = 35;
		int COMBAT_SKILL = 50;
		// Strength must be >= 7 to be able to pick up the rocket launcher
		int STRENGTH = 9;
		int MORALE = 40;
		String name = CampUnitsTable.GetRandomName();
		int id = dbs.RunIdentityInsert_Syncd("INSERT INTO UniverseUnits (UniverseUnitCode, OrderBy, Name, MaxAPs, MaxHealth, MaxEnergy, ShotSkill, CombatSkill, Strength, OwnerID, MaxMorale) VALUES (" + System.currentTimeMillis() + ", 10, " + SQLFuncs.s2sql(name) + ", " + APS + ", " + HEALTH + ", " + ENERGY + ", " + SHOT_SKILL + ", " + COMBAT_SKILL + ", " + STRENGTH + ", " + owner_id + ", " + MORALE + ")");
		UniverseLogTable.AddRec(dbs, owner_id, name + " recruited.", false);
		//this.selectRow(id);
		return id;
	}


	public static int CreateStdUnitFromUniverseUnit(MySQLConnection dbs, int loginid, int uniunitid, int gameid) throws SQLException {
		ResultSet rs = dbs.getResultSet("SELECT * FROM UniverseUnits WHERE UniverseUnitID = " + uniunitid);
		rs.next();
		if (rs.getInt("GameID") > 0) {
			GamesTable game = new GamesTable(dbs);
			game.selectRow(gameid);
			int side = GetSide(dbs, loginid, game);
			int order = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + gameid + " AND Side = " + side)+1;
			UnitsTable unit = new UnitsTable(dbs);
			try {
			return unit.createUnit(gameid, order, getModelTypeFromSide(side), rs.getString("Name"), side, rs.getInt("MaxAPs"), rs.getInt("MaxHealth"), 0, 0, rs.getInt("MaxEnergy"), rs.getInt("ShotSkill"), rs.getInt("CombatSkill"), rs.getInt("Strength"), 1, rs.getInt("MaxMorale"), -1, 1);
			} finally {
				unit.close();
			}
		} else {
			return -1;
		}
	}


	private static int GetSide(MySQLConnection dbs, int loginid, GamesTable game) throws SQLException {
		if (game.getNumOfSides() <= 1) {
			return 1; 
		} else if (game.isPlayerInGame(loginid)) {
			return game.getSideFromPlayerID(loginid);
		} else if (game.getNumOfSides() == 4) {
			throw new RuntimeException("Invalid number of sides!");
		} else {
			// There is space only if there is only one or no comrades in there
			//MyList<Integer> sides_in_game = new MyList<Integer>();
			for (int s=1 ; s<=game.getNumOfSides() ; s++) {
				int lid2 = game.getLoginIDFromSide(s);
				if (UniverseTreatiesTable.AreSidesInTreaty(dbs, loginid, lid2)) {
					if (s == 1) {
						return 2;
					} else if (s == 2) {
						return 1;
					} else if (s == 3) {
						return 4;
					} else if (s == 4) {
						return 3;
					}
				}
			}
			throw new RuntimeException("Error");
		}

	}

	private static int getModelTypeFromSide(int side) {
		switch (side) {
		case 1:
			return UnitsTable.MT_MALE_SIDE_1;
		case 2:
			return UnitsTable.MT_MALE_SIDE_2;
		case 3:
			return UnitsTable.MT_MALE_SIDE_3;
		case 4:
			return UnitsTable.MT_MALE_SIDE_4;
		default:
			throw new RuntimeException("Unknown side: " + side);
		}
	}


	private void incStatsRandomly(int rank_val_inc) throws SQLException { // todo - call this a few times?
		try {
			for(int i=0 ; i<rank_val_inc*7 ; i++) { // Increase one point per stat.
				int stat = Functions.rnd(1, 7);
				String sql = "UPDATE UniverseUnits SET ";
				switch (stat) {
				case 1:
					sql = sql + "Strength = Strength + 1";
					break;
				case 2:
					sql = sql + "MaxAPs = MaxAPs + 1";
					break;
				case 3:
					sql = sql + "MaxHealth = MaxHealth + 1";
					break;
				case 4:
					sql = sql + "MaxEnergy = MaxEnergy + 5";
					break;
				case 5:
					sql = sql + "MaxMorale = MaxMorale + 1";
					break;
				case 6:
					sql = sql + "CombatSkill = CombatSkill + 1";
					break;
				case 7:
					sql = sql + "ShotSkill = ShotSkill + 1";
					break;
				default:
					throw new RuntimeException("Unknown stat num: " + stat);
				}
				sql = sql + " WHERE UniverseUnitID = " + this.getID();
				dbs.runSQLUpdate(sql);
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	public String getName() throws SQLException {
		return rs.getString("Name");
	}



}
