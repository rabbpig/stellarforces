package dsrwebserver.tables;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.lang.Dates;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;

public final class GameLogTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("GameLog") == false) {
			dbs.runSQL("CREATE TABLE GameLog (GameLogID INTEGER AUTO_INCREMENT KEY, GameID INTEGER, LoginID INTEGER, Entry VARCHAR(256), Side TINYINT, TurnNo INTEGER, HideUntilEnd TINYINT, EventTime BIGINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		// Create index on game
		try {
			if (dbs.doesIndexExist("GameLog", "idx_GameLog_GameID") == false) {
				dbs.runSQL("CREATE INDEX idx_GameLog_GameID ON GameLog(GameID)");
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}

	
	/**
	 * loginid is the player who should see the log (or 0 for all).
	 */
	public static void AddRec(MySQLConnection dbs, GamesTable game, int loginid1, int loginid2, String entry, boolean bold, long event_time) throws SQLException {
		AddRec(dbs, game, loginid1, loginid2, entry, bold, false, event_time);
	}
	
	
	public static void AddRec(MySQLConnection dbs, GamesTable game, int loginid1, int loginid2, String entry, boolean bold, boolean hide_till_end, long event_time) throws SQLException {
		try {
			String remaining = "";
			if (entry.length() >= 256) {
				remaining = entry.substring(255, entry.length());
				entry = entry.substring(0, 255);
			}
			if (bold) {
				entry = "<b>" + entry + "</b>";
			}
			dbs.RunIdentityInsert("INSERT INTO GameLog (GameID, LoginID, TurnNo, Entry, HideUntilEnd, EventTime) VALUES (" + game.getID() + ", " + loginid1 + ", " + game.getTurnNo() + ", " + SQLFuncs.s2sql(entry) + ", " + SQLFuncs.b201(hide_till_end) + ", " + event_time + ")");
			if (loginid2 > 0 && loginid1 > 0 && loginid2 != loginid1) { // Only send to login2 if we haven't already sent to all players
				dbs.RunIdentityInsert("INSERT INTO GameLog (GameID, LoginID, TurnNo, Entry, HideUntilEnd, EventTime) VALUES (" + game.getID() + ", " + loginid2 + ", " + game.getTurnNo() + ", " + SQLFuncs.s2sql(entry) + ", " + SQLFuncs.b201(hide_till_end) + ", " + event_time + ")");
			}
			// Tell our comrade?
			try {
				if (loginid1 > 0 && game.getNumOfSides() > 2) { // Only tell comrade if we're not already telling everyone.
					AbstractMission mission = AbstractMission.Factory(game.getMissionID());
					int side = game.getSideFromPlayerID(loginid1);
					int comrade_loginid = game.getComradesLoginID(mission, side);
					if (comrade_loginid > 0) {
						dbs.RunIdentityInsert("INSERT INTO GameLog (GameID, LoginID, TurnNo, Entry, HideUntilEnd, EventTime) VALUES (" + game.getID() + ", " + comrade_loginid + ", " + game.getTurnNo() + ", " + SQLFuncs.s2sql(entry) + ", " + SQLFuncs.b201(hide_till_end) + ", " + event_time + ")");
					}
				}
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}

			
			// Call ourselves to add the rest
			if (remaining.length() > 0) {
				AddRec(dbs, game, loginid1, loginid2, remaining, bold, hide_till_end, event_time);
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}
	}

	
	public static String GetLogEntries(MySQLConnection dbs, GamesTable game, int since_turn_no, int loginid, boolean html, boolean show_hidden) throws SQLException  {
		StringBuffer str = new StringBuffer();
		String sql = "SELECT * FROM GameLog WHERE GameID = " + game.getID() + " AND (COALESCE(LoginID, 0) <= 0 OR LoginID = " + loginid + ")";
		if (since_turn_no > 0) {
			sql = sql + " AND TurnNo >= " + since_turn_no;
		}
		if (show_hidden == false) {
			sql = sql + " AND COALESCE(HideUntilEnd, 0) = 0";
		}
		sql = sql + " ORDER BY DateCreated"; // EventTime,   Don't use EventTime as user's PC times by be out.
		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) {
			String dt = Dates.FormatDate(rs.getTimestamp("DateCreated"), Dates.UKDATE_FORMAT2_WITH_TIME);
			String turn = "";
			if (rs.getInt("TurnNo") > 0) {
				turn = " (Turn " + rs.getInt("TurnNo") + ")" ;
			}
			if (html) {
				str.append(dt + turn + ": " + rs.getString("Entry") + "<br />");
			} else {
				str.append(dt + turn + ": " + rs.getString("Entry").replaceAll("\\<.*?\\>", "") + "\n");
			}
		}
		return str.toString();
	}



}
