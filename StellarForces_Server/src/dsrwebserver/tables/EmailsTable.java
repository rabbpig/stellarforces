package dsrwebserver.tables;

import java.sql.SQLException;
import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

public class EmailsTable {
	
	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		dbs.runSQL("CREATE TABLE IF NOT EXISTS Emails (EmailID INTEGER AUTO_INCREMENT KEY, FromAddress VARCHAR(256), ToAddress VARCHAR(256), Subject VARCHAR(1024), Body VARCHAR(4096), Attempts TINYINT, Sent TINYINT, DateSent DATETIME, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");

		// Index
		if (dbs.doesIndexExist("Emails", "idx_Emails_Sent") == false) {
			dbs.runSQL("create index idx_Emails_Sent ON Emails (Sent)");
		}

		//dbs.runSQL("DELETE FROM Emails WHERE Sent = 1");
	}
	
	
	public static void AddRec(MySQLConnection dbs, String from, String to, String subject, String body) throws SQLException {
		if (body.length() > 4000) {
			body = body.substring(body.length()-4000, body.length());
		}
		dbs.RunIdentityInsert("INSERT INTO Emails (FromAddress, ToAddress, Subject, Body, Attempts) VALUES (" + SQLFuncs.s2sql(from) + ", " + SQLFuncs.s2sql(to) + ", " + SQLFuncs.s2sql(subject) + ", " + SQLFuncs.s2sql(body) + ", 0)");
	}
	
	
	public static void MarkAsSent(MySQLConnection dbs, int id) throws SQLException {
		dbs.runSQLUpdate("DELETE FROM Emails WHERE EmailID = " + id);
		//dbs.runSQLUpdate("UPDATE Emails SET Sent = 1, DateSent = NOW() WHERE EmailID = " + id);
	}

	
	public static void IncAttempts(MySQLConnection dbs, int id) throws SQLException {
		dbs.runSQLUpdate("UPDATE Emails SET Attempts = COALESCE(Attempts, 0) + 1 WHERE EmailID = " + id);
	}

}
