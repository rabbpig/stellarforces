package dsrwebserver.tables;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import ssmith.dbs.MySQLConnection;

public class ForumSubscriptionsTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("ForumSubscriptions") == false) {
			dbs.runSQL("CREATE TABLE ForumSubscriptions (ForumSubscriptionID INTEGER AUTO_INCREMENT KEY, LoginID INTEGER, ForumTopicID INTEGER, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}

		// Index
		if (dbs.doesIndexExist("ForumSubscriptions", "idx_ForumSubscriptions_ForumTopicID") == false) {
			dbs.runSQL("create index idx_ForumSubscriptions_ForumTopicID ON ForumSubscriptions (ForumTopicID)");
		}
	}
	
	
	public static void AddSub(MySQLConnection dbs, int loginid, int topicid) throws SQLException {
		if (dbs.getScalarAsInt("SELECT Count(*) FROM ForumSubscriptions WHERE LoginID = " + loginid + " AND ForumTopicID = " + topicid) == 0) {
			dbs.RunIdentityInsert("INSERT INTO ForumSubscriptions (LoginID, ForumTopicID) VALUES (" + loginid + ", " + topicid + ")");
		}
	}
	
	
	public static boolean IsSubbed(MySQLConnection dbs, int loginid, int topicid) throws SQLException {
		return dbs.getScalarAsInt("SELECT Count(*) FROM ForumSubscriptions WHERE LoginID = " + loginid + " AND ForumTopicID = " + topicid) > 0;
	}
	
	
	public static void RemoveSub(MySQLConnection dbs, int loginid, int topicid) throws SQLException {
		dbs.RunIdentityInsert("DELETE FROM ForumSubscriptions WHERE LoginID = " + loginid + " AND ForumTopicID = " + topicid);
	}
	
	
	public static ArrayList<Integer> GetAllSub(MySQLConnection dbs, int forumid) throws SQLException {
		ArrayList<Integer> al = new ArrayList<Integer>();
		ResultSet rs = dbs.getResultSet("SELECT LoginID FROM ForumSubscriptions WHERE ForumTopicID = " + forumid);
		while (rs.next()) {
			al.add(rs.getInt("LoginID"));
		}
		
		return al;
	}
	
	
}
