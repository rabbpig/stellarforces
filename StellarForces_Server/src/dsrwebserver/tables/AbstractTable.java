package dsrwebserver.tables;

import java.io.Closeable;
import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsrwebserver.DSRWebServer;

public abstract class AbstractTable implements Closeable {
	
	protected MySQLConnection dbs;
	protected ResultSet rs;
	protected boolean has_rows;
	private String name;
	protected String id_field;

	public AbstractTable(MySQLConnection sqldbs, String _name, String _unique_field) {
		dbs = sqldbs;
		name = _name;
		id_field = _unique_field;
		
	}

	
	public boolean hasRow() throws SQLException {
		return has_rows;
	}
	
	
	public boolean doesRowExist(int id) throws SQLException {
		if (rs != null) {
			rs.close();
		}
		rs = dbs.getResultSet("SELECT * FROM " + name + " WHERE " + this.id_field + " = " + id);
		return rs.next();
	}

	
	public void selectRow(int id) throws SQLException {
		if (rs != null) {
			rs.close();
		}
		rs = dbs.getResultSet("SELECT * FROM " + name + " WHERE " + this.id_field + " = " + id);
		has_rows = rs.next();
		if (!has_rows) {
			throw new RuntimeException("Row ID " + id + " not found in table " + name);
		}
	}

	
	public int getID() throws SQLException {
		return rs.getInt(id_field);
	}

	
	public void refreshData() throws SQLException {
		this.selectRow(this.getID());
	}
	
	
	public int getFieldAsInt(String fld) throws SQLException {
		return rs.getInt(fld);
	}

	
	public String getFieldAsString(String fld) throws SQLException {
		return rs.getString(fld);
	}
	
	
	public void delete(int id) throws SQLException {
		if (id == this.getID()) {
			dbs.runSQLDelete("DELETE FROM " + name + " WHERE " + this.id_field + " = " + id);
		} else {
			DSRWebServer.SendEmailToAdmin("ID != id", "ID != id");
		}
	}
	
	
	public static String GetPlayerSubQuery(int loginid) {
		StringBuffer str = new StringBuffer("(");
		for (int i=1 ; i<=4 ; i++) {
			str.append(" Player" + i + "ID = " + loginid + " ");
			if (i<4) {
				str.append(" OR ");
			}
		}
		str.append(") ");
		return str.toString();
	}
	
	
	public static String GetWinningPlayerSubQuery(int loginid) {
		StringBuffer str = new StringBuffer("(");
		for (int i=1 ; i<=4 ; i++) {
			str.append(" (Player" + i + "ID = " + loginid + " AND WinningSide = " + i + ") ");
			if (i<4) {
				str.append(" OR ");
			}
		}
		str.append(") ");
		return str.toString();
	}
	
	
	public static String GetLosingPlayerSubQuery(int loginid) {
		StringBuffer str = new StringBuffer("(");
		for (int i=1 ; i<=4 ; i++) {
			str.append(" (Player" + i + "ID = " + loginid + " AND WinningSide <> " + i + ") ");
			if (i<4) {
				str.append(" OR ");
			}
		}
		str.append(") ");
		return str.toString();
	}
	
	
	public boolean isRowSelected() {
		return this.has_rows;
	}
	
	
	@Override
	public void close() {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
