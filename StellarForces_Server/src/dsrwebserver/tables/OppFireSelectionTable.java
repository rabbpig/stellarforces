package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;

public class OppFireSelectionTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("OppFireSelection") == false) {
			dbs.runSQL("CREATE TABLE OppFireSelection (OppFireSelectionID INTEGER AUTO_INCREMENT KEY, GameID INTEGER, LoginID INTEGER, OpponentSide TINYINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}
	
	
	public static void AddRec(MySQLConnection dbs, int gameid, int loginid, int oppside, boolean oppfire) throws SQLException {
		if (oppfire) {
			dbs.RunIdentityInsert("INSERT INTO OppFireSelection (GameID, LoginID, OpponentSide) VALUES (" + gameid + ", " + loginid + ", " + oppside + ")");
		} else {
			dbs.runSQLDelete("DELETE FROM OppFireSelection WHERE GameID = " + gameid + " AND LoginID = " + loginid + " AND OpponentSide = " + oppside);
		}
	}
	
	
	/**
	 * If there is a rec, then there is opp fire
	 */
	public static boolean IsOppFire(MySQLConnection dbs, int gameid, int loginid, int oppside) throws SQLException {
		return dbs.getScalarAsInt("SELECT Count(*) FROM OppFireSelection WHERE GameID = " + gameid + " AND LoginID = " + loginid + " AND OpponentSide = " + oppside + " LIMIT 1") > 0;
	}
	
}
