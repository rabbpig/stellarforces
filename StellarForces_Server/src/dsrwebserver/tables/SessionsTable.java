package dsrwebserver.tables;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

public final class SessionsTable {

	private MySQLConnection dbs;
	private ResultSet rs;
	private String sid;
	private boolean has_session = false;

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("Sessions") == false) {
			dbs.runSQL("CREATE TABLE Sessions (SessionID INTEGER AUTO_INCREMENT KEY, SessionCookie VARCHAR(32), LoginID INTEGER, IsLoggedIn TINYINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		if (dbs.doesIndexExist("Sessions", "idx_SessionCookie") == false) {
			dbs.runSQL("CREATE INDEX idx_SessionCookie ON Sessions(SessionCookie)");
		}
	}
	

	public SessionsTable(MySQLConnection sqldbs) throws SQLException {
		dbs = sqldbs;
	}

	
	public void selectSessionByCookie(String id) throws SQLException {
		sid = id;
		if (dbs.getScalarAsInt("SELECT Count(*) FROM Sessions WHERE SessionCookie = " + SQLFuncs.s2sql(sid)) == 0) {
			// Create new session rec
			dbs.runSQL("INSERT INTO Sessions (SessionCookie) VALUES (" + SQLFuncs.s2sql(id) + ")");
		}
		this.refreshTable();
	}

	
	public boolean selectSessionByCookie_NoAdd(String id) throws SQLException {
		sid = id;
		boolean res = dbs.getScalarAsInt("SELECT Count(*) FROM Sessions WHERE SessionCookie = " + SQLFuncs.s2sql(sid)) > 0;
		if (res) {
			this.refreshTable();
		}
		return res;
	}

	
	private void refreshTable() throws SQLException {
		rs = dbs.getResultSet("SELECT * FROM Sessions WHERE SessionCookie = " + SQLFuncs.s2sql(sid));
		has_session = rs.next();

	}

	
	public void setLoggedIn(int loginid, boolean b) throws SQLException {
		if (b) {
			dbs.runSQL("UPDATE Sessions SET LoginID = " + loginid + ", IsLoggedIn = " + SQLFuncs.b201(b) + " WHERE SessionCookie = " + SQLFuncs.s2sql(this.getSessionCookie()));
		} else {
			dbs.runSQL("DELETE FROM Sessions WHERE SessionCookie = " + SQLFuncs.s2sql(this.getSessionCookie()));
		}
		this.refreshTable();
	}

	
	public boolean isLoggedIn() throws SQLException {
		if (has_session) {
			return rs.getInt("IsLoggedIn") == 1;
		} else {
			return false;
		}
	}

	
	public String getSessionCookie() throws SQLException {
		return rs.getString("SessionCookie");

	}

	
	public int getLoginID() throws SQLException {
		if (has_session) {
			return rs.getInt("LoginID");
		} else {
			return -1;
		}

	}

}
