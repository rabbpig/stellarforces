package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;

public class StaticDataTable { //extends AbstractTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("StaticData") == false) {
			dbs.runSQL("CREATE TABLE StaticData (StaticDataID INTEGER AUTO_INCREMENT KEY, CampUnitStatsFixed TINYINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
			dbs.RunIdentityInsert("INSERT INTO StaticData (CampUnitStatsFixed) VALUES (0)");
		}
		// todo - drop CampUnitStatsFixed
	}
	
	
}
