package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;

public class LeagueTableTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("LeagueTable") == false) {
			dbs.runSQL("CREATE TABLE LeagueTable (LeagueTableID INTEGER AUTO_INCREMENT KEY, LoginID INTEGER, Played INTEGER, Won INTEGER, Drawn INTEGER, Lost INTEGER, Points INTEGER, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
		}

	}
	
	/*
	public static int AddRec(MySQLConnection dbs, int loginid, int played, int won, int drawn, int lost, int points) throws SQLException {
		return dbs.RunIdentityInsert_Syncd("INSERT INTO LeagueTable (LoginID, Played, Won, Drawn, Lost, Points) VALUES (" + loginid + ", " + played + ", " + won + ", " + drawn + ", " + lost + ", " + points + ")");
	}
	
	
	public static int GetTopPlayer(MySQLConnection dbs) throws SQLException {
		return dbs.getScalarAsInt("SELECT LoginID FROM Logins WHERE COALESCE(Admin, 0) = 0 ORDER BY LeaguePoints DESC LIMIT 1");
	}


	public static String GetTopPlayerName(MySQLConnection dbs) throws SQLException {
		return LoginsTable.GetDisplayName(dbs, GetTopPlayer(dbs));
	}
*/
}
