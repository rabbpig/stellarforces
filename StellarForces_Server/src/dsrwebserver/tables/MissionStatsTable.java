package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;

public class MissionStatsTable extends AbstractTable {
	
	public MissionStatsTable(MySQLConnection sqldbs) {
		super(sqldbs, "MissionStats", "MissionStatID");
	}

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("MissionStats") == false) {
			dbs.runSQL("CREATE TABLE MissionStats (MissionStatID INTEGER AUTO_INCREMENT KEY, MissionID INTEGER, LoginID INTEGER, TotalVictories SMALLINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}
	
	
	public static void ClearStats(MySQLConnection dbs) throws SQLException {
		dbs.runSQLDelete("DELETE FROM MissionStats");
	}
	
	
	public static int GetBestPlayerForMission(MySQLConnection dbs, int mission) throws SQLException {
		//try {
			return dbs.getScalarAsInt("SELECT LoginID FROM MissionStats WHERE MissionID = " + mission + " AND TotalVictories > 0 UNION SELECT -1 ORDER BY 1 DESC");
		/*} catch (SQLException ex) {
			return -1;
		}*/
	}
	
	
	public static int GetTotalVictoriesForPlayerAndMission(MySQLConnection dbs, int mission, int loginid) throws SQLException {
		return dbs.getScalarAsInt("SELECT TotalVictories FROM MissionStats WHERE MissionID = " + mission + " AND LoginID = " + loginid + " UNION SELECT 0");
	}
	
	
	public static void SetPlayer(MySQLConnection dbs, int mission, int loginid, int victories) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO MissionStats (MissionID, LoginID, TotalVictories) VALUES (" + mission + ", " + loginid + ", " + victories + ")");
	}
}
