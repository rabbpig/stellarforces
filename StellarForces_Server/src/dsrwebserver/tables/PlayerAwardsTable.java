package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import dsrwebserver.DSRWebServer;

public class PlayerAwardsTable extends AbstractTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("PlayerAwards") == false) {
			dbs.runSQL("CREATE TABLE PlayerAwards (PlayerAwardID INTEGER AUTO_INCREMENT KEY, LoginIDFrom INTEGER, LoginIDTo INTEGER, Name VARCHAR(255), DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
		}

		// Indexes
		if (dbs.doesIndexExist("PlayerAwards", "idx_PlayerAwards_LoginIDTo") == false) {
			dbs.runSQL("CREATE INDEX idx_PlayerAwards_LoginIDTo ON PlayerAwards (LoginIDTo)");
		}
	}

	public PlayerAwardsTable(MySQLConnection sqldbs) throws SQLException {
		super(sqldbs, "PlayerAwards", "PlayerAwardID");
	}


	public static boolean CanPlayerMakeAward(MySQLConnection dbs, LoginsTable login) throws SQLException {
		try {
			if (login.isAdmin()) {
				return true;
			}
			// Have they awarded a an award in the last 7 days?
			String sql = "SELECT Count(*) FROM PlayerAwards WHERE LoginIDFrom = " + login.getID() + " AND DateDiff(CurDate(), DateCreated) < " + DSRWebServer.DAYS_BETWEEN_AWARDS;
			if (dbs.getScalarAsInt(sql) == 0) {
				// Have they won a game within the last 7 days?
				sql = "SELECT Count(*) FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED + " AND " + AbstractTable.GetWinningPlayerSubQuery(login.getID());
				if (dbs.getScalarAsInt(sql) > 0) {
					sql = "SELECT DateDiff(CurDate(), DateFinished) FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED + " AND " + AbstractTable.GetWinningPlayerSubQuery(login.getID());
					sql = sql + " ORDER BY DateFinished DESC LIMIT 1";
					if (dbs.getScalarAsInt(sql) <= DSRWebServer.DAYS_BETWEEN_AWARDS) {
						//DSRWebServer.p("4");
						return true;
					} else {
						//DSRWebServer.p("3");
					}
				} else {
					//DSRWebServer.p("2");
				}
			} else {
				//DSRWebServer.p("1");
			}
		} catch (Exception ex) {
			//DSRWebServer.HandleError(ex);
		}
		return false;
	}


	public static boolean DoesPlayerHaveAward(MySQLConnection dbs, int loginid) throws SQLException {
		try {
			String sql = "SELECT Count(*) FROM PlayerAwards WHERE LoginIDTo = " + loginid;
			return (dbs.getScalarAsInt(sql) > 0);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
		return false;
	}


	public static void AddRec(MySQLConnection dbs, int from, int to, String name) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO PlayerAwards (LoginIDFrom, LoginIDTo, Name) VALUES (" + from + ", " + to + ", " + SQLFuncs.s2sql(name) + ")");

		try {
			WebsiteEventsTable.AddRec(dbs, LoginsTable.GetDisplayName_Enc(dbs, from, false) + " has awarded another player an award.", -1);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}
	}


}
