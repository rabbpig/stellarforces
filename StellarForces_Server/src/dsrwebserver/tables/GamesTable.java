package dsrwebserver.tables;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.lang.Functions;
import ssmith.util.MyList;
import dsr.AppletMain;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.MainPage;
import dsrwebserver.pages.dsr.leaguetable;

public final class GamesTable extends AbstractTable {

	private String NON_PANIC_OR_TIRED_UNITS_SQL = " ModelType NOT IN (" +  UnitsTable.MT_ALIEN_TYRANT  + ", " + UnitsTable.MT_QUEEN_ALIEN  + ", " + UnitsTable.MT_BLOB  + ", " + UnitsTable.MT_ZOMBIE  + ", " + UnitsTable.MT_ANGEL + ")";
	//private static final int CANCEL_DAYS = 14; // DO NOT READ DIRECTLY!

	// Game status
	public static final int GS_CREATED_DEPLOYMENT = 20; // Must be same as deployment
	public static final int GS_STARTED = 30;
	public static final int GS_FINISHED = 40;

	// Win Types
	public static final int WIN_COMPLETED_MISSION = 1;
	public static final int WIN_KILLED_OPPOSITION = 2;
	public static final int WIN_OPPONENT_CONCEDED = 3;
	public static final int WIN_DRAW = 4;
	public static final int WIN_TIME_EXPIRED = 5;
	public static final int WIN_DRAW_MUTUAL_CONCEDE = 6;
	public static final int WIN_CANCELLED = 7;

	// Special Power
	/*public static final int SP_WALK_THROUGH_WALLS = 1;
	public static final int SP_KNOW_DISTANCE = 2;
	public static final int SP_TELEPORT = 3;
	public static final int SP_CONVERT_ENERGY = 4;
	public static final int MAX_SPECIAL_POWERS = 4;*/
	// ## IF YOU ADD ANY HERE, ADD THEM TO GamesTable on the Android client!


	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("Games") == false) {
			dbs.runSQL("CREATE TABLE Games (GameID INTEGER AUTO_INCREMENT KEY, GameCode VARCHAR(32), Player1ID INTEGER, Player2ID INTEGER, Player3ID INTEGER, Player4ID INTEGER, MapDataID INTEGER, GameStatus TINYINT, Player1Equipped TINYINT, Player2Equipped TINYINT, Player3Equipped TINYINT, Player4Equipped TINYINT, Player1Deployed TINYINT, Player2Deployed TINYINT, Player3Deployed TINYINT, Player4Deployed TINYINT, TurnSide TINYINT, TurnNo SMALLINT, DateLastReminderSend DATETIME, Mission TINYINT, WinningSide TINYINT, WinType TINYINT, DateTurnStarted DATETIME, GameType TINYINT, ForumID INTEGER, Sides TINYINT, DateFinished DATETIME, CanHearEnemies TINYINT, CampGame TINYINT, ProposeConceedLoginID INTEGER, Player1SnafuData INTEGER, Player2SnafuData INTEGER, Player3SnafuData INTEGER, Player4SnafuData INTEGER, PcentCreds INTEGER, WinningSide2 TINYINT, StarmapX TINYINT, StarmapY TINYINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		if (dbs.doesColumnExist("Games", "UniverseGame") == false) {
			dbs.runSQL("ALTER TABLE Games ADD UniverseGame TINYINT");
		}
		for (int s=1 ; s<=4 ; s++) {
			if (dbs.doesColumnExist("Games", "UndercoverAgents" + s) == false) {
				dbs.runSQL("ALTER TABLE Games ADD UndercoverAgents" + s + " TINYINT");
			}
			if (dbs.doesColumnExist("Games", "AIForSide" + s) == false) {
				dbs.runSQL("ALTER TABLE Games ADD AIForSide" + s + " TINYINT");
			}
			if (dbs.doesColumnExist("Games", "Player" + s + "Conceded") == false) {
				dbs.runSQL("ALTER TABLE Games ADD Player" + s + "Conceded TINYINT");
			}
			if (dbs.doesColumnExist("Games", "Player" + s + "ChosenUnits") == false) { // Only used in camp games?
				dbs.runSQL("ALTER TABLE Games ADD Player" + s + "ChosenUnits TINYINT");
			}
			if (dbs.doesColumnExist("Games", "Side" + s + "VPs") == false) {
				dbs.runSQL("ALTER TABLE Games ADD Side" + s + "VPs SMALLINT");
			}
			/*if (DSRWebServer.IsAE()) {
				String field = "Player" + s + "SpecialPower";
				if (dbs.doesColumnExist("Games", field) == false) {
					dbs.runSQL("ALTER TABLE Games ADD " + field + " SMALLINT");
				}
			}*/
			if (dbs.doesColumnExist("Games", "PointsForSide" + s) == false) {
				dbs.runSQL("ALTER TABLE Games ADD PointsForSide" + s + " TINYINT");
			}
			if (dbs.doesColumnExist("Games", "ResPoints" + s) == false) {
				dbs.runSQL("ALTER TABLE Games ADD ResPoints" + s + " INTEGER");
			}
			if (dbs.doesColumnExist("Games", "ELORunningTotal" + s) == false) {
				dbs.runSQL("ALTER TABLE Games ADD ELORunningTotal" + s + " INTEGER");
			}

			// Indexes
			if (dbs.doesIndexExist("Games", "idx_Games_Player" + s + "ID") == false) {
				dbs.runSQL("create index idx_Games_Player" + s + "ID ON Games (Player" + s + "ID)");
			}
		}
		if (dbs.doesColumnExist("Games", "UploadedMapID") == false) {
			dbs.runSQL("ALTER TABLE Games ADD UploadedMapID INTEGER");
		}


	}


	public GamesTable(MySQLConnection sqldbs) throws SQLException {
		super(sqldbs, "Games", "GameID");
	}


	public int getNumUndercoverUnits(int side) throws SQLException {
		return rs.getInt("UndercoverAgents" + side);
	}


	public int getELOPointsForSide(int side) throws SQLException {
		return rs.getInt("PointsForSide" + side);
	}


	public int getELORunningTotalForSide(int side) throws SQLException {
		return rs.getInt("ELORunningTotal" + side);
	}


	public int createGame(int sides, int p1id, int p2id, int p3id, int p4id, int mission, int type, boolean can_hear_enemies, boolean camp_game, int pcent, int map_x, int map_y, int mapid) throws SQLException {
		int id = dbs.RunIdentityInsert_Syncd("INSERT INTO Games (GameCode, Sides, Player1ID, Player2ID, Player3ID, Player4ID, GameStatus, TurnSide, TurnNo, Mission, DateTurnStarted, GameType, CanHearEnemies, CampGame, PcentCreds, StarmapX, StarmapY, UploadedMapID) VALUES (" 
				+ System.currentTimeMillis() + ", " + sides + ", " + p1id + ", " + p2id + ", " + p3id + ", " + p4id + ", " + GS_CREATED_DEPLOYMENT + ", 1, 1, " + mission + ", NOW(), " + type + ", " + SQLFuncs.b201(can_hear_enemies) + ", " + SQLFuncs.b201(camp_game) + ", " + pcent + ", " + map_x + ", " + map_y + ", " + mapid + ")");
		this.selectRow(id);
		GameLogTable.AddRec(dbs, this, -1, -1, "Game created.", false, false, 1);

		/*try {
			if (id >= 10000 && id <= 10002) {
				DSRWebServer.SendEmailToAdmin("10,000 games!", "Game " + id + " has been created.");
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}*/

		return id;
	}


	public int getWaitingForLoginID() throws SQLException {
		int side = this.getWaitingForSide();
		if (side > 0) {
			return this.getLoginIDFromSide(side);
		} else {
			return -1;
		}
	}


	public int getWaitingForSide() throws SQLException {
		if (rs.getInt("GameStatus") == GamesTable.GS_CREATED_DEPLOYMENT) {
			for (int s=1 ; s<=this.getNumOfSides() ; s++) {
				if (this.hasSideBeenSelected(s)) {
					if (this.hasSideEquipped(s) == false) {
						return s;
					}
				}
			}

			for (int s=1 ; s<=this.getNumOfSides() ; s++) {
				if (this.hasSideBeenSelected(s)) {
					if (this.hasSideDeployed(s) == false) {
						return s;
					}
				}
			}
			return -1; // If you get this far, the game is at the wrong status as everyone has equipped and deployed.
		} else if (rs.getInt("GameStatus") == GamesTable.GS_STARTED) { // this.getID()
			return rs.getInt("TurnSide");
		} else {
			throw new RuntimeException("Unknown game status: " + rs.getInt("GameStatus") + " in game " + this.getID() + "!  (Not a problem unless you get loads of these errors: someone has tried to send a reminder for a finished game, or similar)");
		}
	}


	public int getLoginIDOfWaitingForPlayer() throws SQLException {
		return this.getLoginIDFromSide(getWaitingForSide()); // this.getID()
	}


	public void setPlayerID(int side, int loginid) throws SQLException {
		//assert side >= 1 && side <= 4;
		if (side > this.getNumOfSides()) {
			throw new IllegalArgumentException("Invalid side in game " + this.getID() + ": side " + side + " requested");
		}
		dbs.runSQLUpdate("UPDATE Games SET Player" + side + "ID = " + loginid + " WHERE GameID = " + this.getID());
	}


	/*public void setPointsForWin(int side, int pts) throws SQLException {
		assert side >= 1 && side <= 4;
		dbs.runSQLUpdate("UPDATE Games SET PointsForWin" + side + " = " + pts + " WHERE GameID = " + this.getID());
	}*/


	public void setUndercoverAgents(int side, int amt) throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET UndercoverAgents" + side + " = " + amt + " WHERE GameID = " + this.getID());
	}


	public void setELOPointsForSide(int side, int amt) throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET PointsForSide" + side + " = " + amt + " WHERE GameID = " + this.getID());
	}

	public void setELORunningTotalForSide(int side, int amt) throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET ELORunningTotal" + side + " = " + amt + " WHERE GameID = " + this.getID());
	}


	public void reduceUndercoverAgents(int side) throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET UndercoverAgents" + side + " = UndercoverAgents" + side + " - 1 WHERE GameID = " + this.getID());
	}


	public int getUndercoverAgents(int side) throws SQLException {
		return rs.getInt("UndercoverAgents" + side);
	}


	public int getPlayerID(int side) throws SQLException {
		return rs.getInt("Player" + side + "ID");
	}


	public int getPcentCredit() throws SQLException {
		if (rs.getInt("PcentCreds") <= 0) {
			return 100;
		} else {
			return rs.getInt("PcentCreds");
		}
	}


	public boolean hasSideBeenSelected(int side) throws SQLException {
		return rs.getInt("Player" + side + "ID") > 0 || rs.getInt("AIForSide" + side) > 0;
	}


	public int getLoginIDFromSide(int side) throws SQLException {
		return rs.getInt("Player" + side + "ID");
	}


	public int getSideFromPlayerID(int loginid) throws SQLException {
		for (int i=1 ; i<=this.getNumOfSides() ; i++) {
			if (rs.getInt("Player" + i + "ID") == loginid) {
				return i;
			}
		}
		throw new RuntimeException("Player " + loginid + " not found in game " + this.getID());
	}


	public static int GetSideFromPlayerID(MySQLConnection dbs, int gameid, int loginid) throws SQLException {
		ResultSet rs = null;
		try {
			rs = dbs.getResultSet("SELECT * FROM Games WHERE GameID = " + gameid);
			if (rs.next()) {
				for (int s=1 ; s<=4 ; s++) {
					if (rs.getInt("Player" + s + "ID") == loginid) {
						return s;
					}
				}
			}
			throw new RuntimeException("Player " + loginid + " not found in game " + gameid);
		} finally {
			rs.close();
		}
	}


	public void setMapDataID(int id) throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET MapDataID = " + id + " WHERE GameID = " + this.getID());
	}


	public void setSnafuData(int player, int id) throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET Player" + player + "SnafuData = " + id + " WHERE GameID = " + this.getID());
	}


	public static String GetStatusText(int id) {
		switch (id) {
		/*case GS_EQUIP_UNITS:
			return "Equip Units";
		case GS_DEPLOYMENT:
			return "Deployment";*/
		case GS_CREATED_DEPLOYMENT:
			return "Game Created";
		case GS_STARTED:
			return "In Progress";
		case GS_FINISHED:
			return "Game Finished";
		default:
			throw new RuntimeException("Unknown game status: " + id);
		}
	}


	public boolean isAdvancedMode() throws SQLException {
		return rs.getInt("CanHearEnemies") == 1;
	}


	public boolean hasSideChosenUnits(int side) throws SQLException {
		return rs.getInt("Player" + side + "ChosenUnits") == 1;
	}


	public boolean hasSideEquipped(int side) throws SQLException {
		return rs.getInt("Player" + side + "Equipped") == 1;
	}


	public boolean hasSideDeployed(int side) throws SQLException {
		return rs.getInt("Player" + side + "Deployed") == 1;
	}


	public boolean hasSideConceded(int side) throws SQLException {
		return rs.getInt("Player" + side + "Conceded") == 1;
	}


	public void setSideHasEquipped(int side) throws SQLException {//, UnknownHostException, SmtpException, IOException {
		// Check we don't already know this
		ResultSet rs = dbs.getResultSet("SELECT Player" + side + "Equipped FROM Games WHERE GameID = " + this.getID());
		if (rs.next()) {
			if (rs.getInt("Player" + side + "Equipped") != 1) { // Not already equipped
				dbs.runSQLUpdate("UPDATE Games SET Player" + side + "Equipped = 1 WHERE GameID = " + this.getID());
				this.refreshData();

				GameLogTable.AddRec(dbs, this, -1, -1, LoginsTable.GetDisplayName_Enc(dbs, this.getLoginIDFromSide(side), true)  + " (side " + side + ") has equipped.", false, System.currentTimeMillis());
			}
		}

		try {
			WebsiteEventsTable.AddRec(dbs, LoginsTable.GetDisplayName_Enc(dbs, this.getLoginIDFromSide(side), false)  + " has equipped their units in " + AbstractMission.GetMissionNameFromType(this.getMissionID(), false, this.isCampGame()) + ".", this.getLoginIDFromSide(side));
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}

		checkIfAllPlayersEquippedAndDeployed(System.currentTimeMillis());
	}


	public void setSideHasChosenUnits(int side) throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET Player" + side + "ChosenUnits = 1 WHERE GameID = " + this.getID());
		this.refreshData();
	}


	public void setSideHasConceded(int side) throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET Player" + side + "Conceded = 1 WHERE GameID = " + this.getID());
		this.refreshData();
	}


	public void setSideHasDeployed(int side, long event_time) throws SQLException {
		// Check all the units have actually been deployed
		int units_awaiting = 0;
		try {
			units_awaiting = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + this.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_AWAITING_DEPLOYMENT);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}
		if (units_awaiting == 0) {
			// Check we don't already know this
			ResultSet rs = dbs.getResultSet("SELECT Player" + side + "Deployed FROM Games WHERE GameID = " + this.getID());
			if (rs.next()) {
				if (rs.getInt("Player" + side + "Deployed") != 1) {
					dbs.runSQLUpdate("UPDATE Games SET Player" + side + "Deployed = 1 WHERE GameID = " + this.getID());
					this.refreshData();

					GameLogTable.AddRec(dbs, this, -1, -1, LoginsTable.GetDisplayName_Enc(dbs, this.getLoginIDFromSide(side), true)  + " (side " + side + ") has deployed.", false, event_time);
					try {
						WebsiteEventsTable.AddRec(dbs, LoginsTable.GetDisplayName_Enc(dbs, this.getLoginIDFromSide(side), false)  + " has deployed their units in " + AbstractMission.GetMissionNameFromType(this.getMissionID(), false, this.isCampGame()) + ".", this.getLoginIDFromSide(side));
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex, true);
					}
				}
			}
			checkIfAllPlayersEquippedAndDeployed(event_time);
		}
	}


	private void checkIfAllPlayersEquippedAndDeployed(long event_time) throws SQLException {
		boolean all_players_deployed = true;
		for (int i=1 ; i<=this.getNumOfSides() ; i++) {
			if (this.hasSideEquipped(i) == false || this.hasSideDeployed(i) == false) {
				all_players_deployed = false;
				break;
			}
		}

		if (all_players_deployed) {
			dbs.runSQLUpdate("UPDATE Games SET GameStatus = " + GS_STARTED + " WHERE GameID = " + this.getID());
			this.startOfTurn(event_time);
		}

	}


	public int getGameStatus() throws SQLException {
		return rs.getInt("GameStatus");
	}


	public int getTurnNo() throws SQLException {
		return rs.getInt("TurnNo");
	}


	/**
	 * You may want to use getWaitingForSide() instead, as that can handle all game statuses (i.e. equipping, deployment)
	 * @throws SQLException
	 */
	public int getTurnSide() throws SQLException {
		return rs.getInt("TurnSide");
	}


	public int getWinType() throws SQLException {
		return rs.getInt("WinType");
	}


	public MyList<Integer> getWinningSides(MyList<Integer> l) throws SQLException {
		if (l == null) {
			l = new MyList<Integer>();
		} else {
			l.clear();
		}
		if (rs.getInt("WinningSide") > 0) {
			l.add(rs.getInt("WinningSide"));
		}
		if (rs.getInt("WinningSide2") > 0) {
			l.add(rs.getInt("WinningSide2"));
		}
		return l;
	}


	public MyList<Integer> getWinningLoginIDs(MyList<Integer> l) throws SQLException {
		if (l == null) {
			l = new MyList<Integer>();
		} else {
			l.clear();
		}
		if (rs.getInt("WinningSide") > 0) {
			l.add(this.getLoginIDFromSide(rs.getInt("WinningSide")));
		}
		if (rs.getInt("WinningSide2") > 0) {
			l.add(this.getLoginIDFromSide(rs.getInt("WinningSide2")));
		}
		return l;
	}


	public MyList<Integer> getLosingLoginIDs(MyList<Integer> l) throws SQLException { //this.getID()
		if (l == null) {
			l = new MyList<Integer>();
		} else {
			l.clear();
		}
		int sides = this.getNumOfSides();
		for (int side=1 ; side<=sides ; side++) {
			int loginid = this.getLoginIDFromSide(side);
			if (rs.getInt("WinningSide") != side && rs.getInt("WinningSide2") != side) {
				l.add(loginid);
			}
		}
		return l;
	}


	public String getGameCode() throws SQLException {
		return rs.getString("GameCode");
	}


	public int getMissionID() throws SQLException {
		return rs.getInt("Mission");
	}


	public int getNumOfSides() throws SQLException {
		return rs.getInt("Sides");
	}


	public Timestamp getDateStarted() throws SQLException {
		return rs.getTimestamp("DateCreated");
	}


	public Timestamp getDateFinished() throws SQLException {
		return rs.getTimestamp("DateFinished");
	}


	public String getWinTypeAsText() throws SQLException {
		switch (rs.getInt("WinType")) {
		case WIN_COMPLETED_MISSION:
			return "Mission Completed";
		case WIN_KILLED_OPPOSITION:
			return "Opposition Expunged";
		case WIN_OPPONENT_CONCEDED:
			return "Conceded";
		case WIN_DRAW:
			return "Game Drawn";
		case WIN_DRAW_MUTUAL_CONCEDE:
			return "Game Mutually Conceded";
		case WIN_TIME_EXPIRED:
			return "Games Turns Expired";
		case WIN_CANCELLED:
			return "Game Cancelled";
		default:
			throw new RuntimeException("Unknown win type: " + rs.getInt("WinType") + " in game " + this.getID());

		}
	}


	/**
	 * This checks that the player has permission to view a game
	 * @param dbs
	 * @param gameid
	 * @param gamecode
	 * @param loginid
	 * @return
	 * @throws SQLException
	 */
	public static boolean ValidateGame(MySQLConnection dbs, int gameid, int loginid) throws SQLException {
		String sql = "SELECT Count(*) FROM Games WHERE GameID = " + gameid + " AND (";
		//Player1ID = " + loginid + " OR Player2ID = " + loginid + ")";
		for (int s=1 ; s<=4 ; s++) {
			sql = sql + "Player" + s + "ID = " + loginid + " ";
		}
		sql = sql + ")";
		return dbs.getScalarAsInt(sql) > 0;
	}


	public static int GetBattleInProgress(MySQLConnection dbs, int x, int y) throws SQLException {
		String sql = "SELECT GameID FROM Games WHERE GameStatus IN (" + GamesTable.GS_CREATED_DEPLOYMENT + ", " + GamesTable.GS_STARTED + ") AND StarmapX = " + x + " AND StarmapY = " + y;
		ResultSet rs = dbs.getResultSet(sql);
		if (rs.next()) {
			return rs.getInt("GameID");
		} else {
			return -1;
		}
	}


	/**
	 * 
	 * @param side
	 * @return
	 * @throws SQLException
	 */
	public int[] getOppositeSidesForSide(int side) throws SQLException {
		int sides[] = new int[this.getNumOfSides()-1];
		int c = 0;
		for (int i=1 ; i<=this.getNumOfSides() ; i++) {
			if (i != side) {
				sides[c] = i;
				c++;
			}
		}
		return sides;
	}


	public String getOppositeSidesForSideAsCSV(int side) throws SQLException {
		if (this.getNumOfSides() == 1) {
			return "-1";
		}
		int sides[] = this.getOppositeSidesForSide(side);
		StringBuffer str = new StringBuffer();
		for (int i=0 ; i<sides.length ; i++) {
			str.append(sides[i] + ", ");
		}
		str.delete(str.length() - 2, str.length());
		return str.toString();
	}


	/**
	 * 
	 * @param side
	 * @return
	 * @throws SQLException
	 */
	public int[] getOpponentSidesForSide(int side, AbstractMission mission) throws SQLException {
		int sides[] = new int[this.getNumOfSides()-1];
		int c = 0;
		for (int i=1 ; i<=this.getNumOfSides() ; i++) {
			if (mission.getSidesForSide(side).contains(i) == false) {
				sides[c] = i;
				c++;
			}
		}
		return sides;
	}


	public String getOpponentSidesForSideAsCSV(int side, AbstractMission mission) throws SQLException {
		if (this.getNumOfSides() == 1) {
			return "-1";
		}
		int sides[] = this.getOpponentSidesForSide(side, mission);
		StringBuffer str = new StringBuffer();
		for (int i=0 ; i<sides.length ; i++) {
			str.append(sides[i] + ", ");
		}
		str.delete(str.length() - 2, str.length());
		return str.toString();
	}


	/*public String getOpponentSideNumsAsString(int side, AbstractMission mission) throws SQLException {
		if (this.getNumOfSides() == 1) {
			return "-1";
		}
		return mission.getSidesForSide(side).toCSVString();
	}*/


	/** This returns an array of opponent IDs THAT STARTS AT 1!
	 * 
	 * @param our_id
	 * @return
	 * @throws SQLException
	 */
	public int[] getOppositeSidesLoginIDs(int our_id) throws SQLException {
		int sides[] = new int[this.getNumOfSides()];
		int c = 1;
		for (int i=1 ; i<=this.getNumOfSides() ; i++) {
			int loginid = this.getLoginIDFromSide(i);
			if (loginid > 0) {
				if (loginid != our_id) {
					sides[c] = loginid;
					c++;
				}
			}
		}
		return sides;
	}


	public boolean isPlayerInGame(int check_loginid) throws SQLException {
		for (int i=1 ; i<=this.getNumOfSides() ; i++) {
			int loginid = this.getLoginIDFromSide(i);
			if (loginid == check_loginid) {
				return true;
			}
		}
		return false;
	}


	public String getSettings() throws SQLException {
		StringBuffer settings = new StringBuffer();
		if (this.isCampGame()) {
			settings.append("Campaign, ");
		}
		if (this.isPractise()) {
			settings.append("Practise, ");
		}
		if (this.isAdvancedMode()) {
			settings.append("Advanced Mode, ");
		}
		if (settings.length() > 0) {
			settings.delete(settings.length()-2, settings.length());
		}
		return settings.toString();
	}


	public String getOpponentsNamesBySide(int side, boolean link) throws SQLException {
		return getOpponentsNamesBySide(side, link, true);
	}


	public String getOpponentsNamesBySide(int side, boolean link, boolean enc) throws SQLException {
		StringBuffer str = new StringBuffer();
		try {
			for (int i=1 ; i<=this.getNumOfSides() ; i++) {
				if (i != side) {
					int loginid = this.getLoginIDFromSide(i);
					if (enc) {
						str.append(LoginsTable.GetDisplayName_Enc(dbs, loginid, link) + ", ");
					} else {
						str.append(LoginsTable.GetDisplayName(dbs, loginid) + ", ");
					}
				}
			}
		} catch (SQLException ex) {
			throw new SQLException("Error in data for game " + this.getID(), ex);
		}
		if (str.length() > 2) {
			str.delete(str.length()-2, str.length());
		}
		return str.toString();
	}


	public String getPlayersNames(boolean link, boolean enc) throws SQLException {
		StringBuffer str = new StringBuffer();
		try {
			for (int i=1 ; i<=this.getNumOfSides() ; i++) {
				int loginid = this.getLoginIDFromSide(i);
				if (enc) {
					str.append(LoginsTable.GetDisplayName_Enc(dbs, loginid, link) + ", ");
				} else {
					str.append(LoginsTable.GetDisplayName(dbs, loginid) + ", ");
				}
			}
		} catch (SQLException ex) {
			throw new SQLException("Error in data for game " + this.getID(), ex);
		}
		if (str.length() > 2) {
			str.delete(str.length()-2, str.length());
		}
		return str.toString();
	}


	public String getOpponentsNamesByLoginID(int our_loginid, boolean link) throws SQLException {
		StringBuffer str = new StringBuffer();
		for (int i=1 ; i<=this.getNumOfSides() ; i++) {
			if (rs.getInt("AIForSide" + i) > 0) {
				str.append("AI, ");
			} else {
				int loginid = rs.getInt("Player" + i + "ID");
				if (loginid != our_loginid) {
					str.append(LoginsTable.GetDisplayName_Enc(dbs, loginid, link) + ", ");
				}
			}
		}
		if (str.length() > 2) {
			str.delete(str.length()-2, str.length());
		}
		return str.toString();
	}


	public String getNameFromSide(int side) throws SQLException {
		int loginid = this.getLoginIDFromSide(side);
		if (loginid > 0) {
			return LoginsTable.GetDisplayName(dbs, loginid);
		} else {
			if (side <= 2) {
				return "No-one"; // Don't send anything!  Don't throw error though!
			} else {
				return "";
			}
		}
	}


	public void updateDatesToNow() throws SQLException {
		// Fill in date of next players turn
		try {
			dbs.runSQLUpdate("UPDATE Games SET DateTurnStarted = NOW(), DateLastReminderSend = NOW() WHERE GameID = " + this.getID());
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	public void startOfTurn(long event_time) throws SQLException {
		int side = this.getTurnSide();

		// Close all doors
		MapDataTable.CloseAllDoors(dbs, this.getMapDataID());
		MapDataTable.DisperseSmokeAndNerveGas(dbs, this, event_time);

		try {
			// Open doors with units or equipment in the way
			ResultSet rs = dbs.getResultSet("SELECT MapX, MapY FROM Units WHERE GameID = " + this.getID() + " ");
			while (rs.next()) {
				MapDataTable.SetDoorOpen(dbs, this.getMapDataID(), rs.getInt("MapX"), rs.getInt("MapY"), true);
			}
			rs.close();
			rs = dbs.getResultSet("SELECT MapX, MapY FROM Equipment WHERE GameID = " + this.getID() + " AND UnitID <= 0 AND COALESCE(Destroyed, 0) <> 1");
			while (rs.next()) {
				MapDataTable.SetDoorOpen(dbs, this.getMapDataID(), rs.getInt("MapX"), rs.getInt("MapY"), true);
			}
			rs.close();
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

		this.updateDatesToNow();

		this.updateBurden(side);

		if (this.isAdvancedMode()) {
			// Update Energy
			try {
				UnitsTable unit = new UnitsTable(dbs);
				ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE GameID = " + this.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED + " AND " + NON_PANIC_OR_TIRED_UNITS_SQL);
				while (rs.next()) {
					unit.selectRow(rs.getInt("UnitID"));
					/*if (unit.getModelType() != UnitsTable.MT_ALIEN_TYRANT && unit.getModelType() != UnitsTable.MT_QUEEN_ALIEN && unit.getModelType() != UnitsTable.MT_BLOB && unit.getModelType() != UnitsTable.MT_ZOMBIE) {
						continue;
					}*/
					/*if ((this.getMissionID() == AbstractMission.REANIMATOR || this.getMissionID() == AbstractMission.REANIMATOR_4P || this.getMissionID() == AbstractMission.REANIMATOR_PCCLIENT) && unit.getSide() == 2) {
						// Zombies don't get tired!
						continue;
					}*/
					if (unit.getCurrentEnergy() <= 0) { // Already exhausted so inc energy
						unit.incCurrentEnergy(unit.getCurrentAPs());
					} else {
						// Inc or reduce energy based on APs left
						int diff = unit.getCurrentAPs() - (unit.getMaxAPs()/3);
						if (diff > 0) {
							diff = diff * 2; // Recover faster
						}
						unit.incCurrentEnergy(diff);
					}
				}
				unit.close();
				// Ensure energy between 0 and max
				dbs.runSQLUpdate("UPDATE Units SET CurrentEnergy = MaxEnergy WHERE GameID = " + this.getID() + " AND Side = " + side + " AND COALESCE(MaxEnergy, 0) > 0 AND CurrentEnergy > MaxEnergy AND Status = " + UnitsTable.ST_DEPLOYED);
				// Ensure CurrentEnergy isn't less than zero
				dbs.runSQLUpdate("UPDATE Units SET CurrentEnergy = 0 WHERE GameID = " + this.getID() + " AND Side = " + side + " AND COALESCE(MaxEnergy, 0) > 0 AND CurrentEnergy < 0 AND Status = " + UnitsTable.ST_DEPLOYED);
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}

		}

		// Reset AP of opposite side
		dbs.runSQLUpdate("UPDATE Units SET CurrentAPs = MaxAPs - Burden WHERE GameID = " + this.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED);
		try {
			// Update AP's req for Opp Fire SEPERATELY!
			dbs.runSQLUpdate("UPDATE Units SET OppFireAPsReq = CurrentAPs / 2 WHERE GameID = " + this.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED);

			// Halve APs if wounded - after we've calc'd opp fire!  And queen needs lots of APs to lay egg
			dbs.runSQLUpdate("UPDATE Units SET CurrentAPs = CurrentAPs / 2 WHERE ModelType <> " + UnitsTable.MT_BLOB + " AND ModelType <> " + UnitsTable.MT_QUEEN_ALIEN + " AND GameID = " + this.getID() + " AND Side = " + side + " AND Health < (MaxHealth/3) AND Status = " + UnitsTable.ST_DEPLOYED);

			// Min 25 APs to be able to use medikit
			dbs.runSQLUpdate("UPDATE Units SET CurrentAPs = " + AppletMain.APS_USE_EQUIPMENT + " WHERE CurrentAPs < " + AppletMain.APS_USE_EQUIPMENT + " AND GameID = " + this.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED);

			// Halve APs if exhausted - after we've calc'd opp fire!
			try {
				if (this.isAdvancedMode()) {
					// Log exhaustion
					ResultSet rs = dbs.getResultSet("SELECT * FROM Units WHERE GameID = " + this.getID() + " AND Side = " + side + " AND CurrentEnergy <= 0 AND Status = " + UnitsTable.ST_DEPLOYED);
					while (rs.next()) {
						GameLogTable.AddRec(dbs, this, this.getLoginIDFromSide(side), -1, "Unit " + rs.getString("Name") + " is exhausted!", true, event_time);
					}
					dbs.runSQLUpdate("UPDATE Units SET CurrentAPs = CurrentAPs / 2 WHERE GameID = " + this.getID() + " AND Side = " + side + " AND CurrentEnergy <= 0 AND Status = " + UnitsTable.ST_DEPLOYED); 

					// Morale - after we've calc'd opp fire!
					checkForPanicking(side, event_time);
				}
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}

		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}

		this.refreshData();

		// Send email to next player
		int loginid = this.getLoginIDFromSide(side);
		LoginsTable login = new LoginsTable(dbs);
		login.selectRow(loginid);
		String opponents = this.getOpponentsNamesBySide(side, false, false);
		AbstractMission mission = AbstractMission.Factory(this.getMissionID());
		//if (this.getMissionID() != AbstractMission.SF_PRACTISE_MISSION && this.getMissionID() != AbstractMission.SF_PRACTISE_MISSION_WITH_AI && this.getMissionID() != AbstractMission.AE_PRACTISE_MISSION) {
		try {
			if (this.getNumOfSides() > 1) {
				int opp_id = getOppositeSidesLoginIDs(login.getID())[1]; // For checking that their opponent isn't the AI.
				if (opp_id != DSRWebServer.AI_LOGIN_ID) {
					String extra = "";
					login.sendNextTurnEmail(mission.mission_name + " against " + opponents, opponents, mission.mission_name, GameLogTable.GetLogEntries(dbs, this, this.getTurnNo()-1, loginid, false, mission.isSnafu() == false), this.getID(), extra);
				}
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
		login.close();
	}


	private void updateBurden(int side) throws SQLException {
		// Calc each units burden and reduce APs
		ResultSet units = dbs.getResultSet("SELECT UnitID FROM Units WHERE GameID = " + this.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED);
		while (units.next()) {
			int weight = 0;
				weight = EquipmentTable.GetWeightOfArmourAndEquipment(dbs, units.getInt("UnitID"));
			dbs.runSQLUpdate("UPDATE Units SET Burden = " + weight + " WHERE UnitID = " + units.getInt("UnitID"));
		}

	}


	public boolean wasLastTurn() throws SQLException {
		AbstractMission mission = AbstractMission.Factory(this.getMissionID());
		int turn_no = dbs.getScalarAsInt("SELECT TurnNo FROM Games WHERE GameID = " + this.getID());
		return turn_no > mission.getMaxTurns() && mission.getMaxTurns() > 0;
	}


	public void endOfTurn(long event_time, boolean check_ai) throws SQLException {
		int side_that_ended_turn = this.getTurnSide();

		WebsiteEventsTable.AddRec(dbs, LoginsTable.GetDisplayName_Enc(dbs, this.getLoginIDFromSide(side_that_ended_turn), false)  + " has taken their turn in " + AbstractMission.GetMissionNameFromType(this.getMissionID(), false, this.isCampGame()) + ".", this.getLoginIDFromSide(side_that_ended_turn));

		// Store flag location?
		String sql = "SELECT MapX, MapY, UnitID FROM Equipment WHERE GameID = " + this.getID() + " AND EquipmentTypeID = " + EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, "FLAG");
		ResultSet rs2 = dbs.getResultSet(sql);
		while (rs2.next()) {
			int x = rs2.getInt("MapX");
			int y = rs2.getInt("MapY");
			// Is it being carried by a unit?
			if (rs2.getInt("UnitID") > 0) {
				UnitsTable unit = new UnitsTable(dbs);
				unit.selectRow(rs2.getInt("UnitID"));
				x = unit.getMapX();
				y = unit.getMapY();
				unit.close();
			}
			UnitHistoryTable.AddRecord_FlagLocation(dbs, this, x, y, event_time);
		}
		rs2.close();

		// Inc players TotalTurns etc...
		try {
			int loginid = this.getLoginIDFromSide(side_that_ended_turn);
			LoginsTable login = new LoginsTable(dbs);
			login.selectRow(loginid);
			login.incTotalTurns();
			int days = dbs.getScalarAsInt("SELECT DATEDIFF(CURDATE(), DateTurnStarted) FROM Games WHERE GameID = " + this.getID());
			login.incTotalDaysTakingTurns(days);
			login.close();
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

		// Calc who is on opp fire
		dbs.runSQLUpdate("UPDATE Units SET OppFire = 0 WHERE GameID = " + this.getID() + " AND Side = " + side_that_ended_turn);
		dbs.runSQLUpdate("UPDATE Units SET OppFire = 1 WHERE CurrentAPs >= OppFireAPsReq AND GameID = " + this.getID() + " AND Side = " + side_that_ended_turn);

		AbstractMission mission = AbstractMission.Factory(this.getMissionID());
		nextTurnSide(mission);

		GameLogTable.AddRec(dbs, this, -1, -1, LoginsTable.GetDisplayName_Enc(dbs, this.getLoginIDFromSide(side_that_ended_turn), true)  + " (side " + side_that_ended_turn + ") has taken their turn. --- --- --- --- --- --- --- --- --- ---", false, mission.isSnafu(), event_time);

		// Check the next player actually has any units
		for (int i=0 ; i<this.getNumOfSides() ; i++) { // Only do it a certain number of times
			if (this.doesSideHaveUnitsRemaining(this.getTurnSide()) == false) {
				this.nextTurnSide(mission);
			} else {
				break;
			}
		}

		// If the current player is AI, skip them
		if (this.getAIForSide(this.getTurnSide()) > 0 && check_ai) {
			startOfTurn(System.currentTimeMillis()); 
			endOfTurn(System.currentTimeMillis(), false);
		}
	}


	public boolean doesSideHaveUnitsRemaining(int side) throws SQLException {
		return dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + this.getID() + " AND Side = " + side + " And Status = " + UnitsTable.ST_DEPLOYED) > 0;
	}


	public int getPlayersSnafuData(int side) throws SQLException {
		return rs.getInt("Player" + side + "SnafuData");
	}


	public int getSpecialPower(int side) throws SQLException {
		return rs.getInt("Player" + side + "SpecialPower");
	}


	private void removeDeadCampUnits(LoginsTable login) throws SQLException {
		ResultSet rs = dbs.getResultSet("SELECT CampUnitID FROM CampUnits WHERE OwnerID = " + login.getID() + " AND COALESCE(UnitID, 0) > 0");
		CampUnitsTable campunit = new CampUnitsTable(dbs);
		while (rs.next()) {
			campunit.selectRow(rs.getInt("CampUnitID"));
			campunit.deleteIfUnitDead();
		}
		campunit.close();
	}


	private void nextTurnSide(AbstractMission mission) throws SQLException {
		// Reduce ExplodeTurns of primed grenades
		dbs.runSQLUpdate("UPDATE Equipment SET ExplodeTurns = ExplodeTurns - 1 WHERE Primed = 1 AND ExplodeTurns > 0 AND GameID = " + this.getID());

		checkForNewAliens();

		// Inc turn side or turn no
		dbs.runSQLUpdate("UPDATE Games SET TurnSide = TurnSide + 1 WHERE GameID = " + this.getID());
		this.refreshData();
		if (this.getTurnSide() > this.getNumOfSides()) {
			dbs.runSQLUpdate("UPDATE Games SET TurnNo = TurnNo + 1, TurnSide = 1 WHERE GameID = " + this.getID());
			this.refreshData();
			checkForZombies();
			mission.nextTurn(dbs, this);
			this.refreshData();
		}

		mission.nextPhase(dbs, this);

	}


	private void checkForZombies() throws SQLException {
		try {
			// Restore dead on side 2 only after side 1 has taken their turn (so they deon't reappear straight away)
			UnitsTable unit = new UnitsTable(dbs);
			ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE GameID = " + getID() + " AND ModelType = " + UnitsTable.MT_ZOMBIE + " AND Status = " + UnitsTable.ST_DEAD);
			while (rs.next()) {
				if (Functions.rnd(1, 3) == 1) {
					unit.selectRow(rs.getInt("UnitID"));
					unit.setHealth(unit.getMaxHealth());
					unit.setStatus(UnitsTable.ST_DEPLOYED);
					GameLogTable.AddRec(dbs, this, -1, -1, unit.getName() + " has been reanimated.", false, System.currentTimeMillis());

					try {
						// Remove the corpse
						String code = EquipmentTypesTable.CD_HUMAN_CORPSE_SIDE + unit.getSide();
						int mtid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, code);
						dbs.runSQLDelete("DELETE FROM Equipment WHERE GameID = " + getID() + " AND EquipmentTypeID = " + mtid + " AND MapX = " + unit.getMapX() + " AND MapY = " + unit.getMapY());
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex);
					}
				}
			}
			unit.close();
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

	}


	private void checkForNewAliens() throws SQLException {
		try {
			UnitsTable unit = new UnitsTable(dbs);
			ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE GameID = " + getID() + " AND Status = " + UnitsTable.ST_CREATED_MID_MISSION);
			while (rs.next()) {
				unit.selectRow(rs.getInt("UnitID"));
				unit.setStatus(UnitsTable.ST_DEPLOYED);
			}
			unit.close();
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	public int getVPsForSide(int side) throws SQLException {
		int vps = this.rs.getInt("Side" + side + "VPs");
		// Check it's within the boundaries of a byte (for the client)
		if (vps > 100) {
			vps = 100;
		} else if (vps < -100) {
			vps = -100;
		}
		return vps;
	}


	public void recalcVPs() throws SQLException {
		for (int s=1 ; s<=this.getNumOfSides() ; s++) {
			int vps = this.recalcVPsForSide(s, true);
			dbs.runSQLUpdate("UPDATE Games SET Side" + s + "VPs = " + vps + " WHERE GameID = " + this.getID());
		}
		this.refreshData();
	}


	private int recalcVPsForSide(int side, boolean check_combined) throws SQLException {
		if (side > this.getNumOfSides()) { // Not in game
			return 0;
		}
		if (this.getGameStatus() < GamesTable.GS_STARTED) {
			return 0;
		}
		// Has the player conceded?
		if (this.hasSideConceded(side)) {
			return 0;
		}

		int vps = 0;
		AbstractMission mission = AbstractMission.Factory(this.getMissionID());
		if (mission.hasSpecialVPCalc()) {
			vps = mission.getVPs(dbs, this, side);
		}
		if (vps < 100) {
			// Are they the only side left and the other sides can't escape?
			boolean none_can_escape = true;
			boolean all_enemy_units_dead = true;
			//if (mission.getType() == AbstractMission.CLONE_WARS || mission.getType() == AbstractMission.REANIMATOR || mission.getType() == AbstractMission.REANIMATOR_PCCLIENT) {
			/*if (mission.canSideWinByKillingAllOpposition(side) == false) {
				all_dead = false;
			} else {*/
			for (byte s=1 ; s<=mission.getNumOfSides() ; s++) {
				if (mission.getSidesForSide(side).contains(s) == false) { // Don't check our own side(s), e.g. LabAttack, all opponents are killed (SCS 12/11/12)  //if (s != side) {
					if (mission.canUnitsEscape(s)) {
						none_can_escape = false;
						break;
					} else { // This side cannot escape
						// Are all enemies dead
						int c = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + this.getID() + " AND Side = " + s + " AND Status = " + UnitsTable.ST_DEPLOYED);
						if (c > 0) {
							all_enemy_units_dead = false;
							break;
						}
						if (mission.canSideWinByKillingAllOpposition(s) == false) {
							all_enemy_units_dead = false;
							break;
						}
					}
				}
			}
			//}
			if (none_can_escape && all_enemy_units_dead && mission.canSideWinByKillingAllOpposition(side) && mission.isSnafu() == false) {
				vps = 100;
			} else if (mission.hasSpecialVPCalc() == false) {
				// Calc from units killed
				String sql_vps = "SELECT SUM(VPsIfKilled) FROM Units WHERE GameID = " + this.getID() + " AND Side IN (" + this.getOpponentSidesForSideAsCSV(side, mission) + ") AND Status = " + UnitsTable.ST_DEAD;
				//if (this.getNumOfSides() > 2) { // If more than two, check who the killer was - NO, SOMETIMES WE DON'T CARE, E.G. ASSASSINS 2V2
				if (this.getNumOfSides() > 2 && mission.doWeCareWhoKilledUnits()) { // True for LMS, false for most 2v2 missions
					sql_vps = sql_vps + " AND COALESCE(KilledBySide, 0) IN (0, " + side + ")";
				}
				vps = dbs.getScalarAsInt(sql_vps);
				// Add units escaped
				vps += dbs.getScalarAsInt("SELECT SUM(VPsIfEscape) FROM Units WHERE GameID = " + this.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_ESCAPED);

				// Calc from destroyed computers
				int vps_per_cpu = mission.getVPsPerComputer(side);
				if (vps_per_cpu != 0) {
					String sql = "SELECT Count(*) FROM MapDataSquares WHERE MapDataID = " + this.getMapDataID() + " AND SquareType = " + MapDataTable.MT_COMPUTER + " AND Destroyed = 1 ";
					if (mission.doWeCareWhoDestroyedComputers()) { // We care who destroyed them
						sql = sql + "AND COALESCE(DestroyedBySide, 0) IN (0, " + mission.getSidesForSide(side).toCSVString() + ")";
					}
					if (mission.doWeCareWhoOwnsTheComputers()) {
						sql = sql + " AND OwnerSide IN (0, " + getOpponentSidesForSideAsCSV(side, mission) + ")";
					}
					vps += (vps_per_cpu * dbs.getScalarAsInt(sql));
				}

				// Check if flag is at end for capture the flag
				if (mission.doesMissionInvolveFlag()) {
					String sql = "SELECT MapX, MapY, UnitID FROM Equipment WHERE GameID = " + this.getID() + " AND EquipmentTypeID = " + EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_FLAG);
					ResultSet rs = dbs.getResultSet(sql);
					while (rs.next()) { // Loop through all the flags in the game
						int x = rs.getInt("MapX");
						int y = rs.getInt("MapY");
						if (rs.getInt("UnitID") > 0) {
							UnitsTable unit = new UnitsTable(dbs);
							unit.selectRow(rs.getInt("UnitID"));
							x = unit.getMapX();
							y = unit.getMapY();
							unit.close();
						}
						if (side == 1 && x >= MapDataTable.GetMapSize(dbs, this.getMapDataID()).width-1) {
							vps = 100;
						} else if (side == 2 && x == 0) {
							vps = 100;
						} else if (side == 3 && y >= MapDataTable.GetMapSize(dbs, this.getMapDataID()).height-1) {
							vps = 100;
						} else if (side == 4 && y == 0) {
							vps = 100;
						}
					}
				}

				if (check_combined && mission.addComradesVPs(side)) { // Need addComradesVPs() since in, say, MA 2v2, we don't care who killed the units but we don't want to add VPs
					// Combine our VPs with our comrade
					try {
						MyList<Integer> ml_sides = mission.getSidesForSide(side);
						for (int i : ml_sides) {
							if (i != side) {
								vps += this.recalcVPsForSide(i, false);
							}
						}
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex);
					}
				}
			}
		}

		if (vps >= 100) {
			vps = 100;
		}
		return vps;
	}


	public void setAsDraw() throws SQLException {
		setWinType(GamesTable.WIN_DRAW, -1);
	}


	public void restart() throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET GameStatus = " + GamesTable.GS_STARTED + ", WinType = NULL, WinningSide = NULL, WinningSide2 = NULL, DateFinished = NULL WHERE GameID = " + this.getID());
		GameLogTable.AddRec(dbs, this, -1, -1, "GAME HAS BEEN RESTARTED BY ADMIN", true, System.currentTimeMillis());
		this.refreshData();
	}


	/**
	 * @param winning_side is -1 for draws.
	 */
	public void setWinType(int type, int winning_side) throws SQLException {
		if (this.isUniverseGame()) {
			throw new RuntimeException("Universe games cannot be completed.  GID:" + this.getID());
		}

		MainPage.update_data_int.fireInterval(); // Update front page with new finished game

		dbs.runSQLUpdate("UPDATE Games SET GameStatus = " + GamesTable.GS_FINISHED + ", WinType = " + type + ", WinningSide = " + winning_side + ", DateFinished = NOW() WHERE GameID = " + this.getID());
		this.refreshData();

		AbstractMission mission = AbstractMission.Factory(this.getMissionID());

		MyList<Integer> winning_sides = new MyList<Integer>(); // Default no winner in case of draw

		if (winning_side > 0) {
			winning_sides = mission.getSidesForSide(winning_side); 
			// Check for comrades
			try {
				dbs.runSQLUpdate("UPDATE Games SET WinningSide = " + winning_sides.get(0) + " WHERE GameID = " + this.getID());
				GameLogTable.AddRec(dbs, this, -1, -1, LoginsTable.GetDisplayName_Enc(dbs, this.getLoginIDFromSide(winning_sides.get(0)), true)  + " (side " + winning_sides.get(0) + ") has won!", true, System.currentTimeMillis());
				if (winning_sides.size() > 1) {
					dbs.runSQLUpdate("UPDATE Games SET WinningSide2 = " + winning_sides.get(1) + " WHERE GameID = " + this.getID());
					GameLogTable.AddRec(dbs, this, -1, -1, LoginsTable.GetDisplayName_Enc(dbs, this.getLoginIDFromSide(winning_sides.get(1)), true)  + " (side " + winning_sides.get(1) + ") has won!", true, System.currentTimeMillis());
				}
				this.refreshData();
				try {
					MyList<Integer> winning_logins = getWinningLoginIDs(null);
					String winners = LoginsTable.GetDisplayNames_Enc(dbs, winning_logins, false);

					MyList<Integer> losing_logins = getLosingLoginIDs(null);
					String losers = LoginsTable.GetDisplayNames_Enc(dbs, losing_logins, false);
					String s = winners + " has defeated " + losers + " in " + AbstractMission.GetMissionNameFromType(getMissionID(), false, isCampGame()) + ".";

					WebsiteEventsTable.AddRec(dbs, s, -1);
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex, true);
				}
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}
		} else {
			if (this.getNumOfSides() > 1) {
				GameLogTable.AddRec(dbs, this, -1, -1, "The game is a draw!", true, System.currentTimeMillis());
				WebsiteEventsTable.AddRec(dbs, this.getPlayersNames(false, true) + " have drawn in " + AbstractMission.GetMissionNameFromType(this.getMissionID(), false, this.isCampGame()) + ".", -1);
			}
		}

		try {
			if (this.getNumOfSides() > 1) {
				if (this.isCampGame() == false) {
					leaguetable.UpdateGame(dbs, this);
					this.refreshData(); // To get new ELOPoints
				}
				// Send emails
				String extra_text = "\n\nYou can now view the finished game at " + DSRWebServer.WEBSITE_URL + "/dsr/viewfinishedgame.cls?gid=" + this.getID() + " and don't forget you can watch a complete playback of the game on your Android device.\n";// or on the website at " + DSRWebServer.WEBSITE_URL + "/dsr/playbackapplet/playbackpage.cls?gid=" + this.getID() + "\n";
				extra_text = extra_text + "\nIf you like this game, please let others know by leaving a review.  Thanks!  ";
				LoginsTable login = new LoginsTable(dbs);
				for (int s=1 ; s <= this.getNumOfSides() ; s++) {
					int loginid = this.getLoginIDFromSide(s);
					if (loginid > 0 && loginid != DSRWebServer.AI_LOGIN_ID) {
						login.selectRow(loginid);
						String EXTRA_TEXT2 = "\n";
						if (login.getTotalGamesFinished() == 1) {
							EXTRA_TEXT2 = "Also, as it is your first game, your feedback is especially welcome by replying to this email with what you thought the most confusing aspect of the game was for you, so we can change it to help future new players.\n";
						}
						String pts_text = "";
						if (this.isPractise() == false) {
							int pts = this.getELOPointsForSide(s);
							if (pts > 0) {
								pts_text = " and you gained " + pts + " league points";
							} else if (pts < 0) {
								pts_text = " and you lost " + Functions.mod(pts) + " league points";
							}
						}
						String opponents = this.getOpponentsNamesBySide(s, false, false);
						if (type == GamesTable.WIN_DRAW) {
							login.sendEmail("Game Drawn", "Your game against " + opponents + " in " + AbstractMission.GetMissionNameFromType(this.getMissionID(), false, this.isCampGame()) + " has ended in a draw" + pts_text + ".  " + extra_text + EXTRA_TEXT2);
						} else if (type == GamesTable.WIN_DRAW_MUTUAL_CONCEDE) {
							login.sendEmail("Game Mutually Conceded", "Your game against " + opponents + " in " + AbstractMission.GetMissionNameFromType(this.getMissionID(), false, this.isCampGame()) + " has ended with a mutual concede" + pts_text + ".  " + extra_text + EXTRA_TEXT2);
						} else if (winning_sides.contains(s)) {
							if (this.getTurnNo() > 1 || this.getWinType() != GamesTable.WIN_OPPONENT_CONCEDED) {
								login.sendEmail("You Have Won!", "Congratulations!  You have won your game against " + opponents + " in " + AbstractMission.GetMissionNameFromType(this.getMissionID(), false, this.isCampGame()) + "" + pts_text + ".  " + extra_text + EXTRA_TEXT2);
							}
						} else {
							if (this.getTurnNo() > 1 || this.getWinType() != GamesTable.WIN_OPPONENT_CONCEDED) {
								login.sendEmail("Game Lost!", "I'm afraid you have lost your game against " + opponents + " in " + AbstractMission.GetMissionNameFromType(this.getMissionID(), false, this.isCampGame()) + "" + pts_text + ".  " + extra_text + EXTRA_TEXT2);
							}
						}
						if (this.isCampGame()) {
							CampUnitsTable.EnsureEnoughCampUnits(dbs, login);
						}
					}
				}
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}

		// Update camp units
		if (this.isCampGame()) {
			try {
				CampUnitsTable campunit = new CampUnitsTable(dbs);
				for (int side=1 ; side <= this.getNumOfSides() ; side++) {
					ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE GameID = " + this.getID() + " AND Side = " + side + " AND Status <> " + UnitsTable.ST_DEAD);
					while (rs.next()) {
						if (campunit.selectByUnitID(rs.getInt("UnitID"))) {
							campunit.incTotalMissions();
							campunit.setUnitID(0); // So they can be re-deployed
							if (winning_sides.contains(side)) {
								campunit.incTotalVictories();
							}
						} else {
							//DSRWebServer.SendEmailToAdmin("Invalid unit/camp unit", "Game: " + this.getID() + ", side: " + side);
						}
					}
				}
				campunit.close();

				LoginsTable login = new LoginsTable(dbs);
				for (int s=1 ; s <= this.getNumOfSides() ; s++) {
					login.selectRow(this.getLoginIDFromSide(s));
					removeDeadCampUnits(login);
					CampUnitsTable.DetachUnitsAndCampUnitsIfGameFinished(dbs, this.getID(), login);
				}

				CampUnitsTable.RecalcRanks(dbs, true);

			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}

			// Update sector and email players
			try {
				if (winning_sides.size() > 0) {
					int winning_player_id = this.getPlayerID(winning_sides.get(0));
					int losing_player_id = -1;
					for (int side=1 ; side <= this.getNumOfSides() ; side++) {
						if (winning_sides.get(0) != side) {
							losing_player_id = this.getPlayerID(side);
							break;
						}
					}

					int old_facid = StarmapSquaresTable.GetFactionID(dbs, getStarmapX(), getStarmapY());
					StarmapSquaresTable.SetFactionID(dbs, getStarmapX(), getStarmapY(), LoginsTable.GetFactionID(dbs, winning_player_id));
					int new_facid = StarmapSquaresTable.GetFactionID(dbs, getStarmapX(), getStarmapY());
					try {
						if (old_facid == new_facid) {
							// Successfully defended
							FactionsTable.EmailFactionMembers(dbs, old_facid, "Sector Successfully Defended", "Sector " + getStarmapX() + "," + getStarmapY() + " was successfully defended by comrade " + LoginsTable.GetDisplayName(dbs, winning_player_id) + " against " + LoginsTable.GetDisplayName(dbs, losing_player_id) + ".", winning_player_id);
							FactionsTable.EmailFactionMembers(dbs, LoginsTable.GetFactionID(dbs, losing_player_id), "Sector Attack Failed", "The attack on sector " + getStarmapX() + "," + getStarmapY() + " by comrade " + LoginsTable.GetDisplayName(dbs, losing_player_id) + " against " + LoginsTable.GetDisplayName(dbs, winning_player_id) + " was unsuccessful.", winning_player_id);
							StarmapLogTable.AddRec(dbs, LoginsTable.GetDisplayName(dbs, winning_player_id) + " (" + FactionsTable.GetName(dbs, LoginsTable.GetFactionID(dbs, winning_player_id)) + ") has defeated " + LoginsTable.GetDisplayName(dbs, losing_player_id) + " (" + FactionsTable.GetName(dbs, LoginsTable.GetFactionID(dbs, losing_player_id)) + ") and successfully defended sector " + getStarmapX() + "," + getStarmapY() + ".", false);
						} else {
							// Sector overtaken
							FactionsTable.EmailFactionMembers(dbs, old_facid, "Sector Lost!", "Your sector " + getStarmapX() + "," + getStarmapY() + " has been overtaken by the " + FactionsTable.GetName(dbs, LoginsTable.GetFactionID(dbs, winning_player_id)) + " after a successful attack by " + LoginsTable.GetDisplayName(dbs, winning_player_id) + " against " + LoginsTable.GetDisplayName(dbs, losing_player_id) + ".", losing_player_id);
							FactionsTable.EmailFactionMembers(dbs, new_facid, "Sector Won!", "The attack on sector " + getStarmapX() + "," + getStarmapY() + " by comrade " + LoginsTable.GetDisplayName(dbs, winning_player_id) + " against " + LoginsTable.GetDisplayName(dbs, losing_player_id) + " was successful!", losing_player_id);
							/*try {
								// Email other players in other factions
								ResultSet rs = dbs.getResultSet("SELECT FactionID FROM Factions WHERE FactionID NOT IN (" + old_facid + ", " + new_facid + ")");
								while (rs.next()) {
									FactionsTable.EmailFactionMembers(dbs, rs.getInt("FactionID"), "Remote Sector Battle Report", "Enemy sector " + getStarmapX() + "-" + getStarmapY() + " has been overtaken by the " + FactionsTable.GetName(dbs, LoginsTable.GetFactionID(dbs, winning_player_id)) + " after a successful attack by " + LoginsTable.GetDisplayName(dbs, winning_player_id) + ".", -1);
								}
							} catch (Exception ex) {
								DSRWebServer.HandleError(ex);
							}*/

							StarmapLogTable.AddRec(dbs, LoginsTable.GetDisplayName(dbs, winning_player_id) + " (" + FactionsTable.GetName(dbs, LoginsTable.GetFactionID(dbs, winning_player_id)) + ") has defeated " + LoginsTable.GetDisplayName(dbs, losing_player_id) + " (" + FactionsTable.GetName(dbs, LoginsTable.GetFactionID(dbs, losing_player_id)) + ") and successfully overtaken sector " + getStarmapX() + "," + getStarmapY() + ".", false);
						}
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex, true);
					}
				}

			} catch (Exception ex) {
				DSRWebServer.HandleError(ex, true);
			}

		}
	}


	public void cancelGame() throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET GameStatus = " + GamesTable.GS_FINISHED + ", WinType = " + GamesTable.WIN_CANCELLED + ", WinningSide = -1, DateFinished = NOW() WHERE GameID = " + this.getID());
		this.refreshData();

		GameLogTable.AddRec(dbs, this, -1, -1, "The game has been cancelled.", true, System.currentTimeMillis());
	}


	public int getDaysSinceLastReminder() throws SQLException {
		int days = dbs.getScalarAsInt("SELECT DATEDIFF(CURDATE(), COALESCE(DateLastReminderSend, DateTurnStarted)) FROM Games WHERE GameID = " + this.getID());
		return days;
	}


	public void updateDaysSinceLastReminder() throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET DateLastReminderSend = CURDATE() WHERE GameID = " + this.getID());
	}


	public void setForumID(int id) throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET ForumID = " + id + " WHERE GameID = " + this.getID());
	}


	public void setAISide(int side) throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET AIForSide" + side + " = 1 WHERE GameID = " + this.getID());
	}


	public int getAIForSide(int side) throws SQLException {
		return rs.getInt("AIForSide" + side);
	}


	public boolean isSideAI(int side) throws SQLException {
		if (side > 0) {
			return rs.getInt("AIForSide" + side) > 0;
		}
		return false;
	}


	public boolean isAITurn() throws SQLException {
		return this.isSideAI(this.getWaitingForSide());
	}


	public static int GetTotalCurrentGames(MySQLConnection dbs) throws SQLException {
		//return dbs.getScalarAsInt("SELECT Count(*) FROM Games WHERE GameStatus IN (" + GS_EQUIP_UNITS + ", " + GS_DEPLOYMENT + ", " + GS_STARTED + ")");
		return dbs.getScalarAsInt("SELECT Count(*) FROM Games WHERE GameStatus IN (" + GS_CREATED_DEPLOYMENT + ", " + GS_STARTED + ")");
	}


	public int getDaysSinceTurnStarted() throws SQLException {
		return dbs.getScalarAsInt("SELECT DATEDIFF(CURDATE(), DateTurnStarted) FROM Games WHERE GameID = " + this.getID());
	}


	public int getMapDataID() throws SQLException {
		return rs.getInt("MapDataID");
	}


	public int getSnafuData(int side) throws SQLException {
		return rs.getInt("Player" + side + "SnafuData");
	}


	public int getForumID() throws SQLException {
		return rs.getInt("ForumID");
	}


	public int getGameType() throws SQLException {
		return rs.getInt("GameType");
	}


	public int getProposeConcedeLoginID() throws SQLException {
		return rs.getInt("ProposeConceedLoginID");
	}


	public void setProposeMutualConcedeLoginID(int loginid) throws SQLException {
		if (this.isPlayerInGame(loginid) || loginid == 0) {
			dbs.runSQLUpdate("UPDATE Games SET ProposeConceedLoginID = " + loginid + " WHERE GameID = " + this.getID());
			try {
				if (loginid > 0) {
					// Send emails
					int our_side = this.getSideFromPlayerID(loginid);
					sendEmailToOppositeSides(our_side, "Mutual Concede Proposed", "Your opponent " + LoginsTable.GetDisplayName_Enc(dbs, loginid, false) + " in " + AbstractMission.GetMissionNameFromType(this.getMissionID(), false, this.isCampGame()) + " has proposed mutually conceding the game.  Please visit the website for more details.");
					GameLogTable.AddRec(dbs, this, 0, 0, LoginsTable.GetDisplayName_Enc(dbs, loginid, false) + " has proposed mutually conceding the game", true, System.currentTimeMillis());
				} else {
					GameLogTable.AddRec(dbs, this, 0, 0, "The proposed mutual conceding of the game has been cancelled.", true, System.currentTimeMillis());
				}
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}
		}
	}


	public void sendEmailToOppositeSides(int our_side, String subject, String body) throws SQLException {
		LoginsTable login = new LoginsTable(dbs);
		int sides[] = this.getOppositeSidesForSide(our_side);
		for (int i=0 ; i<sides.length ; i++) {
			int loginid = this.getLoginIDFromSide(sides[i]);
			if (loginid > 0) {
				login.selectRow(loginid);
				login.sendEmail(subject, body);
			}
		}
		login.close();
	}


	public boolean isPractise() throws SQLException {
		return rs.getInt("GameType") == GameRequestsTable.GT_PRACTISE;
	}


	public boolean isCampGame() throws SQLException {
		return rs.getInt("CampGame") == 1;
	}


	public boolean isUniverseGame() throws SQLException {
		return rs.getInt("UniverseGame") == 1;
	}


	public void conceedGame(int winner_loginid) throws SQLException {
		this.setWinType(GamesTable.WIN_OPPONENT_CONCEDED, this.getSideFromPlayerID(winner_loginid));
	}


	public boolean haveAllPlayersJoined() throws SQLException {
		for (int s=1 ; s<=this.getNumOfSides() ; s++) {
			if (this.hasSideBeenSelected(s) == false) {
				return false;
			}
		}
		return true;
	}


	public static boolean DoesPlayerHaveActions(MySQLConnection dbs, int loginid) throws SQLException {
		String sql = "SELECT Count(*) FROM Games WHERE (";
		for (int s=1 ; s<=4 ; s++) {
			sql = sql + "(GameStatus = " + GamesTable.GS_CREATED_DEPLOYMENT + " AND Player" + s + "ID = " + loginid + " AND COALESCE(Player" + s + "Equipped, 0) <> 1)";
			sql = sql + " OR (GameStatus = " + GamesTable.GS_CREATED_DEPLOYMENT + " AND Player" + s + "ID = " + loginid + " AND COALESCE(Player" + s + "Deployed, 0) <> 1)";
			sql = sql + " OR (GameStatus = " + GamesTable.GS_STARTED + " AND Player" + s + "ID = " + loginid + " AND TurnSide = " + s + ")";
			if (s<4) {
				sql = sql + " OR ";
			}
		}
		sql = sql + ") AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI + " LIMIT 1";
		return dbs.getScalarAsInt(sql) > 0;
	}


	public static boolean DoesPlayerHaveGames(MySQLConnection dbs, int loginid) throws SQLException {
		String sql = "SELECT Count(*) FROM Games WHERE GameStatus <> " + GamesTable.GS_FINISHED + " AND " + AbstractTable.GetPlayerSubQuery(loginid) + " LIMIT 1";
		return dbs.getScalarAsInt(sql) > 0;
	}


	public int getPhaseNo() throws SQLException {
		int phase_no = (this.getTurnNo()*this.getNumOfSides())+this.getTurnSide();
		return phase_no;
	}


	public void removePlayersUnits(int loginid) throws SQLException {
		try {
			int side = GamesTable.GetSideFromPlayerID(dbs, this.getID(), loginid);
			boolean give_units_to_comrade = false;
			AbstractMission mission = AbstractMission.Factory(this.getMissionID());
			MyList<Integer> sides = mission.getSidesForSide(side);
			if (sides.size() > 1) {
				give_units_to_comrade = true;
			}
			if (give_units_to_comrade == false) {
				// In the next line, note that we only change the status of deployed units.
				// Otherwise, other players lose VPs if they've killed some of this player's units.
				// Note we don't change them to NOT EXIST since in SNAFU missions they need to be dead
				// to trigger then end of the game
				dbs.runSQLUpdate("UPDATE Units SET Status = " + UnitsTable.ST_DEAD + " WHERE GameID = " + this.getID() + " AND Side = " + side + " AND Status IN (" + UnitsTable.ST_AWAITING_DEPLOYMENT + ", " + UnitsTable.ST_DEPLOYED + ")");
			} else {
				sides.removeInt(side);
				dbs.runSQLUpdate("UPDATE Units SET Side = " + sides.get(0) + " WHERE GameID = " + this.getID() + " AND Side = " + side);
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	public int getCancelDays() throws SQLException {
		if (this.isCampGame()) {
			return 3;
		} else if (this.getNumOfSides() > 2) {
			return 7;
		} else {
			return 14;
		}
	}


	public int getSabSide() throws SQLException {
		for (int i=1 ; i<=this.getNumOfSides() ; i++) {
			if (getSnafuData(i) > 0) {
				return i;
			}
		}
		return -1;
	}


	/*public static void DeleteGame(MySQLConnection dbs, int gameid) throws SQLException {
		dbs.runSQLDelete("DELETE FROM Games WHERE GameID = " + gameid);
		dbs.runSQLDelete("DELETE FROM Units WHERE GameID = " + gameid);
		DSRWebServer.SendEmailToAdmin("Game " + gameid + " Deleted", "Game " + gameid + " Deleted");
		// Del everything else like equipment
		//int mapdataid = dbs.getScalarAsInt("SELECT ");
		//dbs.runSQLDelete("DELETE FROM MapData WHERE GameID = " + gameid);
		//dbs.runSQLDelete("DELETE FROM MapDataSquares WHERE GameID = " + gameid);
	}*/


	/**
	 * If this_side == false, it means all sides *except* the selected side.
	 */
	public void adjustSidesMorale(boolean this_side, int side, int amt) throws SQLException {
		try {
			/*if (amt < 0 && (this.getMissionID() == AbstractMission.REANIMATOR || this.getMissionID() == AbstractMission.REANIMATOR_PCCLIENT) && side == 2) {
				// Zombies don't get scared!
				return;
			}*/
			dbs.runSQLUpdate("UPDATE Units SET CurrentMorale = CurrentMorale + " + amt + " WHERE GameID = " + this.getID() + " AND Side " + (this_side?"=":"<>") + " " + side + " AND Status = " + UnitsTable.ST_DEPLOYED + " AND " + NON_PANIC_OR_TIRED_UNITS_SQL);//ModelType <> " + UnitsTable.MT_ALIEN_TYRANT + " AND ModelType <> " + UnitsTable.MT_QUEEN_ALIEN + " AND ModelType <> " + UnitsTable.MT_BLOB + " AND ModelType <> " + UnitsTable.MT_ZOMBIE);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	private void checkForPanicking(int side, long event_time) throws SQLException {
		try {
			// Set panicked flag and adjust stats
			dbs.runSQLUpdate("UPDATE Units SET Panicked = 0 WHERE GameID = " + this.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED);

			UnitsTable unit = new UnitsTable(dbs);
			ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE GameID = " + this.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED + " AND CurrentMorale <= 0 AND " + NON_PANIC_OR_TIRED_UNITS_SQL);
			while (rs.next()) {
				unit.selectRow(rs.getInt("UnitID"));
				unit.dropCurrentItem();
				GameLogTable.AddRec(dbs, this, this.getLoginIDFromSide(side), -1, unit.getName() +  " has panicked!", true, event_time);
				//DSRWebServer.SendEmailToAdmin("Unit Panicked", "Unit " + unit.getName() + " in game " + this.getID() + " has panicked.");
			}
			unit.close();

			// Must be after we've logged it!
			dbs.runSQLUpdate("UPDATE Units SET Panicked = 1, CurrentAPs = CurrentAPs / 2, CurrentMorale = " + UnitsTable.SCARED_LEVEL + " WHERE GameID = " + this.getID() + " AND Side = " + side + " AND Status = " + UnitsTable.ST_DEPLOYED + " AND CurrentMorale <= 0 AND " + NON_PANIC_OR_TIRED_UNITS_SQL);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}
	}


	public int getStarmapX() throws SQLException {
		return rs.getInt("StarmapX");
	}


	public int getStarmapY() throws SQLException {
		return rs.getInt("StarmapY");
	}


	public int getResPointsForSide(int s) throws SQLException {
		return rs.getInt("ResPoints" + s);
	}


	public int getComradesLoginID(AbstractMission mission, int our_side) throws SQLException {
		if (this.getNumOfSides() > 2) {
			MyList<Integer> sides = mission.getSidesForSide(our_side);
			if (sides.size() > 1) {
				sides.removeInt(our_side);
				return this.getLoginIDFromSide(sides.get(0));
			} 
		}
		return -1;

	}


	@Override
	public String toString() {
		if (this.isRowSelected()) {
			try {
				return "Game_" + this.getID();
			} catch (SQLException e) {
				return "Game_RowUnselected (SQL error)";
			}
		} else {
			return "Game_RowUnselected";
		}
	}


	/*public static String SpecialPower2String(int type) {
		switch (type) {
		case SP_WALK_THROUGH_WALLS: return "Walk Through Walls";
		case SP_KNOW_DISTANCE: return "Know Distance";
		case SP_TELEPORT: return "Teleport";
		case SP_CONVERT_ENERGY: return "Convert Energy";
		default: return "Unknown!";
		}
	}*/


	public boolean wasAgainstAI() throws SQLException {
		for (int i=1 ; i<=this.getNumOfSides() ; i++) {
			if (this.getLoginIDFromSide(i) == DSRWebServer.AI_LOGIN_ID) {
				return true;
			}
		}
		return false;

	}


	public void setResourcePointsForSide(int side, int amt) throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET ResPoints" + side + " = " + amt + " WHERE GameID = " + this.getID());
		this.refreshData();
	}


	public void setAsPractise() throws SQLException {
		dbs.runSQLUpdate("UPDATE Games SET GameType = " + GameRequestsTable.GT_PRACTISE + " WHERE GameID = " + this.getID());
	}


	public boolean checkAndReduceResourcePointsForSide(int side, int amt) throws SQLException {
		int rps = this.getResPointsForSide(side);
		if (rps >= amt) {
			this.setResourcePointsForSide(side, rps-amt);
			return true;
		} else {
			return false;
		}

	}


	public int getUploadedMapID() throws SQLException {
		return rs.getInt("UploadedMapID");
	}


}


