package dsrwebserver.tables;

import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.lang.Functions;
import dsr.TextureStateCache;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.ServerMapSquare;

public final class MapDataTable {

	// Major types
	public static final byte MT_NOTHING = 0;
	public static final byte MT_FLOOR = 1;
	public static final byte MT_COMPUTER = 2;
	public static final byte MT_WALL = 3;

	// Doors
	public static final byte DOOR_NS = 1;
	public static final byte DOOR_EW = 2;

	// Scenery codes
	public static final byte FLOWERPOT = 1;
	public static final byte FLOWERPOT2 = 2;
	public static final byte BOOKSHELF = 3;
	public static final byte CHAIR = 4;
	public static final byte COMPUTER_TABLE = 5;
	public static final byte DESK = 6;
	public static final byte METAL_SHELF = 7;
	public static final byte SMOKE_FX = 8;
	public static final byte SPARKS = 9;
	public static final byte HEDGEROW = 10;
	public static final byte SECTOR1 = 11;
	public static final byte SMALL_POST = 12;
	public static final byte BARREL1 = 13;
	public static final byte SECTOR2 = 14;
	public static final byte SECTOR3 = 15;
	public static final byte SECTOR4 = 16;
	public static final byte BRICK_WALL = 17;
	public static final byte PIPES_L = 18;
	public static final byte PIPES_R = 19;
	public static final byte RUBBLE = 20;
	public static final byte RUBBLE_RED = 21;
	public static final byte RUBBLE_YELLOW = 22;
	public static final byte RUBBLE_WHITE = 23;
	public static final byte CHAIR_T = 24;
	public static final byte CHAIR_B = 25;
	public static final byte CHAIR2_L = 26;
	public static final byte CHAIR2_R = 27;
	public static final byte CRYO_CHAMBER = 28;
	public static final byte COMPUTER_SCREEN1 = 29;
	public static final byte PLANT3 = 30;
	public static final byte RED_WALL_PANEL = 31;
	public static final byte GREEN_WALL_PANEL = 32;
	public static final byte DAMAGED_FLOOR = 33;
	public static final byte DAMAGED_FLOOR2 = 34;
	public static final byte GRILL = 35;
	public static final byte SINGLE_PIPE_L = 36;
	public static final byte SINGLE_PIPE_R = 37;
	public static final byte HORIZONTAL_PIPE = 38;
	public static final byte PLANT4 = 39;
	public static final byte PLANT5 = 40;
	public static final byte PLANT6 = 41;
	public static final byte PLANT7 = 42;
	public static final byte REPLICATOR = 43;
	public static final byte CRACK1 = 44;
	public static final byte CRACK2 = 45;
	public static final byte WEED1 = 46;
	public static final byte WEED2 = 47;
	public static final byte GRAFFITI_DIE = 48;
	public static final byte GRAFFITI_HELP = 49;
	public static final byte GRAFFITI_BRAINS = 50;
	public static final byte BARREL2 = 51;
	public static final byte DAMAGED_FLOOR3 = 52;
	public static final byte DAMAGED_FLOOR4 = 53;
	public static final byte DAMAGED_FLOOR5 = 54;
	public static final byte DAMAGED_FLOOR6 = 55;
	public static final byte DAMAGED_FLOOR7 = 56;
	public static final byte BED_TOP = 57;
	public static final byte BED_BOTTOM = 58;
	// If you add any here, add them to MapDataTable in the Android client

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("MapData") == false) {
			dbs.runSQL("CREATE TABLE MapData (MapDataID INTEGER AUTO_INCREMENT KEY, GameID INTEGER, Width SMALLINT, Height SMALLINT, FullyCreated TINYINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}


		if (dbs.doesTableExist("MapDataSquares") == false) {
			dbs.runSQL("CREATE TABLE MapDataSquares (MapDataSquareID INTEGER AUTO_INCREMENT KEY, MapDataID INTEGER, MapX SMALLINT, MapY SMALLINT, SquareType TINYINT, DeploymentSquareSide TINYINT, DoorType TINYINT, EscapeHatch TINYINT, FloorTex SMALLINT, Destroyed TINYINT, OwnerSide TINYINT, DoorOpen TINYINT, DestroyedBySide TINYINT, SmokeType TINYINT, SmokeCausedBy INTEGER, SmokeTurnCount SMALLINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		// ## IF YOU ADD ANY TO THIS, ADD THEM TO MapDataSquaresStart !! ###
		if (dbs.doesTableExist("MapDataSquaresStart") == false) {
			dbs.runSQL("CREATE TABLE MapDataSquaresStart (MapDataSquareStartID INTEGER AUTO_INCREMENT KEY, MapDataID INTEGER, MapX SMALLINT, MapY SMALLINT, SquareType TINYINT, DoorType TINYINT, EscapeHatch TINYINT, FloorTex SMALLINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		// TRY NOT TO ADD ANY FIELDS TO THIS UNLESS THEY ARE GOING TO BE USED BY ALL SQUARES; USE SpecialMapDataSquares INSTEAD


		// Squares with scenery etc...
		if (dbs.doesTableExist("SpecialMapDataSquares") == false) {
			dbs.runSQL("CREATE TABLE SpecialMapDataSquares (SpecialMapDataSquareID INTEGER AUTO_INCREMENT KEY, MapDataID INTEGER, MapX SMALLINT, MapY SMALLINT, SceneryCode SMALLINT, SceneryExtraData SMALLINT, RaisedFloorTex SMALLINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		// ## IF YOU ADD ANY TO THIS, ADD THEM TO SpecialMapDataSquaresStart !! ###
		if (dbs.doesTableExist("SpecialMapDataSquaresStart") == false) {
			dbs.runSQL("CREATE TABLE SpecialMapDataSquaresStart (SpecialMapDataSquaresStartID INTEGER AUTO_INCREMENT KEY, MapDataID INTEGER, MapX SMALLINT, MapY SMALLINT, SceneryCode SMALLINT, SceneryExtraData SMALLINT, RaisedFloorTex SMALLINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}

		try {
			if (dbs.doesIndexExist("MapDataSquares", "idx_MapDataSquares_MapDataID_MapX_MapY") == false) {
				dbs.runSQL("CREATE INDEX idx_MapDataSquares_MapDataID_MapX_MapY ON MapDataSquares(MapDataID, MapX, MapY)");
			}
			if (dbs.doesIndexExist("SpecialMapDataSquares", "idx_SpecialMapDataSquares_MapDataID_MapX_MapY") == false) {
				dbs.runSQL("CREATE INDEX idx_SpecialMapDataSquares_MapDataID_MapX_MapY ON SpecialMapDataSquares(MapDataID, MapX, MapY)");
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

	}


	public static int CreateRec(MySQLConnection dbs, int gid) throws SQLException {
		return dbs.RunIdentityInsert_Syncd("INSERT INTO MapData (GameID) VALUES (" + gid + ")");
	}


	public static void SetSize(MySQLConnection dbs, int mid, int w, int h) throws SQLException {
		dbs.runSQLUpdate("UPDATE MapData SET Width = " + w + ", Height = " + h + " WHERE MapDataID = " + mid);
	}


	public static Dimension GetMapSize(MySQLConnection dbs, int mid) throws SQLException {
		ResultSet rs = dbs.getResultSet("SELECT * FROM MapData WHERE MapDataID = " + mid);
		if (rs.next()) {
			return new Dimension(rs.getInt("Width"), rs.getInt("Height"));
		} else {
			throw new RuntimeException("Map not found: " + mid);
		}
	}


	public static int GetMapIDFromGameID(MySQLConnection dbs, int gid) throws SQLException {
		return dbs.getScalarAsInt("SELECT MapDataID FROM MapData WHERE GameID = " + gid);
	}


	/**
	 * This should only be called when the map is initially generated!
	 */
	public static void CreateMapSquare(MySQLConnection dbs, int mid, ServerMapSquare sq) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO MapDataSquares (MapDataID, MapX, MapY, SquareType, DoorType, DeploymentSquareSide, FloorTex, EscapeHatch, Destroyed, OwnerSide) VALUES (" + mid + ", " + sq.x + ", " + sq.y + ", " + sq.major_type + ", " + sq.door_type + ", " + sq.deploy_sq_side + ", " + sq.texture_code + ", " + sq.escape_hatch_side + ", " + sq.destroyed + ", " + sq.owner_side + ")");
		if (sq.scenery_code > 0 || sq.raised_texture_code > 0) {
			dbs.RunIdentityInsert("INSERT INTO SpecialMapDataSquares (MapDataID, MapX, MapY, SceneryCode, SceneryExtraData, RaisedFloorTex) VALUES (" + mid + ", " + sq.x + ", " + sq.y + ", " + sq.scenery_code + ", " + sq.scenery_direction + ", " + sq.raised_texture_code + ")");
		}

		// Create backup map
		try {
			dbs.RunIdentityInsert("INSERT INTO MapDataSquaresStart (MapDataID, MapX, MapY, SquareType, DoorType, FloorTex, EscapeHatch) VALUES (" + mid + ", " + sq.x + ", " + sq.y + ", " + sq.major_type + ", " + sq.door_type + ", " + sq.texture_code + ", " + sq.escape_hatch_side + ")");
			if (sq.scenery_code > 0 || sq.raised_texture_code > 0) {
				dbs.RunIdentityInsert("INSERT INTO SpecialMapDataSquaresStart (MapDataID, MapX, MapY, SceneryCode, SceneryExtraData, RaisedFloorTex) VALUES (" + mid + ", " + sq.x + ", " + sq.y + ", " + sq.scenery_code + ", " + sq.scenery_direction + ", " + sq.raised_texture_code + ")");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public static void SetMapSquareDestroyed(MySQLConnection dbs, int sqid, int destroyed, int by_side) throws SQLException {
		dbs.runSQLUpdate("UPDATE MapDataSquares SET Destroyed = " + destroyed + " WHERE MapDataSquareID = " + sqid);
		if (by_side > 0) {
			dbs.runSQLUpdate("UPDATE MapDataSquares SET DestroyedBySide = " + by_side + " WHERE COALESCE(DestroyedBySide, 0) = 0 AND MapDataSquareID = " + sqid);
		}
	}


	public static void UpdateMapSquare(MySQLConnection dbs, GamesTable game, int sqid, int mapx, int mapy, int type, int tex, int door_type, int door_open, int smoke_type, int smoke_caused_by, int new_owner, int destroyed, int raised_tex_code, int scenery_code, long event_time) throws SQLException {
		try {
			// If it's a new smoke, set the SmokeTurnCount to num_sides-1
			if (smoke_type > 0) {
				dbs.runSQLUpdate("UPDATE MapDataSquares SET SmokeTurnCount = " + (game.getNumOfSides()-1) + " WHERE MapDataSquareID = " + sqid + " AND COALESCE(SmokeType, 0) = 0");
				//try {
					// Add history
					if (smoke_type == EquipmentTypesTable.ET_SMOKE_GRENADE) {
						UnitHistoryTable.AddRecord_Smoke(dbs, game, mapx, mapy, event_time, true);
					} else if (smoke_type == EquipmentTypesTable.ET_NERVE_GAS) {
						UnitHistoryTable.AddRecord_NerveGas(dbs, game, mapx, mapy, event_time, true);
					} else {
						UnitHistoryTable.AddRecord_Fire(dbs, game, mapx, mapy, event_time, true);
					}
				/*} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}*/
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
		dbs.runSQLUpdate("UPDATE MapDataSquares SET SquareType = " + type + ", FloorTex = " + tex + ", DoorType = " + door_type + ", DoorOpen = " + door_open + ", SmokeType = " + (smoke_type>=0?smoke_type:"SmokeType") + ", SmokeCausedBy = " + (smoke_caused_by>=0?smoke_caused_by:"SmokeCausedBy") + ", Destroyed = " + destroyed + " WHERE MapDataSquareID = " + sqid);
		if (new_owner > 0) {
			dbs.runSQLUpdate("UPDATE MapDataSquares SET OwnerSide = " + new_owner  + " WHERE MapDataSquareID = " + sqid);
		}
		try {
			dbs.runSQLUpdate("UPDATE SpecialMapDataSquares SET RaisedFloorTex = " + raised_tex_code + ", SceneryCode = " + scenery_code + " WHERE MapDataID = " + game.getID() + " AND MapX = " + mapx + " AND MapY = " + mapy);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	public static int GetMapSquareType(MySQLConnection dbs, int mid, int x, int y) throws SQLException {
		return dbs.getScalarAsInt("SELECT SquareType FROM MapDataSquares WHERE MapDataID = " + mid + " AND mapX = " + x + " AND MapY = " + y);
	}


	public static void DisperseSmokeAndNerveGas(MySQLConnection dbs, GamesTable game, long event_time) throws SQLException {
		try {
			ResultSet rs = dbs.getResultSet("SELECT MapDataSquareID, MapX, MapY, SmokeType, SmokeCausedBy, SmokeTurnCount FROM MapDataSquares WHERE MapDataID = " + game.getMapDataID() + " AND SmokeType > 0");
			while (rs.next()) {
				if (rs.getInt("SmokeTurnCount") <= 0) {
					int max = game.getNumOfSides() * 4; 
					int rnd = Functions.rnd(1, max); // 8
					if (rs.getInt("SmokeType") == EquipmentTypesTable.ET_SMOKE_GRENADE) {
						rnd++; // Less chance of disappearing
					} else if (rs.getInt("SmokeType") == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
						rnd++; // Less chance of disappearing
					}
					switch (rnd) {
					case 1:
					case 2:
					case 3:
						// Disappears
						dbs.runSQLUpdate("UPDATE MapDataSquares SET SmokeType = 0, SmokeCausedBy = 0 WHERE MapDataSquareID = " + rs.getInt("MapDataSquareID"));
						// Add history
						if (rs.getInt("SmokeType") == EquipmentTypesTable.ET_SMOKE_GRENADE) {
							UnitHistoryTable.AddRecord_Smoke(dbs, game, rs.getInt("MapX"), rs.getInt("MapY"), event_time, false);
						} else if (rs.getInt("SmokeType") == EquipmentTypesTable.ET_NERVE_GAS) {
							UnitHistoryTable.AddRecord_NerveGas(dbs, game, rs.getInt("MapX"), rs.getInt("MapY"), event_time, false);
						} else if (rs.getInt("SmokeType") == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
							UnitHistoryTable.AddRecord_Fire(dbs, game, rs.getInt("MapX"), rs.getInt("MapY"), event_time, false);
						} else {
							DSRWebServer.SendEmailToAdmin("Invalid gas type", "Type:" + rs.getInt("SmokeType"));
						}
						break;
					case 4:
					case 5:
						// Drifts
						int rndx = rs.getInt("MapX") + Functions.rnd(-1, 1) ;
						if (rndx < 0) {
							rndx = 0;
						}
						int rndy = rs.getInt("MapY") + Functions.rnd(-1, 1) ;
						if (rndy < 0) {
							rndy = 0;
						}
						String sql = "UPDATE MapDataSquares SET SmokeType = " + rs.getInt("SmokeType") + ", SmokeCausedBy = " + rs.getInt("SmokeCausedBy");
						sql = sql + " WHERE MapDataID = " + game.getMapDataID() + " AND MapX = " + rndx + " AND MapY = " + rndy;
						sql = sql + " AND COALESCE(SmokeType, 0) = 0 AND SquareType = " + MapDataTable.MT_FLOOR + " AND COALESCE(DoorType, 0) <= 0";
						dbs.runSQLUpdate(sql);
						// Add history
						if (rs.getInt("SmokeType") == EquipmentTypesTable.ET_SMOKE_GRENADE) {
							UnitHistoryTable.AddRecord_Smoke(dbs, game, rndx, rndy, event_time, true);
						} else if (rs.getInt("SmokeType") == EquipmentTypesTable.ET_NERVE_GAS) {
							UnitHistoryTable.AddRecord_NerveGas(dbs, game, rndx, rndy, event_time, true);
						} else if (rs.getInt("SmokeType") == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
							UnitHistoryTable.AddRecord_Fire(dbs, game, rndx, rndy, event_time, true);
						} else {
							DSRWebServer.SendEmailToAdmin("Invalid gas type", "Type:" + rs.getInt("SmokeType"));
						}
						break;
					default:
						// Do nothing
					}
				} else {
					// Reduce timer
					dbs.runSQLUpdate("UPDATE MapDataSquares SET SmokeTurnCount = SmokeTurnCount - 1 WHERE MapDataSquareID = " + rs.getInt("MapDataSquareID"));
				}
				// If fire, create smoke
				if (rs.getInt("SmokeType") == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
					for (int y=rs.getInt("MapY")-1 ; y<=rs.getInt("MapY")+1 ; y++) {
						for (int x=rs.getInt("MapX")-1 ; x<=rs.getInt("MapX")+1 ; x++) {
							if (Functions.rnd(1, game.getNumOfSides()*2) == 1) {
								String sql = "UPDATE MapDataSquares SET SmokeType = " + EquipmentTypesTable.ET_SMOKE_GRENADE + ", SmokeCausedBy = " + rs.getInt("SmokeCausedBy");
								sql = sql + " WHERE MapDataID = " + game.getMapDataID() + " AND MapX = " + x + " AND MapY = " + y;
								sql = sql + " AND COALESCE(SmokeType, 0) = 0 AND SquareType = " + MapDataTable.MT_FLOOR + " AND COALESCE(DoorType, 0) <= 0";
								dbs.runSQLUpdate(sql);
								// Add history
								UnitHistoryTable.AddRecord_Fire(dbs, game, x, y, event_time, true);
							}
						}
					}
				}
			}
			// Remove all fire from water
			try {
				dbs.runSQLUpdate("UPDATE MapDataSquares SET SmokeType = 0, SmokeCausedBy = 0, SmokeTurnCount = 0 WHERE MapDataID = " + game.getMapDataID() + " AND SmokeType = " + EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE + " AND FloorTex = " + TextureStateCache.TEX_WATER);
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	public static void CloseAllDoors(MySQLConnection dbs, int mapid) throws SQLException {
		dbs.runSQLUpdate("UPDATE MapDataSquares SET DoorOpen = 0 WHERE MapDataID = " + mapid);
	}


	public static void SetDoorOpen(MySQLConnection dbs, int mapid, int x, int y, boolean open) throws SQLException {
		dbs.runSQLUpdate("UPDATE MapDataSquares SET DoorOpen = " + SQLFuncs.b201(open) + " WHERE MapDataID = " + mapid + " AND MapX = " + x + " AND MapY = " + y);
	}


	public static void SetFullyCreated(MySQLConnection dbs, int mapid) throws SQLException {
		dbs.runSQLUpdate("UPDATE MapData SET FullyCreated = 1 WHERE MapDataID = " + mapid);
	}


	public static boolean IsFullyCreated(MySQLConnection dbs, int mapid) throws SQLException {
		return dbs.getScalarAsInt("SELECT Count(*) FROM MapData WHERE FullyCreated = 1 AND MapDataID = " + mapid) > 0;
	}



}
