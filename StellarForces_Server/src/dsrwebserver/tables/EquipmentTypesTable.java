package dsrwebserver.tables;

import java.sql.SQLException;

import dsrwebserver.DSRWebServer;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.util.MyList;

/**
 * This stores all the data about equipment.
 *
 */
public final class EquipmentTypesTable extends AbstractTable {
	
	public static MyList<Integer> CORPSES;

	public static final byte ET_GUN = 1;
	public static final byte ET_GRENADE = 2;
	public static final byte ET_MEDIKIT = 3;
	public static final byte ET_CC_WEAPON = 4;
	public static final byte ET_AMMO_CLIP = 5;
	//public static final byte unused = 6;
	public static final byte ET_PORTA_PORTER_TRIGGER = 7;
	public static final byte ET_PORTA_PORTER_LANDER = 8;
	public static final byte ET_DEATH_GRENADE = 9;
	public static final byte ET_ADRENALIN_SHOT = 10;
	public static final byte ET_EXPLOSIVES = 11; // Can destroy walls even in non-destroyable maps!
	public static final byte ET_FLAG = 12;
	public static final byte ET_EGG = 13;
	public static final byte ET_HUMAN_CORPSE_1 = 14;
	public static final byte ET_HUMAN_CORPSE_2 = 15;
	public static final byte ET_HUMAN_CORPSE_3 = 16;
	public static final byte ET_HUMAN_CORPSE_4 = 17;
	public static final byte ET_SCIENTIST_CORPSE = 18;
	public static final byte ET_ALIEN_CORPSE = 19;
	public static final byte ET_ALIEN_QUEEN_CORPSE = 20;
	public static final byte ET_SMOKE_GRENADE = 21;
	public static final byte ET_NERVE_GAS = 22;
	public static final byte ET_GAS_CANNISTER = 23;
	public static final byte ET_STARDRIVE = 24;
	public static final byte ET_SCANNER = 25;
	public static final byte ET_CRAB_CORPSE = 26;
	public static final byte ET_GHOUL_CORPSE = 27;
	public static final byte ET_GHOUL_QUEEN_CORPSE = 28;
	public static final byte ET_ZOMBIE_CORPSE = 29;
	public static final byte ET_CLONE_CORPSE_1 = 30;
	public static final byte ET_CLONE_CORPSE_2 = 31;
	public static final byte ET_CLONE_CORPSE_3 = 32;
	public static final byte ET_CLONE_CORPSE_4 = 33;
	public static final byte ET_INCENDIARY_FIRE_GRENADE = 34; // For smoke_type
	public static final byte ET_MAX_TYPES = 34;
	// # If you add any, add them to the Android client as well!! ##

	
	// Codes
	public static final String CD_EXPLOSIVE = "EXPLOSIVE";
	public static final String CD_ROCKET_LAUNCHER = "ROCKET_LAUNCHER";
	public static final String CD_ROCKET = "ROCKET";
	public static final String CD_AUTOCANNON = "AUTOCANNON";
	public static final String CD_FLAG = "FLAG";
	public static final String CD_EGG = "EGG";
	public static final String CD_SCIENTIST_CORPSE = "SCIENTISTCORPSE";
	public static final String CD_HUMAN_CORPSE_SIDE = "HUMANCORPSE";
	public static final String CD_ALIEN_CORPSE = "ALIENCORPSE";
	public static final String CD_SP30 = "SP30";
	public static final String CD_MK1 = "MK1";
	public static final String CD_AP50 = "AP50";
	public static final String CD_AP100 = "AP100";
	public static final String CD_MACHINE_CLIP = "MACHINE_CLIP";
	public static final String CD_PISTOL_CLIP = "PISTOL_CLIP";
	public static final String CD_GASCANNISTER = "GASCANNISTER"; 
	public static final String CD_POWER_SWORD = "POWER_SWORD"; 
	public static final String CD_STARDRIVE = "STARDRIVE";
	public static final String CD_NERVE_GAS = "NERVEGAS";
	public static final String CD_SMOKE_GRENADE = "SMOKEGRENADE";
	public static final String CD_SCANNER = "SCANNER";
	public static final String CD_DEATHGRENADE = "DEATHGRENADE";
	public static final String CD_BLASTER = "BLASTER";
	public static final String CD_FLAMETHROWER = "FLAMETHROWER";
	public static final String CD_FIRE_EXTINGUISHER = "FIREEXT";
	public static final String CD_INCENDIARY_GRENADE = "INCENDIARYGREN";
	public static final String CD_SNIPERRIFLE = "SNIPERRIFLE";
	// ## ! IF YOU ADD ANY HERE, ADD THEM TO THE CLIENT AS WELL! 
	
	static {
		CORPSES = MyList.CreateIntsFromInts(ET_HUMAN_CORPSE_1, ET_HUMAN_CORPSE_2, ET_HUMAN_CORPSE_3, ET_HUMAN_CORPSE_4, ET_SCIENTIST_CORPSE, ET_ALIEN_CORPSE, ET_ALIEN_QUEEN_CORPSE);
	}
	
	public EquipmentTypesTable(MySQLConnection dbs) {
		super(dbs, "EquipmentTypes", "EquipmentTypeID");
	}
	
	
	public static String GetMajorTypeFromID(byte t) {
		switch (t) {
		case ET_GUN:
			return "Guns";
		case ET_GRENADE:
			return "Grenades";
		case ET_MEDIKIT:
			return "Medi-Kits";
		case ET_CC_WEAPON:
			return "Close Combat Weapons";
		case ET_AMMO_CLIP:
			return "Ammo Clip";
		/*case ET_PORTA_PORTER_TRIGGER:
			return "PP Trigger";
		/*case ET_PORTA_PORTER_LANDER:
			return "PP Lander";*/
		case ET_DEATH_GRENADE:
			return "Death Grenade";
		/*case ET_ADRENALIN_SHOT:
			return "Adrenalin Shot";*/
		case ET_EXPLOSIVES:
			return "Explosives";
		case ET_SMOKE_GRENADE:
			return "Smoke Grenade";
		case ET_NERVE_GAS:
			return "Nerve Gas";
		case ET_SCANNER:
			return "Tracking Device";
		case ET_INCENDIARY_FIRE_GRENADE:
			return "Incendiary Grenade";
		default:
			throw new RuntimeException("Unknown equipment type: " + t);
		}
	}

	
	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("EquipmentTypes") == false) {
			dbs.runSQL("CREATE TABLE EquipmentTypes (EquipmentTypeID INTEGER AUTO_INCREMENT KEY, MajorTypeID INTEGER, Name VARCHAR(64), Code VARCHAR(16), Description VARCHAR(2048), MinClientVersion FLOAT, Weight SMALLINT, AimedShotAccuracy SMALLINT, AimedShotAPCost SMALLINT, SnapshotAccuracy SMALLINT, SnapshotAPCost SMALLINT, AutoshotAccuracy SMALLINT, AutoshotAPCost SMALLINT, ShotDamage SMALLINT, CCDamage SMALLINT, AmmoCapacity SMALLINT, ReloadAPCost SMALLINT, AmmoTypeID SMALLINT, Explodes TINYINT, ExplosionRad SMALLINT, ExplosionDamage SMALLINT, Cost SMALLINT, CanBuy TINYINT, CCAccuracy SMALLINT, Indestructable TINYINT, NoiseRange TINYINT, ShowStats TINYINT, RestrictedToWinners TINYINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		if (dbs.doesColumnExist("EquipmentTypes", "ShotRange") == false) {
			dbs.runSQL("ALTER TABLE EquipmentTypes ADD ShotRange SMALLINT");
		}
	}

	
	public static int GetEquipmentTypeFromCode(MySQLConnection dbs, String code) throws SQLException {
		return dbs.getScalarAsInt("SELECT EquipmentTypeID FROM EquipmentTypes WHERE Code = " + SQLFuncs.s2sql(code));
	}

	
	public int getEquipmentTypeFromCode(String code) throws SQLException {
		return dbs.getScalarAsInt("SELECT EquipmentTypeID FROM EquipmentTypes WHERE Code = " + SQLFuncs.s2sql(code));
	}

	
	public static int GetAmmoCapacityFromEquipmentType(MySQLConnection dbs, int id) throws SQLException {
		try {
			return dbs.getScalarAsInt("SELECT AmmoCapacity FROM EquipmentTypes WHERE EquipmentTypeID = " + id);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
			return 7;
		}
	}

	
	public static String GetNameFromEquipmentType(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsString("SELECT Name FROM EquipmentTypes WHERE EquipmentTypeID = " + id);
	}

	
	public static int IsIndestructable(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsInt("SELECT Indestructable FROM EquipmentTypes WHERE EquipmentTypeID = " + id);
	}

	
	public static int CanBuy(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsInt("SELECT CanBuy FROM EquipmentTypes WHERE EquipmentTypeID = " + id);
	}

	
	public static void AddRec(MySQLConnection dbs, int majortype, String name, String code, int weight, int aimed_acc, int aimed_ap_cost, int snap_acc, int snap_ap_cost, int auto_acc, int auto_ap_cost, int shot_damage, int cc_damage, int ammo_cap, int reload_ap_cost, int ammo_type, int explodes, int expl_rad, int expl_dam, int cc_acc, int cost, int can_buy, float min_vers, String desc, int indestructable, int noise_range, int showstats, int restricted, int range) throws SQLException {
		if (dbs.getScalarAsInt("SELECT Count(*) FROM EquipmentTypes WHERE Code = " + SQLFuncs.s2sql(code)) == 0) {
			dbs.RunIdentityInsert("INSERT INTO EquipmentTypes (MajorTypeID, Name, Code, Weight, AimedShotAccuracy, AimedShotAPCost, SnapShotAccuracy, SnapShotAPCost, AutoShotAccuracy, AutoShotAPCost, ShotDamage, CCDamage, AmmoCapacity, ReloadAPCost, AmmoTypeID, Explodes, ExplosionRad, ExplosionDamage, Cost, CanBuy, CCAccuracy, MinClientVersion, Description, Indestructable, NoiseRange, ShowStats, RestrictedToWinners, ShotRange) VALUES (" + 
					majortype + ", " + SQLFuncs.s2sql(name) + ", " + SQLFuncs.s2sql(code) + ", " + weight + ", " + aimed_acc + ", " + aimed_ap_cost + ", " + snap_acc + ", " + snap_ap_cost + ", " + auto_acc + ", " + auto_ap_cost + ", " + shot_damage + ", " + cc_damage + ", " + ammo_cap + ", " + reload_ap_cost + ", " + ammo_type + ", " + explodes + ", " + expl_rad + ", " + expl_dam + ", " + cost + ", " + can_buy + ", " + cc_acc + ", " + min_vers + ", " + SQLFuncs.s2sql(desc) + ", " + indestructable + ", " + noise_range + ", " + showstats + ", " + restricted + ", " + range + ")");
		} else {
			dbs.runSQLUpdate("UPDATE EquipmentTypes SET MajorTypeID = " + majortype + ", Name = " + SQLFuncs.s2sql(name) + ", Weight = " + weight + ", AimedShotAccuracy = " + aimed_acc + ", AimedShotAPCost = " + aimed_ap_cost + ", SnapShotAccuracy = " + snap_acc + ", SnapShotAPCost = " + snap_ap_cost + ", AutoShotAccuracy = " + auto_acc + ", AutoShotAPCost = " + auto_ap_cost + 
					", ShotDamage = " + shot_damage + ", CCDamage = " + cc_damage + ", AmmoCapacity = " + ammo_cap + ", ReloadAPCost = " + reload_ap_cost + ", AmmoTypeID = " + ammo_type + ", Explodes = " + explodes + ", ExplosionRad = " + expl_rad + ", ExplosionDamage = " + expl_dam + ", Cost = " + cost  +", CanBuy = " + can_buy + ", CCAccuracy = " + cc_acc + ", MinClientVersion = " + min_vers + ", Description = " + SQLFuncs.s2sql(desc)+ ", Indestructable = " + indestructable + ", NoiseRange = " + noise_range + ", ShowStats = " + showstats + ", RestrictedToWinners = " + restricted + ", ShotRange = " + range + " WHERE Code = " + SQLFuncs.s2sql(code));
		}
	}


	public void selectAmmoByAmmoType(int type) throws SQLException {
		this.has_rows = false;
		rs = dbs.getResultSet("SELECT * FROM EquipmentTypes WHERE MajorTypeID = " + ET_AMMO_CLIP + " AND AmmoTypeID = " + type);
		if (rs.next()) {
			this.has_rows = true;
			return;
		}
		throw new RuntimeException("Unknown Ammo Type: " + type);
	}
	

	public void selectByCode(String code) throws SQLException {
		super.selectRow(this.getEquipmentTypeFromCode(code));
	}
	

	public int getAmmoCapacity() throws SQLException {
		return rs.getInt("AmmoCapacity");
	}
	

	public int getNoiseRange() throws SQLException {
		return rs.getInt("NoiseRange");
	}

	
	public int getAmmoTypeID() throws SQLException {
		return rs.getInt("AmmoTypeID");
	}

	
	public int getMajorType() throws SQLException {
		return rs.getInt("MajorTypeID");
	}

	
	public String getCode() throws SQLException {
		return rs.getString("Code");
	}

	
	public String getName() throws SQLException {
		return rs.getString("Name");
	}

	
	public int getCost() throws SQLException {
		return rs.getInt("Cost");
	}

	
	/*public boolean isRestricted() throws SQLException {
		return rs.getInt("RestrictedToWinners") == 1;
	}
*/
	
	public String getDesc() throws SQLException {
		return rs.getString("Description");
	}

	
	public int getAimedShotAcc() throws SQLException {
		return rs.getInt("AimedShotAccuracy");
	}

	
	public int getAimedShotAps() throws SQLException {
		return rs.getInt("AimedShotAPCost");
	}

	
	public int getSnapShotAcc() throws SQLException {
		return rs.getInt("SnapShotAccuracy");
	}
	

	public int getSnapShotAps() throws SQLException {
		return rs.getInt("SnapShotAPCost");
	}
	

	public int getAutoShotAcc() throws SQLException {
		return rs.getInt("AutoShotAccuracy");
	}
	

	public int getAutoShotAps() throws SQLException {
		return rs.getInt("AutoShotAPCost");
	}
	

	public int getShotDamage() throws SQLException {
		return rs.getInt("ShotDamage");
	}

	
	public int getCCAccuracy() throws SQLException {
		return rs.getInt("CCAccuracy");
	}

	
	public int getCCDamage() throws SQLException {
		return rs.getInt("CCDamage");
	}

	
	public int getExplosionRadius() throws SQLException {
		return rs.getInt("ExplosionRad");
	}

	
	public int getExplosionDamage() throws SQLException {
		return rs.getInt("ExplosionDamage");
	}

	
	public int getWeight() throws SQLException {
		return rs.getInt("Weight");
	}

	
	public int getReloadAPs() throws SQLException {
		return rs.getInt("ReloadAPCost");
	}
	

}
