package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;

public class LeagueTempTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException { // todo - drop this
		if (dbs.doesTableExist("LeagueTemp") == false) {
			dbs.runSQL("CREATE TABLE LeagueTemp (LeagueTempID INTEGER AUTO_INCREMENT KEY, LoginID INTEGER, Points INTEGER, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
		}

	}
	
	/*
	public static int AddRec(MySQLConnection dbs, int loginid, int points) throws SQLException {
		return dbs.RunIdentityInsert_Syncd("INSERT INTO LeagueTemp (LoginID, Points) VALUES (" + loginid + ", " + points + ")");
	}*/
}
