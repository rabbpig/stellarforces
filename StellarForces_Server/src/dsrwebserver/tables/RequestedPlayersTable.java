package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

public class RequestedPlayersTable {
	
	// Statuses
	private static final int NEW = 0;
	//private static final int ACCEPTED = 1;
	private static final int DECLINED_OR_EMAIL_INVALID = -1;

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("RequestedPlayers") == false) {
			dbs.runSQL("CREATE TABLE RequestedPlayers (RequestedPlayerID INTEGER AUTO_INCREMENT KEY, EmailAddress VARCHAR(256), LoginID INTEGER, Status TINYINT, ReferredByLoginID INTEGER, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}
	

	public static void AddRec(MySQLConnection dbs, String email, int loginid, int referred_by) throws SQLException {
		if (dbs.getScalarAsInt("SELECT Count(*) FROM RequestedPlayers WHERE EmailAddress = " + SQLFuncs.s2sql(email)) == 0) {
			dbs.RunIdentityInsert("INSERT INTO RequestedPlayers (EmailAddress, LoginID, Status, ReferredByLoginID) VALUES (" + SQLFuncs.s2sql(email) + ", " + loginid + ", " + NEW + ", " + referred_by + ")");
			//DSRWebServer.SendEmailToAdmin("New Unregistered Player", "A new player has been registered by another player!");
		}
	}


	public static boolean HasPlayerDeclined(MySQLConnection dbs, String email) throws SQLException {
		return dbs.getScalarAsInt("SELECT Count(*) FROM RequestedPlayers WHERE EmailAddress = " + SQLFuncs.s2sql(email) + " AND Status = " + DECLINED_OR_EMAIL_INVALID) > 0;
	}


	public static boolean IsNewPlayer(MySQLConnection dbs, int loginid) throws SQLException {
		return dbs.getScalarAsInt("SELECT Count(*) FROM RequestedPlayers WHERE LoginID = " + loginid + " AND COALESCE(Status, 0) = 0") > 0;
	}

}
