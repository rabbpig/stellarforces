package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;

public class VisibleEnemiesTable {

	// Visible types
	public static final int VT_SEEN = 0;
	public static final int VT_HEARD_MOVING = 1;
	public static final int VT_HEARD_SHOOTING = 2;

	private VisibleEnemiesTable() {

	}


	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("VisibleEnemies") == false) {
			dbs.runSQL("CREATE TABLE VisibleEnemies (VisibleEnemyID INTEGER AUTO_INCREMENT KEY, GameID INTEGER, TurnNo INTEGER, PhaseNo INTEGER, UnitID INTEGER, Side TINYINT, MapX SMALLINT, MapY SMALLINT, SeenBySide TINYINT, Angle SMALLINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		if (dbs.doesColumnExist("VisibleEnemies", "VisibleType") == false) {
			dbs.runSQL("ALTER TABLE VisibleEnemies ADD VisibleType TINYINT");
		}

		VisibleEnemiesTable.DeleteOldRecs(dbs);

		if (dbs.doesIndexExist("VisibleEnemies", "idx_VisibleEnemies_GameID") == false) {
			dbs.runSQL("CREATE INDEX idx_VisibleEnemies_GameID ON VisibleEnemies (GameID)");
		}
	}


	public static void DeleteOldRecs(MySQLConnection dbs) throws SQLException {
		// Delete obsolete recs
		dbs.runSQLDelete("DELETE FROM VisibleEnemies WHERE GameID IN (SELECT GameID FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED + ")");
	}


	public static void AddRec(MySQLConnection dbs, GamesTable game, UnitsTable unit_seen, int loginid_log, int seen_by_side, int vis_type, long event_time) throws SQLException {
		try {
			// Check it doesn't already exist
			if (dbs.getScalarAsInt("SELECT Count(*) FROM VisibleEnemies WHERE GameID = " + game.getID() + " AND TurnNo = " + game.getTurnNo() + " AND PhaseNo = " + game.getPhaseNo() + " AND UnitID = " + unit_seen.getID() + " AND MapX = " + unit_seen.getMapX() + " AND MapY = " + unit_seen.getMapY() + " AND Angle = " + unit_seen.getAngle() + " AND SeenBySide = " + seen_by_side + " AND VisibleType = " + vis_type) == 0) {
				if (vis_type == VT_HEARD_SHOOTING) {
					GameLogTable.AddRec(dbs, game, loginid_log, -1, "An enemy unit has been heard shooting! (See Scanner)", false, false, event_time);
				} else {
					try {
						// Only add a movement record to the log if it's the first time they've been spotted.
						if (dbs.getScalarAsInt("SELECT Count(*) FROM VisibleEnemies WHERE GameID = " + game.getID() + " AND TurnNo = " + game.getTurnNo() + " AND PhaseNo = " + game.getPhaseNo() + " AND UnitID = " + unit_seen.getID() + "  AND SeenBySide = " + seen_by_side) == 0) {
							// Add it to the log
							AbstractMission mission = AbstractMission.Factory(game.getMissionID());
							if (vis_type == VT_SEEN) {
								GameLogTable.AddRec(dbs, game, loginid_log, -1, unit_seen.getName() + " (" + LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(unit_seen.getSide()), false) + "'s #" + unit_seen.getOrderBy() + ") has been spotted!", false, mission.isSnafu(), System.currentTimeMillis());
							} else if (vis_type == VT_HEARD_MOVING) {
								GameLogTable.AddRec(dbs, game, loginid_log, -1, "An enemy unit has been heard moving! (See Scanner)", false, false, System.currentTimeMillis());
							}
						}
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex);
					}
				}
				// This line must be *after* we've checked the VisibleEnemies table!
				dbs.RunIdentityInsert("INSERT INTO VisibleEnemies (GameID, TurnNo, PhaseNo, UnitID, Side, MapX, MapY, Angle, SeenBySide, VisibleType) VALUES (" + game.getID() + ", " + game.getTurnNo() + ", " + game.getPhaseNo() + ", " + unit_seen.getID() + ", " + unit_seen.getSide() + ", " + unit_seen.getMapX() + ", " + unit_seen.getMapY() + ", " + unit_seen.getAngle() + ", " + seen_by_side + ", " + vis_type + ")");
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}
	}

}
