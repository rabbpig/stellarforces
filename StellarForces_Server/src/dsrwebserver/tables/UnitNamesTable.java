package dsrwebserver.tables;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

public class UnitNamesTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("UnitNames") == false) {
			dbs.runSQL("CREATE TABLE UnitNames (UnitNameID INTEGER AUTO_INCREMENT KEY, LoginID INTEGER, Seq INTEGER, Name VARCHAR(128), DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}
	

	public static void AddRecs(MySQLConnection dbs, int loginid, String data) throws SQLException {
		dbs.runSQLDelete("DELETE FROM UnitNames WHERE LoginID = " + loginid);
		String d[] = data.trim().split("\n");
		for (int i=0 ; i<d.length ; i++) {
			String name = d[i].replaceAll("\r", "").trim();
			if (name.length() > 0) {
				dbs.RunIdentityInsert("INSERT INTO UnitNames (LoginID, Seq, Name) VALUES (" + loginid + ", " + (i+1) + ", " + SQLFuncs.s2sql(name) + ")");
			}
		}
		
	}
	
	
	public static String GetName(MySQLConnection dbs, int loginid, int seq) throws SQLException {
		ResultSet rs =  dbs.getResultSet("SELECT Name FROM UnitNames WHERE LoginID = " + loginid + " AND Seq = " + seq);
		if (rs.next()) {
			return rs.getString("Name").trim();
		} else {
			return "";
		}
	}


	public static String GetAllNames(MySQLConnection dbs, int loginid) throws SQLException {
		ResultSet rs =  dbs.getResultSet("SELECT Name FROM UnitNames WHERE LoginID = " + loginid + " ORDER BY Seq");
		StringBuffer str = new StringBuffer("");
		while (rs.next()) {
			str.append(rs.getString("Name") + "\n");
		}
		return str.toString();
	}


}
