package dsrwebserver.tables;

import java.sql.SQLException;

import dsrwebserver.DSRWebServer;

import ssmith.dbs.MySQLConnection;

public class PlayerTrophiesTable {
	
	public static final int LONG_SERVICE_TROPHY = 1;
	public static final int LASER_SQUAD_TROPHY = 2;
	public static final int FIRST_BLOOD_TROPHY = 3;
	public static final int WINNERS_TROPHY = 4;
	public static final int LOSERS_TROPHY = 5;
	public static final int MAX_TROPHIES = 5;
	
	private static final String BASE_URL = "/images/medals/";

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("PlayerTrophies") == false) {
			dbs.runSQL("CREATE TABLE PlayerTrophies (PlayerTrophyID INTEGER AUTO_INCREMENT KEY, LoginID INTEGER, Trophy TINYINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}
	
	
	public static String GetTrophyName(int id) {
		switch (id) {
		case LONG_SERVICE_TROPHY: return "Long Service Trophy"; 
		case LASER_SQUAD_TROPHY: return "Honourary Laser Squad Trophy";
		case FIRST_BLOOD_TROPHY: return "First Blood Trophy"; 
		case WINNERS_TROPHY: return "Winners Trophy"; 
		case LOSERS_TROPHY: return "Punctured Heart Medal"; 
		default: return "Unknown"; 
		}
	}
	
	
	/*public static int GetFactionPointsForTrophy(int id) {
		switch (id) {
		case LONG_SERVICE_TROPHY: return 10; 
		case LASER_SQUAD_TROPHY: return 12;
		case FIRST_BLOOD_TROPHY: return 6; 
		case WINNERS_TROPHY: return 9; 
		case LOSERS_TROPHY: return 12; 
		default: return 0; 
		}
	}*/
	
	
	public static String GetTrophyImageURL(int id) {
		switch (id) {
		case LONG_SERVICE_TROPHY: return BASE_URL + "long_service.png"; 
		case LASER_SQUAD_TROPHY: return BASE_URL + "laser_squad.png"; 
		case FIRST_BLOOD_TROPHY: return BASE_URL + "first_blood.png";
		case WINNERS_TROPHY: return BASE_URL + "winners_medal.png"; 
		case LOSERS_TROPHY: return BASE_URL + "losers_medal.png"; 
		default: return ""; 
		}
	}
	
	
	public static boolean DoesPlayerHaveTrophy(MySQLConnection dbs, int loginid, int trophy) throws SQLException {
		return dbs.getScalarAsInt("SELECT Count(*) FROM PlayerTrophies WHERE LoginID = " + loginid + " AND Trophy = " + trophy + " LIMIT 1") > 0;
	}
	

	public static boolean DoesPlayerHaveAnyTrophy(MySQLConnection dbs, int loginid) throws SQLException {
		return dbs.getScalarAsInt("SELECT Count(*) FROM PlayerTrophies WHERE LoginID = " + loginid + " LIMIT 1") > 0;
	}
	

	public static void AddTrophy(MySQLConnection dbs, int loginid, int trophy) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO PlayerTrophies (LoginID, Trophy) VALUES(" + loginid + ", " + trophy + ")");

		try {
			WebsiteEventsTable.AddRec(dbs, LoginsTable.GetDisplayName_Enc(dbs, loginid, false) + " has won a trophy.", -1);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}
	}
	
}

