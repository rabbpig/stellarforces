package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

/**
 * Note that this is different to the Campaign log!
 *
 */
public class StarmapLogTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("StarmapLog") == false) {
			dbs.runSQL("CREATE TABLE StarmapLog (StarmapLogID INTEGER AUTO_INCREMENT KEY, Entry VARCHAR(256), DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}
	
	/**
	 * LoginID is the player who should see the log (or 0 for all).
	 */
	public static void AddRec(MySQLConnection dbs, String entry, boolean bold) throws SQLException {
		if (bold) {
			entry = "<b>" + entry + "</b>";
		}
		dbs.RunIdentityInsert("INSERT INTO StarmapLog (Entry) VALUES (" + SQLFuncs.s2sql(entry) + ")");
	}

}
