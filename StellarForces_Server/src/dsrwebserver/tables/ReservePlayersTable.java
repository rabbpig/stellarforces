package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;

public class ReservePlayersTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("ReservePlayers") == false) {
			dbs.runSQL("CREATE TABLE ReservePlayers (ReservePlayerID INTEGER AUTO_INCREMENT KEY, LoginID INTEGER, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}
	
	
	public static void AddRec(MySQLConnection dbs, int loginid) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO ReservePlayers (LoginID) VALUES (" + loginid + ")");
	}
	
	
	public static void DelRec(MySQLConnection dbs, int loginid) throws SQLException {
		dbs.RunIdentityInsert("DELETE FROM ReservePlayers WHERE LoginID = " + loginid);
	}
	
	
	public static boolean IsPlayerOnList(MySQLConnection dbs, int loginid) throws SQLException {
		return dbs.getScalarAsInt("SELECT Count(*) FROM ReservePlayers WHERE LoginID = " + loginid) > 0;
	}
	
	
}
