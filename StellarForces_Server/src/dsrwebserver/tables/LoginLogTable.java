package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

public class LoginLogTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("LoginLog") == false) {
			dbs.runSQL("CREATE TABLE LoginLog (LoginLogID INTEGER AUTO_INCREMENT KEY, Login VARCHAR(128), Pwd VARCHAR(50), Successful TINYINT, IPAddress VARCHAR(32), DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
		}

	}
	
	
	public static int AddRec(MySQLConnection dbs, String login, String pwd, String ip_address) throws SQLException {
		return dbs.RunIdentityInsert_Syncd("INSERT INTO LoginLog (Login, Pwd, IPAddress) VALUES (" + SQLFuncs.s2sql(login) + ", " + SQLFuncs.s2sql(pwd) + ", " + SQLFuncs.s2sql(ip_address) + ")");
	}

	
	public static void SetSuccessful(MySQLConnection dbs, int id, boolean success) throws SQLException {
		dbs.runSQLUpdate("UPDATE LoginLog SET Successful = " + SQLFuncs.b201(success) + " WHERE LoginLogID = " + id);
	}
}
