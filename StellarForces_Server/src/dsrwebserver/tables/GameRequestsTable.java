package dsrwebserver.tables;

import java.io.IOException;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;

/**
 * This table stores details of all the games waiting for the second player.
 * 
 * SideChoices: -1 = Don't mind
 *
 */
public final class GameRequestsTable extends AbstractTable {

	// Game types
	public static final int GT_NORMAL = 0;
	public static final int GT_PRACTISE = 1;

	// Opponent types
	public static final int OT_ANYONE = 0;
	public static final int OT_NEWBIES = 1;
	public static final int OT_INTERMEDIATE = 2;
	public static final int OT_NON_INTERMEDIATE = 3;

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("GameRequests") == false) {
			dbs.runSQL("CREATE TABLE GameRequests (GameRequestID INTEGER AUTO_INCREMENT KEY, Player1ID INTEGER, Player2ID INTEGER, Player3ID INTEGER, Player4ID INTEGER, Mission SMALLINT, Player1SideChoice TINYINT, Player2SideChoice TINYINT, Player3SideChoice TINYINT, Player4SideChoice TINYINT, Accepted TINYINT, GameID INTEGER, GameType TINYINT, Sides TINYINT, OpponentType TINYINT, CanHearEnemies TINYINT, CampGame TINYINT, PcentCreds INTEGER, PcentPointsRange INTEGER, Comment VARCHAR(1024), StarmapX TINYINT, StarmapY TINYINT, AttackingFactionID INTEGER, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		if (dbs.doesColumnExist("GameRequests", "UploadedMapID") == false) {
			dbs.runSQL("ALTER TABLE GameRequests ADD UploadedMapID INTEGER");
		}

		dbs.runSQLDelete("DELETE FROM GameRequests WHERE Accepted = 1");
	}


	public GameRequestsTable(MySQLConnection dbs) {
		super(dbs, "GameRequests", "GameRequestID");
	}


	public static int CreateRequest(MySQLConnection dbs, int sides, int player1id, int mission, int p1side, int type, int opp_type, boolean advanced, boolean camp_game, int pcent_creds, String comment, int pcent_pts_range, int attacking_faction, int x, int y, int mapid) throws SQLException {
		int id = dbs.RunIdentityInsert_Syncd("INSERT INTO GameRequests (Sides, Player1ID, Mission, Player1SideChoice, GameType, OpponentType, CanHearEnemies, CampGame, PcentCreds, Comment, PcentPointsRange, AttackingFactionID, StarmapX, StarmapY, UploadedMapID) VALUES (" + sides + ", " + player1id + ", " + mission + ", " + p1side + ", " + type + ", " + opp_type + ", " + SQLFuncs.b201(advanced) + ", " + SQLFuncs.b201(camp_game) + ", " + pcent_creds + ", " +SQLFuncs.s2sql(comment) + ", " + pcent_pts_range + ", " + attacking_faction + ", " + x + ", " + y + ", " + mapid + ")");

		WebsiteEventsTable.AddRec(dbs, LoginsTable.GetDisplayName_Enc(dbs, player1id, false) + " has created a new game.", player1id);

		return id;
	}


	/**
	 * This returns the game request for the square.
	 * @param dbs
	 * @param x
	 * @param y
	 * @return
	 * @throws SQLException
	 */
	public static int IsUnderAttack(MySQLConnection dbs, int x, int y) throws SQLException {
		return dbs.getScalarAsInt("SELECT GameRequestID FROM GameRequests WHERE StarmapX = " + x + " AND StarmapY = " + y + " UNION SELECT -1"); // AND COALESCE(Accepted, 0) = 0 
	}


	public static int GetDaysLeft(MySQLConnection dbs, int x, int y) throws SQLException {
		return dbs.getScalarAsInt("SELECT DATEDIFF(CurDate(), DateCreated) FROM GameRequests WHERE StarmapX = " + x + " AND StarmapY = " + y);
	}


	public static int GetAttackingFactionID(MySQLConnection dbs, int x, int y) throws SQLException {
		return dbs.getScalarAsInt("SELECT AttackingFactionID FROM GameRequests WHERE StarmapX = " + x + " AND StarmapY = " + y + " UNION SELECT -1");
	}


	public static int GetAttackingPlayerID(MySQLConnection dbs, int x, int y) throws SQLException {
		return dbs.getScalarAsInt("SELECT Player1ID FROM GameRequests WHERE StarmapX = " + x + " AND StarmapY = " + y + " UNION SELECT -1");
	}


	public int getAttackingFactionID() throws SQLException {
		return rs.getInt("AttackingFactionID");
	}


	public int getPcentCredit() throws SQLException {
		if (rs.getInt("PcentCreds") <= 0) {
			return 100;
		} else {
			return rs.getInt("PcentCreds");
		}
	}


	public int getOpponentPointsRange() throws SQLException {
		return rs.getInt("PcentPointsRange");
	}


	public static void RemoveRequest(MySQLConnection dbs, int loginid, int reqid) throws SQLException {
		dbs.runSQLDelete("DELETE FROM GameRequests WHERE GameRequestID = " + reqid + " AND Player1ID = " + loginid);
	}


	public static int GetGameID(MySQLConnection dbs, int reqid) throws SQLException {
		return dbs.getScalarAsInt("SELECT GameID FROM GameRequests WHERE GameRequestID = " + reqid);
	}


	public int getPlayerIDByNum(int num) throws SQLException {
		return rs.getInt("Player" + num + "ID");
	}


	public int getGameID() throws SQLException {
		return rs.getInt("GameID");
	}


	public int getPlayersSideChoice(int num) throws SQLException {
		return rs.getInt("Player" + num + "SideChoice");
	}


	public int getPlayerIDFromSideChoice(int side) throws SQLException {
		for (int i=1 ; i<=this.getNumSides() ; i++) {
			if (rs.getInt("Player" + i + "SideChoice") == side) {
				return rs.getInt("Player" + i + "ID");
			}
		}
		throw new RuntimeException("Unknown side: " + side);
	}


	public int getMission() throws SQLException {
		return rs.getInt("Mission");
	}


	public int getType() throws SQLException {
		return rs.getInt("GameType");
	}


	public boolean isAdvancedMode() throws SQLException {
		return rs.getInt("CanHearEnemies") == 1;
	}


	public boolean isCampaignGame() throws SQLException {
		return rs.getInt("CampGame") == 1;
	}


	public boolean isSpaceAvailable() throws SQLException {
		for (int i=1 ; i <= this.getNumSides() ; i++) {
			if (rs.getInt("Player" + i + "ID") == 0) {
				return true;
			}
		}
		return false;
	}


	public boolean isPlayerInGame(int loginid) throws SQLException {
		for (int i=1 ; i<= this.getNumSides() ; i++) {
			if (rs.getInt("Player" + i + "ID") == loginid) {
				return true;
			}
		}
		return false;
	}


	public int getOpponentType() throws SQLException {
		return rs.getInt("OpponentType");
	}


	public int getNumSides() throws SQLException {
		return rs.getInt("Sides");
	}


	public String getComment() throws SQLException {
		return rs.getString("Comment");
	}


	public void setGameID(int gameid) throws SQLException {
		dbs.runSQLUpdate("UPDATE GameRequests SET GameID = " + gameid + " WHERE GameRequestID = " + this.getID());
		this.refreshData();
	}


	public void setOpponent(int loginid, int num) throws SQLException {
		dbs.runSQLUpdate("UPDATE GameRequests SET Player" + num + "ID = " + loginid + " WHERE GameRequestID = " + this.getID());
		this.refreshData();
	}


	public void setFullyAccepted() throws SQLException {
		dbs.runSQLUpdate("UPDATE GameRequests SET Accepted = 1 WHERE GameRequestID = " + this.getID());
		this.refreshData();
	}


	public void setPlayerID(int num, int loginid) throws SQLException {
		dbs.runSQLUpdate("UPDATE GameRequests SET Player" + num + "ID = " + loginid + " WHERE GameRequestID = " + this.getID());
		this.refreshData();

		try {
			WebsiteEventsTable.AddRec(dbs, LoginsTable.GetDisplayName_Enc(dbs, loginid, false) + " has joined a game.", loginid);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}
	}


	public void setPlayerSideChoice(int num, int side) throws SQLException {
		dbs.runSQLUpdate("UPDATE GameRequests SET Player" + num + "SideChoice = " + side + " WHERE GameRequestID = " + this.getID());
		this.refreshData();

	}


	private int getNextEmptyPlayer(int loginid) throws SQLException {
		// First, see if they have already been allocated a space
		for (int i=1 ; i<=this.getNumSides() ; i++) {
			if (this.getPlayerIDByNum(i) == loginid) {
				return i;
			}
		}

		// No allocated slot so find empty slot
		for (int i=1 ; i<=this.getNumSides() ; i++) {
			if (this.getPlayerIDByNum(i) <= 0) {
				return i;
			}
		}
		throw new RuntimeException("No more player slots in request " + this.getID() + " (LoginID " + loginid + ", GameID:" + this.getGameID() +")");
	}


	public int getNumPlayersAcceptedAndChosenSideSoFar() throws SQLException {
		int c = 0;
		for (int i=1 ; i<=this.getNumSides() ; i++) {
			if (this.getPlayerIDByNum(i) > 0 && this.rs.getInt("Player" + i + "SideChoice") > 0) {
				c++;//return i-1;
			}
		}
		return c;
	}


	public boolean areAllPlayersSelected() throws SQLException {
		return this.getNumPlayersAcceptedAndChosenSideSoFar() >= getNumSides();
	}


	public boolean hasSideBeenSelected(int side) throws SQLException {
		for (int i=1 ; i<= this.getNumSides() ; i++) {
			if (rs.getInt("Player" + i + "SideChoice") == side) {
				return true;
			}
		}
		return false;
	}


	public boolean isWaitingForSpecificPlayerToJoin() throws SQLException {
		for (int i=1 ; i<= this.getNumSides() ; i++) {
			if (rs.getInt("Player" + i + "ID") > 0) {
				if (rs.getInt("Player" + i + "SideChoice") <= 0) {
					return true;
				}
			}
		}
		return false;
	}
	
	
	public boolean hasPlayerSelectedASide(int loginid) throws SQLException {
		for (int i=1 ; i<= this.getNumSides() ; i++) {
			if (rs.getInt("Player" + i + "ID") == loginid) {
				if (rs.getInt("Player" + i + "SideChoice") > 0) {
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}


	public int getSideFromPlayerID(int loginid) throws SQLException {
		for (int i=1 ; i<=this.getNumSides() ; i++) {
			if (this.rs.getInt("Player" + i + "ID") == loginid) {
				return rs.getInt("Player" + i + "SideChoice");
			}
		}
		throw new RuntimeException("Player " + loginid + " is not in game");
	}


	public String getOpponents(int loginid) throws SQLException {
		int count = 0;
		StringBuffer str = new StringBuffer();
		for (int i=1 ; i<= this.getNumSides() ; i++) {
			if (rs.getInt("Player" + i + "ID") != loginid) {
				if (rs.getInt("Player" + i + "SideChoice") == 0) {
					if (rs.getInt("Player" + i + "ID") == 0) {
						count++;
					} else {
						str.append(LoginsTable.GetDisplayName_Enc(dbs, rs.getInt("Player" + i + "ID"), false) + ", ");
					}
				}
			}
		}
		if (str.length() > 0) {
			str.delete(str.length()-2, str.length());
			if (count > 0) {
				return str.toString() + " and " + count + " player(s)";
			} else {
				return str.toString();
			}
		} else if (count > 1) {
			return count + " players";
		} else {
			return count + " player";
		}
	}


	public boolean isFactionGame() throws SQLException {
		return rs.getInt("AttackingFactionID") > 0;
	}


	public boolean isPractise() throws SQLException {
		return rs.getInt("GameType") == GT_PRACTISE;
	}


	public int getStarmapX() throws SQLException {
		return rs.getInt("StarmapX");
	}


	public int getStarmapY() throws SQLException {
		return rs.getInt("StarmapY");
	}


	public int getUploadedMapID() throws SQLException {
		return rs.getInt("UploadedMapID");
	}


	public void acceptRequest(LoginsTable current_login, GamesTable game, int player2side) throws SQLException, IOException {
		int next_player = -1;
		if (this.getNumSides() == 1) { // Practise mission
			next_player = 1;
			player2side = 1;
		} else {
			next_player = this.getNextEmptyPlayer(current_login.getID());
		}

		// Check the request hasn't already been accepted.
		if (next_player > 0) {
			// Check the side hasn't already been selected
			if (this.hasSideBeenSelected(player2side) == false || this.getNumSides() == 1) {
				this.setPlayerID(next_player, current_login.getID());
				this.setPlayerSideChoice(next_player, player2side);
				this.refreshData();

				AbstractMission mission = AbstractMission.Factory(this.getMission());

				if (this.isCampaignGame()) {
					if (this.areAllPlayersSelected()) {
						// Make player1 the one who chose side1!
						int side_id[] = new int[5];
						for (int i=1 ; i <= this.getNumSides() ; i++) {
							side_id[i] = this.getPlayerIDFromSideChoice(i);
						}
						int gameid = game.createGame(this.getNumSides(), side_id[1], side_id[2], side_id[3], side_id[4], this.getMission(), this.getType(), true, this.isCampaignGame(), 0, this.getStarmapX(), this.getStarmapY(), this.getUploadedMapID());
						mission.createUnits(dbs, game); // Since they won't have already been created
						int mapid = mission.createMapStart(dbs, gameid);
						game.setMapDataID(mapid);
					}
				} else {
					int gameid = this.getGameID();
					if (game.doesRowExist(gameid)) {
						game.selectRow(gameid);
						game.setPlayerID(player2side, current_login.getID());
						game.updateDatesToNow();
						game.refreshData();
						mission.renameUnits(dbs, game, player2side); // Rename the players units to the names they have chosen
					} else {
						this.setFullyAccepted();
						throw new RuntimeException("Sorry, game " + gameid + " specified in request " + this.getID() + " does not seem to exist.");
					}
				}

				if (this.areAllPlayersSelected()) {
					this.setFullyAccepted();

					// Create forum - only when all players accepted!
					ForumTopicsTable ftt = new ForumTopicsTable(dbs);
					int forumid = ftt.addTopic(DSRWebServer.GAMES_FORUM_ID, "Game " + game.getID() + ": " + game.getOpponentsNamesBySide(player2side, false) + ", " + current_login.getDisplayName() + " in '" + mission.getMissionName(false, game.isCampGame()) + "'", -1);
					game.setForumID(forumid);
					for (int s=1 ; s<=this.getNumSides() ; s++) {
						int loginid = game.getLoginIDFromSide(s);
						ForumSubscriptionsTable.AddSub(dbs, loginid, forumid);
					}

					game.refreshData();
					//game.updatePointsForWin();
				}

				// Let other players know the game has been accepted
				if (game.isRowSelected()) {
					LoginsTable opponent_login = new LoginsTable(dbs);
					for (int s=1 ; s<=this.getNumSides() ; s++) {
						int loginid = game.getLoginIDFromSide(s);
						if (loginid > 0) {
							if (loginid != current_login.getID() && loginid != DSRWebServer.AI_LOGIN_ID && current_login.getID() != DSRWebServer.AI_LOGIN_ID) {
								opponent_login.selectRow(loginid);
								String msg = "Your game '" + AbstractMission.GetMissionNameFromType(game.getMissionID(), false, game.isCampGame()) + "' has been joined by " + current_login.getDisplayName() + ".  Please go to the " + DSRWebServer.TITLE + " website for details.  You will receive another email when it is your turn.";
								if (game.isCampGame()) {
									msg = msg  + "\n\nPlease note that this is a campaign game, so you will need to visit the website to select your squad.";
								}
								opponent_login.sendEmail("Game Request Accepted!", msg);
							}
						}
					}
					opponent_login.close();
				}

				// Delete the request so other players can't accept it
				dbs.runSQLDelete("DELETE FROM GameRequests WHERE Accepted = 1");

			} else {
				throw new RuntimeException("Sorry, side " + player2side + " has already been chosen in game " + game.getID());
			}
		} else {
			throw new RuntimeException("Sorry, that request has already been accepted.");
		}

	}

}
