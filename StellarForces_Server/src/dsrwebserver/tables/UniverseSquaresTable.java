package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import dsrwebserver.missions.AbstractMission;

public class UniverseSquaresTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("UniverseSquares") == false) {
			dbs.runSQL("CREATE TABLE UniverseSquares (UniverseSquareID INTEGER AUTO_INCREMENT KEY, MapX SMALLINT, MapY SMALLINT, Name VARCHAR(32), OwnerID INTEGER, GameID INTEGER, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		if (dbs.doesColumnExist("UniverseSquares", "Name") == false) {
			dbs.runSQL("ALTER TABLE UniverseSquares ADD ");
		}

		CreateUniverse(dbs, 8);
	}


	public static void CreateUniverse(MySQLConnection dbs, int size) throws SQLException {
		GamesTable game = new GamesTable(dbs);
		for (int y=0 ; y<size ; y++) {
			for (int x=0 ; x<size ; x++) {
				//if (Functions.rnd(1, 4) == 1) {  No!  as we need all planets to be adjacent to another planet
				if (dbs.getScalarAsInt("SELECT Count(*) FROM UniverseSquares WHERE MapX = " + x + " AND MapY = " + y) == 0) { // Don't create if it already exists!
					String name = "Planet #" + (dbs.getScalarAsInt("SELECT Count(*) FROM UniverseSquares")+1);
					int gameid = game.createGame(4, 0, 0, 0, 0, AbstractMission.UNIVERSE_MISSION, GameRequestsTable.GT_NORMAL, true, false, 100, -1, -1, -1);
					dbs.RunIdentityInsert("INSERT INTO UniverseSquares (MapX, MapY, Name, GameID) VALUES (" + x + ", " + y + ", " + SQLFuncs.s2sql(name) + ", " + gameid + ")");
				}			
				//}
			}
		}
		game.close();
	}


	public static void SetOwnerID(MySQLConnection dbs, int id, int loginid) throws SQLException {
		dbs.runSQLUpdate("UPDATE UniverseSquares SET OwnerID = " + loginid + " WHERE UniverseSquareID = " + id);
	}


}
