package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

public class UploadedMapsTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("UploadedMaps") == false) {
			dbs.runSQL("CREATE TABLE UploadedMaps (UploadedMapID INTEGER AUTO_INCREMENT KEY, LoginID INTEGER, OriginalFilename VARCHAR(128), ActualFilename VARCHAR(64), MissionID INTEGER, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
		}
		if (dbs.doesColumnExist("UploadedMaps", "Name") == false) {
			dbs.runSQL("ALTER TABLE UploadedMaps ADD Name VARCHAR(128)");
		}
		if (dbs.doesColumnExist("UploadedMaps", "Description") == false) {
			dbs.runSQL("ALTER TABLE UploadedMaps ADD Description VARCHAR(1024)");
		}
	}

	public static void AddRec(MySQLConnection dbs, int loginid, String name, String desc, String orig_filename, String new_filename, int mission) throws SQLException {
		/*if (desc.isEmpty()) {
			desc = GetDesc(dbs, name);
		}*/
		// Delete existing rec with same user and name
		dbs.runSQLDelete("DELETE FROM UploadedMaps WHERE LoginID = " + loginid + " AND Name = " + SQLFuncs.s2sql(name));
		dbs.RunIdentityInsert("INSERT INTO UploadedMaps (LoginID, Name, OriginalFilename, ActualFilename, MissionID) VALUES (" + loginid + ", " + SQLFuncs.s2sql(name) + ", " +  SQLFuncs.s2sql(orig_filename) + ", " +  SQLFuncs.s2sql(new_filename) + ", " + mission + ")");
	}

	
	public static String GetActualFilename(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsString("SELECT ActualFilename FROM UploadedMaps WHERE UploadedMapID = " + id);
	}


	public static String GetName(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsString("SELECT Name FROM UploadedMaps WHERE UploadedMapID = " + id);
	}


	public static String GetDesc(MySQLConnection dbs, String name) throws SQLException {
		return dbs.getScalarAsString("SELECT COALESCE(Description, '') FROM UploadedMaps WHERE Name = " + SQLFuncs.s2sql(name));
	}
}
