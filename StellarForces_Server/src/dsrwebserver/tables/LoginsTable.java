package dsrwebserver.tables;

import java.awt.Point;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.html.HTMLFunctions;
import ssmith.lang.Dates;
import ssmith.util.MemCache;
import ssmith.util.MyList;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.dsr.leaguetable;

public final class LoginsTable extends AbstractTable {

	private static MemCache mc_display_names = new MemCache(Dates.DAY);

	public enum GameType {ALL, ONLY_PRACTISE, ONLY_NON_PRACTISE};

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		// todo - drop HasAndroid
		if (dbs.doesTableExist("Logins") == false) {
			dbs.runSQL("CREATE TABLE Logins (LoginID INTEGER AUTO_INCREMENT KEY, Login VARCHAR(128), Pwd VARCHAR(50), LoginCode VARCHAR(32), LastLoginDate DATETIME, PrevLoginDate DATETIME, DisplayName VARCHAR(128), Admin TINYINT, TotalTurns INTEGER, TotalDaysTakingTurns INTEGER, EmailForumReplies TINYINT, HasBeenEmailedSinceLastLogin TINYINT, Location VARCHAR(256), AboutMe VARCHAR(4096), TotalConcedes SMALLINT, Website VARCHAR(256), EmailOnTurn TINYINT, KilledOwnUnitsCount INTEGER, CampTeamName VARCHAR(128), ImportedIntoForums TINYINT, OptedOutOfEmails TINYINT, DateLastMktEmailRcvd DATETIME, Disabled TINYINT, EmailValidated TINYINT, TimeLastInactivityPointsAdded DATETIME, OnHolidayText VARCHAR(1024), HasAndroid TINYINT, CampaignCredits INTEGER, DateLastAwardAwarded DATETIME, FactionID INTEGER, HasPCClient TINYINT, DateLastReminder DATETIME, BeenSentInvalidEmailMsg TINYINT, DateLastCampUnitReplenishment DATETIME, BeenSentWhyNotPlayingMsg TINYINT, ELOPoints INTEGER, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
		}
		if (dbs.doesColumnExist("Logins", "UniverseCreds") == false) {
			dbs.runSQL("ALTER TABLE Logins ADD UniverseCreds INTEGER");
		}
		if (dbs.doesColumnExist("Logins", "UniverseInventoryCreated") == false) {
			dbs.runSQL("ALTER TABLE Logins ADD UniverseInventoryCreated TINYINT");
		}
		if (dbs.doesColumnExist("Logins", "HighestELOPoints") == false) {
			dbs.runSQL("ALTER TABLE Logins ADD HighestELOPoints INTEGER");
		}
		/*if (dbs.doesColumnExist("Logins", "TotalGames") == false) {
			dbs.runSQL("ALTER TABLE Logins ADD TotalGames INTEGER");
		}*/

		if (dbs.doesIndexExist("Logins", "idx_Login_Pwd") == false) {
			dbs.runSQL("CREATE INDEX idx_Login_Pwd ON Logins(Login, Pwd)");
		}
	}


	public LoginsTable(MySQLConnection sqldbs) throws SQLException {
		super(sqldbs, "Logins", "LoginID");
	}


	/**
	 * This will either create the login, or return a warning as to why it could not be created.
	 * @param pwd
	 * @param displayname
	 * @param email
	 * @return
	 * @throws SQLException
	 */
	public String createLogin(String pwd, String displayname, String email, boolean autoregister) throws SQLException {
		email = email.replaceAll(" ", "");
		pwd = pwd.trim();
		displayname = displayname.trim();
		if (pwd.length() == 0 || displayname.length() == 0) {
			return "Please enter all the required details.";
		} else if (email.length() > 0 && dbs.getResultSet("SELECT * FROM Logins WHERE Login = " + SQLFuncs.s2sql(email)).next()) {
			return "Sorry, one of those details has been taken.";
		} else if (dbs.getResultSet("SELECT * FROM Logins WHERE DisplayName = " + SQLFuncs.s2sql(displayname)).next()) {
			return "Sorry, one of those details has been taken.";
		} else if (displayname.indexOf("@") >= 0) {
			return "Sorry, please do not have an '@' in your name.";
		}
		int id = dbs.RunIdentityInsert_Syncd("INSERT INTO Logins (Pwd, LoginCode, DisplayName, Login, TotalTurns, TotalDaysTakingTurns, Location, AboutMe, TotalConcedes, EmailForumReplies, Website, EmailOnTurn, HasPCClient) VALUES (" + SQLFuncs.s2sql(pwd.trim()) + ", " + System.currentTimeMillis() + ", " + SQLFuncs.s2sql(displayname) + ", " + SQLFuncs.s2sql(email) + ", 0, 0, '', '', 0, 1, '', 1, 1)");

		// Spammer?
		if (email.endsWith("@163.com")) {
			SetLoginDisabled(dbs, id, 1);
		}

		try {
			WebsiteEventsTable.AddRec(dbs, " New login '" + displayname + "' has been registered.", -1);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}

		//DSRWebServer.SendEmail(DSRWebServer.cfgfile.get("admin_email").toString(), "New login on " + AppletMain.TITLE, "New Login: " + displayname);

		// Create the login in PHPBB3
		/*try {
			System.out.println("Adding user to PHP"); // Just print it.
			URL url = new URL("http://127.0.0.3:8081/importusers.php");
			Scanner in = new Scanner(url.openStream());
			while (in.hasNextLine()) { 
				String line = in.nextLine();
				System.out.println(line); // Just print it.
			} 
			in.close(); 
		} catch (Exception ex) {
			// Shouldn't happen
			DSRWebServer.HandleError(ex, true);
		}*/

		this.selectRow(id);

		try {
			if (DSRWebServer.SS_LOGIN_ID > 0) {
				if (autoregister == false) {
					MessagesTable.SendMsg(dbs, DSRWebServer.SS_LOGIN_ID, getID(), "Hello " + displayname + " and welcome to " + DSRWebServer.TITLE, "Welcome to " + DSRWebServer.TITLE + ", the turn-based tactical strategy game.  You have been registered with the following details:\n\nLogin: " + getDisplayName() + "\nPassword: " + getPassword() + "\nEmail Address: " + getEmail() + "\n\nIf your email address is incorrect, please just reply with the correct one.  If you have any questions, suggestions or queries, no matter how trivial, please just reply to this or ask on the forums at " + DSRWebServer.NEW_FORUM_LINK + ".  Feel free to join *any* game and play against any player.  Although there is an AI to play against, it is very simple and doesn't provide much of an opponent, so we recommend playing against another person.\n\nThere is now an online tutorial to help you play the game, which can be viewed at http://www.stellarforces.com/dsr/webtutorial.cls .\n\nIf you like the game, please tell others about it and leave a good review.  If you don't, please tell us!\n\nHope you have fun, and good luck!");
				} else {
					MessagesTable.SendMsg(dbs, DSRWebServer.SS_LOGIN_ID, getID(), "Hello " + displayname + " and welcome to " + DSRWebServer.TITLE, "Welcome to " + DSRWebServer.TITLE + ", the turn-based tactical strategy game.  You have been automatically registered with the following details:\n\nLogin: " + getDisplayName() + "\nPassword: " + getPassword() + "\n\nNote that since we do not have your email address, we cannot email you any turn notifications.  You can add your email address via the website, at My Pages -> Settings.\n\nIf you have any questions, suggestions or queries, no matter how trivial, please just reply to this or ask on the forums at " + DSRWebServer.NEW_FORUM_LINK + ".  Feel free to join *any* game and play against any player.  Although there is an AI to play against, it is very simple and doesn't provide much of an opponent, so we recommend playing against another person.\n\nThere is now an online tutorial to help you play the game, which can be viewed at http://www.stellarforces.com/dsr/webtutorial.cls .\n\nIf you like the game, please tell others about it and leave a good review.  If you don't, please tell us!\n\nHope you have fun, and good luck!");
				}
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
		return "";
	}


	/**
	 * Pass a blank pwd to not change it.
	 * @param pwd
	 * @param displayname
	 * @param email
	 * @return
	 * @throws SQLException
	 */
	public String updateLogin(String pwd, String email, String location, String aboutme, String website, boolean email_on_turn, String camp_team_name, String on_holiday) throws SQLException {
		email = email.replaceAll(" ", "");
		pwd = pwd.trim();
		if (email.length() > 0 && dbs.getResultSet("SELECT * FROM Logins WHERE Login = " + SQLFuncs.s2sql(email) + " AND LoginID <> " + this.getLoginID()).next()) {
			return "Sorry, one of those details has been taken.";
		}

		if (pwd.length() == 0) {
			pwd = this.getPassword();
		}

		// Can't change display name as it causes problems linking the user to phpBB.
		//DisplayName = " + SQLFuncs.s2sql(displayname) + ", 

		dbs.runSQL("UPDATE Logins SET Pwd = " + SQLFuncs.s2sql(pwd.trim()) + ",  Login = " + SQLFuncs.s2sql(email) + ", Location = " + SQLFuncs.s2sql(location) + ", AboutMe = " + SQLFuncs.s2sql(aboutme) + ", Website = " + SQLFuncs.s2sql(website) + ", EmailOnTurn = " + SQLFuncs.b201(email_on_turn) + ", CampTeamName = " + SQLFuncs.s2sql(camp_team_name)  + ", OnHolidayText = " + SQLFuncs.s2sql(on_holiday)  + " WHERE LoginID = " + this.getLoginID());
		this.selectRow(this.getLoginID()); // To refresh the data 
		return "";
	}


	public String updateLogin(String email) throws SQLException {
		email = email.replaceAll(" ", "");
		if (dbs.getResultSet("SELECT * FROM Logins WHERE Login = " + SQLFuncs.s2sql(email) + " AND LoginID <> " + this.getLoginID()).next()) {
			return "Sorry, that login has been taken.";
		}

		dbs.runSQL("UPDATE Logins SET Login = " + SQLFuncs.s2sql(email) + " WHERE LoginID = " + this.getLoginID());
		this.selectRow(this.getLoginID()); // To refresh the data 
		return "";
	}


	public void setLastLoginDate() throws SQLException {
		dbs.runSQL("UPDATE Logins SET LastLoginDate = " + SQLFuncs.d2sql(new Date(), true) + " WHERE LoginID = " + this.getLoginID());
		this.refreshData();

	}


	public void setEmailAddressValidated() throws SQLException {
		dbs.runSQL("UPDATE Logins SET EmailValidated = 1 WHERE LoginID = " + this.getLoginID());
		this.refreshData();

	}


	public static boolean DoesLoginExistAndEnabled(MySQLConnection dbs, String login, String pwd) throws SQLException {
		ResultSet rs = dbs.getResultSet("SELECT * FROM Logins WHERE (Login = " + SQLFuncs.s2sql(login) + " OR DisplayName = " + SQLFuncs.s2sql(login) + ") AND Pwd = " + SQLFuncs.s2sql(pwd) + " AND COALESCE(Disabled, 0) <> 1");
		boolean res = rs.next();
		if (res == false) {
			DSRWebServer.p(login + "/" + pwd + " failed.");
		}
		return res;
	}


	public boolean selectUser(String login, String pwd) throws SQLException {
		login = login.trim();
		pwd = pwd.trim();
		rs = dbs.getResultSet("SELECT * FROM Logins WHERE (Login = " + SQLFuncs.s2sql(login.replaceAll(" ", "")) + " OR DisplayName = " + SQLFuncs.s2sql(login) + ") AND Pwd = " + SQLFuncs.s2sql(pwd));
		has_rows = rs.next();
		/*if (!has_rows) {
			//throw new RuntimeException("Login not found in table.");
			//DSRWebServer.SendEmailToAdmin("Login failed", "Login: '" + login + "', pwd: '" + pwd + "'");
		} else {

		}*/
		return has_rows;
	}


	/**
	 * This is used when the user has forgotten their password.
	 * @param login
	 * @return
	 * @throws SQLException
	 */
	public boolean selectUserByEmailAddress(String email) throws SQLException {
		email = email.replaceAll(" ", "");
		rs = dbs.getResultSet("SELECT * FROM Logins WHERE Login = " + SQLFuncs.s2sql(email));
		has_rows = rs.next();
		return has_rows;
	}


	public boolean selectUserByDisplayName(String name) throws SQLException {
		name = name.trim();
		rs = dbs.getResultSet("SELECT * FROM Logins WHERE DisplayName = " + SQLFuncs.s2sql(name));
		has_rows = rs.next();
		return has_rows;
	}


	public boolean checkPassword(String pwd) throws SQLException {
		return rs.getString("pwd").equalsIgnoreCase(pwd);
	}


	public String getDisplayName_Enc(boolean link) throws SQLException {
		if (link) {
			return "<a href=\"/dsr/playerspublicpage.cls?loginid=" + this.getID() + "\">" + HTMLFunctions.HTMLEncode(rs.getString("DisplayName")) + "</a>";
		} else {
			return HTMLFunctions.HTMLEncode(rs.getString("DisplayName"));
		}
	}


	public static String GetDisplayName(MySQLConnection dbs, int loginid) throws SQLException {
		//return dbs.getScalarAsString("SELECT DisplayName FROM Logins WHERE LoginID = " + loginid);
		if (mc_display_names.containsKey(loginid)) {
			return mc_display_names.get(loginid).toString();
		} else {
			String name = dbs.getScalarAsString("SELECT DisplayName FROM Logins WHERE LoginID = " + loginid);
			mc_display_names.put(loginid, name);
			return name;
		}
	}


	public String getLocation() throws SQLException {
		return rs.getString("Location");
	}


	public String getLogin() throws SQLException {
		return rs.getString("Login");
	}


	public boolean isDisabled() throws SQLException {
		return rs.getInt("Disabled") > 0;// || rs.getString("Login").endsWith("@163.com");
	}


	public boolean hasBeenSentInvalidEmailMsg() throws SQLException {
		return rs.getInt("BeenSentInvalidEmailMsg") > 0;
	}


	public boolean hasBeenSentWhyNotPlayingMsg() throws SQLException {
		return rs.getInt("BeenSentWhyNotPlayingMsg") > 0;
	}


	public String getWebsite() throws SQLException {
		String website = rs.getString("Website");
		if (website != null) {
			if (website.equalsIgnoreCase("null") == false) {
				if (website.toLowerCase().startsWith("http://") == false) {
					website = "http://" + website;
				}
				return website;
			}
		}
		return "";
	}


	public String getCampTeamName() throws SQLException {
		String s = rs.getString("CampTeamName");
		if (s != null) {
			if (s.equalsIgnoreCase("null") == false) {
				return s;
			}
		}
		return "";
	}


	public String getAboutMe() throws SQLException {
		return rs.getString("AboutMe");
	}


	public String getOnHolidayText() throws SQLException {
		String s = rs.getString("OnHolidayText");
		if (s != null) {
			if (s.equalsIgnoreCase("null") == false) {
				return s;
			}
		}
		return "";
	}


	public static String GetOnHolidayText(MySQLConnection dbs, int loginid) throws SQLException {
		String s = dbs.getScalarAsString("SELECT OnHolidayText FROM Logins WHERE LoginID = " + loginid);
		if (s != null) {
			if (s.equalsIgnoreCase("null") == false) {
				return s;
			}
		}
		return "";
	}


	public String getDisplayName() throws SQLException {
		//return rs.getString("DisplayName");
		return LoginsTable.GetDisplayName(dbs, this.getID());
	}


	public String getLoginCode() throws SQLException {
		return rs.getString("LoginCode");
	}


	public boolean isAdmin() throws SQLException {
		return rs.getInt("Admin") == 1;
	}


	public int getTotalGamesFinished() throws SQLException {
		return getTotalGamesFinished(GameType.ALL);
	}


	public int getTotalGamesFinished(GameType type) throws SQLException {
		//return GetTotalGamesFinished(dbs, this.getLoginID(), -1);
		String sql = "SELECT Count(*) FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED + " AND " + AbstractTable.GetPlayerSubQuery(this.getID());
		if (type == GameType.ONLY_NON_PRACTISE) {
			sql = sql + " AND GameType = 0 AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI;
		} else if (type == GameType.ONLY_PRACTISE) {
			sql = sql + " AND (GameType = 1 OR Mission = " + AbstractMission.SF_PRACTISE_MISSION + " OR Mission = " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI + ")";
		}
		return  dbs.getScalarAsInt(sql);
	}


	/*public static int GetTotalGamesFinished_(MySQLConnection dbs, int loginid, int mission) throws SQLException {
		String sql = "SELECT Count(*) FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED + " AND " + AbstractTable.GetPlayerSubQuery(loginid) + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI + " AND Mission <> " + AbstractMission.AE_PRACTISE_MISSION;
		if (mission > 0) {
			sql = sql + " AND Mission = " + mission;
		}
		return  dbs.getScalarAsInt(sql);
	}*/


	public int getGamesExperience() throws SQLException {
		return Math.max(0, this.getTotalGamesFinished() - getTotalConcedes());
	}


	public int canPlayInCampaign() throws SQLException {
		if (getGamesExperience() >= DSRWebServer.MIN_CAMP_GAMES) {
			return 1;
		}
		return 0;
	}


	public int getTotalCurrentGames(GameType type) throws SQLException {
		String sql = "SELECT Count(*) FROM Games WHERE " + AbstractTable.GetPlayerSubQuery(this.getID()) + " AND GameStatus < " + GamesTable.GS_FINISHED;
		/*if (inc_practise == false) {
			sql = sql + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI;
		}*/
		if (type == GameType.ONLY_NON_PRACTISE) {
			sql = sql + " AND GameType = 0 AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI;
		} else if (type == GameType.ONLY_PRACTISE) {
			sql = sql + " AND (GameType = 1 OR Mission = " + AbstractMission.SF_PRACTISE_MISSION + " OR Mission = " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI + ")";
		}
		return dbs.getScalarAsInt(sql);
	}


	public int getTotalVictories(GameType type) throws SQLException {
		String sql = "SELECT Count(*) FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED; //Mission <> " + AbstractMission.SF_PRACTISE_MISSION + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI;
		sql = sql + " AND (";
		for (int i=1 ; i<=4 ; i++) {
			sql = sql + "((Player" + i + "ID = " + this.getID() + " AND WinningSide = " + i + ") OR ";
			sql = sql + "(Player" + i + "ID = " + this.getID() + " AND WinningSide2 = " + i + ")) ";
			if (i<4) {
				sql = sql + " OR ";
			}
		}
		sql = sql + " ) ";
		if (type == GameType.ONLY_NON_PRACTISE) {
			sql = sql + " AND GameType = 0 AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI;
		} else if (type == GameType.ONLY_PRACTISE) {
			sql = sql + " AND (GameType = 1 OR Mission = " + AbstractMission.SF_PRACTISE_MISSION + " OR Mission = " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI + ")";
		}
		return dbs.getScalarAsInt(sql);
	}


	public int getTotalVictoriesByMission(boolean inc_practise, int days_ago, int mission) throws SQLException {
		String sql = "SELECT Count(*) FROM Games WHERE Mission <> " + AbstractMission.SF_PRACTISE_MISSION + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI;
		sql = sql + " AND (";
		for (int i=1 ; i<=4 ; i++) {
			sql = sql + "((Player" + i + "ID = " + this.getID() + " AND WinningSide = " + i + ") OR ";
			sql = sql + "(Player" + i + "ID = " + this.getID() + " AND WinningSide2 = " + i + ")) ";
			if (i<4) {
				sql = sql + " OR ";
			}
		}
		sql = sql + ") AND GameStatus = " + GamesTable.GS_FINISHED;
		if (inc_practise == false) {
			sql = sql + " AND COALESCE(GameType, 0) = " + GameRequestsTable.GT_NORMAL;
		}
		if (days_ago > 0) {
			sql = sql + " AND DATEDIFF(CurDate(), DateFinished) <= " + days_ago;
		}
		if (mission > 0) {
			sql = sql + " AND Mission = " + mission;
		}
		/*if (include_camp == false) {
			sql = sql + " AND COALESCE(CampGame, 0) = 0";
		}*/
		return dbs.getScalarAsInt(sql);
	}


	/*public int recalcPoints_ORIG() throws SQLException {
		String QUAL = leaguetable.GetQual();

		int calc_wins = 0;
		int tot_lose = 0;
		for (int s=1 ; s<=4 ; s++) {
			calc_wins += dbs.getScalarAsInt("SELECT SUM(PointsForWin" + s + ") FROM Games WHERE " + QUAL + " AND (WinningSide = " + s + " OR WinningSide2 = " + s + ") AND Player" + s + "ID = " + rs.getInt("LoginID"));
			tot_lose += dbs.getScalarAsInt("SELECT Count(*) FROM Games WHERE " + QUAL + " AND WinningSide > 0 AND (WinningSide <> " + s + " AND (COALESCE(WinningSide2, 0) <> " + s + " OR COALESCE(WinningSide2, 0) = 0)) AND Player" + s + "ID = " + rs.getInt("LoginID"));
		}
		int calc_draws = dbs.getScalarAsInt("SELECT Count(*) FROM Games WHERE " + QUAL + " AND WinningSide < 1 AND " + AbstractTable.GetPlayerSubQuery(rs.getInt("LoginID")));

		return (calc_draws * leaguetable.DRAW_POINTS) + (calc_wins) - (tot_lose*leaguetable.LOSE_POINTS);
	}*/


	/*public void recalcPoints() throws SQLException {
		dbs.runSQLDelete("DELETE FROM LeagueTemp WHERE LoginID = " + this.getID());

		String QUAL = leaguetable.GetQual();

		for (int s=1 ; s<=4 ; s++) {
			// Do wins
			ResultSet rs = dbs.getResultSet("SELECT PointsForWin" + s + " FROM Games WHERE " + QUAL + " AND (WinningSide = " + s + " OR WinningSide2 = " + s + ") AND Player" + s + "ID = " + this.getID());
			while (rs.next()) {
				LeagueTempTable.AddRec(dbs, this.getID(), rs.getInt("PointsForWin" + s));
			}
			rs.close();

			// Do draws
			rs = dbs.getResultSet("SELECT 1 FROM Games WHERE " + QUAL + " AND WinningSide < 0 AND Player" + s + "ID = " + this.getID());
			while (rs.next()) {
				LeagueTempTable.AddRec(dbs, this.getID(), leaguetable.DRAW_POINTS);
			}
			rs.close();
		}

		// Now loop through results
		ResultSet rs = dbs.getResultSet("SELECT Points FROM LeagueTemp WHERE LoginID = " + this.getID() + " ORDER BY Points DESC LIMIT " + leaguetable.MAX_GAMES);
		int tot = 0;
		while (rs.next()) {
			tot += rs.getInt("Points");
		}

		dbs.runSQLUpdate("UPDATE Logins SET LeaguePoints = " + tot + " WHERE LoginID = " + this.getID());
		this.refreshData();
		//return (calc_draws * leaguetable.DRAW_POINTS) + (calc_wins) - (tot_lose*leaguetable.LOSE_POINTS);
	}*/


	public Point getMinAndMaxOppRange(int pcent) throws SQLException {
		int our_pts = this.getELOPoints();
		// 200 * 10 / 100
		int diff = (our_pts * pcent) / (100);
		Point p = new Point();
		p.x = our_pts - diff;
		if (p.x < 0) {
			p.x = 0;
		}
		p.y = our_pts + diff;
		return p;
	}


	public int getTotalDefeats(GameType type) throws SQLException {
		return this.getTotalGamesFinished(type) - this.getTotalVictories(type) - this.getTotalDraws(type);
	}


	public int getTotalDraws(GameType type) throws SQLException {
		String sql = "SELECT Count(*) FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED + " AND (";
		for (int i=1 ; i<=4 ; i++) {
			sql = sql + "(Player" + i + "ID = " + this.getID() + " AND WinningSide <= 0)";
			if (i<4) {
				sql = sql + " OR ";
			}
		}
		sql = sql + " ) ";
		//sql = sql + ") AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI + " AND 		
		if (type == GameType.ONLY_NON_PRACTISE) {
			sql = sql + " AND GameType = 0 AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI;
		} else if (type == GameType.ONLY_PRACTISE) {
			sql = sql + " AND (GameType = 1 OR Mission = " + AbstractMission.SF_PRACTISE_MISSION + " OR Mission = " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI + ")";
		}

		return dbs.getScalarAsInt(sql);
	}


	public int getTotalConcedes() throws SQLException {
		return rs.getInt("TotalConcedes");
	}


	/*public int getLeaguePoints() throws SQLException {
		return rs.getInt("LeaguePoints");
	}*/


	public int getELOPoints() throws SQLException {
		return rs.getInt("ELOPoints");
	}


	public int getTotalForumPosts() throws SQLException {
		return dbs.getScalarAsInt("SELECT Count(*) FROM ForumPostings WHERE UserID = " + this.getID());
	}


	public int getTotalTurns() throws SQLException {
		return rs.getInt("TotalTurns");
	}


	public Date getDateJoined() throws SQLException {
		return rs.getTimestamp("DateCreated");
	}


	public Date getLastLoginDate() throws SQLException {
		return rs.getDate("LastLoginDate");
	}


	/*public Date getLastClientLoginDate() throws SQLException {
		return rs.getDate("LastClientLoginDate");
	}*/


	public int getTotalDaysTakingTurns() throws SQLException {
		return rs.getInt("TotalDaysTakingTurns");
	}


	public static String GetDisplayNames_Enc(MySQLConnection dbs, MyList<Integer> loginids, boolean link) throws SQLException {
		StringBuffer str = new StringBuffer();
		for (Integer i : loginids) {
			str.append(GetDisplayName_Enc(dbs, i, link));
			str.append(", ");
		}
		if (str.length() > 2) {
			str.delete(str.length()-2, str.length());
		}
		return str.toString();
	}


	public static String GetDisplayName_Enc(MySQLConnection dbs, int loginid, boolean link) throws SQLException {
		if (loginid > 0) {
			if (link) {
				//String s = dbs.getScalarAsString("SELECT DisplayName FROM Logins WHERE LoginID = " + loginid);
				return "<a href=\"/dsr/playerspublicpage.cls?loginid=" + loginid + "\" rel=\"nofollow\">" + HTMLFunctions.HTMLEncode(LoginsTable.GetDisplayName(dbs, loginid)) + "</a>";
			} else {
				//String s = dbs.getScalarAsString("SELECT DisplayName FROM Logins WHERE LoginID = " + loginid);
				return HTMLFunctions.HTMLEncode(LoginsTable.GetDisplayName(dbs, loginid));
			}
		} else {
			return "[Empty]";
		}
	}


	public static String GetEmail(MySQLConnection dbs, int loginid) throws SQLException {
		String s = dbs.getScalarAsString("SELECT Login FROM Logins WHERE LoginID = " + loginid);
		return s;
	}


	public String getEmail() throws SQLException {
		return rs.getString("Login");
	}


	public String getPassword() throws SQLException {
		return rs.getString("Pwd");
	}


	public int getLoginID() throws SQLException {
		return rs.getInt("LoginID");
	}


	public void sendEmail(String subject, String msg) throws SQLException {
		try {
			// Append notices
			if (this.getFactionID() == 0 && this.canPlayInCampaign() == 1) {
				msg = msg + "\n\nAlso, you are now qualified to play in the Stellar Forces campaign.  Join a faction to get started and take over the galaxy!";
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}
		DSRWebServer.SendEmail(this.getEmail(), subject, "Hello " + this.getDisplayName() + ",\n\n" + msg);

		/*try {
			if (this.getID() == 3742) {
				DSRWebServer.SendEmailToAdmin("Email sent to Trolly", "Email text wes:\n\n" + msg);
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}*/
	}


	public void sendMsg(int fromid, String subject, String msg) throws SQLException {
		MessagesTable.SendMsg(dbs, fromid, this.getID(), subject, msg);
	}


	public void incTotalTurns() throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET TotalTurns = TotalTurns + 1 WHERE LoginID = " + this.getID());
		this.refreshData();
	}


	public void setDateLastMktEmailRcvd() throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET DateLastMktEmailRcvd = NOW() WHERE LoginID = " + this.getID());
		this.refreshData();
	}


	public static void IncKilledOwnUnitsCount(MySQLConnection dbs, int loginid) throws SQLException {
		try {
			dbs.runSQLUpdate("UPDATE Logins SET KilledOwnUnitsCount = COALESCE(KilledOwnUnitsCount, 0) + 1 WHERE LoginID = " + loginid);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}
	}


	public void incTotalDaysTakingTurns(int amt) throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET TotalDaysTakingTurns = TotalDaysTakingTurns + " + amt + " WHERE LoginID = " + this.getID());
		this.refreshData();
	}


	public void sendNextTurnEmail(String mission_title, String opponents, String mission, String log, int gid, String extra) throws SQLException {
		if (this.getID() != DSRWebServer.AI_LOGIN_ID) {
			if (this.emailOnTurn()) {
				this.sendEmail("Your Turn in " + mission_title, "It is now your turn in your game against " + opponents + " in mission " + mission  + ".  " + extra + "Below is the game log from the previous turn.\n\n" + log);
				// You can view a playback of the mission at its current point at " + DSRWebServer.WEBSITE_URL + "/dsr/playbackapplet/playbackpage.cls?gid=" + gid + "
			}
		}
	}


	public void setOptedOutOfEmails(boolean b) throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET OptedOutOfEmails = " + SQLFuncs.b201(b) + " WHERE LoginID = " + this.getID());
	}


	public boolean emailForumReplies() throws SQLException {
		return rs.getInt("EmailForumReplies") == 1;
	}


	public void addConcede() throws SQLException {
		try {
			dbs.runSQLUpdate("UPDATE Logins SET TotalConcedes = TotalConcedes + 1 WHERE LoginID = " + this.getID());
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}
	}


	public boolean emailOnTurn() throws SQLException {
		return rs.getInt("EmailOnTurn") == 1;
	}


	public void setEmailOnTurn(boolean b) throws SQLException {
		dbs.runSQL("UPDATE Logins SET EmailOnTurn = " + SQLFuncs.b201(b) + " WHERE LoginID = " + this.getLoginID());
		this.refreshData();
	}


	public void setEmailForumReplies(boolean b) throws SQLException {
		dbs.runSQL("UPDATE Logins SET EmailForumReplies = " + SQLFuncs.b201(b) + " WHERE LoginID = " + this.getLoginID());
		this.refreshData();
	}


	public boolean isPlayerPlayingInCampaign() throws SQLException {
		String sql = "SELECT Count(*) FROM Games WHERE CampGame = 1 AND GameStatus < " + GamesTable.GS_FINISHED + " AND " + AbstractTable.GetPlayerSubQuery(this.getID()) + " LIMIT 1";
		return  dbs.getScalarAsInt(sql) > 0;
	}


	public boolean doesPlayerHaveTooManyFactionRequestOpen() throws SQLException {
		String sql = "SELECT Count(*) FROM GameRequests WHERE Player1ID = " + this.getID() + " AND COALESCE(Accepted, 0) = 0 AND CampGame = 1 AND AttackingFactionID > 0";
		return  dbs.getScalarAsInt(sql) > 3;
	}


	public int getPointsForWinAgainst(LoginsTable login) throws SQLException {
		if (login.getID() == DSRWebServer.AI_LOGIN_ID) {
			return 0;
		} else {
			return getPointsForWinAgainst(login.getELOPoints());
		}
	}


	public int getPointsForLoseAgainst(LoginsTable login) throws SQLException {
		if (login.getID() == DSRWebServer.AI_LOGIN_ID) {
			return 0;
		} else {
			return getPointsForLoseAgainst(login.getELOPoints());
		}
	}


	public int getPointsForWinAgainst(int opp_pts) throws SQLException {
		int our_pts = this.getELOPoints();
		return leaguetable.GetPointsForWinAgainst(our_pts, opp_pts);
	}


	public int getPointsForLoseAgainst(int opp_pts) throws SQLException {
		int our_pts = this.getELOPoints();
		return leaguetable.GetPointsForLoseAgainst(our_pts, opp_pts);
	}


	public static void SetLoginDisabled(MySQLConnection dbs, int id, int i) throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET Disabled = " + i + " WHERE LoginID = " + id);
	}


	public static void SetHasBeenSentInvalidEmailMsg(MySQLConnection dbs, int id) throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET BeenSentInvalidEmailMsg = 1 WHERE LoginID = " + id);
	}


	public static void SetHasBeenSentWhyNotPlayingMsg(MySQLConnection dbs, int id) throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET BeenSentWhyNotPlayingMsg = 1 WHERE LoginID = " + id);
	}


	public static boolean IsLoginDisabled(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsInt("SELECT Disabled FROM Logins WHERE LoginID = " + id) == 1;
	}


	public static boolean IsAdmin(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsInt("SELECT Admin FROM Logins WHERE LoginID = " + id) == 1;
	}


	public static boolean HasBeenSentInvalidEmailMsg(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsInt("SELECT BeenSentInvalidEmailMsg FROM Logins WHERE LoginID = " + id) == 1;
	}


	public static boolean HasBeenSentWhyNotPlayingMsg(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsInt("SELECT BeenSentWhyNotPlayingMsg FROM Logins WHERE LoginID = " + id) == 1;
	}


	public static int GetFactionID(MySQLConnection dbs, int id) throws SQLException {
		return dbs.getScalarAsInt("SELECT FactionID FROM Logins WHERE LoginID = " + id);
	}


	/*public void setHasAndroid(int i) throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET HasAndroid = " + i + " WHERE LoginID = " + this.getID());
		this.refreshData();
	}*/


	public void setHasPCClient(int i) throws SQLException {
		//try {
		dbs.runSQLUpdate("UPDATE Logins SET HasPCClient = " + i + " WHERE LoginID = " + this.getID());
		this.refreshData();
		/*} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}*/
	}


	public void setDatelastAwardAwarded() throws SQLException {
		//try {
		dbs.runSQLUpdate("UPDATE Logins SET DatelastAwardAwarded = NOW() WHERE LoginID = " + this.getID());
		this.refreshData();
		/*} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}*/
	}


	public void incCampCredits(int inc) throws SQLException {
		setCampCredits(this.getCampCredits() + inc);
	}


	public void setCampCredits(int amt) throws SQLException {
		if (amt < 0) {
			amt = 0;
		}
		dbs.runSQLUpdate("UPDATE Logins SET CampaignCredits = " + amt + " WHERE LoginID = " + this.getID());
		this.refreshData();
	}


	public void setFactionID(int id) throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET FactionID = " + id + " WHERE LoginID = " + this.getID());
		this.refreshData();
	}


	public void setEmailedAboutFaction() throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET EmailedAboutFaction = 1 WHERE LoginID = " + this.getID());
		this.refreshData();
	}


	public int getCampCredits() throws SQLException {
		return dbs.getScalarAsInt("SELECT COALESCE(CampaignCredits, 0) FROM Logins WHERE LoginID = " + this.getID());
	}


	public int getFactionID() throws SQLException {
		return dbs.getScalarAsInt("SELECT FactionID FROM Logins WHERE LoginID = " + this.getID());
	}


	public String getUnplayedMissions(int max) throws SQLException {
		int count = 0;
		StringBuffer str = new StringBuffer();
		for (int j=0 ; j<AbstractMission.SF_MISSIONS.length ; j++) {
			int missionid = AbstractMission.SF_MISSIONS[j];
			if (AbstractMission.IsValidMission(missionid)) {
				String sql = "SELECT Count(*) FROM Games WHERE " + GamesTable.GetPlayerSubQuery(this.getID()) + " AND Mission = " + missionid;
				if (dbs.getScalarAsInt(sql) == 0) {
					count++;
					str.append(AbstractMission.GetMissionNameFromType(missionid, true, false) + ", ");
					if (count > max) {
						str.append(" and more, ");
						break;
					}
				}
			}
		}
		if (str.length() == 0) {
			return "You have played all of them!";
		} else {
			str.delete(str.length()-2, str.length());
			return str.toString();
		}
	}


	public static int GetIDFromDisplayName(MySQLConnection dbs, String name) throws SQLException {
		ResultSet rs = dbs.getResultSet("SELECT LoginID FROM Logins WHERE DisplayName = " + SQLFuncs.s2sql(name));
		if (rs.next()) {
			return rs.getInt("LoginID");
		} else {
			return -1;
		}

	}


	public int getDaysSinceLastReminder() throws SQLException {
		int days = dbs.getScalarAsInt("SELECT COALESCE(DATEDIFF(CURDATE(), DateLastReminder), 999) FROM Logins WHERE LoginID = " + this.getID());
		return days;
	}


	public void updateDateLastReminder() throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET DateLastReminder = CURDATE() WHERE LoginID = " + this.getID());
	}


	public void clearDateLastReminder() throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET DateLastReminder = NULL WHERE LoginID = " + this.getID());
	}


	public int getDaysSinceLastLastCampUnitReplenishment() throws SQLException {
		int days = dbs.getScalarAsInt("SELECT COALESCE(DATEDIFF(CURDATE(), DateLastCampUnitReplenishment), 999) FROM Logins WHERE LoginID = " + this.getID());
		return days;
	}


	public void setDateLastCampUnitReplenishment() throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET DateLastCampUnitReplenishment = CURDATE() WHERE LoginID = " + this.getID());
	}


	public int joinFaction() throws SQLException {
		int worst_fid = FactionsTable.GetFactionWithLessThanPlayers(dbs, 1);
		if (worst_fid == 0) {
			worst_fid = FactionsTable.GetFactionWithLessThanPlayers(dbs, 2);
			if (worst_fid == 0) {
				worst_fid = FactionsTable.GetFactionWithLeastLeaguePoints(dbs);
			}
		}
		//int worst_fid = FactionsTable.GetWorstFaction(dbs); // SCS 24/4/14
		// Email other players - before we add the new member!
		FactionsTable.EmailFactionMembers(dbs, worst_fid, "New Faction Member", getDisplayName() + " has joined your faction.", -1);
		setFactionID(worst_fid);
		StarmapLogTable.AddRec(dbs, getDisplayName() + " has joined " + FactionsTable.GetName(dbs, getFactionID()) + ".", true);
		return worst_fid;
	}


	public static int GetELOPoints(MySQLConnection dbs, int loginid) throws SQLException {
		return dbs.getScalarAsInt("SELECT ELOPoints FROM Logins WHERE LoginID = " + loginid);
	}


	public static void IncELOPoints(MySQLConnection dbs, int loginid, int inc) throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET ELOPoints = GREATEST(0, (ELOPoints + " + inc + ")) WHERE LoginID = " + loginid);
		dbs.runSQLUpdate("UPDATE Logins SET HighestELOPoints = GREATEST(ELOPoints, COALESCE(HighestELOPoints, 0)) WHERE LoginID = " + loginid);
	}


	public boolean inventoryCreated() throws SQLException {
		return rs.getInt("UniverseInventoryCreated") == 1;
	}


	public void setInventoryCreated() throws SQLException {
		dbs.runSQLUpdate("UPDATE Logins SET UniverseInventoryCreated = 1 WHERE LoginID = " + this.getID());
		this.refreshData();
	}


	public MyList<Integer> getUniverseComrades() throws SQLException {
		String sql = "SELECT Login1ID FROM UniverseTreaties WHERE Login2ID = " + this.getID() + " UNION SELECT Login2ID FROM UniverseTreaties WHERE Login1ID = " + this.getID();
		return MyList.CreateFromCSVInts(dbs.getListAsString(sql));
	}



}

