package dsrwebserver.tables;

import java.sql.ResultSet;
import java.sql.SQLException;

import dsrwebserver.DSRWebServer;

import ssmith.dbs.MySQLConnection;

/**
 * This stores all the equipment used in the game, and its location.
 *
 */
public final class EquipmentTable extends AbstractTable {

	private EquipmentTypesTable eq_types = new EquipmentTypesTable(dbs);

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("Equipment") == false) {
			dbs.runSQL("CREATE TABLE Equipment (EquipmentID INTEGER AUTO_INCREMENT KEY, GameID INTEGER, UnitID INTEGER, EquipmentTypeID INTEGER, Ammo SMALLINT, MapX SMALLINT, MapY SMALLINT, ExplodeTurns INTEGER, Primed TINYINT, Destroyed TINYINT, EquipmentCode VARCHAR(32), LastUnitToTouch INTEGER, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		for (int s=1 ; s<= 4 ; s++) {
			if (dbs.doesColumnExist("Equipment", "SeenBySide" + s) == false) {
				dbs.runSQL("ALTER TABLE Equipment ADD SeenBySide" + s + " TINYINT");
			}
		}
		if (dbs.doesColumnExist("Equipment", "PrimedTime") == false) {
			dbs.runSQL("ALTER TABLE Equipment ADD PrimedTime INTEGER");
		}

		try {
			if (dbs.doesIndexExist("Equipment", "idx_Equipment_GameID") == false) {
				dbs.runSQL("create index idx_Equipment_GameID ON Equipment (GameID)");
			}
			if (dbs.doesIndexExist("Equipment", "idx_Equipment_UnitID") == false) {
				dbs.runSQL("create index idx_Equipment_UnitID ON Equipment (UnitID)");
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}


	public EquipmentTable(MySQLConnection dbs) {
		super(dbs, "Equipment", "EquipmentID");
	}


	public boolean selectRow(int id, boolean error) throws SQLException {
		String sql = "SELECT * FROM Equipment ";
		sql = sql + " INNER JOIN EquipmentTypes ON EquipmentTypes.EquipmentTypeID = Equipment.EquipmentTypeID";
		sql = sql + " WHERE EquipmentID = " + id;
		rs = dbs.getResultSet(sql);
		has_rows = rs.next();
		if (!has_rows && error) {
			throw new RuntimeException("Error selecting equipment id " + id);
		}
		return has_rows;
	}


	public static int CreateEquipment(MySQLConnection dbs, int gameid, int unitid, int equipment_type_id, int ammo) throws SQLException {
		return dbs.RunIdentityInsert_Syncd("INSERT INTO Equipment (EquipmentCode, GameID, UnitID, EquipmentTypeID, Ammo) VALUES (" + System.currentTimeMillis() + ", " + gameid + ", " + unitid + ", " + equipment_type_id + ", " + ammo + ")");
	}


	public static void Update(MySQLConnection dbs, int equipid, int unitid, int mapx, int mapy, int ammo, int explode_turn, int primed, int destroyed, int last_unit_to_touch, int seen_by_side[], long primed_time) throws SQLException {
		dbs.runSQLUpdate("UPDATE Equipment SET UnitID = " + unitid + ", MapX = " + mapx + ", MapY = " + mapy + ", Ammo = " + ammo + ", ExplodeTurns = " + explode_turn + ", Primed = " + primed + ", LastUnitToTouch = " + last_unit_to_touch + ", SeenBySide1 = " + seen_by_side[1] + ", SeenBySide2 = " + seen_by_side[2] + ", SeenBySide3 = " + seen_by_side[3] + ", SeenBySide4 = " + seen_by_side[4] + ", PrimedTime = " + primed_time + " WHERE EquipmentID = " + equipid);
		// Only do destroyed if not indestructable and not already destroyed
		if (destroyed == 1) {
			try {
				ResultSet rs = dbs.getResultSet("SELECT EquipmentTypeID FROM Equipment WHERE EquipmentID = " + equipid + " AND COALESCE(Destroyed, 0) = 0");
				if (rs.next()) {
					if (EquipmentTypesTable.IsIndestructable(dbs, rs.getInt("EquipmentTypeID")) == 0) {
						dbs.runSQLUpdate("UPDATE Equipment SET Destroyed = " + destroyed + " WHERE EquipmentID = " + equipid);
					}
				}
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
			}
		}
	}


	public void delete() throws SQLException {
		dbs.runSQLDelete("DELETE FROM Equipment WHERE EquipmentID = " + this.getID());
	}


	public int getAmmoCapacity() throws SQLException {
		return rs.getInt("AmmoCapacity");
	}


	public String getName(boolean detailed) throws SQLException {
		eq_types.selectRow(getEquipmentTypeID());
		String eq_name = eq_types.getName();
		if (detailed) {
			if (eq_types.getMajorType() == EquipmentTypesTable.ET_GUN) {
				eq_name = eq_name + " (" + rs.getInt("Ammo") + ")";
			} else if (eq_types.getMajorType() == EquipmentTypesTable.ET_DEATH_GRENADE) {
				if (isPrimed()) {
					int expl_turns = getExplodeTurns(); 
					eq_name = eq_name + " (P:" + expl_turns + ")";
				}
			}
		}
		return eq_name;
	}


	public int getEquipmentTypeID() throws SQLException {
		return rs.getInt("EquipmentTypeID");
	}


	public int getUnitID() throws SQLException {
		return rs.getInt("UnitID");
	}


	public int getExplodeTurns() throws SQLException {
		return rs.getInt("ExplodeTurns");
	}


	public boolean isPrimed() throws SQLException {
		return rs.getInt("Primed") == 1;
	}


	public int getCost() throws SQLException {
		return rs.getInt("Cost");
	}


	public static int GetValueOfArmourAndEquipment(MySQLConnection dbs, int gameid, int side, boolean inc_armour) throws SQLException {
		String sql = "SELECT SUM(Cost) FROM Equipment ";
		sql = sql + " INNER JOIN EquipmentTypes ON EquipmentTypes.EquipmentTypeID = Equipment.EquipmentTypeID ";
		sql = sql + " INNER JOIN Units ON Units.UnitID = Equipment.UnitID ";
		sql = sql + " WHERE Units.GameID = " + gameid;
		sql = sql + " AND Units.Side = " + side;
		if (inc_armour) {
			sql = sql + " UNION ALL";
			sql = sql + " SELECT SUM(Cost) FROM Units ";
			sql = sql + " LEFT JOIN ArmourTypes ON ArmourTypes.ArmourTypeID = Units.ArmourTypeID ";
			sql = sql + " WHERE Units.GameID = " + gameid;
			sql = sql + " AND Units.Side = " + side;
		}
		ResultSet rs = dbs.getResultSet(sql);
		int tot = 0;
		while (rs.next()) {
			tot += rs.getInt(1);
		}
		return tot;

	}


	public static int GetWeightOfArmourAndEquipment(MySQLConnection dbs, int unitid) throws SQLException {
		String sql = "SELECT SUM(Weight) FROM Equipment ";
		sql = sql + " INNER JOIN EquipmentTypes ON EquipmentTypes.EquipmentTypeID = Equipment.EquipmentTypeID ";
		sql = sql + " INNER JOIN Units ON Units.UnitID = Equipment.UnitID ";
		sql = sql + " WHERE Units.UnitID = " + unitid;
		sql = sql + " UNION ALL";
		sql = sql + " SELECT SUM(Weight) FROM Units ";
		sql = sql + " LEFT JOIN ArmourTypes ON ArmourTypes.ArmourTypeID = Units.ArmourTypeID ";
		sql = sql + " WHERE Units.UnitID = " + unitid;
		ResultSet rs = dbs.getResultSet(sql);
		int tot = 0;
		while (rs.next()) {
			tot += rs.getInt(1);
		}
		return tot;

	}


	public static int GetNumItemsOnUnit(MySQLConnection dbs, int unitid) throws SQLException {
		try {
		String sql = "SELECT Count(*) FROM Equipment ";
		sql = sql + " WHERE UnitID = " + unitid;
		return dbs.getScalarAsInt(sql);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
			return 0;
		}

	}


	public static void AddRandomEquipment(MySQLConnection dbs, int gameid, int x, int y) throws SQLException {
		int type = dbs.getScalarAsInt("SELECT EquipmentTypeID FROM EquipmentTypes WHERE MajorTypeID = " + EquipmentTypesTable.ET_GUN + " ORDER BY Rand() LIMIT 1 ");
		int AMMO = EquipmentTypesTable.GetAmmoCapacityFromEquipmentType(dbs, type);
		int id = EquipmentTable.CreateEquipment(dbs, gameid, -1, type, AMMO);
		int seen_by_side[] = new int[5];
		EquipmentTable.Update(dbs, id, -1, x, y, AMMO, 0, 0, 0, 0, seen_by_side, 0);

	}


	public static void CreateEquipmentFromCorpse(MySQLConnection dbs, ResultSet rs_unit) throws SQLException {
		String code = "";
		if (rs_unit.getInt("ModelType") == UnitsTable.MT_ALIEN_TYRANT || rs_unit.getInt("ModelType") == UnitsTable.MT_QUEEN_ALIEN || rs_unit.getInt("ModelType") == UnitsTable.MT_CRAB || rs_unit.getInt("ModelType") == UnitsTable.MT_GHOUL) {
			code = EquipmentTypesTable.CD_ALIEN_CORPSE; //"ALIENCORPSE";
		} else if (rs_unit.getInt("ModelType") == UnitsTable.MT_BLOB) {
			// Don't create one
		} else if (rs_unit.getInt("ModelType") == UnitsTable.MT_SCIENTIST) {
			code = EquipmentTypesTable.CD_SCIENTIST_CORPSE;// "SCIENTISTCORPSE";
		} else {
			code = EquipmentTypesTable.CD_HUMAN_CORPSE_SIDE + rs_unit.getInt("Side");
		}

		if (code.length() > 0) {
			int equiptypeid = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, code);
			int eid = EquipmentTable.CreateEquipment(dbs, rs_unit.getInt("GameID"), -1, equiptypeid, 0);
			int seen_by_side[] = new int[5];
			seen_by_side[rs_unit.getInt("Side")] = 1;
			EquipmentTable.Update(dbs, eid, 0, rs_unit.getInt("MapX"), rs_unit.getInt("MapY"), 0, 0, 0, 0, 0, seen_by_side, 0);
		}

		dbs.runSQLUpdate("UPDATE Units SET CorpseCreated = 1 WHERE UnitID = " + rs_unit.getInt("UnitID"));
	}

}
