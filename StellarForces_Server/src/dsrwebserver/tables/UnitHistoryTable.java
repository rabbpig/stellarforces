package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsrwebserver.DSRWebServer;

/**
 * This table stores ALL event types FOR PLAYBACK, e.g. grenade explosions, not just unit data.
 *
 */
public class UnitHistoryTable {
	
	public static final int UH_UNIT_MOVEMENT = 1;
	public static final int UH_EXPLOSION = 2;
	public static final int UH_WALL_DESTROYED = 3;
	public static final int UH_UNIT_KILLED = 4;
	public static final int UH_SHOT_FIRED = 5;
	public static final int UH_UNIT_DEPLOYED = 6;
	public static final int UH_UNIT_ESCAPED = 7;
	public static final int UH_COMPUTER_DESTROYED = 10;
	public static final int UH_FLAG_LOCATION = 11;
	
	public static final int UH_UNIT_WOUNDED = 17;
	public static final int UH_ITEM_DROPPED = 18;
	public static final int UH_ITEM_THROWN = 19;
	public static final int UH_ITEM_PICKED_UP = 20;
	public static final int UH_CLOSE_COMBAT = 21;
	public static final int UH_DOOR_OPENED = 24;
	public static final int UH_DOOR_CLOSED = 25;
	public static final int UH_GRENADE_PRIMED = 26;
	public static final int UH_BLOB_SPLIT = 27;
	public static final int UH_SMOKE_CREATED = 28;
	public static final int UH_SMOKE_REMOVED = 29;
	public static final int UH_NERVE_GAS_CREATED = 30;
	public static final int UH_NERVE_GAS_REMOVED = 31;
	//public static final int UH_UNIT_TELEPORTED = 32;
	public static final int UH_MAPSQUARE_CHANGED = 33;
	public static final int UH_FIRE_CREATED = 34;
	public static final int UH_FIRE_REMOVED = 35;
	// If you add any here, add them to the UnitHistoryTable in Android and AppletPlayback
	
	
	private UnitHistoryTable() {
		
	}

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("UnitHistory") == false) {
			dbs.runSQL("CREATE TABLE UnitHistory (UnitHistoryID INTEGER AUTO_INCREMENT KEY, GameID INTEGER, TurnNo INTEGER, TurnSide TINYINT, EndOfTurn TINYINT, UnitID INTEGER, Status TINYINT, MapX SMALLINT, MapY SMALLINT, Health SMALLINT, Angle SMALLINT, EventType SMALLINT, Radius SMALLINT, OriginalSquareType TINYINT, OriginalDoorType TINYINT, EventTime BIGINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
		for (int s=1 ; s<=4 ; s++) {
			if (dbs.doesColumnExist("UnitHistory", "SeenBySide"+s) == false) {
				dbs.runSQL("ALTER TABLE UnitHistory ADD SeenBySide" + s + " TINYINT");
			}
		}
		if (dbs.doesIndexExist("UnitHistory", "idx_UnitHistory_GameID") == false) {
			dbs.runSQL("CREATE INDEX idx_UnitHistory_GameID ON UnitHistory (GameID)");
		}
	}
	
	
	public static void AddRecord_UnitMovement(MySQLConnection dbs, GamesTable game, UnitsTable unit, int seen_by_side[], long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, Status, MapX, MapY, Angle, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_UNIT_MOVEMENT + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + unit.getID() + ", " + unit.getStatus() + ", " + unit.getMapX() + ", " + unit.getMapY() + ", " + unit.getAngle() + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ", " + event_time + ")");
	}
	
	
	public static void AddRecord_BlobSplit(MySQLConnection dbs, GamesTable game, UnitsTable unit, int seen_by_side[], long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, Status, MapX, MapY, Angle, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_BLOB_SPLIT + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + unit.getID() + ", " + unit.getStatus() + ", " + unit.getMapX() + ", " + unit.getMapY() + ", " + unit.getAngle() + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ", " + event_time + ")");
	}
	
	
	/*public static void AddRecord_HeardUnitMovement(MySQLConnection dbs, GamesTable game, UnitsTable unit, int seen_by_side[]) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, Status, MapX, MapY, Health, Angle, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4) VALUES (" + UH_HEARD_UNIT_MOVE + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + unit.getID() + ", " + unit.getStatus() + ", " + unit.getMapX() + ", " + unit.getMapY() + ", " + unit.getHealth() + ", " + unit.getAngle() + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ")");
	}*/
	
	
	public static void AddRecord_FlagLocation(MySQLConnection dbs, GamesTable game, int x, int y, long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, MapX, MapY, EndOfTurn, EventTime) VALUES (" + UH_FLAG_LOCATION + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + x + ", " + y + ", 1, " + event_time + ")");
	}
	
	
	public static void AddRecord_ShotFired(MySQLConnection dbs, GamesTable game, UnitsTable shooter, int angle, int length, int seen_by_side[], long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, MapX, MapY, Angle, Radius, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_SHOT_FIRED + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + shooter.getID() + ", " + shooter.getMapX() + ", " + shooter.getMapY() + ", " + angle + ", " + length + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ", " + event_time + ")");
	}
	

	/*public static void AddRecord_HeardShotFired(MySQLConnection dbs, GamesTable game, UnitsTable shooter, int angle, int length, int seen_by_side[]) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, MapX, MapY, Angle, Radius, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4) VALUES (" + UH_HEARD_SHOT_FIRED + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + shooter.getID() + ", " + shooter.getMapX() + ", " + shooter.getMapY() + ", " + angle + ", " + length + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ")");
	}*/
	

	public static void AddRecord_UnitDeployed(MySQLConnection dbs, GamesTable game, UnitsTable unit, int seen_by_side[], long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, MapX, MapY, Angle, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_UNIT_DEPLOYED + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + unit.getID() + ", " + unit.getMapX() + ", " + unit.getMapY() + ", " + unit.getAngle() + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ", " + event_time + ")");
	}
	
	
	public static void AddRecord_UnitKilled(MySQLConnection dbs, GamesTable game, UnitsTable unit_killed, int seen_by_side[], long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, MapX, MapY, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_UNIT_KILLED + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + unit_killed.getID() + ", " + unit_killed.getMapX() + ", " + unit_killed.getMapY() + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ", " + event_time + ")");
	}
	
	
	public static void AddRecord_DoorOpened(MySQLConnection dbs, GamesTable game, int x, int y, long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, MapX, MapY, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_DOOR_OPENED + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + x + ", " + y + ", 1, 1, 1, 1, " + event_time + ")");
	}
	
	
	public static void AddRecord_DoorClosed(MySQLConnection dbs, GamesTable game, int x, int y, long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, MapX, MapY, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_DOOR_CLOSED + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + x + ", " + y + ", 1, 1, 1, 1, " + event_time + ")");
	}
	
	
	public static void AddRecord_UnitEscaped(MySQLConnection dbs, GamesTable game, UnitsTable unit, int seen_by_side[], long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, MapX, MapY, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_UNIT_ESCAPED + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + unit.getID() + ", " + unit.getMapX() + ", " + unit.getMapY() + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ", " + event_time + ")");
	}
	
	
	public static void AddRecord_GrenadeExplosion(MySQLConnection dbs, GamesTable game, int mapx, int mapy, int rad, int eq_type, long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, MapX, MapY, Radius, OriginalSquareType, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_EXPLOSION + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + mapx + ", " + mapy + ", " + rad + ", " + eq_type + ", 1, 1, 1, 1, " + event_time + ")");
	}
	
	
	public static void AddRecord_MapSquareDestroyed(MySQLConnection dbs, GamesTable game, int mapx, int mapy, int orig_sq_type, int orig_door_type, long event_time) throws SQLException {
		try {
			dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, MapX, MapY, OriginalSquareType, OriginalDoorType, EventTime) VALUES (" + UH_WALL_DESTROYED + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + mapx + ", " + mapy + ", " + orig_sq_type + ", " + orig_door_type + ", " + event_time + ")");
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}
	}
	
	
	public static void AddRecord_ComputerDestroyed(MySQLConnection dbs, GamesTable game, int mapx, int mapy, long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, MapX, MapY, EventTime) VALUES (" + UH_COMPUTER_DESTROYED + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + mapx + ", " + mapy + ", " + event_time + ")");
	}
	
	
	public static void AddRecord_Smoke(MySQLConnection dbs, GamesTable game, int mapx, int mapy, long event_time, boolean added) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, MapX, MapY, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + (added?UH_SMOKE_CREATED:UH_SMOKE_REMOVED) + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + mapx + ", " + mapy + ", 1, 1, 1, 1, " + event_time + ")");
	}
	
	
	public static void AddRecord_NerveGas(MySQLConnection dbs, GamesTable game, int mapx, int mapy, long event_time, boolean added) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, MapX, MapY, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + (added?UH_NERVE_GAS_CREATED:UH_NERVE_GAS_REMOVED) + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + mapx + ", " + mapy + ", 1, 1, 1, 1, " + event_time + ")");
	}
	
	
	public static void AddRecord_Fire(MySQLConnection dbs, GamesTable game, int mapx, int mapy, long event_time, boolean added) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, MapX, MapY, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + (added?UH_FIRE_CREATED:UH_FIRE_REMOVED) + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + mapx + ", " + mapy + ", 1, 1, 1, 1, " + event_time + ")");
	}
	
	
	public static void AddRecord_UnitWounded(MySQLConnection dbs, GamesTable game, UnitsTable unit_killed, int seen_by_side[], long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, MapX, MapY, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_UNIT_WOUNDED + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + unit_killed.getID() + ", " + unit_killed.getMapX() + ", " + unit_killed.getMapY() + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ", " + event_time + ")");
	}
	
	
	public static void AddRecord_ItemDropped(MySQLConnection dbs, GamesTable game, UnitsTable unit, int seen_by_side[], long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, MapX, MapY, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_ITEM_DROPPED + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + unit.getID() + ", " + unit.getMapX() + ", " + unit.getMapY() + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ", " + event_time + ")");
	}
	
	
	public static void AddRecord_GrenadePrimed(MySQLConnection dbs, GamesTable game, UnitsTable unit, int turns, int seen_by_side[], long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, MapX, MapY, Radius, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_GRENADE_PRIMED + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + unit.getID() + ", " + unit.getMapX() + ", " + unit.getMapY() + ", " + turns + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ", " + event_time + ")");
	}
	
	
	/*public static void AddRecord_UnitTeleported(MySQLConnection dbs, GamesTable game, UnitsTable unit, int seen_by_side[], long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, MapX, MapY, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_UNIT_TELEPORTED + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + unit.getID() + ", " + unit.getMapX() + ", " + unit.getMapY() + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ", " + event_time + ")");
	}*/
	
	
	public static void AddRecord_MapsquareChanged(MySQLConnection dbs, GamesTable game, int mapx, int mapy, int old_type, int new_type, long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, MapX, MapY, Radius, OriginalSquareType, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_MAPSQUARE_CHANGED + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + mapx + ", " + mapy + ", " + new_type + ", " + old_type + ", 1, 1, 1, 1, " + event_time + ")");
	}
	
	
	public static void AddRecord_ItemThrown(MySQLConnection dbs, GamesTable game, UnitsTable unit, int ang, int dist, int seen_by_side[], long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, MapX, MapY, Angle, Radius, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_ITEM_THROWN + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + unit.getID() + ", " + unit.getMapX() + ", " + unit.getMapY() + ", " + ang + ", " + dist + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ", " + event_time + ")");
	}
	
	
	public static void AddRecord_ItemPickedUp(MySQLConnection dbs, GamesTable game, UnitsTable unit, int seen_by_side[], long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, MapX, MapY, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_ITEM_PICKED_UP + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + unit.getID() + ", " + unit.getMapX() + ", " + unit.getMapY() + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ", " + event_time + ")");
	}
	
	
	public static void AddRecord_CloseCombat(MySQLConnection dbs, GamesTable game, UnitsTable unit, int seen_by_side[], long event_time) throws SQLException {
		dbs.RunIdentityInsert("INSERT INTO UnitHistory (EventType, GameID, TurnNo, TurnSide, UnitID, MapX, MapY, SeenBySide1, SeenBySide2, SeenBySide3, SeenBySide4, EventTime) VALUES (" + UH_CLOSE_COMBAT + ", " + game.getID() + ", " + game.getTurnNo() + ", " + game.getTurnSide() + "," + unit.getID() + ", " + unit.getMapX() + ", " + unit.getMapY() + ", " + seen_by_side[1] + ", " + seen_by_side[2] + ", " + seen_by_side[3] + ", " + seen_by_side[4] + ", " + event_time + ")");
	}
	
	
	public static int GetMaxTurnNo(MySQLConnection dbs, int gid) throws SQLException {
		return dbs.getScalarAsInt("SELECT Max(TurnNo) FROM UnitHistory WHERE GameID = " + gid); //EndOfTurn = 1 AND 
	}

}
