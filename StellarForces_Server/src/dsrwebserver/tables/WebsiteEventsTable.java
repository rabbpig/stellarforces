package dsrwebserver.tables;

import java.sql.SQLException;

import dsrwebserver.DSRWebServer;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;

public class WebsiteEventsTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("WebsiteEvents") == false) {
			dbs.runSQL("CREATE TABLE WebsiteEvents (WebsiteEventID INTEGER AUTO_INCREMENT KEY, Entry VARCHAR(1024), LoginID INTEGER, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}

		if (dbs.doesIndexExist("WebsiteEvents", "idx_WebsiteEvents_DateCreated") == false) {
			dbs.runSQL("CREATE INDEX idx_WebsiteEvents_DateCreated ON WebsiteEvents (DateCreated)");
		}
	}


	public static void AddRec(MySQLConnection dbs, String entry, int loginid) throws SQLException {
		try {
			dbs.RunIdentityInsert("INSERT INTO WebsiteEvents (Entry, LoginID) VALUES (" + SQLFuncs.s2sql(entry) + ", " + loginid + ")");
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}

	}

}
