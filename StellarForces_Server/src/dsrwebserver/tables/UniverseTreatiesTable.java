package dsrwebserver.tables;

import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;

/**
 * I think we need treaties so players can deploy onto planets adjacent to either their own planets or those they have a treatie with.
 * It will also help with opp fire!
 *
 */
public class UniverseTreatiesTable {

	public static void CreateTable(MySQLConnection dbs) throws SQLException {
		if (dbs.doesTableExist("UniverseTreaties") == false) {
			dbs.runSQL("CREATE TABLE UniverseTreaties (UniverseTreatyID INTEGER AUTO_INCREMENT KEY, Login1ID INTEGER, Login2ID INTEGER, Accepted TINYINT, DateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP )");
		}
	}
	
	
	public static boolean AreSidesInTreaty(MySQLConnection dbs, int login1id, int login2id) throws SQLException  {
		String sql = "SELECT COUNT(*) FROM UniverseTreaties WHERE Accepted = 1 AND (Login1ID = " + login1id + " AND Login2ID = " + login2id + ") OR (Login1ID = " + login2id + " AND Login2ID = " + login1id + ")";
		return dbs.getScalarAsInt(sql) > 0;
	}
	

	public static int OfferTreaty(MySQLConnection dbs, int login1id, int login2id) throws SQLException  {
		return dbs.RunIdentityInsert_Syncd("INSERT INTO UniverseTreaties (Login1ID, Login2ID) VALUES (" + login1id + ", " + login2id + ")");
	}


	public static void AcceptTreaty(MySQLConnection dbs, int id) throws SQLException  {
		dbs.runSQLUpdate("UPDATE UniverseTreaties SET Accepted = 1 WHERE UniverseTreatyID = " + id);
	}


	public static void DeclineTreaty(MySQLConnection dbs, int id) throws SQLException  {
		dbs.runSQLUpdate("DELETE FROM UniverseTreaties WHERE UniverseTreatyID = " + id);
	}


	public static void CancelTreaty(MySQLConnection dbs, int id) throws SQLException  {
		dbs.runSQLUpdate("DELETE FROM UniverseTreaties WHERE UniverseTreatyID = " + id);
	}


}
