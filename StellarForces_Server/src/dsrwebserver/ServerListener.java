package dsrwebserver;

import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;

import ssmith.net.NetworkMultiServer2;
import ssmith.net.NetworkMultiServerConn2Text;

public class ServerListener extends NetworkMultiServer2 {
	
	public ServerListener(int port, int max_conns) throws IOException {
		super(port, max_conns);
	}

	
	public NetworkMultiServerConn2Text createConnection(Socket sck) throws IOException {
		//System.out.println("NEW CONN!");
		try {
			return new ClientConnection(this, sck);
		} catch (SQLException e) {
			DSRWebServer.HandleError(e, true);
			return null;
		}
	}


}
