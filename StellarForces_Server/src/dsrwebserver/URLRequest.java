package dsrwebserver;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import ssmith.util.KeyValuePair;

public final class URLRequest extends HashMap<String, StringBuffer> {

	private static final long serialVersionUID = 1L;
	
	private String main_url;
	private String full_url;
	private String first_line;
	private String method;
	private String query;

	public URLRequest(String line1) throws UnsupportedEncodingException {
		first_line = line1;
		int pos = first_line.indexOf(" ");
		if (pos < 0) {
			throw new RuntimeException("Invalid request: " + first_line);
		}
		method = first_line.substring(0, pos);
		
		if (method.equalsIgnoreCase("CONNECT")) {
			throw new ClientException("Connect method not implemented.");
		}
		full_url = first_line.substring(pos+1);
		pos = full_url.lastIndexOf(" ");
		full_url = full_url.substring(0, pos);

		if (full_url.indexOf("?") >= 0) {
			String csv2[] = full_url.split("\\?");
			main_url = csv2[0];
			if (csv2.length > 1) {
				query = csv2[1];
				this.putAll(DecodeParams(query));
			}
		} else {
			main_url = full_url;
		}

		if (main_url.toLowerCase().startsWith("http://")) { // Have we been passed the full URL?
			int slash_pos = main_url.substring(7, main_url.length()).indexOf("/");
			main_url = main_url.substring(slash_pos + 7, main_url.length());
		}
	}

	
	public String getQueryString() {
		return query;
	}
	
	public String getMethod() {
		return this.method;
	}

	
	public static HashMap<String, StringBuffer> DecodeParams(String data) throws UnsupportedEncodingException {
		return DecodeParams(new StringBuffer(data));
	}
	
	
	public static HashMap<String, StringBuffer> DecodeParams(StringBuffer data) throws UnsupportedEncodingException {
		HashMap<String, StringBuffer> hm = new HashMap<String, StringBuffer>();

		StringBuffer str_key = new StringBuffer();
		StringBuffer str_val = new StringBuffer();
		boolean getting_key = true;  // Start getting the key
		int pos = 0;
		while (pos < data.length()) {
			char x = data.charAt(pos);
			if (getting_key) {
				if (x == '=') {
					getting_key = false; // Now getting the value
				} else {
					str_key.append(x);
				}
			} else { // Getting the value
				if (x == '&') {
					addValue(hm, str_key, str_val);
					getting_key = true;
				} else {
					str_val.append(x);
				}
			}
			pos = pos + 1;
		}

		//KeyValuePair kvp = new KeyValuePair(str_key.toString(), str_val.toString());
		//hm.put(HTTPHeaders.DecodeString(kvp.key.toLowerCase()), HTTPHeaders.DecodeString(kvp.value));
		addValue(hm, str_key, str_val);
		return hm;
	}
	
	
	private static void addValue(HashMap<String, StringBuffer> hm, StringBuffer str_key, StringBuffer str_val) throws UnsupportedEncodingException {
		KeyValuePair kvp = new KeyValuePair(HTTPHeaders.URLDecodeString(str_key.toString()).toLowerCase(), HTTPHeaders.URLDecodeString(str_val.toString()));
		// If it already exists, append it
		if (hm.containsKey(kvp.key)) {
			//kvp.value = hm.get(kvp.key) + "," + kvp.value; 
			hm.get(kvp.key).append("," + kvp.value); 
		} else {
			hm.put(kvp.key, kvp.value);
		}
		str_key.delete(0, str_key.length());
		str_val.delete(0, str_val.length());

	}

	public boolean isPostRequest() {
		return first_line.toLowerCase().startsWith("post");
	}

	public boolean isHeadRequest() {
		return first_line.toLowerCase().startsWith("head");
	}

	public boolean isOptionsRequest() {
		return first_line.toLowerCase().startsWith("options");
	}

	public String getMainPartOfURL() {
		return this.main_url;
	}

	public String getFullURL() {
		return this.full_url;
	}

	public String getGetValue(String key) {
		return super.get(key).toString();
	}

}
