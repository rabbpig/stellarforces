package ssmith.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class DataReader {
	
	public InputStream is;
	private byte last_char;
	private boolean last_char_set = false;
	
	public DataReader(InputStream _is) {
		is = _is;
	}
	
	public int available() throws IOException {
		return is.available();
	}
	
	public String readLine() throws IOException {
		StringBuffer str = new StringBuffer();
		if (last_char_set) {
			str.append(last_char);
			last_char_set = false;
		}
		
		while (is.available() > 0) { // Was ">="
			char c = (char)is.read();
			if (c == '\r' || c == '\n') {
				last_char = (byte)is.read();
				if ((char)last_char == '\r' || (char)last_char == '\n') {
					last_char_set = false;
				} else {
					last_char_set = true;
				}
				break;
			}
			//TSMainPage.p("Len: " + str.length());
			str.append(c);
		}
		return str.toString();
	}

	public byte[] readData() throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		if (last_char_set) {
			bos.write(this.last_char);
			last_char_set = false;
		}

		byte b[] = new byte[4096];
		while (true) {
			int len = is.read(b);
			if (len < 0) {
				break;
			}
			bos.write(b, 0, len);
		}
		return bos.toByteArray();
	}
	
	
	public String readChars(int len) throws IOException {
		StringBuffer str = new StringBuffer();
		if (last_char_set) {
			str.append(last_char);
			last_char_set = false;
		} /*else if (is.available() <= 0) {
			return null;
		}*/
		
		while (available() >= 0 && str.length() < len) {
			char c = (char)is.read();
			str.append(c);
		}
		return str.toString();
		
	}

}
