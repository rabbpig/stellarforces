/**
 *
 * @author  steve smith
 * @version
 */

package ssmith.net;

import java.net.*;
import java.io.*;

/**
 * Extend this class for the clients.
 */
public abstract class NetworkMultiServerConn2Text extends Thread {

	public Socket sck;
	protected PrintWriter pw;
	protected DataOutputStream dos;
	protected DataInputStream dis;
	protected BufferedReader bis;
	protected NetworkMultiServer2 server;
	public long started_time;

	
	public NetworkMultiServerConn2Text(NetworkMultiServer2 svr, Socket sck) throws IOException {
		super("Client connection");
		
		this.setDaemon(false); //No, as we don't want to stop it abruptly
		this.sck = sck;
		this.server = svr;
		
		bis = new BufferedReader(new InputStreamReader(sck.getInputStream()));
		pw = new PrintWriter(sck.getOutputStream(), true);
		dos = new DataOutputStream(sck.getOutputStream());
		dis = new DataInputStream(sck.getInputStream());
		this.started_time = System.currentTimeMillis();
	}

	
	public void close() {
		try {
			dis.close();
			dos.close();
			sck.close();
		} catch (IOException e) {
			// Nothing
		}
		server.removeConnection(this);
	}

	
	public BufferedReader getBufferedReader() {
		return bis;
	}
	

	public PrintWriter getPrintWriter() {
		return pw;
	}

	
	public DataOutputStream getDataOutputStream() {
		return dos;
	}

	
	public DataInputStream getDataInputStream() {
		return dis;
	}

	public InetAddress getINetAddress() {
		return sck.getInetAddress();
	}

}
