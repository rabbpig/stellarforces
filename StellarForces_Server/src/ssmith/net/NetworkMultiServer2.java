package ssmith.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import dsrwebserver.DSRWebServer;

public abstract class NetworkMultiServer2 extends Thread {

    private int max_connections;
    private ServerSocket sckListener;
    public boolean debug = false;
    private static volatile boolean stopNow = false;
    private volatile int num_conns = 0;
    public volatile int highest_conns = 0;

    public NetworkMultiServer2(int port, int max_conns) throws IOException {
	    super("NetworkMultiServer2");
	    
        this.max_connections = max_conns;
        this.setDaemon(false);
        sckListener = new ServerSocket(port, max_connections);
    }

    
    public void run() {
        try {
            while (!stopNow) {
                Socket s = sckListener.accept();
                createConnectionPre(s);
            }
            System.out.println("NetworkMultiServer2 stopped.");
        } catch (Exception e) {
        	DSRWebServer.HandleError(e);
        }
    }

    /**
     * Override this method when a connection is made
     */
    public void createConnectionPre(Socket sck) throws IOException {
        NetworkMultiServerConn2Text c = createConnection(sck);
        if (c != null) {
        	num_conns++;
        	if (num_conns > highest_conns) {
        		highest_conns = num_conns;
        	}
        }
        System.out.println("Connection made from " + sck.getInetAddress().toString() + ".  There are now " + num_conns + " users connected.");
    }

    public abstract NetworkMultiServerConn2Text createConnection(Socket sck) throws IOException;


    public void removeConnection(NetworkMultiServerConn2Text conn) {
    	num_conns--;
    	if (num_conns < 0) {
    		num_conns = 0;
    	}
        System.out.println("Connection removed.  There are now " + num_conns + " users connected.");
    }

    public int getNoOfConnections() {
        return num_conns;
    }

    public static void StopListening() {
        stopNow = true;
    }

}
