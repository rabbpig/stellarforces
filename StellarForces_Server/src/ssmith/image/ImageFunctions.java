package ssmith.image;

import java.awt.Color;

import ssmith.lang.Functions;

public class ImageFunctions {

	public static String GetRGB(Color c) {
		return Functions.Prezero(Integer.toHexString(c.getRed()), 2) + Functions.Prezero(Integer.toHexString(c.getGreen()), 2) + Functions.Prezero(Integer.toHexString(c.getBlue()), 2);
	}


}
