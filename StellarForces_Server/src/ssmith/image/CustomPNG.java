package ssmith.image;

import java.awt.Graphics;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;


public class CustomPNG {

	private BufferedImage buf;
	private Graphics g;
	
	public CustomPNG(int w, int h) {
		buf = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		g = buf.getGraphics();
	}
	
	public Graphics getGraphics() {
		return g;
	}
	
	public byte[] generateDataAsPNG() throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream( 1000 );

		// W R I T E
		ImageIO.write( buf, "png", baos );

		// C L O S E
		baos.flush();
		byte[] resultImageAsRawBytes = baos.toByteArray();

		baos.close();
		
		return resultImageAsRawBytes;
	}

}
