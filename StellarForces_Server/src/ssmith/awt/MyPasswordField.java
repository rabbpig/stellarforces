package ssmith.awt;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JPasswordField;

public class MyPasswordField extends JPasswordField implements FocusListener {
	
	private static final long serialVersionUID = 1L;

	public MyPasswordField() {
		super();
		
		this.addFocusListener(this);
	}

	//@Override
	public void focusGained(FocusEvent arg0) {
		this.setSelectionStart(0);
		this.setSelectionEnd(this.getText().length());
	}

	//@Override
	public void focusLost(FocusEvent arg0) {
		
	}

}
