package ssmith.awt;

import java.awt.Font;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class FontLoader {

	// Prepare a static "cache" mapping font names to Font objects.
	private static Map<String, Font> cache = new ConcurrentHashMap<String, Font>();//names.length);
	
	public static Font GetFont(String name) {
		Font font = null;
		if (cache != null) {
			if ((font = cache.get(name)) != null) {
				return font;
			}
		}
		//String fName = "/fonts/" + name;
		try {
			InputStream is = new FileInputStream(name);
			//InputStream is = FontLoader.class.getResourceAsStream(fName);
			font = Font.createFont(Font.TRUETYPE_FONT, is);
			is.close();
			cache.put(name, font);
		} catch (Exception ex) {
			ex.printStackTrace();
			System.err.println(name + " not loaded.  Using serif font.");
			font = new Font("serif", Font.PLAIN, 24);
		}
		return font;
	}

}
