package ssmith.applet;

import java.applet.Applet;
import java.awt.*;
import java.util.*;

public class AppletImageCache {

	private Hashtable<String, Image> hashImages = new Hashtable<String, Image>();
	private MediaTracker mt;
	protected Applet app;

	public AppletImageCache(Applet c) {
		app = c;
		mt = new MediaTracker(c);
	}
	
	
	public Image loadImage(String filename) {
		Image img = app.getImage(app.getCodeBase(), filename);
		mt.addImage(img, 1);
		try {
			mt.waitForID(1);
		}
		catch (InterruptedException e) {
			System.err.println("Error loading images: " + e.getMessage());
		}
		mt.removeImage(img);
		return img;
	}

	public void putImage(Image img, String filename) {
		hashImages.put(filename, img);
	}

	public Image getImage(String filename) {
		Image img = (Image) hashImages.get(filename);
		if (img == null) {
			img = loadImage(filename);
			this.putImage(img, filename);
		}
		return img;
	}

}

