package ssmith.dbs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SQLFuncs {

	private static SimpleDateFormat sql_date_format_datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static SimpleDateFormat sql_date_format_dateonly = new SimpleDateFormat("yyyy-MM-dd");

	public static String s2sql_old(String s) {
		return "'" + s.replace("'", "''") + "'";
	}

	
	public static String s2sql(String s) {
		return s2sql_old(s.replace("\\", "\\\\").replace("&", "\\&"));
	}

	
	public static String d2sql(Date date, boolean show_time) {
		if (show_time) {
			return ("'" + sql_date_format_datetime.format(date) + "'").replace(" ", "T");
		} else {
			return "'" + sql_date_format_dateonly.format(date) + "'";
		}
	}

	
	public static String d2sql(Calendar cal, boolean show_time) {
		Date date = cal.getTime();
		if (show_time) {
			return ("'" + sql_date_format_datetime.format(date) + "'").replace(" ", "T");
		} else {
			//DateFormat df = DateFormat.getInstance();
			return "'" + sql_date_format_dateonly.format(date) + "'";
		}
	}

	
	public static String bool2yn(boolean b) {
		return b ? "'Y'" : "'N'";
	}

	
	public static int b201(boolean b) {
		return b ? 1 : 0;
	}

}
