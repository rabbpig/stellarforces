package ssmith.dbs;

import java.util.Date;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class AbstractDatabase {

	protected Connection conn;

	public abstract void connect(String server, String database, String login, String pwd) throws ClassNotFoundException, SQLException;

	public abstract boolean doesTableExist(String name) throws SQLException;

	public abstract boolean doesColumnExist(String table, String col) throws SQLException;

	public ResultSet getResultSet(String sql) throws SQLException {
		Statement stmt = conn.createStatement();

		// Execute the query
		try {
			ResultSet rs = stmt.executeQuery(sql);

			//stmt.close() ;
			return rs;
		} catch (SQLException e) {
			throw new SQLException("Error performing " + sql + ": " + e);
		}
	}

	
	public int RunIdentityInsert(String sql) throws SQLException {
		Statement stmt = conn.createStatement();
		stmt.execute(sql);
		return getScalarAsInt("SELECT @@IDENTITY AS NewID");
	}

	
	public void runSQL(String sql) throws SQLException {
		Statement stmt = conn.createStatement();
		stmt.execute(sql);
	}

	
	public int runSQLUpdate(String sql) throws SQLException {
		Statement stmt = conn.createStatement();
		return stmt.executeUpdate(sql);
	}

	
	public int runSQLDelete(String sql) throws SQLException {
		Statement stmt = conn.createStatement();
		return stmt.executeUpdate(sql);
	}

	
	public int getScalarAsInt(String sql) throws SQLException {
		ResultSet rs = this.getResultSet(sql);
		rs.next();
		int val = rs.getInt(1);
		rs.close();
		return val;
	}

	
	public Date getScalarAsDate(String sql) throws SQLException {
		ResultSet rs = this.getResultSet(sql);
		rs.next();
		Date d = rs.getTimestamp(1);
		rs.close();
		return d;
	}
	

	public String GetScalarAsString(String sql) throws SQLException {
		ResultSet rs = this.getResultSet(sql);
		rs.next();
		String val = rs.getString(1);
		rs.close();
		return val;
	}

	
	public void close() throws SQLException {
		//try {
			conn.close() ;
		//} catch (SQLException e) {
		//	e.printStackTrace();
		//}

	}

}
