package ssmith.util;

import java.util.ArrayList;

public class LeagueTable<E> {

	//private int max_items;
	private ArrayList<LTItem<E>> list;

	public LeagueTable() {
		super();
		
		list = new ArrayList<LTItem<E>>();
	}
	
	
	public void addItem(E o, int pts) {
		int pos = 0;
		for (pos = 0 ; pos < list.size() ; pos++) {
			LTItem<E> item = list.get(pos);
			if (pts > item.points) {
				list.add(pos, new LTItem<E>(o, pts));
				return;
			}
		}
		list.add(new LTItem<E>(o, pts));
	}
	
	
	public E getObjAtPos(int pos) {
		return list.get(pos).item;
	}


	public int getPointsAtPos(int pos) {
		return list.get(pos).points;
	}


	private class LTItem<E> {
		
		public LTItem(E o, int pts) {
			super();
			
			item = o;
			points = pts;
		}
		
		public E item;
		public int points;

	}


}
