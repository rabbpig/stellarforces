package ssmith.util;

public class MemCacheArray { // todo - add a time limit like in MemCache
	
	public Object data[];
	
	
	public MemCacheArray(int init_size) {
		data = new Object[init_size+1];
	}
	
	
	public Object get(int i) {
		return data[i];
	}
	
	
	public boolean containsKey(int i) {
		if (i < data.length) {
			return data[i] != null;
		} else {
			return false;
		}
	}
	
	
	public void put(int i, Object d) {
		if (i >= data.length) {
			Object data_new[] = new Object[i*2];
			System.arraycopy(data, 0, data_new, 0, data.length);
			data = data_new;
		}
		data[i] = d;
	}
	
	
}
