package ssmith.util;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import ssmith.io.TextFile;
import ssmith.lang.Functions;
import dsrwebserver.DSRWebServer;

public class DiskCache {

	private String path;

	public DiskCache(String _path) {
		super();

		path = _path;

	}


	public boolean contains(String url) {
		String full = Functions.AppendSlashToEnd(path) + url;
		return new File(full).canRead();
		
	}
	
	
	public String get(String url) {
		String full = Functions.AppendSlashToEnd(path) + url;
		try {
			if (new File(full).canRead()) {
				return TextFile.ReadAll(full, "", true);
			}
		} catch (IOException ex) {
			new File(full).delete();
			DSRWebServer.HandleError(ex);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
		return null;
	}


	public void put(String url, String html) {
		String full = Functions.AppendSlashToEnd(path) + url;
		try {
			TextFile.QuickWrite(full, html, true);
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}
	}
	
	
	public String convertPath(String path) {
		try {
			return URLEncoder.encode(path, "UTF-8");
		} catch (UnsupportedEncodingException ex) {
			DSRWebServer.HandleError(ex);
		}
		return path;
	}

}
