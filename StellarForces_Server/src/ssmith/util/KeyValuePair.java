package ssmith.util;

public class KeyValuePair {
	
	public String key;
	public StringBuffer value;
	
	public KeyValuePair(String k, String v) {
		this(k, new StringBuffer(v));
	}

	public KeyValuePair(String k, StringBuffer v) {
		key = k;
		value = v;
	}

}
