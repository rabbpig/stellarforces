package ssmith.util;

public class StringParser {
	
	private String str;
	private int pos;
	
	public StringParser(String s) {
		str = s;
	}
	
	public String runTo(String s) {
		int start = pos;
		while (pos < str.length()) {
			if (str.substring(pos, pos+1).equalsIgnoreCase(s)) {
				break;
			}
			pos++;
		}
		pos += s.length(); // Skip the seperating chars
		return str.substring(start, pos-1).trim();
	}

	public boolean hasMore() {
		return pos < str.length();
	}
}
