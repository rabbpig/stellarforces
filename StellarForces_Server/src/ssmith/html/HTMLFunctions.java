package ssmith.html;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HTMLFunctions {

	public static final String CR = "\r\n";

	public HTMLFunctions() {
	}

	public static String HTMLEncode(String s) {
		s = s.replace("&", "&amp;");
		s = s.replace("<", "&lt;");
		s = s.replace(">", "&gt;");
		s = s.replace("'", "&#39;");
		s = s.replace("\"", "&quot;");
		//s = s.replaceAll("[^\\x20-\\x7e]", ""); // Clean out unwanted chars
		s = s.replaceAll("[^\\x0a-\\x7e]", ""); // Clean out unwanted chars
		return s;
	}

	public static String s2HTML(String s) {
		if (s == null) {
			return "";
		} else {
			return HTMLEncode(s);
		}
	}

	public static String s2js(String s) {
		s = s.replace("'", "\\'");
		return s;
	}

	public static StringBuffer Heading(StringBuffer str, int size, String text) {
		return str.append("<h" + size + ">" + text + "</h" + size + ">" + CR);
	}

	public static StringBuffer Para(StringBuffer str, String text) {
		if (str == null) {
			str = new StringBuffer();
		}
		return str.append("<p>" + text + "</p>" + CR);
	}

	public static StringBuffer Para(StringBuffer str, String cls, String text) {
		if (str == null) {
			str = new StringBuffer();
		}
		str.append("<p");
		if (cls.length() > 0) {
			str.append(" class=\"" + cls + "\"");
		}
		str.append(">" + text + "</p>" + CR);
		return str;
	}

	public static StringBuffer Line(StringBuffer str, String text) {
		return str.append(text + "<br />" + CR);
	}

	public static StringBuffer StartTable(StringBuffer str, String cls, String width, int border, String align, int cellpadding) {
		str.append("<table");
		if (cls.length() > 0) {
			str.append(" class=\"" + cls + "\"");
		}
		if (width.length() > 0) {
			str.append(" width=\"" + width + "\"");
		}
		if (border > 0) {
			str.append(" border=\"" + border + "\"");
		}
		if (align.length() > 0) {
			str.append(" align=\"" + align + "\"");
		}
		if (cellpadding > 0) {
			str.append(" cellpadding=\"" + cellpadding + "\"");
		}
		str.append(">");
		return str;
	}

	public static StringBuffer StartTable(StringBuffer str) {
		return str.append("<table>");
	}

	public static StringBuffer StartTable(StringBuffer str, int padding) {
		return str.append("<table cellpadding=\"" + padding + "\">");
	}

	public static StringBuffer EndTable(StringBuffer str) {
		return str.append("</table>");
	}

	public static StringBuffer StartDiv(StringBuffer str, String id) {
		return str.append("<div id=\"" + id + "\">");
	}

	public static StringBuffer EndDiv(StringBuffer str) {
		return str.append("</div>");
	}

	public static StringBuffer StartRow(StringBuffer str) {
		return str.append("<tr>");
	}

	public static StringBuffer EndRow(StringBuffer str) {
		return str.append("</tr>" + CR);
	}

	public static StringBuffer StartCell(StringBuffer str) {
		return StartCell(str, "top", "", 1, "");
	}

	
	public static StringBuffer StartCell(StringBuffer str, int colspan) {
		return StartCell(str, "", "", colspan, "");
	}


	public static StringBuffer StartCell(StringBuffer str, String valign) {
		return StartCell(str, valign, "", 1, "");
	}


	public static StringBuffer StartCell(StringBuffer str, String valign, String halign) {
		return StartCell(str, valign, halign, 1, "");
	}

	public static StringBuffer StartCell(StringBuffer str, String valign, String align, int colspan, String other) {
		str.append("<td");
		if (colspan != 1) {
			str.append(" colspan=\"" + colspan + "\"");
		}
		if (valign.length() > 0) {
			str.append(" valign=\"" + valign + "\"");
		}
		if (align.length() > 0) {
			str.append(" align=\"" + align + "\"");
		}
		if (other.length() > 0) {
			str.append(" " + other);
		}
		return str.append(">");

	}
	

	public static StringBuffer EndCell(StringBuffer str) {
		return str.append("</td>");
	}

	
	public static StringBuffer FinishRow(StringBuffer str) {
		return EndRow(str);
	}

	
	public static StringBuffer StartUnorderedList(StringBuffer str) {
		return str.append("<ul>" + CR);
	}
	

	public static StringBuffer StartOrderedList(StringBuffer str) {
		return str.append("<ol>" + CR);
	}

	
	public static StringBuffer StartUnorderedList(StringBuffer str, String id) {
		str.append("<ul");
		if (id.length() > 0) {
			str.append(" id=\"" + id + "\"");
		}
		return str.append(">" + CR);
	}

	
	public static StringBuffer EndUnorderedList(StringBuffer str) {
		return str.append("</ul>" + CR);
	}
	

	public static StringBuffer EndOrderedList(StringBuffer str) {
		return str.append("</ol>" + CR);
	}
	

	public static StringBuffer AddListEntry(StringBuffer str, String text) {
		return AddListEntry(str, "", text);
	}

	
	public static StringBuffer AddListEntry(StringBuffer str, String cls, String text) {
		str.append("<li");
		if (cls.length() > 0) {
			str.append(" class=\"" + cls + "\"");
		}
		return str.append(">" + text + "</li>" + CR);
	}

	
	public static StringBuffer Centre(StringBuffer str, String text) {
		return str.append("<center>" + text + "</center>");
	}

	
	public static StringBuffer AddCell(StringBuffer str, int text) {
		return AddCell(str, 1, ""+text, -1, "", "top", "");
	}

	
	public static StringBuffer AddCell(StringBuffer str, String text) {
		return AddCell(str, 1, text, -1, "", "top", "");
	}

	
	public static StringBuffer AddCell(StringBuffer str, int colspan, String text) {
		return AddCell(str, colspan, text, -1, "", "", "");
	}

	
	public static StringBuffer AddCell(StringBuffer str, String text, String align) {
		return AddCell(str, 1, text, -1, align, "", "");
	}

	
	public static StringBuffer AddCell(StringBuffer str, String text, int width) {
		return AddCell(str, 1, text, width, "", "", "");
	}

	
	public static StringBuffer AddCell(StringBuffer str, int colspan, String text, int width, String align, String valign, String cls) {
		str.append("<td" + (width > 0 ? " width=\"" + width + "\"" : ""));
		str.append((colspan > 1 ? " colspan=\"" + colspan + "\"" : ""));
		str.append((align.length() > 0 ? " align=\"" + align + "\"" : ""));
		str.append((valign.length() > 0 ? " valign=\"" + valign + "\"" : ""));
		str.append((cls.length() > 0 ? " class=\"" + cls + "\"" : ""));
		str.append(">" + text);
		str.append("</td>");
		return str;
	}

	
	public static StringBuffer AddCellHeading(StringBuffer str, String text) {
		return AddCellHeading(str, 1, text, -1, "", "", "");
	}

	public static StringBuffer AddCellHeading(StringBuffer str, int colspan, String text) {
		return AddCellHeading(str, colspan, text, -1, "", "", "");
	}

	public static StringBuffer AddCellHeading(StringBuffer str, String cls, String text) {
		return AddCellHeading(str, 1, text, -1, "", "", cls);
	}

	/*public static StringBuffer AddCellHeading(StringBuffer str, String text, int width) {
		return AddCellHeading(str, 1, text, width, "", "");
	}*/

	public static StringBuffer AddCellHeading(StringBuffer str, int colspan, String text, int width, String align, String valign, String cls) {
		str.append("<th" + (width > 0 ? " width=\"" + width + "\"" : ""));
		str.append((colspan > 1 ? " colspan=\"" + colspan + "\"" : ""));
		str.append((align.length() > 0 ? " align=\"" + align + "\"" : ""));
		str.append((valign.length() > 0 ? " valign=\"" + valign + "\"" : ""));
		str.append((cls.length() > 0 ? " class=\"" + valign + "\"" : ""));
		str.append(">" + text);
		str.append("</th>");
		return str;
	}

	public static StringBuffer StartForm(StringBuffer str, String name, String action, String method) {
		str.append("<form name=\"" + name + "\" action=\""+ action + "\" method=\""+ method + "\">" + CR);
		return str;
	}

	public static StringBuffer EndForm(StringBuffer str) {
		return str.append("</form>" + CR);
	}

	public static StringBuffer HiddenValue(StringBuffer str, String name, int val) {
		return HiddenValue(str, name, ""+val);
	}

	public static StringBuffer HiddenValue(StringBuffer str, String name, String val) {
		return str.append("<input type=\"hidden\" name=\"" + name + "\" value=\"" + val + "\" />" + CR);
	}

	public static StringBuffer TextBox(StringBuffer str, String name, String def, boolean ro, int len) {
		return TextBox(str, name, def, ro, len, -1);
	}


	public static StringBuffer TextBox(StringBuffer str, String name, String def, boolean ro, int len, int size) {
		str.append("<input name=\"" + name + "\" type=\"text\"");
		if (def.length() > 0) {
			str.append(" value=\"" + HTMLFunctions.HTMLEncode(def) + "\"");
		}
		if (ro) {
			str.append(" readonly=\"readonly\"");
		}
		if (len > 0) {
			str.append(" maxlength=\"" + len + "\"");
		}
		if (size > 0) {
			str.append(" size=\"" + size + "\"");
		}
		str.append(" />" + CR);
		return str;
	}


	public static StringBuffer CheckBox(StringBuffer str, String name, String val, boolean checked) {
		str.append("<input type=\"checkbox\" name=\"" + name + "\" value=\"" + val + "\"");
		if (checked) {
			str.append(" checked=\"checked\"");
		}
		str.append(" />");
		return str;
	}

	public static StringBuffer TextArea(StringBuffer str, String name, int rows, int cols, String def, boolean ro) {
		str.append("<textarea name=\"" + name + "\" id=\"" + name + "\" cols=\"" + cols + "\" rows=\"" + rows + "\">");
		if (def.length() > 0) {
			//str.append(s2HTML(def));
			str.append(def);
		}
		str.append("</textarea>" + CR);
		return str;
	}

	public static StringBuffer PasswordBox(StringBuffer str, String name, String def, int len) {
		str.append("<input name=\"" + name + "\" type=\"password\" ");
		str.append("maxlength=\"" + len + "\" ");
		if (def.length() > 0) {
			str.append("value=\"" + def + "\" ");
		}
		str.append("/>" + CR);
		return str;
	}

	
	public static StringBuffer ComboBox(StringBuffer str, String name, String javascript, Object keys[], Object values[], int def, boolean read_only) {
		return ComboBox(str, name, "combobox", javascript, keys, values, ""+def, read_only);
	}

	
	public static StringBuffer ComboBox(StringBuffer str, String name, String cls, String javascript, Object keys[], Object values[], String def, boolean read_only) {
		if (!read_only) {
			str.append("<select name=\"" + name + "\"");
			if (cls.length() > 0) {
				str.append(" class=\"" + cls + "\" ");
			}
			if (javascript.length() > 0) {
				str.append(" " + javascript + " ");
			}
			str.append(">");
			for (int i=0 ; i<values.length ; i++) {
				str.append("<option value=\"" + keys[i] + "\"");
				if (def.equalsIgnoreCase(keys[i].toString())) {
					str.append(" selected=\"selected\" ");
				}
				str.append(">" + values[i] + "</option>");
			}
			str.append("</select>" + CR);
		} else {
			for (int i=0 ; i<values.length ; i++) {
				if (def.equalsIgnoreCase(keys[i].toString())) {
					TextBox(str, name, values[i].toString(), true, values[i].toString().length());
					break;
				}
			}
		}
		return str;
	}

	public static StringBuffer ComboBox(StringBuffer str, String name, String cls, ResultSet rs, int def, boolean blank_row) throws SQLException {
		str.append("<select class=\"" + cls + "\" name=\"" + name + "\">");
		if (blank_row) {
			str.append("<option value=\"\"></option>");
		}
		while (rs.next()) {
			str.append("<option value=\"" + rs.getInt(1) + "\"");
			if (def == rs.getInt(1)) {
				str.append(" selected=\"selected\" ");
			}
			str.append(">" + s2HTML(rs.getString(2)) + "</option>");
		}
		str.append("</select>");
		return str;
	}

	public static StringBuffer ComboBox(StringBuffer str, String name, String[] values, int def) throws SQLException {
		str.append("<select name=\"" + name + "\" class=\"combobox\" >");
		for (int i=0 ; i<values.length ; i++) {
			str.append("<option value=\"" + i + "\"");
			if (i == def) {
				str.append(" selected=\"selected\" ");
			}
			str.append(">" + s2HTML(values[i]) + "</option>");
		}
		str.append("</select>");
		return str;
	}

	public static StringBuffer SubmitButton(StringBuffer str, String text) {
		return str.append("<input type=\"submit\" value=\"" + text + "\" />");
	}

	public static StringBuffer SubmitButtonWithConf(StringBuffer str, String formname, String text, String conf, String extra_msg) {
		str.append("<button class=\"input\" onclick=\"javascript:");
		str.append("if (confirm('" + conf + "')) {\n");
		str.append("var frm = document.getElementsByName('" + formname + "');\n");
		if (extra_msg.length() > 0) {
			str.append("alert('" + extra_msg + "');");
		}
		str.append("frm[0].submit();\n");
		str.append("}\n");
		str.append("return false;\n");
		str.append("\">" + text + "</button>");
		return str;
	}

	public static StringBuffer StartLink(StringBuffer str, String text) {
		return str.append("<a href=\"" + text + "\">");
	}

	public static StringBuffer EndLink(StringBuffer str) {
		return str.append("</a>");
	}

	public static StringBuffer AddLink(StringBuffer str, String link, String text) {
		return str.append("<a href=\"" + link + "\">" + HTMLFunctions.s2HTML(text) + "</a>");
	}

	public static StringBuffer StartCentre(StringBuffer str) {
		return str.append("<center>");
	}

	public static StringBuffer EndCentre(StringBuffer str) {
		return str.append("</center>");
	}

	public static StringBuffer Image(StringBuffer str, String url, String align, String alt, int width, int height) {
		return Image(str, url, align, alt, width, height, 0, "");
	}

	public static StringBuffer Image(StringBuffer str, String url, String align, String alt, int width, int height, int space, String link) {
		if (link.length() > 0) {
			str.append("<a href=\"" + link + "\">");
		}
		str.append("<img src=\"" + url + "\"");
		if (align.length() > 0) {
			str.append(" align=\"" + align + "\"");
		}
		if (alt.length() > 0) {
			str.append(" alt=\"" + alt + "\"");
			str.append(" title=\"" + alt + "\"");
		}
		if (width > 0) {
			str.append(" width=\"" + width + "\"");
		}
		if (height > 0) {
			str.append(" height=\"" + height + "\"");
		}
		if (space > 0) {
			str.append(" vspace=\"" + space + "\" hspace=\"" + space + "\"");
		}
		str.append(" border=\"0\" />");
		if (link.length() > 0) {
			str.append("</a>");
		}
		return str;
	}

	public static StringBuffer BlankLines(StringBuffer str, int no) {
		for (int i=0 ; i<no ; i++) {
			str.append("<br />");
		}
		return str;

	}
	
	
	public static String BoldOpt(String s, boolean b) {
		if (b) {
			return "<b>" + s + "</b>";
		} else {
			return s;
		}
	}

}
