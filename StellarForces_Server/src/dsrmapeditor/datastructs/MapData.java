package dsrmapeditor.datastructs;

import dsrwebserver.maps.ServerMapSquare;
import dsrwebserver.tables.MapDataTable;

public class MapData {
	
	public int size;
	public ServerMapSquare map[][];
	
	public MapData(int sz) {
		size = sz;
		
		map = new ServerMapSquare[sz][sz]; 
	}

	public ServerMapSquare getSq(byte x, byte y) {
		while (map[x][y] == null) {
			map[x][y] = new ServerMapSquare(MapDataTable.MT_FLOOR, x, y, (byte)0, false);
		}
		return map[x][y];
	}
}
