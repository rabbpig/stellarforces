package dsrmapeditor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import java.awt.Image;

import javax.swing.JComponent;

import dsr.TextureStateCache;
import dsrmapeditor.datastructs.MapData;
import dsrwebserver.maps.ServerMapSquare;
import dsrwebserver.tables.MapDataTable;

public class MapWindow extends JComponent {

	private static final long serialVersionUID = 1L;

	private static final int CPU_INSET = 3;

	private MapEditorMain main;
	public static int SQ_SIZE = 20;

	public MapWindow(MapEditorMain m) {
		main = m;

		this.setPreferredSize();
		this.addMouseListener(m);
		this.addMouseMotionListener(m);

	}
	
	
	public void setPreferredSize() {
		MapData map = main.getMapData();
		this.setSize(map.size * SQ_SIZE, map.size * SQ_SIZE);
		this.setPreferredSize(new Dimension(map.size * SQ_SIZE, map.size * SQ_SIZE));
		this.validate();

		
	}


	public void paint(Graphics g) {
		MapData map = main.getMapData();

		for(int y=0 ; y<map.size ; y++) {
			for(int x=0 ; x<map.size ; x++) {
				ServerMapSquare sq = map.map[x][y];
				if (sq.major_type == MapDataTable.MT_WALL && sq.texture_code > 0) {
					if (main.layers_panel.walls.isSelected()) {
						String filename = TextureStateCache.GetFilename(sq.texture_code);
						Image img = MapEditorMain.GetImage(filename);
						g.drawImage(img, x * SQ_SIZE, y * SQ_SIZE, SQ_SIZE, SQ_SIZE, this);
						g.setColor(Color.black);
						g.drawRect(x * SQ_SIZE, y * SQ_SIZE, SQ_SIZE, SQ_SIZE);
						g.setColor(Color.white);
						g.drawRect((x * SQ_SIZE)+1, (y * SQ_SIZE)+1, SQ_SIZE-2, SQ_SIZE-2);
					}
				}
				if ((sq.major_type == MapDataTable.MT_FLOOR || sq.major_type == MapDataTable.MT_COMPUTER) && sq.texture_code > 0) {
					if (main.layers_panel.floor.isSelected()) {
						String filename = TextureStateCache.GetFilename(sq.texture_code);
						Image img = MapEditorMain.GetImage(filename);
						g.drawImage(img, x * SQ_SIZE, y * SQ_SIZE, SQ_SIZE, SQ_SIZE, this);
						
						if (sq.major_type == MapDataTable.MT_COMPUTER) {
							int side = sq.owner_side;
							if (side == 0) {
								side = 1; // In case there's no side attached to the computer
							}
							img = MapEditorMain.GetImage(IconFilenames.COMPUTER(side));
							g.drawImage(img, x * SQ_SIZE + CPU_INSET, y * SQ_SIZE + CPU_INSET, SQ_SIZE - CPU_INSET*2, SQ_SIZE - CPU_INSET*2, this);
						}
					}
				}

				if (sq.major_type == MapDataTable.MT_FLOOR && sq.raised_texture_code > 0) {
					if (main.layers_panel.raised_floor.isSelected()) {
						String filename = TextureStateCache.GetFilename(sq.raised_texture_code);
						Image img = MapEditorMain.GetImage(filename);
						g.drawImage(img, x * SQ_SIZE, y * SQ_SIZE, SQ_SIZE, SQ_SIZE, this);
					}
				}

				if (main.layers_panel.other.isSelected()) {
					if (sq.door_type == MapDataTable.DOOR_NS) {
						Image img = MapEditorMain.GetImage(IconFilenames.DOOR_NS);
						g.drawImage(img, x * SQ_SIZE, y * SQ_SIZE, SQ_SIZE, SQ_SIZE, this);
					} else if (sq.door_type == MapDataTable.DOOR_EW) {
						Image img = MapEditorMain.GetImage(IconFilenames.DOOR_EW);
						g.drawImage(img, x * SQ_SIZE, y * SQ_SIZE, SQ_SIZE, SQ_SIZE, this);
					}
					if (sq.deploy_sq_side > 0) {
						Image img = MapEditorMain.GetImage(IconFilenames.DEPLOY(sq.deploy_sq_side));
						g.drawImage(img, x * SQ_SIZE, y * SQ_SIZE, SQ_SIZE, SQ_SIZE, this);
					}
					if (sq.owner_side > 0) {
						Image img = MapEditorMain.GetImage(IconFilenames.OWNER(sq.owner_side));
						g.drawImage(img, x * SQ_SIZE, y * SQ_SIZE, SQ_SIZE, SQ_SIZE, this);
					}
					if (sq.escape_hatch_side > 0) {
						Image img = MapEditorMain.GetImage(IconFilenames.ESCAPE_HATCH(sq.escape_hatch_side));
						g.drawImage(img, x * SQ_SIZE, y * SQ_SIZE, SQ_SIZE, SQ_SIZE, this);
					}
				}
				if (main.layers_panel.scenery.isSelected()) {
					if (sq.scenery_code > 0) {
						String filename = ""; 
						switch (sq.scenery_code) {
						case MapDataTable.CHAIR:
							filename = IconFilenames.CHAIR(sq.scenery_direction);
							break;
						case MapDataTable.BOOKSHELF:
							filename = IconFilenames.BOOKSHELF(sq.scenery_direction);
							break;
						case MapDataTable.COMPUTER_TABLE:
							filename = IconFilenames.COMPUTER_TABLE(sq.scenery_direction);
							break;
						case MapDataTable.DESK:
							filename = IconFilenames.DESK(sq.scenery_direction);
							break;
						case MapDataTable.FLOWERPOT:
							filename = IconFilenames.FLOWERPOT;
							break;
						case MapDataTable.FLOWERPOT2:
							filename = IconFilenames.FLOWERPOT2;
							break;
						case MapDataTable.METAL_SHELF:
							filename = IconFilenames.METAL_SHELF(sq.scenery_direction);
							break;
						case MapDataTable.SMOKE_FX:
							filename = IconFilenames.SMOKE;
							break;
						case MapDataTable.SPARKS:
							filename = IconFilenames.SPARKS;
							break;
						case MapDataTable.HEDGEROW:
							filename = IconFilenames.HEDGEROW;
							break;
						case MapDataTable.SECTOR1:
							filename = IconFilenames.SECTOR(1, sq.scenery_direction);
							break;
						case MapDataTable.SMALL_POST:
							filename = IconFilenames.SMALL_POST;
							break;
						case MapDataTable.BARREL1:
							filename = IconFilenames.BARREL1;
							break;
						case MapDataTable.BARREL2:
							filename = IconFilenames.BARREL2;
							break;
						case MapDataTable.SECTOR2:
							filename = IconFilenames.SECTOR(2, sq.scenery_direction);
							break;
						case MapDataTable.SECTOR3:
							filename = IconFilenames.SECTOR(3, sq.scenery_direction);
							break;
						case MapDataTable.SECTOR4:
							filename = IconFilenames.SECTOR(4, sq.scenery_direction);
							break;
						case MapDataTable.BRICK_WALL:
							filename = IconFilenames.BRICK_WALL;
							break;
						case MapDataTable.PIPES_L:
							filename = IconFilenames.PIPES_L;
							break;
						case MapDataTable.PIPES_R:
							filename = IconFilenames.PIPES_R;
							break;
						case MapDataTable.RUBBLE:
							filename = IconFilenames.RUBBLE;
							break;
						case MapDataTable.RUBBLE_RED:
							filename = IconFilenames.RUBBLE_RED;
							break;
						case MapDataTable.RUBBLE_YELLOW:
							filename = IconFilenames.RUBBLE_YELLOW;
							break;
						case MapDataTable.RUBBLE_WHITE:
							filename = IconFilenames.RUBBLE_WHITE;
							break;
						case MapDataTable.CHAIR_T:
							filename = IconFilenames.CHAIR_T;
							break;
						case MapDataTable.CHAIR_B:
							filename = IconFilenames.CHAIR_B;
							break;
						case MapDataTable.CHAIR2_L:
							filename = IconFilenames.CHAIR2_L;
							break;
						case MapDataTable.CHAIR2_R:
							filename = IconFilenames.CHAIR2_R;
							break;
						case MapDataTable.CRYO_CHAMBER:
							filename = IconFilenames.CRYO_CHAMBER;
							break;
						case MapDataTable.COMPUTER_SCREEN1:
							filename = IconFilenames.COMPUTER_SCREEN1;
							break;
						case MapDataTable.PLANT3:
							filename = IconFilenames.PLANT3;
							break;
						case MapDataTable.RED_WALL_PANEL:
							filename = IconFilenames.RED_WALL_PANEL;
							break;
						case MapDataTable.GREEN_WALL_PANEL:
							filename = IconFilenames.GREEN_WALL_PANEL;
							break;
						case MapDataTable.DAMAGED_FLOOR:
							filename = IconFilenames.DAMAGED_FLOOR;
							break;
						case MapDataTable.DAMAGED_FLOOR2:
							filename = IconFilenames.DAMAGED_FLOOR2;
							break;
						case MapDataTable.DAMAGED_FLOOR3:
							filename = IconFilenames.DAMAGED_FLOOR3;
							break;
						case MapDataTable.DAMAGED_FLOOR4:
							filename = IconFilenames.DAMAGED_FLOOR4;
							break;
						case MapDataTable.DAMAGED_FLOOR5:
							filename = IconFilenames.DAMAGED_FLOOR5;
							break;
						case MapDataTable.DAMAGED_FLOOR6:
							filename = IconFilenames.DAMAGED_FLOOR6;
							break;
						case MapDataTable.DAMAGED_FLOOR7:
							filename = IconFilenames.DAMAGED_FLOOR7;
							break;
						case MapDataTable.GRILL:
							filename = IconFilenames.GRILL;
							break;
						case MapDataTable.SINGLE_PIPE_L:
							filename = IconFilenames.SINGLE_PIPE_L;
							break;
						case MapDataTable.SINGLE_PIPE_R:
							filename = IconFilenames.SINGLE_PIPE_R;
							break;
						case MapDataTable.HORIZONTAL_PIPE:
							filename = IconFilenames.HORIZONTAL_PIPE;
							break;
						case MapDataTable.PLANT4:
							filename = IconFilenames.PLANT4;
							break;
						case MapDataTable.PLANT5:
							filename = IconFilenames.PLANT5;
							break;
						case MapDataTable.PLANT6:
							filename = IconFilenames.PLANT6;
							break;
						case MapDataTable.PLANT7:
							filename = IconFilenames.PLANT7;
							break;
						case MapDataTable.REPLICATOR:
							filename = IconFilenames.REPLICATOR;
							break;
						case MapDataTable.CRACK1:
							filename = IconFilenames.CRACK1;
							break;
						case MapDataTable.CRACK2:
							filename = IconFilenames.CRACK2;
							break;
						case MapDataTable.WEED1:
							filename = IconFilenames.WEED1;
							break;
						case MapDataTable.WEED2:
							filename = IconFilenames.WEED2;
							break;
						case MapDataTable.GRAFFITI_DIE:
							filename = IconFilenames.GRAFFITI_DIE;
							break;
						case MapDataTable.GRAFFITI_HELP:
							filename = IconFilenames.GRAFFITI_HELP;
							break;
						case MapDataTable.GRAFFITI_BRAINS:
							filename = IconFilenames.GRAFFITI_BRAINS;
							break;
						case MapDataTable.BED_TOP:
							filename = IconFilenames.BED_TOP;
							break;
						case MapDataTable.BED_BOTTOM:
							filename = IconFilenames.BED_BOTTOM;
							break;
						default:
							throw new RuntimeException("Unknown scenery type: " + sq.scenery_code);
						}
						Image img = MapEditorMain.GetImage(filename);
						g.drawImage(img, x * SQ_SIZE, y * SQ_SIZE, SQ_SIZE, SQ_SIZE, this);
					}
				}
			}
		}
	}

}
