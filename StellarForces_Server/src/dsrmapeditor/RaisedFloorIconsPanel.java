package dsrmapeditor;

import dsr.TextureStateCache;

public class RaisedFloorIconsPanel extends AbstractIconPanel {

	private static final long serialVersionUID = 1L;

	public RaisedFloorIconsPanel(MapEditorMain main) {
		super();
		
		// Texture Icons
		for (short i=1 ; i<=TextureStateCache.MAX_TEX_NUM ; i++) {
			if (TextureStateCache.GetFilename(i).length() > 0) {
				RaisedFloorIcon ti = new RaisedFloorIcon(main, i, TextureStateCache.GetFilename(i), main);
				this.add(ti);
			}
		}
		
		RaisedFloorIcon ti = new RaisedFloorIcon(main, (short)-1, MapEditorMain.MAP_ED_ICONS + "erase.png", main);
		this.add(ti);

	}
}
