package dsrmapeditor;

import java.awt.event.MouseListener;

public class MiscIcon extends Icon {
	
	// Major type
	public static final byte DOOR = 1;
	public static final byte DEPLOY = 2;
	public static final byte OWNER = 3;
	public static final byte ESCAPE_HATCH = 4;
	public static final byte COMPUTER = 5;

	private static final long serialVersionUID = 1L;
	
	public byte side;

	public MiscIcon(MapEditorMain m, byte cmd, byte _side, String filename, MouseListener ml) {
		super(m, cmd, filename, ml);
		
		side = _side;
	}

}
