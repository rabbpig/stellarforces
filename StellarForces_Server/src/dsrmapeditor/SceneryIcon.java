package dsrmapeditor;

import java.awt.event.MouseListener;

public class SceneryIcon extends Icon {
	
	private static final long serialVersionUID = 1L;
	
	public byte direction;
	
	public SceneryIcon(MapEditorMain m, short type, byte _dir, String filename, MouseListener ml) {
		super(m, type, filename, ml);
		direction = _dir;
	}

}
