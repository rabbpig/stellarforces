package dsrmapeditor;

import dsr.AppletMain;

public class IconFilenames {
	
	public static final String DOOR_NS = MapEditorMain.MAP_ED_ICONS + "door_ns.png";
	public static final String DOOR_EW = MapEditorMain.MAP_ED_ICONS + "door_ew.png";
	public static final String FLOWERPOT = MapEditorMain.MAP_ED_ICONS + "plant1.png";
	public static final String FLOWERPOT2 = MapEditorMain.MAP_ED_ICONS + "plant2.png";
	public static final String SMOKE = MapEditorMain.MAP_ED_ICONS + "smoke.png";
	public static final String SPARKS = MapEditorMain.MAP_ED_ICONS + "sparks.png";
	public static final String HEDGEROW = MapEditorMain.MAP_ED_ICONS + "hedge.png";
	public static final String SMALL_POST = MapEditorMain.MAP_ED_ICONS + "smallpost.png";
	public static final String BARREL1 = MapEditorMain.MAP_ED_ICONS + "barrel1.png";
	public static final String BARREL2 = MapEditorMain.MAP_ED_ICONS + "barrel2.png";
	public static final String BRICK_WALL = MapEditorMain.MAP_ED_ICONS + "bricks.png";
	public static final String PIPES_L = MapEditorMain.MAP_ED_ICONS + "pipes_l.png";
	public static final String PIPES_R = MapEditorMain.MAP_ED_ICONS + "pipes_r.png";
	public static final String RUBBLE = MapEditorMain.MAP_ED_ICONS + "rubble.png";
	public static final String RUBBLE_RED = MapEditorMain.MAP_ED_ICONS + "rubble_red.png";
	public static final String RUBBLE_YELLOW = MapEditorMain.MAP_ED_ICONS + "rubble_yellow.png";
	public static final String RUBBLE_WHITE = MapEditorMain.MAP_ED_ICONS + "rubble_white.png";
	public static final String CHAIR_T = MapEditorMain.MAP_ED_ICONS + "chair_t.png";
	public static final String CHAIR_B = MapEditorMain.MAP_ED_ICONS + "chair_b.png";
	public static final String CHAIR2_L = MapEditorMain.MAP_ED_ICONS + "chair2_l.png";
	public static final String CHAIR2_R = MapEditorMain.MAP_ED_ICONS + "chair2_r.png";
	public static final String CRYO_CHAMBER = MapEditorMain.MAP_ED_ICONS + "cryo_chamber.png";
	public static final String COMPUTER_SCREEN1 = MapEditorMain.MAP_ED_ICONS + "computer_screen1.png";
	public static final String PLANT3 = MapEditorMain.MAP_ED_ICONS + "plant3.png";
	public static final String RED_WALL_PANEL = MapEditorMain.MAP_ED_ICONS + "red_wall_panel.png";
	public static final String GREEN_WALL_PANEL = MapEditorMain.MAP_ED_ICONS + "green_wall_panel.png";
	public static final String DAMAGED_FLOOR = MapEditorMain.MAP_ED_ICONS + "damaged_floor.png";
	public static final String DAMAGED_FLOOR2 = MapEditorMain.MAP_ED_ICONS + "damaged_floor2.png";
	public static final String DAMAGED_FLOOR3 = MapEditorMain.MAP_ED_ICONS + "damaged_floor3.png";
	public static final String DAMAGED_FLOOR4 = MapEditorMain.MAP_ED_ICONS + "damaged_floor4.png";
	public static final String DAMAGED_FLOOR5 = MapEditorMain.MAP_ED_ICONS + "damaged_floor5.png";
	public static final String DAMAGED_FLOOR6 = MapEditorMain.MAP_ED_ICONS + "damaged_floor6.png";
	public static final String DAMAGED_FLOOR7 = MapEditorMain.MAP_ED_ICONS + "damaged_floor7.png";
	public static final String GRILL = MapEditorMain.MAP_ED_ICONS + "grill.png";
	public static final String SINGLE_PIPE_L = MapEditorMain.MAP_ED_ICONS + "single_pipe_l.png";
	public static final String SINGLE_PIPE_R = MapEditorMain.MAP_ED_ICONS + "single_pipe_r.png";
	public static final String HORIZONTAL_PIPE = MapEditorMain.MAP_ED_ICONS + "pipe_horiz.png";
	public static final String PLANT4 = MapEditorMain.MAP_ED_ICONS + "plant4.png";
	public static final String PLANT5 = MapEditorMain.MAP_ED_ICONS + "plant5.png";
	public static final String PLANT6 = MapEditorMain.MAP_ED_ICONS + "plant6.png";
	public static final String PLANT7 = MapEditorMain.MAP_ED_ICONS + "plant7.png";
	public static final String REPLICATOR = MapEditorMain.MAP_ED_ICONS + "replicator.png";
	public static final String CRACK1 = MapEditorMain.MAP_ED_ICONS + "crack1.png";
	public static final String CRACK2 = MapEditorMain.MAP_ED_ICONS + "crack2.png";
	public static final String WEED1 = MapEditorMain.MAP_ED_ICONS + "weed1.png";
	public static final String WEED2 = MapEditorMain.MAP_ED_ICONS + "weed2.png";
	public static final String GRAFFITI_DIE = MapEditorMain.MAP_ED_ICONS + "graffiti_die.png";
	public static final String GRAFFITI_HELP = MapEditorMain.MAP_ED_ICONS + "graffiti_help.png";
	public static final String GRAFFITI_BRAINS = MapEditorMain.MAP_ED_ICONS + "graffiti_brains.png";
	public static final String BED_TOP = MapEditorMain.MAP_ED_ICONS + "bed_top.png";
	public static final String BED_BOTTOM = MapEditorMain.MAP_ED_ICONS + "bed_bottom.png";
	
	public static String DEPLOY(int side) {
		return MapEditorMain.MAP_ED_ICONS + "deploy" + side + ".png";
	}

	public static String OWNER(int side) {
		return MapEditorMain.MAP_ED_ICONS + "owner" + side + ".png";
	}

	public static String COMPUTER(int side) {
		return AppletMain.DATA_DIR + "textures/ComputerConsole" + side + ".jpg";
	}

	public static String CHAIR(int side) {
		return MapEditorMain.MAP_ED_ICONS + "chair_" + side + ".png";
	}

	public static String BOOKSHELF(int side) {
		return MapEditorMain.MAP_ED_ICONS + "bookshelf_" + side + ".png";
	}

	public static String METAL_SHELF(int side) {
		return MapEditorMain.MAP_ED_ICONS + "metalshelf_" + side + ".png";
	}

	public static String COMPUTER_TABLE(int side) {
		return MapEditorMain.MAP_ED_ICONS + "computerdesk_" + side + ".png";
	}

	public static String DESK(int side) {
		return MapEditorMain.MAP_ED_ICONS + "desk_" + side + ".png";
	}

	public static String ESCAPE_HATCH(int side) {
		return MapEditorMain.MAP_ED_ICONS + "escape" + side + ".png";
	}

	public static String SECTOR(int num, int side) {
		return MapEditorMain.MAP_ED_ICONS + "sector" + num + "_" + side + ".png";
	}

}
