package dsrmapeditor;

import dsrwebserver.tables.MapDataTable;

public class SceneryIconsPanel extends AbstractIconPanel {

	private static final long serialVersionUID = 1L;

	public SceneryIconsPanel(MapEditorMain m) {
		super();

		SceneryIcon ti = new SceneryIcon(m, (byte)-1, (byte)-1, MapEditorMain.MAP_ED_ICONS + "erase.png", m);
		this.add(ti);

		/*for (byte i=0 ; i<=3 ; i++) {
			ti = new SceneryIcon(m, MapDataTable.CHAIR, i, IconFilenames.CHAIR(i), m);
			this.add(ti);
		}*/

		/*for (byte i=0 ; i<=3 ; i++) {
			ti = new SceneryIcon(m, MapDataTable.BOOKSHELF, i, IconFilenames.BOOKSHELF(i), m);
			this.add(ti);
		}*/

		/*for (byte i=0 ; i<=3 ; i++) {
			ti = new SceneryIcon(m, MapDataTable.COMPUTER_TABLE, i, IconFilenames.COMPUTER_TABLE(i), m);
			this.add(ti);
		}*/

		for (byte i=0 ; i<=3 ; i++) {
			ti = new SceneryIcon(m, MapDataTable.DESK, i, IconFilenames.DESK(i), m);
			this.add(ti);
		}

		for (byte i=0 ; i<=3 ; i++) {
			ti = new SceneryIcon(m, MapDataTable.METAL_SHELF, i, IconFilenames.METAL_SHELF(i), m);
			this.add(ti);
		}

		ti = new SceneryIcon(m, MapDataTable.FLOWERPOT, (byte)0, IconFilenames.FLOWERPOT, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.FLOWERPOT2, (byte)0, IconFilenames.FLOWERPOT2, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.SMOKE_FX, (byte)0, IconFilenames.SMOKE, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.SPARKS, (byte)0, IconFilenames.SPARKS, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.HEDGEROW, (byte)0, IconFilenames.HEDGEROW, m);
		this.add(ti);

		for (byte i=0 ; i<=3 ; i++) {
			ti = new SceneryIcon(m, MapDataTable.SECTOR1, i, IconFilenames.SECTOR(1, i), m);
			this.add(ti);
		}

		ti = new SceneryIcon(m, MapDataTable.SMALL_POST, (byte)0, IconFilenames.SMALL_POST, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.BARREL1, (byte)0, IconFilenames.BARREL1, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.BARREL2, (byte)0, IconFilenames.BARREL2, m);
		this.add(ti);

		for (byte i=0 ; i<=3 ; i++) {
			ti = new SceneryIcon(m, MapDataTable.SECTOR2, i, IconFilenames.SECTOR(2, i), m);
			this.add(ti);
		}

		for (byte i=0 ; i<=3 ; i++) {
			ti = new SceneryIcon(m, MapDataTable.SECTOR3, i, IconFilenames.SECTOR(3, i), m);
			this.add(ti);
		}

		for (byte i=0 ; i<=3 ; i++) {
			ti = new SceneryIcon(m, MapDataTable.SECTOR4, i, IconFilenames.SECTOR(4, i), m);
			this.add(ti);
		}

		ti = new SceneryIcon(m, MapDataTable.BRICK_WALL, (byte)0, IconFilenames.BRICK_WALL, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.PIPES_L, (byte)0, IconFilenames.PIPES_L, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.PIPES_R, (byte)0, IconFilenames.PIPES_R, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.RUBBLE, (byte)0, IconFilenames.RUBBLE, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.RUBBLE_RED, (byte)0, IconFilenames.RUBBLE_RED, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.RUBBLE_YELLOW, (byte)0, IconFilenames.RUBBLE_YELLOW, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.RUBBLE_WHITE, (byte)0, IconFilenames.RUBBLE_WHITE, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.CHAIR_T, (byte)0, IconFilenames.CHAIR_T, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.CHAIR_B, (byte)0, IconFilenames.CHAIR_B, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.CHAIR2_L, (byte)0, IconFilenames.CHAIR2_L, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.CHAIR2_R, (byte)0, IconFilenames.CHAIR2_R, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.CRYO_CHAMBER, (byte)0, IconFilenames.CRYO_CHAMBER, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.COMPUTER_SCREEN1, (byte)0, IconFilenames.COMPUTER_SCREEN1, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.PLANT3, (byte)0, IconFilenames.PLANT3, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.RED_WALL_PANEL, (byte)0, IconFilenames.RED_WALL_PANEL, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.GREEN_WALL_PANEL, (byte)0, IconFilenames.GREEN_WALL_PANEL, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.DAMAGED_FLOOR, (byte)0, IconFilenames.DAMAGED_FLOOR, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.DAMAGED_FLOOR2, (byte)0, IconFilenames.DAMAGED_FLOOR2, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.DAMAGED_FLOOR3, (byte)0, IconFilenames.DAMAGED_FLOOR3, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.DAMAGED_FLOOR4, (byte)0, IconFilenames.DAMAGED_FLOOR4, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.DAMAGED_FLOOR5, (byte)0, IconFilenames.DAMAGED_FLOOR5, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.DAMAGED_FLOOR6, (byte)0, IconFilenames.DAMAGED_FLOOR6, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.DAMAGED_FLOOR7, (byte)0, IconFilenames.DAMAGED_FLOOR7, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.GRILL, (byte)0, IconFilenames.GRILL, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.SINGLE_PIPE_L, (byte)0, IconFilenames.SINGLE_PIPE_L, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.SINGLE_PIPE_R, (byte)0, IconFilenames.SINGLE_PIPE_R, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.HORIZONTAL_PIPE, (byte)0, IconFilenames.HORIZONTAL_PIPE, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.PLANT4, (byte)0, IconFilenames.PLANT4, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.PLANT5, (byte)0, IconFilenames.PLANT5, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.PLANT6, (byte)0, IconFilenames.PLANT6, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.PLANT7, (byte)0, IconFilenames.PLANT7, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.REPLICATOR, (byte)0, IconFilenames.REPLICATOR, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.CRACK1, (byte)0, IconFilenames.CRACK1, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.CRACK2, (byte)0, IconFilenames.CRACK2, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.WEED1, (byte)0, IconFilenames.WEED1, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.WEED2, (byte)0, IconFilenames.WEED2, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.GRAFFITI_DIE, (byte)0, IconFilenames.GRAFFITI_DIE, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.GRAFFITI_HELP, (byte)0, IconFilenames.GRAFFITI_HELP, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.GRAFFITI_BRAINS, (byte)0, IconFilenames.GRAFFITI_BRAINS, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.BED_TOP, (byte)0, IconFilenames.BED_TOP, m);
		this.add(ti);

		ti = new SceneryIcon(m, MapDataTable.BED_BOTTOM, (byte)0, IconFilenames.BED_BOTTOM, m);
		this.add(ti);


	}

}
