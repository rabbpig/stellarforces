package dsrmapeditor;

import java.io.FileNotFoundException;
import java.io.IOException;

import ssmith.io.TextFile;
import ssmith.lang.Functions;
import dsr.SharedStatics;
import dsrmapeditor.datastructs.MapData;
import dsrwebserver.maps.ServerMapSquare;
import dsrwebserver.tables.MapDataTable;

public class MapEditorImportExport {

	public static MapData Import(String filename) throws IOException {
		TextFile tf = new TextFile();
		tf.openFile(filename, TextFile.READ);
		String line[] = tf.readLine().split(",");
		int size = Functions.ParseInt(line[0]);
		MapData map = new MapData(size);
		//map.random_deploy_squares.clear();
		byte z=0;
		while (tf.isEOF() == false) { // Loop through each line of the file
			line = tf.readLine().replaceAll("\"", "").split(",");
			if (z<size) {
				for(byte x=0 ; x<size ; x++) { // Loop through each section of the line
					ServerMapSquare sq = map.getSq(x, z);
					String data[] = null;
					try {
						data = line[x].split("\\|");
					} catch (java.lang.ArrayIndexOutOfBoundsException ex2) {
						ex2.printStackTrace();
					}
					for (int i=0 ; i<data.length ; i++) { // Loop through each bit of data in the cell
						if (data[i].length() > 0) {
							String subdata[] = data[i].split("\\:");
							if (subdata[0].equalsIgnoreCase("NOTHING")) {
								sq.major_type = MapDataTable.MT_NOTHING;
							} else if (subdata[0].equalsIgnoreCase("FLOOR")) {
								sq.major_type = MapDataTable.MT_FLOOR;
								sq.texture_code = Short.parseShort(subdata[1]);
							} else if (subdata[0].equalsIgnoreCase("WALL")) {
								sq.major_type = MapDataTable.MT_WALL;
								sq.texture_code = Short.parseShort(subdata[1]);
							} else if (subdata[0].equalsIgnoreCase("COMP")) {
								sq.major_type = MapDataTable.MT_COMPUTER;
								sq.texture_code = Short.parseShort(subdata[1]);
							} else if (subdata[0].equalsIgnoreCase("RAISED_FLOOR")) {
								sq.raised_texture_code = Short.parseShort(subdata[1]);
							} else if (subdata[0].equalsIgnoreCase("DOOR")) {
								sq.door_type = Byte.parseByte(subdata[1]);
							} else if (subdata[0].equalsIgnoreCase("OWNER")) {
								sq.owner_side = Byte.parseByte(subdata[1]);
							} else if (subdata[0].equalsIgnoreCase("DEPLOY")) {
								sq.deploy_sq_side = Byte.parseByte(subdata[1]);
							} else if (subdata[0].equalsIgnoreCase("ESCAPE")) {
								sq.escape_hatch_side = Byte.parseByte(subdata[1]);
							//} else if (subdata[0].equalsIgnoreCase("RND_DEPLOY")) {
								//map.random_deploy_squares.add(new Point(x, z));
							} else if (subdata[0].equalsIgnoreCase("SCENERY")) {
								sq.scenery_code = Short.parseShort(subdata[1]);
								if (subdata.length >= 3) {
									sq.scenery_direction = Byte.parseByte(subdata[2]);
								}
							} else {
								throw new RuntimeException("Unknown code: " + subdata[0]);
							}
						}
					}
				}
			}
			z++;
		}
		tf.close();

		return map;
	}


	public static void Export(MapData map, String filename) throws FileNotFoundException, IOException {
		StringBuffer data = new StringBuffer();
		data.append(map.size + "," + SharedStatics.VERSION + "\n");

		for (byte y=0 ; y<map.size ; y++) {
			StringBuffer str_line = new StringBuffer();
			for (byte x=0 ; x<map.size ; x++) {
				StringBuffer str_tile = new StringBuffer();
				ServerMapSquare sq = map.getSq(x, y);
				switch (sq.major_type) {
				case MapDataTable.MT_WALL:
					str_tile.append("WALL:" + sq.texture_code);
					break;
				case MapDataTable.MT_FLOOR:
					str_tile.append("FLOOR:" + sq.texture_code);
					break;
				case MapDataTable.MT_COMPUTER:
					str_tile.append("COMP:" + sq.texture_code);
					break;
				case MapDataTable.MT_NOTHING:
					str_tile.append("NOTHING:0");
					break;
				default:
					throw new RuntimeException("Unknown major type:" + sq.major_type);
				}
				str_tile.append("|");
				
				if (sq.raised_texture_code > 0) {
					str_tile.append("RAISED_FLOOR:" + sq.raised_texture_code + "|");
				}
				if (sq.door_type > 0) {
					str_tile.append("DOOR:" + sq.door_type + "|");
				}
				if (sq.owner_side > 0) {
					str_tile.append("OWNER:" + sq.owner_side + "|");
				}
				if (sq.deploy_sq_side > 0) {
					str_tile.append("DEPLOY:" + sq.deploy_sq_side + "|");
				}
				if (sq.escape_hatch_side > 0) {
					str_tile.append("ESCAPE:" + sq.escape_hatch_side + "|");
				}
				if (sq.scenery_code > 0) {
					str_tile.append("SCENERY:" + sq.scenery_code + ":" + sq.scenery_direction + "|");
				}
				
				str_line.append(str_tile + ",");
			}
			data.append(str_line + "\n");
		}

		TextFile.QuickWrite(filename, data.toString(), true);

	}

}
