package dsrmapeditor;

import dsr.TextureStateCache;

public class FloorIconsPanel extends AbstractIconPanel {

	private static final long serialVersionUID = 1L;

	public FloorIconsPanel(MapEditorMain main) {
		super();

		// Texture Icons
		for (byte i=1 ; i<=TextureStateCache.MAX_TEX_NUM ; i++) {
			if (TextureStateCache.GetFilename(i).length() > 0) {
				FloorIcon ti = new FloorIcon(main, i, TextureStateCache.GetFilename(i), main);
				this.add(ti);
			}
		}

		FloorIcon ti = new FloorIcon(main, (byte)-1, MapEditorMain.MAP_ED_ICONS + "erase.png", main);
		this.add(ti);

	}
}
