package dsrmapeditor;

import dsrwebserver.tables.MapDataTable;

public class MiscIconsPanel extends AbstractIconPanel {
	
	private static final long serialVersionUID = 1L;

	public MiscIconsPanel(MapEditorMain m) {
		super();

		MiscIcon ti = new MiscIcon(m, MiscIcon.DOOR, MapDataTable.DOOR_NS, IconFilenames.DOOR_NS, m);
		this.add(ti);

		ti = new MiscIcon(m, MiscIcon.DOOR, MapDataTable.DOOR_EW, IconFilenames.DOOR_EW, m);
		this.add(ti);

		for (byte i=1 ; i<=4 ; i++) {
			ti = new MiscIcon(m, MiscIcon.DEPLOY, i, IconFilenames.DEPLOY(i), m);
			this.add(ti);
		}

		for (byte i=1 ; i<=4 ; i++) {
			ti = new MiscIcon(m, MiscIcon.ESCAPE_HATCH, i, IconFilenames.ESCAPE_HATCH(i), m);
			this.add(ti);
		}
		// All escape
		ti = new MiscIcon(m, MiscIcon.ESCAPE_HATCH, (byte)5, MapEditorMain.MAP_ED_ICONS + "escape5.png", m);
		this.add(ti);

		for (byte i=1 ; i<=4 ; i++) {
			ti = new MiscIcon(m, MiscIcon.OWNER, i, IconFilenames.OWNER(i), m);
			this.add(ti);
		}

		for (byte i=1 ; i<=4 ; i++) {
			ti = new MiscIcon(m, MiscIcon.COMPUTER, i, IconFilenames.COMPUTER(i), m);
			this.add(ti);
		}

		ti = new MiscIcon(m, (byte)-1, (byte)-1, MapEditorMain.MAP_ED_ICONS + "erase.png", m);
		this.add(ti);

	}

}
