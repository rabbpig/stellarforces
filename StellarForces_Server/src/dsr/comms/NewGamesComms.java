package dsr.comms;

import java.awt.Point;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.lang.Functions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.dsr.GameRequests;
import dsrwebserver.pages.dsr.startpractisemission;
import dsrwebserver.tables.GameRequestsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UploadedMapsTable;

/**
 * This lists all the games available for joining.
 *
 */
public class NewGamesComms {

	// Called by the server
	public static String GetNewGamesListAsResponse(MySQLConnection dbs, String login, String pwd, int android_version) throws SQLException {
		LoginsTable current_login = new LoginsTable(dbs);
		if (current_login.selectUser(login, pwd)) {
			int our_points = current_login.getELOPoints();
			int games_played = current_login.getGamesExperience();

			GameRequestsTable reqs = new GameRequestsTable(dbs);
			LoginsTable req_owner = new LoginsTable(dbs);

			StringBuffer str = new StringBuffer();
			str.append("GameRequestID|MissionName|Player1Name|GameType|NumSides|Advanced|PCentCreds|Comment|SidesAvailable|MissionType|SideNames|ForUs|PointsForWin|PointsForLose|MapName\n");

			String sql = "SELECT * FROM GameRequests WHERE COALESCE(Accepted, 0) = 0 AND COALESCE(CampGame, 0) = 0 ORDER BY Mission";
			ResultSet rs_gamereqs = dbs.getResultSet(sql);
			while (rs_gamereqs.next()) {
				if (reqs.doesRowExist(rs_gamereqs.getInt("GameRequestID"))) { // In case it has been deleted between selecting the rows and reading them
					reqs.selectRow(rs_gamereqs.getInt("GameRequestID"));

					int pid = rs_gamereqs.getInt("Player1ID");
					/*if (rs_gamereqs.getInt("GameRequestID") == 3929) {
						int dfg = 34545;
					}*/
					req_owner.selectRow(pid);

					AbstractMission this_mission = AbstractMission.Factory(rs_gamereqs.getInt("Mission")); 

					// If it's not our request...
					if (rs_gamereqs.getInt("Player1ID") != current_login.getLoginID()) {
						if (this_mission.getMinAndroidVersion() > android_version) {
							continue;
						}
						// Don't show locked requests not for us
						if (reqs.isSpaceAvailable() == false && reqs.isPlayerInGame(current_login.getID()) == false) {
							continue;
						}

						// Don't show if we're experienced and the game is set for n00bs
						if (reqs.getOpponentType() == GameRequestsTable.OT_NEWBIES && current_login.getGamesExperience() > 0) {
							//if (reqs.getOpponentType() == GameRequestsTable.OT_NEWBIES && current_login.getGamesExperience() > 20 && current_login.getELOPoints() > 20) {
							continue;
						}

						// Don't show if we're experienced and the game is set for intermediates
						if (reqs.getOpponentType() == GameRequestsTable.OT_INTERMEDIATE && current_login.getGamesExperience() > 70) {
							continue;
						}

						// Don't show if we're experienced and the game is set for intermediates
						if (reqs.getOpponentType() == GameRequestsTable.OT_NON_INTERMEDIATE && current_login.getELOPoints() < 20) {
							continue;
						}

						// Don't show faction games
						if (reqs.isCampaignGame()) {
							continue;
						}

						// Don't show if we have an opponent points range set
						if (reqs.getOpponentPointsRange() > 0) {
							Point p = req_owner.getMinAndMaxOppRange(reqs.getOpponentPointsRange());
							int min = p.x;
							int max = p.y;
							if (our_points < min || our_points > max) {
								continue;
							}
						}

						// Only show Android games NO, WE SHOW ALL NOW
						/*if (this_mission.canBePlayedOnAndroid() == AbstractMission.ONLY_ONE_CLIENT && reqs.isAndroid() == 0) {
							continue;
						}*/

						// Don't show the game if we're not experienced enough
						if (games_played < this_mission.getGamesReqToPlay() && reqs.isPlayerInGame(current_login.getID()) == false && DSRWebServer.DEBUG == false) {
							continue;
						}

						// Don't show if we've already accepted it
						if (reqs.isPlayerInGame(current_login.getID()) && reqs.hasPlayerSelectedASide(current_login.getID())) {
							continue; // Already accepted it!
						}

						str.append(reqs.getID() + "|" + AbstractMission.GetMissionNameFromType(reqs.getMission(), false, false) + "|" + LoginsTable.GetDisplayName(dbs, reqs.getPlayerIDByNum(1)) + "|" + reqs.getType() + "|");
						str.append(reqs.getNumSides() + "|" + SQLFuncs.b201(reqs.isAdvancedMode()) + "|" + reqs.getPcentCredit() + "|");
						str.append(AbstractCommFuncs.URLEncodeString(reqs.getComment()) + "|");
						// Sides available
						for (int s=1 ; s<=reqs.getNumSides() ; s++) {
							boolean side_selected = false;			
							for (int player=1 ; player<=reqs.getNumSides() ; player++) {
								if (rs_gamereqs.getInt("Player" + player + "ID") > 0) { // Is this player in the game
									if (rs_gamereqs.getInt("Player" + player + "SideChoice") == s) {
										side_selected = true;
									}
								}
							}
							if (side_selected == false) {
								str.append(s + ",");
							}
						}
						// Remove last comma?
						if (str.toString().endsWith(",")) {
							str.delete(str.length()-1, str.length());
						}
						str.append("|");

						str.append(this_mission.getMissionID() + "|");

						// Side names
						for (int s=1 ; s<=reqs.getNumSides() ; s++) {
							str.append(this_mission.getSideDescription(s) + ",");
						}
						// Remove last comma
						if (str.toString().endsWith(",")) {
							str.delete(str.length()-1, str.length());
						}
						str.append("|");

						// For us?
						if (reqs.isPlayerInGame(current_login.getID())) {
							str.append("1|");
						} else {
							str.append("0|");
						}

						// Points for win
						if (reqs.isPractise() == false && reqs.getNumSides() == 2) {
							str.append(current_login.getPointsForWinAgainst(req_owner) + "|");
							str.append(current_login.getPointsForLoseAgainst(req_owner) + "|");
						} else {
							str.append("0|0|");
						}
						
						if (reqs.getUploadedMapID() > 0) {
							str.append(UploadedMapsTable.GetName(dbs, reqs.getUploadedMapID()) + "|");
						} else {
							str.append("|");
							
						}
						str.append("\n");
					}
				}

			}
			rs_gamereqs.close();
			current_login.close();
			reqs.close();
			return str.toString();
		} else {
			current_login.close();
			return "Invalid login/password";
		}
	}


	// Called by the server
	public static synchronized String GetJoinNewGameAsResponse(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			current_login.selectUser(login, pwd);

			int reqid = Functions.ParseInt(hashmap.get("reqid"));
			int side = Functions.ParseInt(hashmap.get("side"));

			GameRequestsTable requests = new GameRequestsTable(dbs);
			if (requests.doesRowExist(reqid)) {
				requests.selectRow(reqid);
				GamesTable game = new GamesTable(dbs);
				StringBuffer str = new StringBuffer();
				try {
					requests.acceptRequest(current_login, game, side);
				} catch (Exception ex)	 {
					str.append(ex.getMessage());
					DSRWebServer.HandleError(ex);
				}
				game.close();
				requests.close();
				return str.toString(); // Empty means success
			} else {
				return "Sorry, game request " + reqid + " does not exist.";
			}
		} finally {
			current_login.close();
		}
	}


	public static String GetMissionListAsResponse(MySQLConnection dbs, HashMap<String, String> hashmap, String login, String pwd, int android_version) throws SQLException {
		boolean free = hashmap.get("free").length() > 0;
		int games_played = 999;
		try {
			if (login != null && pwd != null) {
				LoginsTable current_login = new LoginsTable(dbs);
				if (current_login.selectUser(login, pwd)) {
					games_played = current_login.getGamesExperience();
				}
				current_login.close();
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

		StringBuffer str = new StringBuffer();
		str.append("MissionID|Name|Description|Sides|Side1Name|Side2Name|Side3Name|Side4Name|MaxTurns|AIForSide1|AIForSide2|AIForSide3|AIForSide4\n");
		//if (DSRWebServer.IsSF()) {
		if (free) {
			AbstractMission mission = AbstractMission.Factory(AbstractMission.THE_ASSASSINS);
			AppendMissionLine(str, mission);
			mission = AbstractMission.Factory(AbstractMission.MOONBASE_ASSAULT);
			AppendMissionLine(str, mission);
		} else {
			for (int j=0 ; j<AbstractMission.SF_MISSIONS.length ; j++) {
				int m = AbstractMission.SF_MISSIONS[j];
				if (AbstractMission.IsValidMission(m)) {
					AbstractMission mission = AbstractMission.Factory(m);
					if (mission.getMinAndroidVersion() > android_version) {
						continue;
					}
					if (games_played < mission.getGamesReqToPlay() && DSRWebServer.DEBUG == false) {
						continue;
					}
					AppendMissionLine(str, mission);
				}
			}
		} 
		return str.toString(); 
	}


	private static void AppendMissionLine(StringBuffer str, AbstractMission mission) {// throws UnsupportedEncodingException {
		str.append(mission.getMissionID() + "|" + mission.mission_name + "|" + AbstractCommFuncs.URLEncodeString(mission.getMissionOverview(false).replaceAll("\\<.*?\\>", " ")) + "|" + mission.getNumOfSides() + "|");
		// Side names
		for (int s=1 ; s<=mission.getNumOfSides() ; s++) {
			str.append(mission.getSideDescription(s) + "|");
		}
		for (int s=mission.getNumOfSides()+1 ; s<=4 ; s++) {
			str.append("|");
		}
		str.append(mission.getMaxTurns() + "|");
		// AI for sides
		for (int s=1 ; s<=mission.getNumOfSides() ; s++) {
			str.append(mission.doesSideHaveAI(s) + "|");
		}
		for (int s=mission.getNumOfSides()+1 ; s<=4 ; s++) {
			str.append("|");
		}
		str.append("\n");

	}


	// Called by the server
	public static String GetCreateNewGameAsResponse(MySQLConnection dbs, final String login, final String pwd, HashMap<String, String> hashmap) throws SQLException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			if (current_login.selectUser(login, pwd)) {
				int side = Functions.ParseInt(hashmap.get("side"));
				int aiside = -1;
				String opponent = "";
				try {
					opponent = AbstractCommFuncs.URLDecodeString(hashmap.get("opponent"));
					aiside = Functions.ParseInt(hashmap.get("aiside"));
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
				boolean advanced = false;
				try {
					if (hashmap.get("advanced") != null) {
						advanced = Functions.ParseInt(hashmap.get("advanced")) == 1;
					}
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
				boolean practise = false;
				try {
					if (hashmap.get("practise") != null) {
						practise = Functions.ParseInt(hashmap.get("practise")) == 1;
					}
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
				AbstractMission selected_mission = AbstractMission.Factory(Functions.ParseInt(hashmap.get("missionid")));
				if (selected_mission.isAlwaysAdvanced()) {
					advanced = true;
				}
				try {
					String ret = GameRequests.CreateGame(dbs, current_login, side, selected_mission, practise, 0, advanced, false, 100, opponent, 0, "", aiside, -1);
					if (Functions.IsNumeric(ret)) { // Was it successful?
						// Email me to tell me
						if (current_login.getTotalTurns() == 0) {
							if (selected_mission.getMissionID() != AbstractMission.SF_PRACTISE_MISSION && selected_mission.getMissionID() != AbstractMission.SF_PRACTISE_MISSION_WITH_AI && aiside <= 0) {
								DSRWebServer.SendEmailToAdmin("New player game req", "New player " + current_login.getDisplayName() + " has created a game.");
							}
						}
						int gameid = Functions.ParseInt(ret);
						// Wait for map & game to be created so it appears straight away on players list
						int mapid = MapDataTable.GetMapIDFromGameID(dbs, gameid);
						while (MapDataTable.IsFullyCreated(dbs, mapid) == false) { 
							Functions.delay(500);
						}
						return "";
					}
					return ret; // Return error
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
					return ex.getMessage();
				}
			} else {
				return "Invalid login/password";
			}
		} finally {
			current_login.close();
		}
	}


	// Called by the server
	public static String GetStartPractiseGameAsResponse(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap, boolean with_ai) throws SQLException {
		LoginsTable current_login = new LoginsTable(dbs);
		if (current_login.selectUser(login, pwd)) {
			try {
				if (with_ai == false) {
					startpractisemission.Start1PlayerPractiseMission(dbs, current_login.getID());
				} else {
					startpractisemission.Start1PlayerPractiseMissionWithAI(dbs, current_login.getID());
				}
				return "";
			} catch (Exception ex) {
				DSRWebServer.HandleError(ex);
				return ex.getMessage();
			} finally {
				current_login.close();
			}
		} else {
			current_login.close();
			return "Invalid login/password";
		}
	}


	public static String GetStartNewGameSameSides(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException, IOException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			if (current_login.selectUser(login, pwd)) {
				GamesTable game = new GamesTable(dbs);
				game.selectRow(Integer.parseInt(hashmap.get("gameid")));
				try {
					if (game.getNumOfSides() == 2) {
						if (game.isPlayerInGame(current_login.getID())) {
							int our_side = game.getSideFromPlayerID(current_login.getID());
							GameRequests.CreateGame(dbs, current_login, our_side, AbstractMission.Factory(game.getMissionID()), game.isPractise(), GameRequestsTable.OT_ANYONE, game.isAdvancedMode(), false, game.getPcentCredit(), game.getOpponentsNamesBySide(our_side, false), 0, "Rematch", -1, -1);
							return "";
						} else {
							return "You are not in that game.";
						}
					} else {
						return "Only 2 player games can be recreated.";
					}
				} finally {
					game.close();

				}
			} else {
				return "Invalid login/password";
			}
		} finally {
			current_login.close();
		}
	}



	public static String GetStartNewGameOppSides(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException, IOException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			if (current_login.selectUser(login, pwd)) {
				GamesTable game = new GamesTable(dbs);
				game.selectRow(Integer.parseInt(hashmap.get("gameid")));
				try {
					if (game.getNumOfSides() == 2) {
						if (game.isPlayerInGame(current_login.getID())) {
							int our_old_side = game.getSideFromPlayerID(current_login.getID());
							int our_new_side = game.getOppositeSidesForSide(our_old_side)[0];
							String s = GameRequests.CreateGame(dbs, current_login, our_new_side, AbstractMission.Factory(game.getMissionID()), game.isPractise(), GameRequestsTable.OT_ANYONE, game.isAdvancedMode(), false, game.getPcentCredit(), game.getOpponentsNamesBySide(our_old_side, false), 0, "Rematch", -1, -1);
							//game.close();
							if (Functions.IsNumeric(s)) {
								return "";
							} else {
								return s;
							}
						} else {
							return "You are not in that game";
						}
					} else {
						return "Only 2 player games can be recreated";
					}
				} finally {
					game.close();
				}
			} else {
				return "Invalid login/password";
			}
		} catch (RuntimeException ex) {
			throw new RuntimeException("Hashmap: " + hashmap.toString(), ex);
		} finally {
			current_login.close();
		}

	}


}

