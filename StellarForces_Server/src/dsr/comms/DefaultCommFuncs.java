package dsr.comms;


/**
 * This is the default encryption class, which basically doesn't encrypt.
 *
 */
public class DefaultCommFuncs extends AbstractCommFuncs {

	public DefaultCommFuncs() {
		
	}

	
	@Override
	public String specialEncode(String s) {
		return s;
	}
	

	@Override
	public String specialDecode(String s)  {
		return s;
	}
}
