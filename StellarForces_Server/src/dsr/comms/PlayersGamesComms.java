package dsr.comms;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.lang.Functions;
import ssmith.util.MyList;

import dsr.SharedStatics;
import dsr.data.GameData;
import dsrwebserver.ClientException;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.appletcomm.MiscCommsPage;
import dsrwebserver.tables.AbstractTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.OppFireSelectionTable;
import dsrwebserver.tables.UnitsTable;

/**
 * This is for the client to get a list of the players game.
 * 
 */
public final class PlayersGamesComms {

	// This is for data request by the applet

	// Called by the applet to get game data for all the player's current games
	public static String GetPlayersGamesRequest(String login, String pwd) {
		return "cmd=" + MiscCommsPage.PLAYERS_GAMES + "&version=" + SharedStatics.VERSION + "&getput=" + MiscCommsPage.GET + "&login=" + login + "&pwd=" + pwd + "&time=" + System.currentTimeMillis();
	}


	// Called by the applet to get game data on a specific game
	public static String GetPlayersGamesRequest(String login, String pwd, int gameid) {
		return "cmd=" + MiscCommsPage.PLAYERS_FINISHED_GAME_DATA + "&version=" + SharedStatics.VERSION + "&getput=" + MiscCommsPage.GET + "&login=" + login + "&pwd=" + pwd + "&gameid=" + gameid + "&time=" + System.currentTimeMillis();
	}


	// Called by the server
	public static String GetPlayersGamesAsResponse(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap, int selected_gameid, boolean from_android) throws SQLException, IOException {
		float version = 0f;
		try {
			version = Float.parseFloat(hashmap.get("version"));
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		LoginsTable logins = new LoginsTable(dbs);
		try {
		if (logins.selectUser(login, pwd)) {
			StringBuffer str = new StringBuffer();
			String sql = "";
			sql = "SELECT GameID FROM Games WHERE ";
			if (selected_gameid > 0) { // Have chosen a single game, probably to play back.
				sql = sql + " GameID = " + selected_gameid;
			} else {
				sql = sql + "(GameStatus = " + GamesTable.GS_CREATED_DEPLOYMENT + " OR GameStatus = " + GamesTable.GS_STARTED + ")";
				sql = sql + " AND " + AbstractTable.GetPlayerSubQuery(logins.getID());
			}

			sql = sql + " ORDER BY DateCreated DESC";
			ResultSet rs = dbs.getResultSet(sql);
			int count = 0;
			GamesTable game = new GamesTable(dbs);
			while (rs.next()) {
				game.selectRow(rs.getInt("GameID"));

				int our_side = -1;
				if (selected_gameid <= 0) { // Are we showing as current game? (as opposed to a playback)
					if (MapDataTable.IsFullyCreated(dbs, game.getMapDataID()) == false) { // Don't show if map not created yet as there will be no units for the player to equip
						continue;
					}

					our_side = game.getSideFromPlayerID(logins.getID());

					// Show games that have an AI taking a turn
					if (game.isAITurn() == false) {
						if (game.getGameStatus() == GamesTable.GS_CREATED_DEPLOYMENT && game.hasSideDeployed(our_side)) { // Are we deployed but not our opponent?
							if (game.hasSideEquipped(our_side)) {
								continue;
							}
						} else if (game.getGameStatus() == GamesTable.GS_CREATED_DEPLOYMENT && game.hasSideEquipped(our_side) == false) { // We haven't equipped yet
							// Don't show game if not Android or less than 1.86
							if (from_android == false || version < 1.86f) {
								continue;
							}
						} else if (game.getGameStatus() == GamesTable.GS_STARTED && game.getLoginIDOfWaitingForPlayer() != logins.getID()) { // Not our turn!
							continue;
						}
					}

					// Check equipment is valid for the player's version of the client
					// Nah, don't bother
					/*sql = "SELECT Count(*) FROM Equipment";
					sql = sql + " INNER JOIN EquipmentTypes ON EquipmentTypes.EquipmentTypeID = Equipment.EquipmentTypeID"; 
					sql = sql + " WHERE Equipment.GameID = " + game.getID();
					sql = sql + " AND EquipmentTypes.MinClientVersion > " + (version + 0.01f); // Add this due to rounding errors
					if (dbs.getScalarAsInt(sql) > 0) {
						continue;
					}*/

				} else { // Selected a specific game
					if (game.getGameStatus() < GamesTable.GS_FINISHED) { // Don't allow viewing of current games!
						continue;
					}				
				}

				AbstractMission mission = AbstractMission.Factory(game.getMissionID());
				count++;

				str.append("|game|" + game.getID());
				str.append("|" + game.getGameCode());
				str.append("|" + game.getGameStatus());
				str.append("|" + game.getTurnNo());
				if (game.isAITurn()) {
					str.append("|" + game.getWaitingForSide());//our_side);  Can't be "our_side" as we may be taking the turn for the AI
				} else {
					str.append("|" + our_side);
				}
				str.append("|" + game.getMissionID());
				str.append("|" + game.getTurnSide());
				str.append("|" + game.getForumID());
				str.append("|" + mission.mission_name);
				str.append("|" + SQLFuncs.b201(mission.showCeilings()));
				str.append("|" + mission.getMaxTurns());
				str.append("|" + mission.getWallsDestroyableType());
				str.append("|" + logins.getDisplayName());
				str.append("|" + game.getNumOfSides());
				for (int s=1 ; s<=4 ; s++) {
					str.append("|" + game.getNameFromSide(s));
				}
				str.append("|" + mission.getMinClientVersion());
				str.append("|" + mission.getThinThickWallType());
				for (int s=1 ; s<=4 ; s++) {
					str.append("|" + (mission.canSideSeeEnemies(s) ? 1:0) );
				}
				str.append("|" + (mission.isSnafu() ? 1:0) );
				int loginid = logins.getID();
				for (int s=1 ; s<=4 ; s++) {
					str.append("|" + (OppFireSelectionTable.IsOppFire(dbs, game.getID(), loginid, s) ? 1:0) );
				}
				str.append("|" + (game.isAdvancedMode()? 1:0) );
				for (int s=1 ; s<=4 ; s++) {
					str.append("|" + mission.getSidesForSide(s).toCSVString());
				}
				for (int s=1 ; s<=4 ; s++) {
					str.append("|" + game.getAIForSide(s));
				}
				for (int s=1 ; s<=4 ; s++) {
					str.append("|" + (game.hasSideEquipped(s) ? 1:0) );
				}
				for (int s=1 ; s<=4 ; s++) {
					str.append("|" + (game.hasSideDeployed(s) ? 1:0) );
				}
				int creds = (mission.getCreditsForSide(our_side) * game.getPcentCredit())/100;
				// If camp mission, add camp creds
				if (game.isCampGame()) {
					// Only allow 20% more
					int max_inc = (int)(creds * 0.2f);
					max_inc = Math.min(max_inc, logins.getCampCredits()); 
					creds += max_inc; //logins.getCampCredits();
				}
				str.append("|" + creds);
				for (int s=1 ; s<=4 ; s++) {
					str.append("|" + game.getVPsForSide(s));
				}
				str.append("|" + mission.getMission1Liner(our_side).replaceAll("\\<.*?\\>", ""));
				for (int s=1 ; s<=4 ; s++) {
					if (s <= game.getNumOfSides()) {
						str.append("|" + mission.getSideDescription(s).replaceAll("\\<.*?\\>", ""));
					} else {
						str.append("|."); // Need to put something otherwise it gets truncated 
					}
				}
				for (int s=1 ; s<=4 ; s++) {
					str.append("|" + UnitsTable.GetUnitsRemaining(dbs, game.getID(), s));
				}
				str.append("|0|0|0|0"); // todo -re-use
				str.append("|" + SQLFuncs.b201(game.isCampGame()));
				for (int s=1 ; s<=4 ; s++) {
					str.append("|" + (game.hasSideChosenUnits(s) ? 1:0));
				}
				for (int s=1 ; s<=4 ; s++) {
					str.append("|" + mission.getMinUnitsForSide(s));
				}
				str.append("|" + game.getDaysSinceTurnStarted());
				str.append("|" + SQLFuncs.b201(game.isPractise()));
				try {
					for (int s=1 ; s<=4 ; s++) {
						str.append("|" + mission.getMaxProtection(s));
					}
					for (int s=1 ; s<=4 ; s++) {
						str.append("|" + game.getResPointsForSide(s));
					}
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex);
				}
				str.append("|" + mission.canBuildAndDismantle());
				for (int s=1 ; s<=4 ; s++) {
					str.append("|" + mission.getMaxUnitsForSide(s));
				}

			}
			// Add the number of units to the start of the data
			str.insert(0, "qty|" + count);
			rs.close();
			game.close();

			return str.toString();
		} else {
			throw new ClientException("Invalid login/password (" + login + "/" + pwd + ")");
		}
		} finally {
			logins.close();
		}
	}


	// Called by the PC client
	public static ArrayList<GameData> DecodeGameDataResponse(String response) {
		String data[] = response.split("\\|");
		ArrayList<GameData> games = new ArrayList<GameData>();
		if (data.length > 1) {
			int pos=0;
			while (true) {
				if (data[pos].equalsIgnoreCase("qty")) {
					//int tot_games = Functions.ParseInt(data[pos+1]);
					pos += 2;
				} else if (data[pos].equalsIgnoreCase("game")) {
					GameData gamedata = new GameData();
					gamedata.game_id = Functions.ParseInt(data[pos+1]);
					gamedata.gamecode = data[pos+2];
					gamedata.game_status = Functions.ParseByte(data[pos+3]);
					gamedata.turn_no = Functions.ParseByte(data[pos+4]);
					gamedata.our_side = Byte.parseByte(data[pos+5]);
					gamedata.mission_type = Short.parseShort(data[pos+6]);
					gamedata.turn_side = Functions.ParseByte(data[pos+7]);
					gamedata.forum_id = Functions.ParseInt(data[pos+8]);
					gamedata.mission_name = data[pos+9];
					gamedata.show_ceilings = Functions.ParseByte(data[pos+10]);
					gamedata.max_turns = Functions.ParseByte(data[pos+11]);
					gamedata.wall_type = Byte.parseByte(data[pos+12]);
					gamedata.our_name = data[pos+13];
					gamedata.num_players = Functions.ParseByte(data[pos+14]);
					gamedata.players_name_by_side[1] = data[pos+15];
					gamedata.players_name_by_side[2] = data[pos+16];
					gamedata.players_name_by_side[3] = data[pos+17];
					gamedata.players_name_by_side[4] = data[pos+18];
					gamedata.min_client_version = Float.parseFloat(data[pos+19]);

					gamedata.map_model_type = Functions.ParseByte(data[pos+20]);
					gamedata.side_see_enemies[1] = Functions.ParseByte(data[pos+21]);
					gamedata.side_see_enemies[2] = Functions.ParseByte(data[pos+22]);
					gamedata.side_see_enemies[3] = Functions.ParseByte(data[pos+23]);
					gamedata.side_see_enemies[4] = Functions.ParseByte(data[pos+24]);

					gamedata.is_snafu = Functions.ParseByte(data[pos+25]);
					gamedata.opp_fire[1] = Functions.ParseByte(data[pos+26]);
					gamedata.opp_fire[2] = Functions.ParseByte(data[pos+27]);
					gamedata.opp_fire[3] = Functions.ParseByte(data[pos+28]);
					gamedata.opp_fire[4] = Functions.ParseByte(data[pos+29]);
					gamedata.is_advanced = Functions.ParseByte(data[pos+30]);

					gamedata.side_sides[1] = MyList.CreateFromCSVBytes(data[pos+31]);
					gamedata.side_sides[2] = MyList.CreateFromCSVBytes(data[pos+32]);
					gamedata.side_sides[3] = MyList.CreateFromCSVBytes(data[pos+33]);
					gamedata.side_sides[4] = MyList.CreateFromCSVBytes(data[pos+34]);

					gamedata.ai_for_side[1] = Functions.ParseByte(data[pos+35]);
					gamedata.ai_for_side[2] = Functions.ParseByte(data[pos+36]);
					gamedata.ai_for_side[3] = Functions.ParseByte(data[pos+37]);
					gamedata.ai_for_side[4] = Functions.ParseByte(data[pos+38]);

					try {
						gamedata.has_side_equipped[1] = Functions.ParseByte(data[pos+39]);
						gamedata.has_side_equipped[2] = Functions.ParseByte(data[pos+40]);
						gamedata.has_side_equipped[3] = Functions.ParseByte(data[pos+41]);
						gamedata.has_side_equipped[4] = Functions.ParseByte(data[pos+42]);

						gamedata.has_side_deployed[1] = Functions.ParseByte(data[pos+43]);
						gamedata.has_side_deployed[2] = Functions.ParseByte(data[pos+44]);
						gamedata.has_side_deployed[3] = Functions.ParseByte(data[pos+45]);
						gamedata.has_side_deployed[4] = Functions.ParseByte(data[pos+46]);

						gamedata.creds = Functions.ParseInt(data[pos+47]);

						gamedata.vps[1] = Functions.ParseByte(data[pos+48]);
						gamedata.vps[2] = Functions.ParseByte(data[pos+49]);
						gamedata.vps[3] = Functions.ParseByte(data[pos+50]);
						gamedata.vps[4] = Functions.ParseByte(data[pos+51]);

						gamedata.mission1liner = data[pos+52];

						gamedata.side_names[1] = data[pos+53];
						gamedata.side_names[2] = data[pos+54];
						gamedata.side_names[3] = data[pos+55];
						gamedata.side_names[4] = data[pos+56];

						pos += 57;
					} catch (Exception ex) {
						ex.printStackTrace();
						pos += 43;
					}

					if (SharedStatics.VERSION < gamedata.min_client_version) {
						gamedata.enabled = false;
					}

					games.add(gamedata);
				} else {
					//AppletMain.p("Ignoring game data.");
					pos++;
				}

				if (pos >= data.length) {
					break;
				}
			}
		}

		/*if (games == null) {
			throw new RuntimeException("Error decoding game data");
		} else {*/
		return games;
		//}
	}


}
