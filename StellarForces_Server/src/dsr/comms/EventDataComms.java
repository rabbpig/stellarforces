package dsr.comms;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.lang.Functions;
import ssmith.lang.Geometry;
import dsr.SharedStatics;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.appletcomm.MiscCommsPage;
import dsrwebserver.tables.CampUnitsTable;
import dsrwebserver.tables.GameLogTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;
import dsrwebserver.tables.VisibleEnemiesTable;

public class EventDataComms {

	private EventDataComms() {

	}

	//------------------------------------------------------------------------------

	// These are for updating the server from the applet

	// This is called by the applet
	public static String GetNewEventRequest(int gameid, String gamecode, int type, int unitid, int x, int z, int data1, int data2, boolean seen_by_sides[], int data3, int data4) {
		StringBuffer str = new StringBuffer();
		str.append(gameid + "|");
		str.append(gamecode + "|");
		str.append(type + "|");
		str.append(unitid + "|");
		str.append(x + "|");
		str.append(z + "|");
		str.append(data1 + "|"); // Grenade expl: Radius, unit killed: killed by, seen by side, shot angle, attacked by side, thrown angle, prime turns
		str.append(data2 + "|"); // shot length, attack by unit, throw dist, expl type
		if (seen_by_sides != null) {
			for (int i=1 ; i<seen_by_sides.length ; i++) {
				str.append(SQLFuncs.b201(seen_by_sides[i]) + "|");
			}
			// Add extra |0
			for (int i=seen_by_sides.length ; i <= 4 ; i++) {
				str.append("0|");
			}
		} else {
			str.append("0|0|0|0|");
		}
		str.append(data3 + "|"); // form of attack?
		str.append(data4 + "|"); // amount wounded?
		str.append(System.currentTimeMillis() + "|");

		return "cmd=" + MiscCommsPage.EVENT_DATA + "&version=" + SharedStatics.VERSION + "&getput=" + MiscCommsPage.PUT + "&data=" + str.toString();
	}


	// This is called by the server
	public static void DecodeNewEventRequest(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException {
		try {
			String response = hashmap.get("data");
			String data[] = response.split("\\|");

			int gameid = Functions.ParseInt(data[0]);
			String gamecode = data[1];
			int type = Functions.ParseInt(data[2]);
			int unitid = Functions.ParseInt(data[3]);
			int x = Functions.ParseInt(data[4]);
			int z = Functions.ParseInt(data[5]);
			int data1 = Functions.ParseInt(data[6]);
			int data2 = 0;
			int seen_by_side[] = new int[5];
			int data3 = 0;
			int data4 = 0;
			long event_time = 0;
			try {
				data2 = Functions.ParseInt(data[7]);
				seen_by_side[1] = Functions.ParseInt(data[8]);
				seen_by_side[2] = Functions.ParseInt(data[9]);
				seen_by_side[3] = Functions.ParseInt(data[10]);
				seen_by_side[4] = Functions.ParseInt(data[11]);
				data3 = Functions.ParseInt(data[12]);
				data4 = Functions.ParseInt(data[13]);
				event_time = Long.parseLong(data[14]);
			} catch (Exception ex) {
				DSRWebServer.p("Old version of client:-");
				ex.printStackTrace();
			}

			GamesTable game = new GamesTable(dbs);
			game.selectRow(gameid);
			if (game.getGameCode().equalsIgnoreCase(gamecode)) {
				UnitsTable unit = new UnitsTable(dbs);
				try {
					String owner = "";
					if (unitid > 0) {
						unit.selectRow(unitid);
						owner = " (" + LoginsTable.GetDisplayName_Enc(dbs, game.getLoginIDFromSide(unit.getSide()), false) + "'s #" + unit.getOrderBy() + ")";
					}

					switch (type) { 
					case UnitHistoryTable.UH_UNIT_KILLED:
						try {
							/*if (AppletMain.VERBOSE_COMMS) {
							DSRWebServer.p("Unit " + unit.getName() + " killed.");
						}*/
							UnitsTable attacker = null;
							LoginsTable attacker_owner = null;
							AbstractMission mission = AbstractMission.Factory(game.getMissionID());
							boolean turn_into_alien = false;
							if (data2 > 0) { // Do we know the attacking unit?
								attacker = new UnitsTable(dbs);
								attacker.selectRow(data2);
								attacker_owner = new LoginsTable(dbs);
								attacker_owner.selectRow(game.getLoginIDFromSide(attacker.getSide()));

								turn_into_alien = mission.aliensImpregnate();
								// Check we're not an alien but were attacked by an alien
								turn_into_alien = turn_into_alien && (unit.getModelType() != UnitsTable.MT_ALIEN_TYRANT && attacker.getModelType() == UnitsTable.MT_ALIEN_TYRANT);
								// Check it was by melee
								turn_into_alien = turn_into_alien && (data3 == UnitDataComms.FOA_MELEE);
							}

							unit.setKilled(data1, data2, game);
							UnitHistoryTable.AddRecord_UnitKilled(dbs, game, unit, seen_by_side, event_time);
							String foa_text = "";
							try {
								if (data3 == UnitDataComms.FOA_SHOT) {
									foa_text = "shot dead".toUpperCase();
								} else if (data3 == UnitDataComms.FOA_CRUSHED) {
									foa_text = "crushed to death".toUpperCase();
								} else if (data3 == UnitDataComms.FOA_MELEE) {
									foa_text = "killed in combat".toUpperCase();
								} else if (data3 == UnitDataComms.FOA_EXPLOSION) {
									foa_text = "killed in an explosion".toUpperCase();
								} else if (data3 == UnitDataComms.FOA_NERVE_GAS) {
									foa_text = "killed by nerve gas".toUpperCase();
								} else if (data3 == UnitDataComms.FOA_MERGED) {
									foa_text = "merged".toUpperCase();
								} else if (data3 == UnitDataComms.FOA_FIRE) {
									foa_text = "killed by fire".toUpperCase();
								} else {
									throw new RuntimeException("Unknown FOA: " + data3);
								}
							} catch (Exception ex) {
								DSRWebServer.HandleError(ex);
							}
							String by_unit = "";
							if (game.isAdvancedMode() == false) { // If advanced, don't say who did it!
								if (attacker != null) {
									//if (data3 != UnitDataComms.FOA_NERVE_GAS) { // Don't say if gas!
									by_unit = " by " + attacker.getName() + " (" + attacker_owner.getDisplayName() + "'s #" + attacker.getOrderBy() + ")";
									//}
								}
								if (unit.getSide() == data1) {
									if (unit.getID() == data2) {
										by_unit = by_unit + " (THEMSELVES!)";
									} else {
										by_unit = by_unit + " (THEIR OWN SIDE!)";
									}
								}
							}

							if (data3 != UnitDataComms.FOA_MERGED) { // Don't bother logging merged events
								for (int s=1 ; s<seen_by_side.length ; s++) {
									if (seen_by_side[s] == 1 || game.isAdvancedMode() == false) {
										int loginid = game.getLoginIDFromSide(s);
										int other_loginid = -1;
										if (game.isAdvancedMode() == false) {
											loginid = 0;
										}
										GameLogTable.AddRec(dbs, game, loginid, other_loginid, "Unit " + unit.getName() + owner + " HAS BEEN " + foa_text + " " + by_unit + ".", true, false, event_time);
										if (game.isAdvancedMode() == false) {
											break;
										}
									}
								}
							}
							try {
								if (game.isCampGame()) {
									if (attacker != null) {
										CampUnitsTable campunit = new CampUnitsTable(dbs) ;
										if (campunit.selectByUnitID(attacker.getID())) {
											if (unit.getSide() != attacker.getSide()) {
												// Update attacker rank
												campunit.hasKilledEnemy();
											} else {
												// Has killed their own side!
												campunit.hasKilledFriend();
												LoginsTable.IncKilledOwnUnitsCount(dbs, campunit.getOwnerID());
											}
										}
										campunit.close();
									}
								}
							} catch (Exception ex) {
								DSRWebServer.HandleError(ex, true);
							}

							if (turn_into_alien) {
								try {
									// Alien impreg-type mission - Create a new alien unit
									int loginid = 0;// Tell all
									if (game.isAdvancedMode()) { 
										loginid = game.getLoginIDFromSide(unit.getSide()); // Only tell the owner
									}
									GameLogTable.AddRec(dbs, game, loginid, attacker_owner.getID(), unit.getName() + " (" + attacker_owner.getDisplayName() + "'s #" + unit.getOrderBy() + ") has been impregnated!", true, event_time);

									UnitsTable new_alien = new UnitsTable(dbs);
									new_alien.createUnit(game.getID(), 1, UnitsTable.MT_ALIEN_TYRANT, "Alien " + unit.getName(), attacker.getSide(), unit.getMaxAPs(), unit.getMaxHealth()/2, attacker.getVPsIfKilled(), attacker.getVPsIfEscape(), 999, 0, 100, 60, 0, 999, attacker.getAIType(), 0);
									new_alien.setStatus(UnitsTable.ST_CREATED_MID_MISSION);
									new_alien.setMapXYAngle(unit.getMapX(), unit.getMapY(), unit.getAngle());
									new_alien.close();
								} catch (Exception ex) {
									DSRWebServer.HandleError(ex);
								}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						break;
					case UnitHistoryTable.UH_EXPLOSION:
						try {
							UnitHistoryTable.AddRecord_GrenadeExplosion(dbs, game, x, z, data1, data2, event_time);
							GameLogTable.AddRec(dbs, game, -1, -1, "There has been an explosion!", true, false, event_time);
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						break;
					case UnitHistoryTable.UH_WALL_DESTROYED:
						break;  // This is handled in MapDataComms
					case UnitHistoryTable.UH_SHOT_FIRED:
						// Can it be heard?
						try {
							if (game.isAdvancedMode()) {
								int range = unit.getCurrentItemNoiseRange();
								if (range > 0) {
									int heard_by_side[] = new int[5];
									String sql = "SELECT * FROM Units WHERE GameID = " + game.getID() + " AND Side <> " + unit.getSide() + " AND Status = " + UnitsTable.ST_DEPLOYED;
									ResultSet rs = dbs.getResultSet(sql);
									while (rs.next()) {
										if (seen_by_side[rs.getInt("Side")] == 1) { // don't bother telling sides that saw it fired
											continue;
										}
										if (heard_by_side[rs.getInt("Side")] == 0) { // only report to each side once
											double dist = Geometry.distance(unit.getMapX(), unit.getMapY(), rs.getInt("MapX"), rs.getInt("MapY"));
											if (dist < range) {
												VisibleEnemiesTable.AddRec(dbs, game, unit, game.getLoginIDFromSide(rs.getInt("Side")), rs.getInt("Side"), VisibleEnemiesTable.VT_HEARD_SHOOTING, event_time);
												heard_by_side[rs.getInt("Side")] = 1;
											}
										}
									}
									rs.close();
								}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex, true);
						}
						try {
							for (int s=1 ; s<seen_by_side.length ; s++) {
								if (seen_by_side[s] == 1) {
									int loginid = game.getLoginIDFromSide(s);
									GameLogTable.AddRec(dbs, game, loginid, -1, "Unit " + unit.getName() + owner + " has fired a shot.", false, false, event_time);
								}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex, true);
						}
						UnitHistoryTable.AddRecord_ShotFired(dbs, game, unit, data1, data2, seen_by_side, event_time);
						break;

					case UnitHistoryTable.UH_UNIT_WOUNDED:
						try {
							UnitHistoryTable.AddRecord_UnitWounded(dbs, game, unit, seen_by_side, event_time);
							String foa_text = "";
							try {
								if (data3 == UnitDataComms.FOA_SHOT) {
									foa_text = " shot";
								} else if (data3 == UnitDataComms.FOA_CRUSHED) {
									foa_text = " crushed";
								} else if (data3 == UnitDataComms.FOA_MELEE) {
									foa_text = " attacked";
								} else if (data3 == UnitDataComms.FOA_EXPLOSION) {
									foa_text = " caught in an explosion";
								} else if (data3 == UnitDataComms.FOA_NERVE_GAS) {
									foa_text = " caught in nerve gas";
								} else if (data3 == UnitDataComms.FOA_MERGED) {
									// Will never happen as they are "killed"
								} else if (data3 == UnitDataComms.FOA_FIRE) {
									foa_text = " caught in fire";
								} else {
									throw new RuntimeException("Unknown FOA: " + data3);
								}
							} catch (Exception ex) {
								DSRWebServer.HandleError(ex);
							}
							String by_unit = "";
							if (data3 != UnitDataComms.FOA_NERVE_GAS && data3 != UnitDataComms.FOA_FIRE) { // Don't say if gas!
								if (unit.getSide() == data1) {
									if (unit.getID() == data2) {
										by_unit = " BY THEMSELVES";
									} else {
										by_unit = " BY THEIR OWN SIDE";
									}
								} else {
									if (data2 > 0) {
										UnitsTable attacker = new UnitsTable(dbs);
										attacker.selectRow(data2);
										by_unit = "by " + attacker.getName();
										LoginsTable attacker_owner = new LoginsTable(dbs);
										attacker_owner.selectRow(game.getLoginIDFromSide(attacker.getSide()));
										by_unit = by_unit + " (" + attacker_owner.getDisplayName() + "'s #" + attacker.getOrderBy() + ")";
										attacker_owner.close();
										attacker.close();
									}
								}
							}
							for (int s=1 ; s<seen_by_side.length ; s++) {
								if (seen_by_side[s] == 1 || game.isAdvancedMode() == false) {
									int loginid = game.getLoginIDFromSide(s);
									int other_loginid = -1;
									if (game.isAdvancedMode() == false) {
										loginid = 0;
									} /*else {
									// Inform their comrade?
									try {
										AbstractMission mission = AbstractMission.Factory(game.getMissionID());
										other_loginid = game.getComradesLoginID(mission, unit.getSide());
									} catch (Exception ex) {
										DSRWebServer.HandleError(ex);
									}
								}*/
									GameLogTable.AddRec(dbs, game, loginid, other_loginid, "Unit " + unit.getName() + owner + " has been" + foa_text + " " + by_unit + " and wounded " + data4 + "!", false, false, event_time);
									if (game.isAdvancedMode() == false) {
										break;
									}
								}
							}
							// Adjust morale
							try {
								if (game.isAdvancedMode()) {
									if (data4 < unit.getHealth()) { // Don't adjust if they're going to die
										// Adjust morale for the sides
										game.adjustSidesMorale(true, unit.getSide(), -data4 / 10);
										unit.incCurrentMorale(-data4/3);
									}
								}
							} catch (Exception ex) {
								DSRWebServer.HandleError(ex);
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						break;
					case UnitHistoryTable.UH_ITEM_DROPPED:
						try {
							UnitHistoryTable.AddRecord_ItemDropped(dbs, game, unit, seen_by_side, event_time);
							for (int s=1 ; s<seen_by_side.length ; s++) {
								if (seen_by_side[s] == 1) {
									int loginid = game.getLoginIDFromSide(s);
									GameLogTable.AddRec(dbs, game, loginid, -1, "Unit " + unit.getName() + owner + " has dropped an item.", false, false, event_time);
								}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						break;
					case UnitHistoryTable.UH_ITEM_THROWN:
						try {
							UnitHistoryTable.AddRecord_ItemThrown(dbs, game, unit, data1, data2, seen_by_side, event_time); //game.getID()
							for (int s=1 ; s<seen_by_side.length ; s++) {
								if (seen_by_side[s] == 1) {
									int loginid = game.getLoginIDFromSide(s);
									GameLogTable.AddRec(dbs, game, loginid, -1, "Unit " + unit.getName() + owner + " has thrown an item.", false, false, event_time);
								}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						break;
					case UnitHistoryTable.UH_ITEM_PICKED_UP:
						try {
							//if (version >= 1.48f) {
							UnitHistoryTable.AddRecord_ItemPickedUp(dbs, game, unit, seen_by_side, event_time);
							for (int s=1 ; s<seen_by_side.length ; s++) {
								if (seen_by_side[s] == 1) {
									int loginid = game.getLoginIDFromSide(s);
									GameLogTable.AddRec(dbs, game, loginid, -1, "Unit " + unit.getName() + owner + " has picked up an item.", false, false, event_time);
								}
							}
							//}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						break;
					case UnitHistoryTable.UH_UNIT_ESCAPED:
						try {
							//if (version >= 1.48f) {
							UnitHistoryTable.AddRecord_UnitEscaped(dbs, game, unit, seen_by_side, event_time);
							for (int s=1 ; s<seen_by_side.length ; s++) {
								if (seen_by_side[s] == 1) {
									int loginid = game.getLoginIDFromSide(s);
									GameLogTable.AddRec(dbs, game, loginid, -1, "Unit " + unit.getName() + owner + " has escaped!", true, false, event_time);
								}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						break;
					case UnitHistoryTable.UH_CLOSE_COMBAT:
						try {
							UnitHistoryTable.AddRecord_CloseCombat(dbs, game, unit, seen_by_side, event_time);
							for (int s=1 ; s<seen_by_side.length ; s++) {
								if (seen_by_side[s] == 1 || game.isAdvancedMode() == false) {
									int loginid = game.getLoginIDFromSide(s);
									if (game.isAdvancedMode() == false) {
										loginid = 0;
									}
									GameLogTable.AddRec(dbs, game, loginid, -1, "Unit " + unit.getName() + owner + " has engaged in close combat.", false, false, event_time);
									if (game.isAdvancedMode() == false) {
										break;
									}
								}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						break;
					case UnitHistoryTable.UH_UNIT_MOVEMENT:
						try {
							UnitHistoryTable.AddRecord_UnitMovement(dbs, game, unit, seen_by_side, event_time);
							// Can opponent hear them? (only if they moved, not turned)
							if (game.isAdvancedMode()) {
								if (unit.isSilent() == 0) {
									int heard_by_side[] = new int[5];
									AbstractMission mission = AbstractMission.Factory(game.getMissionID());
									String sql = "SELECT * FROM Units WHERE GameID = " + game.getID() + " AND Side NOT IN (" + mission.getSidesForSide(unit.getSide()).toCSVString() + ") AND Status = " + UnitsTable.ST_DEPLOYED;
									ResultSet rs = dbs.getResultSet(sql);
									while (rs.next()) {
										if (seen_by_side[rs.getInt("Side")] == 1) { // don't bother telling sides that actually saw it
											continue;
										}
										if (heard_by_side[rs.getInt("Side")] == 0) { // only report to each side once
											int hear_dist = UnitDataComms.HEAR_ENEMY_DISTANCE;
											if (rs.getInt("ModelType") == UnitsTable.MT_QUEEN_ALIEN) {
												hear_dist = UnitDataComms.QUEEN_ALIEN_HEAR_DIST;
											}	
											double dist = Geometry.distance(unit.getMapX(), unit.getMapY(), rs.getInt("MapX"), rs.getInt("MapY"));
											if ((int)dist <= (int)hear_dist) {
												VisibleEnemiesTable.AddRec(dbs, game, unit, game.getLoginIDFromSide(rs.getInt("side")), rs.getInt("Side"), VisibleEnemiesTable.VT_HEARD_MOVING, event_time);
												heard_by_side[rs.getInt("Side")] = 1;
											}
										}
									}
									rs.close();
								}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						break;
					case UnitHistoryTable.UH_DOOR_OPENED:
						// This is handled in MapDataComms
						break;
					case UnitHistoryTable.UH_DOOR_CLOSED:
						// This is handled in MapDataComms
						break;
					case UnitHistoryTable.UH_GRENADE_PRIMED:
						try {
							UnitHistoryTable.AddRecord_GrenadePrimed(dbs, game, unit, data1, seen_by_side, event_time);
							for (int s=1 ; s<seen_by_side.length ; s++) {
								if (seen_by_side[s] == 1) {
									int loginid = game.getLoginIDFromSide(s);
									GameLogTable.AddRec(dbs, game, loginid, -1, "Unit " + unit.getName() + owner + " has primed a grenade for " + data1 + " turn(s).", false, false, event_time);
								}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						break;
					case UnitHistoryTable.UH_BLOB_SPLIT:
						try {
							UnitHistoryTable.AddRecord_BlobSplit(dbs, game, unit, seen_by_side, event_time);

							// Create new unit
							UnitsTable new_unit = new UnitsTable(dbs);
							/*int order_by = 1;
						try {
							while (true) {
								if (dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + unit.getSide() + " And OrderBy = " + order_by) == 0) {
									break;
								}
								order_by++;
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}*/
							int order_by = dbs.getScalarAsInt("SELECT MAX(OrderBy) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + unit.getSide())+1;
							new_unit.createUnit(game.getID(), order_by, unit.getModelType(), unit.getName(), unit.getSide(), unit.getMaxAPs(), unit.getHealth(), unit.getVPsIfKilled(), unit.getVPsIfEscape(), unit.getMaxEnergy(), unit.getShotSkill(), unit.getCombatSkill(), unit.getStrength(), 0, unit.getMaxMorale(), unit.getAIType(), 0);
							new_unit.setMapXYAngle(data1, data2, unit.getAngle());
							new_unit.setStatus(UnitsTable.ST_DEPLOYED);
							new_unit.close();

						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						break;
						//case UnitHistoryTable.UH_UNIT_TELEPORTED:
					case UnitHistoryTable.UH_MAPSQUARE_CHANGED:
						try {
							UnitHistoryTable.AddRecord_MapsquareChanged(dbs, game, x, z, data1, data2, event_time);
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						break;
					default:
						throw new RuntimeException("Unknown event type: " + type);
					}
				} finally {
					unit.close();
				}
			} else {
				// Do nothing
			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex, true);
		}
	}


}
