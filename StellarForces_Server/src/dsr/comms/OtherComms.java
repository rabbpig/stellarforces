package dsr.comms;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.lang.Functions;
import dsrwebserver.DSRWebServer;
import dsrwebserver.pages.dsr.leaguetable;
import dsrwebserver.pages.dsr.starmap;
import dsrwebserver.pages.dsr.startpractisemission;
import dsrwebserver.tables.CampUnitsTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.StarmapSquaresTable;


public class OtherComms {

	public static String AutoRegister(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException, IOException {
		String displayname = "Player " + CampUnitsTable.GetRandomName();
		// Check it's unique
		while (dbs.getScalarAsInt("SELECT Count(*) FROM Logins WHERE DisplayName = " + SQLFuncs.s2sql(displayname)) > 0) {
			displayname = displayname + Functions.rnd(1, 9);
		}
		String pwd = ""+Functions.rnd(10000, 99999);
		LoginsTable login = new LoginsTable(dbs);
		try {
			String res = login.createLogin(pwd, displayname, "", true);
			if (res.length() == 0) {
				StringBuffer str = new StringBuffer();
				str.append("Login|Pwd\n");
				str.append(login.getDisplayName() + "|" + login.getPassword() + "\n");
				//DSRWebServer.SendEmailToAdmin("Player autoregistered", login.getDisplayName() + "/" + login.getPassword());
				startpractisemission.Start1PlayerPractiseMissionWithAI(dbs, login.getID());
				return str.toString();
			} else {
				return res;
			}
		} finally {
			login.close();
		}
	}


	public static String RespondToServerMsg(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException {
		try {
			int app_version = Functions.ParseInt(AbstractCommFuncs.URLDecodeString(hashmap.get("app_version")));
			boolean full = AbstractCommFuncs.URLDecodeString(hashmap.get("full")).equalsIgnoreCase("1");
			if ((full && app_version < DSRWebServer.CURR_FULL_APP_VERSION) || (full == false && app_version < DSRWebServer.CURR_FREE_APP_VERSION)) {
				return "There is a newer version available - Please Upgrade!";
			} else {
				if (leaguetable.top_player.length() > 0) {
					return "The current top player is " + leaguetable.top_player + "!";
				}
			}
		} catch (Exception ex) {
			//DSRWebServer.HandleError(ex);
		}
		return GetRandomMsg(dbs);
	}


	private static String GetRandomMsg(MySQLConnection dbs) throws SQLException {
		try {
			Calendar c = Calendar.getInstance();
			/*if (c.get(Calendar.MONTH) == Calendar.APRIL && c.get(Calendar.DAY_OF_MONTH) == 1) {
				// April fool's messages
				switch (Functions.rnd(1, 6)) {
				case 2: return "Send more money!";
				case 3: return "Message from Nigeria: We have your money";
				case 4: return "Formatting drive C:...";
				}
			} else {*/
				switch (Functions.rnd(1, 17)) {
				case 1: return "Please leave a rating if you haven't already!";
				case 2: return "If you enjoy this game, try 'Sorcerers'!";
				case 3: return "Visit the website for more settings and information";
				case 4: return "What's that smell?";
				case 5: return "Hello to Jason Isaacs";
				case 6: return "Visit Marsec for all your weaponry needs";
				case 7: return "If you can find them, maybe you can hire the Laser Squad";
				case 8: return "INSERT COIN!";
				case 9: return "It's like Chess with grenades";
				case 10: return "Warning: Contains nuts";
				case 11: return "All your base are belong to us";
				case 12: return "It's not a bug, it's a feature";
				case 13: return "Not available in the shops";
				case 14: return "This game belongs to ______________";
				case 15: return  dbs.getScalarAsInt("SELECT Count(*) FROM Logins") + " registered users!";
				case 16: return  dbs.getScalarAsInt("SELECT Count(*) FROM Games") + " games played!";

				default: return "Your login and password will work in the forums";
				}
			//}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
			return "";
		}
	}


	public static String GetLeagueTable(MySQLConnection dbs, String login) throws SQLException, UnsupportedEncodingException {
		StringBuffer str = new StringBuffer();

		str.append("Pos|Name|Played|Won|Drawn|Lost|Points\n");

		ResultSet rs = dbs.getResultSet("SELECT * FROM LeagueTable ORDER BY Points DESC LIMIT 20");
		int i=1;
		while (rs.next()) {
			String display_name = LoginsTable.GetDisplayName(dbs, rs.getInt("LoginID"));
			str.append(i + "|" + display_name + "|" + rs.getInt("Played") + "|" + rs.getInt("Won") + "|" + rs.getInt("Drawn") + "|" + rs.getInt("Lost") + "|" + rs.getInt("Points") + "\n");
			i++;
		}
		rs.close();
		return str.toString(); 
	}


	public static String GetPlayersData(MySQLConnection dbs, String login, String pwd) throws SQLException, UnsupportedEncodingException {
		LoginsTable logintbl = new LoginsTable(dbs);
		try {
			if (logintbl.selectUser(login, pwd)) {
				StringBuffer str = new StringBuffer();

				str.append("LoginID|DisplayName|Email|FactionID|CanJoinFaction|CanInvade|SectorsUnderAttack\n");

				int can_invade = 0; // todo - use this in client
				int sectors_under_attack = StarmapSquaresTable.GetNumFactionsUnderAttack(dbs, logintbl.getFactionID());// todo - use this in client
				if (logintbl.getFactionID() > 0) {
					if (starmap.canPlayerAttack(dbs, logintbl)) {
						can_invade = 1;
					}
				}
				str.append(logintbl.getID() + "|" + logintbl.getDisplayName() + "|" + logintbl.getEmail() + "|" + logintbl.getFactionID() + "|" + logintbl.canPlayInCampaign() + "|" + can_invade + "|" + sectors_under_attack);

				return str.toString(); 
			} else {
				throw new RuntimeException("Player " + login + "/" + pwd + " not found.");
			}
		} finally {
			logintbl.close();
		}
	}




}
