package dsr.comms;

import java.applet.Applet;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Properties;

import ssmith.io.IOFunctions;
import ssmith.io.TextFile;
//import dsr.AppletMain;
import dsr.IDisplayMessages;
import dsr.SharedStatics;
import dsr.start.StartupNew;
import dsrwebserver.HTTPHeaders;

public final class WGet4Client extends Authenticator {

	private static final String PROXY_FILENAME = "proxy.txt";

	public static int T_STD = 0; // Default MiscDataComms
	public static int T_SPECIFIED = 1; // URL is specifically specified
	public static int T_PLAYBACK = 2; // It's called by the applet!

	private static final int MAX_TRIES = 6;
	private static final int TIMEOUT = 1000 * 60 * 2;

	private String s_url;
	private String text_response = "";
	private byte[] data_response;
	private int response_code;
	private String redirect_to = "";
	private int tries_remaining = MAX_TRIES;
	private IDisplayMessages show_msg;

	private static boolean checked_for_proxy = false;
	private static String login, pwd, proxy, port;


	/**
	 * Call this for playback data
	 */
	public WGet4Client(Applet app, String _post_data, boolean _encode) throws UnknownHostException, IOException {
		this(null, T_PLAYBACK, app.getDocumentBase().getHost().length() > 0 ? "http://" + app.getDocumentBase().getHost() + "/appletcomm/PlaybackComms.cls" : "http://127.0.0.1:8083/appletcomm/PlaybackComms.cls", _post_data, _encode);
	}


	/**
	 * Call this for std comms data
	 */
	public WGet4Client(IDisplayMessages _owner, String _post_data, boolean _encode) throws UnknownHostException, IOException {
		this(_owner, T_STD, null, _post_data, _encode);
	}


	/**
	 * Call this for anything else data
	 */
	public WGet4Client(IDisplayMessages _owner, int type, String full_url, String _post_data, boolean _encode) throws UnknownHostException, IOException {
		super();

		show_msg = _owner;
		
		System.out.println("Req:" + full_url);
	
		if (checked_for_proxy == false && type != T_PLAYBACK) { // Don't use for playback!
			checked_for_proxy = true;
			try {
				if (new File(PROXY_FILENAME).canRead()) {
					TextFile tf = new TextFile();
					tf.openFile(PROXY_FILENAME, TextFile.READ);
					proxy = tf.readLine();
					port = tf.readLine();
					login = tf.readLine();
					pwd = tf.readLine();
					tf.close();

					if (show_msg != null) {
						show_msg.displayMessage("Using Proxy " + proxy + ":" + port);
					}
				} else {
					if (show_msg != null) {
						//show_msg.displayMessage("Not using a proxy server.");
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				//AppletMain.HandleError(null, ex);
			}
		}

		// Encode data?
		String post_data = "";
		if (_post_data.length() > 0) {
			if (_encode) {
				String b64 = AbstractCommFuncs.GetCommFuncs().specialEncode(_post_data);
				post_data = "post=" + HTTPHeaders.URLEncodeString(b64);
			} else {
				post_data = "post=" + HTTPHeaders.URLEncodeString(_post_data);
			}
		}

		if (type == T_STD) {
			s_url = SharedStatics.URL_FOR_CLIENT + "/appletcomm/MiscCommsPage.cls";
		} else if (type == T_SPECIFIED || type == T_PLAYBACK) {
			s_url = full_url;
		}


		while (tries_remaining > 0) { // In case of re-trying
			tries_remaining--;
			try {
				while (true) { // In case of redirects
					this.post(s_url, post_data);
					if (this.response_code == 302 && this.redirect_to.length() > 0) {
						// Redirect ourselves
						this.s_url = redirect_to;
					} else if (response_code == 200) {
						if (tries_remaining+1 < MAX_TRIES) { // Show text to say we've connected after earlier failure
							displayMessage("* " + StartupNew.strings.getTranslation("Connected to server"));
						}
						tries_remaining = 0; // So we drop out
						break;
					} else if (response_code == 500) {
						throw new RuntimeException("Server error");
					} else {
						throw new RuntimeException(""+this.response_code);
					}
				}
			} catch (java.net.ConnectException ex) { 
				//AppletMain.HandleError(null, ex, false);
				displayMessage(" *" + StartupNew.strings.getTranslation("Failed to connect to server.") + " *");
				// Loop round
			} catch (java.io.FileNotFoundException ex) {
				//AppletMain.HandleError(null, ex, false);
				displayMessage(StartupNew.strings.getTranslation("Comms error: " + ex.toString()));
				break; // No point re-trying
			} catch (IOException ex) { 
				//AppletMain.HandleError(null, ex, false);
				displayMessage(StartupNew.strings.getTranslation("Comms error: " + ex.toString()));
				// Loop round
			}
			if (tries_remaining > 0) {
				displayMessage("* " + StartupNew.strings.getTranslation("Retrying") + "...");
			}
			// loop around again
		}
		if (getResponseCode() != 200) {
			if (getResponseCode() > 0) {
				throw new IOException("Got response " + getResponseCode() + " from server for " + s_url);
			} else { // No response
				throw new IOException("Could not connect to server.");
			}
		}
	}


	private void displayMessage(String s) {
		if (this.show_msg != null) {
			this.show_msg.displayMessage(s);
		}
	}


	public int getResponseCode() {
		return this.response_code;
	}


	public String getResponse() {
		return text_response.toString();
	}


	public byte[] getDataResponse() {
		if (data_response == null && this.text_response != null) {
			// What we thought was a data file is in fact text, so convert it
			data_response = this.text_response.getBytes();
		}
		return data_response;
	}


	private void post(String server, String post_data) throws IOException {
		URL url = new URL(server.replaceAll("\\\\", "/")); 

		if (proxy != null) {
			Authenticator.setDefault(this);
			Properties systemProperties = System.getProperties();
			systemProperties.setProperty("http.proxySet", "true" );
			systemProperties.setProperty("http.proxyHost", proxy);
			systemProperties.setProperty("http.proxyPort", port);
		}

		HttpURLConnection urlConn = (HttpURLConnection)url.openConnection(); 
		urlConn.setDoInput(true); 
		urlConn.setDoOutput(true); 
		urlConn.setUseCaches(false);
		if (post_data != null) {
			if (post_data.length() > 0) {
				urlConn.setRequestProperty ("Content-Type", "application/x-www-form-urlencoded");
				urlConn.setRequestProperty ("Content-Length", ""+post_data.length());

				DataOutputStream dos = new DataOutputStream(urlConn.getOutputStream()); 
				dos.writeBytes(post_data);
				dos.writeBytes("\r\n");

				dos.flush(); 
				dos.close();
			}
		}

		response_code = urlConn.getResponseCode();
		DataInputStream dis = new DataInputStream(urlConn.getInputStream());
		IOFunctions.WaitForData(dis, TIMEOUT);
		// Check if it's data rather than text
		if (urlConn.getContentType().startsWith("text")) {
			String s = ""; 
			StringBuffer str = new StringBuffer();
			while ((s = dis.readLine()) != null) { 
				str.append(s + "\n");
			}
			// remove last \n
			if (str.length() > 0) {
				str.delete(str.length()-1, str.length());
			}
			text_response = str.toString();
		} else {
			int bytes_remaining = urlConn.getContentLength();
			int bytes_read = 0;
			data_response = new byte[urlConn.getContentLength()]; // data_response.length
			while (bytes_remaining > 0) {
				try {
					int len = dis.read(data_response, bytes_read, bytes_remaining);
					if (len < 0) {
						break;
					}
					bytes_read += len;
					bytes_remaining -= len;
				} catch (java.lang.IndexOutOfBoundsException ex) {
					ex.printStackTrace();
				}
			}
		}
		dis.close(); 
	}


	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(login, pwd.toCharArray());
	}

}

