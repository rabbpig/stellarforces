package dsr.comms;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import dsrwebserver.tables.GamesTable;

public class SetStatComms {

	// Update Types
	public static final int STAT_RES_POINTS = 1;
	// ## IF YOU ADD ANY HERE, ADD TO THE CLIENT! # !!

	public static String SetStat(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException, UnsupportedEncodingException {
		int gameid = Integer.parseInt(hashmap.get("gameid"));
		String gamecode = hashmap.get("gc");
		int side = Integer.parseInt(hashmap.get("side"));
		int type = Integer.parseInt(hashmap.get("type"));
		int amt = Integer.parseInt(hashmap.get("amt"));

		GamesTable game = new GamesTable(dbs);
		try {
		game.selectRow(gameid);
		if (game.getGameCode().equalsIgnoreCase(gamecode)) {
			if (game.getGameStatus() != GamesTable.GS_FINISHED) {
				if (type == STAT_RES_POINTS) {
					game.setResourcePointsForSide(side, amt);
				} else {
					throw new RuntimeException("Unknown stat: " + type);
				}
				return "";
			} else {
				throw new RuntimeException("Invalid game");
			}
		} else {
			throw new RuntimeException("Invalid gamecode");
		}
		} finally {
			game.close();
		}

	}


}

