package dsr.comms;

import java.io.UnsupportedEncodingException;

/**
 * To encrypt the comms between the server and the client, you must create a class called "CommFuncs.java" in the same directory as this class, and extending
 * this class.  You must also do the same in the server code.  If you don't do this, the software will default to "no encryption".
 * 
 * The encoding functions in this class must work with the encoding functions in the server, otherwise the client and
 * server won't be able to understand each other.
 * 
 */
public abstract class AbstractCommFuncs {

	protected AbstractCommFuncs() {

	}


	private static AbstractCommFuncs comm_funcs;

	public abstract String specialEncode(String s);

	public abstract String specialDecode(String s);

	public static AbstractCommFuncs GetCommFuncs() {
		if (comm_funcs == null) {
			try {
				comm_funcs = (DefaultCommFuncs)Class.forName("dsr.comms.CommFuncs").newInstance();
			} catch (Exception e) {
				e.printStackTrace();
				comm_funcs = new DefaultCommFuncs();
				System.out.println("Warning: using default Comm Funcs");
			}
		}
		return comm_funcs;
	}

	
	public static String URLEncodeString(String s) {
		if (s != null && s.length() > 0) {
			try {
				return java.net.URLEncoder.encode(s, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return s;
	}


	public static String URLDecodeString(String s) throws UnsupportedEncodingException {
		if (s != null && s.length() > 0) {
			try {
				return java.net.URLDecoder.decode(s, "UTF-8");
			} catch (java.lang.IllegalArgumentException ex) {
				ex.printStackTrace();
				System.err.println("String was '" + s + "'");
			}
		}
		return s;
	}


    /*private static char toHex(int ch) {
        return (char) (ch < 10 ? '0' + ch : 'A' + ch - 10);
    }*/

    
   /* private static boolean isUnsafe(char ch) {
        if (ch > 128 || ch < 0) {
            return true;
        }
        return " %$&+,/:;=?@<>#%".indexOf(ch) >= 0;
    }*/
}
