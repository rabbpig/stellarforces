package dsr.comms;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.AppletMain;
import dsr.SharedStatics;
import dsr.data.OppFireTypes;
import dsr.data.UnitData;
import dsrwebserver.DSRWebServer;
import dsrwebserver.HTTPHeaders;
import dsrwebserver.functions.ServerUnitData;
import dsrwebserver.pages.appletcomm.MiscCommsPage;
import dsrwebserver.tables.ArmourTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public final class UnitDataComms {

	public static final int HEAR_ENEMY_DISTANCE = 3;
	public static final int HEAR_DOOR_DISTANCE = 3;
	public static final int QUEEN_ALIEN_HEAR_DIST = 10;

	// Forms of attack
	public static final int FOA_SHOT = 1;
	public static final int FOA_MELEE = 2;
	public static final int FOA_EXPLOSION = 3;
	public static final int FOA_CRUSHED = 4; // As in Gladiator mission
	public static final int FOA_NERVE_GAS = 5;
	public static final int FOA_MERGED = 6;
	public static final int FOA_FIRE = 7;
	//# If you add any here, add them to UnitDataComms on the client!!  ##

	// These are for updating the applet from the server

	public static String GetUnitDataRequest_ORIG(int gameid, String gamecode, int loginid, String logincode) {
		return "cmd=" + MiscCommsPage.UNIT_DATA + "&version=" + SharedStatics.VERSION + "&getput=" + MiscCommsPage.GET + "&gid=" + gameid + "&gc=" + gamecode + "&loginid=" + loginid + "&logincode=" + logincode;
	}


	public static String GetUnitDataRequest(int gameid, String gamecode, String login, String pwd) {
		return "cmd=" + MiscCommsPage.UNIT_DATA + "&version=" + SharedStatics.VERSION + "&getput=" + MiscCommsPage.GET + "&gid=" + gameid + "&gc=" + gamecode + "&login=" + login + "&pwd=" + pwd;
	}


	// Called by the server
	public static String GetUnitDataAsResponse(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException {
		int gameid = Integer.parseInt(hashmap.get("gid"));
		String gamecode = hashmap.get("gc");

		StringBuffer str = new StringBuffer();

		GamesTable game = new GamesTable(dbs);
		try {
			game.selectRow(gameid);
			if (game.getGameCode().equalsIgnoreCase(gamecode)) {
				// Loop through units!
				String sql = "SELECT * FROM Units ";
				sql = sql + " WHERE GameID = " + gameid;
				sql = sql + " ORDER BY OrderBy";

				ResultSet rs = dbs.getResultSet(sql);
				int count = 0;
				UnitsTable unit = new UnitsTable(dbs);
				while (rs.next()) {
					count++;
					unit.selectRow(rs.getInt("UnitID"));
					str.append("|unit|" + rs.getInt("UnitID") + "|" + rs.getString("UnitCode") + "|" + rs.getString("Name") + "|" + rs.getInt("Side") + "|" + rs.getInt("Status"));
					str.append("|" + rs.getInt("MapX") + "|" + rs.getInt("MapY") + "|" + rs.getInt("Angle") + "|" + rs.getInt("ModelType") + "|" + rs.getInt("CurrentAPs") + "|" + rs.getInt("MaxAPs"));
					str.append("|" + rs.getInt("CurrentEquipmentID"));
					str.append("|" + ArmourTypesTable.GetProtection(dbs, rs.getInt("ArmourTypeID")));
					str.append("|" + rs.getInt("OppFire") + "|" + rs.getInt("ShotSkill") + "|" + rs.getInt("CombatSkill"));
					str.append("|" + rs.getInt("CanUseEquipment") + "|" + rs.getInt("Strength") + "|" + rs.getInt("Health"));
					str.append("|" + rs.getInt("OppFireAPsReq") + "|" + rs.getInt("MaxHealth") + "|" + rs.getInt("OrderBy"));
					str.append("|" + rs.getInt("OppFireType") + "|" + rs.getInt("CurrentMorale") + "|" + rs.getInt("CurrentEnergy") + "|" + rs.getInt("Panicked"));
					str.append("|" + rs.getInt("ArmourTypeID") + "|" + rs.getInt("CanDeploy") + "|0");
					str.append("|0|" + rs.getInt("AIType") + "|" + rs.getInt("UnitType") + "|" + rs.getInt("OnFire") + "|" + rs.getInt("CanEquip"));
					str.append("|" + rs.getInt("SkillID") + "|");
				}
				// Add the number of units to the start of the data
				str.insert(0, "qty|" + count);
				rs.close();
				unit.close();

				return str.toString();
			} else {
				throw new RuntimeException("Invalid game");
			}
		} finally {
			game.close();

		}
	}


	// This is called by the client to decode unit data
	public static UnitData[] DecodeUnitDataResponse(String response) throws IOException {
		response = HTTPHeaders.URLDecodeString(response);
		String data[] = response.split("\\|");
		int pos=0;
		int num = 0; // Must start from 0!
		UnitData units[] = null;
		while (true) {
			//main.addToHUD("Processing data: " + pos);
			//AppletMain.ps(".");//data=" + data[pos]);
			if (data[pos].equalsIgnoreCase("qty")) {
				int u = Functions.ParseInt(data[pos+1]);
				units = new UnitData[u];
				pos += 2;
			} else if (data[pos].equalsIgnoreCase("unit")) {
				units[num] = new UnitData(num);
				units[num].unitid = Functions.ParseInt(data[pos+1]);
				units[num].unitcode = data[pos+2];
				units[num].name = data[pos+3];
				units[num].setSide(Functions.ParseByte(data[pos+4]));
				units[num].setStatus(Functions.ParseByte(data[pos+5]));
				byte x = Functions.ParseByte(data[pos+6]);
				byte z = Functions.ParseByte(data[pos+7]);
				units[num].setAngle(Functions.ParseInt(data[pos+8]));
				units[num].model_type = Functions.ParseByte(data[pos+9]);
				units[num].setAPs(Functions.ParseByte(data[pos+10]));
				units[num].max_aps = Functions.ParseShort(data[pos+11]);
				units[num].current_item_id_TMP = Functions.ParseInt(data[pos+12]);
				units[num].protection = Functions.ParseShort(data[pos+13]);
				units[num].opp_fire_01 = Functions.ParseByte(data[pos+14]);
				units[num].shot_skill = Functions.ParseShort(data[pos+15]);
				units[num].combat_skill = Functions.ParseShort(data[pos+16]);
				units[num].can_use_equipment = Functions.ParseInt(data[pos+17]) == 1;
				units[num].strength = Functions.ParseShort(data[pos+18]);
				units[num].setHealth(Functions.ParseShort(data[pos+19]));
				units[num].opp_fire_aps_req = Functions.ParseShort(data[pos+20]);
				units[num].max_health = Functions.ParseShort(data[pos+21]);
				units[num].order_by = Functions.ParseByte(data[pos+22]);
				try {
					units[num].opp_fire_type = Functions.ParseByte(data[pos+23]);
					units[num].curr_morale = Functions.ParseShort(data[pos+24]);
					units[num].curr_energy = Functions.ParseShort(data[pos+25]);
					units[num].panicked = Functions.ParseByte(data[pos+26]);
					units[num].armour_type_id = Functions.ParseShort(data[pos+27]);
					units[num].can_deploy = Functions.ParseByte(data[pos+28]);
					// unusued in PC client units[num].shot_damage = NumberFunctions.ParseByte(data[pos+29]);
					// unusued in PC client units[num].power_points = NumberFunctions.ParseByte(data[pos+30]);
					// unusued in PC client units[num].setAIType(NumberFunctions.ParseInt(data[pos+31]));

					pos += 29;
				} catch (Exception ex) {
					pos += 23;
				}

				units[num].setMapLocation(null, x, z, null);

				num++;
			} else {
				//throw new RuntimeException("Unknown data type");
				AppletMain.p("Ignoring unit data.");
				pos++;
			}
			if (pos >= data.length) {
				break;
			}
		}
		if (units == null) {
			throw new RuntimeException("Error decoding unit data");
		} else {
			return units;
		}
	}


	//------------------------------------------------------------------------------

	// These are for updating the server from the applet

	// This is called by the client
	public static String GetUnitUpdateRequest(UnitData unit) {
		StringBuffer str = new StringBuffer();
		str.append(unit.unitid + "|");
		str.append(unit.unitcode + "|");
		str.append(unit.getMapX() + "|");
		str.append(unit.getMapZ() + "|");
		str.append(unit.getAngle() + "|");
		str.append(unit.getStatus() + "|");
		if (unit.current_item != null) {
			str.append(unit.current_item.equip_id + "|");
		} else {
			str.append("0|");
		}
		str.append(unit.getAPs() + "|");
		str.append(unit.getHealth() + "|");
		str.append(unit.opp_fire_type + "|");
		str.append(System.currentTimeMillis() + "|");
		str.append(unit.getMaxHealth() + "|");
		str.append(unit.combat_skill + "|");
		str.append(unit.strength + "|");
		str.append(unit.armour_type_id + "|");

		return "cmd=" + MiscCommsPage.UNIT_DATA + "&version=" + SharedStatics.VERSION + "&getput=" + MiscCommsPage.PUT + "&data=" + str.toString();
	}


	// This is called by the server to decode data sent by client
	public static void DecodeUnitUpdateRequest(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException {
		String response = hashmap.get("data");
		String data[] = response.split("\\|");

		ServerUnitData svr_unit = new ServerUnitData();

		svr_unit.unitid = Functions.ParseInt(data[0]);
		svr_unit.unitcode = data[1];
		svr_unit.x = Functions.ParseInt(data[2]);
		svr_unit.z = Functions.ParseInt(data[3]);
		svr_unit.angle = Functions.ParseInt(data[4]);
		svr_unit.new_status = Functions.ParseInt(data[5]);
		svr_unit.curr_item = Functions.ParseInt(data[6]);
		svr_unit.aps = Functions.ParseInt(data[7]);
		svr_unit.health = Functions.ParseInt(data[8]);
		int opp_fire_type = OppFireTypes.SNAP_SHOT; // Default
		long event_time = 0;
		try {
			opp_fire_type = Functions.ParseInt(data[9]);
			event_time = Long.parseLong(data[10]);
			svr_unit.max_health = Functions.ParseInt(data[11]);
			svr_unit.combat_skill = Functions.ParseInt(data[12]);
			svr_unit.strength = Functions.ParseInt(data[13]);
			if (data.length > 14 && data[14].length() > 0) {
				svr_unit.armour_type_id = Functions.ParseInt(data[14]);
			} else {
				// No armour data, so don't change anything!
				svr_unit.armour_type_id = -1;
			}
			svr_unit.shot_skill = Functions.ParseInt(data[15]);
			//svr_unit.shot_damage = Functions.ParseInt(data[16]);
			//svr_unit.protection = Functions.ParseInt(data[17]);
			//svr_unit.power_points = Functions.ParseInt(data[18]);
			svr_unit.on_fire = Functions.ParseInt(data[19]);
		} catch (Exception ex) {
			//ex.printStackTrace();
		}

		UnitsTable unit = new UnitsTable(dbs);
		try {
			unit.selectRow(svr_unit.unitid); // unit.getName()

			if (unit.getUnitCode().equalsIgnoreCase(svr_unit.unitcode)) {
				GamesTable game = new GamesTable(dbs);
				game.selectRow(unit.getGameID());
				//LoginsTable login = new LoginsTable(dbs);
				//login.selectRow(game.getLoginIDFromSide(unit.getSide()));

				int orig_status = unit.getStatus();

				UnitsTable.Update(dbs, svr_unit.unitid, svr_unit.unitcode, svr_unit.x, svr_unit.z, svr_unit.angle, svr_unit.new_status, svr_unit.curr_item, svr_unit.aps, svr_unit.health, opp_fire_type, svr_unit.max_health, svr_unit.combat_skill, svr_unit.strength, svr_unit.armour_type_id, svr_unit.shot_skill, svr_unit.on_fire);
				unit.refreshData();

				if (orig_status == UnitsTable.ST_AWAITING_DEPLOYMENT) {
					int seen_by_side[] = {0, 0, 0, 0, 0};
					seen_by_side[unit.getSide()] = 1; // Only owner side knows about it
					UnitHistoryTable.AddRecord_UnitDeployed(dbs, game, unit, seen_by_side, event_time);
				} else if (orig_status == UnitsTable.ST_DEAD) {
					try {
						if (svr_unit.new_status != UnitsTable.ST_DEAD && unit.getModelType() != UnitsTable.MT_ZOMBIE) {
							//DSRWebServer.SendEmailToAdmin("Unit resurrected!", "Unit " + unit.getOrderBy() + " on side " + unit.getSide() + " in game " + unit.getGameID() + " has been ressurrected!");
							unit.setStatus(UnitsTable.ST_DEAD);
						}
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex);
					}
				}
			} else {
				throw new RuntimeException("Error decoding unit data: " + unit.getUnitCode() + " != " + svr_unit.unitcode);
			}
		} finally {
			unit.close();
		}
	}


}
