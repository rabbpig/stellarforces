package dsr.comms;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;

import dsr.SharedStatics;
import dsr.data.UnitData;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.appletcomm.MiscCommsPage;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.UnitsTable;
import dsrwebserver.tables.VisibleEnemiesTable;

public class VisibleEnemyComms {

	// These are for updating the server from the applet

	// This is called by the applet
	public static String GetVisibleEnemyRequest(UnitData enemy, UnitData seen_by) {
		StringBuffer str = new StringBuffer();
		str.append(enemy.unitid + "|");
		str.append(enemy.unitcode + "|");
		str.append(seen_by.unitid + "|");
		str.append(seen_by.unitcode + "|");
		str.append(System.currentTimeMillis() + "|");

		return "cmd=" + MiscCommsPage.VISIBLE_ENEMY_DATA + "&version=" + SharedStatics.VERSION + "&getput=" + MiscCommsPage.PUT + "&data=" + str.toString();
	}


	// This is called by the server
	public static void DecodeVisibleEnemyRequest(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException {
		String response = hashmap.get("data");
		String data[] = response.split("\\|");

		int enemy_unitid = Functions.ParseInt(data[0]);
		String enemy_unitcode = data[1];
		int seen_by_unitid = -1;
		String seen_by_unitcode = "";
		long event_time = 0;
		try {
			seen_by_unitid = Functions.ParseInt(data[2]);
			seen_by_unitcode = data[3];
			event_time = Long.parseLong(data[4]);
		} catch (Exception ex) {
			// Do nothing
		}

		UnitsTable enemy_unit = new UnitsTable(dbs);
		try {
		enemy_unit.selectRow(enemy_unitid);
		if (enemy_unit.getUnitCode().equalsIgnoreCase(enemy_unitcode)) {
			GamesTable game = new GamesTable(dbs);
			game.selectRow(enemy_unit.getGameID());

			int seen_by_side = -1;
			if (seen_by_unitid > 0) {
				UnitsTable seen_by_unit = new UnitsTable(dbs);
				try {
				seen_by_unit.selectRow(seen_by_unitid);
				if (seen_by_unit.getUnitCode().equalsIgnoreCase(seen_by_unitcode) == false) {
					throw new RuntimeException("Error decoding visible enemy data (2)");
				}
				seen_by_side = seen_by_unit.getSide();
				} finally {
					seen_by_unit.close();
				}
			} else {
				// Default to opponent
				seen_by_side = game.getOppositeSidesForSide(enemy_unit.getSide())[0];
			}
			
			//LoginsTable login = new LoginsTable(dbs);
			//login.selectRow(game.getLoginIDFromSide(seen_by_side));

			VisibleEnemiesTable.AddRec(dbs, game, enemy_unit, game.getLoginIDFromSide(seen_by_side), seen_by_side, VisibleEnemiesTable.VT_SEEN, event_time);
			
			//login.close();

		} else {
			throw new RuntimeException("Error decoding visible enemy data");
		}
		} finally {
			enemy_unit.close();
		}
	}



	public static String GetHeardEnemyData(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException {
		LoginsTable login_table = new LoginsTable(dbs);
		login_table.selectUser(login, pwd);
		GamesTable game = new GamesTable(dbs);
		game.selectRow(Integer.parseInt(hashmap.get("gameid")));
		AbstractMission mission = AbstractMission.Factory(game.getMissionID());
		int our_side = game.getSideFromPlayerID(login_table.getID());
		
		String sql = "SELECT Units.UnitID, Units.Side, Units.Status, VisibleEnemies.MapX, VisibleEnemies.MapY, VisibleEnemies.Angle, VisibleEnemies.VisibleType FROM VisibleEnemies ";
		sql = sql + " INNER JOIN Units ON Units.UnitID = VisibleEnemies.UnitID ";
		sql = sql + " WHERE VisibleEnemies.GameID = " + game.getID();
		sql = sql + " AND VisibleEnemies.PhaseNo >= " + (game.getPhaseNo()-game.getNumOfSides());
		sql = sql + " AND VisibleEnemies.SeenBySide IN (" + mission.getSidesForSide(our_side).toCSVString() + ")";
		sql = sql + " AND VisibleEnemies.VisibleType <> " + VisibleEnemiesTable.VT_SEEN; // Only heard!
		sql = sql + " AND Units.Side NOT IN (" + mission.getSidesForSide(our_side).toCSVString() + ")";
		sql = sql + " ORDER BY Status DESC, Side, OrderBy";

		StringBuffer str = new StringBuffer();
		str.append("UnitID|MapX|MapY|Angle|VisibleType|Side\n");
		
		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) {
			str.append(rs.getInt("UnitID") + "|" + rs.getInt("MapX") + "|" + rs.getInt("MapY") + "|" + rs.getInt("Angle") + "|" + rs.getInt("VisibleType") + "|" + rs.getInt("Side") + "\n");
		}
		rs.close();
		game.close();
		login_table.close();
		return str.toString();
	}


	public static String GetSeenEnemyData(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException {
		LoginsTable login_table = new LoginsTable(dbs);
		login_table.selectUser(login, pwd);
		GamesTable game = new GamesTable(dbs);
		game.selectRow(Integer.parseInt(hashmap.get("gameid")));
		AbstractMission mission = AbstractMission.Factory(game.getMissionID());
		int our_side = game.getSideFromPlayerID(login_table.getID());

		// Select our units then visible enemy units
		String sql = " SELECT Units.UnitID, Units.Side, VisibleEnemies.MapX, VisibleEnemies.MapY, Units.OrderBy, VisibleEnemies.Angle, VisibleEnemies.VisibleType FROM VisibleEnemies ";
		sql = sql + " INNER JOIN Units ON Units.UnitID = VisibleEnemies.UnitID ";
		sql = sql + " WHERE VisibleEnemies.GameID = " + game.getID();
		sql = sql + " AND Units.Status = " + UnitsTable.ST_DEPLOYED;
		sql = sql + " AND VisibleEnemies.PhaseNo >= " + (game.getPhaseNo()-game.getNumOfSides());
		sql = sql + " AND VisibleEnemies.SeenBySide IN (" + mission.getSidesForSide(our_side).toCSVString() + ")";
		sql = sql + " AND VisibleEnemies.VisibleType = " + VisibleEnemiesTable.VT_SEEN;
		sql = sql + " AND Units.Side NOT IN (" + mission.getSidesForSide(our_side).toCSVString() + ")";
		sql = sql + " ORDER BY Side, OrderBy";

		StringBuffer str = new StringBuffer();
		str.append("UnitID|MapX|MapY|Angle|Side\n");
		
		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) {
			str.append(rs.getInt("UnitID") + "|" + rs.getInt("MapX") + "|" + rs.getInt("MapY") + "|" + rs.getInt("Angle") + "|" + rs.getInt("Side") + "\n");
		}
		rs.close();
		game.close();
		login_table.close();
		return str.toString();

	}


}
