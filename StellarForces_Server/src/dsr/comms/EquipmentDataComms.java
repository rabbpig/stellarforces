package dsr.comms;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.dbs.SQLFuncs;
import ssmith.lang.Functions;

import dsr.SharedStatics;
import dsr.data.ClientMapData;
import dsr.data.EquipmentData;
import dsr.data.UnitData;
import dsrwebserver.DSRWebServer;
import dsrwebserver.pages.appletcomm.MiscCommsPage;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.UnitsTable;

public final class EquipmentDataComms {

	// These are for updating the client from the server

	// Called by the applet
	public static String GetEquipmentDataRequest(int gameid, String gamecode, int loginid, String logincode) {
		return "cmd=" + MiscCommsPage.EQUIPMENT_DATA + "&version=" + SharedStatics.VERSION + "&getput=" + MiscCommsPage.GET + "&gid=" + gameid + "&gc=" + gamecode + "&loginid=" + loginid + "&logincode=" + logincode;
	}


	// Called by the server to service the client's request
	public static String GetEquipmentDataAsResponse(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException {
		int gameid = Integer.parseInt(hashmap.get("gid"));
		String gamecode = hashmap.get("gc");

		StringBuffer str = new StringBuffer();

		GamesTable game = new GamesTable(dbs);
		try {
			game.selectRow(gameid);
			if (game.getGameCode().equalsIgnoreCase(gamecode)) {
				// Loop through equipment!
				String sql = "SELECT * FROM Equipment";
				sql = sql + " INNER JOIN EquipmentTypes ON EquipmentTypes.EquipmentTypeID = Equipment.EquipmentTypeID"; 
				sql = sql + " WHERE Equipment.GameID = " + gameid;
				ResultSet rs = dbs.getResultSet(sql);
				int count = 0;
				while (rs.next()) {
					if (rs.getInt("Destroyed") == 1 || rs.getInt("UnitID") > 0 || (rs.getInt("MapX") >= 0 && rs.getInt("MapY") >= 0)) {
						count++;
						str.append("|equipment|" + rs.getInt("EquipmentID") + "|" + rs.getString("EquipmentCode") + "|" + rs.getString("Name") + "|" + rs.getInt("UnitID") + "|" + rs.getInt("MajorTypeID"));
						str.append("|" + rs.getInt("MapX") + "|" + rs.getInt("MapY") + "|" + rs.getInt("ShotDamage") + "|" + rs.getInt("CCDamage") + "|" + rs.getInt("AimedShotAccuracy") + "|" + rs.getInt("AimedShotAPCost") + "|" + rs.getInt("SnapShotAccuracy") + "|" + rs.getInt("SnapShotAPCost") + "|" + rs.getInt("AutoShotAccuracy") + "|" + rs.getInt("AutoShotAPCost")); 
						str.append("|" + rs.getInt("ExplosionRad") + "|" + rs.getInt("CCAccuracy") + "|" + rs.getInt("Primed") + "|" + rs.getInt("ExplodeTurns"));
						str.append("|" + rs.getInt("ExplosionDamage") + "|" + rs.getInt("Ammo") + "|" + rs.getInt("AmmoTypeID") + "|" + rs.getInt("AmmoCapacity"));
						str.append("|" + rs.getInt("Weight") + "|" + rs.getInt("Destroyed") + "|" + rs.getInt("Explodes"));
						str.append("|" + rs.getInt("ReloadAPCost") + "|0|" + rs.getInt("Indestructable") + "|" + rs.getInt("LastUnitToTouch")); 
						for (int s=1 ; s<= 4 ; s++) {
							str.append("|" + rs.getInt("SeenBySide" + s));
						}
						str.append("|" + rs.getInt("EquipmentTypeID"));
						str.append("|" + rs.getInt("ShotRange"));
						str.append("|" + rs.getString("Code") + "|" + rs.getLong("PrimedTime"));
						// if you add any here, replace some of the "|0|" in the code above
					} else {
						DSRWebServer.p("Invalid piece of equipment: id=" + rs.getInt("EquipmentID"));
					}
				}
				str.insert(0, "qty|" + count);
				rs.close();

				return str.toString();
			} else {
				throw new RuntimeException("Invalid game");
			}
		} finally {
			game.close();
		}
	}


	// Called by the applet to update it's data from the server's response
	public static EquipmentData[] DecodeEquipmentDataResponse_UNUSED(ClientMapData map, UnitData[] units, String response) {
		String data[] = response.split("\\|");
		int pos=0;
		int num = 0;
		EquipmentData equipment[] = null;
		while (true) {
			//main.addToHUD("Processing data: " + pos);
			//AppletMain.ps(".");//data=" + data[pos]);
			if (data[pos].equalsIgnoreCase("qty")) {
				int u = Functions.ParseInt(data[pos+1]);
				equipment = new EquipmentData[u];
				pos += 2;
			} else if (data[pos].equalsIgnoreCase("equipment")) {
				equipment[num] = new EquipmentData();
				equipment[num].equip_id = Functions.ParseInt(data[pos+1]);
				equipment[num].equipcode = data[pos+2];
				equipment[num].setName(data[pos+3]);
				equipment[num].setUnitID(Functions.ParseInt(data[pos+4]));
				equipment[num].major_type = Functions.ParseByte(data[pos+5]);
				int x = Functions.ParseInt(data[pos+6]);
				int y = Functions.ParseInt(data[pos+7]);
				equipment[num].shot_damage = Functions.ParseShort(data[pos+8]);
				equipment[num].cc_damage = Functions.ParseShort(data[pos+9]);
				equipment[num].aimed_shot_acc = Functions.ParseByte(data[pos+10]);
				equipment[num].aimed_shot_aps = Functions.ParseByte(data[pos+11]);
				equipment[num].snap_shot_acc = Functions.ParseByte(data[pos+12]);
				equipment[num].snap_shot_aps = Functions.ParseByte(data[pos+13]);
				equipment[num].auto_shot_acc = Functions.ParseByte(data[pos+14]);
				equipment[num].auto_shot_aps = Functions.ParseByte(data[pos+15]);
				equipment[num].explosion_rad = Functions.ParseByte(data[pos+16]);
				equipment[num].cc_acc = Functions.ParseByte(data[pos+17]);
				equipment[num].primed = Functions.ParseInt(data[pos+18]) == 1;
				equipment[num].explode_turns = Functions.ParseByte(data[pos+19]);
				equipment[num].explosion_dam = Functions.ParseShort(data[pos+20]);
				equipment[num].setAmmo(Functions.ParseByte(data[pos+21]));
				equipment[num].ammo_type_id = Functions.ParseInt(data[pos+22]);
				equipment[num].ammo_capacity = Functions.ParseByte(data[pos+23]);
				equipment[num].weight = Functions.ParseByte(data[pos+24]);
				equipment[num].destroyed = Functions.ParseInt(data[pos+25]) == 1;
				equipment[num].explodes = Functions.ParseInt(data[pos+26]) == 1;
				equipment[num].reload_cost = Functions.ParseInt(data[pos+27]);
				try {
					//equipment[num].cost = Functions.ParseInt(data[pos+28]);
					equipment[num].indestructable = Functions.ParseByte(data[pos+29]);
					equipment[num].last_unit_to_touch = Functions.ParseInt(data[pos+30]);
					equipment[num].seen_by_side[1] = Functions.ParseByte(data[pos+31]);
					equipment[num].seen_by_side[2] = Functions.ParseByte(data[pos+32]);
					equipment[num].seen_by_side[3] = Functions.ParseByte(data[pos+33]);
					equipment[num].seen_by_side[4] = Functions.ParseByte(data[pos+34]);
					equipment[num].equipment_type_id = Functions.ParseInt(data[pos+35]);

					pos += 36;
				} catch (Exception ex) {
					pos += 28;
				}

				// Set last_unit_to_touch to unitid if there is one and last_unit_to_touch is zero (as it needs filling in)
				if (equipment[num].last_unit_to_touch <= 0 && equipment[num].getUnitID() > 0) {
					equipment[num].last_unit_to_touch = equipment[num].getUnitID();
				}

				// Add the equipment to the map or the unit
				if (equipment[num].getUnitID() > 0) { // Carried by a unit
					UnitData unit = UnitData.GetUnitDataFromID(units, equipment[num].getUnitID());
					unit.items.add(equipment[num]);
					// Is it there current item?
					if (unit.current_item_id_TMP == equipment[num].equip_id) {
						unit.current_item = equipment[num];
					}
				} else if (x >= 0 && y >= 0) {
					// Add it to the map 
					map.getSq_MaybeNULL(x, y).addEquipment(equipment[num]);
				} else {
					// It's been destroyed!
				}

				num++;
			} else {
				// Extra fields have obviously been added, so this version of the client ignores them.
				//AppletMain.p("Ignoring equipment data.");
				pos++;
			}
			if (pos >= data.length) {
				break;
			}
		}
		if (equipment == null) {
			throw new RuntimeException("Error decoding equipment data");
		} else {
			return equipment;
		}
	}


	//------------------------------------------------------------------------------

	// These are for updating the server from the applet

	// Called by the applet
	public static String GetEquipmentUpdateRequest_UNUSED(EquipmentData eq, int mapx, int mapy, int gameid) {
		StringBuffer str = new StringBuffer();
		str.append(eq.equip_id + "|");
		str.append(eq.equipcode + "|");
		str.append(eq.getUnitID() + "|");
		str.append(mapx + "|");
		str.append(mapy + "|");
		str.append(eq.getAmmo() + "|");
		str.append(eq.explode_turns + "|");
		str.append(SQLFuncs.b201(eq.primed) + "|");
		str.append(SQLFuncs.b201(eq.destroyed) + "|");
		str.append(gameid + "|");
		str.append(eq.last_unit_to_touch + "|");
		str.append(eq.seen_by_side[1] + "|");
		str.append(eq.seen_by_side[2] + "|");
		str.append(eq.seen_by_side[3] + "|");
		str.append(eq.seen_by_side[4] + "|");
		str.append(System.currentTimeMillis() + "|");
		str.append(eq.new_item + "|");
		str.append(eq.equipment_type_id + "|");
		// If you need to add any, re-use any blanks above

		return "cmd=" + MiscCommsPage.EQUIPMENT_DATA + "&version=" + SharedStatics.VERSION + "&getput=" + MiscCommsPage.PUT + "&data=" + str.toString();//CommFuncs.Encode(str.toString());
	}


	// Called by the server
	public static void DecodeEquipmentUpdateRequest(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException {
		String response = hashmap.get("data");
		String data[] = response.split("\\|");

		int equipid = Functions.ParseInt(data[0]); // -1 if a new item
		//String equipcode = data[1];
		int unitid = Functions.ParseInt(data[2]);
		int x = Functions.ParseInt(data[3]);
		int z = Functions.ParseInt(data[4]);
		int ammo = Functions.ParseInt(data[5]);
		int explode_turns = Functions.ParseInt(data[6]);
		int primed = Functions.ParseInt(data[7]);
		int destroyed = Functions.ParseInt(data[8]);
		int gameid = -1;
		int last_unit_to_touch = 0;
		int seen_by_side[] = new int[5];
		int new_item = 0;
		int equipment_type_id = -1;
		long primed_time = 0;
		gameid = Functions.ParseInt(data[9]);
		last_unit_to_touch = Functions.ParseInt(data[10]);
		seen_by_side[1] = Functions.ParseInt(data[11]);
		seen_by_side[2] = Functions.ParseInt(data[12]);
		seen_by_side[3] = Functions.ParseInt(data[13]);
		seen_by_side[4] = Functions.ParseInt(data[14]);
		try {
			new_item = Functions.ParseInt(data[16]);
			equipment_type_id = Functions.ParseInt(data[17]);
			primed_time = Long.parseLong(data[18]);
		} catch (Exception ex) {
			DSRWebServer.p("Old version of client:-");
			ex.printStackTrace();
		}

		if (new_item > 0 && destroyed == 0) {
			// Do we need to remove equipment?
			/*if (ht_equip_to_remove.containsKey(gameid)) {
				UnitsTable unit = new UnitsTable(dbs);
				unit.selectRow(unitid);
				int side = unit.getSide();
				RemoveEquipment(dbs, gameid, side);
				ht_equip_to_remove.remove(gameid);
			}*/
			// Only add if the side hasn't equipped, otherwise we're re-creating equipment that already exists!
			boolean create = true;
			if (unitid > 0) {
				UnitsTable unit = new UnitsTable(dbs);
				unit.selectRow(unitid);
				int side = unit.getSide();
				GamesTable game = new GamesTable(dbs);
				game.selectRow(gameid);
				if (game.hasSideEquipped(side)) {
					create = false;
				}
				game.close();
				unit.close();
			}
			if (create) {
				equipid = EquipmentTable.CreateEquipment(dbs, gameid, unitid, equipment_type_id, ammo);
				//DSRWebServer.LogEquipping(gameid, "Game " + gameid + " Added equipment for unit " + unitid + " (" + equipid + ")");
			} else {
				//DSRWebServer.LogEquipping(gameid, "Game " + gameid + " NOT added equipment for unit " + unitid + " (" + equipid + ")");
			}
		}
		EquipmentTable.Update(dbs, equipid, unitid, x, z, ammo, explode_turns, primed, destroyed, last_unit_to_touch, seen_by_side, primed_time);
	}


	//------------------------------------------------------------------------------------

	// Called by the server to remove equipment
	public static String GetRemoveEquipmentDataAsResponse(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException {
		int gameid = Integer.parseInt(hashmap.get("gid"));
		String gamecode = hashmap.get("gc");
		int side = Integer.parseInt(hashmap.get("side"));

		// Check the gamecode
		GamesTable game = new GamesTable(dbs);
		try {
			game.selectRow(gameid);
			if (game.getGameCode().equalsIgnoreCase(gamecode)) {
				// Notice we don't actually remove the equipment yet!  YES WE DO!
				//DSRWebServer.LogEquipping("Game " + gameid + " Planning to remove equipment for side " + side);
				//ht_equip_to_remove.put(gameid, side);
				//UnitsTable unit = new UnitsTable(dbs);
				//unit.selectRow(unitid);
				//int side = unit.getSide();
				RemoveEquipment(dbs, gameid, side);

				return "";
			} else {
				throw new RuntimeException("Invalid game");
			}
		} finally {
			game.close();
		}

	}


	public static void RemoveEquipment(MySQLConnection dbs, int gameid, int side) throws SQLException {
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);
		int ignore_type_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_STARDRIVE);
		//DSRWebServer.LogEquipping(gameid, "Game " + gameid + ": Actually removing equipment for side " + side);
		ResultSet rs = dbs.getResultSet("SELECT UnitID FROM Units WHERE GameID = " + gameid + " AND Side = " + side);
		while (rs.next()) {
			dbs.runSQLDelete("DELETE FROM Equipment WHERE GameID = " + gameid + " AND UnitID = " + rs.getInt("UnitID") + " AND EquipmentTypeID <> " + ignore_type_id);
			//DSRWebServer.LogEquipping(gameid, "Game " + gameid + ": Removed equipment for unit " + rs.getInt("UnitID"));
		}
		rs.close();
		game.close();
	}


	/*public static String GetFinishedEquippingAsResponse(MySQLConnection dbs, float version, HashMap<String, String> hashmap) throws SQLException {
		int gameid = Integer.parseInt(hashmap.get("gid"));
		String gamecode = hashmap.get("gc");
		int side = Integer.parseInt(hashmap.get("side"));

		// Check the gamecode
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);
		if (game.getGameCode().equalsIgnoreCase(gamecode)) {
			DSRWebServer.LogEquipping(gameid, "Game " + gameid + ": Side " + side + " has finished equipping");
			return "";
		} else {
			throw new RuntimeException("Invalid game");
		}

	}*/


}