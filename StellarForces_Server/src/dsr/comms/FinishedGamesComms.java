package dsr.comms;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Dates;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;

public class FinishedGamesComms {

	public static String GetFinishedGamesList(MySQLConnection dbs, String login, String pwd, boolean mine_only) throws SQLException {
		StringBuffer str = new StringBuffer();
		str.append("GameID|Name|MissionName|Sides|Player1Name|Player2Name|Player3Name|Player4Name|WinningSide1|WinningSide2|DateFinished|YourSide|IsAdvanced|CampGame|ForumID|Side1VPs|Side2VPs|Side3VPs|Side4VPs|Side1Name|Side2Name|Side3NAme|Side4Name\n");

		LoginsTable logintbl = new LoginsTable(dbs);
		if (logintbl.selectUser(login, pwd)) {

			GamesTable game = new GamesTable(dbs);
			String sql = "SELECT * FROM Games WHERE GameStatus = " + GamesTable.GS_FINISHED + " AND WinType !=" + GamesTable.WIN_OPPONENT_CONCEDED;
			if (mine_only) {
				//+ " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION + " AND Mission <> " + AbstractMission.SF_PRACTISE_MISSION_WITH_AI
				sql = sql + " AND " + LoginsTable.GetPlayerSubQuery(logintbl.getID());
			}
			sql = sql + " AND WinType != " + GamesTable.WIN_CANCELLED;
			sql = sql + " ORDER By DateFinished DESC LIMIT 50";
			ResultSet rs = dbs.getResultSet(sql);
			while (rs.next()) {
				game.selectRow(rs.getInt("GameID"));
				AbstractMission mission = AbstractMission.Factory(game.getMissionID());

				String name = "Game " + rs.getInt("GameID") + ": " + AbstractMission.GetMissionNameFromType(rs.getInt("Mission"), false, false);
				name = name + " with " + game.getPlayersNames(false, false);
				name = name + " (Finished " + Dates.FormatDate(rs.getDate("DateFinished"), "dd MMM yyyy") + ")";
				str.append(rs.getInt("GameID") + "|" + name + "|" + AbstractMission.GetMissionNameFromType(rs.getInt("Mission"), false, false) + "|" + rs.getInt("Sides") + "|");
				// Player names
				for (int s=1 ; s<= 4 ; s++) {
					if (s <= rs.getInt("Sides")) {
						if (rs.getInt("Player" + s + "ID") > 0) {
							String display_name = LoginsTable.GetDisplayName(dbs, rs.getInt("Player" + s + "ID"));
							str.append(display_name);
						}
					}
					str.append("|");
				}
				str.append(rs.getInt("WinningSide") + "|" + rs.getInt("WinningSide2"));
				str.append("|" + Dates.FormatDate(game.getDateFinished(), Dates.UKDATE_FORMAT));
				if (mine_only) {
					str.append("|" + game.getSideFromPlayerID(logintbl.getID()));
				} else {
					str.append("|0");
				}
				str.append("|" + rs.getInt("CanHearEnemies") + "|" + rs.getInt("CampGame") + "|" + rs.getInt("ForumID") + "|");
				// VPs
				for (int s=1 ; s<= 4 ; s++) {
					if (s <= rs.getInt("Sides")) {
						str.append(rs.getInt("Side" + s + "VPs"));
					}
					str.append("|");
				}
				// Side names
				for (int s=1 ; s<= 4 ; s++) {
					if (s <= rs.getInt("Sides")) {
						str.append(mission.getSideDescription(s));
					}
					str.append("|");
				}
				str.append("\n");

			}
			rs.close();
			game.close();
		}
		logintbl.close();
		return str.toString(); 

	}

}
