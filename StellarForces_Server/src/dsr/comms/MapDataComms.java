package dsr.comms;

import java.awt.Dimension;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;

import dsr.AppletMain;
import dsr.SharedStatics;
import dsr.data.AppletMapSquare;
import dsr.data.ClientMapData;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.appletcomm.MiscCommsPage;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GameLogTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;

public final class MapDataComms {

	// These are for updating the applet from the server

	// Called by the applet
	public static String GetMapDataRequest(int gameid) {
		return "cmd=" + MiscCommsPage.MAP_DATA + "&version=" + SharedStatics.VERSION + "&getput=" + MiscCommsPage.GET + "&gid=" + gameid;
	}
	

	// Called by the server
	public static String GetMapDataAsResponse(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException {
		int gameid = Integer.parseInt(hashmap.get("gid"));

		GamesTable games = new GamesTable(dbs);
		games.selectRow(gameid);
		int mid = games.getFieldAsInt("MapDataID");

		StringBuffer str = new StringBuffer();

		Dimension d = MapDataTable.GetMapSize(dbs, mid);

		str.append("width|" + d.width + "|");
		str.append("height|" + d.height);

		String sql = "SELECT * FROM MapDataSquares ";
		sql = sql + " LEFT JOIN SpecialMapDataSquares ON SpecialMapDataSquares.MapDataID = MapDataSquares.MapDataID AND SpecialMapDataSquares.MapX = MapDataSquares.MapX AND SpecialMapDataSquares.MapY = MapDataSquares.MapY ";
		sql = sql + " WHERE MapDataSquares.MapDataID = " + mid;
		//long now = System.currentTimeMillis();
		ResultSet rs = dbs.getResultSet(sql);
		//long time = (System.currentTimeMillis() - now) / 1000;
		//DSRWebServer.p("Time taken to collect map: " + time);
		while (rs.next()) {
			int scenery_code = rs.getInt("SceneryCode");
			if (rs.getInt("Destroyed") != 0) {
				scenery_code = 0; // DOn't send scenery
			}
			str.append("|ms|" + rs.getInt("MapDataSquareID") + "|" + rs.getInt("MapX") + "|" + rs.getInt("MapY"));
			str.append("|" + rs.getInt("SquareType") + "|" + rs.getInt("DoorType"));
			str.append("|" + rs.getInt("DeploymentSquareSide") + "|" + rs.getInt("EscapeHatch") + "|" + rs.getInt("FloorTex"));
			str.append("|" + rs.getInt("Destroyed") + "|" + rs.getInt("DoorOpen"));
			str.append("|" + scenery_code + "|" + rs.getInt("SceneryExtraData") + "|" + rs.getInt("RaisedFloorTex") + "|" + rs.getInt("SmokeType"));
			str.append("|" + rs.getInt("SmokeCausedBy") + "|" + rs.getInt("OwnerSide"));
		}
		rs.close();
		games.close();
		return str.toString();
	}


	// Called by the applet
	public static ClientMapData DecodeMapDataResponse(String response) {
		//response = CommFuncs.Decode(response);
		response = response.replaceAll("\r\n", "");
		String data[] = response.split("\\|");
		int w=-1, pos=0;
		AppletMapSquare[][] mapdata = null;
		while (true) {
			//AppletMain.ps(".");//data=" + data[pos]);
			if (data[pos].equalsIgnoreCase("width")) {
				w = Functions.ParseInt(data[pos+1]);
				pos += 2;
			} else if (data[pos].equalsIgnoreCase("height")) {
				int h = Functions.ParseInt(data[pos+1]);
				mapdata = new AppletMapSquare[w][h];
				pos += 2;
			} else if (data[pos].equalsIgnoreCase("ms")) {
				int id = Functions.ParseInt(data[pos+1]);
				byte x = Functions.ParseByte(data[pos+2]);
				byte z = Functions.ParseByte(data[pos+3]);
				byte type = Functions.ParseByte(data[pos+4]);
				byte door = Functions.ParseByte(data[pos+5]);
				byte deploy_sq_side = Functions.ParseByte(data[pos+6]);
				byte escape_hatch_side = Functions.ParseByte(data[pos+7]);
				short floor_tex = Functions.ParseShort(data[pos+8]);
				byte destroyed = Functions.ParseByte(data[pos+9]);
				byte door_open = 0;
				short scenery_code = 0;
				byte scenery_direction = 0;
				short raised_floor_tex = 0;
				byte smoke_type = -1;
				int smoke_caused_by = -1;
				try {
					door_open = Functions.ParseByte(data[pos+10]);
					scenery_code = Functions.ParseShort(data[pos+11]);
					scenery_direction = Functions.ParseByte(data[pos+12]);
					raised_floor_tex = Functions.ParseShort(data[pos+13]);
					smoke_type = Functions.ParseByte(data[pos+14]);
					smoke_caused_by = Functions.ParseInt(data[pos+15]);
					pos += 16;
				} catch (Exception ex) { 
					pos += 10;
				}

				AppletMapSquare ms = new AppletMapSquare(id, type, x, z, deploy_sq_side, escape_hatch_side, floor_tex, destroyed, door_open, raised_floor_tex, smoke_type, smoke_caused_by);
				ms.door_type = door;
				ms.scenery_code = scenery_code;
				ms.scenery_direction = scenery_direction;

				mapdata[x][z] = ms;
			} else {
				//throw new RuntimeException("Unknown data decoding map data");
				AppletMain.p("Ignoring map data.");
				pos++;
			}
			if (pos >= data.length) {
				break;
			}
		}

		// Check we got some valid data
		if (mapdata == null) {
			throw new RuntimeException("Error decoding map data");
		} else {
			return new ClientMapData(mapdata);
		}

	}


	//------------------------------------------------------------------------------

	// These are for updating the server from the applet

	// This is called by the applet
	public static String GetMapDataUpdateRequest(int gameid, String gamecode, AppletMapSquare sq, int caused_by_side) {
		StringBuffer str = new StringBuffer();
		str.append(gameid + "|");
		str.append(gamecode + "|");
		str.append(sq.x + "|");
		str.append(sq.y + "|");
		str.append(sq.major_type + "|");
		str.append(sq.destroyed + "|");
		str.append(sq.texture_code + "|");
		str.append(sq.door_type + "|");
		str.append((sq.door_open ?  1:0) + "|");
		str.append(caused_by_side + "|");
		str.append(sq.smoke_type + "|");
		str.append(sq.smoke_caused_by + "|");
		str.append(System.currentTimeMillis() + "|");
		str.append(sq.owner_side + "|");

		return "cmd=" + MiscCommsPage.MAP_DATA + "&version=" + SharedStatics.VERSION + "&getput=" + MiscCommsPage.PUT + "&data=" + str.toString();
	}


	// This is called by the server
	public static void DecodeMapDataUpdateRequest(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException {
		String response = hashmap.get("data");
		String data[] = response.split("\\|");

		int gameid = Functions.ParseInt(data[0]);
		String gamecode = data[1];
		int x = Functions.ParseInt(data[2]);
		int z = Functions.ParseInt(data[3]);
		int major_type = Functions.ParseInt(data[4]);
		int destroyed = 0, floor_tex = 0, door_type = 0, door_open = 0, destroyed_by_side = -1;
		int smoke = -1; // So we know if it has changed
		int smoke_caused_by = -1; // So we know if it has changed
		long event_time = 0;
		int new_owner = 0;
		int raised_tex_code = 0;
		int scenery_code = 0;
		try { // Earlier clients may not send this information
			destroyed = Functions.ParseInt(data[5]);
			floor_tex = Functions.ParseInt(data[6]);
			door_type = Functions.ParseInt(data[7]);
			door_open = Functions.ParseInt(data[8]);
			destroyed_by_side = Functions.ParseInt(data[9]);
			smoke = Functions.ParseInt(data[10]);
			smoke_caused_by = Functions.ParseInt(data[11]);
			event_time = Long.parseLong(data[12]);
			new_owner = Functions.ParseInt(data[13]);
			raised_tex_code = Functions.ParseInt(data[14]);
			scenery_code = Functions.ParseInt(data[15]);
		} catch (Exception ex) {
			DSRWebServer.p("Old version of client:-");
			ex.printStackTrace();
		}

		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);
		if (game.getGameCode().equals(gamecode)) {
			try {
				ResultSet rs = dbs.getResultSet("SELECT * FROM MapDataSquares WHERE MapDataID = " + game.getMapDataID() + " AND MapX = " + x + " AND MapY = " + z);
				if (rs.next()) {
					if (destroyed == 1 && rs.getInt("Destroyed") == 0) {
						// Square has been destroyed!
						int sq_type = rs.getInt("SquareType"); 
						if (sq_type == MapDataTable.MT_COMPUTER) {
							// If mission Meltdown, drop some explosives!
							if (game.getMissionID() == AbstractMission.MELTDOWN || game.getMissionID() == AbstractMission.MELTDOWN_4P) {
								// Create explosives that will explode later
								int expl_id = EquipmentTypesTable.GetEquipmentTypeFromCode(dbs, EquipmentTypesTable.CD_EXPLOSIVE);
								int id = EquipmentTable.CreateEquipment(dbs, gameid, -1, expl_id, 1);
								int explode_turns = Functions.rnd(2, 4) * game.getNumOfSides();
								int seen_by_side[] = new int[5];
								EquipmentTable.Update(dbs, id, -1, x, z, 1, explode_turns, 1, 0, 0, seen_by_side, System.currentTimeMillis());
								GameLogTable.AddRec(dbs, game, -1, -1, "A computer has been destroyed and will explode soon!", true, event_time);
							} else {
								GameLogTable.AddRec(dbs, game, -1, -1, "A computer has been destroyed!", true, true, event_time);
							}
							UnitHistoryTable.AddRecord_ComputerDestroyed(dbs, game, x, z, event_time);
							major_type = MapDataTable.MT_COMPUTER; // Don't remove destroyed computers!
						} else {
							try {
								int orig_door_type = rs.getInt("DoorType");
								UnitHistoryTable.AddRecord_MapSquareDestroyed(dbs, game, x, z, sq_type, orig_door_type, event_time);
							} catch (Exception ex) {
								DSRWebServer.HandleError(ex, true);
							}
						}
						MapDataTable.SetMapSquareDestroyed(dbs, rs.getInt("MapDataSquareID"), destroyed, destroyed_by_side);
					}
					if (floor_tex > 0) {
						try {
							// Create door event if door state changed
							int orig_door_open = rs.getInt("DoorOpen");
							if (orig_door_open != door_open) {
								if (door_open > 0) {
									UnitHistoryTable.AddRecord_DoorOpened(dbs, game, x, z, event_time);
								} else {
									UnitHistoryTable.AddRecord_DoorOpened(dbs, game, x, z, event_time);
								}
							}
						} catch (Exception ex) {
							DSRWebServer.HandleError(ex);
						}
						MapDataTable.UpdateMapSquare(dbs, game, rs.getInt("MapDataSquareID"), x, z, major_type, floor_tex, door_type, door_open, smoke, smoke_caused_by, new_owner, destroyed, raised_tex_code, scenery_code, event_time);
					}
				}
				rs.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

}
