package dsr.comms;

import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Dates;
import ssmith.lang.Functions;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.MessagesTable;

public class MessagesComms {

	public static String GetNumUnreadMessages(MySQLConnection dbs, String login, String pwd) throws SQLException, UnsupportedEncodingException {
		LoginsTable current_login = null;
		try {
			current_login = new LoginsTable(dbs);
			if (current_login.selectUser(login, pwd)) {
				String sql = "SELECT Count(*) FROM Messages WHERE ToID = " + current_login.getID() + " AND COALESCE(MsgRead, 0) = 0";
				return "" + dbs.getScalarAsInt(sql);
			} else {
				return "0"; // Invalid login, no messages!
			}
		} finally {
			current_login.close();
		}
	}


	public static String GetMessagesListAsResponse(MySQLConnection dbs, String login, String pwd) throws SQLException, UnsupportedEncodingException {
		LoginsTable current_login = new LoginsTable(dbs);
		if (current_login.selectUser(login, pwd)) {

			StringBuffer str = new StringBuffer();
			str.append("MessageID|FromID|FromName|DateCreated|Subject|Message|Read|DaysOld\n");

			ResultSet rs = dbs.getResultSet("SELECT *, DATEDIFF(NOW(), DateCreated) AS DaysOld FROM Messages WHERE ToID = " + current_login.getID() + " ORDER BY MsgRead, DateCreated DESC LIMIT 12"); // AND (COALESCE(MsgRead, 0) = 0 OR DATEDIFF(curdate(), DateCreated) < 21)
			while (rs.next()) {
				String display_name = "[Unknown]";
				if (rs.getInt("FromID") > 0) {
					display_name = LoginsTable.GetDisplayName(dbs, rs.getInt("FromID"));
				}
				str.append(rs.getInt("MessageID") + "|" + rs.getInt("FromID") + "|" + display_name + "|" + Dates.FormatDate(rs.getTimestamp("DateCreated"), Dates.UKDATE_FORMAT2_WITH_TIME) + "|" + AbstractCommFuncs.URLEncodeString(rs.getString("Subject")) + "|" + AbstractCommFuncs.URLEncodeString(rs.getString("Message")) + "|" + rs.getInt("MsgRead") + "|" + rs.getInt("DaysOld") + "\n");
			}
			rs.close();
			current_login.close();
			return str.toString(); 
		} else {
			current_login.close();
			return "Invalid login/password";
		}
	}


	public static String MarkMsgAsRead(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException, UnsupportedEncodingException {
		int msgid = Functions.ParseInt(hashmap.get("msgid"));
		MessagesTable.MarkAsRead(dbs, msgid);
		return "";
	}


	public static String SendMessage(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException, UnsupportedEncodingException {
		LoginsTable current_login = new LoginsTable(dbs);
		if (current_login.selectUser(login, pwd)) {
			String subject = AbstractCommFuncs.URLDecodeString(hashmap.get("subject"));
			String msg = AbstractCommFuncs.URLDecodeString(hashmap.get("msg"));

			String tonames = AbstractCommFuncs.URLDecodeString(hashmap.get("toname"));
			MessagesTable.SendMsgToNames(dbs, current_login.getID(), tonames, subject, msg);
			current_login.close();
			return "";
		} else {
			current_login.close();
			return "Invalid Login/password";
		}
	}

}

