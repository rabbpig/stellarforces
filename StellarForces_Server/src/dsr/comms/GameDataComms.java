package dsr.comms;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.util.MyList;
import dsr.AppletMain;
import dsr.SharedStatics;
import dsrwebserver.DSRWebServer;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.pages.appletcomm.MiscCommsPage;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.GameLogTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.UnitsTable;

public final class GameDataComms {

	// These funcs are for updating the server from the applet

	// Update Types
	public static final int DEPLOYED = 1;
	public static final int TURN_ENDED = 2;
	private static final int EQUIPPED = 3;


	// This is called by the applet
	public static String GetGameUpdateRequest(AppletMain main, int gameid, String gamecode, int side, int type) {
		return "cmd=" + MiscCommsPage.GAME_DATA + "&version=" + SharedStatics.VERSION + "&getput=" + MiscCommsPage.PUT + "&gameid=" + gameid + "&gc=" + gamecode + "&side=" + side + "&type=" + type + "&event_time=" + System.currentTimeMillis();
	}


	// This is called by the server
	public static String DecodeGameUpdateRequest(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException {
		int gameid = Integer.parseInt(hashmap.get("gameid"));
		String gamecode = hashmap.get("gc");
		int side = Integer.parseInt(hashmap.get("side"));
		int type = Integer.parseInt(hashmap.get("type"));
		long event_time = 0;
		try {
			event_time = Long.parseLong(hashmap.get("event_time"));
		} catch (Exception ex) {
			// Do nothing
		}

		GamesTable game = new GamesTable(dbs);
		try {
		game.selectRow(gameid);
		if (game.getGameCode().equalsIgnoreCase(gamecode)) {
			if (game.getGameStatus() != GamesTable.GS_FINISHED) {
				switch (type) {
				case EQUIPPED:
					game.setSideHasEquipped(side);
					UnitsTable.UseFirstItem(dbs, game.getID(), side);
					// If it's a camp game, deduct creds
					if (game.isCampGame()) {
						AbstractMission mission = AbstractMission.Factory(game.getMissionID());
						int tot_creds = mission.getCreditsForSide(side);
						LoginsTable login = new LoginsTable(dbs);
						login.selectRow(game.getLoginIDFromSide(side));
						tot_creds = tot_creds + login.getCampCredits();
						int creds_remaining = tot_creds - EquipmentTable.GetValueOfArmourAndEquipment(dbs, gameid, side, true);
						login.setCampCredits(creds_remaining);
						login.close();
					}
					return "";
				case DEPLOYED:
					game.setSideHasDeployed(side, event_time);
					return "";
				case TURN_ENDED:
					// Check the player that has ended is the one who's turn it was!
					if (game.getTurnSide() == side) {
						game.recalcVPs();
						String check_for_winner = CheckForWinner(dbs, game);
						if (check_for_winner.length() > 0) {
							return check_for_winner;
						} else {
							game.endOfTurn(event_time, true);
							if (game.wasLastTurn() == false) {
								game.startOfTurn(event_time);
								return "";
							} else { // That was the Last turn!  Game over.
								GameLogTable.AddRec(dbs, game, -1, -1, "The number of turns has expired!", true, System.currentTimeMillis());
								AbstractMission mission = AbstractMission.Factory(game.getMissionID());
								int winner = -1;
								if (mission.highestVPsWinAtEnd()) {
									// Winner is the side with most VPs
									winner = getSideWithHighestVPs(game);
								} else { // Get specified winner at end
									winner = mission.getWinnerAtEnd();
								} 
								if (winner < 1) {
									game.setAsDraw();
									return MiscCommsPage.DRAW;
								} else {
									game.setWinType(GamesTable.WIN_TIME_EXPIRED, winner);
									return MiscCommsPage.GetWinnerCodeFromWinner(winner);
								}
							}
						}
					} else {
						//DSRWebServer.SendEmailToAdmin("Wrong side ended turn", "Wrong side ended turn in game " + gameid);
					}
					return "";
				default:
					throw new RuntimeException("Unknown game update command: " + type);
				}
			} else {
				//throw new RuntimeException("Game comms for finished game! (GID:" + game.getID() + ")");
				/*try {
					DSRWebServer.SendEmailToAdmin("GAME ALREADY FINISHED", "GID:" + game.getID());
				} catch (Exception ex) {
					ex.printStackTrace();
				}*/
				return "";
			}
		} else {
			throw new RuntimeException("Invalid game");
		}
		} finally {
			game.close();

		}
	}


	public static String CheckForWinner(MySQLConnection dbs, GamesTable game) throws SQLException {
		if (game.getNumOfSides() == 1) { // Don't end game on practise mission 
			return "";
		}
		int vps[] = new int[game.getNumOfSides()+1];
		boolean draw = true;
		ArrayList<Integer> winning_sides = new ArrayList<Integer>(); 
		for (int side=1 ; side<=game.getNumOfSides() ; side++) {
			vps[side] = game.getVPsForSide(side);
			if (vps[side] < 100) {
				draw = false;
			} else {
				winning_sides.add(side);
			}
		}

		AbstractMission mission = AbstractMission.Factory(game.getMissionID());

		// See if there are two winners on the same side
		boolean multiple_winners_on_same_side = false;
		try {
			if (winning_sides.size() > 1) {
				MyList<Integer> same_sides = mission.getSidesForSide(winning_sides.get(0));
				if (winning_sides.size() == 2) {
					if (same_sides.contains(winning_sides.get(0)) && same_sides.contains(winning_sides.get(1))) {
						multiple_winners_on_same_side = true;
					}
				} else if (winning_sides.size() == 3) {
					if (same_sides.contains(winning_sides.get(0)) && same_sides.contains(winning_sides.get(1)) && same_sides.contains(winning_sides.get(2))) {
						multiple_winners_on_same_side = true;
					}
				}

			}
		} catch (Exception ex) {
			DSRWebServer.HandleError(ex);
		}

		if (draw || (winning_sides.size() > 1 && multiple_winners_on_same_side == false)) { // && game.getNumOfSides() == 2)) { // Two sides might have 100 VPs but be on the same side in a 2v2 game!
			game.setAsDraw();
			return MiscCommsPage.DRAW;
		} else if (winning_sides.size() > 0) {
			game.setWinType(GamesTable.WIN_COMPLETED_MISSION, winning_sides.get(0));
			return MiscCommsPage.GetWinnerCodeFromWinner(winning_sides.get(0));
		} else if (game.getGameStatus() >= GamesTable.GS_STARTED) { // It's not a draw and there's no clear winner...
			// Are there any units left in the game?  We must only check this AFTER we've checked for VPs in case one side has all escaped (and thus won)
			// Notice that we also check for escaped units as otherwise a H&R mission ends in a draw if both sides have zero units left on the last turn
			int c = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Status IN ( " + UnitsTable.ST_DEPLOYED + ", " + UnitsTable.ST_ESCAPED + ")");
			if (c <= 0 ) { // No units left
				game.setAsDraw();
				return MiscCommsPage.DRAW;
			} else {
				try {
					if (mission.isSnafu()) {
						// If snafu, see if all sabs have been killed
						int sab_side = game.getSabSide(); 
						int sab_units = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + sab_side + " AND Status = " + UnitsTable.ST_DEPLOYED);
						if (sab_units == 0) {
							int winning_side = getSideWithHighestVPs(game);
							if (winning_side > 0) {
								game.setWinType(GamesTable.WIN_COMPLETED_MISSION, winning_side);
								return MiscCommsPage.GetWinnerCodeFromWinner(winning_side);
							}					
						}
					}
				} catch (Exception ex) {
					DSRWebServer.HandleError(ex, true);
				}
				// See if there's only one side left
				int sides = dbs.getScalarAsInt("SELECT Count(DISTINCT Side) FROM Units WHERE GameID = " + game.getID() + " AND Status = " + UnitsTable.ST_DEPLOYED);
				if (sides <= 1) { // There is only one side left
					try {
						// Special checks if one side can be fully reanimated (zombies, clone wars)
						if (sides > 0) { // There may be no sides left as one side might have escaped!
							int side_remaining = dbs.getScalarAsInt("SELECT Side FROM Units WHERE GameID = " + game.getID() + " AND Status = " + UnitsTable.ST_DEPLOYED);
							if (mission.canSideWinByKillingAllOpposition(side_remaining) == false) {
								return "";
							}
						}
						int winning_side = getSideWithHighestVPs(game); // Highest VPs win
						if (winning_side < 1 && (sides == 1 || mission.canUnitsEscape(winning_side))) { // No winner but only one side left standing (or no sides left standing but the winning side all escaped)
							// Winner is side left standing
							winning_side = dbs.getScalarAsInt("SELECT Side FROM Units WHERE GameID = " + game.getID() + " AND Status = " + UnitsTable.ST_DEPLOYED);
						}
						if (winning_side > 0) {
							game.setWinType(GamesTable.WIN_COMPLETED_MISSION, winning_side);
							return MiscCommsPage.GetWinnerCodeFromWinner(winning_side);
						} else {
							game.setAsDraw();
							return MiscCommsPage.DRAW;
						}
					} catch (Exception ex) {
						DSRWebServer.HandleError(ex, true);
					}
				}
			}
		}
		return "";
	}


	private static int getSideWithHighestVPs(GamesTable game) throws SQLException {
		// What happens if two sides have joint highest vps?
		int vps[] = new int[game.getNumOfSides()+1];
		for (int side=1 ; side<=game.getNumOfSides() ; side++) {
			vps[side] = game.getVPsForSide(side);
		}

		int highest_side = -1;
		int most_vps = 1; // Ensure they have at least 1 VP!
		for (int side=1 ; side<=game.getNumOfSides() ; side++) {
			if (vps[side] >= most_vps) {
				highest_side = side;
				most_vps = vps[side];
			}
		}
		return highest_side;
	}

}
