package dsr.comms;

import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.util.MyList;
import dsrwebserver.DSRWebServer;
import dsrwebserver.tables.EquipmentTable;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;
import dsrwebserver.tables.UniversePlayersEquipmentTable;
import dsrwebserver.tables.UniverseSquaresTable;
import dsrwebserver.tables.UniverseTreatiesTable;
import dsrwebserver.tables.UniverseUnitsTable;

public class UniverseComms {

	public static final int FS_UNOWNED = 0;
	public static final int FS_OWNED_BY_US = 1;
	public static final int FS_OWNED_BY_COMRADE = 2;
	public static final int FS_OWNED_BY_ENEMY = 3;
	// ## IF YOU ADD TO THIS, ADD TO UniverseMapPlanetIcon.class

	public static String GetUniverseMapData(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException, UnsupportedEncodingException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			if (current_login.selectUser(login, pwd)) {

				// Check the player owns at least one planet, if not, give them one
				if (dbs.getScalarAsInt("SELECT Count(*) FROM UniverseSquares WHERE OwnerID = " + current_login.getID()) == 0) {
					ResultSet rs = dbs.getResultSet("SELECT UniverseSquareID FROM UniverseSquares WHERE COALESCE(OwnerID, 0) = 0 ORDER BY Rand()");
					if (rs.next()) {
						UniverseSquaresTable.SetOwnerID(dbs, rs.getInt("UniverseSquareID"), current_login.getID());
					} else {
						// No spaces left!
						DSRWebServer.SendEmailToAdmin("No space left in Universe", "Title says it all");
					}
					rs.close();
				}
				MyList<Integer> comrades = current_login.getUniverseComrades();
				if (comrades.size() == 0) {
					comrades.add(-1);
				}

				StringBuffer str = new StringBuffer();
				str.append("UniverseSquareID|MapX|MapY|Name|OwnerID|GameID|Units|FriendlyStatus|CanDeploy\n");

				GamesTable game = new GamesTable(dbs);

				ResultSet rs_squares = dbs.getResultSet("SELECT * FROM UniverseSquares");
				while (rs_squares.next()) {
					game.selectRow(rs_squares.getInt("GameID"));
					int units = 0;
					if (game.isPlayerInGame(current_login.getID())) {
						int side = game.getSideFromPlayerID(current_login.getID()) ;
						units = dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + game.getID() + " AND Side = " + side);
					}
					int friendly_status = FS_UNOWNED;
					if (rs_squares.getInt("OwnerID") > 0) {
						if (rs_squares.getInt("OwnerID") == current_login.getID()) {
							friendly_status = FS_OWNED_BY_US;
						} else if (UniverseTreatiesTable.AreSidesInTreaty(dbs, current_login.getID(), rs_squares.getInt("OwnerID"))) {
							friendly_status = FS_OWNED_BY_COMRADE;
						} else {
							friendly_status = FS_OWNED_BY_ENEMY;
						}
					}
					int can_deploy = 0;
					boolean b_cd = IsAdjacent(dbs, rs_squares.getInt("MapX"), rs_squares.getInt("MapY"), current_login.getID(), comrades) && IsSpace(dbs, game, rs_squares.getInt("MapX"), rs_squares.getInt("MapY"), current_login.getID(), comrades);
					if (b_cd) {
						can_deploy = 1;
					}
					str.append(rs_squares.getInt("UniverseSquareID") + "|" + rs_squares.getInt("MapX") + "|" + rs_squares.getInt("MapY") + "|" + rs_squares.getString("Name") + "|" + rs_squares.getInt("OwnerID") + "|" + rs_squares.getInt("GameID") + "|" + units + "|" + friendly_status + "|" + can_deploy + "\n");
				}
				rs_squares.close();
				return str.toString(); 
			} else {
				return "Invalid login/password";
			}
		} finally {
			current_login.close();
		}
	}


	private static boolean IsAdjacent(MySQLConnection dbs, int map_x, int map_y, int loginid, MyList<Integer> comrades) throws SQLException {
		for (int y=map_y-1 ; y<=map_y+1 ; y++) {
			for (int x=map_x-1 ; x<=map_x+1 ; x++) {
				String sql = "SELECT Count(*) FROM UniverseSquares WHERE MapX = " + map_x + " AND MapY = " + map_y + " AND OwnerID IN (" + loginid + ", " + comrades.toCSVString() + ")";
				if (dbs.getScalarAsInt(sql) > 0) {
					return true;
				}
			}
		}
		return false;
	}


	private static boolean IsSpace(MySQLConnection dbs, GamesTable game, int map_x, int map_y, int loginid, MyList<Integer> comrades) throws SQLException {
		if (game.getNumOfSides() <= 1) {
			return true; 
		} else if (game.isPlayerInGame(loginid)) {
			return true;
		} else if (game.getNumOfSides() == 4) {
			return false;
		} else {
			// There is space only if there is only one or no comrades in there
			MyList<Integer> sides_in_game = new MyList<Integer>();
			for (int s=1 ; s<=game.getNumOfSides() ; s++) {
				sides_in_game.add(game.getLoginIDFromSide(s));
			}
			MyList<Integer> new_list = sides_in_game.getDupes(comrades);
			return new_list.size() <= 1;
		}
	}


	public static String GetPlayersUniverseSquadList(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException, UnsupportedEncodingException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			if (current_login.selectUser(login, pwd)) {
				// Create them if they don't have any
				String sql = "SELECT Count(*) FROM UniverseUnits WHERE OwnerID = " + current_login.getID();
				if (dbs.getScalarAsInt(sql) == 0) {
					for (int i=0 ; i<UniverseUnitsTable.STARTING_UNITS ; i++) {
						UniverseUnitsTable.CreateUniverseUnit(dbs, current_login.getID());
					}
				}

				StringBuffer str = new StringBuffer();
				str.append("UniverseUnitID|Name|MaxHealth|MaxAPs|ShotSkill|CombatSkill|Strength|MaxEnergy|MaxMorale\n");

				ResultSet rs = dbs.getResultSet("SELECT * FROM UniverseUnits WHERE OwnerID = " + current_login.getID() + " AND COALESCE(UnitID, 0) = 0 ORDER BY OrderBy");
				UniverseUnitsTable campunit = new UniverseUnitsTable(dbs);
				while (rs.next()) {
					campunit.selectRow(rs.getInt("UniverseUnitID"));
					str.append(rs.getInt("UniverseUnitID") + "|" + campunit.getName() + "|" + rs.getInt("MaxHealth") + "|" + rs.getInt("MaxAPs") + "|" + rs.getInt("ShotSkill") + "|" + rs.getInt("CombatSkill") + "|" + rs.getInt("Strength") + "|" + rs.getInt("MaxEnergy") + "|" + rs.getInt("MaxMorale") + "\n");
				}
				campunit.close();
				return str.toString(); 
			} else {
				return "Invalid login/password";
			}
		} finally {
			current_login.close();
		}
	}


	public static String SetUniverseSquadSelection(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException, UnsupportedEncodingException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			if (current_login.selectUser(login, pwd)) {
				int gameid = Integer.parseInt(hashmap.get("gid"));

				// todo - check there is still space to deploy

				String lines[] = hashmap.get("data").split("\n");
				int line = 0;
				while (line < lines.length) {
					String s = lines[line];
					if (s.length() > 0) {
						String data[] = s.split("\\|");
						int uniunitid = Integer.parseInt(data[0]);
						int unitid = UniverseUnitsTable.CreateStdUnitFromUniverseUnit(dbs, current_login.getID(), uniunitid, gameid);
						if (unitid > 0) {
							int num_eq = Integer.parseInt(data[1]);
							if (num_eq > 0) {
								for (int d=2 ; d<data.length ; d++)  {
									int equipment_type_id = Integer.parseInt(data[d]);
									int ammo = EquipmentTypesTable.GetAmmoCapacityFromEquipmentType(dbs, equipment_type_id); 
									EquipmentTable.CreateEquipment(dbs, gameid, unitid, equipment_type_id, ammo);
									// Remove equipment from their inventory
									UniversePlayersEquipmentTable.DeductEquipment(dbs, current_login.getID(), equipment_type_id);
								}
							}

						}
					}
					line++;
				}
				return "";

			} else {
				return "Invalid login/password";
			}
		} finally {
			current_login.close();
		}
	}


	public static String GetUniInventoryData(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException, UnsupportedEncodingException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			if (current_login.selectUser(login, pwd)) {

				// Check if they have any inventory
				if (current_login.inventoryCreated() == false) {
					UniversePlayersEquipmentTable.CreateData(dbs, current_login.getID());
					current_login.setInventoryCreated();
				}

				StringBuffer str = new StringBuffer();
				str.append("UniversePlayersEquipmentID|EquipmentTypeID|Name|Qty\n");

				ResultSet rs = dbs.getResultSet("SELECT * FROM UniversePlayersEquipment WHERE LoginID = " + current_login.getID());
				EquipmentTypesTable eq = new EquipmentTypesTable(dbs);
				while (rs.next()) {
					eq.selectRow(rs.getInt("EquipmentTypeID"));
					str.append(rs.getInt("UniversePlayersEquipmentID") + "|" + rs.getInt("EquipmentTypeID") + "|" + eq.getName() + "|" + rs.getInt("Qty") + "\n");
				}
				eq.close();
				return str.toString(); 
			} else {
				return "Invalid login/password";
			}
		} finally {
			current_login.close();
		}
	}


	public static String GetTreatyData(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException, UnsupportedEncodingException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			if (current_login.selectUser(login, pwd)) {
				StringBuffer str = new StringBuffer();
				str.append("UniverseTreatyID|ComradeLoginID|ComradeName|Accepted\n");
				String sql = "SELECT * FROM UniverseTreaties WHERE Login1ID = " + current_login.getID() + " OR Login2ID = " + current_login.getID();
				ResultSet rs = dbs.getResultSet(sql);
				while (rs.next()) {
					int loginid = rs.getInt("Login1ID");
					if (loginid == current_login.getID()) {
						loginid = rs.getInt("Login2ID");
					}
					str.append(rs.getInt("UniverseTreatyID") + "|" + loginid + "|" + LoginsTable.GetDisplayName(dbs, loginid) + "|" + rs.getInt("Accepted") + "\n");
				}
				return str.toString();
			} else {
				return "Invalid login/password";
			}
		} finally {
			current_login.close();
		}
	}


	public static String OfferTreaty(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException, UnsupportedEncodingException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			if (current_login.selectUser(login, pwd)) {
				int login2id = Integer.parseInt(hashmap.get("login2id"));
				UniverseTreatiesTable.OfferTreaty(dbs, current_login.getID(), login2id);
				return "";
			} else {
				return "Invalid login/password";
			}
		} finally {
			current_login.close();
		}

	}


	public static String AcceptTreaty(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException, UnsupportedEncodingException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			if (current_login.selectUser(login, pwd)) {
				int tid = Integer.parseInt(hashmap.get("tid"));
				UniverseTreatiesTable.AcceptTreaty(dbs, tid);
				return "";
			} else {
				return "Invalid login/password";
			}
		} finally {
			current_login.close();
		}

	}


	public static String DeclineTreaty(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException, UnsupportedEncodingException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			if (current_login.selectUser(login, pwd)) {
				int tid = Integer.parseInt(hashmap.get("tid"));
				UniverseTreatiesTable.DeclineTreaty(dbs, tid);
				return "";
			} else {
				return "Invalid login/password";
			}
		} finally {
			current_login.close();
		}

	}


	public static String CancelTreaty(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException, UnsupportedEncodingException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			if (current_login.selectUser(login, pwd)) {
				int tid = Integer.parseInt(hashmap.get("tid"));
				UniverseTreatiesTable.CancelTreaty(dbs, tid);
				return "";
			} else {
				return "Invalid login/password";
			}
		} finally {
			current_login.close();
		}

	}


	public static String GetContractsData(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException, UnsupportedEncodingException {
		//todo
		return "";
	}



}
