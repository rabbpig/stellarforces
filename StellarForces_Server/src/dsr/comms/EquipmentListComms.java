package dsr.comms;

import java.sql.ResultSet;
import java.sql.SQLException;

import ssmith.dbs.MySQLConnection;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;

public class EquipmentListComms {

	// Called by the server
	public static String GetEquipmentListAsResponse(MySQLConnection dbs, String login, String pwd, int gameid) throws SQLException {
		GamesTable game = new GamesTable(dbs);
		game.selectRow(gameid);

		LoginsTable login_table = new LoginsTable(dbs);
		login_table.selectUser(login, pwd);

		int our_side = game.getSideFromPlayerID(login_table.getID());

		/*boolean restricted = false;
		try {
			if (game.isCampGame()) {
				if (FactionsTable.IsBestFaction(dbs, login_table.getFactionID()) == false) {
					restricted = true;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}*/

		AbstractMission mission = AbstractMission.Factory(game.getMissionID());

		StringBuffer str = new StringBuffer();
		str.append("EquipmentTypeID|MajorTypeID|Name|Code|Weight|AimedShotAccuracy|AimedShotAPCost|SnapShotAccuracy|SnapShotAPCost|AutoShotAccuracy|AutoShotAPCost");
		str.append("|ShotDamage|CCDamage|AmmoCapacity|ReloadAPCost");
		str.append("|AmmoTypeID|Explodes|ExplosionRad"); 
		str.append("|ExplosionDamage|Cost|CCAccuracy|Description|Indestructable|NoiseRange|MajorType\n");

		// Loop through equipment!
		String sql = "SELECT * FROM EquipmentTypes WHERE CanBuy = 1";
		/*if (restricted) {
			sql = sql + " AND COALESCE(RestrictedToWinners, 0) = 0";
		}*/
		sql = sql + " ORDER BY MajorTypeID, Cost";
		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) {
			if (mission.isItemTypeAllowed(rs.getInt("MajorTypeID"), our_side)) {
				if (mission.isItemAllowed(rs.getString("Code"), our_side)) {
					str.append(rs.getInt("EquipmentTypeID") + "|" + rs.getInt("MajorTypeID") + "|" + rs.getString("Name") + "|" + rs.getString("Code"));
					str.append("|" + rs.getInt("Weight") + "|" + rs.getInt("AimedShotAccuracy") + "|" + rs.getInt("AimedShotAPCost") + "|" + rs.getInt("SnapShotAccuracy") + "|" + rs.getInt("SnapShotAPCost") + "|" + rs.getInt("AutoShotAccuracy") + "|" + rs.getInt("AutoShotAPCost"));
					str.append("|" + rs.getInt("ShotDamage") + "|" + rs.getInt("CCDamage") + "|" + rs.getInt("AmmoCapacity") + "|" + rs.getInt("ReloadAPCost"));
					str.append("|" + rs.getInt("AmmoTypeID") + "|" + rs.getInt("Explodes") + "|" + rs.getInt("ExplosionRad")); 
					str.append("|" + rs.getInt("ExplosionDamage") + "|" + rs.getInt("Cost") + "|" + rs.getInt("CCAccuracy") + "|" + rs.getString("Description") + "|" + rs.getInt("Indestructable"));
					str.append("|" + rs.getInt("NoiseRange") + "|" + EquipmentTypesTable.GetMajorTypeFromID((byte)rs.getInt("MajorTypeID")) + "\n");
				}
			}

		}
		login_table.close();
		game.close();
		rs.close();
		return str.toString();

	}


	// Called by the server
	public static String GetArmourListAsResponse(MySQLConnection dbs, String login, String pwd, int gameid) throws SQLException {
		/*boolean restricted = false;
		try {
			GamesTable game = new GamesTable(dbs);
			if (game.doesRowExist(gameid)) {
				game.selectRow(gameid);
				if (game.isCampGame()) {
					LoginsTable login_table = new LoginsTable(dbs);
					login_table.selectUser(login, pwd);
					if (FactionsTable.IsBestFaction(dbs, login_table.getFactionID()) == false) {
						restricted = true;
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}*/

		StringBuffer str = new StringBuffer();
		str.append("ArmourTypeID|Name|Protection|Weight|Cost\n");

		// Loop through equipment!
		String sql = "SELECT * FROM ArmourTypes";
		/*if (restricted) {
			sql = sql + " WHERE COALESCE(RestrictedToWinners, 0) = 0";
		}*/
		ResultSet rs = dbs.getResultSet(sql);
		while (rs.next()) {
			str.append(rs.getInt("ArmourTypeID") + "|" + rs.getString("Name"));
			str.append("|" + rs.getInt("Protection") + "|" + rs.getInt("Weight") + "|" + rs.getInt("Cost") + "\n");
		}
		rs.close();
		return str.toString();

	}

}
