package dsr.comms;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import ssmith.util.MyList;
import dsrwebserver.pages.dsr.campaign;
import dsrwebserver.tables.CampUnitsTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.LoginsTable;

public class CampaignComms {

	public static String JoinCampaign(MySQLConnection dbs, String login, String pwd) throws SQLException, UnsupportedEncodingException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			if (current_login.selectUser(login, pwd)) {
				if (current_login.canPlayInCampaign() == 1 && current_login.getFactionID() <= 0) {
					int fid = current_login.joinFaction();
					return ""+fid;
				} else {
					return "You cannot join a new faction.";
				}
			} else {
				return "Invalid login/password";
			}
		} finally {
			current_login.close();
		}
	}


	public static String GetPlayersCampSquadList(MySQLConnection dbs, String login, String pwd) throws SQLException, UnsupportedEncodingException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
		if (current_login.selectUser(login, pwd)) {
			StringBuffer str = new StringBuffer();
			str.append("CampUnitID|Name|MaxHealth|MaxAPs|ShotSkill|CombatSkill|Strength|MaxEnergy|MaxMorale\n");

			ResultSet rs = dbs.getResultSet("SELECT * FROM CampUnits WHERE OwnerID = " + current_login.getID() + " AND COALESCE(UnitID, 0) = 0 ORDER BY OrderBy");
			CampUnitsTable campunit = new CampUnitsTable(dbs);
			while (rs.next()) {
				campunit.selectRow(rs.getInt("CampUnitID"));
				str.append(rs.getInt("CampUnitID") + "|" + campunit.getName() + "|" + rs.getInt("MaxHealth") + "|" + rs.getInt("MaxAPs") + "|" + rs.getInt("ShotSkill") + "|" + rs.getInt("CombatSkill") + "|" + rs.getInt("Strength") + "|" + rs.getInt("MaxEnergy") + "|" + rs.getInt("MaxMorale") + "\n");
			}
			campunit.close();
			rs.close();
			return str.toString(); 
		} else {
			return "Invalid login/password";
		}
		} finally {
			current_login.close();
		}
	}


	public static String SetPlayersCampSquad(MySQLConnection dbs, String login, String pwd, HashMap<String, String> hashmap) throws SQLException, FileNotFoundException, IOException {
		LoginsTable current_login = new LoginsTable(dbs);
		try {
			if (current_login.selectUser(login, pwd)) {
				MyList<Integer> ids = MyList.CreateFromCSVInts(hashmap.get("data"));
				GamesTable game = new GamesTable(dbs);
				game.selectRow(Functions.ParseInt(hashmap.get("gameid")));
				int side = Functions.ParseInt(hashmap.get("side"));
				campaign.CreateUnits(dbs, current_login, game, side, ids);
				return "";
			} else {
				return "Invalid login/password";
			}
		} finally {
			current_login.close();
		}
	}

}
