package dsr;

import com.jme.math.Vector3f;
import com.jme.renderer.Camera;

public final class CameraController implements IUpdatable {

	private static final float MAX_DIST = 5f;
	private static final float SPEED = .03f;

	private Camera cam;
	private Vector3f target_pos;
	private boolean needs_to_move = false;

	public CameraController(Camera _cam) {
		super();

		cam = _cam;
		target_pos = cam.getLocation().clone();
	}

	
	public void setTarget(Vector3f v) {
		this.setTarget(v, false);
	}

	
	public void setTarget(Vector3f pos, boolean now) {
		Vector3f v2 = pos.clone();
		this.target_pos = v2;

		if (now) {
			this.cam.setLocation(v2);
			needs_to_move = false;
			cam.update();
		} else {
			needs_to_move = true;
		}
	}

	
	public void update(float interpolation) {
		if (needs_to_move) {
			if (target_pos.distance(cam.getLocation()) > MAX_DIST) {
				Vector3f diff = target_pos.subtract(cam.getLocation()).normalizeLocal();
				diff.mult(SPEED);
				this.cam.getLocation().addLocal(diff);
			} else {
				cam.setLocation(target_pos.clone());
				needs_to_move = false;
			}
			this.cam.update();
		}
	}

}
