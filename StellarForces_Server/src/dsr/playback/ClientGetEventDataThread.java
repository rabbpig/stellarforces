package dsr.playback;

import java.util.ArrayList;

import ssmith.lang.Functions;
import dsr.AppletMain;
import dsr.SharedStatics;
import dsr.comms.WGet4Client;

public class ClientGetEventDataThread extends Thread {

	public ArrayList<ClientPlaybackTurnData> al_event_data = new ArrayList<ClientPlaybackTurnData>();
	private int max_turns, game_id, login_id, turn_no = 0;
	private AppletMain app;

	public ClientGetEventDataThread(AppletMain _app, int gid, int lid, int _max_turns) {
		super("GetEventDataThread");

		app = _app;
		game_id = gid;
		login_id = lid;
		max_turns = _max_turns;

		this.setDaemon(true);
		start();
	}


	public void run() {
		try {
			while (turn_no <= max_turns) {
				WGet4Client wg = new WGet4Client(app, WGet4Client.T_PLAYBACK, SharedStatics.URL_FOR_CLIENT + "/appletcomm/PlaybackComms3D.cls", ClientTurnDataCommFunctions.GetTurnDataRequest(game_id, login_id, turn_no), true);
				String response = wg.getResponse();
				ArrayList<ClientPlaybackTurnData> turn_data = ClientTurnDataCommFunctions.DecodeResponse(response);
				al_event_data.addAll(turn_data);

				Functions.delay(2000);
				turn_no++; // Get turn 0 first just in case
			}
		} catch (Exception ex) {
			AppletMain.HandleError(null, ex);
		}
	}

}
