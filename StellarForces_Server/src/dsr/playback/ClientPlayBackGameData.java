package dsr.playback;

import dsr.data.UnitData;
import dsrwebserver.maps.ServerMapSquare;

public class ClientPlayBackGameData {
	
	// Most of these fields are ignored but keep them here so we can use the
	// same server code as appletplayback to get the fields we do need.
	public int max_turns, max_events, total_sides, map_width;
	public UnitData units_DO_NOT_USE[];
	public ServerMapSquare map[][];

	public ClientPlayBackGameData() {
		super();
	}

}
