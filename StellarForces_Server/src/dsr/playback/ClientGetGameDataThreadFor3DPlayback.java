package dsr.playback;

import dsr.AppletMain;
import dsr.SharedStatics;
import dsr.comms.WGet4Client;

public class ClientGetGameDataThreadFor3DPlayback extends Thread {
	
	public ClientPlayBackGameData game_data;
	private int game_id;
	private AppletMain app;
	
	public ClientGetGameDataThreadFor3DPlayback(AppletMain _app, int gid) {
		super("ClientGetGameDataThreadFor3DPlayback");
		this.setDaemon(true);
		
		app = _app;
		game_id = gid;
		
		start();
	}
	
	public void run() {
		try {
			WGet4Client wg = new WGet4Client(app, WGet4Client.T_PLAYBACK, SharedStatics.URL_FOR_CLIENT + "/appletcomm/PlaybackComms3D.cls", ClientGameDataCommFunctions.GetGameDataRequest(game_id), true);
			String response = wg.getResponse();
			game_data = ClientGameDataCommFunctions.DecodeResponse(response);
			
		} catch (Exception ex) {
			AppletMain.HandleError(null, ex);
		}
	}

}
