package dsr.playback;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import ssmith.dbs.MySQLConnection;
import ssmith.lang.Functions;
import dsr.data.UnitData;
import dsrwebserver.DSRWebServer;
import dsrwebserver.maps.ServerMapSquare;
import dsrwebserver.pages.appletcomm.PlaybackComms;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

/**
 * This is for the applet in the web site!
 *
 */
public class ClientGameDataCommFunctions {

	// Called by the applet to request the data
	public static String GetGameDataRequest(int game_id) {
		return "cmd=" + PlaybackComms.GET_PLAYBACK_GAME_DATA + "&gid=" + game_id;
	}

	// --------------------------------------------------------------------

	// Called by the Server to send the data
	public static String GetGameDataResponse_(MySQLConnection dbs, HashMap<String, String> hashmap) throws SQLException {
		int gid = Integer.parseInt(hashmap.get("gid"));
		//int lid = Integer.parseInt(hashmap.get("lid"));

		StringBuffer str = new StringBuffer();
		GamesTable game = new GamesTable(dbs);
		try {
		if (game.doesRowExist(gid)) {
			game.selectRow(gid);
			if (game.getGameStatus() == GamesTable.GS_FINISHED) { // Only view finished games for now
				// No probs
			} else if (game.getGameStatus() == GamesTable.GS_STARTED) { // Only view finished games for now
				// No probs
			} else {
				DSRWebServer.p("Game " + gid + " not started yet!");
				return null;
			}
			int max_turns = dbs.getScalarAsInt("SELECT MAX(TurnNo) FROM UnitHistory WHERE GameID = " + gid);

			str.append(max_turns + "|");
			str.append(dbs.getScalarAsInt("SELECT Count(*) FROM UnitHistory WHERE GameID = " + gid) + "|");
			str.append(game.getNumOfSides() + "|");
			str.append(dbs.getScalarAsInt("SELECT Width FROM MapData WHERE MapDataID = " + game.getMapDataID()) + "|");
			str.append(dbs.getScalarAsInt("SELECT Count(*) FROM Units WHERE GameID = " + gid) + "|");

			// Get Units
			ResultSet rs = dbs.getResultSet("SELECT * FROM Units WHERE GameID = " + gid + " ORDER BY Side, OrderBy");
			while (rs.next()) {
				str.append(rs.getInt("UnitID") + "|" + rs.getInt("Side") + "|" + rs.getInt("OrderBy") + "|" + rs.getInt("MapX") + "|" + rs.getInt("MapY") + "|" + rs.getInt("Angle") + "|");
			}
			rs.close();

			str.append("start_of_map1|");
			// Map data
			int c = dbs.getScalarAsInt("SELECT Count(*) FROM MapDataSquares WHERE MapDataID = " + game.getMapDataID() + " And SquareType IN (" + MapDataTable.MT_WALL + ", " + MapDataTable.MT_COMPUTER + ", " + MapDataTable.MT_NOTHING + ")");
			str.append(c + "|");
			String sql = "SELECT MapX, MapY, SquareType FROM MapDataSquares WHERE MapDataID = " + game.getMapDataID() + " And SquareType IN (" + MapDataTable.MT_WALL + ", " + MapDataTable.MT_COMPUTER + ", " + MapDataTable.MT_NOTHING + ")";
			ResultSet rs_map = dbs.getResultSet(sql); 
			while (rs_map.next()) {
				int x = rs_map.getInt("MapX");
				int y = rs_map.getInt("MapY");
				str.append(x + "|" + y + "|" + rs_map.getInt("SquareType") + "|");
			}
			rs_map.close();

			// Destroyed map squares
			str.append("start_of_map2|");
			c = dbs.getScalarAsInt("SELECT Count(*) FROM UnitHistory WHERE EventType = " + UnitHistoryTable.UH_WALL_DESTROYED + " AND GameID = " + game.getID() + " AND OriginalSquareType = " + MapDataTable.MT_WALL);
			str.append(c + "|");
			rs = dbs.getResultSet("SELECT * FROM UnitHistory WHERE EventType = " + UnitHistoryTable.UH_WALL_DESTROYED + " AND GameID = " + game.getID() + " AND OriginalSquareType = " + MapDataTable.MT_WALL);
			while (rs.next()) {
				int x = rs.getInt("MapX");
				int y = rs.getInt("MapY");
				str.append(x + "|" + y + "|");
			}
			
		} else {
			DSRWebServer.p("Game " + gid + " does not exist!");
		}
		} finally {
			game.close();
		}
		return str.toString();
	}


	// --------------------------------------------------------------------

	// Called by the applet to decode the data
	public static ClientPlayBackGameData DecodeResponse(String response) {
		if (response.length() > 0) {
			ClientPlayBackGameData game_data = new ClientPlayBackGameData();
			String data[] = response.split("\\|");
			game_data.max_turns = Functions.ParseInt(data[0]);
			game_data.max_events = Functions.ParseInt(data[1]);
			game_data.total_sides = Functions.ParseInt(data[2]);
			game_data.map_width = Functions.ParseInt(data[3]);
			game_data.units_DO_NOT_USE = new UnitData[Functions.ParseInt(data[4])];
			int cell = 5;
			for (int i=0 ; i<game_data.units_DO_NOT_USE.length ; i++) {
				game_data.units_DO_NOT_USE[i] = new UnitData(Functions.ParseInt(data[cell+2]));
				game_data.units_DO_NOT_USE[i].setStatus(UnitsTable.ST_AWAITING_DEPLOYMENT);
				game_data.units_DO_NOT_USE[i].unitid = Functions.ParseInt(data[cell]);
				game_data.units_DO_NOT_USE[i].setSide(Functions.ParseByte(data[cell+1]));
				game_data.units_DO_NOT_USE[i].order_by = Functions.ParseByte(data[cell+2]);
				game_data.units_DO_NOT_USE[i].map_x = Functions.ParseByte(data[cell+3]);
				game_data.units_DO_NOT_USE[i].map_z = Functions.ParseByte(data[cell+4]);
				game_data.units_DO_NOT_USE[i].angle= Functions.ParseInt(data[cell+5]);
				cell += 6;
			}

			game_data.map = new ServerMapSquare[game_data.map_width][game_data.map_width];
			for (byte y=0 ; y<game_data.map_width ; y++) {
				for (byte x=0 ; x<game_data.map_width ; x++) {
					game_data.map[x][y] = new ServerMapSquare(MapDataTable.MT_FLOOR, x, y, (byte)0, false); // Default to floor
				}			
			}

			// Walls
			//String check = data[cell];
			int squares = Functions.ParseInt(data[cell+1]);
			cell+=2;
			for (int i=0 ; i<squares ; i++) {
				game_data.map[Functions.ParseInt(data[cell])][Functions.ParseInt(data[cell+1])].major_type = Functions.ParseByte(data[cell+2]);
				cell += 3;
			}

			// Destroyed walls
			//String check2 = data[cell];
			squares = Functions.ParseInt(data[cell+1]);
			cell+=2;
			for (int i=0 ; i<squares ; i++) {
				game_data.map[Functions.ParseInt(data[cell])][Functions.ParseInt(data[cell+1])].major_type = MapDataTable.MT_WALL;
				cell += 2;
			}

			return game_data;
		} else {
			return null;
		}
	}

}
