package dsr.playback;

import com.jme.math.Vector3f;
import com.jme.scene.Spatial.CullHint;

import ssmith.lang.Functions;
import ssmith.lang.Geometry;
import dsr.AppletMain;
import dsr.data.EquipmentData;
import dsr.data.GameData;
import dsr.data.UnitData;
import dsr.effects.Bullet;
import dsr.models.units.AbstractCorpse;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

public class ClientPlaybackControllerThread extends Thread {
	
	private static final int DEF_LOGIN_ID = 2; 
	
	private AppletMain main;
	private ClientGetGameDataThreadFor3DPlayback game_data_thread;
	private ClientGetEventDataThread event_data_thread;
	private ClientPlayBackGameData playback_game_data;
	private int event_data_pos = 0;
	private GameData typical_game_data;
	public volatile boolean is_ready = false;
	
	public ClientPlaybackControllerThread(AppletMain m, GameData _game_data) {
		super("PlaybackControllerThread");
		
		main = m;
		typical_game_data = _game_data;
	}
	
	
	public void run() {
		game_data_thread = new ClientGetGameDataThreadFor3DPlayback(main, typical_game_data.game_id);
		while (game_data_thread.isAlive()) {
			Functions.delay(1000); // Wait until we've got the game data
		}
		playback_game_data = game_data_thread.game_data;
		
		event_data_thread = new ClientGetEventDataThread(main, typical_game_data.game_id, DEF_LOGIN_ID, playback_game_data.max_turns);
		
		main.addToHUD("Please Wait...");
		
		// Wait until client 3d window started
		while (main.equipment == null) { // main.getAppletStage() < AppletMain.APP_PROCESSING || 
			Functions.delay(500);
		}
		
		// Remove Corpses
		EquipmentData equipment[] = main.equipment;
		for(int i=0 ; i<equipment.length ; i++) {
			EquipmentData equip = equipment[i];
			if (equip.major_type >= EquipmentTypesTable.ET_HUMAN_CORPSE_1 && equip.major_type <= EquipmentTypesTable.ET_ALIEN_QUEEN_CORPSE) {// It's a corpse
				equip.destroyed = true; // Don't show it
			}
		}
		
		is_ready = true;

		// Wait until the 3D client has created the models etc..
		while (main.getAppletStage() != AppletMain.APP_STARTED) {
			Functions.delay(500);
		}
		
		int turn = 0;
		while (true) { // Main loop
			while (event_data_pos < this.event_data_thread.al_event_data.size()) { // Have we got any more turn data to get?
				ClientPlaybackTurnData turn_data = this.event_data_thread.al_event_data.get(event_data_pos);
				// process this piece of turn data
				boolean did_something_happen = false;
				try {
					did_something_happen = processEvent(turn_data);
				} catch (java.lang.IndexOutOfBoundsException ex) {
					ex.printStackTrace();
				}
				if (did_something_happen) { // Update graphics
					AppletMain.p("Shown event " + event_data_pos + ": " + turn_data.event_type);
					if (turn_data.turn_no != turn) {
						turn = turn_data.turn_no;
						main.addToHUD("Turn " + turn);
					}
					AppletMain.p("Pausing");
					Functions.delay(250);
				}
				event_data_pos++;
			}
			if (event_data_pos >= this.game_data_thread.game_data.max_events) {
				main.addToHUD("Playback finished");
				break;
			}
		}

	}

	/**
	 * Returns true if we pause after it.
	 */
	private boolean processEvent(ClientPlaybackTurnData turn_data) {
		UnitData unit = null;
		switch (turn_data.event_type) {
		case UnitHistoryTable.UH_UNIT_MOVEMENT:
			unit = UnitData.GetUnitDataFromID(main.units, turn_data.unit_id);

			// Have they changed?
			boolean changed = false;
			if (unit.angle != turn_data.angle || unit.getMapX() != turn_data.mapx || unit.getMapZ() != turn_data.mapy) {
				changed = true;
				unit.angle = turn_data.angle;
				unit.updateAngleFromUnitData(true);
				if (unit.getStatus() == UnitsTable.ST_AWAITING_DEPLOYMENT) {
					unit.setStatus(UnitsTable.ST_DEPLOYED); // Just in case
				}
				unit.updateModelFromUnitData(); // to update angle?
				
				unit.setMapLocation(main, turn_data.mapx, turn_data.mapy, null);
			}
			return changed;
		case UnitHistoryTable.UH_EXPLOSION:
			byte type = EquipmentTypesTable.ET_GRENADE; // Default in case data not filled in correctly
			if (turn_data.orig_sq_type > 0) { // Stores the explosion type
				type = turn_data.orig_sq_type;
			}
			main.showExplosion(turn_data.mapx, turn_data.mapy, turn_data.rad, type);
			return true;
		case UnitHistoryTable.UH_WALL_DESTROYED:
			return false;
		case UnitHistoryTable.UH_UNIT_KILLED:
			unit = UnitData.GetUnitDataFromID(main.units, turn_data.unit_id);//UnitData.GetUnitDataFromID(playback_game_data.units_unused, turn_data.unit_id);
			unit.map_x = turn_data.mapx;
			unit.map_z = turn_data.mapy;
			unit.angle = turn_data.angle;
			unit.setStatus(UnitsTable.ST_DEAD);
			if (unit.model != null) {
				//unit.model.removeFromParent();
				main.addObjectForRemoval(unit.model);
			}
			// Add corpse
			AbstractCorpse corpse = AbstractCorpse.CorpseFactory(main, unit);
			if (corpse != null) {
				main.attachToRootNode(corpse, true);
			}
			return false;
		case UnitHistoryTable.UH_SHOT_FIRED:
			unit = UnitData.GetUnitDataFromID(main.units, turn_data.unit_id);//UnitData.GetUnitDataFromID(this.playback_game_data.units_unused, turn_data.unit_id);
			// Update the unit just in case
			unit.map_x = turn_data.mapx;
			unit.map_z = turn_data.mapy;
			int ang = turn_data.angle;
			if (ang == 0) {
				ang = unit.angle; // Default, in case data not set
			}
			int length = turn_data.rad;
			if (length == 0) {
				length = 2; // Default, in case data not set
			}
			Vector3f end = Geometry.GetVectorFromAngle(ang);
			end.multLocal(length).addLocal(unit.model.getWorldTranslation());
			new Bullet(main, unit.model.getWorldTranslation(), end, false);
			return false;
		case UnitHistoryTable.UH_ITEM_THROWN:
			unit = UnitData.GetUnitDataFromID(main.units, turn_data.unit_id);//UnitData.GetUnitDataFromID(this.playback_game_data.units_unused, turn_data.unit_id);
			// Update the unit
			unit.map_x = turn_data.mapx;
			unit.map_z = turn_data.mapy;
			ang = turn_data.angle;
			if (ang == 0) {
				ang = unit.angle;
			}
			length = turn_data.rad;
			if (length == 0) {
				length = 2; // Default
			}
			//length = length * sq_size;
			// this.graphics.add(new ThrowingGraphic(turn_data.mapx*sq_size+(sq_size/2), turn_data.mapy*sq_size+(sq_size/2), ang, length));
			return false;
		case UnitHistoryTable.UH_UNIT_DEPLOYED:
			unit = UnitData.GetUnitDataFromID(main.units, turn_data.unit_id);//UnitData.GetUnitDataFromID(playback_game_data.units_unused, turn_data.unit_id);
			unit.map_x = turn_data.mapx;
			unit.map_z = turn_data.mapy;
			unit.angle = turn_data.angle;
			unit.setStatus(UnitsTable.ST_DEPLOYED);
			if (unit.model != null) {
				unit.model.setCullHint(CullHint.Inherit); // Now show it
			}
			unit.updateModelFromUnitData();
			return false;
		case UnitHistoryTable.UH_UNIT_ESCAPED:
			unit = UnitData.GetUnitDataFromID(main.units, turn_data.unit_id);//UnitData.GetUnitDataFromID(playback_game_data.units_unused, turn_data.unit_id);
			unit.setStatus(UnitsTable.ST_ESCAPED);
			return false;
		case UnitHistoryTable.UH_COMPUTER_DESTROYED:
			playback_game_data.map[turn_data.mapx][turn_data.mapy].destroyed = 1;
			return false;
		case UnitHistoryTable.UH_FLAG_LOCATION:
			return false;
		case UnitHistoryTable.UH_DOOR_OPENED:
			return false;
		case UnitHistoryTable.UH_DOOR_CLOSED:
			return false;
		case UnitHistoryTable.UH_GRENADE_PRIMED:
			return false;
		case UnitHistoryTable.UH_BLOB_SPLIT:
			return false;
		default:
			//throw new RuntimeException("Unknown event type:" + turn_data.event_type);
			System.err.println("Unknown event type: " + turn_data.event_type);
			return false;
		}

	}


}
