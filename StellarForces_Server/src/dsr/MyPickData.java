package dsr;

import com.jme.intersection.PickData;
import com.jme.math.Ray;
import com.jme.scene.Geometry;

public class MyPickData extends PickData {
	
	public MyPickData(Ray ray, Geometry targetMesh, boolean checkDistance) {
		super(ray, targetMesh, null, checkDistance);
	}

	
	public void setDistance(float d) {
		this.distance = d;
	}
	
	
	public String toString() {
		return super.getTargetMesh().toString() + " (" + distance + ")";
	}


}
