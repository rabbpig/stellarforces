package dsr;

import dsr.start.StartupNew;
import dsrwebserver.tables.GamesTable;
import java.util.ArrayList;

public final class Tutorial {

	private AppletMain main;
	private int stage = 0;
	private ArrayList<String> text = new ArrayList<String>();

	public Tutorial(AppletMain m) {
		main = m;

		text.add(StartupNew.strings.getTranslation("Welcome to the tutorial.  Press T or click on the Tutorial icon at any time to go to the next stage."));
		text.add(StartupNew.strings.getTranslation("You can move the camera with the keys W, A, S, and D."));
		//text.add(StartupNew.strings.getTranslation("Change the direction of the camera by holding the left mouse button and moving the mouse."));
		text.add(StartupNew.strings.getTranslation("Select icons by clicking on them with the left mouse button."));
		if (main.game_data.game_status == GamesTable.GS_CREATED_DEPLOYMENT) {
			text.add(StartupNew.strings.getTranslation("To deploy the next unit, right-click on a deployment square (marked with 'D's)."));
		} else {
			text.add(StartupNew.strings.getTranslation("You can move a unit using the keys on the numpad or icons on the bottom-right of the screen."));
			text.add(StartupNew.strings.getTranslation("The movement keys are:- Fwd: 8, Back: 2, Turn Left: 7, Turn Right: 9"));
			text.add(StartupNew.strings.getTranslation("You can select units by pressing N or P for Next/Previous."));
			text.add(StartupNew.strings.getTranslation("All the keyboard shortcuts are on the website under the Controls on the Help menu."));
			text.add(StartupNew.strings.getTranslation("You can also select units by right-clicking on them."));
			text.add(StartupNew.strings.getTranslation("Each unit has a certain amount of Action Points (APs) to spend during each turn."));
			text.add(StartupNew.strings.getTranslation("All actions, such as moving or shooting, cost a certain amount of APs."));
			text.add(StartupNew.strings.getTranslation("Once a unit has run out of APs, they can't do anything else until the next turn."));
			if (main.getCurrentUnit() != null) {
				if (main.getCurrentUnit().current_item == null) {
					text.add(StartupNew.strings.getTranslation("Equip a unit by clicking on Equip and then select the item."));
				}
			}
			text.add(StartupNew.strings.getTranslation("You can only see enemy units that have been seen by one of your units."));
			text.add(StartupNew.strings.getTranslation("If one of your units sees any enemy unit, an icon will appear.  Click on this icon to centre the camera on the enemy."));
			if (main.getCurrentUnit() != null) {
				if (main.getCurrentUnit().current_item != null) {
					text.add(StartupNew.strings.getTranslation("To shoot, click on the Shoot icon."));
					text.add(StartupNew.strings.getTranslation("The white line will show the direction you wish your unit to shoot in."));
					text.add(StartupNew.strings.getTranslation("You can move the white line by moving the camera."));
					text.add(StartupNew.strings.getTranslation("To make a shot, click on any 3 of the shot type icons: Aimed Shot, Snap Shot or Auto Shot."));
					text.add(StartupNew.strings.getTranslation("Aimed shots are more accurate but cost more APs, Auto Shots are much less accurate but don't cost as much."));
					text.add(StartupNew.strings.getTranslation("If you shoot an enemy unit, they will be wounded and maybe killed."));
				}
			}
			text.add(StartupNew.strings.getTranslation("You can see a picture of the map by clicking on Scanner.  This will show all your units and all visible enemy units."));
			text.add(StartupNew.strings.getTranslation("When you have finished moving your units, select 'End Turn' then 'End Turn Now' to confirm.  You may need to click on Cancel first if you have a unit selected."));
			text.add(StartupNew.strings.getTranslation("You can identify items of equipment that are on the floor (shown as brown crates) by right-clicking on them."));
			text.add(StartupNew.strings.getTranslation("Units can perform other actions, such as throwing items, priming grenades or opening doors, by clicking on the relevant icons."));
			text.add(StartupNew.strings.getTranslation("When you end your turn, your opponent will receive an email informing them that they can take their turn."));
			text.add(StartupNew.strings.getTranslation("You will receive the same email when your opponent has taken their turn."));
			text.add(StartupNew.strings.getTranslation("After your opponent's turn, visit the website to see if your units spotted any enemy units (and other details)."));
			text.add(StartupNew.strings.getTranslation("Depending on the mission, you can win by killing all of your opponents units or completing your objective first."));
		}
		text.add(StartupNew.strings.getTranslation("More instructions can be found on the website and wiki."));
	}


	public String getText() {
		if (stage < text.size()) {
			String s = text.get(stage);
			stage++;
			return s;
		} else {
			return StartupNew.strings.getTranslation("That is the end of the tutorial.  If you have any problems or questions either see the website or send an email.");
		}
	}

}
