package dsr;

import ssmith.lang.Geometry;

import com.jme.bounding.BoundingBox;
import com.jme.math.Ray;
import com.jme.math.Vector3f;
import com.jme.scene.Line;
import com.jme.scene.Node;
import com.jme.scene.TriMesh;

import dsr.models.units.AbstractUnit;
import dsr.models.units.Blob;
import dsr.models.units.QueenAlien;
import dsr.models.units.Tyrant;

public abstract class GameObject extends Node {

	public static final int VIEW_ANGLE = 55;

	protected static final boolean COLLIDES = true;
	protected static final boolean DOESNT_COLLIDE = false;
	protected static final boolean BLOCKS_VIEW = true;
	protected static final boolean DOESNT_BLOCK_VIEW = false;
	protected static final boolean CAN_BE_SHOT = true;
	protected static final boolean CANT_BE_SHOT = false;

	private static final long serialVersionUID = 1L;

	public float y_off; // The height of the object to make it stand on the floor.
	public AppletMain main;
	public TriMesh collider; // All instances must implement ICollider!
	public boolean blocks_view, can_be_shot;

	public GameObject(AppletMain m, String name, boolean collides, boolean _can_be_shot, boolean _blocks_view, float _x, float _z, float _y_off) {
		super(name);
		main = m;
		blocks_view = _blocks_view;
		can_be_shot = _can_be_shot;

		this.getLocalTranslation().x = _x;
		this.getLocalTranslation().y = _y_off;
		this.getLocalTranslation().z = _z;

		y_off = _y_off;

		super.setIsCollidable(collides);
	}


	protected void addAxis() {
		// Add axis lines
		Vector3f[] vertex = new Vector3f[2];
		vertex[0] = new Vector3f(0, 0, 0);
		vertex[1] = new Vector3f(10, 0, 0);
		Line l = new Line("LinesX", vertex, null, null, null);
		l.setIsCollidable(false);
		l.setRandomColors();
		l.setModelBound(new BoundingBox());
		l.updateModelBound();
		this.attachChild(l);

		vertex = new Vector3f[2];
		vertex[0] = new Vector3f(0, 0, 0);
		vertex[1] = new Vector3f(0, 10, 0);
		l = new Line("LinesY", vertex, null, null, null);
		l.setIsCollidable(false);
		l.setModelBound(new BoundingBox());
		l.updateModelBound();
		this.attachChild(l);

		vertex = new Vector3f[2];
		vertex[0] = new Vector3f(0, 0, 0);
		vertex[1] = new Vector3f(0, 0, 10);
		l = new Line("LinesZ", vertex, null, null, null);
		l.setRandomColors();
		l.setIsCollidable(false);
		l.setModelBound(new BoundingBox());
		l.updateModelBound();
		this.attachChild(l);
	}


	public float getDistTo(GameObject go, boolean check_y) {
		return getDistTo(go.getWorldTranslation(), check_y);// this.getLocalTranslation().distance(go.getLocalTranslation());
	}


	public float getDistTo(Vector3f v, boolean check_y) {
		if (check_y == true) {
			return this.getLocalTranslation().distance(v);
		} else {
			return (float)Geometry.distance(this.getLocalTranslation().x, this.getLocalTranslation().z, v.x, v.z);
		}
	}


	public Vector3f getDirTo(GameObject go) {
		Vector3f vect = new Vector3f();
		vect.set(go.getLocalTranslation().subtract(this.getLocalTranslation()).normalize());
		return vect;
	}


	public Vector3f getDirTo(Vector3f v) {
		Vector3f vect = new Vector3f();
		vect.set(v.subtract(this.getLocalTranslation()).normalize());
		return vect;
	}


	/*	public boolean canSee(GameObject other, int ang_or_minus_one, boolean do_units_block) {
		return canSee(other, ang_or_minus_one, do_units_block, null);
	}


	/*public boolean canSee(GameObject other, int ang_or_minus_one, boolean do_units_block, Class ignore_classes[]) {
		if (ang_or_minus_one >= 0) {
			int ang_to_target = this.getAbsoluteAngleTo(other.getWorldTranslation());
			float diff = Geometry.GetDiffBetweenAngles(ang_or_minus_one, ang_to_target);
			if (diff > VIEW_ANGLE) {
				return false;
			}
		}
		// If they are within one square, just show them (there are slight problems with corners of walls)
		if (this.getDistTo(other) <= 1.5f) {
			return true;
		}

		Vector3f dir = other.getLocalTranslation().subtract(this.getLocalTranslation());
		dir.y = 0; // Make the ray horizontal
		dir.normalizeLocal();
		Ray r = new Ray(this.getLocalTranslation().clone(), dir);
		r.origin.y = 0.2f;// To make sure we can see small objects with low colliders!
		MyPickResults can_see_results = new MyPickResults(this.collider);
		can_see_results.clear();
		main.getRootNode().findPick(r, can_see_results);
		if (can_see_results.getNumber() > 0) {
			for (int i=0 ; i<can_see_results.getNumber() ; i++) {
				GameObject obj = can_see_results.getGameObject(i);
				// Check for ignorable objects
				if (ignore_classes != null) {
					for (int c=0 ; c<ignore_classes.length ; c++) {
						Class klass = ignore_classes[c];
						if (obj.getClass().equals(klass)) {
							continue;
						}
					}
				}
				if (obj == other) {
					return true;
				} else if (obj.blocks_view) {
					return false;
				} else if (do_units_block && obj instanceof AbstractUnit) {
					return false;
				}
			}
		}
		return false;

	}
	 */

	public boolean canSee(GameObject other, int ang_or_minus_one, boolean do_units_block) {
		return canSee(other, ang_or_minus_one, do_units_block, null, 0);
	}


	public boolean canSee(GameObject other, int ang_or_minus_one, boolean do_units_block, int max_block_count) {
		return canSee(other, ang_or_minus_one, do_units_block, null, max_block_count);
	}


	public boolean canSee(GameObject other, int ang_or_minus_one, boolean do_units_block, Class ignore_classes[], int max_block_count) {
		return getCanSeeCount(other, ang_or_minus_one, do_units_block, ignore_classes, 0) == 0;
	}


	/**
	 * This returns the number of objects between the viewer and viewee.
	 * Returns -1 if the viewer CANNOT see the viewee (i.e. because not facing it)
	 * Returns 999 if there is nothing blocking it.
	 * 
	 * @param other
	 * @param ang_or_minus_one
	 * @param do_units_block
	 * @param ignore_classes
	 * @param block_count
	 * @return
	 */
	public int getCanSeeCount(GameObject other, int ang_or_minus_one, boolean do_units_block, Class ignore_classes[], int max_block_count) {
		int CANNOT_SEE = 999;
		int CAN_SEE = 0;

		if (ang_or_minus_one >= 0 && this instanceof Blob == false) { // Blobs can see 360, aliens 180
			int ang_to_target = this.getAbsoluteAngleTo(other.getWorldTranslation());
			float diff = Geometry.GetDiffBetweenAngles(ang_or_minus_one, ang_to_target);
			int field_of_vision = VIEW_ANGLE; 
			if (this instanceof Tyrant || this instanceof QueenAlien) {
				field_of_vision = field_of_vision * 2;
			}
			if (diff > field_of_vision) {
				return CANNOT_SEE;
			}
		}
		// If they are within one square, just show them (there are slight problems with corners of walls)
		if (this.getDistTo(other, false) <= 1.5f) {
			return CAN_SEE;
		}

		int block_count = 0;

		Vector3f dir = other.getLocalTranslation().subtract(this.getLocalTranslation());
		dir.y = 0; // Make the ray horizontal
		dir.normalizeLocal();
		Ray r = new Ray(this.getLocalTranslation().clone(), dir);
		r.origin.y = 0.2f;// To make sure we can see small objects with low colliders!
		MyPickResults can_see_results = new MyPickResults(this.collider);
		can_see_results.clear();
		main.getRootNode().findPick(r, can_see_results);
		if (can_see_results.getNumber() > 0) {
			for (int i=0 ; i<can_see_results.getNumber() ; i++) {
				GameObject obj = can_see_results.getGameObject(i); // can_see_results.getGameObject(2)
				// Check for ignorable objects
				if (ignore_classes != null) {
					boolean ignore_this = false;
					for (int c=0 ; c<ignore_classes.length ; c++) {
						Class klass = ignore_classes[c];
						if (obj.getClass().equals(klass)) {
							ignore_this = true;
							break;
						}
					}
					if (ignore_this) {
						continue;
					}
				}
				if (obj == other) {
					return block_count;
				} else if (obj.blocks_view) {
					max_block_count--;
					block_count++;
					if (max_block_count < 0) {
						return block_count;
					}
				} else if (do_units_block && obj instanceof AbstractUnit) {
					max_block_count--;
					block_count++;
					if (max_block_count < 0) {
						return block_count;
					}
				}
			}
		}
		return CANNOT_SEE;
	}


	/**
	 * This is used for checking which walls are hit by an explosion.
	 * 
	 */
	public GameObject getFirstObjectInWay(GameObject other, Class req_classes[]) {
		Vector3f dir = other.getLocalTranslation().subtract(this.getLocalTranslation());
		dir.y = 0; // Make the ray horizontal
		dir.normalizeLocal();
		Ray r = new Ray(this.getLocalTranslation().clone(), dir);
		r.origin.y = 0.2f;// To make sure we can see small objects with low colliders!
		MyPickResults can_see_results = new MyPickResults(this.collider);
		can_see_results.clear();
		main.getRootNode().findPick(r, can_see_results);
		if (can_see_results.getNumber() > 0) {
			for (int i=0 ; i<can_see_results.getNumber() ; i++) {
				GameObject obj = can_see_results.getGameObject(i); // can_see_results.getGameObject(3)
				// Check for req objects
				if (req_classes != null) {
					boolean found = false;
					for (int c=0 ; c<req_classes.length ; c++) {
						Class klass = req_classes[c];
						if (obj.getClass().equals(klass)) {
							found = true; // Need this cos we break out of two loops!
							break;
						}
					}
					if (found == false) {
						continue;
					}
				}
				return obj;
			}
		}
		return null;
	}


	public void highlight() {
		if (this.collider != null) {
			this.collider.setRandomColors();
			this.collider.updateRenderState();
		}
	}


	public int getAbsoluteAngleTo(Vector3f pos) {
		int x = (int)(pos.x - this.getWorldTranslation().x);
		int z = (int)(pos.z - this.getWorldTranslation().z);
		return (int) Math.toDegrees(Math.atan2(z, x)); //+ 90
	}


	public boolean isChanceofNotHitting() {
		return this.getChanceofNotHitting() > 0;
	}


	/**
	 * Override if req
	 * @return
	 */
	public int getChanceofNotHitting() {
		return 0;
	}

}
