package dsr.ai;

import dsr.AppletMain;
import dsr.data.AppletMapSquare;
import dsr.data.UnitData;
import dsrwebserver.tables.GamesTable;
import ssmith.lang.Functions;

public class AIThread extends Thread {

	private AppletMain main;

	public AIThread(AppletMain _main) {
		super();

		main = _main;

		this.setDaemon(true);
		this.setPriority(Thread.MIN_PRIORITY);
	}


	public void run() {
		if (main.game_data.game_status == GamesTable.GS_CREATED_DEPLOYMENT) {
			while (true) {
				main.getNextUnitToDeploy();
				if (main.next_to_deploy > 0) {
					while (true) { // Keep trying to find a depoy square
						byte x = Functions.rndByte(0, main.mapdata.getMapWidth()-1);
						byte z = Functions.rndByte(0, main.mapdata.getMapHeight()-1);
						AppletMapSquare sq = main.mapdata.getSq_MaybeNULL(x, z);
						if (sq != null) {
							if (main.game_data.areSidesFriends(sq.deploy_sq_side, main.game_data.our_side)) {
								if (main.deployUnit(x, z)) {
									main.lookAt(x, z);
									break;
								}
							}
						}
					}
				} else {
					break; // No more units
				}
			}
		} else if (main.game_data.game_status == GamesTable.GS_STARTED) {
			while (main.selectNextUnit()) {
				AppletMain.p("Selected " + main.getCurrentUnit().name);
				int turns = Functions.rnd(8, 15);
				for (int i=0 ; i<turns ; i++) {
					UnitData enemy = main.getCurrentUnit().getFirstVisibileEnemy();
					if (enemy != null) {
						main.makeOppFireShot(main.getCurrentUnit(), enemy);
					}
					main.getCurrentUnit().model.turnBy(45);
					Functions.delay(500);
				}
				Functions.delay(1000);
			}
			AppletMain.p("AI finished");
			main.showExplosionsAndEndTurn();
		}
	}

}
