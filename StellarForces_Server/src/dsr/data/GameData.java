package dsr.data;

import ssmith.util.MyList;

public final class GameData {

	public int game_id;
	public String gamecode, mission_name, our_name;
	private String opponents_names;
	public String[] players_name_by_side = new String[5]; // Starts at 1!
	public byte game_status, map_model_type;
	public byte turn_no, turn_side, max_turns; 
	public byte our_side, wall_type;
	public short mission_type;
	public byte is_snafu = 0, is_advanced = 0, num_players = 0;
	public byte show_ceilings; // Not used?
	public byte side_see_enemies[] = new byte[5];
	public byte opp_fire[] = new byte[5];
	public float min_client_version;
	public boolean enabled = true;
	public MyList<Byte> side_sides[] = new MyList[5]; // Starts at 1!
	public byte ai_for_side[] = new byte[5];
	public byte has_side_equipped[] = new byte[5]; // Starts at 1!
	public byte has_side_deployed[] = new byte[5]; // Starts at 1!
	public int forum_id, creds;
	public byte vps[] = new byte[5]; // Starts at 1!
	public String mission1liner;
	public String[] side_names = new String[5]; // Starts at 1! 

	public String GetSideNameFromSide(int side) {
		String result;
		if (side == our_side) {
			result = our_name;
		} else {
			result = players_name_by_side[side];
		}
		if (result != null) {
			return result;
		} else {
			return opponents_names;
		}
	}


	public String getFullTitle() {
		StringBuffer str = new StringBuffer();
		str.append(mission_name);
		str.append(" vs ");
		str.append(getOpponentsName());
		str.append(", turn ");
		str.append(turn_no);
		if (this.ai_for_side[our_side] > 0) {
			str.append(" - *AI Turn*");
		}
		return str.toString();
	}


	public String getOpponentsName() {
		if (opponents_names == null) {
			StringBuffer str = new StringBuffer();
			if (num_players == 1) {
				str.append("No-one");
			} else {
				for (int i=1 ; i < players_name_by_side.length ; i++) {
					if (i != our_side) {
						if (players_name_by_side[i] != null) {
							if (players_name_by_side[i].length() > 0) {
								str.append(players_name_by_side[i] + ", ");
							}
						}
					}
				}
				if (str.length() > 2) {
					str.delete(str.length()-2, str.length());
				} else {
					str.append("No-one");
				}
			}
			opponents_names = str.toString();
		}
		return opponents_names;
	}


	public boolean areSidesFriends(int s1, int s2) {
		if (s1 == s2) {
			return true;
		}
		if (s1 < 0 || s2 < 0) {
			return false;
		}
		if (this.side_sides[s1] != null) {
			return (this.side_sides[s1].contains(s2));
		} else {
			return false;
		}
	}


}
