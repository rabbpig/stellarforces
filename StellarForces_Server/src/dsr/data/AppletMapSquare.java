package dsr.data;

import java.util.ArrayList;

import dsr.GameObject;
import dsr.models.ComputerModel;
import dsr.models.map.SlidingDoor;
import dsrwebserver.maps.AbstractMapSquare;

public final class AppletMapSquare extends AbstractMapSquare {

	//private static final long serialVersionUID = 1L;

	public int sq_id;
	public SlidingDoor door;
	private ArrayList<EquipmentData> equipment;
	public ComputerModel cpu;
	public int smoke_type; // e.g. EquipmentTypesTable.ET_NERVE_GAS
	public int smoke_caused_by;
	public ArrayList<GameObject> threeds; // 3D objects attached to mapsquare

	public AppletMapSquare(int id, byte t, byte x, byte z, byte deploy, byte escape, short floor_tex, byte destroyed, byte _door_open, short raised_tex, byte _smoke_type, int _smoke_caused_by) {
		super(t, x, z, destroyed, _door_open == 1);

		sq_id = id;
		this.deploy_sq_side = deploy;
		this.escape_hatch_side = escape;
		this.texture_code = floor_tex;
		this.raised_texture_code = raised_tex;
		this.smoke_type = _smoke_type;
		this.smoke_caused_by = _smoke_caused_by;
		
		//if (AppletMain.CREATE_MAP_TEST) {
			threeds = new ArrayList<GameObject>();
		//}
	}


	public String toString() {
		return "Mapsquare (" + this.x + ", " + this.y + ")";
	}


	public void addEquipment(EquipmentData eq) {
		if (this.equipment == null) {
			this.equipment = new ArrayList<EquipmentData>(5);
		}
		this.equipment.add(eq);
	}


	public void removeEquipment(EquipmentData eq) {
		this.equipment.remove(eq);
		if (this.equipment.size() == 0) {
			this.equipment = null;
		}
	}


	public ArrayList<EquipmentData> getEquipment() {
		return this.equipment;
	}


	public boolean contains(EquipmentData eq) {
		if (this.equipment != null) {
			return this.equipment.contains(eq);
		} 
		return false;
	}


	public String getListOfEquipment(int advanced_mode, int side) {
		if (equipment != null) {
			StringBuffer str = new StringBuffer();
			for(int i=0 ; i<equipment.size() ; i++) {
				EquipmentData eq = equipment.get(i);
				// Only show if we can see it (if advanced mode) if (is_advanced == false || ) {
				if (advanced_mode != 1 || eq.seen_by_side[side] != 0) {
					str.append(eq.getName(false) + ", ");
				}
			}
			if (str.length() > 2) {
				str.delete(str.length()-2, str.length());
			}
			return str.toString();
		} else {
			return "";
		}
	}


	public boolean containsType(int type) {
		if (equipment != null) {
			for(int i=0 ; i<equipment.size() ; i++) {
				EquipmentData eq = equipment.get(i);
				if (eq.major_type == type) {
					return true;
				}
			}
		}
		return false;
	}


}
