package dsr.data;

import java.util.ArrayList;


import ssmith.lang.Geometry;

import com.jme.math.FastMath;
import com.jme.math.Vector3f;

import dsr.AppletMain;
import dsr.SharedStatics;
import dsr.SoundEffects;
import dsr.SpeechPlayer;
import dsr.comms.UnitDataComms;
import dsr.hud.AbstractIcon;
import dsr.models.units.AbstractCorpse;
import dsr.models.units.AbstractUnit;
import dsr.models.units.Blob;
import dsr.start.StartupNew;
import dsrwebserver.tables.UnitsTable;

public final class UnitData implements Cloneable {

	public int num, unitid;
	public byte model_type, order_by;
	private byte side;
	private byte status;
	public String name, unitcode;
	public int map_x, map_z;
	public int angle; // 0 is facing right!!
	public short aps, max_aps, opp_fire_aps_req, protection, armour_type_id;
	public byte can_deploy;
	public byte opp_fire_01;
	public short max_health, shot_skill;
	public short combat_skill, strength;
	private short health;
	public ArrayList<EquipmentData> items = new ArrayList<EquipmentData>();
	public EquipmentData current_item;
	public int current_item_id_TMP; // Only used when setting up the data!
	public boolean has_been_seen = false; // So we know if an enemy unit has been seen.
	public boolean can_use_equipment;
	public AbstractUnit model;
	public AbstractIcon icon;
	private Vector3f look_at; // This is used for the direction of walking!
	public ArrayList<UnitData> can_see_at_start = new ArrayList<UnitData>();
	private ArrayList<UnitData> can_see = new ArrayList<UnitData>(); // Which units can they see
	public byte opp_fire_type = OppFireTypes.SNAP_SHOT;
	public short curr_morale, curr_energy;
	public byte panicked;


	public UnitData(int n) {
		num = n;
	}


	public UnitData clone() {
		try {
			return (UnitData)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}


	public byte getSide() {
		return side;
	}


	public void setSide(byte b) {
		side = b;
	}


	public String toString() {
		return "Unit: " + name;
	}


	public void clearSeenUnits() {
		this.can_see.clear();

	}


	public void setCanSee(UnitData unit) {
		this.can_see.add(unit);
	}


	public boolean canUnitSee_Cached(UnitData unit) {
		return this.can_see.contains(unit);
	}


	public UnitData getFirstVisibileEnemy() {
		if (this.can_see.size() > 0) {
			return this.can_see.get(0);
		} else {
			return null;
		}
	}


	public int getStatus() {
		return this.status;
	}


	public void setStatus(byte i) {
		status = i;
	}


	public static UnitData GetUnitDataFromID(UnitData units[], int unitid) {
		for (int i=0 ; i<units.length ; i++) {
			UnitData unit = units[i];
			if (unit.unitid == unitid) {
				return unit;
			}
		}
		throw new RuntimeException("UnitData not found for unit id " + unitid);
	}


	public static UnitData GetUnitDataFromMapSq(UnitData units[], int mx, int my, byte side, boolean error) {
		for (int i=0 ; i<units.length ; i++) {
			UnitData unit = units[i];
			if (unit.map_x == mx && unit.map_z == my && (side <= 0 || unit.side == side)) {
				return unit;
			}
		}
		if (error) {
			throw new RuntimeException("UnitData not found at " + mx + ", " + my);
		} else {
			return null;
		}
	}


	public void damage(AppletMain main, short amt, UnitData by_unit, int form_of_attack) {
		if (isAlive()) {
			int by_side = -1;
			int by_unitid = -1;
			String by_name = "fate";
			if (by_unit != null) {
				by_side = by_unit.side;
				by_unitid = by_unit.unitid;
				by_name = by_unit.name;
			}
			boolean who_can_see[] = main.whichSidesCanSeeUnit(this);
			boolean can_we_see = who_can_see[main.game_data.our_side];
			if (amt > 0) {
				this.incHealth((short)-amt);
				if (this.health > 0) {
					if (can_we_see) {
						main.playSound(SoundEffects.HIT);
						if (by_unit != null) {
							if (amt <= SpeechPlayer.MINOR_INJURY) {
								if (by_unit.side == main.game_data.our_side) { // Only play if we've done the attack
									main.playSound(by_unit, SpeechPlayer.EV_INJURED_ENEMY);
								} else {
									main.playSound(this, SpeechPlayer.EV_RCVD_MINOR_INJURY);
								}

							} else {
								if (by_unit.side == main.game_data.our_side) { // Only play if we've done the attack
									main.playSound(by_unit, SpeechPlayer.EV_INJURED_ENEMY);
								} else {
									main.playSound(this, SpeechPlayer.EV_RCVD_MAJOR_INJURY);
								}
							}
						}
					}
					main.sendEventToServer_UnitWounded(this, by_side, by_unitid, form_of_attack, amt, who_can_see);
					this.model.anim_harmed();
				} else { // Killed!
					if (status != UnitsTable.ST_DEAD && status != UnitsTable.ST_NO_LONGER_EXISTS) {// In case they've already been killed (i.e. shot dead, then caught in their own death grenade)
						if (form_of_attack == UnitDataComms.FOA_MERGED) {
							this.status = UnitsTable.ST_NO_LONGER_EXISTS;
						} else {
							this.status = UnitsTable.ST_DEAD;
						}

						//boolean who_can_see[] = main.whichSidesCanSeeUnit(this);
						main.sendEventToServer_UnitKilled(this, by_side, by_unitid, form_of_attack, amt, who_can_see);

						main.checkForDeathGrenadesOrAlienExplode(this);// Must be before the unit is killed as items are dropped, and thus a death grenade won't be activated!
						main.unitKilled(this);
						this.health = 0;

						if (can_we_see) {
							if (by_unit != null) {
								if (by_unit.side == main.game_data.our_side) { // Only play if we've done the attack
									main.playSound(by_unit, SpeechPlayer.EV_KILLED_ENEMY);
								} else {
									main.playSound(this, SpeechPlayer.EV_UNIT_KILLED);
								}
							}
						}

						try {
							this.model.collider.removeFromParent();
							this.model.collider = null;
							this.model.removeFromParent();
							this.model = null;

							this.model = null;
							AbstractCorpse corpse = AbstractCorpse.CorpseFactory(main, this);
							if (corpse != null) {
								main.attachToRootNode(corpse, true);
							}
						} catch (Exception ex) {
							AppletMain.HandleError(main, ex);
						}
						main.recalcVisibleEnemiesAndOppFire(false, null); // NOTICE WE DON'T DO OPP FIRE SINCE THEY ALL SHOOT AGAIN, EVEN THOSE WITH NOTHING TO DO WITH THIS DEATH
						main.updateMenu(); // To remove the enemy unit icon
					}
				}
				if (can_we_see) {
					if (form_of_attack == UnitDataComms.FOA_EXPLOSION) {
						main.addToHUD(this.name + " " + StartupNew.strings.getTranslation("wounded ") + amt + " " + StartupNew.strings.getTranslation("by") + " " + StartupNew.strings.getTranslation("an explosion"));
					} else if (form_of_attack == UnitDataComms.FOA_NERVE_GAS) {
						main.addToHUD(this.name + " " + StartupNew.strings.getTranslation("wounded ") + amt + " " + StartupNew.strings.getTranslation("by") + " " + StartupNew.strings.getTranslation("nerve gas"));
					} else if (form_of_attack == UnitDataComms.FOA_FIRE) {
						main.addToHUD(this.name + " " + StartupNew.strings.getTranslation("wounded ") + amt + " " + StartupNew.strings.getTranslation("by") + " fire");
					} else if (form_of_attack == UnitDataComms.FOA_MERGED) {
						// Show nothing
					} else {
						main.addToHUD(this.name + " " + StartupNew.strings.getTranslation("wounded ") + amt + " " + StartupNew.strings.getTranslation("by") + " " + by_name);
					}
				}
				// Note that we must update the server AFTER we've updated the unit's data locally!
				main.updateUnitOnServer(this);
				if (can_we_see) {
					if (health <= 0) {
						// Write this after we've shown how much they were wounded by
						if (form_of_attack == UnitDataComms.FOA_MERGED) {
							main.addToHUD(name + " " + StartupNew.strings.getTranslation("has been absorbed"));
						} else {
							main.addToHUD(name + " " + StartupNew.strings.getTranslation("has been killed!"));
						}
					}
				}
			} else { // Not harmed
				if (can_we_see) {
					if (by_unit != null) {
						if (by_unit.side == main.game_data.our_side) { // Only play if we've done the attack
							main.playSound(by_unit, SpeechPlayer.EV_NOT_HARMED_ENEMY);
						} else {
							main.playSound(this, SpeechPlayer.EV_RCVD_NO_HARM);
						}
					}

					if (form_of_attack == UnitDataComms.FOA_EXPLOSION) {
						main.addToHUD(this.name + " " + StartupNew.strings.getTranslation("was not harmed by") + " " + StartupNew.strings.getTranslation("an explosion"));
					} else if (form_of_attack == UnitDataComms.FOA_NERVE_GAS) {
						main.addToHUD(this.name + " " + StartupNew.strings.getTranslation("was not harmed by") + " " + StartupNew.strings.getTranslation("nerve gas"));
					} else if (form_of_attack == UnitDataComms.FOA_FIRE) {
						main.addToHUD(this.name + " " + StartupNew.strings.getTranslation("was not harmed by") + " fire");
					} else {
						main.addToHUD(this.name + " " + StartupNew.strings.getTranslation("was not harmed by") + " " + by_name);
					}
				}
				//boolean who_can_see[] = main.whichSidesCanSeeUnit(this);
				main.sendEventToServer_UnitWounded(this, by_side, by_unitid, form_of_attack, 0, who_can_see);
			}
		}
	}


	public void escaped(AppletMain main) {
		main.unitEscaped(this);
		this.status = UnitsTable.ST_ESCAPED;

		this.model.collider.removeFromParent();
		this.model.collider = null;
		this.model.removeFromParent();
		this.model = null;

		main.updateUnitOnServer(this);
		boolean seen_by_side[] = main.whichSidesCanSeeUnit(this);
		main.sendEventToServer_UnitEscaped(this, seen_by_side);
	}


	public boolean isAlive() {
		return this.status != UnitsTable.ST_DEAD && this.status != UnitsTable.ST_NO_LONGER_EXISTS; // Don't check health as this may be neg, but unit still alive (due to bug) this.health > 0;
	}


	public void setMapLocation(AppletMain main, byte x, byte z, AppletMapSquare sq) {
		map_x = x;
		map_z = z;
		if (model != null) {
			if (this.status == UnitsTable.ST_AWAITING_DEPLOYMENT) {
				// Just deployed
				updateModelFromUnitData();
			} else { // Must have walked
				main.addObjectForUpdate(this.model);
				this.model.anim_walk();
			}
		}

		if (sq != null) {
			if (sq.escape_hatch_side == this.side) {
				// Unit escaped!
				// NO!  select menu option now - this.escaped(main);
			}
			// Open the door
			if (sq.door_type > 0) {
				if (sq.door_open == false) {
					main.playSound(SoundEffects.DOOR);

					sq.door.startOpening();
					sq.door.opposite.startOpening();
					sq.door_open = true;

					main.updateMapOnServer(sq, -1);
				}
			}
		}
	}


	public void turnBy(int a) {
		this.angle += a;
		this.setAngle(angle);
	}


	public void setAngle(int a) {
		this.angle = a;
		this.angle = Geometry.NormalizeAngle(this.angle);
		if (model != null) {
			updateModelFromUnitData();
		}
	}


	public int getMapX() {
		return this.map_x;
	}


	public int getMapZ() {
		return this.map_z;
	}


	public int getAngle() {
		return this.angle;
	}


	/**
	 * This updates the unit's location and direction based on their unitdata
	 */
	public void updateModelFromUnitData() {
		model.getLocalTranslation().x = map_x + 0.5f;
		model.getLocalTranslation().z = map_z + 0.5f;

		updateAngleFromUnitData(false);

		model.updateGeometricState(0, true);
	}


	public void updateAngleFromUnitData(boolean update_geom_stats) {
		double rads = this.angle * FastMath.DEG_TO_RAD;
		float x_off = (float)Math.cos(rads) * 10;
		float z_off = (float)Math.sin(rads) * 10;

		this.look_at = new Vector3f(model.getLocalTranslation().x+x_off, model.getLocalTranslation().y, model.getLocalTranslation().z+z_off);
		this.model.lookAt(this.look_at, Vector3f.UNIT_Y);

		if (update_geom_stats) {
			model.updateGeometricState(0, true);
		}

	}


	public Vector3f getLookAt() {
		return this.look_at;
	}


	public short getHealth() {
		return this.health;
	}


	public short getMaxHealth() {
		return this.max_health;
	}


	public void setHealth(short h) {
		this.health = h;

		// Update the model
		if (this.model instanceof Blob) {
			Blob b = (Blob)this.model;
			b.updateSphere();
		}
	}


	public boolean checkAndReduceAPs(AppletMain main, int amt, boolean nerve_gas) {
		if (SharedStatics.DEBUG) {
			return true;
		}
		if (this.aps >= amt) {
			this.aps -= amt;
			this.model.checkForNerveGasAndFire(amt);
			main.refreshHUD();
			return true;
		} else {
			if (this == main.getCurrentUnit()) {
				main.playSound(SoundEffects.APS_OUT);
				main.addToHUD(StartupNew.strings.getTranslation("Not enough APs!"));
			}
			return false;
		}
	}


	public short getAPs() {
		return this.aps;
	}


	public void setAPs(short a) {
		this.aps = a;
	}


	public void incAPs(short a) {
		this.aps += a;
	}


	public void removeCurrentItem(AppletMain main) {
		this.removeItem(main, this.current_item, true);
	}


	public void incHealth(short amt) {
		incHealth(amt, false);
	}


	public void incHealth(short amt, boolean override_max) {
		this.health = (short)(this.health + amt);
		if (this.health > this.max_health) {
			if (override_max) {
				this.max_health = this.health;
			} else {
				this.health = this.max_health;
			}
		}

		// Update the model
		if (this.health > 0) {
			if (this.model instanceof Blob) {
				Blob b = (Blob)this.model;
				b.updateSphere();
			}
		}
	}


	public void setMaxHealth(short amt) {
		this.max_health = amt;
	}


	public void removeItem(AppletMain main, EquipmentData eq, boolean update_server) {
		this.items.remove(eq);
		if (this.current_item == eq) {
			this.current_item = null;
			if (update_server) {
				main.updateUnitOnServer(this);
			}
		}
		eq.removeFromUnit();

	}


	public String getUnitsCurrentItemAsString() {
		if (this.current_item != null) {
			return this.current_item.getName(false) + ".";
		} else {
			return StartupNew.strings.getTranslation("nothing") + ".";
		}
	}


}
