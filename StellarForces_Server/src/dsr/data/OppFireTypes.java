package dsr.data;

public class OppFireTypes {
	
	public static final byte SNAP_SHOT = 0; // Default
	public static final byte AIMED_SHOT = 1;
	public static final byte AUTO_SHOT = -1;

}
