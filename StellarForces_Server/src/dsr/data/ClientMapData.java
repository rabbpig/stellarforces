package dsr.data;

public final class ClientMapData {
	
	private AppletMapSquare[][] mapdata;
	
	public ClientMapData(AppletMapSquare[][] data) {
		mapdata = data;
	}

	public AppletMapSquare getSq_MaybeNULL(int x, int z) {
		try {
			return mapdata[x][z];
		} catch (java.lang.ArrayIndexOutOfBoundsException ex) {
			//AppletMain.p("Error: sq " + x + "," + z + " not found.");
			return null;
		}
	}
	
	public int getMapWidth() {
		return mapdata.length;
	}

	public int getMapHeight() {
		return mapdata[0].length;
	}

}
