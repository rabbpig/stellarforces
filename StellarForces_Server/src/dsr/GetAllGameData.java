package dsr;

import java.io.IOException;

import dsr.comms.EquipmentDataComms;
import dsr.comms.MapDataComms;
import dsr.comms.UnitDataComms;
import dsr.comms.WGet4Client;
import dsr.data.ClientMapData;
import dsr.data.EquipmentData;
import dsr.data.GameData;
import dsr.data.UnitData;
import dsr.start.StartupNew;

public final class GetAllGameData extends Thread {

	public int game_stage = -1;
	private ClientMapData mapdata;
	public UnitData units[];
	public GameData game_data;
	public EquipmentData equipment[];
	private IDisplayMessages id;
	private boolean success = false;
	private String login, pwd;
	

	public GetAllGameData(GameData _game_data, boolean thread, IDisplayMessages _id, String _login, String _pwd) throws IOException {
		game_data = _game_data;
		id = _id;
		login = _login;
		pwd = _pwd;

		this.setDaemon(true);

		if (thread) {
			start();
		} else {
			run();
		}
	}


	public void run() {
		try {
			this.getMapData(); // Must be before getting the equipment and getting units
			this.getUnitData(); // Must be before getting the equipment
			this.getEquipmentData(); // Must be after getMapData since we use that to place the equipment
			success = true;
		} catch (Exception ex) {
			AppletMain.HandleError(null, ex);
		}
	}


	private void getMapData() throws IOException {
		writeText(StartupNew.strings.getTranslation("Getting map data..."));
		WGet4Client wc = new WGet4Client(id, MapDataComms.GetMapDataRequest(game_data.game_id), true);
		String response = wc.getResponse();
		if (response.length() > 0) {
			mapdata = MapDataComms.DecodeMapDataResponse(response);
		} else {
			throw new RuntimeException("Error getting map data");
		}
		writeText(StartupNew.strings.getTranslation("Finished getting map data."));
	}


	private void getUnitData() throws IOException {
		writeText(StartupNew.strings.getTranslation("Getting unit data..."));
		WGet4Client wc = new WGet4Client(id, UnitDataComms.GetUnitDataRequest(game_data.game_id, game_data.gamecode, login, pwd), true);
		String response = wc.getResponse();
		if (response.length() > 0) {
			units = UnitDataComms.DecodeUnitDataResponse(response);
		} else {
			throw new RuntimeException("Error getting unit data");
		}
		writeText(StartupNew.strings.getTranslation("Finished getting unit data."));
	}


	private void getEquipmentData() throws IOException {
		writeText(StartupNew.strings.getTranslation("Getting equipment data..."));
		WGet4Client wc = new WGet4Client(id, EquipmentDataComms.GetEquipmentDataRequest(game_data.game_id, game_data.gamecode, -1, "unused"), true);
		String response = wc.getResponse();
		if (response.length() > 0) {
			equipment = EquipmentDataComms.DecodeEquipmentDataResponse_UNUSED(mapdata, units, response);
		} else {
			throw new RuntimeException("Error getting equipment data");
		}
		writeText(StartupNew.strings.getTranslation("Finished getting equipment data."));
	}


	public ClientMapData getMap() {
		return mapdata;
	}


	public boolean wasSuccessful() {
		return this.success;
	}


	private void writeText(String s) {
		if (id != null) {
			id.displayMessage(s);
		} else if (StartupNew.lw != null) {
			StartupNew.lw.appendLine(s);
		} else {
			AppletMain.p(s);
		}
	}

}
