package dsr.models.map;

import com.jme.math.FastMath;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.scene.state.CullState;
import com.jme.scene.state.TextureState;
import com.jme.system.DisplaySystem;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.AppletMapSquare;
import dsr.models.CollisionQuad;

/**
 * This is used by the thick-wall map model.
 *
 */
public class Ceiling extends GameObject {

	private static final long serialVersionUID = 1L;

	public static Quaternion q = new Quaternion();
	private static CullState cs_ceiling;

	static {
		q.fromAngleAxis(FastMath.PI/2, new Vector3f(-1,0,0));
	}

	private TextureState getTextureFromSquare(AppletMapSquare sq) {
		return main.getTextureState(sq.texture_code);
	}

	public Ceiling(AppletMain main, AppletMapSquare sq) {
		super(main, "Ceiling", false, false, false, (float)sq.x + 0.5f, (float)sq.y+0.5f, 1f); // Notice that WE are the collider!  

		CollisionQuad ceiling = new CollisionQuad(1, 1, this);
		ceiling.setLocalRotation(q);
		ceiling.setRenderState(this.getTextureFromSquare(sq));

		if (cs_ceiling == null) {
			cs_ceiling = DisplaySystem.getDisplaySystem().getRenderer().createCullState();
			cs_ceiling.setEnabled(true);
			cs_ceiling.setCullFace(CullState.Face.Back);
		}
		ceiling.setRenderState(cs_ceiling);

		ceiling.updateRenderState();
		this.attachChild(ceiling);

		this.updateModelBound();

		sq.threeds.add(this);
	}

}
