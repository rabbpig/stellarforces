package dsr.models.map;

import java.util.HashSet;

import com.jme.scene.Node;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.AppletMapSquare;
import dsr.data.ClientMapData;
import dsr.start.StartupNew;
import dsrwebserver.tables.MapDataTable;


/**
 * This is the original map model that uses 1x1 blocks.
 *
 */
public final class MapModel_Original extends AbstractMapModel {

	private static final long serialVersionUID = 1L;

	private boolean show_ceilings;

	public MapModel_Original(AppletMain m, ClientMapData _map, boolean _show_ceilings, HashSet<AppletMapSquare> changed_squares) {
		super("Map_Node");
		main = m;
		map = _map;
		show_ceilings = _show_ceilings;

		this.setIsCollidable(true);
		this.loadMapObjects(changed_squares);
	}


	public void loadMapObjects(HashSet<AppletMapSquare> changed_squares) {
		for (int z=0 ; z<map.getMapHeight() ; z++) {
			for (int x=0 ; x<map.getMapWidth() ; x++) {
				AppletMapSquare ms = map.getSq_MaybeNULL(x, z);

				if (changed_squares != null) {
					if (changed_squares.contains(ms) == false) {
						continue;
					} else {
						// Remove existing models
						while (ms.threeds.size() > 0) {
							GameObject o = ms.threeds.remove(0);
							o.removeFromParent();
						}
					}
				}

				Node n = this.getSubNode(x, z);

				if (ms.raised_texture_code > 0 && StartupNew.hi_mem) {
					n.attachChild(new TransparentFloor(main, ms));
				}

				if (ms.major_type == MapDataTable.MT_FLOOR || ms.major_type == MapDataTable.MT_COMPUTER) {
					addFloorsEtc(n, ms);
				} else if (ms.major_type == MapDataTable.MT_NOTHING) {
					// Do nothing
				} else if (ms.major_type == MapDataTable.MT_WALL) {
					if (show_ceilings) {
						n.attachChild(new Ceiling(main, ms));
					}
				} else {
					throw new RuntimeException("Unknown mapsquare type: " + ms.major_type);
				}
			}
		}

		// Add Walls - note we leave out the outermost squares
		for (int z=1 ; z<map.getMapHeight()-1 ; z++) {
			for (int x=1 ; x<map.getMapWidth()-1 ; x++) {
				AppletMapSquare ms = map.getSq_MaybeNULL(x, z);

				if (changed_squares != null) {
					if (changed_squares.contains(ms) == false) {
						continue;
					}
				}

				if (ms.major_type == MapDataTable.MT_FLOOR || ms.major_type == MapDataTable.MT_COMPUTER) {
					// Check surrounding squares
					int i=x-1;
					int j=z;
					this.addWallIfRequired(ms, i, j, 0);

					i=x+1;
					j=z;
					this.addWallIfRequired(ms, i, j, 2);

					i=x;
					j=z-1;
					this.addWallIfRequired(ms, i, j, 1);

					i=x;
					j=z+1;
					this.addWallIfRequired(ms, i, j, 3);
				}
			}
		}

		super.addWallsAndMiniFloors(changed_squares);

		this.updateModelBound();

		this.updateGeometricState(0, true);

	}


	private void addWallIfRequired(AppletMapSquare ms, int i, int j, int rot) {
		AppletMapSquare ms2 = map.getSq_MaybeNULL(i, j); 
		if (ms2.major_type != MapDataTable.MT_FLOOR && ms2.major_type != MapDataTable.MT_COMPUTER) {
			Node n = this.getSubNode(ms.x, ms.y);
			/*if (AppletMain.DEBUG) {
				if (tw == null) {
					tw = new ThickWall(main, ms, ms2, rot);
					tw.setIsCollidable(false);
				}
				ThickWall tmp = new ThickWall(main, ms, ms2, rot);
				MySharedNode m = new MySharedNode(tw);
				m.setLocalTranslation(tmp.getLocalTranslation().clone());
				m.setLocalRotation(ThickWall.q);
				n.attachChild(m);
			} else {*/
			n.attachChild(new ThickWall(main, ms, ms2, rot));
			//}
		}
	}


}
