package dsr.models.map;

import java.nio.FloatBuffer;

import com.jme.math.FastMath;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.scene.state.CullState;
import com.jme.system.DisplaySystem;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.AppletMapSquare;
import dsr.models.CollisionQuad;

public class MiniFloor extends GameObject {
	
	private static final long serialVersionUID = 1L;

	private static Quaternion q = new Quaternion();
	private static CullState cs_floor;

	static {
		q.fromAngleAxis(FastMath.PI/2, new Vector3f(-1,0,0));
	}

	
	/*public static MiniFloor Factory(AppletMain main, AppletMapSquare pos_sq, float off_x, float off_z, AppletMapSquare tex_sq) {
		String key = "MiniFloor|" + off_x + "|" + off_z + "|" + tex_sq.texture_code;
		MiniFloor n = (MiniFloor)main.model_cache.getNode(key);
		if (n == null) {
			n = new MiniFloor(main, pos_sq, off_x, off_z, tex_sq);
			main.model_cache.putNode(key, n);
		}
		//if (AppletMain.CREATE_MAP_TEST) {
		pos_sq.threeds.add(n);
		//}
		return n;
	}*/
	
	
	public MiniFloor(AppletMain main, AppletMapSquare pos_sq, float off_x, float off_z, AppletMapSquare tex_sq) {
		super(main, "MiniFloor", true, true, true, (float)pos_sq.x + 0.25f + off_x, (float)pos_sq.y + 0.25f + off_z, 0); // Notice that WE are the collider!  And Floors need to be collideable for when selecting squares (for deploying units)

		// Floor
		this.collider = new CollisionQuad(0.5f, 0.5f, this);
		this.collider.setLocalRotation(q);
		this.collider.setRenderState(Floor.GetTextureFromSquare(main, tex_sq, false));

		FloatBuffer tbuf = this.collider.getTextureCoords(0).coords;
		tbuf.clear();		
		tbuf.put(0).put(0.5f);
		tbuf.put(0).put(0);
		tbuf.put(0.5f).put(0);
		tbuf.put(0.5f).put(0.5f);

		if (cs_floor == null) {
			cs_floor = DisplaySystem.getDisplaySystem().getRenderer().createCullState();
			cs_floor.setEnabled(true);
			cs_floor.setCullFace(CullState.Face.Back);
		}
		this.collider.setRenderState(cs_floor);

		this.collider.updateRenderState();
		this.attachChild(this.collider);

		this.updateModelBound();

		pos_sq.threeds.add(this);
	}


}
