package dsr.models.map;

import com.jme.math.Vector3f;
import com.jme.scene.state.TextureState;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.AppletMapSquare;
import dsr.models.CollisionBox;

/**
 * This is used by the Slim-wall map model for a 1x1 square.
 *
 */
public class BigWall extends GameObject {
	
	private static final long serialVersionUID = 1L;

	public BigWall(AppletMain m, float x, float z, AppletMapSquare sq_ceiling) {
		super(m, "BigWall" + (int)x + "_" + (int)z, true, true, true, x + 0.5f, z + 0.5f, 0f);
		
		// Our collider quad is also the actual rendered object
		//this.collider = new CollisionBox(new Vector3f(0, 0.5f, 0), .5f, .5f, .5f, this);
		this.collider = new CollisionBox(new Vector3f(0, 0.5f, 0), .5f, .5f, .5f, this);
		//collider.setModelBound(new BoundingBox());
		//collider.updateModelBound();
		this.attachChild(collider);
		this.setIsCollidable(true);

		TextureState ts = main.getTextureState(sq_ceiling.texture_code);

		collider.setRenderState(ts);

		updateModelBound();
		
		sq_ceiling.threeds.add(this);
	}


}
