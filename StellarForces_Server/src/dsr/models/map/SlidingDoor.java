package dsr.models.map;

import com.jme.math.FastMath;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.IUpdatable;
import dsr.data.AppletMapSquare;
import dsr.models.CollisionQuad;

/**
 * Sliding doors only open during the game.  They are closed at the end of each turn
 * (if there is no-one stood in the way).
 *
 */
public final class SlidingDoor extends GameObject implements IUpdatable {

	private static final float SPEED = 0.4f;
	private static final float MAX_LEFT = -0.65f; // -0.85
	private static final float MAX_RIGHT = -0.25f;//-0.35f;, was -0.15f;

	public static Quaternion q90 = new Quaternion();
	public static Quaternion q180 = new Quaternion();
	public static Quaternion q270 = new Quaternion();

	private static final long serialVersionUID = 1L;

	static {
		q90.fromAngleAxis(FastMath.PI/2, new Vector3f(0,-1,0));
		q180.fromAngleAxis(FastMath.PI, new Vector3f(0,1,0));
		q270.fromAngleAxis(FastMath.PI/2, new Vector3f(0,1,0));
	}

	private boolean is_moving = false;
	private int dir = 0;
	public SlidingDoor opposite;

	public SlidingDoor(AppletMain m, AppletMapSquare sq, int side, SlidingDoor opp) {
		super(m, "SlidingDoor" + sq.x + "_" + sq.y, true, true, true, sq.x + 0.5f, sq.y + 0.5f, 0f);
		
		opposite = opp;

		switch (side) {
		case 0: // Side
			break;
		case 1: // Back
			setLocalRotation(q90);
			break;
		case 2: // Other side
			setLocalRotation(q180);
			break;
		case 3: // Front
			setLocalRotation(q270);
			break;
		default:
			throw new RuntimeException("Unknown door rotation");
		}

		// Our collider quad is also the actual rendered object
		this.collider = new CollisionQuad(0.5f + AppletMain.QUAD_OVERLAP, 1, this); // 0.501 otherwise it's possible to see through the cracks!
		collider.setLocalTranslation(-0.25f, 0.5f, 0f);
		//collider.setModelBound(new BoundingBox());
		//collider.updateModelBound();
		this.attachChild(collider);
		this.setIsCollidable(true);

		collider.setRenderState(main.getTextureState(AppletMain.DATA_DIR + "textures/door1.jpg"));

		updateModelBound();

			sq.threeds.add(this);
	}

	public void startOpening() {
		this.collider.setIsCollidable(false);
		if (is_moving == false) {
			is_moving = true;
			main.addObjectForUpdate(this);
		}
		dir = -1;
		
	}

	public void startClosing() {
		this.collider.setIsCollidable(true);
		if (is_moving == false) {
			is_moving = true;
			main.addObjectForUpdate(this);
		}
		dir = 1;
		
	}

	public void update(float interpolation) {
		if (is_moving) {
			if (dir == -1) { // Opening
				this.collider.getLocalTranslation().x -= (SPEED*interpolation); // Open
				if (this.collider.getLocalTranslation().x <= MAX_LEFT) { // Have gone far enough?
					open();
				}
			} else if (dir == 0){ // Closing
				dir = 1;
			} else { // Closing
				this.collider.getLocalTranslation().x += (SPEED*interpolation);
				if (this.collider.getLocalTranslation().x >= MAX_RIGHT) {
					this.collider.getLocalTranslation().x = MAX_RIGHT;
					this.is_moving = false;
				}
			}
		}
	}
	
	public void open() {
		this.collider.setIsCollidable(false);
		this.collider.getLocalTranslation().x = MAX_LEFT;
		dir = 0;
		is_moving = false;
	}
	
	
}
