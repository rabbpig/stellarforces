package dsr.models.map;

import java.util.HashSet;

import com.jme.scene.Node;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.AppletMapSquare;
import dsr.data.ClientMapData;
import dsrwebserver.tables.MapDataTable;

/**
 * This is the new map model with fancy walls.
 *
 */
public class MapModel_SlimWalls extends AbstractMapModel {

	private static final long serialVersionUID = 1L;

	public MapModel_SlimWalls(AppletMain m, ClientMapData _map, HashSet<AppletMapSquare> changed_squares) {
		super("Map_Node");
		main = m;
		map = _map;

		this.setIsCollidable(true);
		this.loadMapObjects(changed_squares);
	}


	public void loadMapObjects(HashSet<AppletMapSquare> changed_squares) {
		for (int z=0 ; z<map.getMapHeight() ; z++) {
			for (int x=0 ; x<map.getMapWidth() ; x++) {
				AppletMapSquare ms = map.getSq_MaybeNULL(x, z);
				if (ms == null) {
					throw new RuntimeException("No map square at " + x + ", " + z);
				}
				
				if (changed_squares != null) {
					if (changed_squares.contains(ms) == false) {
						continue;
					} else {
						// Remove existing models
						while (ms.threeds.size() > 0) {
							GameObject o = ms.threeds.remove(0);
							o.removeFromParent();
						}
					}
				}
				
				Node n = this.getSubNode(x, z);
				if (ms.major_type == MapDataTable.MT_FLOOR || ms.major_type == MapDataTable.MT_COMPUTER) {
					addFloorsEtc(n, ms);
				} else if (ms.major_type == MapDataTable.MT_NOTHING) {
					// Do nothing
				} else if (ms.major_type == MapDataTable.MT_WALL) {
					//this.addWallsAndMiniFloors(ms); NO NEED TO DO THIS AT THE END!
				} else {
					System.err.println("Unknown mapsquare type: " + ms.major_type);
					//throw new RuntimeException("Unknown mapsquare type: " + ms.major_type);  NONONNO!
				}


			}
		}
		
		super.addWallsAndMiniFloors(changed_squares);

		this.updateModelBound();
		this.updateGeometricState(0, true);
		this.updateRenderState();

	}


}
