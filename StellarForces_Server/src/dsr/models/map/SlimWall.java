package dsr.models.map;

import com.jme.math.FastMath;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.scene.state.TextureState;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.AppletMapSquare;
import dsr.models.CollisionBox;

/**
 * This is used by the slim wall map model.
 *
 */
public class SlimWall extends GameObject {

	public static final float THICKNESS = 0.12f; // Was .15f

	private static Quaternion q90 = new Quaternion();
	private static Quaternion q180 = new Quaternion();
	private static Quaternion q270 = new Quaternion();

	private static final long serialVersionUID = 1L;

	static {
		q90.fromAngleAxis(FastMath.PI/2, new Vector3f(0,-1,0));
		q180.fromAngleAxis(FastMath.PI, new Vector3f(0,1,0));
		q270.fromAngleAxis(FastMath.PI/2, new Vector3f(0,1,0));
	}

	public SlimWall(AppletMain m, int side, AppletMapSquare sq, boolean double_length) {
		super(m, "SlimWall" + sq.x + "_" + sq.y, true, true, true, sq.x + 0.5f, sq.y + 0.5f, 0f);

		switch (side) {
		case 0: // Side
			break;
		case 1: // Back
			setLocalRotation(q90);
			break;
		case 2: // Other side
			setLocalRotation(q180);
			break;
		case 3: // Front
			setLocalRotation(q270);
			break;
		default:
			throw new RuntimeException("Unknown wall rotation");
		}

		// Our collider quad is also the actual rendered object
		float len = 0.25f;
		float thickness = THICKNESS;
		if (double_length) {
			len = 0.5f;
			thickness = 0f;
		}
		this.collider = new CollisionBox(new Vector3f(len-0.5f + (thickness/2), 0.5f, 0f), len + (thickness/2), .5f, THICKNESS, this);
		//collider.setModelBound(new BoundingBox());
		//collider.updateModelBound();
		this.attachChild(collider);
		this.setIsCollidable(true);

        TextureState ts = main.getTextureState(sq.texture_code);
		collider.setRenderState(ts);

		updateModelBound();

			sq.threeds.add(this);


	}

}
