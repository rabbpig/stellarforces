package dsr.models.map;

import com.jme.math.FastMath;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.scene.state.CullState;
import com.jme.scene.state.TextureState;
import com.jme.system.DisplaySystem;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.AppletMapSquare;
import dsr.models.CollisionQuad;
import dsrwebserver.tables.GamesTable;

public final class Floor extends GameObject {

	private static final long serialVersionUID = 1L;

	public static Quaternion q = new Quaternion();
	private static CullState cs_floor;

	static {
		q.fromAngleAxis(FastMath.PI/2, new Vector3f(-1,0,0));
	}


	public static TextureState GetTextureFromSquare(AppletMain main, AppletMapSquare sq) {
		return GetTextureFromSquare(main, sq, true);
	}

	
	public static TextureState GetTextureFromSquare(AppletMain main, AppletMapSquare sq, boolean override) {
		// Do we override texture?
		if (override == false) {
			return main.getTextureState(sq.texture_code);
		} else {
			if (sq.escape_hatch_side == main.game_data.our_side && main.game_data.our_side > 0) { // Check for playback with main.game_data.our_side>0
				return main.getTextureState(AppletMain.DATA_DIR + "textures/escape_hatch.jpg");
			} else if (main.game_data.areSidesFriends(sq.deploy_sq_side, main.game_data.our_side) && main.game_data.game_status == GamesTable.GS_CREATED_DEPLOYMENT) {
				return main.getTextureState(AppletMain.DATA_DIR + "textures/deploy_sq.png");
			} else {
				return main.getTextureState(sq.texture_code);
			}
		}
	}


	public Floor(AppletMain main, AppletMapSquare sq) {
		super(main, "Floor", true, true, true, (float)sq.x + 0.5f, (float)sq.y+0.5f, 0);

		// Floor
		collider = new CollisionQuad(1, 1, this); // Notice that WE are the collider!  And Floors need to be collideable for when selecting squares (for deploying units etc)
		collider.setLocalRotation(q);
		TextureState ts = Floor.GetTextureFromSquare(main, sq);
		collider.setRenderState(ts);

		if (cs_floor == null) {
			cs_floor = DisplaySystem.getDisplaySystem().getRenderer().createCullState();
			cs_floor.setEnabled(true);
			cs_floor.setCullFace(CullState.Face.Back);
		}
		collider.setRenderState(cs_floor);
		collider.updateRenderState();
		this.attachChild(collider);

		this.updateModelBound();
		
		sq.threeds.add(this);

	}


}
