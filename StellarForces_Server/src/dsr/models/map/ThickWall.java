package dsr.models.map;

import com.jme.math.FastMath;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.scene.state.MaterialState;
import com.jme.scene.state.TextureState;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.AppletMapSquare;
import dsr.models.CollisionQuad;

/**
 * This is used by the thick-wall map model.
 *
 */
public final class ThickWall extends GameObject {

	private static final long serialVersionUID = 1L;

	private static Quaternion q90 = new Quaternion();
	private static MaterialState ms;

	static {
		q90.fromAngleAxis(FastMath.PI/2, new Vector3f(0,-1,0));
	}

	public ThickWall(AppletMain m, AppletMapSquare sq_floor, AppletMapSquare sq, int side) {
		super(m, "Wall_" + sq.x + "_" + sq.y, true, true, true, sq.x, sq.y, 0f); // Notice that we are NOT the collider!

		switch (side) {
		case 0: // Side
			setLocalRotation(q90);
			setLocalTranslation((float)sq_floor.x, .5f, (float)sq_floor.y + 0.5f);
			break;
		case 1: // Back
			setLocalTranslation((float)sq_floor.x + 0.5f, .5f, (float)sq_floor.y);
			break;
		case 2: // Other side
			setLocalRotation(q90);
			setLocalTranslation((float)sq_floor.x + 1f, .5f, (float)sq_floor.y + 0.5f);
			break;
		case 3: // Front
			setLocalTranslation((float)sq_floor.x + 0.5f, .5f, (float)sq_floor.y + 1f);
			break;
		default:
			throw new RuntimeException("Unknown wall rotation");
		}

		this.collider = new CollisionQuad(1f + AppletMain.QUAD_OVERLAP, 1f, this);
		//collider.setModelBound(new BoundingBox());
		//collider.updateModelBound();
		this.attachChild(collider);
		this.setIsCollidable(true);

		TextureState ts = main.getTextureState(sq.texture_code);

		collider.setRenderState(ts);

		if (ms == null) {
			ms = main.getDisplay().getRenderer().createMaterialState();  
			ms.setEnabled(true);
			ms.setShininess(128);

		}
		collider.setRenderState(ts);

		updateModelBound();

		//if (AppletMain.CREATE_MAP_TEST) {
			sq.threeds.add(this);
		//}
	}

}
