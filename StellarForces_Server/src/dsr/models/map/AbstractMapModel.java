package dsr.models.map;

import java.util.HashSet;
import java.util.Hashtable;

import com.jme.math.Vector3f;
import com.jme.scene.Node;

import dsr.AppletMain;
import dsr.TextureStateCache;
import dsr.data.AppletMapSquare;
import dsr.data.ClientMapData;
import dsr.data.UnitData;
import dsr.models.ComputerModel;
import dsr.models.scenery.Bookshelf;
import dsr.models.scenery.CeilingSpark;
import dsr.models.scenery.Chair;
import dsr.models.scenery.ComputerTable;
import dsr.models.scenery.Desk;
import dsr.models.scenery.FlowerPot;
import dsr.models.scenery.FlowerPot2;
import dsr.models.scenery.MetalShelf;
import dsr.models.scenery.NerveGasCloud;
import dsr.models.scenery.SmallPost;
import dsr.models.scenery.SmokeGrenCloud;
import dsr.models.scenery.SmokeScenery;
import dsr.models.scenery.WallScenery;
import dsr.start.StartupNew;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.MapDataTable;

public abstract class AbstractMapModel extends Node {

	protected static final int SUBNODE_SIZE = 5;

	public static final int BLOCK_WALLS = 1;
	public static final int SLIM_WALLS = 2;

	private static final long serialVersionUID = 1L;

	protected ClientMapData map;
	protected Hashtable<String,Node> subnodes = new Hashtable<String,Node>();

	protected AppletMain main;

	public AbstractMapModel(String name) {
		super(name);
	}


	public abstract void loadMapObjects(HashSet<AppletMapSquare> changed_squares);

	
	protected Node getSubNode(int x, int z) {
		x = x/SUBNODE_SIZE;
		z = z/SUBNODE_SIZE;
		String s = "|" + x + "," + z + "|";
		while (subnodes.containsKey(s) == false) {
			Node n = new Node("MapNode_" + x + "_" + z);
			n.updateModelBound();
			this.attachChild(n);
			this.subnodes.put(s, n);

		}
		Node n2 = (Node)this.subnodes.get(s);
		return n2;
	}


	public void addFloorsEtc(Node n, AppletMapSquare ms) {
		if (ms.door_type <= 0) {
			n.attachChild(new Floor(main, ms));
		}

		if (ms.raised_texture_code > 0 && StartupNew.hi_mem) {
			n.attachChild(new TransparentFloor(main, ms));
		}

		// Doors
		if (ms.door_type == MapDataTable.DOOR_EW) {
			SlidingDoor s = new SlidingDoor(main, ms, 0, null);
			n.attachChild(s);
			ms.door = s;

			SlidingDoor s2 = new SlidingDoor(main, ms, 2, s);
			n.attachChild(s2);

			s.opposite = s2;

			// Open the door if there is a unit there or the door is actually open.
			UnitData unit = main.getUnitAt(ms.x, ms.y);
			if (unit != null || ms.door_open) {
				s.open();
				s.opposite.open();
			} else { // Open the door if there is a corpse
				unit = main.getDeadUnitAt(ms.x, ms.y);
				if (unit != null) {
					s.open();
					s.opposite.open();
				}
			}
		} else if (ms.door_type == MapDataTable.DOOR_NS) {
			SlidingDoor s = new SlidingDoor(main, ms, 1, null);
			n.attachChild(s);
			ms.door = s;

			SlidingDoor s2 = new SlidingDoor(main, ms, 3, s);
			n.attachChild(s2);

			s.opposite = s2;

			// Open the door if there is a unit there or the door is actually open.
			UnitData unit = main.getUnitAt(ms.x, ms.y);
			if (unit != null || ms.door_open) {
				s.open();
				s.opposite.open();
			} else { // Open the door if there is a corpse
				unit = main.getDeadUnitAt(ms.x, ms.y);
				if (unit != null) {
					s.open();
					s.opposite.open();
				}
			}
		}
		if (ms.major_type == MapDataTable.MT_COMPUTER) {
			ComputerModel c = new ComputerModel(main, ms);
			n.attachChild(c);
		}
		if (ms.smoke_type > 0) {
			if (ms.smoke_type == EquipmentTypesTable.ET_NERVE_GAS) {
				NerveGasCloud c = new NerveGasCloud(main, ms);
				n.attachChild(c);
			} else {
				SmokeGrenCloud c = new SmokeGrenCloud(main, ms);
				n.attachChild(c);
			}
		}

		this.checkScenery(n, ms);

	}


	private void checkScenery(Node n, AppletMapSquare ms) {
		// Scenery?
		if (ms.destroyed <= 0) { // Don't show scenery on destroyed squares
			if (ms.scenery_code > 0) {
				if (StartupNew.hi_mem) {
					switch (ms.scenery_code) {
					case MapDataTable.FLOWERPOT:
						n.attachChild(new FlowerPot(main, ms));
						break;
					case MapDataTable.FLOWERPOT2:
						n.attachChild(new FlowerPot2(main, ms));
						break;
					case MapDataTable.SMOKE_FX:
						n.attachChild(new SmokeScenery(main, ms, new Vector3f(0f, 1f, 0f)));
						break;
					case MapDataTable.SPARKS:
						n.attachChild(new CeilingSpark(main, ms, new Vector3f(0f, 1f, 0f)));
						break;
					case MapDataTable.BOOKSHELF: 
						n.attachChild(new Bookshelf(main, ms, ms.scenery_direction));
						break;
					case MapDataTable.CHAIR: 
						n.attachChild(new Chair(main, ms, ms.scenery_direction));
						break;
					case MapDataTable.COMPUTER_TABLE:
						n.attachChild(new ComputerTable(main, ms, ms.scenery_direction));
						break;
					case MapDataTable.DESK: 
						n.attachChild(new Desk(main, ms, ms.scenery_direction));
						break;
					case MapDataTable.METAL_SHELF:
						n.attachChild(new MetalShelf(main, ms, ms.scenery_direction));
						break;
					case MapDataTable.HEDGEROW: 
						AddHedges(ms, TextureStateCache.TEX_HEDGE);
						break;
					case MapDataTable.SECTOR1: 
						n.attachChild(new WallScenery(main, ms, ms.scenery_direction, TextureStateCache.TEX_SECTOR1));
						break;
					case MapDataTable.SMALL_POST: 
						n.attachChild(new SmallPost(main, ms));
						break;
					case MapDataTable.BARREL1: 
						// NO AS IT IS EQUIPMENT n.attachChild(new GasCannister(main, ms.x, ms.y));
						break;
					case MapDataTable.BARREL2: 
						// NO AS IT IS EQUIPMENT n.attachChild(new GasCannister(main, ms.x, ms.y));
						break;
					case MapDataTable.SECTOR2: 
						n.attachChild(new WallScenery(main, ms, ms.scenery_direction, TextureStateCache.TEX_SECTOR2));
						break;
					case MapDataTable.SECTOR3: 
						n.attachChild(new WallScenery(main, ms, ms.scenery_direction, TextureStateCache.TEX_SECTOR3));
						break;
					case MapDataTable.SECTOR4: 
						n.attachChild(new WallScenery(main, ms, ms.scenery_direction, TextureStateCache.TEX_SECTOR4));
						break;
					case MapDataTable.BRICK_WALL: 
						AddHedges(ms, TextureStateCache.TEX_BRICKS);
						break;
					default:
						System.err.println("Unknown scenery code: " + ms.scenery_code);
					}
				}
			}
		}
	}


	protected void addWallsAndMiniFloors(HashSet<AppletMapSquare> changed_squares) {
		// Add Walls - note we leave out the outermost squares
		for (int z=1 ; z<map.getMapHeight()-1 ; z++) {
			for (int x=1 ; x<map.getMapWidth()-1 ; x++) {
				AppletMapSquare ms = map.getSq_MaybeNULL(x, z);

				if (changed_squares != null) {
					if (changed_squares.contains(ms) == false) {
						continue;
					}
				}

				if (ms.major_type == MapDataTable.MT_WALL || ms.door_type > 0) {
					// Check surrounding squares
					this.addWallsAndMiniFloors(ms);
				}
			}
		}


	}


	private void addWallsAndMiniFloors(AppletMapSquare ms) {
		int x = ms.x;
		int z = ms.y;
		Node node = this.getSubNode(ms.x, ms.y);

		AppletMapSquare ms_n = map.getSq_MaybeNULL(x, z-1);
		AppletMapSquare ms_s = map.getSq_MaybeNULL(x, z+1);
		AppletMapSquare ms_e = map.getSq_MaybeNULL(x+1, z);
		AppletMapSquare ms_w = map.getSq_MaybeNULL(x-1, z);

		boolean n = isFloor(ms_n); 
		boolean s = isFloor(ms_s); 
		boolean e = isFloor(ms_e); 
		boolean w = isFloor(ms_w); 

		if (ms.door_type <= 0 && this instanceof MapModel_SlimWalls) {
			// Add walls
			if (!n && !s) {
				node.attachChild(new SlimWall(main, 1, ms, true)); // North
			} else {
				if (!n) {
					node.attachChild(new SlimWall(main, 1, ms, false)); // North
				}
				if (!s) {
					node.attachChild(new SlimWall(main, 3, ms, false)); // South
				}
			}

			//-----------------------
			if (!e && !w) {
				node.attachChild(new SlimWall(main, 2, ms, true)); // East
			} else {
				if (!e){
					node.attachChild(new SlimWall(main, 2, ms, false)); // East
				}
				if (!w) {
					node.attachChild(new SlimWall(main, 0, ms, false));  // West
				}
			}
			//-----------------------

		}

		if (n && s && e && w && ms.door_type <= 0) {
			// It's a 1x1 wall
			node.attachChild(new BigWall(main, x, z, ms));
		} else {
			if (ms.door_type > 0 || StartupNew.hi_mem) {
			// Add mini floors
			AppletMapSquare ms_ne = map.getSq_MaybeNULL(x+1, z-1); 
			AppletMapSquare ms_se = map.getSq_MaybeNULL(x+1, z+1); 
			AppletMapSquare ms_nw = map.getSq_MaybeNULL(x-1, z-1);
			AppletMapSquare ms_sw = map.getSq_MaybeNULL(x-1, z+1);

			AppletMapSquare text_square_to_use = ms_w;//ms_nw;
			if (!isFloor(text_square_to_use)) {
				text_square_to_use = ms_n;
			}
			if (!isFloor(text_square_to_use)) {
				text_square_to_use = ms_nw;//ms_w;
			}
			node.attachChild(new MiniFloor(main, ms, 0f, 0f, text_square_to_use)); // NW

			text_square_to_use = ms_e;//ms_ne;
			if (!isFloor(text_square_to_use)) {
				text_square_to_use = ms_n;
			}
			if (!isFloor(text_square_to_use)) {
				text_square_to_use = ms_ne;//ms_e;
			}
			node.attachChild(new MiniFloor(main, ms, .5f, 0f, text_square_to_use)); // NE

			text_square_to_use = ms_w; //ms_sw;
			if (!isFloor(text_square_to_use)) {
				text_square_to_use = ms_s;
			}
			if (!isFloor(text_square_to_use)) {
				text_square_to_use = ms_sw;
			}
			node.attachChild(new MiniFloor(main, ms, 0f, .5f, text_square_to_use));  // SW // this one!

			text_square_to_use = ms_e;//ms_se;
			if (!isFloor(text_square_to_use)) {
				text_square_to_use = ms_s;
			}
			if (!isFloor(text_square_to_use)) {
				text_square_to_use = ms_se;//ms_e;
			}
			node.attachChild(new MiniFloor(main, ms, .5f, .5f, text_square_to_use)); // SE
			}
		}
	}


	private boolean isFloor(AppletMapSquare ms) {
		return (ms.major_type == MapDataTable.MT_FLOOR || ms.major_type == MapDataTable.MT_COMPUTER) && ms.door == null;
	}



	private void AddHedges(AppletMapSquare ms, int tex) {
		int x = ms.x;
		int z = ms.y;
		AppletMapSquare ms_n = map.getSq_MaybeNULL(x, z-1);
		AppletMapSquare ms_s = map.getSq_MaybeNULL(x, z+1);
		AppletMapSquare ms_e = map.getSq_MaybeNULL(x+1, z);
		AppletMapSquare ms_w = map.getSq_MaybeNULL(x-1, z);

		Node node = this.getSubNode(ms.x, ms.y);


		boolean n = false, s = false, e = false, w = false;
		if (ms_n != null) {
			n = ms_n.scenery_code != MapDataTable.HEDGEROW;
		}
		if (ms_s != null) {
			s = ms_s.scenery_code != MapDataTable.HEDGEROW;
		}
		if (ms_e != null) {
			e = ms_e.scenery_code != MapDataTable.HEDGEROW;
		}
		if (ms_w != null) {
			w = ms_w.scenery_code != MapDataTable.HEDGEROW;
		}

		if (!n && !s) {
			node.attachChild(new Hedgerow(main, 1, ms, true, tex)); // North
		} else {
			if (!n) {
				node.attachChild(new Hedgerow(main, 1, ms, false, tex)); // North
			}
			if (!s) {
				node.attachChild(new Hedgerow(main, 3, ms, false, tex)); // South
			}
		}

		//-----------------------
		if (!e && !w) {
			node.attachChild(new Hedgerow(main, 2, ms, true, tex)); // East
		} else {
			if (!e){
				node.attachChild(new Hedgerow(main, 2, ms, false, tex)); // East
			}
			if (!w) {
				node.attachChild(new Hedgerow(main, 0, ms, false, tex));  // West
			}
		}
		//-----------------------

	}



}
