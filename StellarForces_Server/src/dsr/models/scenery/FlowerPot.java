package dsr.models.scenery;

import java.io.IOException;

import com.jme.scene.Node;

import dsr.AppletMain;
import dsr.data.AppletMapSquare;
import dsr.models.ModelLoaders;

public class FlowerPot extends AbstractSceneryModel {

	private static final long serialVersionUID = 1L;

	private static final String PATH = AppletMain.DATA_DIR + "models/ufo_ai/objects/flowerpot" ;

	private Node body_node;
	//protected Quaternion q_StandStill = new Quaternion();

	public FlowerPot(AppletMain m, AppletMapSquare sq) {
		super(m, "FlowerPot", sq.x+0.5f, sq.y+0.5f, 0.25f);

		body_node = (Node)main.model_cache.getNode(PATH);
		try {
			if (body_node == null) {
				body_node = ModelLoaders.LoadMD2Model(main.getDisplay(), PATH, "flowerpot.md2", "flowerpot.tga", 0.04f);
				body_node.setName(this.getName() + "_body");
				main.model_cache.putNode(PATH, body_node);
			}
			this.attachChild(body_node);
			this.updateRenderState();
		} catch (IOException ex) {
			AppletMain.HandleError(m, ex);
		}

		//if (AppletMain.CREATE_MAP_TEST) {
			sq.threeds.add(this);
		//}

	}


}
