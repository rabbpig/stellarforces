package dsr.models.scenery;

import java.io.IOException;

import com.jme.scene.Node;

import dsr.AppletMain;
import dsr.data.AppletMapSquare;
import dsr.models.ModelLoaders;

public class FlowerPot2 extends AbstractSceneryModel {

	private static final long serialVersionUID = 1L;

	private static final String PATH = AppletMain.DATA_DIR + "models/ufo_ai/objects/flowerpot2" ;

	private Node body_node;
	//protected Quaternion q_StandStill = new Quaternion();

	public FlowerPot2(AppletMain m, AppletMapSquare sq) {
		super(m, "FlowerPot2", sq.x+0.5f, sq.y+0.5f, 0f);

		body_node = (Node)main.model_cache.getNode(PATH);
		try {
			if (body_node == null) {
				body_node = ModelLoaders.LoadMD2Model(main.getDisplay(), PATH, "flowerpot2.md2", "flowerpot2.jpg", 0.04f);
				body_node.setName(this.getName() + "_body");
				main.model_cache.putNode(PATH, body_node);
			}
			this.attachChild(body_node);
			//body_node.setLocalRotation(q_StandStill);
			this.updateRenderState();
		} catch (IOException ex) {
			AppletMain.HandleError(m, ex);
		}
		//if (AppletMain.CREATE_MAP_TEST) {
			sq.threeds.add(this);
		//}

	}


}
