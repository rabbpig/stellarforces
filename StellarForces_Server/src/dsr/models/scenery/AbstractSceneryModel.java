package dsr.models.scenery;

import dsr.AppletMain;
import dsr.GameObject;

public abstract class AbstractSceneryModel extends GameObject {
	
	private static final long serialVersionUID = 1L;

	public AbstractSceneryModel(AppletMain m, String name, float _x, float _z, float _y_off) {
		super(m, name, false, false, false, _x, _z, _y_off);
	}

}
