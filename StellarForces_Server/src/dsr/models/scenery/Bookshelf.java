package dsr.models.scenery;

import java.io.IOException;

import com.jme.scene.Node;

import dsr.AppletMain;
import dsr.data.AppletMapSquare;
import dsr.models.ModelLoaders;
import dsr.models.map.AbstractMapModel;
import dsr.models.map.SlidingDoor;
import dsr.models.map.SlimWall;

public class Bookshelf extends AbstractSceneryModel {

	private static final String PATH = AppletMain.DATA_DIR + "models/ufo_ai/objects/bookshelf" ;

	private static final long serialVersionUID = 1L;

	private Node body_node;

	public Bookshelf(AppletMain m, AppletMapSquare sq, int side) {
		super(m, "Bookshelf", sq.x+0.5f, sq.y+0.5f, 0f);

		body_node = (Node)main.model_cache.getNode(PATH);
		try {
			if (body_node == null) {
				body_node = ModelLoaders.LoadMD2Model(main.getDisplay(), PATH, "bookshelf.md2", "bookshelf.jpg", 0.022f);
				body_node.setName(this.getName() + "_body");
				main.model_cache.putNode(PATH, body_node);
			}
			float offset = 0;
			if (m.game_data.map_model_type == AbstractMapModel.SLIM_WALLS) {
				offset = (1f - SlimWall.THICKNESS)/2;
			}
			body_node.setLocalTranslation(0.3f+offset, 0f, .4f);
			this.attachChild(body_node);

			switch (side) {
			case 0: // Side
				break;
			case 1: // Back
				setLocalRotation(SlidingDoor.q90);
				break;
			case 2: // Other side
				setLocalRotation(SlidingDoor.q180);
				break;
			case 3: // Front
				setLocalRotation(SlidingDoor.q270);
				break;
			default:
				throw new RuntimeException("Unknown bookshelf rotation");
			}

			this.updateRenderState();
			
		} catch (IOException ex) {
			AppletMain.HandleError(m, ex);
		}
		//if (AppletMain.CREATE_MAP_TEST) {
			sq.threeds.add(this);
		//}

	}


}
