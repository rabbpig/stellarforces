package dsr.models.scenery;

import com.jme.math.FastMath;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.renderer.Renderer;
import com.jme.scene.state.BlendState;
import com.jme.scene.state.TextureState;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.AppletMapSquare;
import dsr.models.CollisionQuad;
import dsr.models.map.AbstractMapModel;
import dsr.models.map.SlimWall;

public class WallScenery extends GameObject {

	private static final long serialVersionUID = 1L;

	private static BlendState alphaState;

	private static Quaternion q90 = new Quaternion();
	private static Quaternion q180 = new Quaternion();
	private static Quaternion q270 = new Quaternion();

	static {
		q90.fromAngleAxis(FastMath.PI/2, new Vector3f(0,-1,0));
		q180.fromAngleAxis(FastMath.PI, new Vector3f(0,1,0));
		q270.fromAngleAxis(FastMath.PI/2, new Vector3f(0,1,0));
	}

	public WallScenery(AppletMain m, AppletMapSquare sq, byte side, short tex) {
		super(m, "WallScenery_" + sq.x + "_" + sq.y, false, false, false, sq.x, sq.y, 1f);

		float offset = 0f;
		if (m.game_data.map_model_type == AbstractMapModel.SLIM_WALLS) {
			offset = (0.85f - SlimWall.THICKNESS)/2;
		}

		switch (side) {
		case 0: // Side
			setLocalRotation(q270);
			setLocalTranslation((float)sq.x - offset, .5f, (float)sq.y + 0.5f);
			break;
		case 1: // Back
			setLocalTranslation((float)sq.x + 0.5f, .5f, (float)sq.y - offset);
			break;
		case 2: // Other side
			setLocalRotation(q90);
			setLocalTranslation((float)sq.x + 1f + offset, .5f, (float)sq.y + 0.5f);
			break;
		case 3: // Front
			setLocalRotation(q180);
			setLocalTranslation((float)sq.x + 0.5f, .5f, (float)sq.y + 1f + offset);
			break;
		default:
			throw new RuntimeException("Unknown wall rotation");
		}

		this.collider = new CollisionQuad(1f, 1f, this);
		//collider.setModelBound(new BoundingBox());
		//collider.updateModelBound();
		this.attachChild(collider);
		this.setIsCollidable(false);

		TextureState ts = main.getTextureState(tex);
		collider.setRenderState(ts);

		updateModelBound();

		// to handle transparency: a BlendState
		if (alphaState == null) {
			alphaState = main.getDisplay().getRenderer().createBlendState();
			alphaState.setBlendEnabled(true);
			alphaState.setSourceFunction(BlendState.SourceFunction.SourceAlpha);
			alphaState.setDestinationFunction(BlendState.DestinationFunction.OneMinusSourceAlpha);
			alphaState.setTestEnabled(true);
			alphaState.setTestFunction(BlendState.TestFunction.GreaterThan);
			alphaState.setEnabled(true);
		}
		
		collider.setRenderState(alphaState); //SCS
		collider.updateRenderState();

		// IMPORTANT: since the sphere will be transparent, place it
		// in the transparent render queue!
		collider.setRenderQueueMode(Renderer.QUEUE_TRANSPARENT);

		//if (AppletMain.CREATE_MAP_TEST) {
			sq.threeds.add(this);
		//}

	}


}
