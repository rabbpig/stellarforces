package dsr.models.scenery;

import java.io.IOException;

import com.jme.scene.Node;

import dsr.AppletMain;
import dsr.models.CollisionBox;
import dsr.models.ModelLoaders;
import dsr.models.map.SlidingDoor;
import dsrwebserver.tables.MapDataTable;

public class LoadTruck extends AbstractSceneryModel {

	private static final String PATH = AppletMain.DATA_DIR + "models/modelpack9/loadtruck" ;

	private static final long serialVersionUID = 1L;

	private Node body_node;

	public LoadTruck(AppletMain m, float x, float z, int side) {
		super(m, "LoadTruck", x+1f, z+0.0f, 0f);
		
		//super.setIsCollidable(true);
		this.blocks_view = true;
		this.can_be_shot = true;

		body_node = (Node)main.model_cache.getNode(PATH);
		try {
			if (body_node == null) {
				body_node = ModelLoaders.LoadMD2Model(main.getDisplay(), PATH, "loadtrucka.md2", "loadtrucka.bmp", 0.022f);
				body_node.setName(this.getName() + "_body");
				main.model_cache.putNode(PATH, body_node);
			}
			this.attachChild(body_node);

			switch (side) {
			case 0: // Side
				break;
			case 1: // Back
				setLocalRotation(SlidingDoor.q90);
				// Set floor squares as wall squares to block movement
				for (int z2=0 ; z2<4 ; z2++) {
					for (int x2=0 ; x2<2 ; x2++) {
						main.mapdata.getSq_MaybeNULL((int)x+x2, (int)z+z2).major_type = MapDataTable.MT_WALL;
					}
				}
				break;
			case 2: // Other side
				setLocalRotation(SlidingDoor.q180);
				break;
			case 3: // Front
				setLocalRotation(SlidingDoor.q270);
				break;
			default:
				throw new RuntimeException("Unknown door rotation");
			}

			this.updateRenderState();
			
		} catch (IOException ex) {
			AppletMain.HandleError(m, ex);
		}
		
		// Add collider box - must touch the ground for LOS purposes!
		collider = new CollisionBox(this.getLocalTranslation(), 1.5f, 1f, 1f, this);
		collider.setCullHint(CullHint.Always);

	}


}
