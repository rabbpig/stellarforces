package dsr.models.scenery;

import com.jme.image.Texture;
import com.jme.math.FastMath;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.scene.state.CullState;
import com.jme.scene.state.TextureState;
import com.jme.system.DisplaySystem;
import com.jme.util.TextureManager;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.AppletMapSquare;
import dsr.models.CollisionCylinder;

public class SmallPost extends GameObject {

	private static final float RAD = 0.1f;
	private static final float HEIGHT = .5f;

	private static final long serialVersionUID = 1L;

	public static CullState cs;
	private static TextureState ts;
	protected Quaternion q_StandStill = new Quaternion();

	public SmallPost(AppletMain main, AppletMapSquare sq) {
		super(main, "SmallPost_Model", true, true, false, sq.x, sq.y-1f, HEIGHT/2f);

		this.collider = new CollisionCylinder("Post", new Vector3f(0f, HEIGHT, 0f), 8, 8, RAD, HEIGHT, true, this);

		if (ts == null) {
			ts = main.getDisplay().getRenderer().createTextureState();  
			ts.setEnabled(true);  
			ts.setTexture(TextureManager.loadTexture(AppletMain.DATA_DIR + "textures/smallpost.png", Texture.MinificationFilter.NearestNeighborLinearMipMap, Texture.MagnificationFilter.NearestNeighbor), 0);
		}
		this.setRenderState(ts);
		
		if (cs == null) {
			cs = DisplaySystem.getDisplaySystem().getRenderer().createCullState();
			cs.setEnabled(true);
			cs.setCullFace(CullState.Face.Back);
		}
		this.setRenderState(cs);

		this.attachChild(collider);

		// Rotate
		Quaternion q = new Quaternion();
		q.fromAngleAxis(90 * FastMath.DEG_TO_RAD, new Vector3f(1,0,0));
		this.setLocalRotation(q);

		//if (AppletMain.CREATE_MAP_TEST) {
			sq.threeds.add(this);
		//}

	}


}
