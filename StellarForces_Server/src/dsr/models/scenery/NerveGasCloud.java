package dsr.models.scenery;

import ssmith.lang.Functions;

import com.jme.image.Texture;
import com.jme.math.Vector3f;
import com.jme.renderer.Renderer;
import com.jme.scene.state.BlendState;
import com.jme.scene.state.CullState;
import com.jme.scene.state.TextureState;
import com.jme.system.DisplaySystem;
import com.jme.util.TextureManager;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.AppletMapSquare;
import dsr.models.CollisionBox;

public class NerveGasCloud extends GameObject {

	private static final float RAD = 0.5f;

	private static final long serialVersionUID = 1L;

	public static CullState cs;
	private static TextureState ts;
	private static BlendState alphaState;

	public NerveGasCloud(AppletMain main, AppletMapSquare sq) {
		super(main, "NerveGasCloud", true, false, true, sq.x+0.5f, sq.y+0.5f, 0);
		
		float xoff = Functions.rndFloat(-0.2f, 0.2f);
		float yoff = Functions.rndFloat(-0.1f, 0.1f);
		float zoff = Functions.rndFloat(-0.2f, 0.2f);
		this.collider =  new CollisionBox(new Vector3f(0, RAD, 0), RAD+xoff, RAD+yoff, RAD+zoff, this);
		if (ts == null) {
			ts = main.getDisplay().getRenderer().createTextureState();  
			ts.setEnabled(true);  
			ts.setTexture(TextureManager.loadTexture(AppletMain.DATA_DIR + "textures/nerve_gas.png", Texture.MinificationFilter.NearestNeighborLinearMipMap, Texture.MagnificationFilter.NearestNeighbor), 0);
		}
		
		collider.setRenderState(ts);

		if (cs == null) {
			cs = DisplaySystem.getDisplaySystem().getRenderer().createCullState();
			cs.setEnabled(true);
			cs.setCullFace(CullState.Face.Back);
		}
		collider.setRenderState(cs);

		collider.updateRenderState();
		this.attachChild(collider);

		this.updateModelBound();

		// to handle transparency: a BlendState
		if (alphaState == null) {
			alphaState = main.getDisplay().getRenderer().createBlendState();
			alphaState.setBlendEnabled(true);
			alphaState.setSourceFunction(BlendState.SourceFunction.SourceAlpha);
			alphaState.setDestinationFunction(BlendState.DestinationFunction.OneMinusSourceAlpha);
			alphaState.setTestEnabled(true);
			alphaState.setTestFunction(BlendState.TestFunction.GreaterThan);
			alphaState.setEnabled(true);
		}
		collider.setRenderState(alphaState);
		collider.updateRenderState();

		// IMPORTANT: since the sphere will be transparent, place it
		// in the transparent render queue!
		collider.setRenderQueueMode(Renderer.QUEUE_TRANSPARENT);

		//if (AppletMain.CREATE_MAP_TEST) {
			sq.threeds.add(this);
		//}

	}
	
}
