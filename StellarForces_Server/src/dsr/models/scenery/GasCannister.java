package dsr.models.scenery;

import com.jme.image.Texture;
import com.jme.math.FastMath;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.scene.state.CullState;
import com.jme.scene.state.TextureState;
import com.jme.system.DisplaySystem;
import com.jme.util.TextureManager;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.EquipmentData;
import dsr.models.CollisionCylinder;

public class GasCannister extends GameObject { 

	private static final float RAD = 0.3f;
	private static final float HEIGHT = 0.55f;

	private static final long serialVersionUID = 1L;

	public static CullState cs;
	private static TextureState ts;
	public EquipmentData eq;

	public GasCannister(AppletMain main, EquipmentData _eq, float x, float z) {
		super(main, "GasCannister_Model", true, true, false, x+0.5f, z, HEIGHT/2);

		eq = _eq;
		
		this.collider = new CollisionCylinder("GasCannister", new Vector3f(0f, HEIGHT, 0f), 8, 8, RAD, HEIGHT, true, this);

		if (ts == null) {
			ts = main.getDisplay().getRenderer().createTextureState();  
			ts.setEnabled(true);  
			ts.setTexture(TextureManager.loadTexture(AppletMain.DATA_DIR + "textures/gascannister.jpg", Texture.MinificationFilter.NearestNeighborLinearMipMap, Texture.MagnificationFilter.NearestNeighbor), 0);
		}
		this.setRenderState(ts);

		if (cs == null) {
			cs = DisplaySystem.getDisplaySystem().getRenderer().createCullState();
			cs.setEnabled(true);
			cs.setCullFace(CullState.Face.Back);
		}
		this.setRenderState(cs);
		this.attachChild(collider);

		// Rotate
		Quaternion q = new Quaternion();
		q.fromAngleAxis(90 * FastMath.DEG_TO_RAD, new Vector3f(1,0,0));
		this.setLocalRotation(q);
	}


}
