package dsr.models.equipment;

import com.jme.image.Texture;
import com.jme.math.Vector3f;
import com.jme.scene.shape.Sphere;
import com.jme.scene.state.CullState;
import com.jme.scene.state.TextureState;
import com.jme.system.DisplaySystem;
import com.jme.util.TextureManager;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.EquipmentData;
import dsr.models.CollisionBox;

public class Grenade extends GameObject {

	private static final float RAD = 0.08f;

	private static final long serialVersionUID = 1L;

	public static CullState cs;
	private static TextureState ts;

	public Grenade(AppletMain main, EquipmentData _equip, float x, float z) {
		super(main, _equip.getName(false) + "_Model", true, false, false, x+0.5f, z+0.5f, 0);

		this.collider =  new CollisionBox(new Vector3f(0, RAD, 0), RAD, 0.5f, RAD, this); // Changed to 0.5f so it can
		collider.setCullHint(CullHint.Always);

		if (ts == null) {
			ts = main.getDisplay().getRenderer().createTextureState();  
			ts.setEnabled(true);  
			ts.setTexture(TextureManager.loadTexture(AppletMain.DATA_DIR + "textures/floor2.jpg", Texture.MinificationFilter.NearestNeighborLinearMipMap, Texture.MagnificationFilter.NearestNeighbor), 0);
		}
		
		Sphere s = new Sphere("Grenade", new Vector3f(0, RAD, 0), 5, 5, RAD);
		s.setIsCollidable(false);
		
		s.setRenderState(ts);

		if (cs == null) {
			cs = DisplaySystem.getDisplaySystem().getRenderer().createCullState();
			cs.setEnabled(true);
			cs.setCullFace(CullState.Face.Back);
		}
		s.setRenderState(cs);

		s.updateRenderState();
		this.attachChild(collider);
		this.attachChild(s);

		this.updateModelBound();

	}


}
