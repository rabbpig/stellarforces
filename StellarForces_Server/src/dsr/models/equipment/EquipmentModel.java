package dsr.models.equipment;

import ssmith.lang.Functions;

import com.jme.image.Texture;
import com.jme.math.FastMath;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.scene.state.CullState;
import com.jme.scene.state.TextureState;
import com.jme.system.DisplaySystem;
import com.jme.util.TextureManager;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.EquipmentData;
import dsr.models.CollisionBox;
import dsr.models.scenery.GasCannister;
import dsr.models.units.MaleLightCorpse;
import dsr.models.units.TyrantCorpse;
import dsrwebserver.tables.EquipmentTypesTable;

public final class EquipmentModel extends GameObject {

	private static final float RAD = 0.2f;

	private static final long serialVersionUID = 1L;

	public static CullState cs;
	private static TextureState ts;
	//public EquipmentData equipment;

	public static GameObject Factory(AppletMain main, EquipmentData equip, float x, float z) {
		try {
			switch (equip.major_type) {
			case EquipmentTypesTable.ET_HUMAN_CORPSE_1:
				return new MaleLightCorpse(main, equip.getName(false), x, z, "models/ufo_ai/soldiers/malelight/", "body.md2", "body_cccp.jpg", 28);
			case EquipmentTypesTable.ET_HUMAN_CORPSE_2:
				return new MaleLightCorpse(main, equip.getName(false), x, z, "models/ufo_ai/soldiers/malelight/", "body.md2", "body_arctic.jpg", 28);
			case EquipmentTypesTable.ET_HUMAN_CORPSE_3:
				return new MaleLightCorpse(main, equip.getName(false), x, z, "models/ufo_ai/soldiers/malelight/", "body.md2", "body_jungle.jpg", 28);
			case EquipmentTypesTable.ET_HUMAN_CORPSE_4:
				return new MaleLightCorpse(main, equip.getName(false), x, z, "models/ufo_ai/soldiers/malelight/", "body.md2", "body_desert.jpg", 28);
			case EquipmentTypesTable.ET_SCIENTIST_CORPSE:
				return new MaleLightCorpse(main, equip.getName(false), x, z, "models/ufo_ai/civilians/male/", "scientist01.md2", "scientist01.jpg", 29);
			case EquipmentTypesTable.ET_ALIEN_CORPSE:
			case EquipmentTypesTable.ET_ALIEN_QUEEN_CORPSE:
			case EquipmentTypesTable.ET_CRAB_CORPSE:
			case EquipmentTypesTable.ET_GHOUL_CORPSE:
			case EquipmentTypesTable.ET_GHOUL_QUEEN_CORPSE:
				return new TyrantCorpse(main, x, z, null);
			case EquipmentTypesTable.ET_EGG:
				return new AlienEgg(main, equip, x, z);
			case EquipmentTypesTable.ET_GRENADE:
			case EquipmentTypesTable.ET_SMOKE_GRENADE:
			case EquipmentTypesTable.ET_NERVE_GAS:
			case EquipmentTypesTable.ET_DEATH_GRENADE:
				return new Grenade(main, equip, x, z);
			case EquipmentTypesTable.ET_GAS_CANNISTER:
				return new GasCannister(main, equip, x, z);
			default:
				// DON'T THROW AN ERROR HERE AS WE MAY ADD NEW TYPES THAT AN OLD CLIENT WON'T KNOW ABOUT
				return new EquipmentModel(main, equip, x, z);
			}
		} catch (Exception ex) {
			AppletMain.HandleError(main, ex);
			return new EquipmentModel(main, equip, x, z);
		}
	}

	
	private EquipmentModel(AppletMain main, EquipmentData _equip, float x, float z) {
		super(main, _equip.getName(false) + "_Model", true, false, false, x+0.5f, z+0.5f, 0);// SCS 18/10/10 - collides was FALSE!

		//equipment = _equip;

		this.collider =  new CollisionBox(new Vector3f(0, RAD, 0), RAD, RAD, RAD, this);
		if (ts == null) {
			ts = main.getDisplay().getRenderer().createTextureState();  
			ts.setEnabled(true);  
			ts.setTexture(TextureManager.loadTexture(AppletMain.DATA_DIR + "textures/crate.png", Texture.MinificationFilter.NearestNeighborLinearMipMap, Texture.MagnificationFilter.NearestNeighbor), 0);
		}
		collider.setRenderState(ts);

		if (cs == null) {
			cs = DisplaySystem.getDisplaySystem().getRenderer().createCullState();
			cs.setEnabled(true);
			cs.setCullFace(CullState.Face.Back);
		}
		collider.setRenderState(cs);

		collider.updateRenderState();
		this.attachChild(collider);

		this.updateModelBound();

		// Rotate by a random amount
		Quaternion q = new Quaternion();
		q.fromAngleAxis(Functions.rndFloat(0, 359) * FastMath.DEG_TO_RAD, new Vector3f(0,-1,0));
		this.setLocalRotation(q);

	}


}
