package dsr.models;

import dsr.GameObject;

public interface ICollidable {
	
	public GameObject getOwner();

}
