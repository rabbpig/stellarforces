package dsr.models.units;

import java.io.IOException;

import ssmith.lang.Functions;

import com.jme.math.FastMath;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.scene.Controller;
import com.jme.scene.Node;
import com.jme.scene.Spatial;
import com.jmex.model.animation.KeyframeController;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.UnitData;
import dsr.models.CollisionBox;
import dsr.models.ModelLoaders;
import dsrwebserver.tables.UnitsTable;

public abstract class AbstractCorpse extends GameObject {

	private static final long serialVersionUID = 1L;

	public Node body_node;
	private String path, mdl1, skin1;
	private float scale;
    private int frame;


	public static AbstractCorpse CorpseFactory(AppletMain m, UnitData data) { 
		try {
			switch (data.model_type) {
			case UnitsTable.MT_MALE_SIDE_1:
				return new MaleLightCorpse(m, data.name, data.getMapX(), data.getMapZ(), "models/ufo_ai/soldiers/malelight/", "body.md2", "body_cccp.jpg", 28);
			case UnitsTable.MT_MALE_SIDE_2:
				return new MaleLightCorpse(m, data.name, data.getMapX(), data.getMapZ(), "models/ufo_ai/soldiers/malelight/", "body.md2", "body_arctic.jpg", 28);
			case UnitsTable.MT_MALE_SIDE_3:
				return new MaleLightCorpse(m, data.name, data.getMapX(), data.getMapZ(), "models/ufo_ai/soldiers/malelight/", "body.md2", "body_jungle.jpg", 28);
			case UnitsTable.MT_MALE_SIDE_4:
				return new MaleLightCorpse(m, data.name, data.getMapX(), data.getMapZ(), "models/ufo_ai/soldiers/malelight/", "body.md2", "body_desert.jpg", 28);
			case UnitsTable.MT_ALIEN_TYRANT:
			case UnitsTable.MT_QUEEN_ALIEN:
			case UnitsTable.MT_CRAB:
			case UnitsTable.MT_GHOUL:
				return new TyrantCorpse(m, data.getMapX(), data.getMapZ(), data);
			case UnitsTable.MT_SCIENTIST:
				return new MaleLightCorpse(m, data.name, data.getMapX(), data.getMapZ(), "models/ufo_ai/civilians/male/", "scientist01.md2", "scientist01.jpg", 29);
			case UnitsTable.MT_BLOB:
				return null;//new MaleLightCorpse(m, data.name, data.getMapX(), data.getMapZ(), "models/ufo_ai/civilians/male/", "scientist01.md2", "scientist01.jpg", 29);
			default:
				throw new RuntimeException("Unknown model type: " + data.model_type);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
	}


	protected AbstractCorpse(AppletMain m, String name, float _x, float _z, float _y_off, float _diam, String _path, String _mdl1, String _skin1, float _scale, int _frame) {
		super(m, name, false, false, false, _x, _z, _y_off);

		path = _path;
		mdl1 =_mdl1;
		skin1 = _skin1;
		scale = _scale;
		frame = _frame;
		
		this.loadModels();

		this.can_be_shot = false;
		this.blocks_view = false;
		
		// Rotate by a random amount
		Quaternion q = new Quaternion();
		q.fromAngleAxis(Functions.rndFloat(0, 359) * FastMath.DEG_TO_RAD, new Vector3f(0,-1,0));
		this.setLocalRotation(q);
		
		this.anim_corpse();

		// Add collider box - must touch the ground for LOS purposes!
		collider = new CollisionBox(this.getLocalTranslation(), _diam/2, this.getLocalTranslation().y, _diam/2, this);
		collider.setCullHint(CullHint.Always);

		main.attachToRootNode(collider, false);
	}


	private void loadModels() {
		try {
			//Main.p("Loading model " + total_models);

			body_node = (Node)main.model_cache.getNode(path + mdl1 + skin1);
			if (body_node == null) {
				body_node = ModelLoaders.LoadMD2Model(main.getDisplay(), path, mdl1, skin1, scale);
				body_node.setName(this.getName() + "_body");
				main.model_cache.putNode(path + mdl1 + skin1, body_node);
			}
			this.attachChild(body_node);

			this.updateRenderState();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	public void anim_corpse() {
		if (this.children != null) {
			this.animate(frame, frame, Controller.RT_CLAMP, 0);
		}
	}

	
	protected void animate(int s, int f, int type, int speed) {
		if (this.getChildren() != null) {
			Spatial sp = this.getChild(0);// Only animate body!
			if (sp instanceof Node) {
				Node n = (Node)sp;
				KeyframeController kc = (KeyframeController) n.getChild(0).getController(0);
				kc.setSpeed(speed);
				kc.setRepeatType(type);
				kc.setNewAnimationTimes(s, f);
			}
		}
	}


}
