package dsr.models.units;

import java.io.IOException;

import ssmith.lang.Functions;
import ssmith.lang.Geometry;
import ssmith.util.PointByte;

import com.jme.math.Vector3f;
import com.jme.scene.Node;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.IUpdatable;
import dsr.SoundEffects;
import dsr.TextureStateCache;
import dsr.comms.UnitDataComms;
import dsr.data.AppletMapSquare;
import dsr.data.EquipmentData;
import dsr.data.UnitData;
import dsr.models.CollisionBox;
import dsr.start.StartupNew;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitsTable;

public abstract class AbstractUnit extends GameObject implements IUpdatable {

	// Each row is a level of armour
	private static String[] male_data = {"male", "head02_jungle.jpg", "head02_arctic.jpg", "head05b_desert.jpg", "head01_desert.jpg"};
	private static String[] malelight_data = {"malelight", "head01a_cccp.jpg", "head01b.jpg", "head02a.jpg", "head02b.jpg"};
	private static String[] malemedium_data = {"malemedium", "head01_cccp.jpg", "head01_arctic.jpg", "head01_jungle.jpg", "head01_desert.jpg"};
	private static String[] malepower_data = {"malepower", "head01_cccp.jpg", "head01_arctic.jpg", "head01_jungle.jpg", "head01_desert.jpg"};

	public static float SPEED = 1f;

	private static final long serialVersionUID = 1L;

	public UnitData unit_data;
	public Node body_node, weapon_node;

	public static AbstractUnit Factory(AppletMain m, UnitData unit_data) {
		try {
			String[] arr_data = malelight_data; // default
			int walk_start = 191;
			int walk_end = 206;
			if (StartupNew.hi_mem) {
				if (unit_data.protection == 0) {
					arr_data = male_data;
				} else if (unit_data.protection == 10) {
					arr_data = malelight_data;
				} else if (unit_data.protection == 20 || unit_data.protection == 30) {
					arr_data = malemedium_data;
				} else {
					arr_data = malepower_data;
					walk_start = 63;
					walk_end = 81;
				}
			}

			switch (unit_data.model_type) {
			// IF YOU CHANGE THIS, DON'T FORGET TO CHANGE THE CORPSE FUNCTION AS WELL!
			case UnitsTable.MT_MALE_SIDE_1: // Red
				return new MaleLight(m, unit_data.name, unit_data.getMapX(), unit_data.getMapZ(), "models/ufo_ai/soldiers/" + arr_data[0] + "/", "body.md2", "body_cccp.jpg", "head01a.md2", arr_data[unit_data.getSide()], 95, 142, 1, 28, walk_start, walk_end, unit_data);
			case UnitsTable.MT_MALE_SIDE_2: // Blue
				return new MaleLight(m, unit_data.name, unit_data.getMapX(), unit_data.getMapZ(), "models/ufo_ai/soldiers/" + arr_data[0] + "/", "body.md2", "body_arctic.jpg", "head01b.md2", arr_data[unit_data.getSide()], 95, 142, 1, 28, walk_start, walk_end, unit_data);
			case UnitsTable.MT_MALE_SIDE_3: // Green
				return new MaleLight(m, unit_data.name, unit_data.getMapX(), unit_data.getMapZ(), "models/ufo_ai/soldiers/" + arr_data[0] + "/", "body.md2", "body_yellow.jpg", "head02a.md2", arr_data[unit_data.getSide()], 95, 142, 1, 28, walk_start, walk_end, unit_data);
			case UnitsTable.MT_MALE_SIDE_4: // 
				return new MaleLight(m, unit_data.name, unit_data.getMapX(), unit_data.getMapZ(), "models/ufo_ai/soldiers/" + arr_data[0] + "/", "body.md2", "body_desert.jpg", "head02b.md2", arr_data[unit_data.getSide()], 95, 142, 1, 28, walk_start, walk_end, unit_data);
			case UnitsTable.MT_ALIEN_TYRANT:
				return new Tyrant(m, unit_data.getMapX(), unit_data.getMapZ(), unit_data);
			case UnitsTable.MT_QUEEN_ALIEN:
				return new QueenAlien(m, unit_data.getMapX(), unit_data.getMapZ(), unit_data);
			case UnitsTable.MT_SCIENTIST:
				return new MaleLight(m, unit_data.name, unit_data.getMapX(), unit_data.getMapZ(), "models/ufo_ai/civilians/male/", "scientist01.md2", "scientist01.jpg", "sci_head03a.md2", "sci_head03.jpg", 69, 69, 1, 28, 72, 88, unit_data);
			case UnitsTable.MT_BLOB:
				return new Blob(m, unit_data.getMapX(), unit_data.getMapZ(), unit_data);
				// IF YOU CHANGE THIS, DON'T FORGET TO CHANGE THE CORPSE FUNCTION AS WELL!
			default:
				throw new RuntimeException();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
	}


	protected AbstractUnit(AppletMain m, String node_name, float _x, float _z, float _y_off, float _diam, UnitData data) {
		super(m, node_name, false, true, false, _x, _z, _y_off);
		unit_data = data;

		main.attachToRootNode(this, false);

		// Add collider box - must touch the ground for LOS purposes!
		collider = new CollisionBox(this.getLocalTranslation(), _diam/2, this.getLocalTranslation().y, _diam/2, this);
		/*Vector3f tmp_centre = this.getLocalTranslation().clone();
		tmp_centre.y = 0.5f;// So the box stretches from floor to ceiling
		collider = new CollisionBox(tmp_centre, _diam/2, 0.5f, _diam/2, this);
		 */
		collider.setCullHint(CullHint.Always);

		/*if (AppletMain.DEBUG) {
			collider.setCullHint(CullHint.Never);
			collider.setRandomColors();
			collider.updateRenderState();
		}*/

		main.attachToRootNode(collider, false);
	}

	public abstract void anim_standStill();

	public abstract void anim_walk();

	public abstract void anim_shoot();

	public abstract void anim_harmed();

	public abstract void anim_die();

	public void updateGeometricState(float time, boolean initiator) {
		super.updateGeometricState(time, initiator);
		if (collider != null) {
			collider.updateGeometricState(time, initiator);
		}
	}


	public void moveFwd() {
		PointByte p = GetDirFromAngle(this.unit_data.getAngle());
		this.move(p, 1);
	}


	public void moveBack() {
		PointByte p = GetDirFromAngle(this.unit_data.getAngle() + 180);
		this.move(p, 2f);
	}


	public void moveStrafeLeft() {
		PointByte p = GetDirFromAngle(this.unit_data.getAngle() - 90);
		this.move(p, 1.5f);
	}


	public void moveStrafeRight() {
		PointByte p = GetDirFromAngle(this.unit_data.getAngle() + 90);
		this.move(p, 1.5f);
	}


	public void turnBy(int ang) {
		if (this.unit_data.checkAndReduceAPs(main, 1, true)) {
			this.unit_data.turnBy(ang);

			main.updateUnitOnServer(this.unit_data);
			boolean who_can_see[] = main.whichSidesCanSeeUnit(this.unit_data);
			main.sendEventToServer_UnitMoved(this.unit_data, who_can_see);

			main.recalcVisibleEnemiesAndOppFire(true, this.unit_data);
			main.updateMenu();
		}
	}


	public void checkForNerveGasAndFire(int aps) {
		try {
			AppletMapSquare sq = this.main.mapdata.getSq_MaybeNULL(this.unit_data.getMapX(), this.unit_data.getMapZ());
			if (sq != null) {
				if (sq.smoke_type == EquipmentTypesTable.ET_NERVE_GAS) {
					UnitData unit = UnitData.GetUnitDataFromID(main.units, sq.smoke_caused_by);
					this.unit_data.damage(main, (short)aps, unit, UnitDataComms.FOA_NERVE_GAS);
				} else if (sq.smoke_type == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
						UnitData unit = UnitData.GetUnitDataFromID(main.units, sq.smoke_caused_by);
						this.unit_data.damage(main, (short)(aps/2), unit, UnitDataComms.FOA_FIRE);
				}
			}
		} catch (Exception ex) {
			AppletMain.HandleError(main, ex);
		}
	}


	public AppletMapSquare getSquareInFrontOfUnit_MaybeNULL() {
		PointByte offset = GetDirFromAngle(this.unit_data.getAngle());
		PointByte new_pos = new PointByte(this.unit_data.getMapX() + offset.x, this.unit_data.getMapZ() + offset.y);
		AppletMapSquare sq = this.main.mapdata.getSq_MaybeNULL(new_pos.x, new_pos.y);
		return sq;
	}


	private void move(PointByte offset, float mult) {
		// Check we're not currently moving
		if (this.main.isObjectUpdating(this) == false) {
			// See if the square is empty
			PointByte new_pos = new PointByte(this.unit_data.getMapX() + offset.x, this.unit_data.getMapZ() + offset.y);
			AppletMapSquare sq = this.main.mapdata.getSq_MaybeNULL(new_pos.x, new_pos.y);
			if (sq != null) { // In case walking off edge of map
				boolean can_walk = sq.major_type == MapDataTable.MT_FLOOR && sq.texture_code != TextureStateCache.TEX_WATER;
				// Aliens can walk through alien walls
				if (main.game_data.mission_type == AbstractMission.ALIENS && this.unit_data.getSide() == 2) {
					if (sq.door_type > 0) {
						can_walk = false; // Aliens can't walk through walls
					} else if (can_walk == false) { // Double APs if walking through walls
						can_walk = can_walk || (sq.major_type == MapDataTable.MT_WALL && sq.texture_code == TextureStateCache.TEX_ALIEN_SKIN);
						if (can_walk) {
							mult = mult * 2;
						}
					}
				}
				if (can_walk) {
					byte ap_cost = this.getAPCostToWalk(mult, sq);
					UnitData opponent = main.getUnitAt(new_pos.x, new_pos.y);
					if (opponent != null) { // Inc cost for combat
						ap_cost = (byte)(ap_cost * 2);
					}
					if (this.unit_data.checkAndReduceAPs(main, ap_cost, true)) {
						if (opponent == null) {
							main.playSound(SoundEffects.FOOTSTEP);
							this.unit_data.setMapLocation(main, new_pos.x, new_pos.y, sq);
							main.updateUnitOnServer(this.unit_data);

							boolean who_can_see[] = main.whichSidesCanSeeUnit(this.unit_data);
							main.sendEventToServer_UnitMoved(this.unit_data, who_can_see);

						} else {
							boolean snafu_blocked = (main.game_data.is_snafu == 1 && main.game_data.opp_fire[opponent.getSide()] == 0);
							if (main.game_data.areSidesFriends(opponent.getSide(), this.unit_data.getSide()) || snafu_blocked) {
								if (main.game_data.areSidesFriends(opponent.getSide(), this.unit_data.getSide())) {
									// Is it a blob?
									if (this.unit_data.model_type == UnitsTable.MT_BLOB && opponent.model_type == UnitsTable.MT_BLOB) {
										// Merge!
										this.unit_data.incHealth(opponent.getHealth(), true);
										this.unit_data.combat_skill += opponent.combat_skill;
										this.unit_data.strength += opponent.strength;
										main.updateUnitOnServer(this.unit_data);

										// Remove the blob we moved into
										opponent.damage(main, opponent.getHealth(), this.unit_data, UnitDataComms.FOA_MERGED);
										return;
									} else {
										main.addToHUD(opponent.name + " is in the way.");
									}
								} else if (snafu_blocked) {
									main.addToHUD(opponent.name + " is not set as an enemy.");
								}
								// Re-add APs
								this.unit_data.incAPs(ap_cost);
								main.updateUnitOnServer(this.unit_data);
							} else {
								main.closeCombat(this.unit_data, opponent);
							}
						}
					}
				} else if (sq.major_type == MapDataTable.MT_COMPUTER) {
					if (sq.destroyed == 0) {
						if (main.game_data.areSidesFriends(sq.owner_side, this.unit_data.getSide()) == false) {
							int ap_cost = this.getAPCostToWalk(mult, sq) * 3;
							if (this.unit_data.checkAndReduceAPs(main, ap_cost, true)) {
								main.updateUnitOnServer(this.unit_data);
								int str = this.unit_data.strength;
								if (this.unit_data.current_item != null) {
									str += this.unit_data.current_item.cc_damage;
								}
								AppletMain.p("Str: " + str);
								int rnd = Functions.rnd(1, str);
								AppletMain.p("Rnd: " + rnd);
								AppletMain.p("Required: " + AppletMain.STRENGTH_TO_DESTROY_COMP);
								if (rnd >= AppletMain.STRENGTH_TO_DESTROY_COMP) {
									main.computerDestroyed(sq.cpu, this.unit_data);
								} else {
									main.addToHUD("Computer not destroyed.");
								}
							}
						} else {
							// We own the computer!
						}
					} else {
						// Computer already destroyed!
					}
				}
			}
		}
	}


	private byte getAPCostToWalk(float mult, AppletMapSquare sq) {
		// Calc AP cost
		byte ap_cost = 6;
		if (this.unit_data.getAngle() % 90 == 0) { // Move at 90 degrees
			ap_cost = 4;
		}
		if (sq.door_type > 0) {
			if (sq.door_open == false) {
				ap_cost = (byte)(ap_cost * 2);
			}
		}
		if (sq.scenery_code == MapDataTable.HEDGEROW) {// Brick walls not drawn FSR! || sq.scenery_code == MapDataTable.BRICK_WALL) {
			ap_cost = (byte)(ap_cost * 2);
		}
		ap_cost = (byte)((float)ap_cost * mult);
		return ap_cost;
	}

	
	private static PointByte tmp = new PointByte(0, 0);
	private static PointByte GetDirFromAngle(int ang_deg) {
		ang_deg = Geometry.NormalizeAngle(ang_deg);
		switch (ang_deg) {
		case 0:
			//return new PointByte(1, 0);
			tmp.x = 1;
			tmp.y = 0;
			break;
		case 45:
			//return new PointByte(1, 1);
			tmp.x = 1;
			tmp.y = 1;
			break;
		case 90:
			tmp.x = 0;
			tmp.y = 1;
			break;
			//return new PointByte(0, 1);
		case 135:
			//return new PointByte(-1, 1);
			tmp.x = -1;
			tmp.y = 1;
			break;
		case 180:
			//return new PointByte(-1, 0);
			tmp.x = -1;
			tmp.y = 0;
			break;
		case 225:
			//return new PointByte(-1, -1);
			tmp.x = -1;
			tmp.y = -1;
			break;
		case 270:
			//return new PointByte(0, -1);
			tmp.x = 0;
			tmp.y = -1;
			break;
		case 315:
			//return new PointByte(1, -1);
			tmp.x = 1;
			tmp.y = -1;
			break;
		default:
			throw new RuntimeException("Unknown angle: " + ang_deg);
		}
		return tmp;
	}


	public void changeCurrentItem(EquipmentData equipment) {
		this.unit_data.current_item = equipment;
	}


	public void update(float interpolation) {
		Vector3f target_pos = new Vector3f(this.unit_data.getMapX()+0.5f, this.getLocalTranslation().y, this.unit_data.getMapZ()+0.5f);
		float dist_to_target = this.getLocalTranslation().distance(target_pos);
		//AppletMain.p("Dist:" + dist_to_target);
		if (dist_to_target <= 0.1f) { // Close, so just stick them onto target
			this.getLocalTranslation().x = unit_data.getMapX() + 0.5f;
			this.getLocalTranslation().z = unit_data.getMapZ() + 0.5f;
			this.main.removeObjectForUpdate(this);
			this.main.objectFinishedMoving(this.unit_data);
			this.anim_standStill();
		} else { // Keep moving!
			Vector3f move_dir = target_pos.subtract(this.getLocalTranslation()).normalize();
			float x_off = (move_dir.x * SPEED * interpolation);
			if (x_off > dist_to_target) {
				x_off = dist_to_target;
			}
			this.getLocalTranslation().x += x_off;

			float z_off = (move_dir.z * SPEED * interpolation);
			if (z_off > dist_to_target) {
				z_off = dist_to_target;
			}
			this.getLocalTranslation().z += z_off;
		}
		this.updateGeometricState(0, true);
	}


}
