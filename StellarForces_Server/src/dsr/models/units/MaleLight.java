package dsr.models.units;

import java.io.IOException;

import com.jme.math.FastMath;
import com.jme.math.Vector3f;
import com.jme.scene.Controller;

import dsr.AppletMain;
import dsr.data.UnitData;

public class MaleLight extends AbstractMD2UnitModel {
	
    private static final float WIDTH = 0.6f;
    public static final float Y_OFF = 0.75f;
    private static final float SCALE = 0.022f;
    
	private static final long serialVersionUID = 1L;

    private int stand_frame, shoot_frame, die_start, die_end, walk_start_frame, walk_end_frame;

	protected MaleLight(AppletMain m, String name, float x, float z, String model_path, String model1, String skin1, String model2, String skin2, int _stand_frame, int _shoot_frame, int _die_start, int _die_end, int walk_start, int walk_end, UnitData data) throws IOException {
		super(m, "MaleLight_" + name, x, z, Y_OFF, WIDTH, AppletMain.DATA_DIR + model_path, model1, skin1, model2, skin2, SCALE, data);
		
	    stand_frame = _stand_frame;
	    shoot_frame = _shoot_frame;
	    die_start = _die_start;
	    die_end = _die_end;
	    walk_start_frame = walk_start;
	    walk_end_frame = walk_end;
	    
	    q_StandStill.fromAngleAxis(270 * FastMath.DEG_TO_RAD, new Vector3f(0,1,0));
	    q_Walk.fromAngleAxis(270 * FastMath.DEG_TO_RAD, new Vector3f(0,1,0));
	    
	    // Adjust the head
	    this.getChild(1).setLocalTranslation(0f, 0.23f, -0.1f);

		this.anim_standStill();
	}

	
	public void anim_standStill() {
		if (this.children != null) {
			rotateChildSpatials(q_StandStill);
			this.animate(stand_frame, stand_frame, Controller.RT_CLAMP, 0);
		}
	}

	
	public void anim_walk() {
		if (this.children != null) {
			rotateChildSpatials(q_Walk);
			//this.animate(191, 206, Controller.RT_CYCLE, 22);
			this.animate(walk_start_frame, walk_end_frame, Controller.RT_CYCLE, 22);
		}
	}

	
	public void anim_shoot() {
		rotateChildSpatials(q_Shoot);
		this.animate(shoot_frame, shoot_frame, Controller.RT_CLAMP, 0);
	}

	

	public void anim_die() {
		if (this.children != null) {
			rotateChildSpatials(q_Die);
			this.animate(die_start, die_end, Controller.RT_CLAMP, 0);
		}
	}


	@Override
	public void anim_harmed() {
		
	}



}
