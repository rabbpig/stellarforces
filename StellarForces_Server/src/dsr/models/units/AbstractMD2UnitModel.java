package dsr.models.units;

import java.io.IOException;


import com.jme.math.Quaternion;
import com.jme.scene.Node;
import com.jme.scene.Spatial;
import com.jmex.model.animation.KeyframeController;

import dsr.AppletMain;
import dsr.data.UnitData;
import dsr.models.ModelLoaders;

public abstract class AbstractMD2UnitModel extends AbstractUnit  {

	private static final long serialVersionUID = 1L;

	// These are the angle adjustments for various anims
	protected Quaternion q_Walk = new Quaternion();
	protected Quaternion q_StandStill = new Quaternion();
	protected Quaternion q_Shoot = new Quaternion();
	protected Quaternion q_Attack = new Quaternion();
	protected Quaternion q_Die = new Quaternion();

	private String path, mdl1, skin1, mdl2, skin2;
	private float scale;

	protected AbstractMD2UnitModel(AppletMain m, String node_name, float _x, float _z, float y_off, float _w, String _path, String _mdl1, String _skin1, String _mdl2, String _skin2, float _scale, UnitData data) throws IOException {
		super(m, node_name, _x, _z, y_off, _w, data);

		path = _path;
		mdl1 = _mdl1;
		skin1 =_skin1;
		mdl2 = _mdl2;
		skin2 =_skin2;
		scale = _scale;

		this.loadModels();
	}


	private void loadModels() {
		try {
			body_node = (Node)main.model_cache.getNode(path + mdl1 + skin1);
			if (body_node == null) {
				body_node = ModelLoaders.LoadMD2Model(main.getDisplay(), path, mdl1, skin1, scale);
				body_node.setName(this.getName() + "_body");
				main.model_cache.putNode(path + mdl1 + skin1, body_node);
			}
			this.attachChild(body_node);

			if (mdl2.length() > 0) {
				weapon_node = (Node)main.model_cache.getNode(path + mdl2 + skin2);
				if (weapon_node == null) {
					weapon_node = ModelLoaders.LoadMD2Model(main.getDisplay(), path, mdl2, skin2, scale);
					weapon_node.setName(this.getName() + "_weapon");
					main.model_cache.putNode(path + mdl2 + skin2, weapon_node);
				}
				this.attachChild(weapon_node);
			}
			this.updateRenderState();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	protected void rotateChildSpatials(Quaternion q) {
		if (this.getChildren() != null) {
			int ch = this.getChildren().size();
			for (int i=0 ; i<ch ; i++) {
				Spatial sp = this.getChild(i);
				if (sp instanceof Node) {
					Node n = (Node)sp;
					n.setLocalRotation(q);
				}
			}
		}
	}


	protected void animate(int s, int f, int type, int speed) {
		if (this.getChildren() != null) {
			Spatial sp = this.getChild(0);// Only animate body!
			if (sp instanceof Node) {
				Node n = (Node)sp;
				KeyframeController kc = (KeyframeController) n.getChild(0).getController(0);
				kc.setSpeed(speed);
				kc.setRepeatType(type);
				kc.setNewAnimationTimes(s, f);
			}
		}
	}

}
