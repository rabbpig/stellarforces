package dsr.models.units;

import com.jme.image.Texture;
import com.jme.math.Vector3f;
import com.jme.scene.shape.Sphere;
import com.jme.scene.state.CullState;
import com.jme.scene.state.TextureState;
import com.jme.system.DisplaySystem;
import com.jme.util.TextureManager;

import dsr.AppletMain;
import dsr.data.UnitData;
import dsr.models.CollisionBox;

public class Blob extends AbstractUnit {
	
	private static final float MIN_RAD = 0.14f;
	
	private static final long serialVersionUID = 1L;

	private static CullState cs;
	private static TextureState ts;
	private Sphere s;
	private float radius;

	private static float getRad(int health) {
		float r = health/200f;
		if (r < MIN_RAD) {
			r = MIN_RAD;
		}
		if (r > 1f) {
			r = 1f;
		}
		return r;
	}
	
	
	public Blob(AppletMain m, float _x, float _z, UnitData data) {
		super(m, "Blob" + data.order_by, _x, _z, 0.5f, getRad(data.getHealth()), data);

		radius = getRad(data.getHealth());
		
		if (ts == null) {
			ts = main.getDisplay().getRenderer().createTextureState();  
			ts.setEnabled(true);  
			ts.setTexture(TextureManager.loadTexture(AppletMain.DATA_DIR + "textures/game/alienskin.png", Texture.MinificationFilter.NearestNeighborLinearMipMap, Texture.MagnificationFilter.NearestNeighbor), 0);
		}
		
		s = new Sphere("BlobSphere", new Vector3f(0, radius-0.5f, 0), 10, 10, radius);
		this.updateSphere();
		
		s.setIsCollidable(true);

		s.setRenderState(ts);

		if (cs == null) {
			cs = DisplaySystem.getDisplaySystem().getRenderer().createCullState();
			cs.setEnabled(true);
			cs.setCullFace(CullState.Face.Back);
		}
		s.setRenderState(cs);

		s.updateRenderState();
		this.attachChild(s);

		this.updateModelBound();

		// Rotate by a random amount
		/*Quaternion q = new Quaternion();
		q.fromAngleAxis(Functions.rndFloat(0, 359) * FastMath.DEG_TO_RAD, new Vector3f(0,-1,0));
		this.setLocalRotation(q);
		*/
	}
	
	
	public void updateSphere() {
		radius = getRad(this.unit_data.getHealth());
		this.s.updateGeometry(this.s.center, 10, 10, radius);
		s.updateGeometricState(-1, true);
		
		// Adjust size of collider
		CollisionBox c = (CollisionBox)this.collider;
		c.xExtent = radius/2;
		c.zExtent = radius/2;
		c.updateGeometry();
	}

	
	@Override
	public void anim_die() {
		// Do nothing
	}

	@Override
	public void anim_shoot() {
		// Do nothing
	}

	@Override
	public void anim_standStill() {
		// Do nothing
	}

	@Override
	public void anim_walk() {
		// Do nothing
	}

	@Override
	public void anim_harmed() {
		//this.updateSphere();
	}

}
