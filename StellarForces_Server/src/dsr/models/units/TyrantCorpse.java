package dsr.models.units;

import java.io.IOException;

import dsr.AppletMain;
import dsr.data.UnitData;

public final class TyrantCorpse extends AbstractCorpse {

	private static final float WIDTH = 0.45f;
	private static final float X_OFF = 0.5f;
	public static final float Y_OFF = 0.4f;
	private static final float Z_OFF = 0.5f;
	private static final float SCALE = 0.016f;

	private static final long serialVersionUID = 1L;

	public TyrantCorpse(AppletMain m, float x, float z, UnitData data) throws IOException {
		super(m, "TyrantCorpse", x+X_OFF, z+Z_OFF, Y_OFF, WIDTH, AppletMain.DATA_DIR + "models/tyrant/", "tris.MD2", "hivetyrant.png", SCALE, 183);

	}

}
