package dsr.models.units;

import java.io.IOException;

import ssmith.lang.Functions;

import com.jme.math.FastMath;
import com.jme.math.Vector3f;
import com.jme.scene.Controller;

import dsr.AppletMain;
import dsr.data.UnitData;

public class QueenAlien extends AbstractMD2UnitModel {

	private static final float WIDTH = 0.45f;
	private static final float X_OFF = 0.5f;
	public static final float Y_OFF = 0.8f;//0.4f;
	private static final float Z_OFF = 0.5f;
	private static final float SCALE = 0.04f;//0.032f;

	private static final long serialVersionUID = 1L;

	protected QueenAlien(AppletMain m, float x, float z, UnitData data) throws IOException {
		super(m, "QueenAlien", x+X_OFF, z+Z_OFF, Y_OFF, WIDTH, AppletMain.DATA_DIR + "models/xenoid/", "tris.MD2", "xenotype1.png", "", "", SCALE, data);

		q_StandStill.fromAngleAxis(270 * FastMath.DEG_TO_RAD, new Vector3f(0,1,0));
		q_Walk.fromAngleAxis(270 * FastMath.DEG_TO_RAD, new Vector3f(0,1,0)); // was 325
		
		this.anim_standStill();
	}

	public void anim_standStill() {
		if (this.children != null) {
			rotateChildSpatials(q_StandStill); // Works for Terminators!
			//this.animate(1, 2, Controller.RT_CLAMP, 3);
			this.animate(0, 1, Controller.RT_CYCLE, 2);
		}
	}
	
	
	public void anim_shoot() {
		//this.animate(1, 1, Controller.RT_CLAMP, 0);
	}


	public void anim_walk() {
		if (this.children != null) {
			rotateChildSpatials(q_Walk);
			//this.animate(40, 45, Controller.RT_CYCLE, 5);
			this.animate(40, 45, Controller.RT_CYCLE, 5);
		}
	}

	
	public void anim_die() {
		rotateChildSpatials(q_Die);

		// Choose random die
		int x = (Functions.rnd(0, 2)*6) + 178;
		this.animate(x, x+5, Controller.RT_CLAMP, 6);
	}

	@Override
	public void anim_harmed() {
		
	}


}
