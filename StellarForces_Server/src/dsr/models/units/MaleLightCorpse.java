package dsr.models.units;

import java.io.IOException;

import dsr.AppletMain;

public class MaleLightCorpse extends AbstractCorpse {
	
    private static final float WIDTH = 0.6f;
    public static final float Y_OFF = 0.75f;
    private static final float SCALE = 0.022f;
    
	private static final long serialVersionUID = 1L;

	public MaleLightCorpse(AppletMain m, String name, float x, float z, String path, String model, String skin, int _frame) throws IOException {
		super(m, "MaleLightCorpse_" + name, x+0.5f, z+0.5f, Y_OFF, WIDTH, AppletMain.DATA_DIR + path, model, skin, SCALE, _frame);
		
	}


}
