package dsr.models;

import com.jme.bounding.BoundingBox;
import com.jme.math.Vector3f;
import com.jme.scene.shape.Cylinder;

import dsr.GameObject;

public class CollisionCylinder extends Cylinder implements ICollidable {
	
	private GameObject owner;
	
	private static final long serialVersionUID = 1L;

	public CollisionCylinder(String name, Vector3f centre, int axisSamples, int radialSamples, float radius, float height, boolean closed, GameObject _owner) {
		super(_owner.getName() + "_collider", axisSamples, radialSamples, radius, height, closed);
		
		this.setLocalTranslation(centre);// Needs this so it tracks the owner!
		
		// Note we don't make this invisible as some objects use it for their model (e.g. crate).
		this.setModelBound(new BoundingBox());
		this.updateModelBound();
		this.setIsCollidable(true);

		owner = _owner;
	}
	
	public GameObject getOwner() {
		return this.owner;
	}

}
