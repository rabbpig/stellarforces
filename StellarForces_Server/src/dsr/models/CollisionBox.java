package dsr.models;

import com.jme.bounding.BoundingBox;
import com.jme.math.Vector3f;
import com.jme.scene.shape.Box;

import dsr.GameObject;

public final class CollisionBox extends Box implements ICollidable {
	
	private GameObject owner;
	
	private static final long serialVersionUID = 1L;

	public CollisionBox(Vector3f obj_centre, float x_extent, float y_extent, float z_extent, GameObject _owner) {
		super(_owner.getName() + "_collider", new Vector3f(0f, 0f, 0f), x_extent, y_extent, z_extent);
		
		this.setLocalTranslation(obj_centre);// Needs this so it tracks the owner!
		
		// Note we don't make this invisible as some objects use it for their model (e.g. crate).
		this.setModelBound(new BoundingBox());
		this.updateModelBound();
		this.setIsCollidable(true);
		
		owner = _owner;
	}
	
	public GameObject getOwner() {
		return this.owner;
	}

}
