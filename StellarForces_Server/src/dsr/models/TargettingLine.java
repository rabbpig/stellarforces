package dsr.models;

import java.nio.FloatBuffer;

import com.jme.math.Vector3f;
import com.jme.renderer.ColorRGBA;
import com.jme.scene.Line;
import com.jme.util.geom.BufferUtils;

import dsr.AppletMain;

public final class TargettingLine extends Line {
	
	private static final long serialVersionUID = 1L;

	private Vector3f[] vertex = new Vector3f[2];
	private ColorRGBA col = new ColorRGBA(1f, 1f, 0f, 1f);
	private Vector3f endpoint;
	
	public TargettingLine(AppletMain m) {
		super("TargettingLine");
		
		this.setIsCollidable(false);
		this.setDefaultColor(col);
		this.updateRenderState();
	}
	
	public void updateLine(Vector3f start, Vector3f end) {
		endpoint = end;
		
		vertex[0] = start;
		vertex[1] = end;

		FloatBuffer fb = BufferUtils.createFloatBuffer(vertex);

		this.reconstruct(fb, null, null, null);
		this.updateModelBound();
	}
	
	
	public Vector3f getEndpoint() {
		return endpoint;
	}

}
