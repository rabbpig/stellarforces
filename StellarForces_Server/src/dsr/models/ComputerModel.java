package dsr.models;

import ssmith.lang.Functions;

import com.jme.image.Texture;
import com.jme.math.Vector3f;
import com.jme.scene.state.CullState;
import com.jme.scene.state.TextureState;
import com.jme.system.DisplaySystem;
import com.jme.util.TextureManager;

import dsr.AppletMain;
import dsr.GameObject;
import dsr.data.AppletMapSquare;

public class ComputerModel extends GameObject {

	private static final float RAD = 0.4f;

	private static final long serialVersionUID = 1L;

	public static CullState cs;
	private static TextureState ts[];
	private static TextureState ts_damaged;
	private AppletMapSquare sq;

	public ComputerModel(AppletMain main, AppletMapSquare _sq) {
		super(main, "Computer", true, true, true, _sq.x+0.5f, _sq.y+0.5f, 0);
		
		sq = _sq;
		sq.cpu = this;

		this.collider =  new CollisionBox(new Vector3f(0, RAD, 0), RAD, RAD, RAD, this);
		if (ts == null) {
			ts = new TextureState[2];
			ts[0] = main.getDisplay().getRenderer().createTextureState();  
			ts[0].setEnabled(true);  
			ts[0].setTexture(TextureManager.loadTexture(AppletMain.DATA_DIR + "textures/ComputerConsole1.jpg", Texture.MinificationFilter.NearestNeighborLinearMipMap, Texture.MagnificationFilter.NearestNeighbor), 0);

			ts[1] = main.getDisplay().getRenderer().createTextureState();  
			ts[1].setEnabled(true);  
			ts[1].setTexture(TextureManager.loadTexture(AppletMain.DATA_DIR + "textures/ComputerConsole2.jpg", Texture.MinificationFilter.NearestNeighborLinearMipMap, Texture.MagnificationFilter.NearestNeighbor), 0);

			ts_damaged = main.getDisplay().getRenderer().createTextureState();  
			ts_damaged.setEnabled(true);  
			ts_damaged.setTexture(TextureManager.loadTexture(AppletMain.DATA_DIR + "textures/damaged_computer.jpg", Texture.MinificationFilter.NearestNeighborLinearMipMap, Texture.MagnificationFilter.NearestNeighbor), 0);

		}
		if (sq.destroyed == 0) {
			collider.setRenderState(ts[Functions.rnd(0, 1)]);
		} else {
			this.showDamage();
		}

		if (cs == null) {
			cs = DisplaySystem.getDisplaySystem().getRenderer().createCullState();
			cs.setEnabled(true);
			cs.setCullFace(CullState.Face.Back);
		}
		collider.setRenderState(cs);

		collider.updateRenderState();
		this.attachChild(collider);

		this.updateModelBound();

			sq.threeds.add(this);
	}
	
	
	public AppletMapSquare getMapSquare() {
		return sq;
	}

	
	public void showDamage() {
		collider.setRenderState(ts_damaged);
		this.updateRenderState();
	}

	

}
