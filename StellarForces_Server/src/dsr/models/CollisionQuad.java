package dsr.models;

import com.jme.bounding.BoundingBox;
import com.jme.scene.shape.Quad;

import dsr.GameObject;

public final class CollisionQuad extends Quad implements ICollidable {

	private static final long serialVersionUID = 1L;

	private GameObject owner;
	
	public CollisionQuad(float w, float h, GameObject _owner) {
		super((_owner.getName() + "_Cldr").intern(), w, h);
		owner = _owner;
		
		this.setModelBound(new BoundingBox());
		this.updateModelBound();
		this.setIsCollidable(true);
	}
	
	
	public GameObject getOwner() {
		return this.owner;
	}

}
