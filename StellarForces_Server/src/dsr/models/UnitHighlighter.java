package dsr.models;

import com.jme.math.FastMath;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.scene.shape.Disk;
import com.jme.scene.state.CullState;
import com.jme.system.DisplaySystem;

import dsr.AppletMain;
import dsr.GameObject;

public class UnitHighlighter extends GameObject {

	private static final long serialVersionUID = 1L;

	private static Quaternion q = new Quaternion();
	private static CullState cs_floor;

	static {
		q.fromAngleAxis(FastMath.PI/2, new Vector3f(-1, 0.5f, 0)); // too far
	}

	public UnitHighlighter(AppletMain main) {
		super(main, "UnitHighlighter", false, false, false, 0, 0, 0); // Notice that WE are the collider!

		Disk disc = new Disk("UnitHighlighter", 3, 3, .4f);
		disc.setLocalRotation(q);
		//disc.setLocalTranslation(0.0f, 0f, 0f);
		disc.setRenderState(main.getTextureState(AppletMain.DATA_DIR + "textures/game/unit_highlighter.png"));

		if (cs_floor == null) {
			cs_floor = DisplaySystem.getDisplaySystem().getRenderer().createCullState();
			cs_floor.setEnabled(true);
			cs_floor.setCullFace(CullState.Face.Back);
		}
		disc.setRenderState(cs_floor);

		disc.updateRenderState();
		this.attachChild(disc);

		this.updateModelBound();

	}


}
