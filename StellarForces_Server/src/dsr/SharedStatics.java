package dsr;

public class SharedStatics {

	// Debugging/testing
	public static final boolean DEBUG = false;

	public static final String URL_FOR_CLIENT = "http://www.stellarforces.com"; // Do NOT add a slash to the end
	//public static final String URL_FOR_CLIENT = "http://127.0.0.1:8083"; // Do NOT add a slash to the end

	public static final float VERSION = 1.87f; // Post to forum when releasing a new client
	public static final float MAX_UPGRADE_VERSION = 1.82f;
	public static final float MIN_CLIENT_VERSION = 1.81f;

	public static final boolean VERBOSE_COMMS = false;
	public static final String TITLE = "Stellar Forces";
	public static final String WEBSITE_LINK = "http://www.stellarforces.com"; // Do NOT add a slash to the end
	//public static final String EMAIL_ADDRESS = "StellarForces@stellarforces.com";
	//public static final String COMMENTS_EMAIL_ADDRESS = "comments@stellarforces.com";
	//public static final String SUPPORT_EMAIL_ADDRESS = "support@stellarforces.com";

}
