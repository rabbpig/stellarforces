package dsr.hud;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

import com.jme.scene.Spatial.CullHint;

import dsr.AppletMain;
import dsr.TextureStateCache;
import dsr.data.AppletMapSquare;
import dsr.data.UnitData;
import dsr.gui.AbstractComponent;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitsTable;

public class StrategicScanner extends AbstractComponent {

	private static final Color background_col = new Color(0f, 0.6f, 0f, 0.4f);
	private static Color blue = new Color(0f, 0f, 1f, 1f);
	private static Color green = new Color(0f, 1f, 0f, 1f);
	private static Color yellow = new Color(1f, 1f, 0f, 1f);
	private static Color red = new Color(1f, 0f, 0f, 1f);
	private static Color black = new Color(1f, 0f, 0f, 0f);
	private static Color white = new Color(1f, 1f, 1f, 1f);

	private static final long serialVersionUID = 1L;

	private int inset, sq_size = 1; // Default to 1.

	public StrategicScanner(AppletMain m, HUD _hud) {
		super(m, _hud, 0, 0, 1, 1, true, _hud);

		inset = _hud.getImageHeight()/20;
		this.setSize(_hud.getImageHeight() - (inset*2), _hud.getImageHeight() - (inset*2));
		this.x = (_hud.getImageHeight() - this.height)/2;
		this.y = (_hud.getImageHeight() - this.height)/2;
	}


	@Override
	public boolean mouseClicked(Point p) {
		this.hud.remove(this);
		return true;
	}


	@Override
	public void paint(Graphics2D g) {
		g.translate(x, y);

		g.setBackground(background_col);
		g.clearRect(0, 0, width, height);

		g.drawRect(0, 0, this.width, this.height);

		int pixels = this.width;
		
		// Draw the walls
		sq_size = pixels / main.mapdata.getMapHeight();
		for (int y2 = 0 ; y2<main.mapdata.getMapHeight() ; y2++) {
			for (int x2 = 0 ; x2<main.mapdata.getMapWidth() ; x2++) {
				AppletMapSquare sq = main.mapdata.getSq_MaybeNULL(x2, y2); 
				if (sq.major_type == MapDataTable.MT_COMPUTER) {
					g.setColor(blue);
					//g.fillRect(x2*sq_size, y2 * sq_size, sq_size, sq_size);
				} else if (sq.major_type == MapDataTable.MT_FLOOR && sq.texture_code == TextureStateCache.TEX_WATER) {
					g.setColor(blue);
					//g.fillRect(x2*sq_size, y2 * sq_size, sq_size, sq_size);
				} else if (sq.major_type == MapDataTable.MT_WALL) {
					g.setColor(green);
					//g.fillRect(x2*sq_size, y2 * sq_size, sq_size, sq_size);
				} else if (sq.escape_hatch_side > 0) {
					g.setColor(white);
					//g.fillRect(x2*sq_size, y2 * sq_size, sq_size, sq_size);
				} else if (sq.major_type == MapDataTable.MT_NOTHING) {
					g.setColor(black);
					//g.fillRect(x2*sq_size, y2 * sq_size, sq_size, sq_size);
				} else {
					g.setColor(black);
				}
				g.fillRect(x2*sq_size, y2 * sq_size, sq_size, sq_size);
			}
		}

		// Draw our units
		for (int i=0 ; i<main.units.length  ; i++) {
			UnitData unit = main.units[i];
			if (unit.getStatus() == UnitsTable.ST_DEPLOYED) {
				if (main.game_data.areSidesFriends(unit.getSide(), main.game_data.our_side)) {
					if (unit == main.getCurrentUnit()) {
						g.setColor(white);
					} else {
						if (unit.getSide() == main.game_data.our_side) {
							g.setColor(yellow);
						} else {
							g.setColor(blue);
						}
					}
					g.fillOval(unit.map_x*sq_size, unit.map_z * sq_size, sq_size, sq_size);
				} else {
					if (unit.model != null) {
						if (unit.model.getCullHint() == CullHint.Never) {
							g.setColor(red);
							g.fillOval(unit.map_x*sq_size, unit.map_z * sq_size, sq_size, sq_size);
						}
					}
				}
			}
		}

		g.translate(-x, -y);

	}
	
	
	public Point getMapCoords(Point mouse_coords) {
		Point p = new Point(0, 0);
		p.x = (mouse_coords.x - this.x) / sq_size;
		p.y = (mouse_coords.y - this.y) / sq_size;
		return p;
	}


}
