package dsr.hud;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

import dsr.AppletMain;
import dsr.gui.AbstractComponent;

public final class TextHUD extends AbstractComponent {

	private static boolean INCREMENT_CHAR = false;
	private static final int TEXT_Y_OFFSET = 19;
	private static final int MAX_LINES = 13;//Need at least 8!
	private static final Color background_col = new Color(0f, .6f, 0f, HUD.TRANSPARENCY);
	private static int FONT_HEIGHT_CALC = -1;
	
	private static final long serialVersionUID = 1L;

	private ArrayList<PriorityText> al_strings = new ArrayList<PriorityText>();
	private ArrayList<PriorityText> al_waiting_strings = new ArrayList<PriorityText>();
	private int char_pos = 0;

	
	public TextHUD(AppletMain m, HUD hud) {
		super(m, hud, 0, hud.getImageHeight()/2, (int)(hud.getImageWidth() * 0.75f), hud.getImageHeight()/2, false, hud);
		
		//font = HUD.DEF_FONT_NORMAL;// AppletMain.base_font.deriveFont(AppletMain.font_style, hud.getImageWidth()/20);
	}

	
	public void paint(Graphics2D g) {
		g.setBackground(background_col);
		g.clearRect(x, y, width-1, height-1);

		if (al_strings.size() > 0) {
			g.setFont(HUD.DEF_FONT_NORMAL);
			if (FONT_HEIGHT_CALC <= 0) {
				FontMetrics fm = g.getFontMetrics();
				FONT_HEIGHT_CALC = fm.getHeight();
			}
			for (int i=0 ; i<al_strings.size() ; i++) {
				PriorityText pt = al_strings.get(i);
				if (pt.is_pri) {
					g.setColor(Color.red);
				} else {
					g.setColor(Color.white);
				}

				if (i < al_strings.size()-1) { // Not the last line, so just write it
					g.drawString(pt.text, x+10, y+TEXT_Y_OFFSET + (i*(FONT_HEIGHT_CALC)));
				} else { // The last line
					String s = pt.text;
					if (char_pos >= s.length() || INCREMENT_CHAR == false) { // Draw the whole string then
						g.drawString(s, x+10, y+TEXT_Y_OFFSET + (i*(FONT_HEIGHT_CALC)));
						this.addWaitingString();
					} else { // Draw part of the string
						g.drawString(s.substring(0, char_pos), x+10, y+TEXT_Y_OFFSET + (i*(FONT_HEIGHT_CALC)));
						char_pos += 3;
						this.hud.markForRefresh();
					}
				}
			}
		} else {
			addWaitingString();
		}
	}

	
	private void addWaitingString() {
		// Do we need to add another string?
		if (this.al_waiting_strings.size() > 0) {
			PriorityText s2 = this.al_waiting_strings.remove(0);
			this.al_strings.add(s2);
			// Remove earlier strings if we're showing too many
			if (this.al_strings.size() > MAX_LINES) {
				this.al_strings.remove(0);
			}
			char_pos = 0;
			this.hud.markForRefresh();
		}
		
	}
	
	
	public synchronized void add(String s, boolean pri) {
		this.al_waiting_strings.add(new PriorityText(s, pri));
		hud.markForRefresh();
	}
	
	
	public void clear() {
		this.al_strings.clear();
		this.al_waiting_strings.clear();
		this.char_pos = 0;
		hud.markForRefresh();
	}


	@Override
	public boolean mouseClicked(Point p) {
		return false;
	}

}

