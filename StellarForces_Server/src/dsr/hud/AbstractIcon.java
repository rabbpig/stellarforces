package dsr.hud;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

import dsr.AppletMain;
import dsr.gui.AbstractComponent;

public abstract class AbstractIcon extends AbstractComponent {

	// Cols
	public static Color green = new Color(0f, 1f, 0f, HUD.TRANSPARENCY);
	public static Color yellow = new Color(1f, 1f, 0f, HUD.TRANSPARENCY);
	public static Color blue = new Color(.2f, .2f, 1f, HUD.TRANSPARENCY);
	public static Color orange = new Color(1f, .6f, .1f, HUD.TRANSPARENCY);
	public static Color red = new Color(1f, 0f, 0f, HUD.TRANSPARENCY);

	private static final long serialVersionUID = 1L;

	protected AppletMain main;
	public boolean toggleable, selected = false;
	protected Color col_norm, col_selected, col_darker, col_brighter;
	protected String text;
	protected byte ap_cost = 0;

	
	public AbstractIcon(AppletMain m, HUD hud, boolean toggle, String _text, Color col) {
		super(m, hud, -1, -1, -1, -1, true, null); // Gets set by the IconHUD

		main = m;
		this.toggleable = toggle;
		text = _text;

		if (col != null) {
			col_norm = col;
		} else {
			col_norm = new Color(1f, 0f, 0f, HUD.TRANSPARENCY);
		}
		col_selected = col_norm.brighter();
		this.calcShades();
	}
	
	
	protected void calcShades() {
		col_darker = col_norm.darker();
		col_brighter = col_norm.brighter();
		
	}
	

	public void paint(Graphics2D g) {
		//g.setColor(Color.gray);
		//g.fillRect(x, y, width-2, height-2);

		g.setBackground(col_norm);
		g.clearRect(x, y, width, height);
		if (this.toggleable && this.selected) {
			g.setBackground(col_selected);
			g.clearRect(x, y, width, height);
		}

		// Draw border
		g.setColor(col_brighter);
		g.drawLine(x, y, x+width, y);
		g.drawLine(x, y, x, y+height);
		g.setColor(col_darker);
		g.drawLine(x, y+height, x+width, y+height);
		g.drawLine(x+width, y, x+width, y+height);

		g.setColor(Color.white);
		if (this.ap_cost == 0) {
			g.drawString(text, x+4, y+14);
		} else {
			g.drawString(text + "  (" +this.ap_cost + ")", x+4, y+14);
		}

	}
	
	
	public void setAPCost(byte aps) {
		this.ap_cost = aps;
	}
	

	public boolean mouseClicked(Point p) {
		if (this.toggleable) {
			this.selected = !this.selected;
		}
		main.refreshHUD();
		return true;
	}

	
	public void adjCount(byte amt) {
		// Override if req
	}
	
	
	public boolean equals(Object o) {
		return this == o;
	}
	
	
	public void setText(String s) {
		this.text = s;
	}

}
