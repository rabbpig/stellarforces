package dsr.hud;

import java.awt.Color;

import dsr.AppletMain;

public final class ShotTypeIcon extends CommandIcon {
	
	private static final long serialVersionUID = 1L;

	private String orig_text;
	
	public ShotTypeIcon(AppletMain m, String text, byte _cmd, Color col) {
		super(m, text, _cmd, col);
		
		this.orig_text = text;
	}
	
	public void setPcent(byte amt) {
		this.text = this.orig_text + " " + amt + "%";
	}

}
