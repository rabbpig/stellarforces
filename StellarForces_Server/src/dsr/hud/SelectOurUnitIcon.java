package dsr.hud;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

import dsr.AppletMain;
import dsr.data.UnitData;

public final class SelectOurUnitIcon extends AbstractIcon {
	
	private static final long serialVersionUID = 1L;
	
	private static final Color selected_col = new Color(1f, 1f, 0f, .8f);
	private static final Color unselected_col = new Color(1f, 1f, 0f, 0.3f);

	private UnitData unit;
	
	public SelectOurUnitIcon(AppletMain m, UnitData _unit) {
		super(m, null, false, _unit.name, null);
		
		unit =_unit;
		unit.icon = this;
	}

	public void paint(Graphics2D g) {
		this.text = unit.name + " (" + unit.getAPs() + ")";//unit.order_by + " " + 
		if (unit == main.getCurrentUnit()) {
			col_norm = selected_col;
		} else {
			col_norm = unselected_col;
		}
		this.calcShades();
		super.paint(g);
		
		/*if (AppletMain.DEBUG) {
			AppletMain.p("Drawing " + unit.name + " at " + this.toString());
		}*/
	}
	
	
	@Override
	public boolean mouseClicked(Point p) {
		main.selectOurUnit(unit);
		return true;
	}

}
