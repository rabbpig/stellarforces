package dsr.hud;

import java.awt.Color;
import java.awt.Point;

import dsr.AppletMain;
import dsr.data.GameData;
import dsr.gui.Button;

public class SelectGameButton extends Button {

	private static final long serialVersionUID = 1L;
	
	public GameData game;
	private static Color background_col = new Color(0f, 1f, 0f, .7f);

	public SelectGameButton(GameData _game, AppletMain m, HUD _hud, IContainer add_to) {
		super("", m, _hud, 0, 0, 0, 0, add_to, background_col);

		game = _game;

		this.setText(game.getFullTitle());
		if (game.enabled == false) {
			// btn.setEnabled(false);
			this.setText(game.mission_name + " - UPGRADE CLIENT TO PLAY!");
		}
	}


	@Override
	public boolean mouseClicked(Point p) {
		try {
			main.selectGame(game);
		} catch (Exception ex) {
			AppletMain.HandleError(main, ex);
		}
		return true;
	}

}
