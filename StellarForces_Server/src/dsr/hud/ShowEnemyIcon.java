package dsr.hud;

import java.awt.Color;
import java.awt.Point;

import dsr.AppletMain;
import dsr.data.UnitData;
import dsr.start.StartupNew;

public final class ShowEnemyIcon extends AbstractIcon {
	
	private static final long serialVersionUID = 1L;

	private UnitData unit;
	
	public ShowEnemyIcon(AppletMain m, UnitData _unit) {
		super(m, null, false, StartupNew.strings.getTranslation("EN ") + _unit.getSide() + ": " + _unit.name, Color.red);
		
		unit =_unit;
		_unit.icon = this;

	}

	@Override
	public boolean mouseClicked(Point p) {
		main.lookAtEnemyUnit(unit);
		return true;
	}
	
	
	public String toString() {
		return "ShowEnemyIcon: " + unit.name;
	}

}
