package dsr.hud;

import java.awt.Point;

import dsr.AppletMain;

public final class LessIcon extends AbstractIcon {
	
	public AbstractIcon action;
	
	private static final long serialVersionUID = 1L;

	public LessIcon(AppletMain m) {
		super(m, null, false, "-", AbstractIcon.yellow);
	}
	
	public boolean mouseClicked(Point p) {
		action.adjCount((byte)-1);
		return true;
	}

}
