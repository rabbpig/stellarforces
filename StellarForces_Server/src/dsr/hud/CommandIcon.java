package dsr.hud;

import java.awt.Color;
import java.awt.Point;

import dsr.AppletMain;
import dsr.data.OppFireTypes;

public class CommandIcon extends AbstractIcon {
	
	public static final byte CHOOSE_GAME = 1;
	public static final byte SCANNER = 2;
	public static final byte CANCEL = 3;
	public static final byte PICKUP_ITEM = 4;
	public static final byte CHANGE_ITEM = 5;
	public static final byte DROP_ITEM = 6;
	public static final byte SELECT_SHOT_TYPE = 7;
	public static final byte END_TURN = 10;
	public static final byte AIMED_SHOT= 11;
	public static final byte SNAP_SHOT = 12;
	public static final byte AUTO_SHOT = 13;
	public static final byte RELOAD = 14;
	public static final byte SHOW_PRIME_MENU = 15;
	public static final byte SHOW_THROW_MENU = 16;
	public static final byte CONFIRM_END_TURN = 17;
	public static final byte USE_MEDI_KIT = 18;
	public static final byte NEXT_UNIT = 19;
	public static final byte PREV_UNIT = 20;
	public static final byte WARNING = 21;
	public static final byte USE_PORTA_PORTER = 22;
	public static final byte ACTIVATE = 23;
	public static final byte USE_ADRENALIN_SHOT = 24;
	public static final byte OPEN_DOOR = 25;
	public static final byte TUTORIAL = 26;
	public static final byte REMOVE_ITEM = 27;
	public static final byte AUTO_AIM = 28;
	public static final byte ESCAPE = 29;
	public static final byte CLOSE_DOOR = 30;
	public static final byte OPP_FIRE_AIMED = 31;
	public static final byte OPP_FIRE_SNAP = 32;
	public static final byte OPP_FIRE_AUTO = 33;
	public static final byte ABSORB = 34;
	public static final byte EXPLODE = 35;
	public static final byte SPLIT = 36;
	public static final byte USE_SCANNER = 37;

	private static final long serialVersionUID = 1L;

	public byte cmd;
	
	public CommandIcon(AppletMain m, String text, byte _cmd, Color col) {
		super(m, null, false, text, col);
		
		cmd = _cmd;
	}
	
	
	public String toString() {
		return "CommandIcon:" + cmd;
	}
	
	
	public boolean mouseClicked(Point p) {
		switch (cmd) {
		case CHOOSE_GAME:
			main.showGameSelector();
			break;
		case SCANNER:
			main.toggleScanner();
			break;
		case CANCEL:
			main.cancelMenu();
			break;
		case PICKUP_ITEM:
			main.showPickupItemMenu();
			break;
		case CHANGE_ITEM:
			main.showChangeUnitsItemMenu();
			break;
		case DROP_ITEM:
			main.dropCurrentItem();
			break;
		case SELECT_SHOT_TYPE:
			main.showSelectShotTypeMenu();
			break;
		case AIMED_SHOT:
			main.makeAimedShot();
			break;
		case SNAP_SHOT:
			main.makeSnapShot();
			break;
		case AUTO_SHOT:
			main.makeAutoShot();
			break;
		case RELOAD:
			main.reload();
			break;
		case SHOW_PRIME_MENU:
			main.showPrimeMenu();
			break;
		case SHOW_THROW_MENU:
			main.showThrowMenu();
			break;
		case END_TURN:
			main.showConfirmEndTurnMenu();
			break;
		case CONFIRM_END_TURN:
			main.showExplosionsAndEndTurn();
			break;
		case USE_MEDI_KIT:
			main.useMedikit();
			break;
		case NEXT_UNIT:
			main.selectNextUnit();
			break;
		case PREV_UNIT:
			main.selectPrevUnit();
			break;
		case WARNING:
			// Do nothing
			break;
		case USE_PORTA_PORTER:
			//main.usePortaPorter();
			break;
		case ACTIVATE:
			main.activateEquipment();
			break;
		case USE_ADRENALIN_SHOT:
			//main.useAdrenalinShot();
			break;
		case OPEN_DOOR:
			main.openDoor();
			break;
		/*case WAIT:
			main.makeUnitWait();
			break;*/
		case REMOVE_ITEM:
			main.removeCurrentItem();
			break;
		case AUTO_AIM:
			main.autoAim();
			break;
		case ESCAPE:
			main.escape();
			break;
		case CLOSE_DOOR:
			main.closeDoor();
			break;
		case OPP_FIRE_AIMED:
			main.selectOppFire(OppFireTypes.AIMED_SHOT);
			break;
		case OPP_FIRE_SNAP:
			main.selectOppFire(OppFireTypes.SNAP_SHOT);
			break;
		case OPP_FIRE_AUTO:
			main.selectOppFire(OppFireTypes.AUTO_SHOT);
			break;
		case ABSORB:
			main.blobAbsorb();
			break;
		case SPLIT:
			main.splitBlob();
			break;
		case EXPLODE:
			main.blobAttemptExplode();
			break;
		case TUTORIAL:
			main.tutorial();
			break;
		case USE_SCANNER:
			main.useScanner();
			break;
		default:
			throw new RuntimeException("Unknown command: " + cmd);
		}
		return true;
	}

}
