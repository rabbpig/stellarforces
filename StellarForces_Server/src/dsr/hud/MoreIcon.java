package dsr.hud;

import java.awt.Point;

import dsr.AppletMain;

public final class MoreIcon extends AbstractIcon {
	
	public AbstractIcon action;
	
	private static final long serialVersionUID = 1L;

	public MoreIcon(AppletMain m) {
		super(m, null, false, "+", AbstractIcon.yellow);
	}
	
	public boolean mouseClicked(Point p) {
		action.adjCount((byte)1);
		return true;
	}

}
