package dsr.hud;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

import com.jme.image.Texture;
import com.jme.image.Texture2D;
import com.jme.math.FastMath;
import com.jme.math.Quaternion;
import com.jme.math.Vector3f;
import com.jme.renderer.Renderer;
import com.jme.scene.Spatial;
import com.jme.scene.shape.Quad;
import com.jme.scene.state.BlendState;
import com.jme.scene.state.TextureState;

import dsr.AppletMain;
import dsr.PaintableImage;
import dsr.SharedStatics;
import dsr.gui.AbstractComponent;
import dsr.start.StartupNew;

public final class HUD extends Quad implements IPaintable, IContainer {

	public static final float TRANSPARENCY = 0.15f; 

	private static final long serialVersionUID = 1L;
	private static final Color background_col = new Color(0f, 0f, 0f, 0.0f);

	private AppletMain main;
	private PaintableImage img;
	private TextureState ts;
	private ArrayList<AbstractComponent> components = new ArrayList<AbstractComponent>();
	private boolean refresh_flag = true;
	private TextHUD text_hud;
	public static Font DEF_FONT_NORMAL, DEF_FONT_BIGGER;


	public HUD(AppletMain m) {
		//super("HUD", GetTexSize(m.getDisplay().getWidth()), GetTexSize(m.getDisplay().getWidth()));
		super("HUD", m.getDisplay().getWidth(), m.getDisplay().getHeight());  // SCS

		main = m;

		this.setCullHint(Spatial.CullHint.Never);
		this.setIsCollidable(false);
		this.updateModelBound();
		this.setLocalTranslation(this.width/2, this.height/2, 0);

		Quaternion q = new Quaternion();
		q.fromAngleAxis(FastMath.PI, new Vector3f(1,0,0));
		this.setLocalRotation(q);

		ts = m.getDisplay().getRenderer().createTextureState();  
		ts.setEnabled(true);
		Texture t = new Texture2D();
		//img = new PaintableImage((int)this.width, (int)this.height, this);
		img = new PaintableImage(GetTexSize((int)this.width), GetTexSize((int)this.width), this);
		t.setImage(img);

		ts.setTexture(t);
		this.setRenderState(ts);

		BlendState tempBlendState = main.getDisplay().getRenderer().createBlendState();
		tempBlendState.setBlendEnabled(true);
		tempBlendState.setSourceFunction(BlendState.SourceFunction.SourceAlpha);
		tempBlendState.setDestinationFunction(BlendState.DestinationFunction.OneMinusSourceAlpha);
		tempBlendState.setTestEnabled(false); //SCS was true
		tempBlendState.setTestFunction(BlendState.TestFunction.Always);
		tempBlendState.setEnabled(true);
		this.setRenderState(tempBlendState);

		//this.setLightCombineMode(LightCombineMode.Off);

		this.updateRenderState();

		new StatsHUD(main, this); // Create stats HUD
		text_hud = new TextHUD(main, this);
		text_hud.add(StartupNew.strings.getTranslation("Welcome to ") + SharedStatics.TITLE, false);

		//DEF_FONT_NORMAL = AppletMain.base_font.deriveFont(AppletMain.font_style, this.getImageWidth()/40);
		//DEF_FONT_BIGGER = AppletMain.base_font.deriveFont(AppletMain.font_style, this.getImageWidth()/30);
		DEF_FONT_NORMAL = AppletMain.base_font.deriveFont(AppletMain.font_style, 10);
		DEF_FONT_BIGGER = AppletMain.base_font.deriveFont(AppletMain.font_style, 14);
		
	}


	public void onDraw(Renderer r) { // Use onDraw instead of Draw since we want to update even if it's not in the camera's view
		if (this.refresh_flag) {
			this.refreshImage();
		}
		super.onDraw(r);
	}


	public void paint(Graphics2D g, PaintableImage ti) {
		this.updateRenderState();

		g.setBackground(background_col);
		g.clearRect(0, 0, ti.getWidth(), ti.getHeight());

		for (int i=0 ; i<components.size() ; i++) {
			AbstractComponent control = components.get(i);
			control.paint(g);
		}

	}

	private void refreshImage() {
		try {
			this.refresh_flag = false; // Clear flag first in case we set it while painting
			this.img.refreshImage(ts); // FSR this errors sometimes
		} catch (Exception ex) {
			AppletMain.HandleError(main, ex);//  Too many errors?
		}

	}


	public void add(AbstractComponent c) {
		this.components.add(c);
		this.markForRefresh();
	}


	@Override
	public void remove(AbstractComponent icon) {
		this.components.remove(icon);
		this.markForRefresh();
	}


	public void addComponent(AbstractComponent c) {
		this.components.add(c);
	}


	public boolean componentSelected(Point p) {
		for (int i=components.size()-1 ; i>=0 ; i--) { // Go through in reverse order!
			AbstractComponent comp = components.get(i);
			if (comp.clickable) {
				if (comp.contains(p)) {
					return comp.mouseClicked(p);
				}
			}
		}
		return false;
	}


	/**
	 * Flag this as needing to repaint itself.
	 */
	public void markForRefresh() {
		this.refresh_flag = true;
	}


	public void addText(String s, boolean pri) {
		int MAX_LENGTH = 50;
		if (text_hud != null) {
			String re_add = "";
			// If it's too long, split it up
			if (s.length() > MAX_LENGTH) {
				int cutoff = s.substring(0, MAX_LENGTH).lastIndexOf(" ");
				if (cutoff <= 0) {
					cutoff = MAX_LENGTH;
				}
				re_add = s.substring(cutoff+1);
				s = s.substring(0, cutoff);
			}
			this.text_hud.add(s, pri);
			markForRefresh();
			if (re_add.length() > 0) {
				addText(re_add, pri);
			}
		}

	}


	public void clearText() {
		this.text_hud.clear();
		markForRefresh();
	}


	private static int GetTexSize(int h) {
		return 512;
		/*int sizes[] = {512, 1024, 2048, 4096};
		for (int i=0 ; i<sizes.length ; i++) {
			if (h < sizes[i]) {
				return sizes[i];
			}
		}
		return sizes[sizes.length-1];*/
	}
	
	
	public int getImageWidth() {
		return img.getWidth();
	}
	

	public int getImageHeight() {
		return img.getHeight();
	}

}

