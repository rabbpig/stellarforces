package dsr.hud;

import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

import dsr.AppletMain;
import dsr.SoundEffects;
import dsr.gui.AbstractComponent;

public final class IconHUD extends AbstractComponent implements IContainer {

	public static int ICON_WIDTH;// = 190;
	public static int ICON_HEIGHT;// = 22;

	private static final long serialVersionUID = 1L;

	private ArrayList<AbstractComponent> icons = new ArrayList<AbstractComponent>();
	private int cols;
	
	public IconHUD(AppletMain m, HUD hud, int _cols, IContainer add_to) {
		super(m, hud, 0, hud.getImageHeight()/10, (hud.getImageWidth()/4)*_cols, -1, true, add_to);

		cols = _cols;
		
		ICON_WIDTH = 128;//(hud.getImageWidth()/4);
		ICON_HEIGHT = 20;//hud.getImageHeight()/20;
	}


	@Override
	public boolean mouseClicked(Point p) {
		for (int i=0 ; i<icons.size() ; i++) {
			AbstractComponent icon = icons.get(i);
			if (icon.contains(p)) {
				main.playSound(SoundEffects.CLICK);
				icon.mouseClicked(p);
				return true;
			}
		}
		return false;
	}


	public void paint(Graphics2D g) {
		g.setFont(HUD.DEF_FONT_NORMAL);
		for (int i=0 ; i<icons.size() ; i++) {
			AbstractComponent icon = icons.get(i);
			icon.paint(g);
		}
	}


	public void clear() {
		icons.clear();
	}


	public void add(AbstractComponent icon) {
		if (this.icons.size() % cols == 0) {
			icon.x = this.x;
		} else {
			icon.x = this.x + ICON_WIDTH + 2;

		}
		icon.y = this.y + ((icons.size()/cols) * (ICON_HEIGHT+2)); // Must be before we add the icon to the ArrayList
		icon.width = ICON_WIDTH;
		icon.height = ICON_HEIGHT;

		this.icons.add(icon);

		// Update our own height
		this.height = icons.size() * ICON_HEIGHT;

		this.hud.markForRefresh();
	}


	public void addAll(ArrayList<AbstractIcon> a) {
		for (int i=0 ; i<a.size() ; i++) {
			AbstractComponent icon = a.get(i);
			this.add(icon);
		}
	}


	public void removeAll(ArrayList<AbstractIcon> a) {
		for (int i=0 ; i<a.size() ; i++) {
			this.icons.remove(a.get(i));
		}
	}


	@Override
	public void remove(AbstractComponent icon) {
		this.icons.remove(icon);
	}

}
