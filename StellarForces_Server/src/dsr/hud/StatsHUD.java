package dsr.hud;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

import dsr.AppletMain;
import dsr.data.EquipmentData;
import dsr.data.UnitData;
import dsr.gui.AbstractComponent;
import dsr.start.StartupNew;
import dsrwebserver.tables.GamesTable;

/**
 * This is the bit of text at the very top-left.
 *
 */
public final class StatsHUD extends AbstractComponent {

	//private static Font font_normal;
	private Color background_col = new Color(1f, 0f, 1f, HUD.TRANSPARENCY);

	private static final long serialVersionUID = 1L;

	private StringBuffer str_name, str_equip;

	public StatsHUD(AppletMain m, HUD hud) {
		super(m, hud, 0, 0, hud.getImageWidth(), hud.getImageHeight()/10, false, hud); // Was 612

		//font_normal = HUD.DEF_FONT_BIGGER;// AppletMain.base_font.deriveFont(AppletMain.font_style, hud.getImageWidth()/16);

		str_name = new StringBuffer();
		str_equip = new StringBuffer();
	}


	public void paint(Graphics2D g) {
		try {
			UnitData current_unit = main.getCurrentUnit();

			str_name.setLength(0);
			str_equip.setLength(0);
			if (main.game_data != null) {
				if (main.game_data.game_status == GamesTable.GS_CREATED_DEPLOYMENT) {
					if (main.next_to_deploy >= 0) {
						str_name.append(StartupNew.strings.getTranslation("Deploying Unit") + " " + main.units[main.next_to_deploy].order_by + ": " + main.units[main.next_to_deploy].name);
					} else {
						str_name.append(StartupNew.strings.getTranslation("Deployment Finished"));
					}
				} else if (main.game_data.game_status == GamesTable.GS_STARTED) {
					if (current_unit != null) {
						str_name.append(current_unit.order_by + " " + current_unit.name + "  - APs: " + current_unit.getAPs() + " (" + current_unit.opp_fire_aps_req + ") - HP: " + main.getCurrentUnit().getHealth() + " (" + main.getCurrentUnit().max_health + ")  ");
						str_equip.append("Prot:" + current_unit.protection);
						if (current_unit.current_item != null) {
							EquipmentData eq = current_unit.current_item;
							str_equip.append(" - " + eq.getName(true) + " ");
						} else if (current_unit.can_use_equipment) {
							str_equip.append(" - * " + StartupNew.strings.getTranslation("Nothing used") + " *");
						}
					}
				}
			} else {
				str_name.append("SELECT GAME");

			}

			if (str_name.length() > 0 || str_equip.length() > 0) {
				g.setBackground(background_col);
				g.clearRect(x, y, width-1, height-1);

				g.setColor(Color.white);
				g.setFont(HUD.DEF_FONT_BIGGER);

				// Change colour if unit injured
				if (main.game_data != null) {
					if (main.game_data.game_status == GamesTable.GS_STARTED) {
						if (current_unit != null) {
							if (current_unit.getHealth() < current_unit.max_health/2) {
								g.setColor(Color.red);
							} else if (current_unit.getHealth() < current_unit.max_health) {
								g.setColor(Color.orange);
							} else {
								// Keep white
							}
						}
					}
				}
				g.drawString(str_name.toString(), x+7, y+21);
				g.setColor(Color.white);
				g.drawString(str_equip.toString(), x+7, y+43);
			}

			if (main.isSendingData()) {
				g.setColor(Color.yellow);
				g.fillRect(0, 0, 10, 10);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	@Override
	public boolean mouseClicked(Point p) {
		return false;
	}

}

