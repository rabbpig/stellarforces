package dsr.hud;

import java.awt.Point;

import dsr.AppletMain;

public final class ThrowIcon extends AbstractIcon {
	
	private static final long serialVersionUID = 1L;

	private byte dist = 5;
	private byte max_dist = -1;

	public ThrowIcon(AppletMain m) {
		super(m, null, false, "", AbstractIcon.green);

	}

	public boolean mouseClicked(Point p) {
		main.throwItem(dist);
		return true;
	}

	
	public void adjCount(byte amt) {
		if (max_dist <= 0 || amt == 0) { // Then it needs updating
			max_dist = (byte)(main.getCurrentUnit().strength - main.getCurrentUnit().current_item.weight);
			if (max_dist <= 1) {
				max_dist = 1;
			}
		}
		
		dist += amt;
		if (dist < 1) {
			dist = 1;
		} else {
			if (main.getCurrentUnit() != null) {
				if (dist > max_dist) {
					dist = max_dist;
				}
			}
		}
		this.text = "Throw " + dist + " (max " + max_dist + ")";
		main.refreshHUD();
	}

}
