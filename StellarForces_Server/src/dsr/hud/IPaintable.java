package dsr.hud;

import java.awt.Graphics2D;

import dsr.PaintableImage;

public interface IPaintable {
	
	void paint(Graphics2D g, PaintableImage ti);

}
