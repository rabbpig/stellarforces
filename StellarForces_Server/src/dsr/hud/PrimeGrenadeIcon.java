package dsr.hud;

import java.awt.Point;

import dsr.AppletMain;

public final class PrimeGrenadeIcon extends AbstractIcon {
	
	private static final long serialVersionUID = 1L;

	private byte count = 0;
	
	public PrimeGrenadeIcon(AppletMain m) {
		super(m, null, false, "", AbstractIcon.green);
		
		this.adjCount((byte)5);
	}
	
	
	public boolean mouseClicked(Point p) {
		main.primeGrenade(count);
		return true;
	}
	

	public void adjCount(byte amt) {
		count += amt;
		if (count < 0) {
			count = 0;
		}
		this.text = "Prime for " + count + " turns";
		main.refreshHUD();
	}

}
