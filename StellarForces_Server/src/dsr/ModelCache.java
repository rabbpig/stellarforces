package dsr;

import java.util.Hashtable;

import com.jme.scene.Node;
import com.jme.util.CloneImportExport;
import com.jme.util.export.Savable;

public final class ModelCache extends Hashtable<String, CloneImportExport> {
	
	private static final long serialVersionUID = 1L;

	public ModelCache() {
		super(20);
	}
	
	
	public void putNode(String name, Node n) {
        CloneImportExport cloneEx = new com.jme.util.CloneImportExport();
        cloneEx.saveClone(n);
        this.put(name, cloneEx);
	}

	
    public Savable getNode(String def) {
        CloneImportExport cloneIn = (CloneImportExport) this.get(def);
        if (cloneIn != null) {
        	return cloneIn.loadClone();
        } else {
        	return null; // Not saved yet!
        }
    }
}
