package dsr.gui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

import dsr.AppletMain;
import dsr.hud.HUD;
import dsr.hud.IContainer;

public class Button extends AbstractTextComponent {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Color background_col, foreground_col;
	protected boolean enabled = false;

	public Button(String text, AppletMain m, HUD _hud, int x, int y, int w, int h, IContainer add_to, Color _background_col) {
		super(text, m, _hud, x, y, w, h, true, add_to);
		
		background_col = _background_col;
		foreground_col = new Color(background_col.getRed()/255, background_col.getGreen()/255, background_col.getBlue()/255, 1f); 
	}


	@Override
	public void paint(Graphics2D g) {
		g.setBackground(background_col);
		g.clearRect(x, y, width-1, height-1);

		g.setColor(foreground_col);
		g.drawRect(x, y, width-1, height-1);
		
		super.paint(g);
	}


	@Override
	public boolean mouseClicked(Point p) {
		return false;
	}

}
