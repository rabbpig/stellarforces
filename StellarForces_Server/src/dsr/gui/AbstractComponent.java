package dsr.gui;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

import dsr.AppletMain;
import dsr.hud.HUD;
import dsr.hud.IContainer;

public abstract class AbstractComponent extends Rectangle {

	private static final long serialVersionUID = 1L;

	protected AppletMain main;
	protected HUD hud;
	public boolean clickable;
	
	
	public AbstractComponent(AppletMain m, HUD _hud, int x, int y, int w, int h, boolean _selectable, IContainer add_to) {
		super(x, y, w, h);
		
		hud = _hud;
		clickable = _selectable;
		main = m;

		if (add_to != null) {
			add_to.add(this);
		}
	}
	
	
	public abstract void paint(Graphics2D g);
	
	
	public abstract boolean mouseClicked(Point p);
	
}
