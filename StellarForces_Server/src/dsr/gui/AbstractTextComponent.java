package dsr.gui;

import java.awt.Color;
import java.awt.Graphics2D;

import dsr.AppletMain;
import dsr.hud.HUD;
import dsr.hud.IContainer;

public abstract class AbstractTextComponent extends AbstractComponent {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String text;
	private int y_off;
	
	public AbstractTextComponent(String _text, AppletMain m, HUD _hud, int x, int y, int w, int h, boolean _selectable, IContainer add_to) {
		super(m, _hud, x, y, w, h, _selectable, add_to);
		text = _text;
		
		y_off = _hud.getImageHeight()/20; 
	}

	@Override
	public void paint(Graphics2D g) {
		g.setColor(Color.white);
		g.drawString(text, x+5, y+y_off);
	}

	
	public void setText(String s) {
		text = s;
	}
}
