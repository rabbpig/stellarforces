package dsr.gui;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

import dsr.AppletMain;
import dsr.SoundEffects;
import dsr.hud.AbstractIcon;
import dsr.hud.HUD;
import dsr.hud.IContainer;

public class GridLayout extends AbstractComponent implements IContainer {

	//private static int FONT_HEIGHT = 12;

	private static final long serialVersionUID = 1L;

	private Font font;
	private ArrayList<AbstractComponent> components = new ArrayList<AbstractComponent>();
	private int cols, btn_w, btn_h;
	
	public GridLayout(AppletMain m, HUD hud, int x, int y, int _cols, int _btn_w, int _btn_h, IContainer add_to) {
		super(m, hud, x, y, _btn_w*_cols, -1, true, add_to);

		font = HUD.DEF_FONT_NORMAL;// AppletMain.base_font.deriveFont(AppletMain.font_style, hud.getImageWidth()/20);
		cols = _cols;
		btn_w = _btn_w;
		btn_h = _btn_h;
	}
	
	
	public ArrayList<AbstractComponent> getComps() {
		return components;
	}


	@Override
	public boolean mouseClicked(Point p) {
		for (int i=0 ; i<components.size() ; i++) {
			AbstractComponent icon = components.get(i);
			if (icon.contains(p)) {
				main.playSound(SoundEffects.CLICK);
				icon.mouseClicked(p);
				return true;
			}
		}
		return false;
	}


	public void paint(Graphics2D g) {
		g.setFont(font);
		for (int i=0 ; i<components.size() ; i++) {
			AbstractComponent icon = components.get(i);
			icon.paint(g);
		}
	}


	public void clear() {
		components.clear();
	}


	public void add(AbstractComponent icon) {
		if (this.components.size() % cols == 0) {
			icon.x = this.x;
		} else {
			icon.x = this.x + btn_w + 2;

		}
		icon.y = (int)(this.y + ((components.size()/cols) * (btn_h * 1.2f))); // Must be before we add the icon to the ArrayList
		icon.width = btn_w;
		icon.height = btn_h;

		this.components.add(icon);

		// Update our own height
		this.height = icon.y + icon.height;// components.size() * btn_h;

		this.hud.markForRefresh();
	}


	public void addAll(ArrayList<AbstractIcon> a) {
		for (int i=0 ; i<a.size() ; i++) {
			AbstractComponent icon = a.get(i);
			this.add(icon);
		}
	}


	public void removeAll(ArrayList<AbstractIcon> a) {
		for (int i=0 ; i<a.size() ; i++) {
			this.components.remove(a.get(i));
		}
	}


	@Override
	public void remove(AbstractComponent icon) {
		this.components.remove(icon);
	}
	
	
	public AbstractComponent get(int i)  {
		return this.components.get(i);
	}


}
