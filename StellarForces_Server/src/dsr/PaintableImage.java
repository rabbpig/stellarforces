package dsr;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import com.jme.image.Image;
import com.jme.scene.state.TextureState;

import dsr.hud.IPaintable;

public class PaintableImage extends Image {

	private static final long serialVersionUID = 1L;

	public BufferedImage backImg;
	private ByteBuffer scratch;
	private IPaintable painter;

	public PaintableImage(int width, int height, IPaintable p) {
		super();
		painter = p;
		backImg = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
		setWidth(backImg.getWidth());
		setHeight(backImg.getHeight());
		scratch = ByteBuffer.allocateDirect(4 * backImg.getWidth() * backImg.getHeight());
		this.setFormat(Image.Format.RGBA2);
	}


	public void refreshImage(TextureState ts) {
		Graphics2D g = backImg.createGraphics();
		paint(g);
		g.dispose();

		/* get the image data */
		byte data[] = (byte[]) backImg.getRaster().getDataElements(0, 0, backImg.getWidth(), backImg.getHeight(), null);
		scratch.clear();
		scratch.put(data, 0, data.length);
		scratch.rewind();
		setData(scratch);

		if (ts != null) {
			ts.deleteAll();
		}
	}

	public void paint(Graphics2D g) {
		painter.paint(g, this);
	}

}


