package dsr;

import ssmith.audio.SoundCacheThread;

public class SoundEffects extends SoundCacheThread {

	private static final String ROOT_DIR = "data/sfx/";

	public static final int APS_OUT = 1;
	public static final int CLICK = 2;
	public static final int SCREAM = 3;
	public static final int END_TURN = 4;
	public static final int EXPLOSION = 5;
	public static final int FOOTSTEP = 6;
	public static final int HIT = 7;
	public static final int LASER = 8;
	public static final int DOOR = 9;
	public static final int WON = 10;
	public static final int LOST = 11;

	protected boolean mute = false;

	public SoundEffects() {
		super();
	}


	public void playSound(int i) {
		if (!mute) {
			try {
				super.playSound(ROOT_DIR + GetFilename(i));
			} catch (Exception ex) {
				AppletMain.HandleError(null, ex);
			}
		}
	}


	private static String GetFilename(int i) {
		switch (i) {
		case APS_OUT:
			return "aps_out.wav";
		case CLICK:
			return "toggle2.wav";
		case SCREAM:
			return "deathscream1.wav";
		case END_TURN:
			return "end_turn.wav";
		case EXPLOSION:
			return "explosion1.wav";
		case FOOTSTEP:
			return "footstep.wav";
		case HIT:
			return "hit1.wav";
		case LASER:
			return "laser1.wav";
		case DOOR:
			return "door.wav";
		case WON:
			return "mission_completed.mp3";
		case LOST:
			return "mission_failed.mp3";
		default:
			throw new RuntimeException("Unknown sfx type: " + i);

		}
	}


	public void toggleMute() {
		mute = !mute;
	}

	public boolean isMute() {
		return mute;
	}

}
