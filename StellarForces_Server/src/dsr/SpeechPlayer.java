package dsr;

import dsr.data.UnitData;
import dsrwebserver.tables.UnitsTable;

public class SpeechPlayer extends SoundEffects {

	private static final String ROOT_DIR2 = "data/sfx/speech/";

	public static final int MINOR_INJURY = 10;

	public static final int EV_UNIT_SELECTED = 0; // Called, Linden
	public static final int EV_ENEMY_SPOTTED = 1; // Linden
	public static final int EV_UNIT_ESCAPED = 2; // Called, Linden
	public static final int EV_GOOD_SHOT = 3; // When call? Linden
	//public static final int EV_HEARD_ENEMY = 4; // Linden
	public static final int EV_INJURED_ENEMY = 5;// Called, Linden
	public static final int EV_KILLED_ENEMY = 6; // Called, Linden
	public static final int EV_NOT_HARMED_ENEMY = 7; // Called, Linden
	public static final int EV_SHOT_MISSED = 8; // Called, Linden
	public static final int EV_OUT_OF_AMMO = 9; // Called, Linden
	public static final int EV_GRENADE_PRIMED = 10; // Called, Linden
	public static final int EV_RCVD_MAJOR_INJURY = 11; // Called, Linden
	public static final int EV_RCVD_MINOR_INJURY = 12; // Called, Linden
	public static final int EV_RCVD_NO_HARM = 13;// Called, Linden
	public static final int EV_COMPUTER_DESTROYED = 14; // Called, Linden
	public static final int EV_UNIT_KILLED = 15; // Called, Linden
	public static final int EV_EQUIPMENT_SPOTTED = 16;
	public static final int EV_FLAG_ACQUIRED = 17;
	public static final int EV_INCIDENTAL = 18;
	public static final int EV_USED_MEDIKIT = 19; // Called

	protected AppletMain main;

	public SpeechPlayer(AppletMain m) {
		super();

		main = m;
	}


	public void playSound(UnitData unit, int evt) {
		if (!mute) {
			try {
				String filename = GetFilename(unit, evt);
				if (filename.length() > 0) {
					if (unit.model_type == UnitsTable.MT_ALIEN_TYRANT || unit.model_type == UnitsTable.MT_BLOB) {
						super.playSound(ROOT_DIR2 + "alien1" + "/" + filename);
					} else {
						super.playSound(ROOT_DIR2 + "unit1" + "/" + filename);
					}
				}
			} catch (Exception ex) {
				AppletMain.HandleError(null, ex);
			}
		}
	}


	private String GetFilename(UnitData unit, int event) {
		if (unit.model_type == UnitsTable.MT_ALIEN_TYRANT || unit.model_type == UnitsTable.MT_QUEEN_ALIEN || unit.model_type == UnitsTable.MT_BLOB) {
			switch (event) {
			case EV_UNIT_SELECTED: return "gurp1.wav";
			case EV_ENEMY_SPOTTED: return "death4.wav";
			case EV_INJURED_ENEMY: return "pain75_1.wav";
			case EV_KILLED_ENEMY: return "pain75_2.wav";
			case EV_NOT_HARMED_ENEMY: return "death2.wav";
			case EV_RCVD_MAJOR_INJURY: return "jump1.wav";
			case EV_RCVD_MINOR_INJURY: return "pain100_1.wav";
			case EV_RCVD_NO_HARM: return "death1.wav";
			case EV_UNIT_KILLED: return "pain100_2.wav";
			}
		} else {
			switch (event) {
			case EV_UNIT_SELECTED: return "unit_selected.mp3";
			case EV_ENEMY_SPOTTED: return "enemy_spotted.mp3";
			case EV_UNIT_ESCAPED: return "unit_escaped.mp3";
			case EV_GOOD_SHOT: return "good_shot.mp3";
			case EV_INJURED_ENEMY: return "injured_enemy.mp3";
			case EV_KILLED_ENEMY: return "killed_enemy.mp3";
			case EV_NOT_HARMED_ENEMY: return "enemy_unharmed.mp3";
			case EV_SHOT_MISSED: return "shot_missed.mp3";
			case EV_OUT_OF_AMMO: return "out_of_ammo.mp3";
			case EV_GRENADE_PRIMED: return "primed_grenade.mp3";
			case EV_RCVD_MAJOR_INJURY: return "rcvd_major_injury.mp3";
			case EV_RCVD_MINOR_INJURY: return "rcvd_minor_injury.mp3";
			case EV_RCVD_NO_HARM: return "rcvd_no_damage.mp3";
			case EV_COMPUTER_DESTROYED: return "computer_destroyed.mp3";
			case EV_UNIT_KILLED: return "unit_died.mp3";
			}
		}
		return "";
	}

}
