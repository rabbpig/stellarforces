package dsr.effects;

import com.jme.renderer.Renderer;

import dsr.AppletMain;
import dsr.GameObject;

public class AbstractEffect extends GameObject {//implements IUpdatable {

	private static final long serialVersionUID = 1L;

	protected long remove_at;
	protected boolean permanent;

	public AbstractEffect(AppletMain m, String name, float x, float y, float z, boolean perm, long duration) {
		super(m, name, false, false, false, x, z, y);
		main.attachToRootNode(this, true);

		permanent = perm;
		if (!permanent) {
			remove_at = System.currentTimeMillis() + duration;
		}
	}

	//public void update(float interpolation) {
	public void onDraw(Renderer r) { // Use onDraw instead of Draw since we want to update even if it's not in the camera's view
		if (System.currentTimeMillis() > remove_at && this.permanent == false) {
			main.addObjectForRemoval(this);
		} else {
			super.onDraw(r);
		}
	}


}
