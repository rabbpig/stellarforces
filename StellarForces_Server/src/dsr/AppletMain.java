/*
 * Copyright (c) 2009-2016 Stephen Carlyle-Smith (stephen.carlylesmith@googlemail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * * Neither the name of 'jMonkeyEngine' nor the names of its contributors
 *   may be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dsr;

import java.awt.Font;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import org.lwjgl.input.Keyboard;

import ssmith.io.TextFile;
import ssmith.lang.Dates;
import ssmith.lang.Functions;
import ssmith.lang.Geometry;
import ssmith.util.PointByte;

import com.jme.app.AbstractGame;
import com.jme.app.BaseSimpleGame;
import com.jme.bounding.BoundingBox;
import com.jme.input.FirstPersonHandler;
import com.jme.input.InputHandler;
import com.jme.input.KeyBindingManager;
import com.jme.input.KeyInput;
import com.jme.input.MouseInput;
import com.jme.input.action.InputActionEvent;
import com.jme.input.action.InputActionInterface;
import com.jme.intersection.PickData;
import com.jme.math.Ray;
import com.jme.math.Vector2f;
import com.jme.math.Vector3f;
import com.jme.renderer.ColorRGBA;
import com.jme.renderer.Renderer;
import com.jme.scene.Line;
import com.jme.scene.Node;
import com.jme.scene.Spatial;
import com.jme.scene.Spatial.CullHint;
import com.jme.scene.state.TextureState;
import com.jme.scene.state.WireframeState;
import com.jme.system.DisplaySystem;
import com.jme.util.ThrowableHandler;
import com.jme.util.resource.ResourceLocator;
import com.jme.util.resource.ResourceLocatorTool;

import dsr.ai.AIThread;
import dsr.comms.AbstractCommFuncs;
import dsr.comms.EquipmentDataComms;
import dsr.comms.EventDataComms;
import dsr.comms.GameDataComms;
import dsr.comms.MapDataComms;
import dsr.comms.PlayersGamesComms;
import dsr.comms.UnitDataComms;
import dsr.comms.VisibleEnemyComms;
import dsr.comms.WGet4Client;
import dsr.data.AppletMapSquare;
import dsr.data.ClientMapData;
import dsr.data.EquipmentData;
import dsr.data.GameData;
import dsr.data.UnitData;
import dsr.effects.Bullet;
import dsr.effects.Explosion;
import dsr.gui.GridLayout;
import dsr.hud.AbstractIcon;
import dsr.hud.CommandIcon;
import dsr.hud.EquipmentIcon;
import dsr.hud.HUD;
import dsr.hud.IconHUD;
import dsr.hud.LessIcon;
import dsr.hud.MoreIcon;
import dsr.hud.PrimeGrenadeIcon;
import dsr.hud.SelectGameButton;
import dsr.hud.SelectOurUnitIcon;
import dsr.hud.ShotTypeIcon;
import dsr.hud.ShowEnemyIcon;
import dsr.hud.StrategicScanner;
import dsr.hud.ThrowIcon;
import dsr.hud.UnitMovementIcons;
import dsr.models.ComputerModel;
import dsr.models.TargettingLine;
import dsr.models.UnitHighlighter;
import dsr.models.equipment.EquipmentModel;
import dsr.models.map.AbstractMapModel;
import dsr.models.map.BigWall;
import dsr.models.map.Floor;
import dsr.models.map.MapModel_Original;
import dsr.models.map.MapModel_SlimWalls;
import dsr.models.map.MiniFloor;
import dsr.models.map.SlidingDoor;
import dsr.models.map.SlimWall;
import dsr.models.map.ThickWall;
import dsr.models.scenery.GasCannister;
import dsr.models.scenery.NerveGasCloud;
import dsr.models.scenery.SmokeGrenCloud;
import dsr.models.scenery.Starfield;
import dsr.models.units.AbstractUnit;
import dsr.playback.ClientPlaybackControllerThread;
import dsr.start.StartupNew;
import dsrwebserver.missions.AbstractMission;
import dsrwebserver.tables.EquipmentTypesTable;
import dsrwebserver.tables.GamesTable;
import dsrwebserver.tables.MapDataTable;
import dsrwebserver.tables.UnitHistoryTable;
import dsrwebserver.tables.UnitsTable;

/**
 * This is obsolete code from when the SF client used to be a 3D PC game.  The client is now an Android app.
 *  
 * @author stephen
 *
 */
public class AppletMain extends BaseSimpleGame implements ResourceLocator, InputActionInterface, ThrowableHandler, IDisplayMessages {

	public static final String DATA_DIR ="./data/";
	public static int font_style = Font.PLAIN;
	public static Font base_font = new Font("Arial", font_style, 14); 
	private static final long TIME_BETWEEN_EXPLOSIONS = 6;

	public static final float QUAD_OVERLAP = 0.01f;

	// Game settings
	private static final byte MIN_SHOT_ACC = 15;
	private static final byte MAX_SHOT_ACC = 85; // Don't make too high otherwise unit can be picked off at distance easily
	private static final byte OPP_FIRE_ACC_REDUCTION = 15;
	private static final int POINT_BLANK_DAMAGE_BONUS = 5;
	public static final int MIN_DAMAGE_REDUCTION_RANGE = 10;
	public static final int STRENGTH_TO_DESTROY_COMP = 5;
	private static final int OPP_CC_COST = 8;

	// Camera settings
	private static final float CAM_START_HEIGHT = 8f;
	private static final float CAM_MOVE_SPEED = 5f;
	private static final float CAM_TURN_SPEED = 0.6f;
	private static final float VIEW_UNIT_DIST = 8f;
	private static final float CAM_LOCK_Z = -4f;

	// Key codes
	private static final String CMD_FWD = "fwd";
	private static final String CMD_BACK = "back";
	private static final String CMD_TURN_LEFT = "turn_left";
	private static final String CMD_TURN_RIGHT = "turn_right";
	private static final String CMD_STRAFE_LEFT = "strafe_left";
	private static final String CMD_STRAFE_RIGHT = "strafe_right";
	private static final String CMD_NEXT_UNIT = "nextunit";
	private static final String CMD_PREV_UNIT = "prevunit";
	private static final String CMD_TUTORIAL = "tutorial";
	private static final String CMD_HELP = "help";
	private static final String CMD_TEST = "test";
	private static final String CMD_MUTE = "toggle_mute";
	private static final String CMD_CAM_UP = "cam_up";
	private static final String CMD_CAM_DOWN = "cam_down";
	private static final String CMD_GRID = "grid";
	private static final String CMD_LOCK_CAM = "lock_cam";
	private static final String CMD_SHOW_LOS = "show_los";
	private static final String CMD_TOGGLE_SCANNER = "toggle_scanner";

	// Client stages
	public static final byte APP_LOADING_AVAILABLE_GAMES = 0;
	public static final byte APP_PLAYER_SELECT_GAMES = 1;
	public static final byte APP_LOADING_GAME_DATA = 2;
	private static final byte APP_LOADING_MODELS = 3;
	public static final byte APP_STARTED = 4;
	private static final byte APP_WAITING_CONF = 5;
	public static final byte APP_TURN_FINISHED_COMPLETELY = 6;
	private static final byte APP_NO_MORE_GAMES = 7;
	public byte client_mode;

	// AP cost
	private static final byte APS_CHANGE_ITEM = 13;
	private static final byte APS_PICKUP_ITEM = 8;
	private static final byte APS_DROP_ITEM = 3;
	private static final byte APS_REMOVE_ITEM = 8;
	private static final byte APS_PRIME = 10;
	private static final byte APS_ACTIVATE = 10;
	public static final byte APS_THROW = 10;
	public static final byte APS_CATCH = 3;
	public static final byte APS_USE_EQUIPMENT = 25;
	private static final byte APS_OPEN_DOOR = 8;
	public static final byte APS_USE_SCANNER = 8;

	// Blob
	private static final byte APS_SPLIT = 10;
	private static final byte APS_ABSORB = 10;
	private static final byte APS_EXPLODE = 20;

	// Menu modes
	private static final byte MM_DEPLOYING_UNITS = 0;
	private static final byte MM_NO_UNIT_SELECTED = 1;
	private static final byte MM_UNIT_SELECTED = 2;
	private static final byte MM_CHANGE_ITEM = 3;
	private static final byte MM_PICKUP_ITEM = 4;
	private static final byte MM_SHOOTING = 5;
	private static final byte MM_THROW = 6;
	private static final byte MM_PRIME_GRENADE = 7;
	private static final byte MM_TURN_FINISHED_NO_MENU = 8;
	private static final byte MM_CONFIRM_END_TURN = 9;

	public ClientMapData mapdata;
	public UnitData units[];
	public EquipmentData equipment[];
	public GameData game_data;
	private UnitData current_unit = null; // Will always be one of ours
	public int next_to_deploy = -1;

	private GetAllGameData setup;
	private ArrayList<IUpdatable> updating_objects = new ArrayList<IUpdatable>();
	private ArrayList<AbstractIcon> our_unit_icons;
	private ArrayList<AbstractIcon> visible_enemy_icons;
	private ArrayList<AbstractIcon> orig_visible_enemy_icons;
	private boolean check_our_init_visible_units = true;
	private byte menu_mode = AppletMain.MM_NO_UNIT_SELECTED;
	private TargettingLine shot_line; // This is the laser
	private WireframeState myWireState;
	private Ray targetting_ray = new Ray();
	private MyPickResults targetting_ray_results = new MyPickResults(null);
	private TextureStateCache ts_cache;
	private Tutorial tutorial;
	private UnitHighlighter highlighter;
	private float cam_height;
	private AbstractMapModel map_model;
	private SpeechPlayer sfx = new SpeechPlayer(this);
	private Node grid_node = null;
	private boolean camera_locked = false;
	public static boolean help_mode_on = true;
	private boolean played_enemy_spotted = false;
	private Starfield starfield;
	private ClientPlaybackControllerThread playback_controller;
	private ArrayList<GameData> available_games;
	private GridLayout game_selector_layout;
	private StrategicScanner scanner;
	private IconHUD icon_hud;
	protected HUD hud;
	public ModelCache model_cache = new ModelCache();
	private ArrayList<Spatial> objects_for_removal = new ArrayList<Spatial>(); 
	private ArrayList<ExplosionPendingData> explosions_to_show = new ArrayList<ExplosionPendingData>();
	private float time_since_last_explosion = 0;
	private AbstractUnit aiming_at;
	private static String last_error = "";

	// Icons
	private CommandIcon cancel_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Cancel"), CommandIcon.CANCEL, CommandIcon.blue);
	private CommandIcon scanner_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Scanner"), CommandIcon.SCANNER, CommandIcon.blue);
	private CommandIcon tutorial_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Tutorial"), CommandIcon.TUTORIAL, CommandIcon.green);
	private CommandIcon pickup_item_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Pick-up") + " (" + APS_PICKUP_ITEM + ")", CommandIcon.PICKUP_ITEM, CommandIcon.green);
	private CommandIcon change_item_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Change Item") + " (" + APS_CHANGE_ITEM + ")", CommandIcon.CHANGE_ITEM, CommandIcon.green);
	private CommandIcon equip_unit_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Equip Item") + " (" + APS_CHANGE_ITEM + ")", CommandIcon.CHANGE_ITEM, CommandIcon.red);
	private CommandIcon drop_item_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Drop Current Item") + " (" + APS_DROP_ITEM + ")", CommandIcon.DROP_ITEM, CommandIcon.green);
	private CommandIcon remove_item_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Remove Current Item") + " (" + APS_REMOVE_ITEM + ")", CommandIcon.REMOVE_ITEM, CommandIcon.green);
	private CommandIcon select_shot_type_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Shoot"), CommandIcon.SELECT_SHOT_TYPE, CommandIcon.green);
	private CommandIcon show_prime_menu_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Prime") + " (" + APS_PRIME + ")", CommandIcon.SHOW_PRIME_MENU, CommandIcon.green);
	private CommandIcon activate_menu_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Activate") + " (" + APS_ACTIVATE + ")", CommandIcon.ACTIVATE, CommandIcon.green);
	private PrimeGrenadeIcon do_prime_icon = new PrimeGrenadeIcon(this);
	private CommandIcon end_turn_icon = new CommandIcon(this, StartupNew.strings.getTranslation("END TURN"), CommandIcon.END_TURN, null);
	private CommandIcon confirm_end_turn_icon = new CommandIcon(this, StartupNew.strings.getTranslation("END TURN NOW!"), CommandIcon.CONFIRM_END_TURN, null);
	private ShotTypeIcon aimed_shot_icon = new ShotTypeIcon(this, StartupNew.strings.getTranslation("Aimed Shot"), CommandIcon.AIMED_SHOT, CommandIcon.green);
	private ShotTypeIcon snap_shot_icon = new ShotTypeIcon(this, StartupNew.strings.getTranslation("Snap Shot"), CommandIcon.SNAP_SHOT, CommandIcon.green);
	private ShotTypeIcon auto_shot_icon = new ShotTypeIcon(this, StartupNew.strings.getTranslation("Auto Shot"), CommandIcon.AUTO_SHOT, CommandIcon.green);
	private CommandIcon auto_aim_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Auto-Aim"), CommandIcon.AUTO_AIM, CommandIcon.green);
	private CommandIcon throw_menu_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Throw") + " (" + APS_THROW + ")", CommandIcon.SHOW_THROW_MENU, CommandIcon.green);
	private CommandIcon escape_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Escape"), CommandIcon.ESCAPE, CommandIcon.orange);
	private ThrowIcon throw_icon = new ThrowIcon(this);
	private CommandIcon reload_icon;
	private LessIcon less_icon = new LessIcon(this);
	private MoreIcon more_icon = new MoreIcon(this);
	private CommandIcon use_medikit_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Use Medikit") + " (" + APS_USE_EQUIPMENT + ")", CommandIcon.USE_MEDI_KIT, CommandIcon.green);
	//private CommandIcon use_porta_porter = new CommandIcon(this, StartupNew.strings.getTranslation("Use") + " PP Trigger (" + APS_USE_EQUIPMENT + ")", CommandIcon.USE_PORTA_PORTER, CommandIcon.green);
	//private CommandIcon use_adrenalin_shot = new CommandIcon(this, StartupNew.strings.getTranslation("Use Adrenalin Shot") + " (" + APS_USE_EQUIPMENT + ")", CommandIcon.USE_ADRENALIN_SHOT, CommandIcon.green);
	private CommandIcon use_scanner_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Use Scanner") + " (" + (APS_USE_SCANNER) + ")", CommandIcon.USE_SCANNER, CommandIcon.green);
	private CommandIcon open_door_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Open Door") + " (" + APS_OPEN_DOOR + ")", CommandIcon.OPEN_DOOR, CommandIcon.green);
	private CommandIcon close_door_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Close Door") + " (" + APS_OPEN_DOOR + ")", CommandIcon.CLOSE_DOOR, CommandIcon.green);
	private CommandIcon next_unit_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Next Unit"), CommandIcon.NEXT_UNIT, CommandIcon.orange);
	private CommandIcon prev_unit_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Prev Unit"), CommandIcon.PREV_UNIT, CommandIcon.orange);
	private CommandIcon warning_icon = new CommandIcon(this, StartupNew.strings.getTranslation("WARNING! See console"), CommandIcon.WARNING, CommandIcon.red);

	// Blobs
	private CommandIcon split_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Split") + " (" + APS_SPLIT + ")", CommandIcon.SPLIT, CommandIcon.green);
	private CommandIcon absorb_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Absorb") + " (" + APS_ABSORB + ")", CommandIcon.ABSORB, CommandIcon.green);
	private CommandIcon explode_icon = new CommandIcon(this, StartupNew.strings.getTranslation("Explode") + " (" + APS_EXPLODE + ")", CommandIcon.EXPLODE, CommandIcon.green);

	//private ShotTypeIcon opp_fire_aimed_icon = new ShotTypeIcon(this, StartupNew.strings.getTranslation("Aimed Shot"), CommandIcon.OPP_FIRE_AIMED, CommandIcon.green);
	//private ShotTypeIcon opp_fire_snap_icon = new ShotTypeIcon(this, StartupNew.strings.getTranslation("Snap Shot"), CommandIcon.OPP_FIRE_SNAP, CommandIcon.green);
	//private ShotTypeIcon opp_fire_auto_icon = new ShotTypeIcon(this, StartupNew.strings.getTranslation("Auto Shot"), CommandIcon.OPP_FIRE_AUTO, CommandIcon.green);


	public AppletMain() {
		super();

		throwableHandler = this;
		System.setProperty("jme.stats", "set");
		ResourceLocatorTool.addResourceLocator(ResourceLocatorTool.TYPE_TEXTURE, this);

		sfx.mute = !StartupNew.play_sfx;

	}


	public AppletMain(GetAllGameData _setup) {
		this();
		setup = _setup;
	}


	//****************************************************************
	// Start of JME functions

	@Override
	protected void simpleInitGame() {
		// Do nothing
	}


	/**
	 * Makes sure the MouseCursor is visible and disables mouse look.
	 */
	protected void initGame() {
		super.initGame();

		MouseInput.get().setCursorVisible(true);

		ts_cache = new TextureStateCache(this);

		//----

		FirstPersonHandler firstPersonHandler = new FirstPersonHandler(cam, CAM_MOVE_SPEED, CAM_TURN_SPEED);
		input = firstPersonHandler;
		input.addAction(this, InputHandler.DEVICE_MOUSE, 0, InputHandler.AXIS_NONE, false);
		((FirstPersonHandler) input).setButtonPressRequired(true);		

		// Add MB2 action
		input.addAction(this, InputHandler.DEVICE_MOUSE, 1, InputHandler.AXIS_NONE, false);

		KeyBindingManager.getKeyBindingManager().remove("toggle_wire"); // So we can use "T" for Tutorial
		KeyBindingManager.getKeyBindingManager().remove("toggle_normals"); 
		KeyBindingManager.getKeyBindingManager().remove("toggle_pause"); 

		KeyBindingManager.getKeyBindingManager().set(CMD_FWD, KeyInput.KEY_NUMPAD8);
		KeyBindingManager.getKeyBindingManager().add(CMD_FWD, KeyInput.KEY_UP);
		KeyBindingManager.getKeyBindingManager().set(CMD_BACK, KeyInput.KEY_NUMPAD2);
		KeyBindingManager.getKeyBindingManager().add(CMD_BACK, KeyInput.KEY_DOWN);
		KeyBindingManager.getKeyBindingManager().set(CMD_STRAFE_LEFT, KeyInput.KEY_NUMPAD4);
		KeyBindingManager.getKeyBindingManager().add(CMD_STRAFE_LEFT, KeyInput.KEY_LEFT);
		KeyBindingManager.getKeyBindingManager().set(CMD_STRAFE_RIGHT, KeyInput.KEY_NUMPAD6);
		KeyBindingManager.getKeyBindingManager().add(CMD_STRAFE_RIGHT, KeyInput.KEY_RIGHT);
		KeyBindingManager.getKeyBindingManager().set(CMD_TURN_LEFT, KeyInput.KEY_NUMPAD7);
		KeyBindingManager.getKeyBindingManager().add(CMD_TURN_LEFT, KeyInput.KEY_HOME);
		KeyBindingManager.getKeyBindingManager().set(CMD_TURN_RIGHT, KeyInput.KEY_NUMPAD9);
		KeyBindingManager.getKeyBindingManager().add(CMD_TURN_RIGHT, KeyInput.KEY_PGUP);

		KeyBindingManager.getKeyBindingManager().set(CMD_NEXT_UNIT, KeyInput.KEY_N);
		KeyBindingManager.getKeyBindingManager().set(CMD_PREV_UNIT, KeyInput.KEY_P);
		KeyBindingManager.getKeyBindingManager().set(CMD_TUTORIAL, KeyInput.KEY_T);
		KeyBindingManager.getKeyBindingManager().set(CMD_HELP, KeyInput.KEY_H);
		KeyBindingManager.getKeyBindingManager().set(CMD_TEST, KeyInput.KEY_X);
		KeyBindingManager.getKeyBindingManager().set(CMD_MUTE, KeyInput.KEY_M);
		KeyBindingManager.getKeyBindingManager().set(CMD_CAM_UP, KeyInput.KEY_E);
		KeyBindingManager.getKeyBindingManager().set(CMD_CAM_DOWN, KeyInput.KEY_Q);
		KeyBindingManager.getKeyBindingManager().set(CMD_GRID, KeyInput.KEY_G);
		KeyBindingManager.getKeyBindingManager().set(CMD_SHOW_LOS, KeyInput.KEY_K);
		KeyBindingManager.getKeyBindingManager().set(CMD_TOGGLE_SCANNER, KeyInput.KEY_Z);

		KeyBindingManager.getKeyBindingManager().remove("camera_out");
		KeyBindingManager.getKeyBindingManager().set(CMD_LOCK_CAM, KeyInput.KEY_C);


		// Redefine the keys?
		String filename = "redefine_keys.txt"; 
		if (new File(filename).canRead()) {
			AppletMain.p("Using redefined keys.");
			TextFile tf = new TextFile();
			try {
				tf.openFile(filename, TextFile.READ);
				while (tf.isEOF() == false) {
					String s = tf.readLine().trim();
					if (s.startsWith("#") == false) {
						String kv[] = s.split("=");
						if (kv.length == 2) {
							int code = getKeyCode(kv[1]); //(Keyboard.getKeyIndex(kv[1]);
							if (code > 0) {
								KeyBindingManager.getKeyBindingManager().remove(kv[0]);
								KeyBindingManager.getKeyBindingManager().set(kv[0], code);
							} else {
								AppletMain.p("Redefining keys: key " + kv[0] + " not recognised; try entering it literally (e.g. SEMICOLON).");
							}
						}
					}
				}
				tf.close();
			} catch (Exception e) {
				AppletMain.HandleError(this, e);
			}
		}

		myWireState = display.getRenderer().createWireframeState();
		myWireState.setEnabled(true);

		((FirstPersonHandler) input).getMouseLookHandler().setEnabled(true);

		//lightState.setEnabled(false); If turned on, it stops the HUD transparency!

		hud = new HUD(this);
		game_selector_layout = new GridLayout(this, hud, 50, this.hud.getImageHeight()/7, 1, (int)(this.hud.getImageWidth() * 0.8f), this.hud.getImageHeight()/15, null);

		showGameSelector();
	}


	private void addStarfield() {
		if (StartupNew.hi_mem) {
			starfield = new Starfield();
			this.attachToRootNode(starfield, false);
		}

	}


	public void toggleScanner() {
		if (scanner == null) {
			scanner = new StrategicScanner(this, hud);
		} else {
			this.hud.remove(scanner);
			scanner = null;
		}
	}


	public void showGameSelector() {
		this.rootNode.detachAllChildren();
		this.statNode.detachAllChildren();

		this.statNode.attachChild(hud);
		statNode.updateGeometricState(0, true);
		statNode.updateRenderState();

		this.addStarfield();

		this.client_mode = APP_LOADING_AVAILABLE_GAMES;

		System.gc();

	}


	/**
	 * Called every frame to update scene information.
	 * 
	 * @param interpolation
	 *            unused in this implementation
	 * @see BaseSimpleGame#update(float interpolation)
	 */
	protected void update(float interpolation_dont_use) {
		super.update(interpolation_dont_use);

		if (this.objects_for_removal.size() > 0) {
			while (this.objects_for_removal.size() > 0) {
				Spatial s = this.objects_for_removal.get(0);
				s.removeFromParent();
				objects_for_removal.remove(0);
			}
			System.gc();
		}

		this.time_since_last_explosion += this.getInterpolation();

		// If there's any queued explosions and we're not showing one, show it.
		if (this.areExplosionsScheduled() && time_since_last_explosion >= TIME_BETWEEN_EXPLOSIONS) {
			ExplosionPendingData epd = this.getNextExplosion();
			p("Showing explosion (" + this.getNumScheduledExplosions() + " remaining)...");
			explodeGrenade(epd.eq, epd.caused_by);
			time_since_last_explosion = 0;
		}

		if (client_mode == APP_LOADING_AVAILABLE_GAMES) {
			try {
				if (available_games == null) {
					WGet4Client wc = new WGet4Client(this, PlayersGamesComms.GetPlayersGamesRequest(StartupNew.login, StartupNew.pwd), true);
					String response = wc.getResponse();
					available_games = PlayersGamesComms.DecodeGameDataResponse(response);
				} else {
					this.model_cache.clear();
					targetting_ray_results.clear();
					this.updating_objects.clear();
					hud.clearText();
					game_selector_layout.clear();
					System.gc();

					int num_games = 0;
					if (available_games.size() > 0) {
						this.hud.add(game_selector_layout);
						for (int i=0 ; i<available_games.size() ; i++) {
							GameData game = available_games.get(i);
							//boolean is_ai = game.ai_for_side[1] > 0 || game.ai_for_side[2] > 0; // Are any sides ai?
							//if (game.game_id != prev_game || is_ai) { // In case the server hasn't updated itself and we end up taking our turn again
							if (game.has_side_equipped[game.our_side] > 0) {
								new SelectGameButton(game, this, hud, game_selector_layout);
								num_games++;
							}
						}
					}
					if (num_games == 0) {
						hud.addText(StartupNew.strings.getTranslation("You have no more turns to take."), false);
						hud.addText(StartupNew.strings.getTranslation("Start a new game on the website!"), false);
						client_mode = APP_NO_MORE_GAMES;
					} else if (num_games == 1) {
						this.client_mode = APP_PLAYER_SELECT_GAMES;
						SelectGameButton btn = (SelectGameButton)game_selector_layout.get(0); 
						this.selectGame(btn.game);
					} else {
						this.client_mode = APP_PLAYER_SELECT_GAMES;
						hud.addText(StartupNew.strings.getTranslation("Please choose one of your games"), false);
					}
					available_games = null;
				}
			} catch (Exception ex) {
				HandleError(this, ex);
			}

		} else if (client_mode == APP_LOADING_GAME_DATA) {
			if (setup.isAlive() == false) { // Wait for thread to finish
				this.startMainGame();
			}

		} else if (client_mode == APP_LOADING_MODELS) {
			startMainGame();
		} else if (client_mode == AppletMain.APP_WAITING_CONF && this.areExplosionsScheduled() == false) {
			if (playback_controller == null) {
				this.recalcVisibleEnemiesAndOppFire(false, null); // See any units that might have become visible due to explosions and walls going down.
				this.updateGameDataOnServer(GameDataComms.TURN_ENDED);
				this.client_mode = AppletMain.APP_TURN_FINISHED_COMPLETELY;
				this.updateMenu(AppletMain.MM_TURN_FINISHED_NO_MENU);
			}
		} else if (this.client_mode == AppletMain.APP_TURN_FINISHED_COMPLETELY && StartupNew.update_server_thread.isDataWaiting() == false) {
			if (playback_controller == null) {
				if (time_since_last_explosion >= TIME_BETWEEN_EXPLOSIONS) { // Pause for a second
					this.updateMenu(AppletMain.MM_TURN_FINISHED_NO_MENU);
					this.showGameSelector();
				}
			}
		}

		if (!pause) {
			if (this.client_mode < AppletMain.APP_TURN_FINISHED_COMPLETELY) {
				if (cam.getLocation().y != cam_height) {
					cam.getLocation().y = cam_height;
				}
			} else {
				if (cam.getLocation().y < 0.5f) {
					cam.getLocation().y = 0.5f;
				}
			}

			if (this.client_mode < AppletMain.APP_WAITING_CONF) {
				if (this.current_unit != null) {
					cam.lookAt(this.current_unit.model.getWorldTranslation(), Vector3f.UNIT_Y);

					// Move cam closer to our unit
					if (this.camera_locked) {
						cam.getLocation().x = this.current_unit.model.getWorldTranslation().x;
						cam.getLocation().z = this.current_unit.model.getWorldTranslation().z + CAM_LOCK_Z;
					} else {
						float dist = cam.getLocation().distance(this.current_unit.model.getWorldTranslation()); 
						float req_dist = (VIEW_UNIT_DIST + this.cam_height);
						if (dist > req_dist) {
							float cam_speed = tpf * CAM_MOVE_SPEED * 3;
							float d = Functions.mod(dist-req_dist); 
							if (d <= 3) {
								cam_speed = d/4;
							}
							cam.getLocation().addLocal(cam.getDirection().mult(cam_speed));
						}
					}
					if (this.game_data != null) {
						if (playback_controller == null && this.game_data.ai_for_side[this.game_data.our_side] <= 0) {
							if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_FWD, false)) {
								this.current_unit.model.moveFwd();
								this.hud.markForRefresh();
							}
							if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_BACK, false)) {
								this.current_unit.model.moveBack();
								this.hud.markForRefresh();
							}
							if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_STRAFE_LEFT, false)) {
								this.current_unit.model.moveStrafeLeft();
								this.hud.markForRefresh();
							}
							if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_STRAFE_RIGHT, false)) {
								this.current_unit.model.moveStrafeRight();
								this.hud.markForRefresh();
							}
							if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_TURN_LEFT, false)) {
								this.current_unit.model.turnBy(-45);
								this.hud.markForRefresh();
							}
							if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_TURN_RIGHT, false)) {
								this.current_unit.model.turnBy(45);
								this.hud.markForRefresh();
							}
						}
					}
				}
				if (this.game_data != null) {
					if (this.game_data.ai_for_side[this.game_data.our_side] <= 0) {
						if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_NEXT_UNIT, false)) {
							this.selectNextUnit();
						}
						if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_PREV_UNIT, false)) {
							this.selectPrevUnit();
						}
					}
				}
				if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_CAM_UP, true)) {
					this.cam_height += 1;
					if (this.cam_height > 20) {
						this.cam_height = 20;
					}
				}
				if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_CAM_DOWN, true)) {
					this.cam_height -= 1;
					if (this.cam_height < 1) {
						this.cam_height = 1;
					}
				}
			}

			if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_TUTORIAL, false)) {
				tutorial();
			}
			if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_HELP, false)) {
				AppletMain.help_mode_on = !AppletMain.help_mode_on;
				if (AppletMain.help_mode_on) {
					this.addToHUD(StartupNew.strings.getTranslation("Help mode is ON"));
				} else {
					this.addToHUD(StartupNew.strings.getTranslation("Help mode is off"));
				}
				this.hud.markForRefresh();
			}
			if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_TEST, false)) {
				this.hud.removeFromParent();
				if (this.map_model != null) {
					this.map_model.removeFromParent();
				}
				this.statNode.updateGeometricState(0, true);
				/*if (FIX_EXPL) {
					UnitData unit = UnitData.GetUnitDataFromID(units, 12688);
					this.explosion((byte)49, (byte)5, (byte)4, 100, unit, true, false);
					this.explosion((byte)45, (byte)5, (byte)3, 80, unit, true, false);
				}*/

				/*long total = Runtime.getRuntime().totalMemory() / 1024;
				long free = Runtime.getRuntime().freeMemory() / 1024;
				p("Total Mem: " + total + "Mb");
				p("Used Mem: " + (total-free) + "Mb");
				p("Free Mem: " + free + "Mb");*/

				// Cause error
				/*String s = null;
				p(s.length());*/
			}
			/*if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_TEST2, false)) {
				this.showExplosion(10, 10, 5);
			}*/
			if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_SHOW_LOS, false)) {
				if (shot_line == null) {
					shot_line = new TargettingLine(this);
					attachToRootNode(shot_line, true);
				} else {
					shot_line.removeFromParent();
					shot_line = null;
				}
			}
			if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_TOGGLE_SCANNER, false)) {
				this.toggleScanner();
			}
			if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_MUTE, false)) {
				sfx.toggleMute();
				if (sfx.isMute() == false) {
					StartupNew.StartMusic();
				} else {
					StartupNew.StopMusic();
				}
			}
			if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_GRID, false)) {
				if (grid_node == null) {
					//p("Showing grid.");
					grid_node = this.getGridNode();
					this.attachToRootNode(grid_node, true);
				} else {
					//p("Removing grid.");
					grid_node.removeFromParent();
					grid_node = null;
					this.rootNode.updateGeometricState(0, true);
				}
			}
			if (KeyBindingManager.getKeyBindingManager().isValidCommand(CMD_LOCK_CAM, false)) {
				this.camera_locked = !camera_locked;
				if (this.camera_locked) {
					this.addToHUD("Camera locked");
				} else {
					this.addToHUD("Camera unlocked");
				}
			}

			for (int i=0 ; i<updating_objects.size() ; i++) {
				IUpdatable obj = updating_objects.get(i);
				obj.update(tpf);
			}

			if (this.shot_line != null && this.current_unit != null && playback_controller == null) {
				// Update aiming line and find what we're aiming it
				Vector3f dir = cam.getDirection().clone();
				dir.y = 0; // Stop us looking up or down

				targetting_ray.origin = this.current_unit.model.getWorldTranslation().clone();
				targetting_ray.origin.y = 0.5f; // So we hit gas cannisters
				targetting_ray.direction = dir;

				targetting_ray_results.setObjectToIgnore(this.current_unit.model.collider);
				targetting_ray_results.clear();
				rootNode.findPick(targetting_ray, targetting_ray_results);
				Vector3f source = this.current_unit.model.getWorldTranslation();
				boolean hit_something = false;
				aiming_at = null;
				if (targetting_ray_results.getNumber() > 0) {
					for (int i=0 ; i<targetting_ray_results.getNumber() ; i++) {
						// If the target is an enemy unit, check whether it is actually visible to our side
						GameObject o = targetting_ray_results.getGameObject(i);
						if (o.can_be_shot) {
							// If there's a chance this item cannot be shot, draw line through it
							if (o.isChanceofNotHitting()) {
								continue;
							}
							if (o instanceof AbstractUnit) {
								AbstractUnit unit = (AbstractUnit)o;
								if (this.game_data.areSidesFriends(unit.unit_data.getSide(), this.current_unit.getSide()) == false) {
									if (unit.getCullHint().equals(CullHint.Always)) { // Is it an invisible enemy?
										continue;
									} else {
										aiming_at = unit;
									}
								}
							}
							Vector3f hit_point = source.add(targetting_ray.getDirection().mult(targetting_ray_results.getPickData(i).getDistance()));
							shot_line.updateLine(source, hit_point);
							this.rootNode.updateGeometricState(0, true);
							hit_something = true;
							break;
						}
					}
				} // Line is going off the edge of the map
				if (hit_something == false) {
					Vector3f hit_point = source.add(targetting_ray.getDirection().mult(20f));
					shot_line.updateLine(source, hit_point);
					this.rootNode.updateGeometricState(0, true);
				}
			}

			if (starfield != null) {
				starfield.update(this.cam.getLocation());
			}

			/** Update controllers/render states/transforms/bounds for rootNode. */
			rootNode.updateGeometricState(tpf, true);
			statNode.updateGeometricState(tpf, true);
		}
	}


	public void tutorial() {
		if (this.tutorial != null) {
			this.addToHUD("--");
			this.addToHUD(this.tutorial.getText());
			this.hud.markForRefresh();
		}

	}


	public void selectGame(GameData game) throws IOException {
		//prev_game = game.game_id;
		setup = new GetAllGameData(game, true, this, StartupNew.login, StartupNew.pwd);
		this.hud.remove(game_selector_layout);

		this.hud.addText(StartupNew.strings.getTranslation("Loading") + " " + game.getFullTitle(), false);
		this.hud.addText(StartupNew.strings.getTranslation("Please wait") + "...", false);
		client_mode = APP_LOADING_GAME_DATA;
	}


	/**
	 * This is called every frame in BaseGame.start(), after update()
	 * 
	 * @param interpolation
	 *            unused in this implementation
	 * @see AbstractGame#render(float interpolation)
	 */
	protected final void render(float interpolation) {
		super.render(interpolation);

		Renderer r = display.getRenderer();

		/** Draw the rootNode and all its children. */
		r.draw(rootNode);

		/** Draw the stats node to show our stat charts. */
		r.draw(statNode); // This also draws the HUD!

		doDebug(r);
	}


	private int getKeyCode(String s) {
		s = s.toUpperCase();
		for (int i=0 ; i<254 ; i++) {
			String t = Keyboard.getKeyName(i);
			if (t != null) {
				//AppletMain.p(t);
				if (t.equalsIgnoreCase(s)) {
					return i;
				}
			}
		}
		return -1;
	}


	/**
	 * Add a addComponentListener which reinitializes the renderer if the
	 * Applet is resized.
	 */
	protected void initSystem() {
		super.initSystem();
		DisplaySystem.getDisplaySystem().getRenderer().setBackgroundColor(new ColorRGBA(0, 0, 0, 1));
		DisplaySystem.getDisplaySystem().setTitle(SharedStatics.TITLE);
	}

	//****************************************************************

	private Node getGridNode() {
		float height = 0.01f;
		Node n = new Node();
		if (this.mapdata != null) {
			int size = this.mapdata.getMapWidth();
			for (int i=0 ; i<size ; i++) {
				// Add axis lines
				Vector3f[] vertex = new Vector3f[2];
				vertex[0] = new Vector3f(i, height, size);
				vertex[1] = new Vector3f(i, height, 0);
				Line l = new Line("LinesX", vertex, null, null, null);
				l.setIsCollidable(false);
				//l.setRandomColors();
				l.setModelBound(new BoundingBox());
				l.updateModelBound();
				//l.updateRenderState();
				n.attachChild(l);
			}

			for (int i=0 ; i<size ; i++) {
				Vector3f[] vertex = new Vector3f[2];
				vertex[0] = new Vector3f(0, height, i);
				vertex[1] = new Vector3f(size, height, i);
				Line l = new Line("LinesZ", vertex, null, null, null);
				//l.setRandomColors();
				l.setIsCollidable(false);
				l.setModelBound(new BoundingBox());
				l.updateModelBound();
				n.attachChild(l);
			}
		}
		n.setModelBound(new BoundingBox());
		n.updateModelBound();
		return n;
	}


	protected void startMainGame() { 
		//try {
		System.gc();

		our_unit_icons = new ArrayList<AbstractIcon>();
		visible_enemy_icons = new ArrayList<AbstractIcon>();
		orig_visible_enemy_icons = new ArrayList<AbstractIcon>();
		check_our_init_visible_units = true;

		cam_height = CAM_START_HEIGHT;

		this.rootNode.detachAllChildren();
		this.statNode.detachAllChildren();

		this.statNode.attachChild(hud);
		icon_hud = new IconHUD(this, hud, 2, hud);
		/*if (game_data != null) {
			if (game_data.ai_for_side[game_data.our_side] <= 0) {
				new UnitMovementIcons(this, hud);
			}
			}*/
		statNode.updateGeometricState(0, true);
		statNode.updateRenderState();
		this.addStarfield();

		AppletMain.p("Creating models...");

		// Get all the data from the setup thread
		this.game_data = setup.game_data;
		this.units = setup.units;
		this.mapdata = setup.getMap();
		this.equipment = setup.equipment;

		if (playback_controller != null) {
			// Wait until the controller is ready
			while (playback_controller.is_ready == false) {
				Functions.delay(1000);
			}
		}
		createMapModel(null);

		// Attach unit models to root node
		for(int i=0 ; i<units.length ; i++) {
			UnitData unit = units[i];
			// Create the model
			if (unit.getStatus() == UnitsTable.ST_DEPLOYED || this.playback_controller != null) {
				unit.model = AbstractUnit.Factory(this, unit);
				unit.model.setCullHint(CullHint.Always); // By default
				unit.updateModelFromUnitData();
			}
		}

		// Attach visible equipment models to root node
		for(int i=0 ; i<equipment.length ; i++) {
			EquipmentData equip = equipment[i];
			if (equip.getUnitID() <= 0 && equip.destroyed == false) {
				try {
					PointByte p = EquipmentData.GetEquipmentMapSquare(this.mapdata, equip);
					if (p.x >= 0 && p.y >= 0) {
						equip.model = EquipmentModel.Factory(this, equip, p.x, p.y);
						this.attachToRootNode(equip.model, false);
						if (playback_controller != null) {
							// Do nothing
						} else {
							if (game_data.is_advanced == 1 && equip.seen_by_side[game_data.our_side] == 0) {
								equip.model.setCullHint(CullHint.Always); // Hide it!
							}
						}
					}
				} catch (Exception ex) {
					AppletMain.HandleError(this, ex);
				}
			}
		}			

		UnitData unit_to_be_selected = null;
		if (this.playback_controller == null) {
			// Show our units
			for(int i=0 ; i<units.length ; i++) {
				UnitData unit = units[i];
				//if (unit.getSide() == game_data.our_side) {
				if (this.game_data.areSidesFriends(unit.getSide(), game_data.our_side)) {
					if (unit.getStatus() == UnitsTable.ST_DEPLOYED) {
						if (game_data.game_status == GamesTable.GS_STARTED) {
							if (this.game_data.turn_side == this.game_data.our_side) {
								if (unit.getSide() == game_data.our_side) {
									// Select our first unit
									if (unit_to_be_selected == null) {
										unit_to_be_selected = unit;
									}
									// Create their icon
									SelectOurUnitIcon icon = new SelectOurUnitIcon(this, unit);
									this.our_unit_icons.add(icon);
								}
							}
						}
						unit.model.setCullHint(CullHint.Inherit);

					}
				} else {
					// Create the enemy's icon
					new ShowEnemyIcon(this, unit);
				}
			}
		}

		rootNode.updateModelBound();
		rootNode.updateGeometricState(0, true);
		rootNode.updateRenderState();

		AppletMain.p("Created models.");

		if (this.playback_controller == null) {
			// Spin models round now that we've updated to rootNode
			for(int i=0 ; i<units.length ; i++) {
				UnitData unit = units[i];
				if (unit.getStatus() == UnitsTable.ST_DEPLOYED) {
					unit.updateModelFromUnitData();
				}
			}			
		}

		if (game_data.ai_for_side[game_data.our_side] <= 0) {
			new UnitMovementIcons(this, hud);
		}

		if (game_data.game_status == GamesTable.GS_CREATED_DEPLOYMENT) {
			this.client_mode = APP_STARTED;
			this.updateMenu(AppletMain.MM_DEPLOYING_UNITS);
			if (game_data.ai_for_side[game_data.our_side] <= 0) {
				cam.setLocation(new Vector3f(0f, cam_height, 0f));
				cam.lookAt(new Vector3f(11f, 0f, 11f), Vector3f.UNIT_Y); // So we are looking at the map
				this.getNextUnitToDeploy();
				this.addToHUD(StartupNew.strings.getTranslation("Deployment squares are big yellow/red 'D's."));
				this.addToHUD(StartupNew.strings.getTranslation("Right-click on the map to deploy a unit."));
			} else {
				this.addToHUD(StartupNew.strings.getTranslation("AI is deploying..."));
			}
		} else if (game_data.game_status == GamesTable.GS_STARTED && unit_to_be_selected != null) { // Need to check if unit_to_be_selected is not null in case they don't have any units left (i.e. client crashed after unit died but before ending turn)
			this.client_mode = APP_STARTED;
			rootNode.updateGeometricState(0, true);

			recalcVisibleEnemiesAndOppFire(false, null);
			if (game_data.ai_for_side[game_data.our_side] <= 0) {
				this.selectOurUnit(unit_to_be_selected);
				cam.setLocation(new Vector3f(this.current_unit.getMapX()-3f, cam_height, this.current_unit.getMapZ()-3f));
				this.addToHUD(StartupNew.strings.getTranslation("Press T to step through the tutorial"));
				if (AppletMain.help_mode_on == false) {
					this.addToHUD(StartupNew.strings.getTranslation("Press H for help mode"));
				}
				this.updateMenu(AppletMain.MM_UNIT_SELECTED); // Must be after we've reclac'd visible enemies so we can show their icons.
				if (this.game_data.max_turns == this.game_data.turn_no) {
					this.addToHUD(StartupNew.strings.getTranslation("This is the last turn!"));
				}
			} else {
				this.addToHUD(StartupNew.strings.getTranslation("AI is taking turn..."));

			}
		} else if (game_data.game_status == GamesTable.GS_FINISHED) {
			// Watching playback
			this.addToHUD(StartupNew.strings.getTranslation("You are watching a playback"));
			this.client_mode = APP_STARTED;
			cam.setLocation(new Vector3f(0f, cam_height, 0f));
			cam.lookAt(new Vector3f(11f, 0f, 11f), Vector3f.UNIT_Y); // So we are looking at the map
		} else {
			this.updateMenu(AppletMain.MM_NO_UNIT_SELECTED);
			this.addToHUD(StartupNew.strings.getTranslation("You do not seem to have any units!"));
		}

		cam.update();

		tutorial = new Tutorial(this);

		if (game_data.ai_for_side[game_data.our_side] == 1) {
			AIThread ai = new AIThread(this);
			ai.start();
			this.addToHUD(StartupNew.strings.getTranslation("Started AI..."));
		}

		System.gc();

		/*} catch (Exception ex) { Removed this otherwise we get stuck in a loop if there is an error
			HandleError(this, ex);
		}*/
	}


	public void finishedDeployment() {
		this.addToHUD(StartupNew.strings.getTranslation("Please wait..."));
		updateGameDataOnServer(GameDataComms.DEPLOYED);
		StartupNew.waitForPendingUpdateRequests();
		client_mode = APP_TURN_FINISHED_COMPLETELY;
		if (game_data.ai_for_side[game_data.our_side] <= 0) {
			this.addToHUD(StartupNew.strings.getTranslation("All units deployed!"));
			this.addToHUD(StartupNew.strings.getTranslation("Please visit the website for the next stage."));
		}
	}


	public void addObjectForUpdate(IUpdatable o) {
		this.updating_objects.add(o);
	}


	public void removeObjectForUpdate(IUpdatable o) {
		this.updating_objects.remove(o);
	}


	public URL locateResource(String resourceName) {
		try {
			return new File(resourceName).toURI().toURL();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}


	//@Override
	public void performAction(InputActionEvent evt) {
		try {
			if (evt.getTriggerPressed()) { // Get the MouseDown
				// Check if an icon has been clicked
				//Point icon_point = new Point(MouseInput.get().getXAbsolute(), display.getHeight() - MouseInput.get().getYAbsolute() - 2);
				Point icon_point = new Point((MouseInput.get().getXAbsolute() * hud.getImageWidth()) / display.getWidth(), ((display.getHeight() - MouseInput.get().getYAbsolute()) * hud.getImageHeight()) / display.getHeight());
				//p("Mouse clicked: Coords:" + icon_point.x + "," + icon_point.y);
				if (scanner == null) {
					if (hud.componentSelected(icon_point) == false) { // No icons selected
						if (evt.getTriggerIndex() == 1) { // RMB
							Vector2f screenPos = new Vector2f(MouseInput.get().getXAbsolute(), MouseInput.get().getYAbsolute());
							Ray ray = display.getPickRay(screenPos, false, null);
							MyPickResults picker = new MyPickResults(null);
							rootNode.findPick(ray, picker);
							if (picker.getNumber() > 0) {
								for (int i=0 ; i<picker.getNumber() ; i++) { 
									GameObject target = picker.getGameObject(i);
									if (target != null) {
										if (target instanceof AbstractUnit && this.next_to_deploy < 0) {
											AbstractUnit unit = (AbstractUnit)target;
											if (unit.unit_data != null) { // Might be a corpse
												if (unit.unit_data.getSide() == this.game_data.our_side) {
													if (this.client_mode < AppletMain.APP_WAITING_CONF) {
														if (unit.unit_data.getStatus() == UnitsTable.ST_DEPLOYED) {
															this.selectOurUnit(unit.unit_data);
														}
													} else {
														this.addToHUD(unit.unit_data.name);
													}
													break;
												} else { // Enemy unit
													if (unit.getCullHint() != CullHint.Always) {
														this.addToHUD("");
														this.addToHUD(unit.unit_data.name);
														this.addToHUD(StartupNew.strings.getTranslation("Owned by") + " " + game_data.GetSideNameFromSide(unit.unit_data.getSide()) + " (side " + unit.unit_data.getSide() + ").");
														if (unit.unit_data.can_use_equipment) {
															this.addToHUD(StartupNew.strings.getTranslation("They are using") + " " + unit.unit_data.getUnitsCurrentItemAsString());
														}
														//break; NO, THERE MIGHT BE SOMETHING ON THE FLOOR AS WELL
													}
												}
											}
											// Continue so we see what's on the floor if it's an invisible enemy unit!
										} else if (target instanceof Floor || target instanceof MiniFloor) {
											byte x = -1, z = -1;
											if (target instanceof Floor) {
												Floor floor = (Floor)target;
												x = (byte)floor.getWorldTranslation().x;
												z = (byte)floor.getWorldTranslation().z;
											} else if (target instanceof MiniFloor) {
												MiniFloor floor = (MiniFloor)target;
												x = (byte)floor.getWorldTranslation().x;
												z = (byte)floor.getWorldTranslation().z;
											}
											if (next_to_deploy >= 0) {
												if (deployUnit(x, z)) {
													break;
												}
											} else { // Say what equipment is on there
												AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(x, z) ;
												if (sq != null) {
													String s = sq.getListOfEquipment(this.game_data.is_advanced, this.game_data.our_side);
													if (s.length() > 0) {
														this.addToHUD(StartupNew.strings.getTranslation("That location contains") + ":-");
														this.addToHUD(s);
													} else {
														//this.addToHUD("There is nothing there.");
													}
												}
											}
										}
									}
								}
							}
						}
					}
				} else {
					// Clicked on scanner
					if (this.game_data.game_status == GamesTable.GS_STARTED) {
						Point map_coords = scanner.getMapCoords(icon_point);
						UnitData unit = UnitData.GetUnitDataFromMapSq(this.units, map_coords.x, map_coords.y, this.game_data.our_side, false);
						if (unit != null) {
							this.selectOurUnit(unit);
						} else {
							this.toggleScanner();
						}
					} else {
						this.toggleScanner();
					}
				}
			}
		} catch (Exception ex) {
			//ex.printStackTrace();
			//this.addToHUD(ex.toString());
			AppletMain.HandleError(this, ex);
		}
	}


	public boolean deployUnit(byte x, byte z) {
		if (this.game_data.areSidesFriends(this.mapdata.getSq_MaybeNULL(x, z).deploy_sq_side, game_data.our_side)) {
			UnitData unit = units[next_to_deploy];
			// Check there isn't already a unit there!
			UnitData other = this.getUnitAt(x, z);
			if (other == null) {
				// Deploy unit!
				unit.model = AbstractUnit.Factory(this, unit);
				unit.setMapLocation(this, x, z, this.mapdata.getSq_MaybeNULL(x, z));
				unit.setAngle(Functions.rnd(0, 7) * 45);
				unit.setStatus(UnitsTable.ST_DEPLOYED);
				this.updateUnitOnServer(unit);
				this.addToHUD("Unit deployed.");
				this.getNextUnitToDeploy();
				return true;
			} else {
				this.addToHUD(StartupNew.strings.getTranslation("There is already a unit there!"));
			}
		} else {
			this.addToHUD(StartupNew.strings.getTranslation("That is not a deployment square."));
		}
		return false;
	}


	public UnitData getUnitAt(int map_x, int map_z) {
		for(int i=0 ; i<units.length ; i++) {
			UnitData unit = units[i];
			if (unit.getStatus() == UnitsTable.ST_DEPLOYED) {
				if (unit.getMapX() == map_x && unit.getMapZ() == map_z) {
					return unit;
				}
			}
		}
		return null;
	}


	public UnitData getDeadUnitAt(int map_x, int map_z) {
		for(int i=0 ; i<units.length ; i++) {
			UnitData unit = units[i];
			if (unit.getStatus() == UnitsTable.ST_DEAD) {
				if (unit.getMapX() == map_x && unit.getMapZ() == map_z) {
					return unit;
				}
			}
		}
		return null;
	}


	public void getNextUnitToDeploy() {
		// Find the next unit to deploy
		next_to_deploy = -1;

		for (int i=0 ; i<this.units.length ; i++) {
			if (units[i].getSide() == game_data.our_side) {
				if (units[i].getStatus() == UnitsTable.ST_AWAITING_DEPLOYMENT) {
					next_to_deploy = i;
					break;
				}
			}
		}

		this.hud.clearText();
		if (next_to_deploy == -1) {
			// All have been deployed!
			this.finishedDeployment();
		} else {
			this.addToHUD(StartupNew.strings.getTranslation("Unit") + " " + units[next_to_deploy].order_by + ":");
			this.addToHUD(StartupNew.strings.getTranslation("Select start square for") + " " + units[next_to_deploy].name);
			if (units[next_to_deploy].can_use_equipment) {
				this.addToHUD(StartupNew.strings.getTranslation("Unit is carrying") + ":-");
				this.addToHUD(getUnitsEquipmentAsString(units[next_to_deploy]).toString());
			}
		}
	}


	public void recalcVisibleEnemiesAndOppFire(boolean opp_fire, UnitData unit_that_moved) {
		if (playback_controller != null) {
			return; // Do nothing
		}

		played_enemy_spotted = false;
		// Store a list of who we can already see
		orig_visible_enemy_icons.clear();
		orig_visible_enemy_icons.addAll(visible_enemy_icons);

		boolean can_see_all_enemies = game_data.side_see_enemies[game_data.our_side] == 1;
		this.visible_enemy_icons.clear();

		// Check if there is any visible equipment
		if (game_data.is_advanced == 1) { // Why not see corpses?
			for (int j_friend=0 ; j_friend<units.length ; j_friend++) { // Loop through our units or comrades
				if (this.game_data.areSidesFriends(units[j_friend].getSide(), game_data.our_side)) {
					if (units[j_friend].getStatus() == UnitsTable.ST_DEPLOYED) {
						for (int e=0 ; e<equipment.length ; e++) { // Loop through equipment
							if (equipment[e].getUnitID() <= 0 && equipment[e].destroyed == false) {
								if (equipment[e].seen_by_side[game_data.our_side] == 0) {
									if (canUnitSeeEquipment(units[j_friend], equipment[e])) {
										equipment[e].model.setCullHint(CullHint.Never);
										equipment[e].seen_by_side[game_data.our_side] = 1;
										PointByte p = EquipmentData.GetEquipmentMapSquare(this.mapdata, equipment[e]);
										this.updateEquipmentOnServer(equipment[e], p.x, p.y);
									}
								}
							}
						}
					}
				}
			}
		}


		// Clear all "can_see" vars
		for (int i=0 ; i<this.units.length ; i++) {
			units[i].clearSeenUnits();
		}


		// Loop through enemy units
		for (int i_enemy=0 ; i_enemy<this.units.length ; i_enemy++) {
			if (this.game_data.areSidesFriends(units[i_enemy].getSide(), game_data.our_side) == false) {
				boolean can_enemy_see_all_us = game_data.side_see_enemies[units[i_enemy].getSide()] == 1;
				if (units[i_enemy].getStatus() == UnitsTable.ST_DEPLOYED) {
					if (units[i_enemy].has_been_seen == false && can_see_all_enemies == false) { // Only hide enemies we haven't already seen
						units[i_enemy].model.setCullHint(CullHint.Always);
					} else if (can_see_all_enemies) {
						units[i_enemy].model.setCullHint(CullHint.Never);
					}

					if (check_our_init_visible_units) {
						if (isUnitAdjToFriendlyUnitHeCanSee(units[i_enemy])) {
							/*if (DEBUG) {
								p("Unit " + units[i_enemy].name + " will NOT opp fire as they are stood adjacent to a friendly unit!");
							}*/
							units[i_enemy].opp_fire_01 = 0;
						}
					}

					// Loop through our units to see who can see who
					for (int j_friend=0 ; j_friend<units.length ; j_friend++) {
						if (units[j_friend].getSide() == game_data.our_side) {
							if (units[j_friend].getStatus() == UnitsTable.ST_DEPLOYED) {
								// Can our unit SEE the enemy unit?
								if (canUnitSeeUnit(units[j_friend], units[i_enemy], true) || can_see_all_enemies) {
									markUnitAsSeen(units[i_enemy], units[j_friend]);
								}
								// Can our unit BE SEEN BY the enemy unit?
								if (canUnitSeeUnit(units[i_enemy], units[j_friend], true) || can_enemy_see_all_us) {
									this.markUnitAsSeen(units[j_friend], units[i_enemy]);
									if (check_our_init_visible_units) {
										units[i_enemy].can_see_at_start.add(units[j_friend]);
									}
								}
							}
						}
					}

					// Loop through our units and see if they should shoot
					if (opp_fire && this.client_mode < AppletMain.APP_WAITING_CONF) { // Don't do opp fire if turn ending!
						if (units[i_enemy].opp_fire_01 != 0) {
							if (game_data.is_snafu == 0 || game_data.opp_fire[units[i_enemy].getSide()] == 1) {
								for (int j_friend=0 ; j_friend<units.length ; j_friend++) {
									if (units[j_friend].getSide() == game_data.our_side) {
										if (units[j_friend].getStatus() == UnitsTable.ST_DEPLOYED) {
											//if (canUnitSeeUnit(units[i_enemy], units[j_friend], true) || can_enemy_see_all_us) {
											if (units[i_enemy].canUnitSee_Cached(units[j_friend])) {
												if (units[j_friend] == unit_that_moved || unit_that_moved == null) {
													// Check they weren't initially visible
													if (units[i_enemy].can_see_at_start.contains(units[j_friend]) == false) {
														// Shot or cc attack?
														if (units[i_enemy].current_item != null && units[i_enemy].current_item.major_type == EquipmentTypesTable.ET_GUN && units[i_enemy].current_item.auto_shot_acc > 0) { // Can it opp fire?
															this.makeOppFireShot(units[i_enemy], units[j_friend]);
															break; // Stop them shooting any more!
														} else if (units[i_enemy].current_item == null || units[i_enemy].current_item.major_type == EquipmentTypesTable.ET_CC_WEAPON) {
															// Close combat attack?
															if (units[i_enemy].model.getDistTo(units[j_friend].model, false) < 2f) {
																if (units[i_enemy].checkAndReduceAPs(this, OPP_CC_COST, false)) {
																	// Close combat!
																	this.closeCombat(units[i_enemy], units[j_friend]);
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		check_our_init_visible_units = false;
	}


	private boolean canUnitSeeEquipment(UnitData unit, EquipmentData eq) {
		if (eq.model instanceof GasCannister) {
			return true; // Always see these
		}
		return (unit.model.canSee(eq.model, unit.getAngle(), false));
	}


	private boolean isUnitAdjToFriendlyUnitHeCanSee(UnitData unit) {
		for (int i_enemy=0 ; i_enemy<units.length ; i_enemy++) {
			if (units[i_enemy] != unit) {
				if (this.game_data.areSidesFriends(units[i_enemy].getSide(), unit.getSide())) {
					if (units[i_enemy].getStatus() == UnitsTable.ST_DEPLOYED) {
						if (units[i_enemy].model.getDistTo(unit.model, false) <= 1.5f) {
							if (unit.model.canSee(units[i_enemy].model, unit.getAngle(), true)) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}


	public void makeOppFireShot(UnitData shooter, UnitData target) {
		// Point us at the target
		targetting_ray.origin = shooter.model.getWorldTranslation();
		targetting_ray.direction = target.model.getWorldTranslation().subtract(shooter.model.getWorldTranslation()).normalize();
		this.shoot(shooter, getSnapShotAccuracy(shooter) - OPP_FIRE_ACC_REDUCTION, shooter.current_item.snap_shot_aps, target.model.getWorldTranslation(), true);
	}


	private boolean canUnitSeeUnit(UnitData friend, UnitData enemy, boolean do_units_block) {
		if (friend.getStatus() == UnitsTable.ST_DEPLOYED) {
			return friend.model.canSee(enemy.model, friend.getAngle(), do_units_block);
		}
		return false;
	}


	private void markUnitAsSeen(UnitData unit, UnitData seen_by) {
		updateVisibleUnitOnServer(unit, seen_by); // Need to call this each time to tell opponents which enemies they can see! 

		if (unit.has_been_seen == false) {
			boolean seen_by_sides[] = {false, false, false, false, false};
			seen_by_sides[seen_by.getSide()] = true;
			this.sendEventToServer_UnitMoved(unit, seen_by_sides);
		}

		unit.has_been_seen = true;
		seen_by.setCanSee(unit);
		if (unit.getSide() != game_data.our_side) {
			unit.model.setCullHint(CullHint.Never);
			if (visible_enemy_icons.contains(unit.icon) == false) {
				visible_enemy_icons.add(unit.icon);
				if (played_enemy_spotted == false) {
					if (this.orig_visible_enemy_icons.contains(unit.icon) == false) {
						this.playSound(seen_by, SpeechPlayer.EV_ENEMY_SPOTTED);
						played_enemy_spotted = true;
					}
				}
			}
		}
	}


	/**
	 * Returns false if looped round
	 * @return
	 */
	public boolean selectNextUnit() {
		System.gc();

		if (game_data.game_status == GamesTable.GS_STARTED) {
			int curr_id = 0;
			if (this.current_unit != null) {
				curr_id = this.current_unit.num;
			}
			for (int i=curr_id+1 ; i<this.units.length ; i++) {
				if (units[i].getSide() == this.game_data.our_side) {
					if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
						this.selectOurUnit(units[i]);
						return true;
					}
				}
			}
			// Start again from the beginning
			for (int i=0 ; i<this.units.length ; i++) {
				if (units[i].getSide() == this.game_data.our_side) {
					if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
						this.selectOurUnit(units[i]);
						return false;
					}
				}
			}
		}
		return false;
	}


	public void selectPrevUnit() {
		if (game_data.game_status == GamesTable.GS_STARTED) {
			int curr_id = 0;
			if (this.current_unit != null) {
				curr_id = this.current_unit.num;
			}
			for (int i=curr_id-1 ; i>=0 ; i--) {
				if (units[i].getSide() == this.game_data.our_side) {
					if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
						this.selectOurUnit(units[i]);
						return;
					}
				}
			}
			for (int i=this.units.length-1 ; i>=0 ; i--) {
				if (units[i].getSide() == this.game_data.our_side) {
					if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
						this.selectOurUnit(units[i]);
						return;
					}
				}
			}
		}
	}


	private void createMapModel(HashSet<AppletMapSquare> changed_squares) {
		if (mapdata == null) {
			throw new RuntimeException("mapdata is null!");
		}

		// Update the model
		if (changed_squares == null) {
			if (map_model != null) {
				this.map_model.removeFromParent();
				this.map_model = null;
				System.gc(); // What the hell, it might save some memory
			}

			if (game_data.map_model_type == AbstractMapModel.BLOCK_WALLS) {
				map_model = new MapModel_Original(this, mapdata, true, changed_squares);
			} else if (game_data.map_model_type == AbstractMapModel.SLIM_WALLS) {
				map_model = new MapModel_SlimWalls(this, mapdata, changed_squares);
			} else { // Default!
				throw new RuntimeException("Unknown map model type:" + game_data.map_model_type);
			}
			this.attachToRootNode(map_model, true);
		} else {
			map_model.loadMapObjects(changed_squares);
		}
	}


	/**
	 * This gets called when a player has definitely confirmed they are ending their turn.
	 * 
	 */
	public void showExplosionsAndEndTurn() {
		this.current_unit = null;

		this.time_since_last_explosion = 0;// Pause for a sec!  //TIME_BETWEEN_EXPLOSIONS; // So we show explosion straight away
		this.client_mode = AppletMain.APP_WAITING_CONF; // Need this first so we know if we can show explosions
		//sfx.playSound(SoundEffects.END_TURN);

		this.addToHUD(StartupNew.strings.getTranslation("Please wait") + "...");
		this.updateMenu(AppletMain.MM_TURN_FINISHED_NO_MENU);

		// Check for units in nerve gas
		for (int i=0 ; i<this.units.length ; i++) {
			if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
				if (units[i].model_type == UnitsTable.MT_ALIEN_TYRANT || units[i].model_type == UnitsTable.MT_QUEEN_ALIEN) {
					continue;
				}
				AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(units[i].map_x, units[i].map_z);
				if (sq != null) {
					if (sq.smoke_type == EquipmentTypesTable.ET_NERVE_GAS) {
						short dam = this.randomizeDamage(units[i].aps);
						units[i].aps = 0; // Stop them getting harmed more and also from doing anything
						units[i].damage(this, dam, UnitData.GetUnitDataFromID(units, sq.smoke_caused_by), UnitDataComms.FOA_NERVE_GAS);
					} else if (sq.smoke_type == EquipmentTypesTable.ET_INCENDIARY_FIRE_GRENADE) {
							short dam = this.randomizeDamage(units[i].aps/2);
							units[i].aps = 0; // Stop them getting harmed more and also from doing anything
							units[i].damage(this, dam, UnitData.GetUnitDataFromID(units, sq.smoke_caused_by), UnitDataComms.FOA_FIRE);
					}
				}
			}
		}


		// Find any grenades that might want to explode
		for (int i=0 ; i<this.equipment.length ; i++) {
			EquipmentData eq = (EquipmentData)this.equipment[i];
			if (eq.destroyed == false) {
				if (eq.primed) {
					if (eq.explode_turns <= 0) {
						if (eq.major_type == EquipmentTypesTable.ET_GRENADE || eq.major_type == EquipmentTypesTable.ET_EXPLOSIVES || eq.major_type == EquipmentTypesTable.ET_SMOKE_GRENADE || eq.major_type == EquipmentTypesTable.ET_NERVE_GAS) {
							// Check the unit holding it hasn't escaped
							try {
								if (eq.getUnitID() > 0 && UnitData.GetUnitDataFromID(units, eq.getUnitID()).getStatus() == UnitsTable.ST_ESCAPED) {
									continue;
								}
							} catch (Exception ex) {
								AppletMain.HandleError(this, ex);
							}
							UnitData caused_by = null;
							if (eq.last_unit_to_touch > 0) {
								caused_by = UnitData.GetUnitDataFromID(units, eq.last_unit_to_touch);
							}
							this.scheduleExplosion(eq, caused_by);
						}
					}
				}
			}
		}
	}


	/**
	 * This should only be called when checking explosions_to_show
	 * @param grenade
	 * @param caused_by_unit
	 */
	private void explodeGrenade(EquipmentData grenade, UnitData caused_by_unit) { // We must pass the causer sep as it may be caused by an explosion of another grenade!
		int mapx, mapz;
		if (grenade.getUnitID() > 0) {
			UnitData unit = UnitData.GetUnitDataFromID(units, grenade.getUnitID());
			mapx = unit.getMapX();
			mapz = unit.getMapZ();
		} else {
			PointByte p = EquipmentData.GetEquipmentMapSquare(mapdata, grenade);
			mapx = p.x;
			mapz = p.y;
		}

		grenade.equipmentDestroyed(this);
		if (grenade.major_type == EquipmentTypesTable.ET_SMOKE_GRENADE || grenade.major_type == EquipmentTypesTable.ET_NERVE_GAS) {
			this.smokeGrenadeExplosion(mapx, mapz, grenade.explosion_rad, grenade.major_type, caused_by_unit);
		} else { // Normal grenade or explosive
			this.normalExplosion(mapx, mapz, grenade.explosion_rad, grenade.explosion_dam, caused_by_unit, grenade.major_type == EquipmentTypesTable.ET_EXPLOSIVES, false);
		}
		if (grenade.model != null) { // Might be being held by a unit, so there is no model!
			grenade.model.removeFromParent();
		}
	}


	private void smokeGrenadeExplosion(int mapx, int mapz, int radius, int eq_type, UnitData caused_by_unit) {
		// These must be here so they get called when it's a rocket launcher
		this.addToHUD(StartupNew.strings.getTranslation("There has been an explosion!"));
		this.sendEventToServer_Explosion(mapx, mapz, radius, eq_type);

		this.showExplosion(mapx, mapz, radius, EquipmentTypesTable.ET_SMOKE_GRENADE);

		EquipmentData eq_data_grenade = new EquipmentData();
		eq_data_grenade.setName("Dummy");
		eq_data_grenade.major_type = eq_type;

		GameObject gren_for_check_can_see = EquipmentModel.Factory(this, eq_data_grenade, mapx, mapz); // Need the model to check canSee()
		this.attachToRootNode(gren_for_check_can_see, true);

		HashSet<AppletMapSquare> changed_squares = new HashSet<AppletMapSquare>(); 
		for (int r=0 ; r<=radius ; r++) {
			checkForSmokeAccess(mapx, mapz, r, radius, gren_for_check_can_see, eq_data_grenade, caused_by_unit.unitid, changed_squares);
		}
		this.createMapModel(changed_squares); // To show smoke

		gren_for_check_can_see.removeFromParent();
		rootNode.updateGeometricState(0, true);
	}


	/**
	 * Try not to call this directly, call ScheduleExplosion
	 * 
	 */
	private void normalExplosion(int mapx, int mapz, int radius, int damage, UnitData caused_by_unit, boolean explosives, boolean alien_acid) {
		// These must be here so they get called when it's a rocket launcher
		this.addToHUD(StartupNew.strings.getTranslation("There has been an explosion!"));
		this.sendEventToServer_Explosion(mapx, mapz, radius, EquipmentTypesTable.ET_GRENADE);

		this.showExplosion(mapx, mapz, radius, EquipmentTypesTable.ET_GRENADE);

		EquipmentData eq_data_grenade = new EquipmentData();
		eq_data_grenade.setName("Dummy");
		eq_data_grenade.major_type = EquipmentTypesTable.ET_GRENADE;

		GameObject gren_for_check_can_see = EquipmentModel.Factory(this, eq_data_grenade, mapx, mapz); // Need the model to check canSee()
		this.attachToRootNode(gren_for_check_can_see, true);

		Class ignore_klasses[] = {SmokeGrenCloud.class, NerveGasCloud.class};

		// Loop through units and see if they are hit.
		// We do this first
		for (int i=0 ; i<this.units.length ; i++) {
			if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
				if (units[i].model_type == UnitsTable.MT_ALIEN_TYRANT || units[i].model_type == UnitsTable.MT_QUEEN_ALIEN) {
					if (alien_acid) {
						continue;
					}
				}

				float dist = gren_for_check_can_see.getDistTo(units[i].model, false);
				if (dist <= radius) {
					int block_count = gren_for_check_can_see.getCanSeeCount(units[i].model, -1, false, ignore_klasses, 2);
					if (explosives || block_count == 0 || (block_count <= 1 && this.game_data.wall_type != AbstractMission.INDESTRUCTABLE_WALLS)) {
						p("------------------------");
						AppletMain.p("Unit " + units[i].name + " caught in explosion.");
						AppletMain.p("Initial damage: " + damage);
						short dam = this.randomizeDamage(damage);
						// Adjust damage because wall is in the way?
						if (explosives == false && block_count == 1 && this.game_data.wall_type == AbstractMission.STRONG_WALLS) {
							AppletMain.p("Unit " + units[i].name + " is protected by a wall");
							dam = (short)(dam / 2);
						}
						if (dist >= 1) { // we're not standing on it/holding it! 
							int x = units[i].map_x - mapx;
							int y = units[i].map_z - mapz;
							int ang = Geometry.NormalizeAngle(Geometry.GetAngleFromDirection(x, y));
							float angle_mult = this.getAdjustmentForAngle(ang, units[i].angle);
							//angle_mult = 1;
							dam -= (int)((float)units[i].protection * angle_mult);
						} else {
							AppletMain.p("Unit is on top of grenade!");
						}
						AppletMain.p("Final damage: " + dam);
						units[i].damage(this, dam, caused_by_unit, UnitDataComms.FOA_EXPLOSION);
					}
				}
			}
		}

		// Loop through equipment and see if they are hit - must be after we've checked units as otherwise the equip won't have a model.
		for (int i=0 ; i<this.equipment.length ; i++) {
			if (equipment[i].destroyed == false && equipment[i].getUnitID() <= 0 && equipment[i].indestructable != 1 && equipment[i].model != null) { 
				float dist = gren_for_check_can_see.getDistTo(equipment[i].model, false);
				if (dist <= radius) {
					if (explosives || gren_for_check_can_see.canSee(equipment[i].model, -1, false, ignore_klasses, 0)) {
						int rnd = Functions.rnd(1, 100);
						if (equipment[i].major_type == EquipmentTypesTable.ET_GRENADE || equipment[i].major_type == EquipmentTypesTable.ET_EXPLOSIVES || equipment[i].major_type == EquipmentTypesTable.ET_GAS_CANNISTER || equipment[i].major_type == EquipmentTypesTable.ET_SMOKE_GRENADE || equipment[i].major_type == EquipmentTypesTable.ET_NERVE_GAS) {
							AppletMain.p("Chain reaction: " + rnd);
							if (rnd <= 50) { // Only 50% chance of exploding
								// This grenade is caused by the same unit as the original explosion, not the unit holding it.
								this.scheduleExplosion(equipment[i], caused_by_unit);
							} else {
								//NO!  equipment[i].equipmentDestroyed(this);
							}
						} else {
							if (rnd <= 50) { // Only 50% chance of being destroyed
								equipment[i].equipmentDestroyed(this);
							}
						}
					}
				}
			}
		}

		// Check for destroyed walls - AFTER WE'VE CHECKED FOR DESTROYED UNITS!
		boolean any_walls_destroyed = false;
		int side = -1;
		if (caused_by_unit != null) {
			side = caused_by_unit.getSide();
		}

		HashSet<AppletMapSquare> changed_squares = new HashSet<AppletMapSquare>(); 
		//if ((this.game_data.wall_type != AbstractMission.INDESTRUCTABLE_WALLS || explosives) && alien_acid == false) {
		if ((this.game_data.wall_type == AbstractMission.WEAK_WALLS || explosives) && alien_acid == false) {			
			any_walls_destroyed = checkForWallDamage_WEAK_WALLS(mapx, mapz, radius, eq_data_grenade, side, changed_squares);
		} else if (this.game_data.wall_type == AbstractMission.STRONG_WALLS) {			
			any_walls_destroyed = checkForWallDamage_STRONG_WALLS(mapx, mapz, radius, gren_for_check_can_see, eq_data_grenade, side, changed_squares);
		} else if (this.game_data.wall_type == AbstractMission.INDESTRUCTABLE_WALLS || alien_acid) {			
			// Check for destroyed doors etc...
			for (int i=0 ; i<=radius ; i++) {
				boolean any_dam = checkForWallDamage_SOLID_WALLS(mapx, mapz, i, gren_for_check_can_see, eq_data_grenade, side, changed_squares);
				any_walls_destroyed = any_walls_destroyed || any_dam;
			}
		}

		if (any_walls_destroyed) {
			this.createMapModel(changed_squares);
		}

		gren_for_check_can_see.removeFromParent();
		rootNode.updateGeometricState(0, true);
	}


	private boolean checkForWallDamage_WEAK_WALLS(int mapx, int mapz, int this_rad, EquipmentData eq, int caused_by_side, HashSet<AppletMapSquare> changed_squares) {
		/*GameObject obj_can_see = EquipmentModel.Factory(this, eq, 0, 0); // Need the model to check canSee()
		obj_can_see.setIsCollidable(true); // Need this so we can see it!
		obj_can_see.collider.setIsCollidable(true); // Need this so we can see it!
		this.attachToRootNode(obj_can_see, true);*/

		boolean any_walls_destroyed = false;

		for (int z=mapz-this_rad ; z<=mapz+this_rad ; z++) {
			for (int x=mapx-this_rad ; x<=mapx+this_rad ; x++) {

				AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(x, z) ;

				/*if (x == 11 && z == 22) {
					int drfgdfg = 45646;
				}*/

				if (sq != null) {
					double dist = Geometry.distance(x, z, mapx, mapz);
					if (dist <= this_rad) {
						boolean is_this_destroyed = false;

						/*if (explosives == false && this.game_data.wall_type == AbstractMission.STRONG_WALLS) {
							obj_can_see.setLocalTranslation(x+0.5f, 0f, z+0.5f);
							obj_can_see.updateGeometricState(0, true);
							if (gren.canSee(obj_can_see, -1, false, null, 0) == false) {
								continue;
							}
						}*/

						if (sq.major_type == MapDataTable.MT_WALL) {
							is_this_destroyed = true;
						} else if (sq.major_type == MapDataTable.MT_FLOOR) {
							if (sq.door_type > 0) {
								sq.door_type = -1;
							}
							is_this_destroyed = true; // Destroy scenery as well
						} else if (sq.major_type == MapDataTable.MT_COMPUTER && sq.destroyed == 0) {
							is_this_destroyed = true;
							this.addToHUD("Computer destroyed!");
						}

						// Update server if any squares were destroyed
						if (is_this_destroyed) {
							sq.major_type = MapDataTable.MT_FLOOR;
							sq.texture_code = TextureStateCache.TEX_RUBBLE;
							sq.destroyed = 1;
							this.updateMapOnServer(sq, caused_by_side);
							any_walls_destroyed = true;

							// Mark the 3x3 surrounding squares as changed
							for (int z2=z-1 ; z2<=z+1 ; z2++) {
								for (int x2=x-1 ; x2<=x+1 ; x2++) {
									AppletMapSquare sq2 = this.mapdata.getSq_MaybeNULL(x2, z2) ;
									if (sq2 != null) {
										changed_squares.add(sq2);
									}
								}
							}
						}
					}

				}
			}
		}
		return any_walls_destroyed;
	}


	private boolean checkForWallDamage_STRONG_WALLS(int epicentre_mapx, int epicentre_mapz, int this_rad, GameObject gren, EquipmentData eq, int caused_by_side, HashSet<AppletMapSquare> changed_squares) {
		GameObject obj_can_see = EquipmentModel.Factory(this, eq, 0, 0); // Need the model to check canSee()
		obj_can_see.setIsCollidable(true); // Need this so we can see it!
		obj_can_see.collider.setIsCollidable(true); // Need this so we can see it!
		this.attachToRootNode(obj_can_see, true);

		boolean any_walls_destroyed = false;

		Class req_klasses[] = {SlimWall.class, ThickWall.class, BigWall.class, SlidingDoor.class};

		for (int z=epicentre_mapz-this_rad ; z<=epicentre_mapz+this_rad ; z++) {
			for (int x=epicentre_mapx-this_rad ; x<=epicentre_mapx+this_rad ; x++) {

				if (z == epicentre_mapz-this_rad || z == epicentre_mapz+this_rad || x == epicentre_mapx-this_rad || x == epicentre_mapx+this_rad) {

					/*if (x == 11 && z == 22) {
						int drfgdfg = 45646;
					}*/

					boolean is_this_destroyed = false;

					//if (explosives == false && this.game_data.wall_type == AbstractMission.STRONG_WALLS) {
					obj_can_see.setLocalTranslation(x+0.5f, 0f, z+0.5f);
					obj_can_see.updateGeometricState(0, true);
					GameObject wall = gren.getFirstObjectInWay(obj_can_see, req_klasses); 
					if (wall != null) {
						//if (wall instanceof SlimWall || wall instanceof ThickWall || wall instanceof BigWall || wall instanceof SlidingDoor) {
						int ox = (int)(wall.getWorldTranslation().x - 0.5f);
						int oz = (int)(wall.getWorldTranslation().z - 0.5f);
						double dist = Geometry.distance(ox, oz, epicentre_mapx, epicentre_mapz);
						if (dist <= this_rad) {
							AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(ox, oz);
							if (sq != null) {
								if (sq.major_type == MapDataTable.MT_WALL) {
									is_this_destroyed = true;
								} else if (sq.major_type == MapDataTable.MT_FLOOR) {
									if (sq.door_type > 0) {
										sq.door_type = -1;
									}
									is_this_destroyed = true; // Destroy scenery as well
								} else if (sq.major_type == MapDataTable.MT_COMPUTER && sq.destroyed == 0) {
									is_this_destroyed = true;
									this.addToHUD("Computer destroyed!");
								}

								// Update server if any squares were destroyed
								if (is_this_destroyed) {
									sq.major_type = MapDataTable.MT_FLOOR;
									sq.texture_code = TextureStateCache.TEX_RUBBLE;
									sq.destroyed = 1;
									this.updateMapOnServer(sq, caused_by_side);
									any_walls_destroyed = true;

									// Mark the 3x3 surrounding squares as changed
									for (int z2=oz-1 ; z2<=oz+1 ; z2++) {
										for (int x2=ox-1 ; x2<=ox+1 ; x2++) {
											AppletMapSquare sq2 = this.mapdata.getSq_MaybeNULL(x2, z2) ;
											if (sq2 != null) {
												changed_squares.add(sq2);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		obj_can_see.removeFromParent();

		return any_walls_destroyed;
	}


	private boolean checkForWallDamage_SOLID_WALLS(int mapx, int mapz, int this_rad, GameObject gren, EquipmentData eq, int caused_by_side, HashSet<AppletMapSquare> changed_squares) {
		GameObject obj_can_see = EquipmentModel.Factory(this, eq, 0, 0); // Need the model to check canSee()
		obj_can_see.setIsCollidable(true); // Need this so we can see it!
		obj_can_see.collider.setIsCollidable(true); // Need this so we can see it!
		this.attachToRootNode(obj_can_see, true);

		boolean any_walls_destroyed = false;
		for (int z=mapz-this_rad ; z<=mapz+this_rad ; z++) {
			for (int x=mapx-this_rad ; x<=mapx+this_rad ; x++) {

				if (x == mapx-this_rad || x == mapx+this_rad || z == mapz-this_rad || z == mapz+this_rad) { // Only check the current radius
					AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(x, z);
					if (sq != null) {
						boolean is_this_destroyed = false;
						Class[] ignore_class = {ComputerModel.class};
						obj_can_see.setLocalTranslation(x+0.5f, 0f, z+0.5f);
						obj_can_see.updateGeometricState(0, true);
						if (gren.canSee(obj_can_see, -1, false, ignore_class, 0)) {
							if (sq.major_type == MapDataTable.MT_NOTHING || sq.major_type == MapDataTable.MT_WALL) {
								// Do nothing as walls are not destroyable
							} else if (sq.major_type == MapDataTable.MT_FLOOR) {
								if (sq.door_type > 0) {
									sq.door_type = -1;
									is_this_destroyed = true;
									sq.texture_code = TextureStateCache.TEX_RUBBLE;
								}
							} else if (sq.major_type == MapDataTable.MT_COMPUTER && sq.destroyed == 0) {
								sq.destroyed = 1;
								is_this_destroyed = true;
								this.addToHUD("Computer destroyed!");
							}
							// Now update the server if anything has changed
							if (is_this_destroyed) { 
								this.updateMapOnServer(sq, caused_by_side);
								any_walls_destroyed = true;
								changed_squares.add(sq);
							}
						}
					}
				}
			}
		}
		obj_can_see.removeFromParent();
		return any_walls_destroyed;
	}


	private void checkForSmokeAccess(int mapx, int mapz, int this_rad, int max_rad, GameObject gren, EquipmentData eq, int caused_by_unitid, HashSet<AppletMapSquare> changed_squares) {
		GameObject obj_can_see = EquipmentModel.Factory(this, eq, 0, 0); // Need the model to check canSee()
		obj_can_see.setIsCollidable(true); // Need this so we can see it!
		obj_can_see.collider.setIsCollidable(true); // Need this so we can see it!
		this.attachToRootNode(obj_can_see, true);

		for (int z=mapz-this_rad ; z<=mapz+this_rad ; z++) {
			for (int x=mapx-this_rad ; x<=mapx+this_rad ; x++) {
				if (x == mapx-this_rad || x == mapx+this_rad || z == mapz-this_rad || z == mapz+this_rad) { // Only check the current radius
					if (Geometry.distance(mapx, mapz, x, z) <= max_rad) { // Ensure a circle of smoke
						AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(x, z); // Check we're on the map!
						if (sq != null) {
							obj_can_see.setLocalTranslation(x+0.5f, 0f, z+0.5f);
							obj_can_see.updateGeometricState(0, true);
							Class[] ignore_class = {SmokeGrenCloud.class, NerveGasCloud.class};
							if (gren.canSee(obj_can_see, -1, false, ignore_class, 0)) {
								if (sq.major_type == MapDataTable.MT_FLOOR) {
									sq.smoke_type = eq.major_type;
									sq.smoke_caused_by = caused_by_unitid;
									this.updateMapOnServer(sq, 0);
									changed_squares.add(sq);
								}
							}
						}
					}
				}
			}
		}
		obj_can_see.removeFromParent();
	}


	/**
	 * Only call this if it's the end of the turn!  NO!  What about death grenades?
	 * @param mapx
	 * @param rad
	 */
	public void showExplosion(int mapx, int mapz, float rad, int gren_type) {
		//if (StartupNew.hi_mem) {
		//p("Showing explosion");
		if (this.client_mode >= AppletMain.APP_WAITING_CONF) {
			// Stop the camera moving back onto the unit
			this.current_unit = null;
			//this.updateMenu(AppletMain.MM_NO_UNIT_SELECTED);
			this.updateMenu(AppletMain.MM_TURN_FINISHED_NO_MENU);

			// Look at the explosion
			cam_height = CAM_START_HEIGHT*2;
			this.cam.setLocation(new Vector3f(mapx-4f, cam_height, mapz-4f));
		}
		if (playback_controller == null) {
			this.cam.lookAt(new Vector3f(mapx, 0f, mapz), Vector3f.UNIT_Y);
			this.cam.update();
		}

		System.gc();
		new Explosion(this, mapx, 0f, mapz, rad, gren_type);
		//}
		System.gc();
	}


	public void updateMenu() {
		if (this.current_unit != null) {
			this.updateMenu(this.menu_mode);
		} else { // Might have escaped so we no longer have a current unit!
			this.updateMenu(AppletMain.MM_NO_UNIT_SELECTED);
		}
	}


	private void updateMenu(byte menu) {
		this.menu_mode = menu;
		icon_hud.clear();

		if (playback_controller == null && this.game_data.ai_for_side[this.game_data.our_side] <= 0) {
			if (this.menu_mode != AppletMain.MM_TURN_FINISHED_NO_MENU) {
				if (this.menu_mode == AppletMain.MM_DEPLOYING_UNITS) {
					// Do nothing
				} else {
					if (this.our_unit_icons.size() > 1) {
						icon_hud.add(this.next_unit_icon);
						icon_hud.add(this.prev_unit_icon);
					}

					if (this.menu_mode == AppletMain.MM_NO_UNIT_SELECTED) {
						if (this.game_data.turn_side == this.game_data.our_side) { // Just in case it's not!
							icon_hud.addAll(this.our_unit_icons);
							icon_hud.add(this.end_turn_icon);
							//icon_hud.add(this.choose_game_icon);
						}
					} else if (this.menu_mode == AppletMain.MM_UNIT_SELECTED) {
						if (this.current_unit != null) {
							if (this.current_unit.can_use_equipment) { 
								EquipmentData eq = this.current_unit.current_item;
								if (this.current_unit.items.size() > 0) {
									if (eq == null) {
										icon_hud.add(equip_unit_icon);
									} else {
										if (this.current_unit.items.size() > 1) {
											icon_hud.add(change_item_icon);
										}
									}
								}
								AppletMapSquare sq = this.current_unit.model.getSquareInFrontOfUnit_MaybeNULL();
								if (sq != null) { // Off the edge of the map!
									if (sq.door_type > 0) {
										if (sq.door_open) {
											icon_hud.add(close_door_icon);
										} else {
											icon_hud.add(open_door_icon);
										}
									}
								}

								sq =  mapdata.getSq_MaybeNULL(this.current_unit.getMapX(), this.current_unit.getMapZ());
								if (sq != null) { // Off the edge of the map!
									if (sq.escape_hatch_side == this.current_unit.getSide()) {
										icon_hud.add(escape_icon);
									}
								}

								ArrayList<EquipmentData> al = mapdata.getSq_MaybeNULL(this.current_unit.getMapX(), this.current_unit.getMapZ()).getEquipment();
								if (al != null) {
									if (al.size() > 0) {
										if (this.current_unit.current_item == null) {
											pickup_item_icon.setText(StartupNew.strings.getTranslation("Pick-up") + " (" + APS_PICKUP_ITEM + ")");
										} else {
											pickup_item_icon.setText(StartupNew.strings.getTranslation("Pick-up") + " (" + (APS_PICKUP_ITEM*2) + ")");
										}
										icon_hud.add(pickup_item_icon);
									}
								}
								if (eq != null) {
									icon_hud.add(this.throw_menu_icon); // Move so this is "after" equipping a unit
									icon_hud.add(remove_item_icon);
									icon_hud.add(drop_item_icon);
									if (eq.major_type == EquipmentTypesTable.ET_GUN) {
										if (eq.getAmmo() > 0) {
											icon_hud.add(select_shot_type_icon);
										}
										if (eq.getAmmo() < eq.ammo_capacity) {
											reload_icon  = new CommandIcon(this, StartupNew.strings.getTranslation("Reload") + " (" + eq.reload_cost + ")", CommandIcon.RELOAD, CommandIcon.green);
											icon_hud.add(reload_icon);
										}
									} else if (eq.major_type == EquipmentTypesTable.ET_GRENADE || eq.major_type == EquipmentTypesTable.ET_SMOKE_GRENADE || eq.major_type == EquipmentTypesTable.ET_NERVE_GAS) {
										if (eq.primed == false) {
											icon_hud.add(this.show_prime_menu_icon);
										}
									} else if (eq.major_type == EquipmentTypesTable.ET_DEATH_GRENADE || eq.major_type == EquipmentTypesTable.ET_PORTA_PORTER_LANDER) {
										if (eq.primed == false) {
											icon_hud.add(this.activate_menu_icon);
										}
									} else if (eq.major_type == EquipmentTypesTable.ET_MEDIKIT) {
										icon_hud.add(this.use_medikit_icon);
									} else if (eq.major_type == EquipmentTypesTable.ET_SCANNER) {
										icon_hud.add(this.use_scanner_icon);
										/*} else if (eq.major_type == EquipmentTypesTable.ET_PORTA_PORTER_TRIGGER) {
										icon_hud.add(this.use_porta_porter);
									} else if (eq.major_type == EquipmentTypesTable.ET_ADRENALIN_SHOT) {
										icon_hud.add(this.use_adrenalin_shot);*/
									}
								}
							}
							if (this.current_unit.model_type == UnitsTable.MT_BLOB) {
								icon_hud.add(this.absorb_icon);
								icon_hud.add(this.split_icon);
								icon_hud.add(this.explode_icon);
							}
						} else {
							this.updateMenu(AppletMain.MM_NO_UNIT_SELECTED);
							return;
						}
					} else if (this.menu_mode == AppletMain.MM_CHANGE_ITEM) {
						EquipmentData equip;
						for (int i=0 ; i<this.equipment.length ; i++) {
							equip = this.equipment[i];
							if (equip.getUnitID() == this.current_unit.unitid) {
								this.icon_hud.add(new EquipmentIcon(this, equip, EquipmentIcon.CHANGE));
							}
						}
					} else if (this.menu_mode == AppletMain.MM_PICKUP_ITEM) {
						EquipmentData equip;
						ArrayList<EquipmentData> al = mapdata.getSq_MaybeNULL(this.current_unit.getMapX(), this.current_unit.getMapZ()).getEquipment();
						if (al != null) {
							for (int i=0 ; i<al.size() ; i++) {
								equip = al.get(i);
								this.icon_hud.add(new EquipmentIcon(this, equip, EquipmentIcon.PICKUP));
							}
						}
					} else if (this.menu_mode == AppletMain.MM_SHOOTING) {
						aimed_shot_icon.setPcent(AppletMain.getAimedShotAccuracy(this.current_unit));
						snap_shot_icon.setPcent(AppletMain.getSnapShotAccuracy(this.current_unit));
						auto_shot_icon.setPcent(AppletMain.getAutoShotAccuracy(this.current_unit));

						aimed_shot_icon.setAPCost(this.current_unit.current_item.aimed_shot_aps);
						snap_shot_icon.setAPCost(this.current_unit.current_item.snap_shot_aps);
						auto_shot_icon.setAPCost(this.current_unit.current_item.auto_shot_aps);

						icon_hud.add(aimed_shot_icon);
						if (this.current_unit.current_item.snap_shot_acc > 0) {
							icon_hud.add(snap_shot_icon);
						}
						if (this.current_unit.current_item.auto_shot_acc > 0) {
							icon_hud.add(auto_shot_icon);
						}
						icon_hud.add(auto_aim_icon);
					} else if (this.menu_mode == AppletMain.MM_THROW) {
						icon_hud.add(this.less_icon);
						icon_hud.add(this.more_icon);
						icon_hud.add(this.throw_icon);
						/*} else if (this.menu_mode == AppletMain.MM_SELECT_OPP_FIRE_TYPE) {
						icon_hud.add(this.opp_fire_aimed_icon);
						icon_hud.add(this.opp_fire_snap_icon);
						icon_hud.add(this.opp_fire_auto_icon);*/
					} else if (this.menu_mode == AppletMain.MM_PRIME_GRENADE) {
						icon_hud.add(this.less_icon);
						icon_hud.add(this.more_icon);
						if (this.current_unit.getAPs() < (APS_PRIME + APS_THROW)) {
							icon_hud.add(this.warning_icon);
							this.addToHUD(StartupNew.strings.getTranslation("Warning - unit will not have enough AP's to throw this turn!"), true);
						}
						icon_hud.add(this.do_prime_icon);
					} else if (this.menu_mode == AppletMain.MM_CONFIRM_END_TURN) {
						if (isAnyFriendlyUnitHoldingPrimedGrenade()) {
							icon_hud.add(this.warning_icon);
							this.addToHUD(StartupNew.strings.getTranslation("Warning - at least one of your units is holding a deadly grenade!"), true);
						}
						icon_hud.add(this.confirm_end_turn_icon);
					} else {
						throw new RuntimeException("Unknown menu mode: " + this.menu_mode);
					}
				}
				if (this.menu_mode != AppletMain.MM_NO_UNIT_SELECTED && this.menu_mode != AppletMain.MM_DEPLOYING_UNITS) {
					icon_hud.add(cancel_icon);
				}
				icon_hud.add(scanner_icon);
				icon_hud.add(tutorial_icon);

				// Add any visible enemies
				icon_hud.addAll(this.visible_enemy_icons);
			}
		}
		// Remove targetting line if applic
		if (this.menu_mode != AppletMain.MM_SHOOTING && this.menu_mode != AppletMain.MM_THROW) {
			if (shot_line != null) {
				shot_line.removeFromParent();
				shot_line = null;
			}
		}
		this.hud.markForRefresh();
	}


	private boolean isAnyFriendlyUnitHoldingPrimedGrenade() {
		for (int i=0 ; i<this.units.length ; i++) {
			if (units[i].getSide() == this.game_data.our_side) {
				if (units[i].current_item != null) {
					EquipmentData eq = units[i].current_item;
					if (eq.major_type == EquipmentTypesTable.ET_GRENADE || eq.major_type == EquipmentTypesTable.ET_SMOKE_GRENADE || eq.major_type == EquipmentTypesTable.ET_NERVE_GAS) {
						if (eq.primed) {
							if (eq.explode_turns < this.game_data.num_players) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}


	public void updateUnitOnServer(UnitData unit) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Updating unit " + unit.name + " data on server.");
		}
		String req = UnitDataComms.GetUnitUpdateRequest(unit);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void updateVisibleUnitOnServer(UnitData enemy, UnitData seen_by) { 
		if (SharedStatics.VERBOSE_COMMS) {
			p("Sending visible enemy " + enemy.name + " seen by " + seen_by + " data to server.");
		}
		String req = VisibleEnemyComms.GetVisibleEnemyRequest(enemy, seen_by);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void sendEventToServer_UnitMoved(UnitData unit, boolean seen_by_sides[]) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Sending unit " + unit.name + " moved event to server.");
		}
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_UNIT_MOVEMENT, unit.unitid, unit.map_x, unit.map_z, 0, 0, seen_by_sides, 0, 0);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void sendEventToServer_Explosion(int x, int z, int rad, int type) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Sending explosion event to server.");
		}
		boolean seen_by_sides[] = {true, true, true, true}; // All sides get informed
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_EXPLOSION, -1, x, z, rad, type, seen_by_sides, 0, 0);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void sendEventToServer_ShotFired(UnitData unit, int ang, int len, boolean seen_by_sides[]) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Sending shot_fired event to server.");
		}
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_SHOT_FIRED, unit.unitid, unit.getMapX(), unit.getMapZ(), ang, len, seen_by_sides, 0, 0);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void sendEventToServer_GrenadePrimed(UnitData unit, int turns, boolean seen_by_sides[]) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Sending grenade primed event to server");
		}
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_GRENADE_PRIMED, unit.unitid, unit.getMapX(), unit.getMapZ(), turns, -1, seen_by_sides, 0, 0);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void sendEventToServer_CloseCombat(UnitData shooter, boolean seen_by_side[]) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Sending close_combat event to server.");
		}
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_CLOSE_COMBAT, shooter.unitid, shooter.getMapX(), shooter.getMapZ(), 0, 0, seen_by_side, 0, 0);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void sendEventToServer_ItemThrown(UnitData shooter, int ang, int dist, boolean seen_by_side[]) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Sending item_thrown event to server.");
		}
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_ITEM_THROWN, shooter.unitid, shooter.getMapX(), shooter.getMapZ(), ang, dist, seen_by_side, 0, 0);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void sendEventToServer_ItemDropped(UnitData shooter, boolean seen_by_side[]) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Sending item_dropped event to server.");
		}
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_ITEM_DROPPED, shooter.unitid, shooter.getMapX(), shooter.getMapZ(), 0, 0, seen_by_side, 0, 0);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void sendEventToServer_ItemPickedUp(UnitData unit, boolean seen_by_side[]) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Sending item_picked_up event to server.");
		}
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_ITEM_PICKED_UP, unit.unitid, unit.getMapX(), unit.getMapZ(), 0, 0, seen_by_side, 0, 0);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void sendEventToServer_UnitKilled(UnitData unit_killed, int attacked_by_side, int attacked_by_unitid, int form_of_attack, int amt, boolean seen_by_side[]) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Sending unit_killed event to server.");
		}
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_UNIT_KILLED, unit_killed.unitid, unit_killed.getMapX(), unit_killed.getMapZ(), attacked_by_side, attacked_by_unitid, seen_by_side, form_of_attack, amt);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void sendEventToServer_BlobSplit(UnitData unit, boolean seen_by_side[], int x, int z) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Sending blob split event to server.");
		}
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_BLOB_SPLIT, unit.unitid, unit.getMapX(), unit.getMapZ(), x, z, seen_by_side, 0, 0);
		StartupNew.update_server_thread.addRequest(req);
	}




	public void sendEventToServer_UnitEscaped(UnitData unit_killed, boolean seen_by_side[]) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Sending unit_escaped event to server.");
		}
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_UNIT_ESCAPED, unit_killed.unitid, unit_killed.getMapX(), unit_killed.getMapZ(), 0, 0, seen_by_side, 0, 0);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void sendEventToServer_UnitWounded(UnitData unit_killed, int attacked_by_side, int attacked_by_unitid, int form_of_attack, int amt, boolean seen_by_side[]) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Sending unit_wounded event to server.");
		}
		String req = EventDataComms.GetNewEventRequest(game_data.game_id, game_data.gamecode, UnitHistoryTable.UH_UNIT_WOUNDED, unit_killed.unitid, unit_killed.getMapX(), unit_killed.getMapZ(), attacked_by_side, attacked_by_unitid, seen_by_side, form_of_attack, amt);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void updateGameDataOnServer(int type) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Updating game data on server.");
		}
		String req = GameDataComms.GetGameUpdateRequest(this, game_data.game_id, game_data.gamecode, game_data.our_side, type);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void updateMapOnServer(AppletMapSquare sq, int destroyed_by_side) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Updating map square on server.");
		}

		String req = MapDataComms.GetMapDataUpdateRequest(this.game_data.game_id, this.game_data.gamecode, sq, destroyed_by_side);
		StartupNew.update_server_thread.addRequest(req);
	}


	public void selectOurUnit(UnitData unit) {
		if (unit != this.current_unit) {
			if (this.game_data.turn_side == this.game_data.our_side) { // Just in case it's not!
				if (unit.getSide() == this.game_data.our_side) {
					if (unit.getStatus() == UnitsTable.ST_DEPLOYED) {
						sfx.playSound(unit, SpeechPlayer.EV_UNIT_SELECTED);
						current_unit = unit;
						this.hud.clearText();
						this.addToHUD(unit.name + " (" + unit.order_by + ") " + StartupNew.strings.getTranslation("selected."));
						if (unit.can_use_equipment) {
							this.addToHUD(StartupNew.strings.getTranslation("Unit is carrying") + ":-");
							this.addToHUD(getUnitsEquipmentAsString(unit).toString());
							if (AppletMain.help_mode_on) {
								if (this.current_unit.current_item == null) {
									this.addToHUD(StartupNew.strings.getTranslation("HELP") + ": " + StartupNew.strings.getTranslation("Unit is not using any item"));
									this.addToHUD(StartupNew.strings.getTranslation("HELP") + ": " + StartupNew.strings.getTranslation("Select 'Equip Item' to use equipment"));
								}
							}
							if (this.current_unit.current_item != null) {
								if (current_unit.current_item.major_type == EquipmentTypesTable.ET_GRENADE || current_unit.current_item.major_type == EquipmentTypesTable.ET_NERVE_GAS) {
									if (current_unit.current_item.primed) {
										this.addToHUD(StartupNew.strings.getTranslation("WARNING") + ": " + StartupNew.strings.getTranslation("Unit is holding a primed grenade!"), true);
									}
								} else if (current_unit.current_item.major_type == EquipmentTypesTable.ET_GUN) {
									if (current_unit.current_item.getAmmo() <= 0) {
										this.addToHUD(StartupNew.strings.getTranslation("WARNING") + ": " + StartupNew.strings.getTranslation("Unit is out of ammo!"));
									}
									if (unit.aps >= unit.opp_fire_aps_req) {
										if (isUnitAdjToFriendlyUnitHeCanSee(unit)) {
											this.addToHUD(StartupNew.strings.getTranslation("WARNING") + ": " + StartupNew.strings.getTranslation("Unit will not opp-fire as an adjacent friendly unit is visible"), true);
										}
									}
								}
							}
						}
						// Warnings
						if (this.game_data.is_advanced == 1) {
							if (this.current_unit.panicked != 0) {
								this.addToHUD(StartupNew.strings.getTranslation("UNIT HAS PANICKED!"));
							} else if (this.current_unit.curr_morale <= UnitsTable.SCARED_LEVEL) {
								this.addToHUD(StartupNew.strings.getTranslation("WARNING") + ": " + StartupNew.strings.getTranslation("Unit is scared."), true);
							}
							if (this.current_unit.curr_energy <= 0) {
								this.addToHUD(StartupNew.strings.getTranslation("UNIT IS EXHAUSTED!"));
							} else if (this.current_unit.curr_energy < this.current_unit.max_aps) {
								this.addToHUD(StartupNew.strings.getTranslation("WARNING") + ": " + StartupNew.strings.getTranslation("Unit is tired"), true);
							}
						}

						this.updateMenu(AppletMain.MM_UNIT_SELECTED);
						if (highlighter == null) {
							highlighter = new UnitHighlighter(this);
							this.attachToRootNode(highlighter, true);
						}
						highlighter.setLocalTranslation(current_unit.model.getLocalTranslation());
						highlighter.getChild(0).getLocalTranslation().y = (-current_unit.model.getLocalTranslation().y + 0.1f);
						highlighter.setLocalRotation(current_unit.model.getLocalRotation());
						highlighter.updateGeometricState(0, true);
					}
				}
			}
		}
	}


	private String getUnitsEquipmentAsString(UnitData unit) {
		StringBuffer str = new StringBuffer();
		boolean any = false;
		for (int i=0 ; i<this.equipment.length ; i++) {
			EquipmentData equip = this.equipment[i];
			if (equip.getUnitID() == unit.unitid) {
				str.append(equip.getName(true) + ", ");
				any = true;
			}
		}
		if (!any) {
			str.append(StartupNew.strings.getTranslation("Nothing") + ".");
		} else {
			str.delete(str.length()-2, str.length());
		}
		return str.toString();
	}


	public void showChangeUnitsItemMenu() {
		this.updateMenu(MM_CHANGE_ITEM);
	}


	public void showPickupItemMenu() {
		this.updateMenu(MM_PICKUP_ITEM);
	}


	public void dropCurrentItem() {
		// If it's an egg, check there is not already an egg
		EquipmentData eq = this.current_unit.current_item;
		if (eq.major_type == EquipmentTypesTable.ET_EGG) {
			// Check there is enough room
			AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(this.current_unit.map_x, this.current_unit.map_z);
			if (sq.containsType(EquipmentTypesTable.ET_EGG)) {
				this.addToHUD(StartupNew.strings.getTranslation("There is not enough room."));
			}
		}
		if (this.current_unit.checkAndReduceAPs(this, APS_DROP_ITEM, true)) {
			this.current_unit.removeCurrentItem(this);

			this.mapdata.getSq_MaybeNULL(current_unit.getMapX(), current_unit.getMapZ()).addEquipment(eq);

			eq.model = EquipmentModel.Factory(this, eq, current_unit.getMapX(), current_unit.getMapZ());
			this.attachToRootNode(eq.model, true);

			try {
				// Send message
				boolean who_can_see[] = this.whichSidesCanSeeUnit(this.current_unit);
				// Send item dropped event to server
				this.sendEventToServer_ItemDropped(this.current_unit, who_can_see);
			} catch (Exception ex) {
				AppletMain.HandleError(this, ex);
			}
			this.updateEquipmentOnServer(eq, current_unit.getMapX(), current_unit.getMapZ());
			this.updateUnitOnServer(this.current_unit);
			this.updateMenu(MM_UNIT_SELECTED);
		}
	}


	public void removeCurrentItem() {
		if (this.current_unit.checkAndReduceAPs(this, APS_REMOVE_ITEM, true)) {
			this.current_unit.current_item = null;
			this.updateUnitOnServer(this.current_unit);
			this.updateMenu(MM_UNIT_SELECTED);
		}
	}


	public void autoAim() {
		if (this.aiming_at != null) {
			//this.cam.lookAt(aiming_at.getWorldTranslation(), Vector3f.UNIT_Y);
			//cam.update();
			//float ang_diff
			//cam.setl
			Vector3f dir = this.current_unit.model.getWorldTranslation().subtract(this.aiming_at.getWorldTranslation());
			dir.normalizeLocal();
			float cam_dist = (float)Geometry.distance(cam.getLocation().x, cam.getLocation().z, this.current_unit.model.getWorldTranslation().x, this.current_unit.model.getWorldTranslation().z);
			dir.multLocal(cam_dist);
			cam.getLocation().x = this.current_unit.model.getWorldTranslation().x + dir.x;
			cam.getLocation().z = this.current_unit.model.getWorldTranslation().z + dir.z;
			cam.update();

		} else {
			this.addToHUD("No enemy found");
		}
	}


	public void escape() {
		AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(this.current_unit.map_x, this.current_unit.map_z);
		if (sq != null) {
			if (sq.escape_hatch_side == this.current_unit.getSide()) {
				this.current_unit.escaped(this);
			}
		}
	}


	public void pickupEquipment(EquipmentData eq) {
		// Check we can carry it (due to weight)
		if (eq.weight <= this.current_unit.strength) {

			int cost = APS_PICKUP_ITEM;
			// Adjust APs, depends on if they were holding anything already
			if (this.current_unit.current_item != null) {
				cost = cost * 2;
			}
			if (this.current_unit.checkAndReduceAPs(this, cost, true)) {
				try {
					eq.model.removeFromParent(); // error here?
					eq.model = null;
				} catch (Exception ex) {
					AppletMain.HandleError(this, ex);
				}
				eq.setUnitID(this.current_unit.unitid);
				this.mapdata.getSq_MaybeNULL(current_unit.getMapX(), current_unit.getMapZ()).removeEquipment(eq);

				this.current_unit.current_item = eq;
				this.current_unit.items.add(eq);

				// Now NOT seen by any side
				for (int s=1 ; s<=4 ; s++) {
					eq.seen_by_side[s] = 0;
				}

				this.updateMenu(MM_UNIT_SELECTED);
				this.updateEquipmentOnServer(eq, current_unit.getMapX(), current_unit.getMapZ());
				this.updateUnitOnServer(this.current_unit);

				try {
					// Send message
					boolean who_can_see[] = this.whichSidesCanSeeUnit(this.current_unit);
					// send item picked up event to server
					this.sendEventToServer_ItemPickedUp(this.current_unit, who_can_see);
				} catch (Exception ex) {
					AppletMain.HandleError(this, ex);
				}

			}
		} else {
			this.addToHUD(StartupNew.strings.getTranslation("That item is too heavy."));
		}
	}


	public void blobAbsorb() {
		// Check there is a corpse
		boolean corpse_found = false;
		EquipmentData equip;
		ArrayList<EquipmentData> al = mapdata.getSq_MaybeNULL(this.current_unit.getMapX(), this.current_unit.getMapZ()).getEquipment();
		if (al != null) {
			for (int i=0 ; i<al.size() ; i++) {
				equip = al.get(i);
				if (EquipmentTypesTable.CORPSES.contains(equip.major_type)) {
					corpse_found = true;
					if (this.current_unit.checkAndReduceAPs(this, APS_ABSORB, true)) {
						if (equip.model != null) {
							equip.model.removeFromParent();
							equip.model = null;
						}
						equip.destroyed = true;
						this.mapdata.getSq_MaybeNULL(current_unit.getMapX(), current_unit.getMapZ()).removeEquipment(equip);

						// Now NOT seen by any side
						for (int s=1 ; s<=4 ; s++) {
							equip.seen_by_side[s] = 0;
						}

						this.updateEquipmentOnServer(equip, current_unit.getMapX(), current_unit.getMapZ());

						this.current_unit.incHealth((short)15, true);
						this.updateUnitOnServer(this.current_unit);

						/*try {
							// Send message
							boolean who_can_see[] = this.whichSidesCanSeeUnit(this.current_unit);
							// send item picked up event to server
							this.sendEventToServer_BlobAbsorbed(this.current_unit, who_can_see);
						} catch (Exception ex) {
							AppletMain.HandleError(this, ex);
						}*/
						this.updateMenu(MM_UNIT_SELECTED);
						return;
					}
				}
			}
		}/* else {
			this.addToHUD(StartupNew.strings.getTranslation("No corpse found."));
		}*/
		if (corpse_found == false) {
			this.addToHUD(StartupNew.strings.getTranslation("No corpse found."));
		}
	}


	public void splitBlob() {
		if (this.current_unit.getHealth() > 6) {
			// Check there is an empty square
			AppletMapSquare sq = null;
			for (int z=this.current_unit.getMapZ()-1 ; z<=this.current_unit.getMapZ()+1 ; z++) {
				for (int x=this.current_unit.getMapX()-1 ; x<=this.current_unit.getMapX()+1 ; x++) {
					AppletMapSquare sq_tmp = mapdata.getSq_MaybeNULL(x, z);
					if (sq_tmp.major_type == MapDataTable.MT_FLOOR && sq_tmp.door_type <= 0) {
						if (this.getUnitAt(x, z) == null) {
							sq = sq_tmp;
						}
					}
				}
			}
			if (sq != null) {
				if (this.current_unit.checkAndReduceAPs(this, APS_SPLIT, true)) {
					this.current_unit.setHealth((short)(this.current_unit.getHealth()/2));
					this.current_unit.setMaxHealth((short)(this.current_unit.getMaxHealth()/2));
					this.current_unit.combat_skill -= this.current_unit.combat_skill/2;
					this.current_unit.strength -= this.current_unit.strength/2;
					this.updateUnitOnServer(this.current_unit);


					try {
						// Send message
						boolean who_can_see[] = this.whichSidesCanSeeUnit(this.current_unit);
						// send item picked up event to server
						this.sendEventToServer_BlobSplit(this.current_unit, who_can_see, sq.x, sq.y);
					} catch (Exception ex) {
						AppletMain.HandleError(this, ex);
					}

					// Create new unit - must be after we've updated the original unit!
					UnitData unit = this.current_unit.clone();
					//unit.unitid = new_id;
					unit.map_x = sq.x;
					unit.map_z = sq.y;
					unit.aps = 0;

					unit.model = AbstractUnit.Factory(this, unit);
					unit.updateModelFromUnitData();
					unit.model.updateRenderState();
					this.recalcVisibleEnemiesAndOppFire(true, null);

					this.updateMenu(MM_UNIT_SELECTED);
				}
			} else {
				this.addToHUD(StartupNew.strings.getTranslation("There is no room."));
			}
		} else {
			this.addToHUD(StartupNew.strings.getTranslation("Blob is too small to split."));
		}
	}


	public void changeCurrentEquipment(EquipmentData eq) {
		if (this.current_unit.current_item != null) {
			if (eq == this.current_unit.current_item) {
				this.updateMenu(MM_UNIT_SELECTED);
				return; // Do nothing
			}
		}

		int cost = APS_CHANGE_ITEM;
		// Inc cost if they're holding something - NO!  TO EXPENSIVE!
		/*if (this.current_unit.current_item != null) {
			cost = cost * 2;
		}*/
		if (this.current_unit.checkAndReduceAPs(this, cost, true)) {
			UnitData unit = UnitData.GetUnitDataFromID(units, eq.getUnitID());
			unit.current_item = eq;
			this.updateMenu(MM_UNIT_SELECTED);

			this.updateEquipmentOnServer(eq, current_unit.getMapX(), current_unit.getMapZ());

			this.updateUnitOnServer(this.current_unit);
		}
	}


	public void cancelMenu() {
		if (menu_mode == AppletMain.MM_UNIT_SELECTED || menu_mode == AppletMain.MM_CONFIRM_END_TURN) {
			this.current_unit = null;
			this.updateMenu(AppletMain.MM_NO_UNIT_SELECTED);
			this.removeHighlighter();
		} else {
			this.updateMenu(AppletMain.MM_UNIT_SELECTED);
		}
	}


	private void removeHighlighter() {
		if (highlighter != null) {
			highlighter.removeFromParent();
			highlighter = null;
		}
	}


	public void showSelectShotTypeMenu() {
		this.updateMenu(MM_SHOOTING);

		shot_line = new TargettingLine(this);
		attachToRootNode(shot_line, true);

		this.addToHUD(StartupNew.strings.getTranslation("Move the camera left/right to aim the shot."));
	}


	public static byte getAimedShotAccuracy(UnitData unit) {
		float acc = Math.max(MIN_SHOT_ACC, (unit.shot_skill + unit.current_item.aimed_shot_acc));
		return (byte)Math.min(MAX_SHOT_ACC, acc);
	}

	public static byte getSnapShotAccuracy(UnitData unit) { 
		float acc =  Math.max(MIN_SHOT_ACC, (unit.shot_skill/2) + unit.current_item.snap_shot_acc);
		return (byte)Math.min(MAX_SHOT_ACC, acc);
	}


	public static byte getAutoShotAccuracy(UnitData unit) {
		float acc =  Math.max(MIN_SHOT_ACC, (unit.shot_skill/3) + unit.current_item.auto_shot_acc);
		return (byte)Math.min(MAX_SHOT_ACC, acc);
	}


	public void makeAimedShot() {
		this.addToHUD(StartupNew.strings.getTranslation("Attempting aimed shot"));
		this.shoot(this.current_unit, getAimedShotAccuracy(this.current_unit), this.current_unit.current_item.aimed_shot_aps, this.shot_line.getEndpoint(), false);
	}


	public void makeSnapShot() {
		this.addToHUD(StartupNew.strings.getTranslation("Attempting snap shot"));
		this.shoot(this.current_unit, getSnapShotAccuracy(this.current_unit), this.current_unit.current_item.snap_shot_aps, this.shot_line.getEndpoint(), false);
	}


	public void makeAutoShot() {
		this.addToHUD(StartupNew.strings.getTranslation("Attempting autoshot"));
		this.shoot(this.current_unit, getAutoShotAccuracy(this.current_unit), this.current_unit.current_item.auto_shot_aps, this.shot_line.getEndpoint(), false);
	}


	private void shoot(UnitData shooter, int acc, int ap_cost, Vector3f targetpoint, boolean opp_fire) {
		EquipmentData gun = shooter.current_item;
		if (gun != null) {
			if (gun.major_type == EquipmentTypesTable.ET_GUN) {
				if (gun.getAmmo() > 0) {
					// Check they are shooting in the direction they are facing!
					int ang = Geometry.GetAngleOfRayAlongYAxis(this.targetting_ray);
					int diff = Geometry.GetDiffBetweenAngles(ang, shooter.getAngle());
					if (diff < GameObject.VIEW_ANGLE) {

						StartupNew.waitForPendingUpdateRequests(); // This is why opp fire makes things slow!

						if (shooter.checkAndReduceAPs(this, ap_cost, !opp_fire)) {
							this.updateUnitOnServer(shooter);
							sfx.playSound(SoundEffects.LASER);

							if (!SharedStatics.DEBUG) {
								gun.decAmmo();
							}

							this.updateEquipmentOnServer(gun, (byte)-1, (byte)-1);

							MyPickResults results = new MyPickResults(shooter.model.collider);
							results.clear();

							Ray actual_shot_ray = new Ray(targetting_ray.origin.clone(), targetting_ray.direction.clone());
							actual_shot_ray.origin.y = 0.2f; // To ensure we can hit low objects 

							p("------------------------");
							p("Acc: " + acc);
							// Is it an accurate shot?
							float angle_adj = 0;
							int rnd = Functions.rnd(0, 100);
							p("Rnd: " + rnd);
							if (rnd < acc) {
								p("Shot is accurate!");
								angle_adj = Functions.rndFloat(0, 0.4f);
							} else {
								int badness = (rnd - acc);
								if (badness > 30) {
									badness = 30;
								}
								p("Inaccuracy = " + badness);
								angle_adj = Functions.rndFloat(2, 2+badness);
							}

							p("angle adjustment = " + angle_adj);
							if (angle_adj != 0) {
								if (Functions.rnd(0, 1) == 1) {
									angle_adj = angle_adj * -1;
								}
								actual_shot_ray.setDirection(Geometry.AdjustVectorByAmtOnYAxis(actual_shot_ray.getDirection(), angle_adj));
							}

							rootNode.findPick(actual_shot_ray, results);
							boolean bullet_created = false;
							if (results.getNumber() > 0) {
								for (int i=0 ; i<results.getNumber() ; i++) {
									PickData o = results.getPickData(i);

									GameObject target = results.getGameObject(i);
									if (target.can_be_shot) {
										// Check it's not a hedge or something
										if (o.getDistance() >= 1.5f) {
											if (target.isChanceofNotHitting()) {
												int rnd2 = Functions.rnd(1, 100);
												if (rnd2 < target.getChanceofNotHitting()) {
													continue;
												}
											}
										}

										// Launch bullet
										Vector3f vdiff = actual_shot_ray.getDirection().mult(o.getDistance());
										Vector3f end = actual_shot_ray.getOrigin().add(vdiff);
										new Bullet(this, shooter.model.getWorldTranslation(), end, true);
										bullet_created = true;

										boolean seen[] = this.whichSidesCanSeeUnit(shooter);
										this.sendEventToServer_ShotFired(shooter, Geometry.GetAngleFromDirection(actual_shot_ray.direction.x, actual_shot_ray.direction.z), (int)o.getDistance(), seen);

										if (target instanceof AbstractUnit) {
											AbstractUnit unit_target = (AbstractUnit)target;
											if (shooter == this.current_unit) { // Check it's not Opp Fire or something
												this.addToHUD(StartupNew.strings.getTranslation("You have shot") + " " + unit_target.unit_data.name);
											}
											short dam = gun.shot_damage;
											dam = randomizeDamage(dam);
											AppletMain.p("Damage: " + dam);
											float ang_adj = this.getAdjustmentForAngle(Geometry.GetAngleFromDirection(actual_shot_ray.direction.x, actual_shot_ray.direction.z), unit_target.unit_data.getAngle());
											AppletMain.p("Full Protec: " + unit_target.unit_data.protection);
											float protec = (float)unit_target.unit_data.protection * ang_adj;
											AppletMain.p("Protec adjusted for angle: " + protec);
											dam = (short)(dam - protec); //unit_target.unit_data.protection = 10
											//AppletMain.p("Protection: " + unit_target.unit_data.protection);
											int dist = (int)shooter.model.getDistTo(target, false);
											if (dist < 2) {
												dam += POINT_BLANK_DAMAGE_BONUS;
												AppletMain.p("Short-range bonus: " + POINT_BLANK_DAMAGE_BONUS);
											} else if (dist >= MIN_DAMAGE_REDUCTION_RANGE) {
												if (gun.ammo_type_id != 2) { // Lasers don't lose damage
													int max_reduction = dam/2;
													int reduction = (dist - MIN_DAMAGE_REDUCTION_RANGE) / 2;
													if (reduction > max_reduction) {
														reduction = max_reduction;
													}
													dam -= reduction; // dam = 21+19
													AppletMain.p("Range reduction: " + reduction);
												}
											}
											unit_target.unit_data.damage(this, dam, shooter, UnitDataComms.FOA_SHOT);
											//this.updateUnitOnServer(unit_target.unit_data);
										} else if (target instanceof ComputerModel) {
											ComputerModel cpu = (ComputerModel)target;
											this.computerDestroyed(cpu, shooter);
										} else if (target instanceof GasCannister) {
											GasCannister gc = (GasCannister)target;
											this.scheduleExplosion(gc.eq, shooter);
											gc.removeFromParent();
										} else if (target instanceof ThickWall) {
											//AppletMain.p("Bullet hit wall.");
											if (shooter == this.current_unit) { // Check it's not Opp Fire or something
												this.addToHUD(StartupNew.strings.getTranslation("You have shot the wall."));
											}
										} else {
											if (shooter == this.current_unit) { // Check it's not Opp Fire or something
												this.playSound(this.current_unit, SpeechPlayer.EV_SHOT_MISSED);
											}
											// Nothing happens
										}
										if (gun.explodes) {
											Vector3f vdiff2 = actual_shot_ray.getDirection().mult(o.getDistance()-1); // Notice we move a square back to get a better explosion!
											Vector3f end2 = actual_shot_ray.getOrigin().add(vdiff2);
											this.normalExplosion((byte)end2.x, (byte)end2.z, gun.explosion_rad, gun.explosion_dam, shooter, false, false);
											this.recalcVisibleEnemiesAndOppFire(true, null);
										} else {
											this.recalcVisibleEnemiesAndOppFire(true, shooter); // Must be shooter otherwise we could get caught in a loop!
										}
										break;
									}
								}
							} 
							if (bullet_created == false) {//else { // Off the edge of the map
								AppletMain.p("The bullet has gone off the edge of the map.");
								// Launch bullet
								Vector3f vdiff = actual_shot_ray.getDirection().mult(20f);
								Vector3f end = actual_shot_ray.getOrigin().add(vdiff);
								new Bullet(this, shooter.model.getWorldTranslation(), end, false);
								bullet_created = true;
								if (shooter == this.current_unit) { // Check it's not Opp Fire or something
									this.playSound(this.current_unit, SpeechPlayer.EV_SHOT_MISSED);
								}
								boolean seen[] = this.whichSidesCanSeeUnit(shooter);
								this.sendEventToServer_ShotFired(shooter, Geometry.GetAngleFromDirection(actual_shot_ray.direction.x, actual_shot_ray.direction.z), 10, seen);
							}
						} else {
							/*if (DEBUG) {
								p("Not enough APs!");
							}*/
						}
					} else {
						if (shooter == this.current_unit) { // Check it's not Opp Fire or something
							this.addToHUD(StartupNew.strings.getTranslation("Not in field of vision!"));
							if (AppletMain.help_mode_on) {
								this.addToHUD(StartupNew.strings.getTranslation("The white line must be in the same direction as the unit."));
							}
						}
						/*if (DEBUG) {
							p("Angle too far");
						}*/
					}
				} else {
					if (shooter == this.current_unit) { // Check it's not Opp Fire or something
						this.addToHUD(StartupNew.strings.getTranslation("No ammo left!"));
						playSound(shooter, SpeechPlayer.EV_OUT_OF_AMMO);
					}
					/*if (DEBUG) {
						p("Not enough Ammo");
					}*/
				}
			} else {
				/*if (DEBUG) {
					p("No gun");
				}*/
			}
		} else {
			/*if (DEBUG) {
				p("No equipment");
			}*/

		}
	}


	public boolean[] whichSidesCanSeeUnit(UnitData shooter) {
		try {
			boolean side_can_see_shooter[] = new boolean[game_data.num_players+1];
			boolean sides_checked[] = new boolean[game_data.num_players+1];
			game_data.side_sides[shooter.getSide()].setArray(sides_checked, true);
			side_can_see_shooter[shooter.getSide()] = true;
			for (byte i=0 ; i<this.units.length ; i++) {
				// Have we checked this side yet?
				if (sides_checked[units[i].getSide()] == false) {
					if (units[i].getStatus() == UnitsTable.ST_DEPLOYED) {
						if (units[i].canUnitSee_Cached(shooter)) {
							sides_checked[units[i].getSide()] = true;
							side_can_see_shooter[units[i].getSide()] = true;
						}
					}
				}
			}
			return side_can_see_shooter;
		} catch (Exception ex) {
			AppletMain.HandleError(this, ex);
		}
		return null;
	}


	private short randomizeDamage(int dam) {
		return (short)((float)dam * (Functions.rndFloat(.5f, 1.5f)));		
	}


	public void computerDestroyed(ComputerModel cpu, UnitData shooter) {
		if (shooter == this.current_unit) { // Check it's not Opp Fire or something
			this.addToHUD(StartupNew.strings.getTranslation("Computer destroyed!"));
			sfx.playSound(shooter, SpeechPlayer.EV_COMPUTER_DESTROYED);
		}
		AppletMapSquare sq = cpu.getMapSquare();
		if (sq.destroyed != 1) {
			sq.destroyed = 1;
			updateMapOnServer(sq, shooter.getSide());
			cpu.showDamage();
		}
	}


	public void showThrowMenu() {
		this.throw_icon.adjCount((byte)0); // To refresh max distance
		this.less_icon.action = this.throw_icon;
		this.more_icon.action = this.throw_icon;
		this.updateMenu(AppletMain.MM_THROW);

		shot_line = new TargettingLine(this);
		attachToRootNode(shot_line, true);

		this.addToHUD(StartupNew.strings.getTranslation("Move the camera left/right to aim the throw."));
		if (this.current_unit.current_item != null) {
			if (this.current_unit.current_item.major_type == EquipmentTypesTable.ET_GRENADE || this.current_unit.current_item.major_type == EquipmentTypesTable.ET_SMOKE_GRENADE || this.current_unit.current_item.major_type == EquipmentTypesTable.ET_NERVE_GAS) {
				if (this.current_unit.current_item.primed == false) {
					this.addToHUD(StartupNew.strings.getTranslation("WARNING: Grenade not primed!"), true);
				}
			}
		}
	}


	public void throwItem(float selected_dist) {
		StartupNew.waitForPendingUpdateRequests();
		if (this.current_unit.current_item != null) {
			// Check they are shooting in the direction they are facing!
			int ang = Geometry.GetAngleOfRayAlongYAxis(this.targetting_ray);
			int diff = Geometry.GetDiffBetweenAngles(ang, this.current_unit.getAngle());
			if (diff < GameObject.VIEW_ANGLE) {
				if (this.current_unit.checkAndReduceAPs(this, APS_THROW, true)) {
					EquipmentData obj_thrown = this.current_unit.current_item;
					this.current_unit.removeCurrentItem(this);

					MyPickResults results = new MyPickResults(this.current_unit.model.collider);
					results.clear();

					// Adjust by accuracy angle
					int ang_rnd = (100-this.current_unit.combat_skill) / 12;
					int angle = Functions.rnd(-ang_rnd, ang_rnd);
					p("Angle adjusted by " + angle);
					targetting_ray.setDirection(Geometry.AdjustVectorByAmtOnYAxis(targetting_ray.getDirection(), angle));

					// Adjust distance by random amount
					float dist_rnd = (100-this.current_unit.combat_skill) / 220f;
					//selected_dist = (int)((float)selected_dist * Functions.rndFloat(0.8f, 1.2f));
					selected_dist = (selected_dist * Functions.rndFloat(1f-dist_rnd, 1f+dist_rnd));
					p("Dist changed to " + selected_dist);

					int max_dist = this.current_unit.strength - obj_thrown.weight;
					if (selected_dist > max_dist) {
						selected_dist = max_dist;
						p("Dist reduced to " + max_dist);
					}

					boolean object_landed = false;
					rootNode.findPick(targetting_ray, results);
					if (results.getNumber() > 0) {
						for (int i=0 ; i<results.getNumber() ; i++) {
							PickData o = results.getPickData(i);
							// Is the target further than our distance?
							if (Math.floor(o.getDistance()) <= selected_dist) { // has it hit something?
								GameObject target = results.getGameObject(i);
								if (target instanceof AbstractUnit) {
									AbstractUnit catcher = (AbstractUnit)target;
									selected_dist = o.getDistance();
									boolean caught = false;
									if (catcher.unit_data.current_item == null && catcher.unit_data.can_use_equipment) { // Can it catch it?
										// Can the catcher see the thrower?
										if (this.canUnitSeeUnit(catcher.unit_data, this.current_unit, true)) {
											if (catcher.unit_data.checkAndReduceAPs(this, APS_CATCH, true)) {
												catcher.unit_data.current_item = obj_thrown;
												catcher.unit_data.items.add(obj_thrown);
												this.updateUnitOnServer(catcher.unit_data);
												obj_thrown.setUnitID(catcher.unit_data.unitid);
												this.updateEquipmentOnServer(obj_thrown, (byte)-1, (byte)-1);
												this.addToHUD(StartupNew.strings.getTranslation("It has been caught by") + " " + catcher.unit_data.name);
												caught = true;
											}
										}
									} 
									if (caught == false) { // Catcher is unable to catch - Dropped at the feet
										this.addToHUD(StartupNew.strings.getTranslation("It has hit") + " " + catcher.unit_data.name + ".");
										this.putEquipmentOnFloor(obj_thrown, catcher.unit_data.getMapX(), catcher.unit_data.getMapZ());
									}
									object_landed = true;
									break;
								} else if (target.can_be_shot && target.getChanceofNotHitting() <= 0) {
									selected_dist = o.getDistance() - 0.5f; // In case it hits door, move back.  But not too far! (was -1f)
									objectThrownToTheFloor(obj_thrown, selected_dist);
									object_landed = true;
									break;
								}
							} else { // landed in middle of room
								break;
							}

						} // End of loop
					}
					if (object_landed == false) { // None of the collisions were valid
						objectThrownToTheFloor(obj_thrown, selected_dist);

					}

					try {
						// Send message
						boolean who_can_see[] = this.whichSidesCanSeeUnit(this.current_unit);
						this.sendEventToServer_ItemThrown(this.current_unit, ang, (int)selected_dist, who_can_see);
					} catch (Exception ex) {
						AppletMain.HandleError(this, ex);
					}

					this.shot_line.removeFromParent();
					shot_line = null;

					// Launch bullet
					Vector3f vdiff = targetting_ray.getDirection().mult(selected_dist);
					Vector3f end = targetting_ray.getOrigin().add(vdiff);
					new Bullet(this, this.current_unit.model.getWorldTranslation(), end, false);

					this.updateMenu(AppletMain.MM_UNIT_SELECTED);
				} else {
					this.addToHUD(StartupNew.strings.getTranslation("Not enough APs!"));
				}
			} else {
				this.addToHUD(StartupNew.strings.getTranslation("That angle is too far!"));
				if (AppletMain.help_mode_on) {
					this.addToHUD(StartupNew.strings.getTranslation("The white line must be in the same direction as the unit."));
				}
			}
		} else {
			this.addToHUD(StartupNew.strings.getTranslation("Unit not carrying anything!"));
		}
	}


	private void objectThrownToTheFloor(EquipmentData obj_thrown, float selected_dist) {
		byte x, z;
		while (true) {
			Vector3f pos_diff = this.targetting_ray.getDirection().mult(selected_dist);
			Vector3f end_pos = targetting_ray.getOrigin().add(pos_diff);
			x = (byte)end_pos.x;
			z = (byte)end_pos.z;
			AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(x, z);
			if (sq != null) { // In case it's off the edge of the map
				if (sq.major_type == MapDataTable.MT_FLOOR) {
					break;
				}
			}
			selected_dist--;
		}

		this.putEquipmentOnFloor(obj_thrown, x, z);

	}


	private void putEquipmentOnFloor(EquipmentData obj_thrown, int mapx, int mapz) {
		this.mapdata.getSq_MaybeNULL(mapx, mapz).addEquipment(obj_thrown);
		obj_thrown.seen_by_side[this.current_unit.getSide()] = 1;
		this.updateEquipmentOnServer(obj_thrown, mapx, mapz);
		obj_thrown.model = EquipmentModel.Factory(this, obj_thrown, mapx, mapz);
		this.attachToRootNode(obj_thrown.model, true);

	}


	public void primeGrenade(byte turns) {
		EquipmentData eq = this.current_unit.current_item;
		if (eq.primed == false) {
			if (this.current_unit.checkAndReduceAPs(this, APS_PRIME, true)) {
				eq.primed = true;
				eq.explode_turns = turns;
				//eq.caused_by_side = this.current_unit.side;
				eq.last_unit_to_touch = this.current_unit.unitid;
				this.updateEquipmentOnServer(eq, (byte)-1, (byte)-1);
				this.updateUnitOnServer(this.current_unit);
				this.addToHUD(StartupNew.strings.getTranslation("Grenade primed for") + " " + turns + " " + StartupNew.strings.getTranslation("turns"));
				this.playSound(this.current_unit, SpeechPlayer.EV_GRENADE_PRIMED);

				try {
					boolean who_can_see[] = whichSidesCanSeeUnit(this.current_unit);
					this.sendEventToServer_GrenadePrimed(this.current_unit, turns, who_can_see);
				} catch (Exception ex) {
					AppletMain.HandleError(this, ex);
				}
			}
		} else {
			this.addToHUD(StartupNew.strings.getTranslation("Grenade is already primed!"));
		}
		this.updateMenu(AppletMain.MM_UNIT_SELECTED);
	}


	public WireframeState getWireframeState() {
		return this.myWireState;
	}


	public void stopGame() {
		this.client_mode = AppletMain.APP_TURN_FINISHED_COMPLETELY;
		//this.addToHUD("* " + StartupNew.strings.getTranslation("PROGRAM HALTED") + " *");
	}


	public void lookAtEnemyUnit(UnitData unit) {
		addToHUD(StartupNew.strings.getTranslation("Showing enemy ") + unit.name);
		this.current_unit = null;
		this.updateMenu(AppletMain.MM_NO_UNIT_SELECTED);
		//cam.lookAt(new Vector3f(unit.getMapX(), 0f, unit.getMapZ()), Vector3f.UNIT_Y);
		//cam.update();
		lookAt(unit.getMapX(), unit.getMapZ());
		this.removeHighlighter();
	}


	public void lookAt(int x, int z) {
		cam.lookAt(new Vector3f(x, 0, z), Vector3f.UNIT_Y);
		cam.update();
	}


	private float getAdjustmentForAngle(int source_ang, int target_ang) {
		int ang_diff = Geometry.GetDiffBetweenAngles(source_ang, target_ang);
		if (ang_diff >= 130) {
			return 1f;
		} else if (ang_diff >= 40) {
			return 0.5f;
		} else {
			return .25f;
		}
	}


	public void closeCombat(UnitData attacker, UnitData defender) {
		StartupNew.waitForPendingUpdateRequests();

		try {
			// send CC event to server
			boolean who_can_see[] = this.whichSidesCanSeeUnit(attacker);
			this.sendEventToServer_CloseCombat(this.current_unit, who_can_see);
		} catch (Exception ex) {
			AppletMain.HandleError(this, ex);
		}

		int att_rating = attacker.combat_skill;
		if (attacker.current_item != null) {
			att_rating += attacker.current_item.cc_acc;
		}
		float angle_mult = this.getAdjustmentForAngle(attacker.angle, defender.angle);
		int def_rating = (int)((float)defender.combat_skill * angle_mult); // Adjust by the angle we're attacking from

		int tot = (att_rating*2) - def_rating;

		AppletMain.p("-------------------");
		AppletMain.p("Att: " + att_rating);
		AppletMain.p("Def: " + def_rating);
		AppletMain.p("%: " + tot);

		int rnd = Functions.rnd(1, 100); 
		if (rnd < tot) {
			// Success!
			AppletMain.p("Attacker won");
			/*damage = attacker.strength;
			if (attacker.current_item != null) {
				damage += attacker.current_item.cc_damage;
			}
			damage = this.randomizeDamage(damage);
			AppletMain.p("Rnd Dam: " + damage);
			damage = (short)(damage - (defender.protection * angle_mult));
			AppletMain.p("Dam after armour: " + damage);*/
			ccDamage(attacker, defender, angle_mult);
		} else {
			// Defender can see attacker
			AppletMain.p("Defender won");
			if (defender.canUnitSee_Cached(attacker)) {
				ccDamage(defender, attacker, angle_mult);
			} else {
				AppletMain.p("Defender cannot see attacker");
			}
		}

	}


	private void ccDamage(UnitData victor, UnitData loser, float angle_mult) {
		short damage = 0;
		damage = victor.strength;
		if (victor.current_item != null) {
			damage += victor.current_item.cc_damage;
		}
		damage = this.randomizeDamage(damage);
		AppletMain.p("Rnd Dam: " + damage);
		damage = (short)(damage - (loser.protection * angle_mult));
		AppletMain.p("Dam after armour: " + damage);
		loser.damage(this, damage, victor, UnitDataComms.FOA_MELEE);

	}


	public void showPrimeMenu() {
		this.less_icon.action = this.do_prime_icon;
		this.more_icon.action = this.do_prime_icon;
		this.updateMenu(AppletMain.MM_PRIME_GRENADE);
		this.addToHUD("");
		this.addToHUD(StartupNew.strings.getTranslation("Priming Help") + ":-");
		this.addToHUD(StartupNew.strings.getTranslation("0 - End of this turn"));
		if (this.game_data.num_players == 2) {
			this.addToHUD(StartupNew.strings.getTranslation("1 - End of your opponent's next turn"));
			this.addToHUD(StartupNew.strings.getTranslation("2 - End of your next turn"));
		} else if (this.game_data.num_players == 3) {
			this.addToHUD(StartupNew.strings.getTranslation("1 - End of your first opponent's next turn"));
			this.addToHUD(StartupNew.strings.getTranslation("2 - End of your second opponent's next turn"));
			this.addToHUD(StartupNew.strings.getTranslation("3 - End of your next turn"));
		} else if (this.game_data.num_players == 4) {
			this.addToHUD(StartupNew.strings.getTranslation("1 - End of your first opponent's next turn"));
			this.addToHUD(StartupNew.strings.getTranslation("2 - End of your second opponent's next turn"));
			this.addToHUD(StartupNew.strings.getTranslation("3 - End of your third opponent's next turn"));
			this.addToHUD(StartupNew.strings.getTranslation("4 - End of your next turn"));
		}
		this.addToHUD(StartupNew.strings.getTranslation("and so on..."));
	}


	public void showConfirmEndTurnMenu() {
		/*if (FIX_EXPL) {
			UnitData unit = UnitData.GetUnitDataFromID(units, 12688);
			this.explosion((byte)49, (byte)5, (byte)4, 100, unit, true, false);
			this.explosion((byte)45, (byte)5, (byte)3, 80, unit, true, false);
		}*/
		StartupNew.waitForPendingUpdateRequests();
		this.updateMenu(AppletMain.MM_CONFIRM_END_TURN);
	}


	public UnitData getCurrentUnit() {
		return this.current_unit;
	}


	public void reload() {
		EquipmentData gun = this.current_unit.current_item; 
		if (gun != null) {
			if (gun.major_type == EquipmentTypesTable.ET_GUN) {
				EquipmentData ammo = this.findAmmo(gun.ammo_type_id);
				if (ammo != null) {
					if (this.current_unit.checkAndReduceAPs(this, gun.reload_cost, true)) {
						while (gun.getAmmoCapacity() > gun.getAmmo() && ammo.getAmmo() > 0) {
							gun.incAmmo();
							ammo.decAmmo();
						}
						this.updateUnitOnServer(this.current_unit);
						this.updateEquipmentOnServer(gun, (byte)-1, (byte)-1);
						this.updateEquipmentOnServer(ammo, (byte)-1, (byte)-1);
						if (ammo.getAmmo() <= 0) {
							ammo.equipmentDestroyed(this);
						}

						this.updateMenu(AppletMain.MM_UNIT_SELECTED);
						this.addToHUD(StartupNew.strings.getTranslation("Weapon reloaded"));
					}
				} else {
					this.addToHUD(StartupNew.strings.getTranslation("No compatible ammo found!"));
				}
			}
		}
	}


	public void useMedikit() {
		if (this.current_unit != null) {
			EquipmentData eq = this.current_unit.current_item; 
			if (eq != null) {
				if (eq.major_type == EquipmentTypesTable.ET_MEDIKIT) {
					if (this.current_unit.getHealth() < this.current_unit.max_health) {
						if (this.current_unit.checkAndReduceAPs(this, APS_USE_EQUIPMENT, true)) {
							this.current_unit.removeItem(this, eq, false);
							this.current_unit.incHealth(eq.shot_damage);
							this.addToHUD("Medikit used!");
							this.updateUnitOnServer(this.current_unit);
							//this.updateEquipmentOnServer(eq, -1, -1);
							eq.equipmentDestroyed(this);
							this.updateMenu(AppletMain.MM_UNIT_SELECTED);
							this.playSound(this.current_unit, SpeechPlayer.EV_USED_MEDIKIT);
						}
					} else {
						this.addToHUD(StartupNew.strings.getTranslation("That unit is at maximum health."));
					}
				} else {
					this.addToHUD(StartupNew.strings.getTranslation("That is not a medikit!"));
				}
			}
		} else {
			this.addToHUD(StartupNew.strings.getTranslation("No unit selected."));
		}
	}


	public void useScanner() {
		if (this.current_unit != null) {
			EquipmentData eq = this.current_unit.current_item; 
			if (eq != null) {
				if (eq.major_type == EquipmentTypesTable.ET_SCANNER) {
					if (this.current_unit.checkAndReduceAPs(this, APS_USE_SCANNER, true)) {
						this.updateUnitOnServer(this.current_unit);

						// Get distance
						int sd_mapx = -1, sd_mapz = -1;
						EquipmentData stardrive = null;
						for (int i=0 ; i<this.equipment.length ; i++) {
							stardrive = equipment[i];
							if (stardrive.major_type == EquipmentTypesTable.ET_FLAG || stardrive.major_type == EquipmentTypesTable.ET_STARDRIVE) {
								//if (stardrive.destroyed == false) {
								if (stardrive.getUnitID() > 0) {
									UnitData unit = UnitData.GetUnitDataFromID(units, stardrive.getUnitID());
									sd_mapx = unit.getMapX();
									sd_mapz = unit.getMapZ();
								} else {
									PointByte p = EquipmentData.GetEquipmentMapSquare(mapdata, stardrive);
									sd_mapx = p.x;
									sd_mapz = p.y;
								}
								break;
								//}
							}
						}
						if (stardrive != null && sd_mapx >= 0 && sd_mapz >= 0) {
							int dist = (int)Geometry.distance(this.current_unit.getMapX(), this.current_unit.getMapZ(), sd_mapx, sd_mapz);
							if (stardrive.major_type == EquipmentTypesTable.ET_FLAG) {
								this.addToHUD(StartupNew.strings.getTranslation("The flag is") + " " + dist + " " + StartupNew.strings.getTranslation("metres away"));
							} else if (stardrive.major_type == EquipmentTypesTable.ET_STARDRIVE) {
								this.addToHUD(StartupNew.strings.getTranslation("The stardrive is") + " " + dist + " " + StartupNew.strings.getTranslation("metres away"));
							} else {
								this.addToHUD(StartupNew.strings.getTranslation("Unknown item: ") + stardrive.major_type);
							}
						} else {
							this.addToHUD(StartupNew.strings.getTranslation("Nothing found"));
						}
						//this.playSound(this.current_unit, SpeechPlayer.EV_USED_MEDIKIT);
					}
				} else {
					this.addToHUD(StartupNew.strings.getTranslation("That is not a scanner!"));
				}
			}
		} else {
			this.addToHUD(StartupNew.strings.getTranslation("No unit selected."));
		}
	}


	/*public void useAdrenalinShot() {
		EquipmentData eq = this.current_unit.current_item; 
		if (eq != null) {
			if (eq.major_type == EquipmentTypesTable.ET_ADRENALIN_SHOT) {
				if (this.current_unit.getAPs() < this.current_unit.max_aps) { // Only use it once!?
					if (units_used_adrenalin.contains(this.current_unit) == false) {
						units_used_adrenalin.add(this.current_unit);
						if (this.current_unit.checkAndReduceAPs(this, APS_USE_EQUIPMENT)) {
							this.current_unit.removeItem(this, eq, false);
							this.current_unit.setAPs(this.current_unit.getAPs() + eq.shot_damage);
							this.addToHUD(StartupNew.strings.getTranslation("Adrenalin shot used!"));
							this.updateUnitOnServer(this.current_unit);
							eq.equipmentDestroyed(this);
							this.updateMenu(AppletMain.MM_UNIT_SELECTED);
						}
					} else {
						this.addToHUD(StartupNew.strings.getTranslation("Another shot will cause overdose!"));
					}
				} else {
					this.addToHUD(StartupNew.strings.getTranslation("Another shot will cause overdose!"));
				}
			} else {
				this.addToHUD(StartupNew.strings.getTranslation("That is not an adrenalin shot!"));
			}
		}
	}*/


	/*	public void usePortaPorter() {
		EquipmentData trigger = this.current_unit.current_item; 
		if (trigger != null) {
			if (trigger.major_type == EquipmentTypesTable.ET_PORTA_PORTER_TRIGGER) {
				if (this.current_unit.checkAndReduceAPs(this, APS_USE_EQUIPMENT, true)) {
					this.updateUnitOnServer(this.current_unit);
					this.addToHUD("Porta-Porter attempted!");
					// Find lander
					EquipmentData lander;
					for (int i=0 ; i<this.equipment.length ; i++) {
						lander = this.equipment[i];
						if (lander.major_type == EquipmentTypesTable.ET_PORTA_PORTER_LANDER) {
							if (lander.getUnitID() <= 0) { // Is it on the floor?
								if (lander.primed) {
									// Check the square is free!
									PointByte p = EquipmentData.GetEquipmentMapSquare(this.mapdata, lander);
									if (this.getUnitAt(p.x, p.y) == null) {
										AppletMapSquare sq = this.mapdata.getSq_MaybeNULL(p.x, p.y);
										this.current_unit.setMapLocation(this, p.x, p.y, sq);
										this.updateUnitOnServer(this.current_unit);
										this.current_unit.removeItem(this, trigger, true);
										trigger.equipmentDestroyed(this);
										lander.primed = false;
										this.updateEquipmentOnServer(lander, p.x, p.y);

										this.updateMenu(AppletMain.MM_UNIT_SELECTED);
										this.addToHUD(StartupNew.strings.getTranslation("Teleportation successful!"));
										return;
									} else {
										//this.addToHUD("The square is not free!");
									}
								}
							}
						}
					}
					this.addToHUD(StartupNew.strings.getTranslation("No lander is available."));

				}
			} else {
				this.addToHUD(StartupNew.strings.getTranslation("That is not a Porta Porter!"));
			}
		}
	}

	 */
	private EquipmentData findAmmo(int type) {
		for (int i=0 ; i<this.current_unit.items.size() ; i++) {
			EquipmentData eq = this.current_unit.items.get(i);
			if (eq.major_type == EquipmentTypesTable.ET_AMMO_CLIP) {
				if (eq.ammo_type_id == type) {
					return eq;
				}
			}
		}
		return null;
	}


	// Override the default close action
	protected void quit() {
		if (display != null) {
			display.close();
		}
		if (sfx != null) {
			sfx.stopNow();
		}
		// DON'T NEED THIS HERE AS IT'S DONE ALREADY WHEN END-TURN SELECTED
		// No!, we do need it in case the player closes the window without ending the turn!
		StartupNew.waitForPendingUpdateRequests();
		AppletMain.p("Ended");
	}


	private void deselectUnitIfCurrent(UnitData unit) {
		if (this.current_unit == unit) {
			this.current_unit = null;
			this.removeHighlighter();
			this.updateMenu(AppletMain.MM_NO_UNIT_SELECTED);
		}
	}


	public void addLine(Vector3f start, Vector3f end) {
		TargettingLine t = new TargettingLine(this);
		t.updateLine(start, end);
		this.attachToRootNode(t, true);
	}


	public void unitKilled(UnitData unit) {
		this.our_unit_icons.remove(unit.icon);
		deselectUnitIfCurrent(unit);

		// Drop equipment
		EquipmentData equip;
		for (int i=0 ; i<equipment.length ; i++) {
			equip = equipment[i];
			if (equip.getUnitID() == unit.unitid) {
				unit.removeItem(this, equip, false);
				equip.seen_by_side[unit.getSide()] = 1;
				this.mapdata.getSq_MaybeNULL(unit.getMapX(), unit.getMapZ()).addEquipment(equip);
				this.updateEquipmentOnServer(equip, unit.getMapX(), unit.getMapZ());
				equip.model = EquipmentModel.Factory(this, equip, unit.getMapX(), unit.getMapZ());
				this.attachToRootNode(equip.model, true);
			}
		}
	}


	public void unitEscaped(UnitData unit) {
		sfx.playSound(unit, SpeechPlayer.EV_UNIT_ESCAPED);
		this.our_unit_icons.remove(unit.icon);
		this.deselectUnitIfCurrent(unit);
		this.addToHUD(unit.name + " " + StartupNew.strings.getTranslation("has escaped!"));
	}


	public boolean isSendingData() {
		if (playback_controller == null) {
			return StartupNew.update_server_thread.isDataWaiting();
		} else {
			return false;
		}
	}


	public void processingStatusChanged() {
		if (this.hud != null) {
			this.hud.markForRefresh();
		}
	}


	public void activateEquipment() {
		EquipmentData eq = this.current_unit.current_item; 
		if (eq != null) {
			if (eq.major_type == EquipmentTypesTable.ET_DEATH_GRENADE || eq.major_type == EquipmentTypesTable.ET_PORTA_PORTER_LANDER) {
				if (eq.primed == false) {
					if (this.current_unit.checkAndReduceAPs(this, APS_ACTIVATE, true)) {
						this.updateUnitOnServer(this.current_unit);
						this.addToHUD(StartupNew.strings.getTranslation("Item activated!"));
						eq.primed = true;
						eq.last_unit_to_touch = this.current_unit.unitid; 
						this.updateEquipmentOnServer(eq, (byte)-1, (byte)-1);
						this.updateMenu(AppletMain.MM_UNIT_SELECTED);
					}
				} else {
					this.addToHUD(StartupNew.strings.getTranslation("The item is already activated."));
				}
			} else {
				this.addToHUD(StartupNew.strings.getTranslation("That item cannot be primed."));
			}
		}
	}


	public void checkForDeathGrenadesOrAlienExplode(UnitData caused_by_unit) {
		if (caused_by_unit.model_type == UnitsTable.MT_ALIEN_TYRANT) {
			short ACID_BLOOD_DAMAGE = 40;
			this.normalExplosion(caused_by_unit.map_x, caused_by_unit.map_z, Functions.rndByte(1, 3), ACID_BLOOD_DAMAGE, caused_by_unit, false, true);
		} else if (caused_by_unit.model_type == UnitsTable.MT_QUEEN_ALIEN) {
			short ACID_BLOOD_DAMAGE = 80;
			this.normalExplosion(caused_by_unit.map_x, caused_by_unit.map_z, Functions.rndByte(2, 4), ACID_BLOOD_DAMAGE, caused_by_unit, false, true);
		} else if (caused_by_unit.model_type == UnitsTable.MT_BLOB) {
			// NO as it harms otjher blobs blobDoExplode(caused_by_unit);
		} else {
			for (int i=0 ; i<caused_by_unit.items.size() ; i++) {
				EquipmentData eq = caused_by_unit.items.get(i);
				if (eq.major_type == EquipmentTypesTable.ET_DEATH_GRENADE) {
					if (eq.primed) {
						eq.primed = false;
						this.updateEquipmentOnServer(eq, (byte)-1, (byte)-1);
						//this.explodeGrenade(eq, caused_by_unit);
						this.scheduleExplosion(eq, caused_by_unit);
					}
				}
			}
		}
	}


	private void blobDoExplode(UnitData caused_by_unit) {
		short DAMAGE = (short)(caused_by_unit.strength * 2);
		this.normalExplosion(caused_by_unit.map_x, caused_by_unit.map_z, (byte)(caused_by_unit.strength/10), DAMAGE, caused_by_unit, false, true);

	}


	public void blobAttemptExplode() {
		if (this.current_unit.checkAndReduceAPs(this, APS_EXPLODE, true)) {
			UnitData this_unit = this.current_unit; // Need this as current_unit is set to null when unit is killed
			this.current_unit.damage(this, this.current_unit.getHealth(), this.current_unit, UnitDataComms.FOA_EXPLOSION);
			this.updateUnitOnServer(this_unit);
			this.blobDoExplode(this_unit);
		}
	}


	public void playSound(int s) {
		sfx.playSound(s);
	}


	public void playSound(UnitData unit, int s) {
		sfx.playSound(unit, s);
	}


	public void openDoor() {
		AppletMapSquare sq = this.current_unit.model.getSquareInFrontOfUnit_MaybeNULL();
		if (sq != null) {
			if (sq.door_type > 0) {
				if (sq.door_open == false) {
					if (this.current_unit.checkAndReduceAPs(this, APS_OPEN_DOOR, true)) {
						this.updateUnitOnServer(this.current_unit);
						playSound(SoundEffects.DOOR);

						sq.door.startOpening();
						sq.door.opposite.startOpening();
						sq.door_open = true;

						this.updateMapOnServer(sq, -1);
						recalcVisibleEnemiesAndOppFire(true, null); //this.current_unit);  MUST BE NULL AS THE UNIT THAT OPENED THE DOOR MAY NOT BE THE NEWLY VISIBLE ONE
						this.updateMenu(AppletMain.MM_UNIT_SELECTED);

					}				
				} else {
					this.addToHUD(StartupNew.strings.getTranslation("The door is already open."));
				}
			} else {
				this.addToHUD(StartupNew.strings.getTranslation("There is no door there."));
			}
		}
	}


	public void closeDoor() {
		AppletMapSquare sq = this.current_unit.model.getSquareInFrontOfUnit_MaybeNULL();
		if (sq != null) {
			if (sq.door_type > 0) {
				if (sq.door_open) {
					UnitData unit = getUnitAt(sq.x, sq.y);
					if (unit == null) {
						if (this.current_unit.checkAndReduceAPs(this, APS_OPEN_DOOR, true)) {
							this.updateUnitOnServer(this.current_unit);
							playSound(SoundEffects.DOOR);

							sq.door.startClosing();
							sq.door.opposite.startClosing();
							sq.door_open = false;

							this.updateMapOnServer(sq, -1);
							this.updateMenu(AppletMain.MM_UNIT_SELECTED);

						}				
					} else {
						this.addToHUD(StartupNew.strings.getTranslation("There is a unit in the way."));
					}
				} else {
					this.addToHUD(StartupNew.strings.getTranslation("The door is already closed."));
				}
			} else {
				this.addToHUD(StartupNew.strings.getTranslation("There is no door there."));
			}
		}
	}


	public void gameIsDraw() {
		this.addToHUD("**********************");
		this.addToHUD("THE GAME IS A DRAW!");
		this.addToHUD("**********************");
		this.stopGame();
	}


	public void sideHasWon(byte side) {
		this.addToHUD("**********************");
		if (game_data.areSidesFriends(side, this.game_data.our_side)) {
			this.addToHUD("YOU HAVE WON!");
			this.sfx.playSound(SoundEffects.WON);
		} else {
			this.addToHUD("YOU HAVE LOST!");
			this.sfx.playSound(SoundEffects.LOST);
		}
		this.addToHUD("**********************");
		this.stopGame();
	}


	public void refreshHUD() {
		if (this.hud != null) {
			hud.markForRefresh();
		}
	}


	public TextureState getTextureState(int tex_code) {
		return ts_cache.get(tex_code);
	}


	public TextureState getTextureState(String tex_code) {
		return ts_cache.get(tex_code);
	}


	public boolean isObjectUpdating(IUpdatable i) {
		return this.updating_objects.contains(i);
	}


	public void objectFinishedMoving(UnitData unit) {
		recalcVisibleEnemiesAndOppFire(true, unit);
		updateMenu(); // In case there's something to pick up.  Must be before we check for visible enemies though in case of Opp Fire.  NO - MUST BE AFTER SO THE ENEMIES ICONS ARE ADDED TO THE HUD!
	}


	public float getInterpolation() {
		return this.tpf;
	}


	public void selectOppFire(byte type) {
		if (this.current_unit != null) {
			this.current_unit.opp_fire_type = type;
			this.updateUnitOnServer(this.current_unit);
			this.updateMenu(AppletMain.MM_UNIT_SELECTED);
		}
	}


	public int getAppletStage() {
		return this.client_mode;
	}


	public void setPlaybackController(ClientPlaybackControllerThread _controller) {
		if (this.playback_controller == null) {
			AbstractUnit.SPEED = AbstractUnit.SPEED * 2; 
		}
		this.playback_controller = _controller;

	}


	/*public boolean areSidesFriends(byte s1, byte s2) {
		if (s1 == s2) {
			return true;
		}
		if (s1 < 0 || s2 < 0) {
			return false;
		}
		if (this.game_data.side_sides[s1] != null) {
			return (this.game_data.side_sides[s1].contains(s2));
		} else {
			return false;
		}
	}*/


	public void scheduleExplosion(EquipmentData eq, UnitData caused_by) {
		//eq.equipmentDestroyed(this);  // NO!  As this remove is from the unit holding it!
		eq.destroyed = true;// Stop it being added again!
		this.explosions_to_show.add(new ExplosionPendingData(eq, caused_by));
	}


	public void updateEquipmentOnServer(EquipmentData eq, int mapx, int mapz) {
		if (SharedStatics.VERBOSE_COMMS) {
			p("Updating equipment on server.");
		}
		String req = EquipmentDataComms.GetEquipmentUpdateRequest_UNUSED(eq, mapx, mapz, game_data.game_id);
		StartupNew.update_server_thread.addRequest(req);
	}


	protected boolean areExplosionsScheduled() {
		return this.explosions_to_show.size() > 0;
	}


	protected ExplosionPendingData getNextExplosion() {
		return this.explosions_to_show.remove(0);
	}


	protected int getNumScheduledExplosions() {
		return this.explosions_to_show.size();
	}


	/**
	 * This is required since we can't remove objects during the render phase (get array index oob)
	 */
	public void addObjectForRemoval(Spatial s) {
		this.objects_for_removal.add(s);
	}


	public void addToHUD(String s, boolean pri) {
		if (hud != null) {
			hud.addText(s, pri);
		}
	}


	public void addToHUD(String s) {
		addToHUD(s, false);
	}


	@Override
	public void displayMessage(String s) {
		this.addToHUD(s, false);
	}


	public static void HandleError(AppletMain m, Throwable ex) {
		HandleError(m, ex, true);
	}


	public static void HandleError(AppletMain m, Throwable ex, boolean send_to_server) {
		try {
			ex.printStackTrace();
			if (m != null) {
				m.addToHUD("ERROR!: " + ex.toString());
			}
			if (StartupNew.lw != null) {
				StartupNew.lw.appendLine("ERROR!: " + ex.toString());
			}
			String filename = "error_log.txt";
			Functions.LogError(ex, "Version=" + SharedStatics.VERSION, filename);
			p("An error log has been created (" + filename + ").");//  Please email this to " + EMAIL_ADDRESS + ".");

			if (send_to_server) {
				try {
					String data = "version=" + SharedStatics.VERSION + "&login=" + AbstractCommFuncs.URLEncodeString(StartupNew.login) + "&error=" + AbstractCommFuncs.URLEncodeString(ex.toString() + "\n\n" + Functions.Exception2String(ex, "\n"));
					if (data.equalsIgnoreCase(last_error) == false) { // Stop multiple errors being sent!
						new WGet4Client(null, WGet4Client.T_SPECIFIED, SharedStatics.URL_FOR_CLIENT + "/appletcomm/collecterrors.cls", data, true);
						AppletMain.p("Error sent to server.");
						last_error = data;
					}
				} catch (Exception e) {
					e.printStackTrace();
					// Do nothing
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			// Do nothing, an error caused by logging the error
		}

	}


	public Node getRootNode() {
		return this.rootNode;
	}


	public void attachToRootNode(Spatial n, boolean update) {
		this.rootNode.attachChild(n);
		if (update) {
			rootNode.updateModelBound();
			rootNode.updateGeometricState(0, true);
			rootNode.updateRenderState();
		}
	}


	public DisplaySystem getDisplay() {
		return this.display;
	}


	public float getFPS() {
		return timer.getFrameRate();
	}


	public static void p(Object o) {
		// 
		System.out.println(Dates.FormatDate(new Date(), Dates.UKDATE_FORMAT_WITH_TIME) + ": " + o);
	}


	//@Override
	public void handle(Throwable t) {
		HandleError(this, t);
	}


}
