package dsr.start;

import javax.swing.JButton;

public class ButtonMission extends JButton {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int mission_num;
	
	public ButtonMission(String s, int _mission_num) {
		super(s);
		mission_num = _mission_num;
	}

}
