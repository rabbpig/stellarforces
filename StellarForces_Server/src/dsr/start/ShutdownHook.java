package dsr.start;

import java.io.File;

public class ShutdownHook extends Thread {
	
	public ShutdownHook() {
		super("ShutdownHook");
	}
	
	
	public void run() {
		// Delete the lock file to allow another client to run
		new File(UpdateFileLockThread.FILENAME).delete();
	}

}
