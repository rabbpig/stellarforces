package dsr.start;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import ssmith.audio.MP3Player;
import ssmith.io.IOFunctions;
import ssmith.io.TextFile;
import ssmith.lang.Functions;

import com.jme.app.AbstractGame;

import dsr.AppletMain;
import dsr.SharedStatics;
import dsr.comms.AbstractCommFuncs;
import dsr.comms.WGet4Client;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public class StartupNew {

	private static final String SETTINGS_DIR_NAME = ".sf_local_settings/";
	private static String SETTINGS_DIR = null;
	private static final String OLD_LOGIN_FILE = "data/last_login.txt";
	private static String NEW_LOGIN_FILE;// = SETTINGS_DIR + "last_login.txt";

	public static LogWindow lw;
	private static MP3Player music;
	public static StringData strings = new StringData();
	public static boolean hi_mem = false, play_music = true, help_mode = true, play_sfx = true, show_jme_options = true;
	public static UpdateServerThread update_server_thread;
	public static String login, pwd;

	public StartupNew() {
		if (UpdateFileLockThread.DoesRecentFileExist()) {
			JOptionPane.showMessageDialog(lw, "There seems to be an instance of the client already running.  Please close it to continue.\n\nIf there is no client already open, please wait a few seconds and try again.", "Client Already Running", JOptionPane.ERROR_MESSAGE);
			return;
		}

		new UpdateFileLockThread();
		Runtime.getRuntime().addShutdownHook(new ShutdownHook());

		if (lw == null) {
			lw = new LogWindow();
		}

		try {
			Logger.getLogger("").setLevel(Level.WARNING);
			
			String user_home = System.getProperty("user.home");
			if (user_home == null || user_home.length() == 0) {
				user_home = ".";
			}
			SETTINGS_DIR = Functions.AppendSlashToEnd(user_home) + Functions.AppendSlashToEnd(SETTINGS_DIR_NAME);
			NEW_LOGIN_FILE = Functions.AppendSlashToEnd(SETTINGS_DIR) + "last_login.txt";
			
			// Get last login and pwd
			login = "";
			pwd = "";
			int language_code = 0; // English, default

			// Create settings dir if not exists
			try {
				File dir = new File(SETTINGS_DIR);
				if (dir.isDirectory() == false) {
					if (dir.mkdir() == false) {
						throw new RuntimeException("Unable to create directory '" + SETTINGS_DIR + "' to save settings.");
					}
				}
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, "Unable to create directory '" + SETTINGS_DIR + "' for saving data.", "Error Creating Directory", JOptionPane.ERROR_MESSAGE);
				AppletMain.HandleError(null, ex, false);
			}

			String login_filename = "";
			if (new File(NEW_LOGIN_FILE).canRead()) {
				login_filename = NEW_LOGIN_FILE;
			} else if (new File(OLD_LOGIN_FILE).canRead()) {
				login_filename = OLD_LOGIN_FILE;
			}
			if (login_filename.length() > 0) {
				TextFile tf = new TextFile();
				tf.openFile(login_filename, TextFile.READ);
				login = tf.readLine();
				try {
					pwd = tf.readLine();
					language_code = Integer.parseInt(tf.readLine());
					hi_mem = Boolean.parseBoolean(tf.readLine());
					play_music = Boolean.parseBoolean(tf.readLine());
					help_mode = Boolean.parseBoolean(tf.readLine());
					play_sfx = Boolean.parseBoolean(tf.readLine());
					show_jme_options = Boolean.parseBoolean(tf.readLine());
				} catch (Exception ex) {
					// Do nothing, it might not exist
				}
				if (pwd == null) {
					pwd = "";
				}
				tf.close();
			}

			String res = strings.setLanguage(language_code);
			if (res.length() > 0) {
				lw.displayMessage(res);
			}

			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (SharedStatics.DEBUG) {
				lw.appendLine("#### DEBUG MODE ON ####");
			}
			lw.appendLine(strings.getTranslation("Welcome to ") + SharedStatics.TITLE);
			lw.appendLine(strings.getTranslation("If you have any problems, please visit the website at ") + SharedStatics.WEBSITE_LINK + "/ " + strings.getTranslation("or email ") + "steve@stellarforces.com");
			lw.appendLine(strings.getTranslation("Client version ") + SharedStatics.VERSION);
			lw.appendLine(strings.getTranslation("Please wait") + " - " + strings.getTranslation("checking version") + "...");

			boolean downloaded_new_version = false;
			// See if there is a newer version
			WGet4Client wc = new WGet4Client(lw, "cmd=" + MiscCommsPage.GET_LATEST_VERSION_NUM, true);
			String response = wc.getResponse();
			float latest_version = Float.parseFloat(response);
			if (latest_version > SharedStatics.VERSION) {
				int result = JOptionPane.showConfirmDialog(lw, "There is a new version of the client to download (v" + latest_version + ").  Do you want to download it (recommended)?", "New Client Update", JOptionPane.YES_NO_OPTION);
				if (result == JOptionPane.YES_OPTION) {
					try {
						downloadNewClient(response);
						downloaded_new_version = true;
						lw.setVisible(false);
					} catch (Exception ex) {
						ex.printStackTrace();
						lw.displayMessage(ex.toString());
						JOptionPane.showMessageDialog(lw, "There was a problem downloading the new version: " + ex.toString() + "\n\nPlease restart the program.  If you continue to have problems, the client can be manually downloaded via the link on the website.", "Client NOT Updated", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
			if (downloaded_new_version == false) {
				wc = new WGet4Client(lw, "cmd=" + MiscCommsPage.CHECK_VERSION2 + "&version=" + SharedStatics.VERSION, true);
				response = wc.getResponse();
				if (isResponseGood(response)) {
					lw.appendLine(strings.getTranslation("Version ok."));

					while (true) {
						LoginForm frm_login = new LoginForm(login, pwd, pwd.length() > 0, language_code, hi_mem, play_music, help_mode, play_sfx, show_jme_options);
						while (frm_login.isVisible()) {
							Functions.delay(1000);
						}
						if (frm_login.ok_clicked) {
							login = frm_login.txt_email.getText();
							pwd = frm_login.txt_pwd.getText();//.getPassword().toString();
							language_code = frm_login.cbLang.getSelectedIndex();
							res = strings.setLanguage(language_code);
							if (res.length() > 0) {
								lw.displayMessage(res);
							}
							hi_mem = frm_login.chk_hi_mem.isSelected();
							play_music = frm_login.chk_music.isSelected();
							help_mode = frm_login.chk_help_mode.isSelected();
							play_sfx = frm_login.chk_sfx.isSelected();
							show_jme_options = frm_login.chk_show_jme.isSelected();

							if (play_music) {
								StartMusic(); // Don't start music until we've upgraded as it locks files
							}
						} else {
							System.exit(0);
						}
						lw.appendLine(strings.getTranslation("Please wait") + "...");
						wc = new WGet4Client(lw, "cmd=" + MiscCommsPage.VALIDATE_LOGIN + "&login=" + AbstractCommFuncs.URLEncodeString(login) + "&pwd=" + AbstractCommFuncs.URLEncodeString(pwd), true);
						response = wc.getResponse();
						if (isResponseGood(response)) {
							lw.appendLine(strings.getTranslation("Login accepted."));
							// Notice we only send the password if the box is ticked
							this.saveData(frm_login.chk_remember_pwd.isSelected() ? pwd : "", language_code);//, hi_mem, play_music, help_mode, play_sfx, show_jme_options);

							update_server_thread = new UpdateServerThread();
							waitForPendingUpdateRequests(); // In case there's any leftover data

							this.selectGameIn3D();

							lw.setVisible(true); // Show it while we're sending data
							update_server_thread.stop_now = true;
							waitForPendingUpdateRequests();

							lw.setVisible(false);
							break;
						} else {
							lw.appendLine(strings.getTranslation("Invalid login/password"));
						}
					}
				} else {
					//System.err.println(response);
					lw.appendLine(strings.getTranslation("* Your client is far too old.  Please download the latest version. *"));
				}
			}
		} catch (Exception e) {
			AppletMain.HandleError(null, e);
		}
		while (lw.isVisible()) { // Keep it open in case of errors.
			Functions.delay(1000);
		}
		System.exit(0);

	}


	private void saveData(String password, int language_code) {
		try {
			TextFile tf = new TextFile();
			tf.openFile(NEW_LOGIN_FILE, TextFile.WRITE);
			tf.writeLine(login);
			tf.writeLine(password);
			tf.writeLine("" + language_code);
			tf.writeLine("" + hi_mem);
			tf.writeLine("" + play_music);
			tf.writeLine("" + help_mode);
			tf.writeLine("" + play_sfx);
			tf.writeLine("" + show_jme_options);
			tf.close();

			// Delete old data file just in case
			File f = new File(OLD_LOGIN_FILE);
			if (f.exists()) {
				f.delete();
			}
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "Unable to save details at '" + NEW_LOGIN_FILE + ".  You will have to enter them again next time.", "Error Saving Details", JOptionPane.WARNING_MESSAGE);
			AppletMain.HandleError(null, ex, false);
		}
	}


	private static boolean isResponseGood(String s) {
		// Have we received multiple types of data?
		if (s.indexOf("|") < 0) { // No
			return s.equalsIgnoreCase(MiscCommsPage.OK) || s.equalsIgnoreCase("200");
		} else {
			String s2[] = s.split("\\|");
			if (Functions.IsNumeric(s2[0])) {
				if (s2.length > 1) {
					if (s2[1].length() > 0) {
						lw.appendLine(s2[1]);
					}
				}
				return Functions.ParseInt(s2[0]) == 200;
			} else {
				return false;
			}
		}
	}


	private void selectGameIn3D() throws IOException {
		File f = new File("./data/images/sf_logo2.png");
		URL url = f.toURI().toURL();
		lw.setVisible(false);
		AppletMain main = new AppletMain();

		update_server_thread.setMain(main);
		main.setConfigShowMode(getJMEShowMode(), url);
		try {
			main.start();
		} catch (java.lang.UnsatisfiedLinkError ex) {
			AppletMain.HandleError(null, ex);
			try {
				if (ex.getMessage().indexOf("Can't load IA 32-bit .dll") >= 0) {
					JOptionPane.showMessageDialog(lw, "It looks like you are trying to use the 32-bit batch file.  Try running 'start_client_win64.bat' instead.\n\nError was '" + ex.toString() + "'\n\nPlease visit the website for further help & support.", "SF Client Error", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(lw, "Sorry, there seems to be a missing library: '" + ex.getMessage() + "'\n\nPlease visit the website for help & support.", "SF Client Error", JOptionPane.ERROR_MESSAGE);
				}
			} catch (Exception ex2) {
				ex2.printStackTrace();
			}
		} catch (Exception ex) {
			AppletMain.HandleError(null, ex);
			JOptionPane.showMessageDialog(lw, "Sorry, there seems to have been a problem running the client.\n\nError: '" + ex.toString() + "'\n\nPlease visit the website for help & support.", "SF Client Error", JOptionPane.ERROR_MESSAGE);
		}
		main = null;
		System.gc();
		lw.setVisible(true);
		Thread.yield();
	}


	private AbstractGame.ConfigShowMode getJMEShowMode() {
		if (show_jme_options) {
			return AbstractGame.ConfigShowMode.AlwaysShow;
		} else {
			return AbstractGame.ConfigShowMode.ShowIfNoConfig;
		}
	}

	
	public static void StartMusic() {
		music = new MP3Player("data/music/Laser_Squad_Mission_Recalled_OC_ReMix.mp3");
	}


	public static void StopMusic() {
		if (music != null) {
			music.stopNow();
			music = null;
		}
	}


	public static void waitForPendingUpdateRequests() {
		// Wait for all server updates to finish.
		if (update_server_thread != null) {
			while (update_server_thread.isDataWaiting() && update_server_thread.isAlive()) {
				Functions.delay(100);
			}
		}
	}


	private void downloadNewClient(String new_ver) throws UnknownHostException, IOException, NoSuchAlgorithmException {
		// Get the filelist if not got already
		String filelist_filename = "./temp_upgrade_files/filelist.txt";
		File f_filelist = new File(filelist_filename); 
		if (f_filelist.canRead() == false) {
			WGet4Client wc = new WGet4Client(null, WGet4Client.T_SPECIFIED, SharedStatics.URL_FOR_CLIENT + "/client_update_" + new_ver + "/filelist.txt", "", true);
			String filelist = wc.getResponse();

			// Create the dir to store the update
			IOFunctions.CreateDirsForFile(f_filelist.getPath());
			TextFile.QuickWrite(filelist_filename, filelist, true);
		}

		TextFile tf = new TextFile();
		tf.openFile(filelist_filename, TextFile.READ);
		while (tf.isEOF() == false) { // Loop through the list of files
			String tmp = tf.readLine();
			if (tmp.length() > 0) {
				String line[] = tmp.split("\\|");
				String fname = line[0];
				String md5 = "";
				if (line.length >= 2) {
					md5 = line[1];
				} else {
					throw new RuntimeException("Missing data in file");
				}
				File client_file = new File("temp_upgrade_files/"+fname);
				AppletMain.p("Checking " + client_file.getPath());
				if (client_file.canRead() == false) {
					// If it exists, copy it from the actual live working directory, since that may be up to date
					if (new File(fname).canRead()) {
						IOFunctions.CopyFile(fname, client_file.getPath(), true);
						// Check it's up to date
						String md = IOFunctions.GetMD5(client_file.getPath());
						if (md.equalsIgnoreCase(md5) == false) {
							downloadFile(new_ver, fname);
						} else {
							AppletMain.p(fname + " is up to date.");
						}
					} else { // Download it from the server
						downloadFile(new_ver, fname);
					}
				}
			}
		}
		tf.close();

		// Now we have downloaded all the files, move them into the right dirs
		AppletMain.p("Moving new files into place.");
		String files[] = {"bin", "data", "map_editor"}; //  "libs" needs doing
		for (int i=0 ; i<files.length ; i++) {
			IOFunctions.moveDirectory("temp_upgrade_files/" + files[i], files[i], "_" + SharedStatics.VERSION);

		}

		//IOFunctions.moveDirectory("temp_upgrade_files/bin", "bin", "_" + AppletMain.VERSION);
		//IOFunctions.moveDirectory("temp_upgrade_files/data", "data", "_" + AppletMain.VERSION);
		//IOFunctions.moveDir("temp_upgrade_files/libs", "libs", "_" + AppletMain.VERSION);
		//IOFunctions.moveDirectory("temp_upgrade_files/map_editor", "map_editor", "_" + AppletMain.VERSION);

		// Delete the original file list
		f_filelist.delete();

		JOptionPane.showMessageDialog(lw, "The new version of the client has been downloaded.  Please restart the program.", "Client Updated", JOptionPane.INFORMATION_MESSAGE);
	}


	private void downloadFile(String new_ver, String file) throws UnknownHostException, IOException {
		AppletMain.p("Downloading " + file);
		lw.appendLine("Downloading " + file);
		try {
			WGet4Client wc = new WGet4Client(null, WGet4Client.T_SPECIFIED, SharedStatics.URL_FOR_CLIENT + "/client_update_" + new_ver + "/" + file, "", true);

			// Create the directory for it
			String full_path = "temp_upgrade_files/" + file;
			IOFunctions.CreateDirsForFile(full_path);

			// Save the new file
			FileOutputStream fos = new FileOutputStream(full_path);
			fos.write(wc.getDataResponse());
			fos.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			// Maybe it's not required?
		}
	}


	//--------------------------------------------------------------

	public static void main(String args[]) {
		new StartupNew();
	}



}
