package dsr.start;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import ssmith.io.TextFile;
import ssmith.lang.Functions;
import dsr.AppletMain;
import dsr.SharedStatics;
import dsr.comms.WGet4Client;
import dsrwebserver.pages.appletcomm.MiscCommsPage;

public final class UpdateServerThread extends Thread {

	private static final String WAITING_DATA = "./data/waiting.txt";

	private AppletMain main;
	private volatile boolean processing = false;
	public volatile boolean stop_now = false;
	private ArrayList<String> reqs; // This gets used if we can't create files


	public UpdateServerThread() {
		super("UpdateServer");

		reqs = new ArrayList<String>();

		// See if there is any waiting data to be sent
		try {
			File f = new File(WAITING_DATA);
			if (f.canRead()) {
				TextFile tf = new TextFile();
				tf.openFile(WAITING_DATA, TextFile.READ);
				while (tf.isEOF() == false) {
					String line = tf.readLine();
					if (line.length() > 0) {
						reqs.add(line);
					}
				}
				tf.close();
				f.delete();
			}
		} catch (Exception ex) {
			AppletMain.HandleError(null, ex);
		}

		this.setDaemon(false);
		setProcessing(true); // So we signal to other programs, in case there is some leftover data from last time.
		start();
	}


	public void setMain(AppletMain m)  {
		main = m;
	}


	public void addRequest(String s) {
		setProcessing(true);

		synchronized (reqs) {
			reqs.add(s);
		}
	}


	public boolean isDataWaiting() {
		return processing;
	}


	public void run() {
		try {
			while (stop_now == false || reqs.size() > 0) {
				if (reqs.size() > 0) {
					setProcessing(true);
					while (reqs.size() > 0) {
						StringBuffer data = new StringBuffer();
						synchronized (reqs) {
							if (SharedStatics.VERBOSE_COMMS) {
								AppletMain.p("Batching " + reqs.size() + " items.");
							}
							for (int i=0 ; i<reqs.size() ; i++) {
								data.append(reqs.get(i) + MiscCommsPage.SEP);
							}
							reqs.clear();
						}
						sendData(data.toString());
					}
				}
				setProcessing(false);
				Functions.delay(1000);
			}
		} catch (Exception ex) {
			AppletMain.HandleError(main, ex);
			if (main != null) {
				main.addToHUD("* " + StartupNew.strings.getTranslation("Please restart client") + " *");
				main.stopGame();
			}
			// Write any outstanding data to file
			try {
				for (int i=0 ; i<reqs.size() ; i++) {
					TextFile.QuickAppend(WAITING_DATA, reqs.get(i), true, true);
				}
			} catch (Exception ex2) {
				AppletMain.HandleError(main, ex2);
			}
		}
	}


	private void setProcessing(boolean p) {
		if (p != this.processing) {
			if (main != null) {
				main.processingStatusChanged();
			}
			this.processing = p;
		}
	}


	private void sendData(String post_data) throws UnknownHostException, IOException {
		WGet4Client wc = new WGet4Client(main, WGet4Client.T_STD, null, post_data, true);
		String tmp_response = wc.getResponse();
		String responses[] = tmp_response.split(MiscCommsPage.SEP);
		for (int i=0 ; i<responses.length ; i++) {
			String response = responses[i];
			if (response.equalsIgnoreCase(MiscCommsPage.OK)) {
				// Do nothing
			} else if (response.equalsIgnoreCase(MiscCommsPage.DRAW)) {
				main.gameIsDraw();
			} else if (response.equalsIgnoreCase(MiscCommsPage.SIDE_1_WINS)) {
				main.sideHasWon((byte)1);
			} else if (response.equalsIgnoreCase(MiscCommsPage.SIDE_2_WINS)) {
				main.sideHasWon((byte)2);
			} else if (response.equalsIgnoreCase(MiscCommsPage.SIDE_3_WINS)) {
				main.sideHasWon((byte)3);
			} else if (response.equalsIgnoreCase(MiscCommsPage.SIDE_4_WINS)) {
				main.sideHasWon((byte)4);
			} else if (response.toLowerCase().startsWith(MiscCommsPage.MSG)) {
				main.addToHUD(response.substring(MiscCommsPage.MSG.length()));
			} else {
				System.err.println("Unexpected response from server: " + response);
				if (main != null) {
					main.addToHUD("Unexpected response from server:");
					main.addToHUD("'" + response + "'");
					main.addToHUD("Please restart the client!");
					main.stopGame();
				}
				this.processing = false;
				return;
			}
		}
	}

}
