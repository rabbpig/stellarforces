package dsr.start;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

import ssmith.awt.MyComboBox;
import ssmith.awt.MyPasswordField;
import ssmith.awt.MyTextField;
import dsr.SharedStatics;

public class LoginForm extends JFrame implements MouseListener, ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	private JButton btn_login;
	public MyTextField txt_email;
	public MyPasswordField txt_pwd;
	public JCheckBox chk_remember_pwd, chk_hi_mem, chk_music, chk_help_mode, chk_sfx, chk_show_jme;
	public boolean ok_clicked = false;
	public MyComboBox cbLang;
	
	public LoginForm(String login, String pwd, boolean remember_pwd, int lang_code, boolean hi_mem, boolean music, boolean help_mode, boolean sfx, boolean show_jme) {
		super();
		
		this.setLayout(null);
		
		this.setTitle(SharedStatics.TITLE);
		this.setSize(350, 515);
		this.setLocation(300, 200);
		//this.setAlwaysOnTop(true);
		this.setResizable(false);

		JLabel lbl1 = new JLabel(StartupNew.strings.getTranslation("Email") + "/" + StartupNew.strings.getTranslation("Login"));
		lbl1.setBounds(40, 15, 200, 30);
		lbl1.setAlignmentX(CENTER_ALIGNMENT);
		this.getContentPane().add(lbl1);
		
		txt_email = new MyTextField();
		txt_email.setBounds(40, 50, 250, 25);
		txt_email.setText(login);
		this.getContentPane().add(txt_email);
		
		JLabel lbl2 = new JLabel(StartupNew.strings.getTranslation("Password"));
		lbl2.setBounds(40, 85, 200, 30);
		lbl2.setAlignmentX(CENTER_ALIGNMENT);
		this.getContentPane().add(lbl2);
		
		txt_pwd = new MyPasswordField();
		txt_pwd.setText(pwd);
		txt_pwd.setBounds(40, 120, 250, 25);
		this.getContentPane().add(txt_pwd);
		
		chk_remember_pwd = new JCheckBox(StartupNew.strings.getTranslation("Remember Password"));
		chk_remember_pwd.setBounds(40, 160, 200, 30);
		chk_remember_pwd.setSelected(remember_pwd);
		this.getContentPane().add(chk_remember_pwd);
		
		chk_hi_mem = new JCheckBox(StartupNew.strings.getTranslation("My computer has lots of memory"));
		chk_hi_mem.setBounds(40, 190, 200, 30);
		chk_hi_mem.setSelected(hi_mem);
		this.getContentPane().add(chk_hi_mem);
		
		chk_music = new JCheckBox(StartupNew.strings.getTranslation("Music"));
		chk_music.setBounds(40, 220, 200, 30);
		chk_music.setSelected(music);
		this.getContentPane().add(chk_music);
		
		chk_sfx = new JCheckBox(StartupNew.strings.getTranslation("Sound F/X"));
		chk_sfx.setBounds(40, 250, 200, 30);
		chk_sfx.setSelected(sfx);
		this.getContentPane().add(chk_sfx);
		
		chk_help_mode = new JCheckBox(StartupNew.strings.getTranslation("Help Mode"));
		chk_help_mode.setBounds(40, 280, 200, 30);
		chk_help_mode.setSelected(help_mode);
		this.getContentPane().add(chk_help_mode);
		
		chk_show_jme = new JCheckBox(StartupNew.strings.getTranslation("Show 3D Options"));
		chk_show_jme.setBounds(40, 310, 200, 30);
		chk_show_jme.setSelected(false);//show_jme);
		this.getContentPane().add(chk_show_jme);
		
		JLabel lbl3 = new JLabel(StartupNew.strings.getTranslation("Language"));
		lbl3.setBounds(40, 350, 200, 30);
		lbl3.setAlignmentX(CENTER_ALIGNMENT);
		this.getContentPane().add(lbl3);
		
		String[] data = { "English", "Espa�ol", "Portugu�s", "Fran�ais", "Deutsch"}; // Must be in the same order as LoginForm consts.
		cbLang = new MyComboBox(data);
		cbLang.setBounds(40, 385, 250, 25);
		cbLang.setSelectedIndex(lang_code);
		this.getContentPane().add(cbLang);

		btn_login = new JButton(StartupNew.strings.getTranslation("Login"));
		btn_login.setBounds(240, 450, 90, 30);
		this.getContentPane().add(btn_login);
		btn_login.addMouseListener(this);
		btn_login.addActionListener(this);
		
		this.getRootPane().setDefaultButton(btn_login);
		this.setVisible(true);
		
		this.txt_email.requestFocusInWindow();
	}

	
	private void login() {
		ok_clicked = true;
		this.setVisible(false);
	}
	
	
	//@Override
	public void mouseClicked(MouseEvent arg0) {
		//AppletMain.p("Clicked!");
		if (arg0.getComponent() == btn_login) {
			login();
		}
	}

	//@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	//@Override
	public void mouseExited(MouseEvent arg0) {
	}

	//@Override
	public void mousePressed(MouseEvent arg0) {
	}

	//@Override
	public void mouseReleased(MouseEvent arg0) {
	}


	//@Override
	public void actionPerformed(ActionEvent arg0) {
		this.login();
	}

}
