package dsr.start;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;

import ssmith.io.TextFile;

public class StringData {

	private static final String UNKNOWN_WORDS = "./data/unknown_strings.txt";

	private int col = 0;
	private Hashtable<String, String> hash = new Hashtable<String, String>();
	private ArrayList<String> unknown_words;

	public StringData() {//throws FileNotFoundException, IOException {
		setLanguage(0); // Default
	}


	/**
	 * This returns a message.
	 */
	public String setLanguage(int lang) {
		try {
			col = lang;

			// Load the data
			TextFile tf = new TextFile();
			tf.openFile("./data/strings.csv", TextFile.READ);
			tf.readLine(); // Skip header
			while (tf.isEOF() == false) {
				String s = tf.readLine();
				if (s.length() > 0) {
					if (s.startsWith("#") == false) {
						String data[] = s.split("\t");
						if (data.length > 0) {
							try {
								hash.put(data[0].replaceAll("\"", "").trim(), data[col].replaceAll("\"", "").trim());
							} catch (ArrayIndexOutOfBoundsException ex) {
								ex.printStackTrace();
							}
						}
					}
				}
			}
			tf.close();

			unknown_words = new ArrayList<String>();
			if (new File(UNKNOWN_WORDS).canRead()) {
				tf.openFile(UNKNOWN_WORDS, TextFile.READ);
				while (tf.isEOF() == false) {
					String s = tf.readLine();
					if (s != null) {
						if (s.length() > 0) {
							if (s.startsWith("#") == false) {
								this.unknown_words.add(s);
							}
						}
					}
				}
				tf.close();
			}
			return "";
		} catch (Exception e) {
			//e.printStackTrace();
			//lw.displayMessage("WARNING: Translation strings cannot be found.  Reverting to English.");
			return "WARNING: Translation strings cannot be found.  Reverting to English.  (Error: " + e.toString() + ")";
		}
	}


	public String getTranslation(String text) {
		if (col == 0) {
			return text; // English
		} else {
			boolean ends_space = text.endsWith(" ");
			text = text.trim();
			if (hash.containsKey(text)) {
				String s = hash.get(text); 
				if (ends_space) {
					s = s + " ";
				}
				return s;
			} else {
				//if (AppletMain.CHECK_WORDS) {
				if (this.unknown_words.contains(text) == false) {
					try {
						TextFile.QuickAppend(UNKNOWN_WORDS, text, true, false);
						//text = getFromWeb(text, col);
					} catch (Exception e) {
						e.printStackTrace();
					}
					this.unknown_words.add(text);
				}
				//}
				return text;
			}
		}
	}

}
