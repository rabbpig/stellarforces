package dsr.start;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import dsr.SharedStatics;
import dsr.data.GameData;

public class SelectMissionForm extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private static final int BTN_HEIGHT = 50;

	public int selected_mission = -1;
	public boolean mission_selected = false;
	public boolean refresh_selected = false;
	public boolean playback_game = false;

	public SelectMissionForm(ArrayList<GameData> games) {
		super();

		this.setTitle(SharedStatics.TITLE + ": " + StartupNew.strings.getTranslation("Select Mission"));
		this.setSize(450, BTN_HEIGHT * (games.size()+2));
		this.setLocation(250, 250);
		//this.setAlwaysOnTop(true);
		this.setResizable(false);

		//this.getContentPane().setLayout(new GridLayout(games.size()+1, 1));
		this.getContentPane().setLayout(new GridLayout(0, 1));

		for (int i=0 ; i<games.size() ; i++) {
			GameData game = games.get(i);
			String s = game.mission_name + " vs " + game.getOpponentsName() + ", turn " + game.turn_no;
			ButtonMission btn = new ButtonMission(s, i);
			if (game.enabled == false) {
				btn.setEnabled(false);
				btn.setText(game.mission_name + " - UPGRADE CLIENT TO PLAY!");
			}
			btn.addActionListener(this);
			this.getContentPane().add(btn);
		}

		// Add playback button
		ButtonRefreshList btn = new ButtonRefreshList();
		btn.addActionListener(this);
		this.getContentPane().add(btn);

		// Add refresh button
			ButtonPlaybackMission btn2 = new ButtonPlaybackMission();
			btn2.addActionListener(this);
			this.getContentPane().add(btn2);

		this.setVisible(true);
	}


	//@Override
	public void actionPerformed(ActionEvent arg0) {
		//System.out.println("Click!");
		if (arg0.getSource() instanceof ButtonMission) {
			ButtonMission btn = (ButtonMission)arg0.getSource();
			this.selected_mission = btn.mission_num;
			this.mission_selected = true;
			this.setVisible(false);
		} else if (arg0.getSource() instanceof ButtonRefreshList) {
			this.refresh_selected = true;
			this.setVisible(false);
		} else if (arg0.getSource() instanceof ButtonPlaybackMission) {
			String s_mission = JOptionPane.showInputDialog(null, "Please enter the game number", "Playback Game", JOptionPane.QUESTION_MESSAGE);
			try {
				this.selected_mission = Integer.parseInt(s_mission);
				this.playback_game = true;
				this.setVisible(false);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

}
