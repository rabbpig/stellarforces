package dsr.start;

import java.io.File;
import java.io.IOException;

import ssmith.lang.Functions;
import ssmith.util.Interval;
import dsr.AppletMain;

public class UpdateFileLockThread extends Thread {

	public static final String FILENAME = AppletMain.DATA_DIR + ".lock";
	
	private Interval int_run_gc = new Interval(1000*10);

	public UpdateFileLockThread() {
		super("UpdateFileLockThread");
		this.setDaemon(true);
		start();
	}

	public void run() {
		File f = new File(FILENAME);
		try {
			while (true) {
				f.delete();
				f.createNewFile(); // So it's dated recently.
				Functions.delay(5000);
				
				if (int_run_gc.hitInterval()) {
					System.gc();
				}
			}
		} catch (IOException ex) {
			// Do nothing as we don't have permission to create a file
		} catch (Exception ex) {
			AppletMain.HandleError(null, ex);
			f.delete();
		}
	}


	public static boolean DoesRecentFileExist() {
		File f = new File(FILENAME);
		if (f.exists()) {
			long time = System.currentTimeMillis() - f.lastModified();
			return time < 5000;
		}
		return false;
	}

}
