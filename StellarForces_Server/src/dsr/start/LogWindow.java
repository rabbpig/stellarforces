package dsr.start;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import dsr.IDisplayMessages;
import dsr.SharedStatics;

public class LogWindow extends JFrame implements IDisplayMessages {
	
	private static final long serialVersionUID = 1L;
	
	private JTextArea ta;
	private StringBuffer str;
	
	public LogWindow() {
		super();
		
		str = new StringBuffer();
		
		this.setTitle(SharedStatics.TITLE + " Client");
		this.setSize(600, 200);
		this.setLocation(200, 200);
		this.setEnabled(true);
		
		this.getContentPane().setLayout(new BorderLayout());
		
		ta = new JTextArea();
		ta.setEditable(false);
		ta.setWrapStyleWord(true);
		ta.setLineWrap(true);
		JScrollPane jsp = new JScrollPane(ta);
		jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.getContentPane().add(jsp, BorderLayout.CENTER);
		
		//this.invalidate();
		//this.repaint();
		this.setVisible(true);

	}
	
	public void appendLine(String s) {
		str.append(s + "\n");
		ta.setText(str.toString());
		ta.setCaretPosition(str.length());
	}

	@Override
	public void displayMessage(String s) {
		appendLine(s);
	}

}
