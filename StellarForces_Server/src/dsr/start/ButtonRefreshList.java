package dsr.start;

import javax.swing.JButton;

public class ButtonRefreshList extends JButton {
	
	private static final long serialVersionUID = 1L;

	public ButtonRefreshList() {
		super();
		this.setText("Refresh List.");
	}

}
