package dsr;

public interface IUpdatable {

	public void update(float interpolation);
}
